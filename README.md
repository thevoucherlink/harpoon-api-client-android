# harpoon-api

## Requirements

Building the API client library requires [Maven](https://maven.apache.org/) to be installed.

## Installation

To install the API client library to your local Maven repository, simply execute:

```shell
mvn install
```

To deploy it to a remote Maven repository instead, configure the settings of the repository and execute:

```shell
mvn deploy
```

Refer to the [official documentation](https://maven.apache.org/plugins/maven-deploy-plugin/usage.html) for more information.

### Maven users

Add this dependency to your project's POM:

```xml
<dependency>
    <groupId>com.harpoon</groupId>
    <artifactId>harpoon-api</artifactId>
    <version>1.2.30</version>
    <scope>compile</scope>
</dependency>
```

### Gradle users

Add this dependency to your project's build file:

```groovy
compile "com.harpoon:harpoon-api:1.2.30"
```

### Others

At first generate the JAR by executing:

    mvn package

Then manually install the following JARs:

* target/harpoon-api-1.2.30.jar
* target/lib/*.jar

## Getting Started

Please follow the [installation](#installation) instruction and execute the following Java code:

```java

import com.harpoon.api.AnalyticApi;

public class AnalyticApiExample {

    public static void main(String[] args) {
        AnalyticApi apiInstance = new AnalyticApi();
        String id = "id_example"; // String | Model id
        Analytic data = new Analytic(); // Analytic | Model instance data
        try {
            Analytic result = apiInstance.analyticReplaceById(id, data);
            System.out.println(result);
        } catch (ApiException e) {
            System.err.println("Exception when calling AnalyticApi#analyticReplaceById");
            e.printStackTrace();
        }
    }
}

```

## Documentation for API Endpoints

All URIs are relative to *https://apicdn.harpoonconnect.com/v1.1.1*

Class | Method | HTTP request | Description
------------ | ------------- | ------------- | -------------
*AnalyticApi* | [**analyticReplaceById**](docs/AnalyticApi.md#analyticReplaceById) | **POST** /Analytics/{id}/replace | Replace attributes for a model instance and persist it into the data source.
*AnalyticApi* | [**analyticReplaceOrCreate**](docs/AnalyticApi.md#analyticReplaceOrCreate) | **POST** /Analytics/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
*AnalyticApi* | [**analyticTrack**](docs/AnalyticApi.md#analyticTrack) | **POST** /Analytics/track | 
*AnalyticApi* | [**analyticUpsertWithWhere**](docs/AnalyticApi.md#analyticUpsertWithWhere) | **POST** /Analytics/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.
*AppApi* | [**appAnalyticTrack**](docs/AppApi.md#appAnalyticTrack) | **POST** /Apps/{id}/analytics/track | 
*AppApi* | [**appCount**](docs/AppApi.md#appCount) | **GET** /Apps/count | Count instances of the model matched by where from the data source.
*AppApi* | [**appCreateChangeStreamGetAppsChangeStream**](docs/AppApi.md#appCreateChangeStreamGetAppsChangeStream) | **GET** /Apps/change-stream | Create a change stream.
*AppApi* | [**appCreateChangeStreamPostAppsChangeStream**](docs/AppApi.md#appCreateChangeStreamPostAppsChangeStream) | **POST** /Apps/change-stream | Create a change stream.
*AppApi* | [**appCreateWithAuthentication**](docs/AppApi.md#appCreateWithAuthentication) | **POST** /Apps | Create a new instance of the model and persist it into the data source.
*AppApi* | [**appDeleteById**](docs/AppApi.md#appDeleteById) | **DELETE** /Apps/{id} | Delete a model instance by {{id}} from the data source.
*AppApi* | [**appExistsGetAppsidExists**](docs/AppApi.md#appExistsGetAppsidExists) | **GET** /Apps/{id}/exists | Check whether a model instance exists in the data source.
*AppApi* | [**appExistsHeadAppsid**](docs/AppApi.md#appExistsHeadAppsid) | **HEAD** /Apps/{id} | Check whether a model instance exists in the data source.
*AppApi* | [**appFind**](docs/AppApi.md#appFind) | **GET** /Apps | Find all instances of the model matched by filter from the data source.
*AppApi* | [**appFindById**](docs/AppApi.md#appFindById) | **GET** /Apps/{id} | Find a model instance by {{id}} from the data source.
*AppApi* | [**appFindByIdForVersion**](docs/AppApi.md#appFindByIdForVersion) | **GET** /Apps/{id}/{appOs}/{appVersion} | 
*AppApi* | [**appFindOne**](docs/AppApi.md#appFindOne) | **GET** /Apps/findOne | Find first instance of the model matched by filter from the data source.
*AppApi* | [**appPrototypeCountAppConfigs**](docs/AppApi.md#appPrototypeCountAppConfigs) | **GET** /Apps/{id}/appConfigs/count | Counts appConfigs of App.
*AppApi* | [**appPrototypeCountApps**](docs/AppApi.md#appPrototypeCountApps) | **GET** /Apps/{id}/apps/count | Counts apps of App.
*AppApi* | [**appPrototypeCountPlaylists**](docs/AppApi.md#appPrototypeCountPlaylists) | **GET** /Apps/{id}/playlists/count | Counts playlists of App.
*AppApi* | [**appPrototypeCountRadioStreams**](docs/AppApi.md#appPrototypeCountRadioStreams) | **GET** /Apps/{id}/radioStreams/count | Counts radioStreams of App.
*AppApi* | [**appPrototypeCountRssFeedGroups**](docs/AppApi.md#appPrototypeCountRssFeedGroups) | **GET** /Apps/{id}/rssFeedGroups/count | Counts rssFeedGroups of App.
*AppApi* | [**appPrototypeCountRssFeeds**](docs/AppApi.md#appPrototypeCountRssFeeds) | **GET** /Apps/{id}/rssFeeds/count | Counts rssFeeds of App.
*AppApi* | [**appPrototypeCreateAppConfigs**](docs/AppApi.md#appPrototypeCreateAppConfigs) | **POST** /Apps/{id}/appConfigs | Creates a new instance in appConfigs of this model.
*AppApi* | [**appPrototypeCreateApps**](docs/AppApi.md#appPrototypeCreateApps) | **POST** /Apps/{id}/apps | Creates a new instance in apps of this model.
*AppApi* | [**appPrototypeCreateCustomers**](docs/AppApi.md#appPrototypeCreateCustomers) | **POST** /Apps/{id}/customers | Creates a new instance in customers of this model.
*AppApi* | [**appPrototypeCreatePlaylists**](docs/AppApi.md#appPrototypeCreatePlaylists) | **POST** /Apps/{id}/playlists | Creates a new instance in playlists of this model.
*AppApi* | [**appPrototypeCreateRadioStreams**](docs/AppApi.md#appPrototypeCreateRadioStreams) | **POST** /Apps/{id}/radioStreams | Creates a new instance in radioStreams of this model.
*AppApi* | [**appPrototypeCreateRssFeedGroups**](docs/AppApi.md#appPrototypeCreateRssFeedGroups) | **POST** /Apps/{id}/rssFeedGroups | Creates a new instance in rssFeedGroups of this model.
*AppApi* | [**appPrototypeCreateRssFeeds**](docs/AppApi.md#appPrototypeCreateRssFeeds) | **POST** /Apps/{id}/rssFeeds | Creates a new instance in rssFeeds of this model.
*AppApi* | [**appPrototypeDeleteAppConfigs**](docs/AppApi.md#appPrototypeDeleteAppConfigs) | **DELETE** /Apps/{id}/appConfigs | Deletes all appConfigs of this model.
*AppApi* | [**appPrototypeDeleteApps**](docs/AppApi.md#appPrototypeDeleteApps) | **DELETE** /Apps/{id}/apps | Deletes all apps of this model.
*AppApi* | [**appPrototypeDeletePlaylists**](docs/AppApi.md#appPrototypeDeletePlaylists) | **DELETE** /Apps/{id}/playlists | Deletes all playlists of this model.
*AppApi* | [**appPrototypeDeleteRadioStreams**](docs/AppApi.md#appPrototypeDeleteRadioStreams) | **DELETE** /Apps/{id}/radioStreams | Deletes all radioStreams of this model.
*AppApi* | [**appPrototypeDeleteRssFeedGroups**](docs/AppApi.md#appPrototypeDeleteRssFeedGroups) | **DELETE** /Apps/{id}/rssFeedGroups | Deletes all rssFeedGroups of this model.
*AppApi* | [**appPrototypeDeleteRssFeeds**](docs/AppApi.md#appPrototypeDeleteRssFeeds) | **DELETE** /Apps/{id}/rssFeeds | Deletes all rssFeeds of this model.
*AppApi* | [**appPrototypeDestroyByIdAppConfigs**](docs/AppApi.md#appPrototypeDestroyByIdAppConfigs) | **DELETE** /Apps/{id}/appConfigs/{fk} | Delete a related item by id for appConfigs.
*AppApi* | [**appPrototypeDestroyByIdApps**](docs/AppApi.md#appPrototypeDestroyByIdApps) | **DELETE** /Apps/{id}/apps/{fk} | Delete a related item by id for apps.
*AppApi* | [**appPrototypeDestroyByIdPlaylists**](docs/AppApi.md#appPrototypeDestroyByIdPlaylists) | **DELETE** /Apps/{id}/playlists/{fk} | Delete a related item by id for playlists.
*AppApi* | [**appPrototypeDestroyByIdRadioStreams**](docs/AppApi.md#appPrototypeDestroyByIdRadioStreams) | **DELETE** /Apps/{id}/radioStreams/{fk} | Delete a related item by id for radioStreams.
*AppApi* | [**appPrototypeDestroyByIdRssFeedGroups**](docs/AppApi.md#appPrototypeDestroyByIdRssFeedGroups) | **DELETE** /Apps/{id}/rssFeedGroups/{fk} | Delete a related item by id for rssFeedGroups.
*AppApi* | [**appPrototypeDestroyByIdRssFeeds**](docs/AppApi.md#appPrototypeDestroyByIdRssFeeds) | **DELETE** /Apps/{id}/rssFeeds/{fk} | Delete a related item by id for rssFeeds.
*AppApi* | [**appPrototypeFindByIdAppConfigs**](docs/AppApi.md#appPrototypeFindByIdAppConfigs) | **GET** /Apps/{id}/appConfigs/{fk} | Find a related item by id for appConfigs.
*AppApi* | [**appPrototypeFindByIdApps**](docs/AppApi.md#appPrototypeFindByIdApps) | **GET** /Apps/{id}/apps/{fk} | Find a related item by id for apps.
*AppApi* | [**appPrototypeFindByIdCustomers**](docs/AppApi.md#appPrototypeFindByIdCustomers) | **GET** /Apps/{id}/customers/{fk} | Find a related item by id for customers.
*AppApi* | [**appPrototypeFindByIdPlaylists**](docs/AppApi.md#appPrototypeFindByIdPlaylists) | **GET** /Apps/{id}/playlists/{fk} | Find a related item by id for playlists.
*AppApi* | [**appPrototypeFindByIdRadioStreams**](docs/AppApi.md#appPrototypeFindByIdRadioStreams) | **GET** /Apps/{id}/radioStreams/{fk} | Find a related item by id for radioStreams.
*AppApi* | [**appPrototypeFindByIdRssFeedGroups**](docs/AppApi.md#appPrototypeFindByIdRssFeedGroups) | **GET** /Apps/{id}/rssFeedGroups/{fk} | Find a related item by id for rssFeedGroups.
*AppApi* | [**appPrototypeFindByIdRssFeeds**](docs/AppApi.md#appPrototypeFindByIdRssFeeds) | **GET** /Apps/{id}/rssFeeds/{fk} | Find a related item by id for rssFeeds.
*AppApi* | [**appPrototypeGetAppConfigs**](docs/AppApi.md#appPrototypeGetAppConfigs) | **GET** /Apps/{id}/appConfigs | Queries appConfigs of App.
*AppApi* | [**appPrototypeGetApps**](docs/AppApi.md#appPrototypeGetApps) | **GET** /Apps/{id}/apps | Queries apps of App.
*AppApi* | [**appPrototypeGetCustomers**](docs/AppApi.md#appPrototypeGetCustomers) | **GET** /Apps/{id}/customers | Queries customers of App.
*AppApi* | [**appPrototypeGetParent**](docs/AppApi.md#appPrototypeGetParent) | **GET** /Apps/{id}/parent | Fetches belongsTo relation parent.
*AppApi* | [**appPrototypeGetPlaylists**](docs/AppApi.md#appPrototypeGetPlaylists) | **GET** /Apps/{id}/playlists | Queries playlists of App.
*AppApi* | [**appPrototypeGetRadioStreams**](docs/AppApi.md#appPrototypeGetRadioStreams) | **GET** /Apps/{id}/radioStreams | Queries radioStreams of App.
*AppApi* | [**appPrototypeGetRssFeedGroups**](docs/AppApi.md#appPrototypeGetRssFeedGroups) | **GET** /Apps/{id}/rssFeedGroups | Queries rssFeedGroups of App.
*AppApi* | [**appPrototypeGetRssFeeds**](docs/AppApi.md#appPrototypeGetRssFeeds) | **GET** /Apps/{id}/rssFeeds | Queries rssFeeds of App.
*AppApi* | [**appPrototypeUpdateAttributesPatchAppsid**](docs/AppApi.md#appPrototypeUpdateAttributesPatchAppsid) | **PATCH** /Apps/{id} | Patch attributes for a model instance and persist it into the data source.
*AppApi* | [**appPrototypeUpdateAttributesPutAppsid**](docs/AppApi.md#appPrototypeUpdateAttributesPutAppsid) | **PUT** /Apps/{id} | Patch attributes for a model instance and persist it into the data source.
*AppApi* | [**appPrototypeUpdateByIdAppConfigs**](docs/AppApi.md#appPrototypeUpdateByIdAppConfigs) | **PUT** /Apps/{id}/appConfigs/{fk} | Update a related item by id for appConfigs.
*AppApi* | [**appPrototypeUpdateByIdApps**](docs/AppApi.md#appPrototypeUpdateByIdApps) | **PUT** /Apps/{id}/apps/{fk} | Update a related item by id for apps.
*AppApi* | [**appPrototypeUpdateByIdCustomers**](docs/AppApi.md#appPrototypeUpdateByIdCustomers) | **PUT** /Apps/{id}/customers/{fk} | Update a related item by id for customers.
*AppApi* | [**appPrototypeUpdateByIdPlaylists**](docs/AppApi.md#appPrototypeUpdateByIdPlaylists) | **PUT** /Apps/{id}/playlists/{fk} | Update a related item by id for playlists.
*AppApi* | [**appPrototypeUpdateByIdRadioStreams**](docs/AppApi.md#appPrototypeUpdateByIdRadioStreams) | **PUT** /Apps/{id}/radioStreams/{fk} | Update a related item by id for radioStreams.
*AppApi* | [**appPrototypeUpdateByIdRssFeedGroups**](docs/AppApi.md#appPrototypeUpdateByIdRssFeedGroups) | **PUT** /Apps/{id}/rssFeedGroups/{fk} | Update a related item by id for rssFeedGroups.
*AppApi* | [**appPrototypeUpdateByIdRssFeeds**](docs/AppApi.md#appPrototypeUpdateByIdRssFeeds) | **PUT** /Apps/{id}/rssFeeds/{fk} | Update a related item by id for rssFeeds.
*AppApi* | [**appReplaceById**](docs/AppApi.md#appReplaceById) | **POST** /Apps/{id}/replace | Replace attributes for a model instance and persist it into the data source.
*AppApi* | [**appReplaceOrCreate**](docs/AppApi.md#appReplaceOrCreate) | **POST** /Apps/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
*AppApi* | [**appUpdateAll**](docs/AppApi.md#appUpdateAll) | **POST** /Apps/update | Update instances of the model matched by {{where}} from the data source.
*AppApi* | [**appUploadFile**](docs/AppApi.md#appUploadFile) | **POST** /Apps/{id}/file/{type} | 
*AppApi* | [**appUpsertPatchApps**](docs/AppApi.md#appUpsertPatchApps) | **PATCH** /Apps | Patch an existing model instance or insert a new one into the data source.
*AppApi* | [**appUpsertPutApps**](docs/AppApi.md#appUpsertPutApps) | **PUT** /Apps | Patch an existing model instance or insert a new one into the data source.
*AppApi* | [**appUpsertWithWhere**](docs/AppApi.md#appUpsertWithWhere) | **POST** /Apps/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.
*AppApi* | [**appVerify**](docs/AppApi.md#appVerify) | **GET** /Apps/verify | 
*AwCollpurCouponApi* | [**awCollpurCouponCount**](docs/AwCollpurCouponApi.md#awCollpurCouponCount) | **GET** /AwCollpurCoupons/count | Count instances of the model matched by where from the data source.
*AwCollpurCouponApi* | [**awCollpurCouponCreate**](docs/AwCollpurCouponApi.md#awCollpurCouponCreate) | **POST** /AwCollpurCoupons | Create a new instance of the model and persist it into the data source.
*AwCollpurCouponApi* | [**awCollpurCouponCreateChangeStreamGetAwCollpurCouponsChangeStream**](docs/AwCollpurCouponApi.md#awCollpurCouponCreateChangeStreamGetAwCollpurCouponsChangeStream) | **GET** /AwCollpurCoupons/change-stream | Create a change stream.
*AwCollpurCouponApi* | [**awCollpurCouponCreateChangeStreamPostAwCollpurCouponsChangeStream**](docs/AwCollpurCouponApi.md#awCollpurCouponCreateChangeStreamPostAwCollpurCouponsChangeStream) | **POST** /AwCollpurCoupons/change-stream | Create a change stream.
*AwCollpurCouponApi* | [**awCollpurCouponDeleteById**](docs/AwCollpurCouponApi.md#awCollpurCouponDeleteById) | **DELETE** /AwCollpurCoupons/{id} | Delete a model instance by {{id}} from the data source.
*AwCollpurCouponApi* | [**awCollpurCouponExistsGetAwCollpurCouponsidExists**](docs/AwCollpurCouponApi.md#awCollpurCouponExistsGetAwCollpurCouponsidExists) | **GET** /AwCollpurCoupons/{id}/exists | Check whether a model instance exists in the data source.
*AwCollpurCouponApi* | [**awCollpurCouponExistsHeadAwCollpurCouponsid**](docs/AwCollpurCouponApi.md#awCollpurCouponExistsHeadAwCollpurCouponsid) | **HEAD** /AwCollpurCoupons/{id} | Check whether a model instance exists in the data source.
*AwCollpurCouponApi* | [**awCollpurCouponFind**](docs/AwCollpurCouponApi.md#awCollpurCouponFind) | **GET** /AwCollpurCoupons | Find all instances of the model matched by filter from the data source.
*AwCollpurCouponApi* | [**awCollpurCouponFindById**](docs/AwCollpurCouponApi.md#awCollpurCouponFindById) | **GET** /AwCollpurCoupons/{id} | Find a model instance by {{id}} from the data source.
*AwCollpurCouponApi* | [**awCollpurCouponFindOne**](docs/AwCollpurCouponApi.md#awCollpurCouponFindOne) | **GET** /AwCollpurCoupons/findOne | Find first instance of the model matched by filter from the data source.
*AwCollpurCouponApi* | [**awCollpurCouponPrototypeUpdateAttributesPatchAwCollpurCouponsid**](docs/AwCollpurCouponApi.md#awCollpurCouponPrototypeUpdateAttributesPatchAwCollpurCouponsid) | **PATCH** /AwCollpurCoupons/{id} | Patch attributes for a model instance and persist it into the data source.
*AwCollpurCouponApi* | [**awCollpurCouponPrototypeUpdateAttributesPutAwCollpurCouponsid**](docs/AwCollpurCouponApi.md#awCollpurCouponPrototypeUpdateAttributesPutAwCollpurCouponsid) | **PUT** /AwCollpurCoupons/{id} | Patch attributes for a model instance and persist it into the data source.
*AwCollpurCouponApi* | [**awCollpurCouponReplaceById**](docs/AwCollpurCouponApi.md#awCollpurCouponReplaceById) | **POST** /AwCollpurCoupons/{id}/replace | Replace attributes for a model instance and persist it into the data source.
*AwCollpurCouponApi* | [**awCollpurCouponReplaceOrCreate**](docs/AwCollpurCouponApi.md#awCollpurCouponReplaceOrCreate) | **POST** /AwCollpurCoupons/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
*AwCollpurCouponApi* | [**awCollpurCouponUpdateAll**](docs/AwCollpurCouponApi.md#awCollpurCouponUpdateAll) | **POST** /AwCollpurCoupons/update | Update instances of the model matched by {{where}} from the data source.
*AwCollpurCouponApi* | [**awCollpurCouponUpsertPatchAwCollpurCoupons**](docs/AwCollpurCouponApi.md#awCollpurCouponUpsertPatchAwCollpurCoupons) | **PATCH** /AwCollpurCoupons | Patch an existing model instance or insert a new one into the data source.
*AwCollpurCouponApi* | [**awCollpurCouponUpsertPutAwCollpurCoupons**](docs/AwCollpurCouponApi.md#awCollpurCouponUpsertPutAwCollpurCoupons) | **PUT** /AwCollpurCoupons | Patch an existing model instance or insert a new one into the data source.
*AwCollpurCouponApi* | [**awCollpurCouponUpsertWithWhere**](docs/AwCollpurCouponApi.md#awCollpurCouponUpsertWithWhere) | **POST** /AwCollpurCoupons/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.
*AwCollpurDealApi* | [**awCollpurDealCount**](docs/AwCollpurDealApi.md#awCollpurDealCount) | **GET** /AwCollpurDeals/count | Count instances of the model matched by where from the data source.
*AwCollpurDealApi* | [**awCollpurDealCreate**](docs/AwCollpurDealApi.md#awCollpurDealCreate) | **POST** /AwCollpurDeals | Create a new instance of the model and persist it into the data source.
*AwCollpurDealApi* | [**awCollpurDealCreateChangeStreamGetAwCollpurDealsChangeStream**](docs/AwCollpurDealApi.md#awCollpurDealCreateChangeStreamGetAwCollpurDealsChangeStream) | **GET** /AwCollpurDeals/change-stream | Create a change stream.
*AwCollpurDealApi* | [**awCollpurDealCreateChangeStreamPostAwCollpurDealsChangeStream**](docs/AwCollpurDealApi.md#awCollpurDealCreateChangeStreamPostAwCollpurDealsChangeStream) | **POST** /AwCollpurDeals/change-stream | Create a change stream.
*AwCollpurDealApi* | [**awCollpurDealDeleteById**](docs/AwCollpurDealApi.md#awCollpurDealDeleteById) | **DELETE** /AwCollpurDeals/{id} | Delete a model instance by {{id}} from the data source.
*AwCollpurDealApi* | [**awCollpurDealExistsGetAwCollpurDealsidExists**](docs/AwCollpurDealApi.md#awCollpurDealExistsGetAwCollpurDealsidExists) | **GET** /AwCollpurDeals/{id}/exists | Check whether a model instance exists in the data source.
*AwCollpurDealApi* | [**awCollpurDealExistsHeadAwCollpurDealsid**](docs/AwCollpurDealApi.md#awCollpurDealExistsHeadAwCollpurDealsid) | **HEAD** /AwCollpurDeals/{id} | Check whether a model instance exists in the data source.
*AwCollpurDealApi* | [**awCollpurDealFind**](docs/AwCollpurDealApi.md#awCollpurDealFind) | **GET** /AwCollpurDeals | Find all instances of the model matched by filter from the data source.
*AwCollpurDealApi* | [**awCollpurDealFindById**](docs/AwCollpurDealApi.md#awCollpurDealFindById) | **GET** /AwCollpurDeals/{id} | Find a model instance by {{id}} from the data source.
*AwCollpurDealApi* | [**awCollpurDealFindOne**](docs/AwCollpurDealApi.md#awCollpurDealFindOne) | **GET** /AwCollpurDeals/findOne | Find first instance of the model matched by filter from the data source.
*AwCollpurDealApi* | [**awCollpurDealPrototypeCountAwCollpurDealPurchases**](docs/AwCollpurDealApi.md#awCollpurDealPrototypeCountAwCollpurDealPurchases) | **GET** /AwCollpurDeals/{id}/awCollpurDealPurchases/count | Counts awCollpurDealPurchases of AwCollpurDeal.
*AwCollpurDealApi* | [**awCollpurDealPrototypeCreateAwCollpurDealPurchases**](docs/AwCollpurDealApi.md#awCollpurDealPrototypeCreateAwCollpurDealPurchases) | **POST** /AwCollpurDeals/{id}/awCollpurDealPurchases | Creates a new instance in awCollpurDealPurchases of this model.
*AwCollpurDealApi* | [**awCollpurDealPrototypeDeleteAwCollpurDealPurchases**](docs/AwCollpurDealApi.md#awCollpurDealPrototypeDeleteAwCollpurDealPurchases) | **DELETE** /AwCollpurDeals/{id}/awCollpurDealPurchases | Deletes all awCollpurDealPurchases of this model.
*AwCollpurDealApi* | [**awCollpurDealPrototypeDestroyByIdAwCollpurDealPurchases**](docs/AwCollpurDealApi.md#awCollpurDealPrototypeDestroyByIdAwCollpurDealPurchases) | **DELETE** /AwCollpurDeals/{id}/awCollpurDealPurchases/{fk} | Delete a related item by id for awCollpurDealPurchases.
*AwCollpurDealApi* | [**awCollpurDealPrototypeFindByIdAwCollpurDealPurchases**](docs/AwCollpurDealApi.md#awCollpurDealPrototypeFindByIdAwCollpurDealPurchases) | **GET** /AwCollpurDeals/{id}/awCollpurDealPurchases/{fk} | Find a related item by id for awCollpurDealPurchases.
*AwCollpurDealApi* | [**awCollpurDealPrototypeGetAwCollpurDealPurchases**](docs/AwCollpurDealApi.md#awCollpurDealPrototypeGetAwCollpurDealPurchases) | **GET** /AwCollpurDeals/{id}/awCollpurDealPurchases | Queries awCollpurDealPurchases of AwCollpurDeal.
*AwCollpurDealApi* | [**awCollpurDealPrototypeUpdateAttributesPatchAwCollpurDealsid**](docs/AwCollpurDealApi.md#awCollpurDealPrototypeUpdateAttributesPatchAwCollpurDealsid) | **PATCH** /AwCollpurDeals/{id} | Patch attributes for a model instance and persist it into the data source.
*AwCollpurDealApi* | [**awCollpurDealPrototypeUpdateAttributesPutAwCollpurDealsid**](docs/AwCollpurDealApi.md#awCollpurDealPrototypeUpdateAttributesPutAwCollpurDealsid) | **PUT** /AwCollpurDeals/{id} | Patch attributes for a model instance and persist it into the data source.
*AwCollpurDealApi* | [**awCollpurDealPrototypeUpdateByIdAwCollpurDealPurchases**](docs/AwCollpurDealApi.md#awCollpurDealPrototypeUpdateByIdAwCollpurDealPurchases) | **PUT** /AwCollpurDeals/{id}/awCollpurDealPurchases/{fk} | Update a related item by id for awCollpurDealPurchases.
*AwCollpurDealApi* | [**awCollpurDealReplaceById**](docs/AwCollpurDealApi.md#awCollpurDealReplaceById) | **POST** /AwCollpurDeals/{id}/replace | Replace attributes for a model instance and persist it into the data source.
*AwCollpurDealApi* | [**awCollpurDealReplaceOrCreate**](docs/AwCollpurDealApi.md#awCollpurDealReplaceOrCreate) | **POST** /AwCollpurDeals/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
*AwCollpurDealApi* | [**awCollpurDealUpdateAll**](docs/AwCollpurDealApi.md#awCollpurDealUpdateAll) | **POST** /AwCollpurDeals/update | Update instances of the model matched by {{where}} from the data source.
*AwCollpurDealApi* | [**awCollpurDealUpsertPatchAwCollpurDeals**](docs/AwCollpurDealApi.md#awCollpurDealUpsertPatchAwCollpurDeals) | **PATCH** /AwCollpurDeals | Patch an existing model instance or insert a new one into the data source.
*AwCollpurDealApi* | [**awCollpurDealUpsertPutAwCollpurDeals**](docs/AwCollpurDealApi.md#awCollpurDealUpsertPutAwCollpurDeals) | **PUT** /AwCollpurDeals | Patch an existing model instance or insert a new one into the data source.
*AwCollpurDealApi* | [**awCollpurDealUpsertWithWhere**](docs/AwCollpurDealApi.md#awCollpurDealUpsertWithWhere) | **POST** /AwCollpurDeals/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.
*AwCollpurDealPurchasesApi* | [**awCollpurDealPurchasesCount**](docs/AwCollpurDealPurchasesApi.md#awCollpurDealPurchasesCount) | **GET** /AwCollpurDealPurchases/count | Count instances of the model matched by where from the data source.
*AwCollpurDealPurchasesApi* | [**awCollpurDealPurchasesCreate**](docs/AwCollpurDealPurchasesApi.md#awCollpurDealPurchasesCreate) | **POST** /AwCollpurDealPurchases | Create a new instance of the model and persist it into the data source.
*AwCollpurDealPurchasesApi* | [**awCollpurDealPurchasesCreateChangeStreamGetAwCollpurDealPurchasesChangeStream**](docs/AwCollpurDealPurchasesApi.md#awCollpurDealPurchasesCreateChangeStreamGetAwCollpurDealPurchasesChangeStream) | **GET** /AwCollpurDealPurchases/change-stream | Create a change stream.
*AwCollpurDealPurchasesApi* | [**awCollpurDealPurchasesCreateChangeStreamPostAwCollpurDealPurchasesChangeStream**](docs/AwCollpurDealPurchasesApi.md#awCollpurDealPurchasesCreateChangeStreamPostAwCollpurDealPurchasesChangeStream) | **POST** /AwCollpurDealPurchases/change-stream | Create a change stream.
*AwCollpurDealPurchasesApi* | [**awCollpurDealPurchasesDeleteById**](docs/AwCollpurDealPurchasesApi.md#awCollpurDealPurchasesDeleteById) | **DELETE** /AwCollpurDealPurchases/{id} | Delete a model instance by {{id}} from the data source.
*AwCollpurDealPurchasesApi* | [**awCollpurDealPurchasesExistsGetAwCollpurDealPurchasesidExists**](docs/AwCollpurDealPurchasesApi.md#awCollpurDealPurchasesExistsGetAwCollpurDealPurchasesidExists) | **GET** /AwCollpurDealPurchases/{id}/exists | Check whether a model instance exists in the data source.
*AwCollpurDealPurchasesApi* | [**awCollpurDealPurchasesExistsHeadAwCollpurDealPurchasesid**](docs/AwCollpurDealPurchasesApi.md#awCollpurDealPurchasesExistsHeadAwCollpurDealPurchasesid) | **HEAD** /AwCollpurDealPurchases/{id} | Check whether a model instance exists in the data source.
*AwCollpurDealPurchasesApi* | [**awCollpurDealPurchasesFind**](docs/AwCollpurDealPurchasesApi.md#awCollpurDealPurchasesFind) | **GET** /AwCollpurDealPurchases | Find all instances of the model matched by filter from the data source.
*AwCollpurDealPurchasesApi* | [**awCollpurDealPurchasesFindById**](docs/AwCollpurDealPurchasesApi.md#awCollpurDealPurchasesFindById) | **GET** /AwCollpurDealPurchases/{id} | Find a model instance by {{id}} from the data source.
*AwCollpurDealPurchasesApi* | [**awCollpurDealPurchasesFindOne**](docs/AwCollpurDealPurchasesApi.md#awCollpurDealPurchasesFindOne) | **GET** /AwCollpurDealPurchases/findOne | Find first instance of the model matched by filter from the data source.
*AwCollpurDealPurchasesApi* | [**awCollpurDealPurchasesPrototypeCreateAwCollpurCoupon**](docs/AwCollpurDealPurchasesApi.md#awCollpurDealPurchasesPrototypeCreateAwCollpurCoupon) | **POST** /AwCollpurDealPurchases/{id}/awCollpurCoupon | Creates a new instance in awCollpurCoupon of this model.
*AwCollpurDealPurchasesApi* | [**awCollpurDealPurchasesPrototypeDestroyAwCollpurCoupon**](docs/AwCollpurDealPurchasesApi.md#awCollpurDealPurchasesPrototypeDestroyAwCollpurCoupon) | **DELETE** /AwCollpurDealPurchases/{id}/awCollpurCoupon | Deletes awCollpurCoupon of this model.
*AwCollpurDealPurchasesApi* | [**awCollpurDealPurchasesPrototypeGetAwCollpurCoupon**](docs/AwCollpurDealPurchasesApi.md#awCollpurDealPurchasesPrototypeGetAwCollpurCoupon) | **GET** /AwCollpurDealPurchases/{id}/awCollpurCoupon | Fetches hasOne relation awCollpurCoupon.
*AwCollpurDealPurchasesApi* | [**awCollpurDealPurchasesPrototypeUpdateAttributesPatchAwCollpurDealPurchasesid**](docs/AwCollpurDealPurchasesApi.md#awCollpurDealPurchasesPrototypeUpdateAttributesPatchAwCollpurDealPurchasesid) | **PATCH** /AwCollpurDealPurchases/{id} | Patch attributes for a model instance and persist it into the data source.
*AwCollpurDealPurchasesApi* | [**awCollpurDealPurchasesPrototypeUpdateAttributesPutAwCollpurDealPurchasesid**](docs/AwCollpurDealPurchasesApi.md#awCollpurDealPurchasesPrototypeUpdateAttributesPutAwCollpurDealPurchasesid) | **PUT** /AwCollpurDealPurchases/{id} | Patch attributes for a model instance and persist it into the data source.
*AwCollpurDealPurchasesApi* | [**awCollpurDealPurchasesPrototypeUpdateAwCollpurCoupon**](docs/AwCollpurDealPurchasesApi.md#awCollpurDealPurchasesPrototypeUpdateAwCollpurCoupon) | **PUT** /AwCollpurDealPurchases/{id}/awCollpurCoupon | Update awCollpurCoupon of this model.
*AwCollpurDealPurchasesApi* | [**awCollpurDealPurchasesReplaceById**](docs/AwCollpurDealPurchasesApi.md#awCollpurDealPurchasesReplaceById) | **POST** /AwCollpurDealPurchases/{id}/replace | Replace attributes for a model instance and persist it into the data source.
*AwCollpurDealPurchasesApi* | [**awCollpurDealPurchasesReplaceOrCreate**](docs/AwCollpurDealPurchasesApi.md#awCollpurDealPurchasesReplaceOrCreate) | **POST** /AwCollpurDealPurchases/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
*AwCollpurDealPurchasesApi* | [**awCollpurDealPurchasesUpdateAll**](docs/AwCollpurDealPurchasesApi.md#awCollpurDealPurchasesUpdateAll) | **POST** /AwCollpurDealPurchases/update | Update instances of the model matched by {{where}} from the data source.
*AwCollpurDealPurchasesApi* | [**awCollpurDealPurchasesUpsertPatchAwCollpurDealPurchases**](docs/AwCollpurDealPurchasesApi.md#awCollpurDealPurchasesUpsertPatchAwCollpurDealPurchases) | **PATCH** /AwCollpurDealPurchases | Patch an existing model instance or insert a new one into the data source.
*AwCollpurDealPurchasesApi* | [**awCollpurDealPurchasesUpsertPutAwCollpurDealPurchases**](docs/AwCollpurDealPurchasesApi.md#awCollpurDealPurchasesUpsertPutAwCollpurDealPurchases) | **PUT** /AwCollpurDealPurchases | Patch an existing model instance or insert a new one into the data source.
*AwCollpurDealPurchasesApi* | [**awCollpurDealPurchasesUpsertWithWhere**](docs/AwCollpurDealPurchasesApi.md#awCollpurDealPurchasesUpsertWithWhere) | **POST** /AwCollpurDealPurchases/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.
*AwEventbookingEventApi* | [**awEventbookingEventCount**](docs/AwEventbookingEventApi.md#awEventbookingEventCount) | **GET** /AwEventbookingEvents/count | Count instances of the model matched by where from the data source.
*AwEventbookingEventApi* | [**awEventbookingEventCreate**](docs/AwEventbookingEventApi.md#awEventbookingEventCreate) | **POST** /AwEventbookingEvents | Create a new instance of the model and persist it into the data source.
*AwEventbookingEventApi* | [**awEventbookingEventCreateChangeStreamGetAwEventbookingEventsChangeStream**](docs/AwEventbookingEventApi.md#awEventbookingEventCreateChangeStreamGetAwEventbookingEventsChangeStream) | **GET** /AwEventbookingEvents/change-stream | Create a change stream.
*AwEventbookingEventApi* | [**awEventbookingEventCreateChangeStreamPostAwEventbookingEventsChangeStream**](docs/AwEventbookingEventApi.md#awEventbookingEventCreateChangeStreamPostAwEventbookingEventsChangeStream) | **POST** /AwEventbookingEvents/change-stream | Create a change stream.
*AwEventbookingEventApi* | [**awEventbookingEventDeleteById**](docs/AwEventbookingEventApi.md#awEventbookingEventDeleteById) | **DELETE** /AwEventbookingEvents/{id} | Delete a model instance by {{id}} from the data source.
*AwEventbookingEventApi* | [**awEventbookingEventExistsGetAwEventbookingEventsidExists**](docs/AwEventbookingEventApi.md#awEventbookingEventExistsGetAwEventbookingEventsidExists) | **GET** /AwEventbookingEvents/{id}/exists | Check whether a model instance exists in the data source.
*AwEventbookingEventApi* | [**awEventbookingEventExistsHeadAwEventbookingEventsid**](docs/AwEventbookingEventApi.md#awEventbookingEventExistsHeadAwEventbookingEventsid) | **HEAD** /AwEventbookingEvents/{id} | Check whether a model instance exists in the data source.
*AwEventbookingEventApi* | [**awEventbookingEventFind**](docs/AwEventbookingEventApi.md#awEventbookingEventFind) | **GET** /AwEventbookingEvents | Find all instances of the model matched by filter from the data source.
*AwEventbookingEventApi* | [**awEventbookingEventFindById**](docs/AwEventbookingEventApi.md#awEventbookingEventFindById) | **GET** /AwEventbookingEvents/{id} | Find a model instance by {{id}} from the data source.
*AwEventbookingEventApi* | [**awEventbookingEventFindOne**](docs/AwEventbookingEventApi.md#awEventbookingEventFindOne) | **GET** /AwEventbookingEvents/findOne | Find first instance of the model matched by filter from the data source.
*AwEventbookingEventApi* | [**awEventbookingEventPrototypeCountAttributes**](docs/AwEventbookingEventApi.md#awEventbookingEventPrototypeCountAttributes) | **GET** /AwEventbookingEvents/{id}/attributes/count | Counts attributes of AwEventbookingEvent.
*AwEventbookingEventApi* | [**awEventbookingEventPrototypeCountPurchasedTickets**](docs/AwEventbookingEventApi.md#awEventbookingEventPrototypeCountPurchasedTickets) | **GET** /AwEventbookingEvents/{id}/purchasedTickets/count | Counts purchasedTickets of AwEventbookingEvent.
*AwEventbookingEventApi* | [**awEventbookingEventPrototypeCountTickets**](docs/AwEventbookingEventApi.md#awEventbookingEventPrototypeCountTickets) | **GET** /AwEventbookingEvents/{id}/tickets/count | Counts tickets of AwEventbookingEvent.
*AwEventbookingEventApi* | [**awEventbookingEventPrototypeCreateAttributes**](docs/AwEventbookingEventApi.md#awEventbookingEventPrototypeCreateAttributes) | **POST** /AwEventbookingEvents/{id}/attributes | Creates a new instance in attributes of this model.
*AwEventbookingEventApi* | [**awEventbookingEventPrototypeCreatePurchasedTickets**](docs/AwEventbookingEventApi.md#awEventbookingEventPrototypeCreatePurchasedTickets) | **POST** /AwEventbookingEvents/{id}/purchasedTickets | Creates a new instance in purchasedTickets of this model.
*AwEventbookingEventApi* | [**awEventbookingEventPrototypeCreateTickets**](docs/AwEventbookingEventApi.md#awEventbookingEventPrototypeCreateTickets) | **POST** /AwEventbookingEvents/{id}/tickets | Creates a new instance in tickets of this model.
*AwEventbookingEventApi* | [**awEventbookingEventPrototypeDeleteAttributes**](docs/AwEventbookingEventApi.md#awEventbookingEventPrototypeDeleteAttributes) | **DELETE** /AwEventbookingEvents/{id}/attributes | Deletes all attributes of this model.
*AwEventbookingEventApi* | [**awEventbookingEventPrototypeDeletePurchasedTickets**](docs/AwEventbookingEventApi.md#awEventbookingEventPrototypeDeletePurchasedTickets) | **DELETE** /AwEventbookingEvents/{id}/purchasedTickets | Deletes all purchasedTickets of this model.
*AwEventbookingEventApi* | [**awEventbookingEventPrototypeDeleteTickets**](docs/AwEventbookingEventApi.md#awEventbookingEventPrototypeDeleteTickets) | **DELETE** /AwEventbookingEvents/{id}/tickets | Deletes all tickets of this model.
*AwEventbookingEventApi* | [**awEventbookingEventPrototypeDestroyByIdAttributes**](docs/AwEventbookingEventApi.md#awEventbookingEventPrototypeDestroyByIdAttributes) | **DELETE** /AwEventbookingEvents/{id}/attributes/{fk} | Delete a related item by id for attributes.
*AwEventbookingEventApi* | [**awEventbookingEventPrototypeDestroyByIdPurchasedTickets**](docs/AwEventbookingEventApi.md#awEventbookingEventPrototypeDestroyByIdPurchasedTickets) | **DELETE** /AwEventbookingEvents/{id}/purchasedTickets/{fk} | Delete a related item by id for purchasedTickets.
*AwEventbookingEventApi* | [**awEventbookingEventPrototypeDestroyByIdTickets**](docs/AwEventbookingEventApi.md#awEventbookingEventPrototypeDestroyByIdTickets) | **DELETE** /AwEventbookingEvents/{id}/tickets/{fk} | Delete a related item by id for tickets.
*AwEventbookingEventApi* | [**awEventbookingEventPrototypeExistsPurchasedTickets**](docs/AwEventbookingEventApi.md#awEventbookingEventPrototypeExistsPurchasedTickets) | **HEAD** /AwEventbookingEvents/{id}/purchasedTickets/rel/{fk} | Check the existence of purchasedTickets relation to an item by id.
*AwEventbookingEventApi* | [**awEventbookingEventPrototypeFindByIdAttributes**](docs/AwEventbookingEventApi.md#awEventbookingEventPrototypeFindByIdAttributes) | **GET** /AwEventbookingEvents/{id}/attributes/{fk} | Find a related item by id for attributes.
*AwEventbookingEventApi* | [**awEventbookingEventPrototypeFindByIdPurchasedTickets**](docs/AwEventbookingEventApi.md#awEventbookingEventPrototypeFindByIdPurchasedTickets) | **GET** /AwEventbookingEvents/{id}/purchasedTickets/{fk} | Find a related item by id for purchasedTickets.
*AwEventbookingEventApi* | [**awEventbookingEventPrototypeFindByIdTickets**](docs/AwEventbookingEventApi.md#awEventbookingEventPrototypeFindByIdTickets) | **GET** /AwEventbookingEvents/{id}/tickets/{fk} | Find a related item by id for tickets.
*AwEventbookingEventApi* | [**awEventbookingEventPrototypeGetAttributes**](docs/AwEventbookingEventApi.md#awEventbookingEventPrototypeGetAttributes) | **GET** /AwEventbookingEvents/{id}/attributes | Queries attributes of AwEventbookingEvent.
*AwEventbookingEventApi* | [**awEventbookingEventPrototypeGetPurchasedTickets**](docs/AwEventbookingEventApi.md#awEventbookingEventPrototypeGetPurchasedTickets) | **GET** /AwEventbookingEvents/{id}/purchasedTickets | Queries purchasedTickets of AwEventbookingEvent.
*AwEventbookingEventApi* | [**awEventbookingEventPrototypeGetTickets**](docs/AwEventbookingEventApi.md#awEventbookingEventPrototypeGetTickets) | **GET** /AwEventbookingEvents/{id}/tickets | Queries tickets of AwEventbookingEvent.
*AwEventbookingEventApi* | [**awEventbookingEventPrototypeLinkPurchasedTickets**](docs/AwEventbookingEventApi.md#awEventbookingEventPrototypeLinkPurchasedTickets) | **PUT** /AwEventbookingEvents/{id}/purchasedTickets/rel/{fk} | Add a related item by id for purchasedTickets.
*AwEventbookingEventApi* | [**awEventbookingEventPrototypeUnlinkPurchasedTickets**](docs/AwEventbookingEventApi.md#awEventbookingEventPrototypeUnlinkPurchasedTickets) | **DELETE** /AwEventbookingEvents/{id}/purchasedTickets/rel/{fk} | Remove the purchasedTickets relation to an item by id.
*AwEventbookingEventApi* | [**awEventbookingEventPrototypeUpdateAttributesPatchAwEventbookingEventsid**](docs/AwEventbookingEventApi.md#awEventbookingEventPrototypeUpdateAttributesPatchAwEventbookingEventsid) | **PATCH** /AwEventbookingEvents/{id} | Patch attributes for a model instance and persist it into the data source.
*AwEventbookingEventApi* | [**awEventbookingEventPrototypeUpdateAttributesPutAwEventbookingEventsid**](docs/AwEventbookingEventApi.md#awEventbookingEventPrototypeUpdateAttributesPutAwEventbookingEventsid) | **PUT** /AwEventbookingEvents/{id} | Patch attributes for a model instance and persist it into the data source.
*AwEventbookingEventApi* | [**awEventbookingEventPrototypeUpdateByIdAttributes**](docs/AwEventbookingEventApi.md#awEventbookingEventPrototypeUpdateByIdAttributes) | **PUT** /AwEventbookingEvents/{id}/attributes/{fk} | Update a related item by id for attributes.
*AwEventbookingEventApi* | [**awEventbookingEventPrototypeUpdateByIdPurchasedTickets**](docs/AwEventbookingEventApi.md#awEventbookingEventPrototypeUpdateByIdPurchasedTickets) | **PUT** /AwEventbookingEvents/{id}/purchasedTickets/{fk} | Update a related item by id for purchasedTickets.
*AwEventbookingEventApi* | [**awEventbookingEventPrototypeUpdateByIdTickets**](docs/AwEventbookingEventApi.md#awEventbookingEventPrototypeUpdateByIdTickets) | **PUT** /AwEventbookingEvents/{id}/tickets/{fk} | Update a related item by id for tickets.
*AwEventbookingEventApi* | [**awEventbookingEventReplaceById**](docs/AwEventbookingEventApi.md#awEventbookingEventReplaceById) | **POST** /AwEventbookingEvents/{id}/replace | Replace attributes for a model instance and persist it into the data source.
*AwEventbookingEventApi* | [**awEventbookingEventReplaceOrCreate**](docs/AwEventbookingEventApi.md#awEventbookingEventReplaceOrCreate) | **POST** /AwEventbookingEvents/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
*AwEventbookingEventApi* | [**awEventbookingEventUpdateAll**](docs/AwEventbookingEventApi.md#awEventbookingEventUpdateAll) | **POST** /AwEventbookingEvents/update | Update instances of the model matched by {{where}} from the data source.
*AwEventbookingEventApi* | [**awEventbookingEventUpsertPatchAwEventbookingEvents**](docs/AwEventbookingEventApi.md#awEventbookingEventUpsertPatchAwEventbookingEvents) | **PATCH** /AwEventbookingEvents | Patch an existing model instance or insert a new one into the data source.
*AwEventbookingEventApi* | [**awEventbookingEventUpsertPutAwEventbookingEvents**](docs/AwEventbookingEventApi.md#awEventbookingEventUpsertPutAwEventbookingEvents) | **PUT** /AwEventbookingEvents | Patch an existing model instance or insert a new one into the data source.
*AwEventbookingEventApi* | [**awEventbookingEventUpsertWithWhere**](docs/AwEventbookingEventApi.md#awEventbookingEventUpsertWithWhere) | **POST** /AwEventbookingEvents/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.
*AwEventbookingEventTicketApi* | [**awEventbookingEventTicketCount**](docs/AwEventbookingEventTicketApi.md#awEventbookingEventTicketCount) | **GET** /AwEventbookingEventTickets/count | Count instances of the model matched by where from the data source.
*AwEventbookingEventTicketApi* | [**awEventbookingEventTicketCreate**](docs/AwEventbookingEventTicketApi.md#awEventbookingEventTicketCreate) | **POST** /AwEventbookingEventTickets | Create a new instance of the model and persist it into the data source.
*AwEventbookingEventTicketApi* | [**awEventbookingEventTicketCreateChangeStreamGetAwEventbookingEventTicketsChangeStream**](docs/AwEventbookingEventTicketApi.md#awEventbookingEventTicketCreateChangeStreamGetAwEventbookingEventTicketsChangeStream) | **GET** /AwEventbookingEventTickets/change-stream | Create a change stream.
*AwEventbookingEventTicketApi* | [**awEventbookingEventTicketCreateChangeStreamPostAwEventbookingEventTicketsChangeStream**](docs/AwEventbookingEventTicketApi.md#awEventbookingEventTicketCreateChangeStreamPostAwEventbookingEventTicketsChangeStream) | **POST** /AwEventbookingEventTickets/change-stream | Create a change stream.
*AwEventbookingEventTicketApi* | [**awEventbookingEventTicketDeleteById**](docs/AwEventbookingEventTicketApi.md#awEventbookingEventTicketDeleteById) | **DELETE** /AwEventbookingEventTickets/{id} | Delete a model instance by {{id}} from the data source.
*AwEventbookingEventTicketApi* | [**awEventbookingEventTicketExistsGetAwEventbookingEventTicketsidExists**](docs/AwEventbookingEventTicketApi.md#awEventbookingEventTicketExistsGetAwEventbookingEventTicketsidExists) | **GET** /AwEventbookingEventTickets/{id}/exists | Check whether a model instance exists in the data source.
*AwEventbookingEventTicketApi* | [**awEventbookingEventTicketExistsHeadAwEventbookingEventTicketsid**](docs/AwEventbookingEventTicketApi.md#awEventbookingEventTicketExistsHeadAwEventbookingEventTicketsid) | **HEAD** /AwEventbookingEventTickets/{id} | Check whether a model instance exists in the data source.
*AwEventbookingEventTicketApi* | [**awEventbookingEventTicketFind**](docs/AwEventbookingEventTicketApi.md#awEventbookingEventTicketFind) | **GET** /AwEventbookingEventTickets | Find all instances of the model matched by filter from the data source.
*AwEventbookingEventTicketApi* | [**awEventbookingEventTicketFindById**](docs/AwEventbookingEventTicketApi.md#awEventbookingEventTicketFindById) | **GET** /AwEventbookingEventTickets/{id} | Find a model instance by {{id}} from the data source.
*AwEventbookingEventTicketApi* | [**awEventbookingEventTicketFindOne**](docs/AwEventbookingEventTicketApi.md#awEventbookingEventTicketFindOne) | **GET** /AwEventbookingEventTickets/findOne | Find first instance of the model matched by filter from the data source.
*AwEventbookingEventTicketApi* | [**awEventbookingEventTicketPrototypeCountAttributes**](docs/AwEventbookingEventTicketApi.md#awEventbookingEventTicketPrototypeCountAttributes) | **GET** /AwEventbookingEventTickets/{id}/attributes/count | Counts attributes of AwEventbookingEventTicket.
*AwEventbookingEventTicketApi* | [**awEventbookingEventTicketPrototypeCountAwEventbookingTicket**](docs/AwEventbookingEventTicketApi.md#awEventbookingEventTicketPrototypeCountAwEventbookingTicket) | **GET** /AwEventbookingEventTickets/{id}/awEventbookingTicket/count | Counts awEventbookingTicket of AwEventbookingEventTicket.
*AwEventbookingEventTicketApi* | [**awEventbookingEventTicketPrototypeCountTickets**](docs/AwEventbookingEventTicketApi.md#awEventbookingEventTicketPrototypeCountTickets) | **GET** /AwEventbookingEventTickets/{id}/tickets/count | Counts tickets of AwEventbookingEventTicket.
*AwEventbookingEventTicketApi* | [**awEventbookingEventTicketPrototypeCreateAttributes**](docs/AwEventbookingEventTicketApi.md#awEventbookingEventTicketPrototypeCreateAttributes) | **POST** /AwEventbookingEventTickets/{id}/attributes | Creates a new instance in attributes of this model.
*AwEventbookingEventTicketApi* | [**awEventbookingEventTicketPrototypeCreateAwEventbookingTicket**](docs/AwEventbookingEventTicketApi.md#awEventbookingEventTicketPrototypeCreateAwEventbookingTicket) | **POST** /AwEventbookingEventTickets/{id}/awEventbookingTicket | Creates a new instance in awEventbookingTicket of this model.
*AwEventbookingEventTicketApi* | [**awEventbookingEventTicketPrototypeCreateTickets**](docs/AwEventbookingEventTicketApi.md#awEventbookingEventTicketPrototypeCreateTickets) | **POST** /AwEventbookingEventTickets/{id}/tickets | Creates a new instance in tickets of this model.
*AwEventbookingEventTicketApi* | [**awEventbookingEventTicketPrototypeDeleteAttributes**](docs/AwEventbookingEventTicketApi.md#awEventbookingEventTicketPrototypeDeleteAttributes) | **DELETE** /AwEventbookingEventTickets/{id}/attributes | Deletes all attributes of this model.
*AwEventbookingEventTicketApi* | [**awEventbookingEventTicketPrototypeDeleteAwEventbookingTicket**](docs/AwEventbookingEventTicketApi.md#awEventbookingEventTicketPrototypeDeleteAwEventbookingTicket) | **DELETE** /AwEventbookingEventTickets/{id}/awEventbookingTicket | Deletes all awEventbookingTicket of this model.
*AwEventbookingEventTicketApi* | [**awEventbookingEventTicketPrototypeDeleteTickets**](docs/AwEventbookingEventTicketApi.md#awEventbookingEventTicketPrototypeDeleteTickets) | **DELETE** /AwEventbookingEventTickets/{id}/tickets | Deletes all tickets of this model.
*AwEventbookingEventTicketApi* | [**awEventbookingEventTicketPrototypeDestroyByIdAttributes**](docs/AwEventbookingEventTicketApi.md#awEventbookingEventTicketPrototypeDestroyByIdAttributes) | **DELETE** /AwEventbookingEventTickets/{id}/attributes/{fk} | Delete a related item by id for attributes.
*AwEventbookingEventTicketApi* | [**awEventbookingEventTicketPrototypeDestroyByIdAwEventbookingTicket**](docs/AwEventbookingEventTicketApi.md#awEventbookingEventTicketPrototypeDestroyByIdAwEventbookingTicket) | **DELETE** /AwEventbookingEventTickets/{id}/awEventbookingTicket/{fk} | Delete a related item by id for awEventbookingTicket.
*AwEventbookingEventTicketApi* | [**awEventbookingEventTicketPrototypeDestroyByIdTickets**](docs/AwEventbookingEventTicketApi.md#awEventbookingEventTicketPrototypeDestroyByIdTickets) | **DELETE** /AwEventbookingEventTickets/{id}/tickets/{fk} | Delete a related item by id for tickets.
*AwEventbookingEventTicketApi* | [**awEventbookingEventTicketPrototypeFindByIdAttributes**](docs/AwEventbookingEventTicketApi.md#awEventbookingEventTicketPrototypeFindByIdAttributes) | **GET** /AwEventbookingEventTickets/{id}/attributes/{fk} | Find a related item by id for attributes.
*AwEventbookingEventTicketApi* | [**awEventbookingEventTicketPrototypeFindByIdAwEventbookingTicket**](docs/AwEventbookingEventTicketApi.md#awEventbookingEventTicketPrototypeFindByIdAwEventbookingTicket) | **GET** /AwEventbookingEventTickets/{id}/awEventbookingTicket/{fk} | Find a related item by id for awEventbookingTicket.
*AwEventbookingEventTicketApi* | [**awEventbookingEventTicketPrototypeFindByIdTickets**](docs/AwEventbookingEventTicketApi.md#awEventbookingEventTicketPrototypeFindByIdTickets) | **GET** /AwEventbookingEventTickets/{id}/tickets/{fk} | Find a related item by id for tickets.
*AwEventbookingEventTicketApi* | [**awEventbookingEventTicketPrototypeGetAttributes**](docs/AwEventbookingEventTicketApi.md#awEventbookingEventTicketPrototypeGetAttributes) | **GET** /AwEventbookingEventTickets/{id}/attributes | Queries attributes of AwEventbookingEventTicket.
*AwEventbookingEventTicketApi* | [**awEventbookingEventTicketPrototypeGetAwEventbookingTicket**](docs/AwEventbookingEventTicketApi.md#awEventbookingEventTicketPrototypeGetAwEventbookingTicket) | **GET** /AwEventbookingEventTickets/{id}/awEventbookingTicket | Queries awEventbookingTicket of AwEventbookingEventTicket.
*AwEventbookingEventTicketApi* | [**awEventbookingEventTicketPrototypeGetTickets**](docs/AwEventbookingEventTicketApi.md#awEventbookingEventTicketPrototypeGetTickets) | **GET** /AwEventbookingEventTickets/{id}/tickets | Queries tickets of AwEventbookingEventTicket.
*AwEventbookingEventTicketApi* | [**awEventbookingEventTicketPrototypeUpdateAttributesPatchAwEventbookingEventTicketsid**](docs/AwEventbookingEventTicketApi.md#awEventbookingEventTicketPrototypeUpdateAttributesPatchAwEventbookingEventTicketsid) | **PATCH** /AwEventbookingEventTickets/{id} | Patch attributes for a model instance and persist it into the data source.
*AwEventbookingEventTicketApi* | [**awEventbookingEventTicketPrototypeUpdateAttributesPutAwEventbookingEventTicketsid**](docs/AwEventbookingEventTicketApi.md#awEventbookingEventTicketPrototypeUpdateAttributesPutAwEventbookingEventTicketsid) | **PUT** /AwEventbookingEventTickets/{id} | Patch attributes for a model instance and persist it into the data source.
*AwEventbookingEventTicketApi* | [**awEventbookingEventTicketPrototypeUpdateByIdAttributes**](docs/AwEventbookingEventTicketApi.md#awEventbookingEventTicketPrototypeUpdateByIdAttributes) | **PUT** /AwEventbookingEventTickets/{id}/attributes/{fk} | Update a related item by id for attributes.
*AwEventbookingEventTicketApi* | [**awEventbookingEventTicketPrototypeUpdateByIdAwEventbookingTicket**](docs/AwEventbookingEventTicketApi.md#awEventbookingEventTicketPrototypeUpdateByIdAwEventbookingTicket) | **PUT** /AwEventbookingEventTickets/{id}/awEventbookingTicket/{fk} | Update a related item by id for awEventbookingTicket.
*AwEventbookingEventTicketApi* | [**awEventbookingEventTicketPrototypeUpdateByIdTickets**](docs/AwEventbookingEventTicketApi.md#awEventbookingEventTicketPrototypeUpdateByIdTickets) | **PUT** /AwEventbookingEventTickets/{id}/tickets/{fk} | Update a related item by id for tickets.
*AwEventbookingEventTicketApi* | [**awEventbookingEventTicketReplaceById**](docs/AwEventbookingEventTicketApi.md#awEventbookingEventTicketReplaceById) | **POST** /AwEventbookingEventTickets/{id}/replace | Replace attributes for a model instance and persist it into the data source.
*AwEventbookingEventTicketApi* | [**awEventbookingEventTicketReplaceOrCreate**](docs/AwEventbookingEventTicketApi.md#awEventbookingEventTicketReplaceOrCreate) | **POST** /AwEventbookingEventTickets/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
*AwEventbookingEventTicketApi* | [**awEventbookingEventTicketUpdateAll**](docs/AwEventbookingEventTicketApi.md#awEventbookingEventTicketUpdateAll) | **POST** /AwEventbookingEventTickets/update | Update instances of the model matched by {{where}} from the data source.
*AwEventbookingEventTicketApi* | [**awEventbookingEventTicketUpsertPatchAwEventbookingEventTickets**](docs/AwEventbookingEventTicketApi.md#awEventbookingEventTicketUpsertPatchAwEventbookingEventTickets) | **PATCH** /AwEventbookingEventTickets | Patch an existing model instance or insert a new one into the data source.
*AwEventbookingEventTicketApi* | [**awEventbookingEventTicketUpsertPutAwEventbookingEventTickets**](docs/AwEventbookingEventTicketApi.md#awEventbookingEventTicketUpsertPutAwEventbookingEventTickets) | **PUT** /AwEventbookingEventTickets | Patch an existing model instance or insert a new one into the data source.
*AwEventbookingEventTicketApi* | [**awEventbookingEventTicketUpsertWithWhere**](docs/AwEventbookingEventTicketApi.md#awEventbookingEventTicketUpsertWithWhere) | **POST** /AwEventbookingEventTickets/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.
*AwEventbookingOrderHistoryApi* | [**awEventbookingOrderHistoryCount**](docs/AwEventbookingOrderHistoryApi.md#awEventbookingOrderHistoryCount) | **GET** /AwEventbookingOrderHistories/count | Count instances of the model matched by where from the data source.
*AwEventbookingOrderHistoryApi* | [**awEventbookingOrderHistoryCreate**](docs/AwEventbookingOrderHistoryApi.md#awEventbookingOrderHistoryCreate) | **POST** /AwEventbookingOrderHistories | Create a new instance of the model and persist it into the data source.
*AwEventbookingOrderHistoryApi* | [**awEventbookingOrderHistoryCreateChangeStreamGetAwEventbookingOrderHistoriesChangeStream**](docs/AwEventbookingOrderHistoryApi.md#awEventbookingOrderHistoryCreateChangeStreamGetAwEventbookingOrderHistoriesChangeStream) | **GET** /AwEventbookingOrderHistories/change-stream | Create a change stream.
*AwEventbookingOrderHistoryApi* | [**awEventbookingOrderHistoryCreateChangeStreamPostAwEventbookingOrderHistoriesChangeStream**](docs/AwEventbookingOrderHistoryApi.md#awEventbookingOrderHistoryCreateChangeStreamPostAwEventbookingOrderHistoriesChangeStream) | **POST** /AwEventbookingOrderHistories/change-stream | Create a change stream.
*AwEventbookingOrderHistoryApi* | [**awEventbookingOrderHistoryDeleteById**](docs/AwEventbookingOrderHistoryApi.md#awEventbookingOrderHistoryDeleteById) | **DELETE** /AwEventbookingOrderHistories/{id} | Delete a model instance by {{id}} from the data source.
*AwEventbookingOrderHistoryApi* | [**awEventbookingOrderHistoryExistsGetAwEventbookingOrderHistoriesidExists**](docs/AwEventbookingOrderHistoryApi.md#awEventbookingOrderHistoryExistsGetAwEventbookingOrderHistoriesidExists) | **GET** /AwEventbookingOrderHistories/{id}/exists | Check whether a model instance exists in the data source.
*AwEventbookingOrderHistoryApi* | [**awEventbookingOrderHistoryExistsHeadAwEventbookingOrderHistoriesid**](docs/AwEventbookingOrderHistoryApi.md#awEventbookingOrderHistoryExistsHeadAwEventbookingOrderHistoriesid) | **HEAD** /AwEventbookingOrderHistories/{id} | Check whether a model instance exists in the data source.
*AwEventbookingOrderHistoryApi* | [**awEventbookingOrderHistoryFind**](docs/AwEventbookingOrderHistoryApi.md#awEventbookingOrderHistoryFind) | **GET** /AwEventbookingOrderHistories | Find all instances of the model matched by filter from the data source.
*AwEventbookingOrderHistoryApi* | [**awEventbookingOrderHistoryFindById**](docs/AwEventbookingOrderHistoryApi.md#awEventbookingOrderHistoryFindById) | **GET** /AwEventbookingOrderHistories/{id} | Find a model instance by {{id}} from the data source.
*AwEventbookingOrderHistoryApi* | [**awEventbookingOrderHistoryFindOne**](docs/AwEventbookingOrderHistoryApi.md#awEventbookingOrderHistoryFindOne) | **GET** /AwEventbookingOrderHistories/findOne | Find first instance of the model matched by filter from the data source.
*AwEventbookingOrderHistoryApi* | [**awEventbookingOrderHistoryPrototypeUpdateAttributesPatchAwEventbookingOrderHistoriesid**](docs/AwEventbookingOrderHistoryApi.md#awEventbookingOrderHistoryPrototypeUpdateAttributesPatchAwEventbookingOrderHistoriesid) | **PATCH** /AwEventbookingOrderHistories/{id} | Patch attributes for a model instance and persist it into the data source.
*AwEventbookingOrderHistoryApi* | [**awEventbookingOrderHistoryPrototypeUpdateAttributesPutAwEventbookingOrderHistoriesid**](docs/AwEventbookingOrderHistoryApi.md#awEventbookingOrderHistoryPrototypeUpdateAttributesPutAwEventbookingOrderHistoriesid) | **PUT** /AwEventbookingOrderHistories/{id} | Patch attributes for a model instance and persist it into the data source.
*AwEventbookingOrderHistoryApi* | [**awEventbookingOrderHistoryReplaceById**](docs/AwEventbookingOrderHistoryApi.md#awEventbookingOrderHistoryReplaceById) | **POST** /AwEventbookingOrderHistories/{id}/replace | Replace attributes for a model instance and persist it into the data source.
*AwEventbookingOrderHistoryApi* | [**awEventbookingOrderHistoryReplaceOrCreate**](docs/AwEventbookingOrderHistoryApi.md#awEventbookingOrderHistoryReplaceOrCreate) | **POST** /AwEventbookingOrderHistories/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
*AwEventbookingOrderHistoryApi* | [**awEventbookingOrderHistoryUpdateAll**](docs/AwEventbookingOrderHistoryApi.md#awEventbookingOrderHistoryUpdateAll) | **POST** /AwEventbookingOrderHistories/update | Update instances of the model matched by {{where}} from the data source.
*AwEventbookingOrderHistoryApi* | [**awEventbookingOrderHistoryUpsertPatchAwEventbookingOrderHistories**](docs/AwEventbookingOrderHistoryApi.md#awEventbookingOrderHistoryUpsertPatchAwEventbookingOrderHistories) | **PATCH** /AwEventbookingOrderHistories | Patch an existing model instance or insert a new one into the data source.
*AwEventbookingOrderHistoryApi* | [**awEventbookingOrderHistoryUpsertPutAwEventbookingOrderHistories**](docs/AwEventbookingOrderHistoryApi.md#awEventbookingOrderHistoryUpsertPutAwEventbookingOrderHistories) | **PUT** /AwEventbookingOrderHistories | Patch an existing model instance or insert a new one into the data source.
*AwEventbookingOrderHistoryApi* | [**awEventbookingOrderHistoryUpsertWithWhere**](docs/AwEventbookingOrderHistoryApi.md#awEventbookingOrderHistoryUpsertWithWhere) | **POST** /AwEventbookingOrderHistories/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.
*AwEventbookingTicketApi* | [**awEventbookingTicketCount**](docs/AwEventbookingTicketApi.md#awEventbookingTicketCount) | **GET** /AwEventbookingTickets/count | Count instances of the model matched by where from the data source.
*AwEventbookingTicketApi* | [**awEventbookingTicketCreate**](docs/AwEventbookingTicketApi.md#awEventbookingTicketCreate) | **POST** /AwEventbookingTickets | Create a new instance of the model and persist it into the data source.
*AwEventbookingTicketApi* | [**awEventbookingTicketCreateChangeStreamGetAwEventbookingTicketsChangeStream**](docs/AwEventbookingTicketApi.md#awEventbookingTicketCreateChangeStreamGetAwEventbookingTicketsChangeStream) | **GET** /AwEventbookingTickets/change-stream | Create a change stream.
*AwEventbookingTicketApi* | [**awEventbookingTicketCreateChangeStreamPostAwEventbookingTicketsChangeStream**](docs/AwEventbookingTicketApi.md#awEventbookingTicketCreateChangeStreamPostAwEventbookingTicketsChangeStream) | **POST** /AwEventbookingTickets/change-stream | Create a change stream.
*AwEventbookingTicketApi* | [**awEventbookingTicketDeleteById**](docs/AwEventbookingTicketApi.md#awEventbookingTicketDeleteById) | **DELETE** /AwEventbookingTickets/{id} | Delete a model instance by {{id}} from the data source.
*AwEventbookingTicketApi* | [**awEventbookingTicketExistsGetAwEventbookingTicketsidExists**](docs/AwEventbookingTicketApi.md#awEventbookingTicketExistsGetAwEventbookingTicketsidExists) | **GET** /AwEventbookingTickets/{id}/exists | Check whether a model instance exists in the data source.
*AwEventbookingTicketApi* | [**awEventbookingTicketExistsHeadAwEventbookingTicketsid**](docs/AwEventbookingTicketApi.md#awEventbookingTicketExistsHeadAwEventbookingTicketsid) | **HEAD** /AwEventbookingTickets/{id} | Check whether a model instance exists in the data source.
*AwEventbookingTicketApi* | [**awEventbookingTicketFind**](docs/AwEventbookingTicketApi.md#awEventbookingTicketFind) | **GET** /AwEventbookingTickets | Find all instances of the model matched by filter from the data source.
*AwEventbookingTicketApi* | [**awEventbookingTicketFindById**](docs/AwEventbookingTicketApi.md#awEventbookingTicketFindById) | **GET** /AwEventbookingTickets/{id} | Find a model instance by {{id}} from the data source.
*AwEventbookingTicketApi* | [**awEventbookingTicketFindOne**](docs/AwEventbookingTicketApi.md#awEventbookingTicketFindOne) | **GET** /AwEventbookingTickets/findOne | Find first instance of the model matched by filter from the data source.
*AwEventbookingTicketApi* | [**awEventbookingTicketPrototypeUpdateAttributesPatchAwEventbookingTicketsid**](docs/AwEventbookingTicketApi.md#awEventbookingTicketPrototypeUpdateAttributesPatchAwEventbookingTicketsid) | **PATCH** /AwEventbookingTickets/{id} | Patch attributes for a model instance and persist it into the data source.
*AwEventbookingTicketApi* | [**awEventbookingTicketPrototypeUpdateAttributesPutAwEventbookingTicketsid**](docs/AwEventbookingTicketApi.md#awEventbookingTicketPrototypeUpdateAttributesPutAwEventbookingTicketsid) | **PUT** /AwEventbookingTickets/{id} | Patch attributes for a model instance and persist it into the data source.
*AwEventbookingTicketApi* | [**awEventbookingTicketReplaceById**](docs/AwEventbookingTicketApi.md#awEventbookingTicketReplaceById) | **POST** /AwEventbookingTickets/{id}/replace | Replace attributes for a model instance and persist it into the data source.
*AwEventbookingTicketApi* | [**awEventbookingTicketReplaceOrCreate**](docs/AwEventbookingTicketApi.md#awEventbookingTicketReplaceOrCreate) | **POST** /AwEventbookingTickets/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
*AwEventbookingTicketApi* | [**awEventbookingTicketUpdateAll**](docs/AwEventbookingTicketApi.md#awEventbookingTicketUpdateAll) | **POST** /AwEventbookingTickets/update | Update instances of the model matched by {{where}} from the data source.
*AwEventbookingTicketApi* | [**awEventbookingTicketUpsertPatchAwEventbookingTickets**](docs/AwEventbookingTicketApi.md#awEventbookingTicketUpsertPatchAwEventbookingTickets) | **PATCH** /AwEventbookingTickets | Patch an existing model instance or insert a new one into the data source.
*AwEventbookingTicketApi* | [**awEventbookingTicketUpsertPutAwEventbookingTickets**](docs/AwEventbookingTicketApi.md#awEventbookingTicketUpsertPutAwEventbookingTickets) | **PUT** /AwEventbookingTickets | Patch an existing model instance or insert a new one into the data source.
*AwEventbookingTicketApi* | [**awEventbookingTicketUpsertWithWhere**](docs/AwEventbookingTicketApi.md#awEventbookingTicketUpsertWithWhere) | **POST** /AwEventbookingTickets/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.
*BrandApi* | [**brandAllFeeds**](docs/BrandApi.md#brandAllFeeds) | **GET** /Brands/feeds | 
*BrandApi* | [**brandCompetitions**](docs/BrandApi.md#brandCompetitions) | **GET** /Brands/{id}/competitions | 
*BrandApi* | [**brandCoupons**](docs/BrandApi.md#brandCoupons) | **GET** /Brands/{id}/coupons | 
*BrandApi* | [**brandDealGroups**](docs/BrandApi.md#brandDealGroups) | **GET** /Brands/{id}/dealGroups | 
*BrandApi* | [**brandDealPaids**](docs/BrandApi.md#brandDealPaids) | **GET** /Brands/{id}/dealPaids | 
*BrandApi* | [**brandDeals**](docs/BrandApi.md#brandDeals) | **GET** /Brands/{id}/deals | 
*BrandApi* | [**brandEvents**](docs/BrandApi.md#brandEvents) | **GET** /Brands/{id}/events | 
*BrandApi* | [**brandFeeds**](docs/BrandApi.md#brandFeeds) | **GET** /Brands/{id}/feeds | 
*BrandApi* | [**brandFind**](docs/BrandApi.md#brandFind) | **GET** /Brands | Find all instances of the model matched by filter from the data source.
*BrandApi* | [**brandFindById**](docs/BrandApi.md#brandFindById) | **GET** /Brands/{id} | Find a model instance by {{id}} from the data source.
*BrandApi* | [**brandFindOne**](docs/BrandApi.md#brandFindOne) | **GET** /Brands/findOne | Find first instance of the model matched by filter from the data source.
*BrandApi* | [**brandFollowers**](docs/BrandApi.md#brandFollowers) | **GET** /Brands/{id}/followers | 
*BrandApi* | [**brandFollowersCreate**](docs/BrandApi.md#brandFollowersCreate) | **PUT** /Brands/{id}/followers/me | 
*BrandApi* | [**brandFollowersDelete**](docs/BrandApi.md#brandFollowersDelete) | **DELETE** /Brands/{id}/followers/me | 
*BrandApi* | [**brandOffers**](docs/BrandApi.md#brandOffers) | **GET** /Brands/{id}/offers | 
*BrandApi* | [**brandReplaceById**](docs/BrandApi.md#brandReplaceById) | **POST** /Brands/{id}/replace | Replace attributes for a model instance and persist it into the data source.
*BrandApi* | [**brandReplaceOrCreate**](docs/BrandApi.md#brandReplaceOrCreate) | **POST** /Brands/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
*BrandApi* | [**brandUpsertWithWhere**](docs/BrandApi.md#brandUpsertWithWhere) | **POST** /Brands/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.
*BrandApi* | [**brandVenues**](docs/BrandApi.md#brandVenues) | **GET** /Brands/{id}/venues | 
*CatalogCategoryApi* | [**catalogCategoryCount**](docs/CatalogCategoryApi.md#catalogCategoryCount) | **GET** /CatalogCategories/count | Count instances of the model matched by where from the data source.
*CatalogCategoryApi* | [**catalogCategoryCreate**](docs/CatalogCategoryApi.md#catalogCategoryCreate) | **POST** /CatalogCategories | Create a new instance of the model and persist it into the data source.
*CatalogCategoryApi* | [**catalogCategoryCreateChangeStreamGetCatalogCategoriesChangeStream**](docs/CatalogCategoryApi.md#catalogCategoryCreateChangeStreamGetCatalogCategoriesChangeStream) | **GET** /CatalogCategories/change-stream | Create a change stream.
*CatalogCategoryApi* | [**catalogCategoryCreateChangeStreamPostCatalogCategoriesChangeStream**](docs/CatalogCategoryApi.md#catalogCategoryCreateChangeStreamPostCatalogCategoriesChangeStream) | **POST** /CatalogCategories/change-stream | Create a change stream.
*CatalogCategoryApi* | [**catalogCategoryDeleteById**](docs/CatalogCategoryApi.md#catalogCategoryDeleteById) | **DELETE** /CatalogCategories/{id} | Delete a model instance by {{id}} from the data source.
*CatalogCategoryApi* | [**catalogCategoryExistsGetCatalogCategoriesidExists**](docs/CatalogCategoryApi.md#catalogCategoryExistsGetCatalogCategoriesidExists) | **GET** /CatalogCategories/{id}/exists | Check whether a model instance exists in the data source.
*CatalogCategoryApi* | [**catalogCategoryExistsHeadCatalogCategoriesid**](docs/CatalogCategoryApi.md#catalogCategoryExistsHeadCatalogCategoriesid) | **HEAD** /CatalogCategories/{id} | Check whether a model instance exists in the data source.
*CatalogCategoryApi* | [**catalogCategoryFind**](docs/CatalogCategoryApi.md#catalogCategoryFind) | **GET** /CatalogCategories | Find all instances of the model matched by filter from the data source.
*CatalogCategoryApi* | [**catalogCategoryFindById**](docs/CatalogCategoryApi.md#catalogCategoryFindById) | **GET** /CatalogCategories/{id} | Find a model instance by {{id}} from the data source.
*CatalogCategoryApi* | [**catalogCategoryFindOne**](docs/CatalogCategoryApi.md#catalogCategoryFindOne) | **GET** /CatalogCategories/findOne | Find first instance of the model matched by filter from the data source.
*CatalogCategoryApi* | [**catalogCategoryPrototypeCountCatalogProducts**](docs/CatalogCategoryApi.md#catalogCategoryPrototypeCountCatalogProducts) | **GET** /CatalogCategories/{id}/catalogProducts/count | Counts catalogProducts of CatalogCategory.
*CatalogCategoryApi* | [**catalogCategoryPrototypeCreateCatalogProducts**](docs/CatalogCategoryApi.md#catalogCategoryPrototypeCreateCatalogProducts) | **POST** /CatalogCategories/{id}/catalogProducts | Creates a new instance in catalogProducts of this model.
*CatalogCategoryApi* | [**catalogCategoryPrototypeDeleteCatalogProducts**](docs/CatalogCategoryApi.md#catalogCategoryPrototypeDeleteCatalogProducts) | **DELETE** /CatalogCategories/{id}/catalogProducts | Deletes all catalogProducts of this model.
*CatalogCategoryApi* | [**catalogCategoryPrototypeDestroyByIdCatalogProducts**](docs/CatalogCategoryApi.md#catalogCategoryPrototypeDestroyByIdCatalogProducts) | **DELETE** /CatalogCategories/{id}/catalogProducts/{fk} | Delete a related item by id for catalogProducts.
*CatalogCategoryApi* | [**catalogCategoryPrototypeExistsCatalogProducts**](docs/CatalogCategoryApi.md#catalogCategoryPrototypeExistsCatalogProducts) | **HEAD** /CatalogCategories/{id}/catalogProducts/rel/{fk} | Check the existence of catalogProducts relation to an item by id.
*CatalogCategoryApi* | [**catalogCategoryPrototypeFindByIdCatalogProducts**](docs/CatalogCategoryApi.md#catalogCategoryPrototypeFindByIdCatalogProducts) | **GET** /CatalogCategories/{id}/catalogProducts/{fk} | Find a related item by id for catalogProducts.
*CatalogCategoryApi* | [**catalogCategoryPrototypeGetCatalogProducts**](docs/CatalogCategoryApi.md#catalogCategoryPrototypeGetCatalogProducts) | **GET** /CatalogCategories/{id}/catalogProducts | Queries catalogProducts of CatalogCategory.
*CatalogCategoryApi* | [**catalogCategoryPrototypeLinkCatalogProducts**](docs/CatalogCategoryApi.md#catalogCategoryPrototypeLinkCatalogProducts) | **PUT** /CatalogCategories/{id}/catalogProducts/rel/{fk} | Add a related item by id for catalogProducts.
*CatalogCategoryApi* | [**catalogCategoryPrototypeUnlinkCatalogProducts**](docs/CatalogCategoryApi.md#catalogCategoryPrototypeUnlinkCatalogProducts) | **DELETE** /CatalogCategories/{id}/catalogProducts/rel/{fk} | Remove the catalogProducts relation to an item by id.
*CatalogCategoryApi* | [**catalogCategoryPrototypeUpdateAttributesPatchCatalogCategoriesid**](docs/CatalogCategoryApi.md#catalogCategoryPrototypeUpdateAttributesPatchCatalogCategoriesid) | **PATCH** /CatalogCategories/{id} | Patch attributes for a model instance and persist it into the data source.
*CatalogCategoryApi* | [**catalogCategoryPrototypeUpdateAttributesPutCatalogCategoriesid**](docs/CatalogCategoryApi.md#catalogCategoryPrototypeUpdateAttributesPutCatalogCategoriesid) | **PUT** /CatalogCategories/{id} | Patch attributes for a model instance and persist it into the data source.
*CatalogCategoryApi* | [**catalogCategoryPrototypeUpdateByIdCatalogProducts**](docs/CatalogCategoryApi.md#catalogCategoryPrototypeUpdateByIdCatalogProducts) | **PUT** /CatalogCategories/{id}/catalogProducts/{fk} | Update a related item by id for catalogProducts.
*CatalogCategoryApi* | [**catalogCategoryReplaceById**](docs/CatalogCategoryApi.md#catalogCategoryReplaceById) | **POST** /CatalogCategories/{id}/replace | Replace attributes for a model instance and persist it into the data source.
*CatalogCategoryApi* | [**catalogCategoryReplaceOrCreate**](docs/CatalogCategoryApi.md#catalogCategoryReplaceOrCreate) | **POST** /CatalogCategories/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
*CatalogCategoryApi* | [**catalogCategoryUpdateAll**](docs/CatalogCategoryApi.md#catalogCategoryUpdateAll) | **POST** /CatalogCategories/update | Update instances of the model matched by {{where}} from the data source.
*CatalogCategoryApi* | [**catalogCategoryUpsertPatchCatalogCategories**](docs/CatalogCategoryApi.md#catalogCategoryUpsertPatchCatalogCategories) | **PATCH** /CatalogCategories | Patch an existing model instance or insert a new one into the data source.
*CatalogCategoryApi* | [**catalogCategoryUpsertPutCatalogCategories**](docs/CatalogCategoryApi.md#catalogCategoryUpsertPutCatalogCategories) | **PUT** /CatalogCategories | Patch an existing model instance or insert a new one into the data source.
*CatalogCategoryApi* | [**catalogCategoryUpsertWithWhere**](docs/CatalogCategoryApi.md#catalogCategoryUpsertWithWhere) | **POST** /CatalogCategories/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.
*CatalogProductApi* | [**catalogProductCount**](docs/CatalogProductApi.md#catalogProductCount) | **GET** /CatalogProducts/count | Count instances of the model matched by where from the data source.
*CatalogProductApi* | [**catalogProductCreate**](docs/CatalogProductApi.md#catalogProductCreate) | **POST** /CatalogProducts | Create a new instance of the model and persist it into the data source.
*CatalogProductApi* | [**catalogProductCreateChangeStreamGetCatalogProductsChangeStream**](docs/CatalogProductApi.md#catalogProductCreateChangeStreamGetCatalogProductsChangeStream) | **GET** /CatalogProducts/change-stream | Create a change stream.
*CatalogProductApi* | [**catalogProductCreateChangeStreamPostCatalogProductsChangeStream**](docs/CatalogProductApi.md#catalogProductCreateChangeStreamPostCatalogProductsChangeStream) | **POST** /CatalogProducts/change-stream | Create a change stream.
*CatalogProductApi* | [**catalogProductDeleteById**](docs/CatalogProductApi.md#catalogProductDeleteById) | **DELETE** /CatalogProducts/{id} | Delete a model instance by {{id}} from the data source.
*CatalogProductApi* | [**catalogProductExistsGetCatalogProductsidExists**](docs/CatalogProductApi.md#catalogProductExistsGetCatalogProductsidExists) | **GET** /CatalogProducts/{id}/exists | Check whether a model instance exists in the data source.
*CatalogProductApi* | [**catalogProductExistsHeadCatalogProductsid**](docs/CatalogProductApi.md#catalogProductExistsHeadCatalogProductsid) | **HEAD** /CatalogProducts/{id} | Check whether a model instance exists in the data source.
*CatalogProductApi* | [**catalogProductFind**](docs/CatalogProductApi.md#catalogProductFind) | **GET** /CatalogProducts | Find all instances of the model matched by filter from the data source.
*CatalogProductApi* | [**catalogProductFindById**](docs/CatalogProductApi.md#catalogProductFindById) | **GET** /CatalogProducts/{id} | Find a model instance by {{id}} from the data source.
*CatalogProductApi* | [**catalogProductFindOne**](docs/CatalogProductApi.md#catalogProductFindOne) | **GET** /CatalogProducts/findOne | Find first instance of the model matched by filter from the data source.
*CatalogProductApi* | [**catalogProductPrototypeCountCatalogCategories**](docs/CatalogProductApi.md#catalogProductPrototypeCountCatalogCategories) | **GET** /CatalogProducts/{id}/catalogCategories/count | Counts catalogCategories of CatalogProduct.
*CatalogProductApi* | [**catalogProductPrototypeCountUdropshipVendorProducts**](docs/CatalogProductApi.md#catalogProductPrototypeCountUdropshipVendorProducts) | **GET** /CatalogProducts/{id}/udropshipVendorProducts/count | Counts udropshipVendorProducts of CatalogProduct.
*CatalogProductApi* | [**catalogProductPrototypeCountUdropshipVendors**](docs/CatalogProductApi.md#catalogProductPrototypeCountUdropshipVendors) | **GET** /CatalogProducts/{id}/udropshipVendors/count | Counts udropshipVendors of CatalogProduct.
*CatalogProductApi* | [**catalogProductPrototypeCreateAwCollpurDeal**](docs/CatalogProductApi.md#catalogProductPrototypeCreateAwCollpurDeal) | **POST** /CatalogProducts/{id}/awCollpurDeal | Creates a new instance in awCollpurDeal of this model.
*CatalogProductApi* | [**catalogProductPrototypeCreateCatalogCategories**](docs/CatalogProductApi.md#catalogProductPrototypeCreateCatalogCategories) | **POST** /CatalogProducts/{id}/catalogCategories | Creates a new instance in catalogCategories of this model.
*CatalogProductApi* | [**catalogProductPrototypeCreateInventory**](docs/CatalogProductApi.md#catalogProductPrototypeCreateInventory) | **POST** /CatalogProducts/{id}/inventory | Creates a new instance in inventory of this model.
*CatalogProductApi* | [**catalogProductPrototypeCreateProductEventCompetition**](docs/CatalogProductApi.md#catalogProductPrototypeCreateProductEventCompetition) | **POST** /CatalogProducts/{id}/productEventCompetition | Creates a new instance in productEventCompetition of this model.
*CatalogProductApi* | [**catalogProductPrototypeCreateUdropshipVendorProducts**](docs/CatalogProductApi.md#catalogProductPrototypeCreateUdropshipVendorProducts) | **POST** /CatalogProducts/{id}/udropshipVendorProducts | Creates a new instance in udropshipVendorProducts of this model.
*CatalogProductApi* | [**catalogProductPrototypeCreateUdropshipVendors**](docs/CatalogProductApi.md#catalogProductPrototypeCreateUdropshipVendors) | **POST** /CatalogProducts/{id}/udropshipVendors | Creates a new instance in udropshipVendors of this model.
*CatalogProductApi* | [**catalogProductPrototypeDeleteCatalogCategories**](docs/CatalogProductApi.md#catalogProductPrototypeDeleteCatalogCategories) | **DELETE** /CatalogProducts/{id}/catalogCategories | Deletes all catalogCategories of this model.
*CatalogProductApi* | [**catalogProductPrototypeDeleteUdropshipVendorProducts**](docs/CatalogProductApi.md#catalogProductPrototypeDeleteUdropshipVendorProducts) | **DELETE** /CatalogProducts/{id}/udropshipVendorProducts | Deletes all udropshipVendorProducts of this model.
*CatalogProductApi* | [**catalogProductPrototypeDeleteUdropshipVendors**](docs/CatalogProductApi.md#catalogProductPrototypeDeleteUdropshipVendors) | **DELETE** /CatalogProducts/{id}/udropshipVendors | Deletes all udropshipVendors of this model.
*CatalogProductApi* | [**catalogProductPrototypeDestroyAwCollpurDeal**](docs/CatalogProductApi.md#catalogProductPrototypeDestroyAwCollpurDeal) | **DELETE** /CatalogProducts/{id}/awCollpurDeal | Deletes awCollpurDeal of this model.
*CatalogProductApi* | [**catalogProductPrototypeDestroyByIdCatalogCategories**](docs/CatalogProductApi.md#catalogProductPrototypeDestroyByIdCatalogCategories) | **DELETE** /CatalogProducts/{id}/catalogCategories/{fk} | Delete a related item by id for catalogCategories.
*CatalogProductApi* | [**catalogProductPrototypeDestroyByIdUdropshipVendorProducts**](docs/CatalogProductApi.md#catalogProductPrototypeDestroyByIdUdropshipVendorProducts) | **DELETE** /CatalogProducts/{id}/udropshipVendorProducts/{fk} | Delete a related item by id for udropshipVendorProducts.
*CatalogProductApi* | [**catalogProductPrototypeDestroyByIdUdropshipVendors**](docs/CatalogProductApi.md#catalogProductPrototypeDestroyByIdUdropshipVendors) | **DELETE** /CatalogProducts/{id}/udropshipVendors/{fk} | Delete a related item by id for udropshipVendors.
*CatalogProductApi* | [**catalogProductPrototypeDestroyInventory**](docs/CatalogProductApi.md#catalogProductPrototypeDestroyInventory) | **DELETE** /CatalogProducts/{id}/inventory | Deletes inventory of this model.
*CatalogProductApi* | [**catalogProductPrototypeDestroyProductEventCompetition**](docs/CatalogProductApi.md#catalogProductPrototypeDestroyProductEventCompetition) | **DELETE** /CatalogProducts/{id}/productEventCompetition | Deletes productEventCompetition of this model.
*CatalogProductApi* | [**catalogProductPrototypeExistsCatalogCategories**](docs/CatalogProductApi.md#catalogProductPrototypeExistsCatalogCategories) | **HEAD** /CatalogProducts/{id}/catalogCategories/rel/{fk} | Check the existence of catalogCategories relation to an item by id.
*CatalogProductApi* | [**catalogProductPrototypeExistsUdropshipVendors**](docs/CatalogProductApi.md#catalogProductPrototypeExistsUdropshipVendors) | **HEAD** /CatalogProducts/{id}/udropshipVendors/rel/{fk} | Check the existence of udropshipVendors relation to an item by id.
*CatalogProductApi* | [**catalogProductPrototypeFindByIdCatalogCategories**](docs/CatalogProductApi.md#catalogProductPrototypeFindByIdCatalogCategories) | **GET** /CatalogProducts/{id}/catalogCategories/{fk} | Find a related item by id for catalogCategories.
*CatalogProductApi* | [**catalogProductPrototypeFindByIdUdropshipVendorProducts**](docs/CatalogProductApi.md#catalogProductPrototypeFindByIdUdropshipVendorProducts) | **GET** /CatalogProducts/{id}/udropshipVendorProducts/{fk} | Find a related item by id for udropshipVendorProducts.
*CatalogProductApi* | [**catalogProductPrototypeFindByIdUdropshipVendors**](docs/CatalogProductApi.md#catalogProductPrototypeFindByIdUdropshipVendors) | **GET** /CatalogProducts/{id}/udropshipVendors/{fk} | Find a related item by id for udropshipVendors.
*CatalogProductApi* | [**catalogProductPrototypeGetAwCollpurDeal**](docs/CatalogProductApi.md#catalogProductPrototypeGetAwCollpurDeal) | **GET** /CatalogProducts/{id}/awCollpurDeal | Fetches hasOne relation awCollpurDeal.
*CatalogProductApi* | [**catalogProductPrototypeGetCatalogCategories**](docs/CatalogProductApi.md#catalogProductPrototypeGetCatalogCategories) | **GET** /CatalogProducts/{id}/catalogCategories | Queries catalogCategories of CatalogProduct.
*CatalogProductApi* | [**catalogProductPrototypeGetCheckoutAgreement**](docs/CatalogProductApi.md#catalogProductPrototypeGetCheckoutAgreement) | **GET** /CatalogProducts/{id}/checkoutAgreement | Fetches belongsTo relation checkoutAgreement.
*CatalogProductApi* | [**catalogProductPrototypeGetCreator**](docs/CatalogProductApi.md#catalogProductPrototypeGetCreator) | **GET** /CatalogProducts/{id}/creator | Fetches belongsTo relation creator.
*CatalogProductApi* | [**catalogProductPrototypeGetInventory**](docs/CatalogProductApi.md#catalogProductPrototypeGetInventory) | **GET** /CatalogProducts/{id}/inventory | Fetches hasOne relation inventory.
*CatalogProductApi* | [**catalogProductPrototypeGetProductEventCompetition**](docs/CatalogProductApi.md#catalogProductPrototypeGetProductEventCompetition) | **GET** /CatalogProducts/{id}/productEventCompetition | Fetches hasOne relation productEventCompetition.
*CatalogProductApi* | [**catalogProductPrototypeGetUdropshipVendorProducts**](docs/CatalogProductApi.md#catalogProductPrototypeGetUdropshipVendorProducts) | **GET** /CatalogProducts/{id}/udropshipVendorProducts | Queries udropshipVendorProducts of CatalogProduct.
*CatalogProductApi* | [**catalogProductPrototypeGetUdropshipVendors**](docs/CatalogProductApi.md#catalogProductPrototypeGetUdropshipVendors) | **GET** /CatalogProducts/{id}/udropshipVendors | Queries udropshipVendors of CatalogProduct.
*CatalogProductApi* | [**catalogProductPrototypeLinkCatalogCategories**](docs/CatalogProductApi.md#catalogProductPrototypeLinkCatalogCategories) | **PUT** /CatalogProducts/{id}/catalogCategories/rel/{fk} | Add a related item by id for catalogCategories.
*CatalogProductApi* | [**catalogProductPrototypeLinkUdropshipVendors**](docs/CatalogProductApi.md#catalogProductPrototypeLinkUdropshipVendors) | **PUT** /CatalogProducts/{id}/udropshipVendors/rel/{fk} | Add a related item by id for udropshipVendors.
*CatalogProductApi* | [**catalogProductPrototypeUnlinkCatalogCategories**](docs/CatalogProductApi.md#catalogProductPrototypeUnlinkCatalogCategories) | **DELETE** /CatalogProducts/{id}/catalogCategories/rel/{fk} | Remove the catalogCategories relation to an item by id.
*CatalogProductApi* | [**catalogProductPrototypeUnlinkUdropshipVendors**](docs/CatalogProductApi.md#catalogProductPrototypeUnlinkUdropshipVendors) | **DELETE** /CatalogProducts/{id}/udropshipVendors/rel/{fk} | Remove the udropshipVendors relation to an item by id.
*CatalogProductApi* | [**catalogProductPrototypeUpdateAttributesPatchCatalogProductsid**](docs/CatalogProductApi.md#catalogProductPrototypeUpdateAttributesPatchCatalogProductsid) | **PATCH** /CatalogProducts/{id} | Patch attributes for a model instance and persist it into the data source.
*CatalogProductApi* | [**catalogProductPrototypeUpdateAttributesPutCatalogProductsid**](docs/CatalogProductApi.md#catalogProductPrototypeUpdateAttributesPutCatalogProductsid) | **PUT** /CatalogProducts/{id} | Patch attributes for a model instance and persist it into the data source.
*CatalogProductApi* | [**catalogProductPrototypeUpdateAwCollpurDeal**](docs/CatalogProductApi.md#catalogProductPrototypeUpdateAwCollpurDeal) | **PUT** /CatalogProducts/{id}/awCollpurDeal | Update awCollpurDeal of this model.
*CatalogProductApi* | [**catalogProductPrototypeUpdateByIdCatalogCategories**](docs/CatalogProductApi.md#catalogProductPrototypeUpdateByIdCatalogCategories) | **PUT** /CatalogProducts/{id}/catalogCategories/{fk} | Update a related item by id for catalogCategories.
*CatalogProductApi* | [**catalogProductPrototypeUpdateByIdUdropshipVendorProducts**](docs/CatalogProductApi.md#catalogProductPrototypeUpdateByIdUdropshipVendorProducts) | **PUT** /CatalogProducts/{id}/udropshipVendorProducts/{fk} | Update a related item by id for udropshipVendorProducts.
*CatalogProductApi* | [**catalogProductPrototypeUpdateByIdUdropshipVendors**](docs/CatalogProductApi.md#catalogProductPrototypeUpdateByIdUdropshipVendors) | **PUT** /CatalogProducts/{id}/udropshipVendors/{fk} | Update a related item by id for udropshipVendors.
*CatalogProductApi* | [**catalogProductPrototypeUpdateInventory**](docs/CatalogProductApi.md#catalogProductPrototypeUpdateInventory) | **PUT** /CatalogProducts/{id}/inventory | Update inventory of this model.
*CatalogProductApi* | [**catalogProductPrototypeUpdateProductEventCompetition**](docs/CatalogProductApi.md#catalogProductPrototypeUpdateProductEventCompetition) | **PUT** /CatalogProducts/{id}/productEventCompetition | Update productEventCompetition of this model.
*CatalogProductApi* | [**catalogProductReplaceById**](docs/CatalogProductApi.md#catalogProductReplaceById) | **POST** /CatalogProducts/{id}/replace | Replace attributes for a model instance and persist it into the data source.
*CatalogProductApi* | [**catalogProductReplaceOrCreate**](docs/CatalogProductApi.md#catalogProductReplaceOrCreate) | **POST** /CatalogProducts/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
*CatalogProductApi* | [**catalogProductUpdateAll**](docs/CatalogProductApi.md#catalogProductUpdateAll) | **POST** /CatalogProducts/update | Update instances of the model matched by {{where}} from the data source.
*CatalogProductApi* | [**catalogProductUpsertPatchCatalogProducts**](docs/CatalogProductApi.md#catalogProductUpsertPatchCatalogProducts) | **PATCH** /CatalogProducts | Patch an existing model instance or insert a new one into the data source.
*CatalogProductApi* | [**catalogProductUpsertPutCatalogProducts**](docs/CatalogProductApi.md#catalogProductUpsertPutCatalogProducts) | **PUT** /CatalogProducts | Patch an existing model instance or insert a new one into the data source.
*CatalogProductApi* | [**catalogProductUpsertWithWhere**](docs/CatalogProductApi.md#catalogProductUpsertWithWhere) | **POST** /CatalogProducts/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.
*CategoryApi* | [**categoryFindBrandCategories**](docs/CategoryApi.md#categoryFindBrandCategories) | **GET** /Categories/types/brand | 
*CategoryApi* | [**categoryFindCampaignTypeCategories**](docs/CategoryApi.md#categoryFindCampaignTypeCategories) | **GET** /Categories/types/campaignType | 
*CategoryApi* | [**categoryFindOfferCategories**](docs/CategoryApi.md#categoryFindOfferCategories) | **GET** /Categories/types/offer | 
*CategoryApi* | [**categoryReplaceById**](docs/CategoryApi.md#categoryReplaceById) | **POST** /Categories/{id}/replace | Replace attributes for a model instance and persist it into the data source.
*CategoryApi* | [**categoryReplaceOrCreate**](docs/CategoryApi.md#categoryReplaceOrCreate) | **POST** /Categories/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
*CategoryApi* | [**categoryUpsertWithWhere**](docs/CategoryApi.md#categoryUpsertWithWhere) | **POST** /Categories/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.
*CompetitionApi* | [**competitionAttendees**](docs/CompetitionApi.md#competitionAttendees) | **GET** /Competitions/{id}/attendees | 
*CompetitionApi* | [**competitionCheckout**](docs/CompetitionApi.md#competitionCheckout) | **POST** /Competitions/{id}/checkout | 
*CompetitionApi* | [**competitionFind**](docs/CompetitionApi.md#competitionFind) | **GET** /Competitions | Find all instances of the model matched by filter from the data source.
*CompetitionApi* | [**competitionFindById**](docs/CompetitionApi.md#competitionFindById) | **GET** /Competitions/{id} | Find a model instance by {{id}} from the data source.
*CompetitionApi* | [**competitionFindOne**](docs/CompetitionApi.md#competitionFindOne) | **GET** /Competitions/findOne | Find first instance of the model matched by filter from the data source.
*CompetitionApi* | [**competitionRedeem**](docs/CompetitionApi.md#competitionRedeem) | **POST** /Competitions/{id}/redeem | 
*CompetitionApi* | [**competitionReplaceById**](docs/CompetitionApi.md#competitionReplaceById) | **POST** /Competitions/{id}/replace | Replace attributes for a model instance and persist it into the data source.
*CompetitionApi* | [**competitionReplaceOrCreate**](docs/CompetitionApi.md#competitionReplaceOrCreate) | **POST** /Competitions/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
*CompetitionApi* | [**competitionUpsertWithWhere**](docs/CompetitionApi.md#competitionUpsertWithWhere) | **POST** /Competitions/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.
*CompetitionApi* | [**competitionValidateAnswer**](docs/CompetitionApi.md#competitionValidateAnswer) | **POST** /Competitions/{id}/validateAnswer | 
*CompetitionApi* | [**competitionVenues**](docs/CompetitionApi.md#competitionVenues) | **GET** /Competitions/{id}/venues | 
*CoreWebsiteApi* | [**coreWebsiteCount**](docs/CoreWebsiteApi.md#coreWebsiteCount) | **GET** /CoreWebsites/count | Count instances of the model matched by where from the data source.
*CoreWebsiteApi* | [**coreWebsiteCreate**](docs/CoreWebsiteApi.md#coreWebsiteCreate) | **POST** /CoreWebsites | Create a new instance of the model and persist it into the data source.
*CoreWebsiteApi* | [**coreWebsiteCreateChangeStreamGetCoreWebsitesChangeStream**](docs/CoreWebsiteApi.md#coreWebsiteCreateChangeStreamGetCoreWebsitesChangeStream) | **GET** /CoreWebsites/change-stream | Create a change stream.
*CoreWebsiteApi* | [**coreWebsiteCreateChangeStreamPostCoreWebsitesChangeStream**](docs/CoreWebsiteApi.md#coreWebsiteCreateChangeStreamPostCoreWebsitesChangeStream) | **POST** /CoreWebsites/change-stream | Create a change stream.
*CoreWebsiteApi* | [**coreWebsiteDeleteById**](docs/CoreWebsiteApi.md#coreWebsiteDeleteById) | **DELETE** /CoreWebsites/{id} | Delete a model instance by {{id}} from the data source.
*CoreWebsiteApi* | [**coreWebsiteExistsGetCoreWebsitesidExists**](docs/CoreWebsiteApi.md#coreWebsiteExistsGetCoreWebsitesidExists) | **GET** /CoreWebsites/{id}/exists | Check whether a model instance exists in the data source.
*CoreWebsiteApi* | [**coreWebsiteExistsHeadCoreWebsitesid**](docs/CoreWebsiteApi.md#coreWebsiteExistsHeadCoreWebsitesid) | **HEAD** /CoreWebsites/{id} | Check whether a model instance exists in the data source.
*CoreWebsiteApi* | [**coreWebsiteFind**](docs/CoreWebsiteApi.md#coreWebsiteFind) | **GET** /CoreWebsites | Find all instances of the model matched by filter from the data source.
*CoreWebsiteApi* | [**coreWebsiteFindById**](docs/CoreWebsiteApi.md#coreWebsiteFindById) | **GET** /CoreWebsites/{id} | Find a model instance by {{id}} from the data source.
*CoreWebsiteApi* | [**coreWebsiteFindOne**](docs/CoreWebsiteApi.md#coreWebsiteFindOne) | **GET** /CoreWebsites/findOne | Find first instance of the model matched by filter from the data source.
*CoreWebsiteApi* | [**coreWebsitePrototypeUpdateAttributesPatchCoreWebsitesid**](docs/CoreWebsiteApi.md#coreWebsitePrototypeUpdateAttributesPatchCoreWebsitesid) | **PATCH** /CoreWebsites/{id} | Patch attributes for a model instance and persist it into the data source.
*CoreWebsiteApi* | [**coreWebsitePrototypeUpdateAttributesPutCoreWebsitesid**](docs/CoreWebsiteApi.md#coreWebsitePrototypeUpdateAttributesPutCoreWebsitesid) | **PUT** /CoreWebsites/{id} | Patch attributes for a model instance and persist it into the data source.
*CoreWebsiteApi* | [**coreWebsiteReplaceById**](docs/CoreWebsiteApi.md#coreWebsiteReplaceById) | **POST** /CoreWebsites/{id}/replace | Replace attributes for a model instance and persist it into the data source.
*CoreWebsiteApi* | [**coreWebsiteReplaceOrCreate**](docs/CoreWebsiteApi.md#coreWebsiteReplaceOrCreate) | **POST** /CoreWebsites/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
*CoreWebsiteApi* | [**coreWebsiteUpdateAll**](docs/CoreWebsiteApi.md#coreWebsiteUpdateAll) | **POST** /CoreWebsites/update | Update instances of the model matched by {{where}} from the data source.
*CoreWebsiteApi* | [**coreWebsiteUpsertPatchCoreWebsites**](docs/CoreWebsiteApi.md#coreWebsiteUpsertPatchCoreWebsites) | **PATCH** /CoreWebsites | Patch an existing model instance or insert a new one into the data source.
*CoreWebsiteApi* | [**coreWebsiteUpsertPutCoreWebsites**](docs/CoreWebsiteApi.md#coreWebsiteUpsertPutCoreWebsites) | **PUT** /CoreWebsites | Patch an existing model instance or insert a new one into the data source.
*CoreWebsiteApi* | [**coreWebsiteUpsertWithWhere**](docs/CoreWebsiteApi.md#coreWebsiteUpsertWithWhere) | **POST** /CoreWebsites/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.
*CouponApi* | [**couponCheckout**](docs/CouponApi.md#couponCheckout) | **POST** /Coupons/{id}/checkout | 
*CouponApi* | [**couponFind**](docs/CouponApi.md#couponFind) | **GET** /Coupons | Find all instances of the model matched by filter from the data source.
*CouponApi* | [**couponFindById**](docs/CouponApi.md#couponFindById) | **GET** /Coupons/{id} | Find a model instance by {{id}} from the data source.
*CouponApi* | [**couponFindOne**](docs/CouponApi.md#couponFindOne) | **GET** /Coupons/findOne | Find first instance of the model matched by filter from the data source.
*CouponApi* | [**couponRedeem**](docs/CouponApi.md#couponRedeem) | **POST** /Coupons/{id}/redeem | 
*CouponApi* | [**couponReplaceById**](docs/CouponApi.md#couponReplaceById) | **POST** /Coupons/{id}/replace | Replace attributes for a model instance and persist it into the data source.
*CouponApi* | [**couponReplaceOrCreate**](docs/CouponApi.md#couponReplaceOrCreate) | **POST** /Coupons/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
*CouponApi* | [**couponUpsertWithWhere**](docs/CouponApi.md#couponUpsertWithWhere) | **POST** /Coupons/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.
*CouponApi* | [**couponVenues**](docs/CouponApi.md#couponVenues) | **GET** /Coupons/{id}/venues | 
*CustomerApi* | [**customerActivities**](docs/CustomerApi.md#customerActivities) | **GET** /Customers/{id}/activities | Get Customer activities
*CustomerApi* | [**customerBadge**](docs/CustomerApi.md#customerBadge) | **GET** /Customers/{id}/badge | Get Customer badges
*CustomerApi* | [**customerCardCreate**](docs/CustomerApi.md#customerCardCreate) | **POST** /Customers/{id}/cards | Create a Card to a Customer
*CustomerApi* | [**customerCardDelete**](docs/CustomerApi.md#customerCardDelete) | **DELETE** /Customers/{id}/cards/{fk} | Remove a Card from a Customer
*CustomerApi* | [**customerCardFindById**](docs/CustomerApi.md#customerCardFindById) | **GET** /Customers/{id}/cards/{fk} | Get a specific Customer Card
*CustomerApi* | [**customerCardUpdate**](docs/CustomerApi.md#customerCardUpdate) | **PUT** /Customers/{id}/cards/{fk} | Update a Customer Card
*CustomerApi* | [**customerCards**](docs/CustomerApi.md#customerCards) | **GET** /Customers/{id}/cards | Get Customer Cards
*CustomerApi* | [**customerCompetitionOrders**](docs/CustomerApi.md#customerCompetitionOrders) | **GET** /Customers/{id}/competitions/{fk}/purchases | 
*CustomerApi* | [**customerCouponOrders**](docs/CustomerApi.md#customerCouponOrders) | **GET** /Customers/{id}/coupons/{fk}/purchases | 
*CustomerApi* | [**customerCreate**](docs/CustomerApi.md#customerCreate) | **POST** /Customers | Create a new instance of the model and persist it into the data source.
*CustomerApi* | [**customerDealGroupOrders**](docs/CustomerApi.md#customerDealGroupOrders) | **GET** /Customers/{id}/dealGroups/{fk}/purchases | 
*CustomerApi* | [**customerDealPaidOrders**](docs/CustomerApi.md#customerDealPaidOrders) | **GET** /Customers/{id}/dealPaids/{fk}/purchases | 
*CustomerApi* | [**customerEventAttending**](docs/CustomerApi.md#customerEventAttending) | **GET** /Customers/{id}/events/attending | 
*CustomerApi* | [**customerEventOrders**](docs/CustomerApi.md#customerEventOrders) | **GET** /Customers/{id}/events/{fk}/purchases | 
*CustomerApi* | [**customerFind**](docs/CustomerApi.md#customerFind) | **GET** /Customers | Find all instances of the model matched by filter from the data source.
*CustomerApi* | [**customerFindById**](docs/CustomerApi.md#customerFindById) | **GET** /Customers/{id} | Find a model instance by {{id}} from the data source.
*CustomerApi* | [**customerFindOne**](docs/CustomerApi.md#customerFindOne) | **GET** /Customers/findOne | Find first instance of the model matched by filter from the data source.
*CustomerApi* | [**customerFollow**](docs/CustomerApi.md#customerFollow) | **PUT** /Customers/{id}/followers/{fk} | Set Customer to follow of another Customer
*CustomerApi* | [**customerFollowers**](docs/CustomerApi.md#customerFollowers) | **GET** /Customers/{id}/followers | Get Customer followers
*CustomerApi* | [**customerFollowings**](docs/CustomerApi.md#customerFollowings) | **GET** /Customers/{id}/followings | Get Customers following a Customer
*CustomerApi* | [**customerLogin**](docs/CustomerApi.md#customerLogin) | **POST** /Customers/login | Login a Customer and receive Authorization Code
*CustomerApi* | [**customerNotifications**](docs/CustomerApi.md#customerNotifications) | **GET** /Customers/{id}/notifications | Get Customer Notifications
*CustomerApi* | [**customerPasswordResetAlt**](docs/CustomerApi.md#customerPasswordResetAlt) | **POST** /Customers/password/reset | Reset Customer password from just their known email
*CustomerApi* | [**customerPasswordUpdate**](docs/CustomerApi.md#customerPasswordUpdate) | **PUT** /Customers/{id}/password | Update the Customer password knowing it&#39;s id and current password
*CustomerApi* | [**customerRefreshToken**](docs/CustomerApi.md#customerRefreshToken) | **POST** /Customers/token/refresh | Use a Customer Refresh Access Token to receive a new Access Token
*CustomerApi* | [**customerReplaceById**](docs/CustomerApi.md#customerReplaceById) | **POST** /Customers/{id}/replace | Replace attributes for a model instance and persist it into the data source.
*CustomerApi* | [**customerReplaceOrCreate**](docs/CustomerApi.md#customerReplaceOrCreate) | **POST** /Customers/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
*CustomerApi* | [**customerTokenFromAuthorizationCode**](docs/CustomerApi.md#customerTokenFromAuthorizationCode) | **POST** /Customers/token/request | Authorize a Customer and receive an Access Token
*CustomerApi* | [**customerUnfollow**](docs/CustomerApi.md#customerUnfollow) | **DELETE** /Customers/{id}/followers/{fk} | Remove Customer from following of another Customer
*CustomerApi* | [**customerUpdate**](docs/CustomerApi.md#customerUpdate) | **PUT** /Customers/{id} | Update the Customer details knowing it&#39;s id
*CustomerApi* | [**customerUpsertWithWhere**](docs/CustomerApi.md#customerUpsertWithWhere) | **POST** /Customers/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.
*CustomerApi* | [**customerVerify**](docs/CustomerApi.md#customerVerify) | **GET** /Customers/token/verify | Verify that the request is authenticated with a Customer Access Token
*CustomerApi* | [**customerWalletCompetition**](docs/CustomerApi.md#customerWalletCompetition) | **GET** /Customers/{id}/wallet/competitions | 
*CustomerApi* | [**customerWalletCoupon**](docs/CustomerApi.md#customerWalletCoupon) | **GET** /Customers/{id}/wallet/coupons | 
*CustomerApi* | [**customerWalletDeal**](docs/CustomerApi.md#customerWalletDeal) | **GET** /Customers/{id}/wallet/deals | 
*CustomerApi* | [**customerWalletDealGroup**](docs/CustomerApi.md#customerWalletDealGroup) | **GET** /Customers/{id}/wallet/dealgroups | 
*CustomerApi* | [**customerWalletDealPaid**](docs/CustomerApi.md#customerWalletDealPaid) | **GET** /Customers/{id}/wallet/dealpaids | 
*CustomerApi* | [**customerWalletEvent**](docs/CustomerApi.md#customerWalletEvent) | **GET** /Customers/{id}/wallet/events | 
*CustomerApi* | [**customerWalletOffer**](docs/CustomerApi.md#customerWalletOffer) | **GET** /Customers/{id}/wallet/offers | 
*DealApi* | [**dealFind**](docs/DealApi.md#dealFind) | **GET** /Deals | Find all instances of the model matched by filter from the data source.
*DealApi* | [**dealFindById**](docs/DealApi.md#dealFindById) | **GET** /Deals/{id} | Find a model instance by {{id}} from the data source.
*DealApi* | [**dealFindOne**](docs/DealApi.md#dealFindOne) | **GET** /Deals/findOne | Find first instance of the model matched by filter from the data source.
*DealApi* | [**dealReplaceById**](docs/DealApi.md#dealReplaceById) | **POST** /Deals/{id}/replace | Replace attributes for a model instance and persist it into the data source.
*DealApi* | [**dealReplaceOrCreate**](docs/DealApi.md#dealReplaceOrCreate) | **POST** /Deals/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
*DealApi* | [**dealUpsertWithWhere**](docs/DealApi.md#dealUpsertWithWhere) | **POST** /Deals/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.
*DealGroupApi* | [**dealGroupCheckout**](docs/DealGroupApi.md#dealGroupCheckout) | **POST** /DealGroups/{id}/checkout | 
*DealGroupApi* | [**dealGroupFind**](docs/DealGroupApi.md#dealGroupFind) | **GET** /DealGroups | Find all instances of the model matched by filter from the data source.
*DealGroupApi* | [**dealGroupFindById**](docs/DealGroupApi.md#dealGroupFindById) | **GET** /DealGroups/{id} | Find a model instance by {{id}} from the data source.
*DealGroupApi* | [**dealGroupFindOne**](docs/DealGroupApi.md#dealGroupFindOne) | **GET** /DealGroups/findOne | Find first instance of the model matched by filter from the data source.
*DealGroupApi* | [**dealGroupRedeem**](docs/DealGroupApi.md#dealGroupRedeem) | **POST** /DealGroups/{id}/redeem | 
*DealGroupApi* | [**dealGroupReplaceById**](docs/DealGroupApi.md#dealGroupReplaceById) | **POST** /DealGroups/{id}/replace | Replace attributes for a model instance and persist it into the data source.
*DealGroupApi* | [**dealGroupReplaceOrCreate**](docs/DealGroupApi.md#dealGroupReplaceOrCreate) | **POST** /DealGroups/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
*DealGroupApi* | [**dealGroupUpsertWithWhere**](docs/DealGroupApi.md#dealGroupUpsertWithWhere) | **POST** /DealGroups/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.
*DealGroupApi* | [**dealGroupVenues**](docs/DealGroupApi.md#dealGroupVenues) | **GET** /DealGroups/{id}/venues | 
*DealPaidApi* | [**dealPaidCheckout**](docs/DealPaidApi.md#dealPaidCheckout) | **POST** /DealPaids/{id}/checkout | 
*DealPaidApi* | [**dealPaidFind**](docs/DealPaidApi.md#dealPaidFind) | **GET** /DealPaids | Find all instances of the model matched by filter from the data source.
*DealPaidApi* | [**dealPaidFindById**](docs/DealPaidApi.md#dealPaidFindById) | **GET** /DealPaids/{id} | Find a model instance by {{id}} from the data source.
*DealPaidApi* | [**dealPaidFindOne**](docs/DealPaidApi.md#dealPaidFindOne) | **GET** /DealPaids/findOne | Find first instance of the model matched by filter from the data source.
*DealPaidApi* | [**dealPaidRedeem**](docs/DealPaidApi.md#dealPaidRedeem) | **POST** /DealPaids/{id}/redeem | 
*DealPaidApi* | [**dealPaidReplaceById**](docs/DealPaidApi.md#dealPaidReplaceById) | **POST** /DealPaids/{id}/replace | Replace attributes for a model instance and persist it into the data source.
*DealPaidApi* | [**dealPaidReplaceOrCreate**](docs/DealPaidApi.md#dealPaidReplaceOrCreate) | **POST** /DealPaids/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
*DealPaidApi* | [**dealPaidUpsertWithWhere**](docs/DealPaidApi.md#dealPaidUpsertWithWhere) | **POST** /DealPaids/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.
*DealPaidApi* | [**dealPaidVenues**](docs/DealPaidApi.md#dealPaidVenues) | **GET** /DealPaids/{id}/venues | 
*EventApi* | [**eventAttendees**](docs/EventApi.md#eventAttendees) | **GET** /Events/{id}/attendees | 
*EventApi* | [**eventCheckout**](docs/EventApi.md#eventCheckout) | **POST** /Events/{id}/checkout | 
*EventApi* | [**eventFind**](docs/EventApi.md#eventFind) | **GET** /Events | Find all instances of the model matched by filter from the data source.
*EventApi* | [**eventFindById**](docs/EventApi.md#eventFindById) | **GET** /Events/{id} | Find a model instance by {{id}} from the data source.
*EventApi* | [**eventFindInLater**](docs/EventApi.md#eventFindInLater) | **GET** /Events/later | 
*EventApi* | [**eventFindInThisMonth**](docs/EventApi.md#eventFindInThisMonth) | **GET** /Events/thisMonth | 
*EventApi* | [**eventFindOne**](docs/EventApi.md#eventFindOne) | **GET** /Events/findOne | Find first instance of the model matched by filter from the data source.
*EventApi* | [**eventRedeem**](docs/EventApi.md#eventRedeem) | **POST** /Events/{id}/redeem | 
*EventApi* | [**eventReplaceById**](docs/EventApi.md#eventReplaceById) | **POST** /Events/{id}/replace | Replace attributes for a model instance and persist it into the data source.
*EventApi* | [**eventReplaceOrCreate**](docs/EventApi.md#eventReplaceOrCreate) | **POST** /Events/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
*EventApi* | [**eventUpsertWithWhere**](docs/EventApi.md#eventUpsertWithWhere) | **POST** /Events/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.
*EventApi* | [**eventVenues**](docs/EventApi.md#eventVenues) | **GET** /Events/{id}/venues | 
*HarpoonHpublicApplicationpartnerApi* | [**harpoonHpublicApplicationpartnerCount**](docs/HarpoonHpublicApplicationpartnerApi.md#harpoonHpublicApplicationpartnerCount) | **GET** /HarpoonHpublicApplicationpartners/count | Count instances of the model matched by where from the data source.
*HarpoonHpublicApplicationpartnerApi* | [**harpoonHpublicApplicationpartnerCreate**](docs/HarpoonHpublicApplicationpartnerApi.md#harpoonHpublicApplicationpartnerCreate) | **POST** /HarpoonHpublicApplicationpartners | Create a new instance of the model and persist it into the data source.
*HarpoonHpublicApplicationpartnerApi* | [**harpoonHpublicApplicationpartnerCreateChangeStreamGetHarpoonHpublicApplicationpartnersChangeStream**](docs/HarpoonHpublicApplicationpartnerApi.md#harpoonHpublicApplicationpartnerCreateChangeStreamGetHarpoonHpublicApplicationpartnersChangeStream) | **GET** /HarpoonHpublicApplicationpartners/change-stream | Create a change stream.
*HarpoonHpublicApplicationpartnerApi* | [**harpoonHpublicApplicationpartnerCreateChangeStreamPostHarpoonHpublicApplicationpartnersChangeStream**](docs/HarpoonHpublicApplicationpartnerApi.md#harpoonHpublicApplicationpartnerCreateChangeStreamPostHarpoonHpublicApplicationpartnersChangeStream) | **POST** /HarpoonHpublicApplicationpartners/change-stream | Create a change stream.
*HarpoonHpublicApplicationpartnerApi* | [**harpoonHpublicApplicationpartnerDeleteById**](docs/HarpoonHpublicApplicationpartnerApi.md#harpoonHpublicApplicationpartnerDeleteById) | **DELETE** /HarpoonHpublicApplicationpartners/{id} | Delete a model instance by {{id}} from the data source.
*HarpoonHpublicApplicationpartnerApi* | [**harpoonHpublicApplicationpartnerExistsGetHarpoonHpublicApplicationpartnersidExists**](docs/HarpoonHpublicApplicationpartnerApi.md#harpoonHpublicApplicationpartnerExistsGetHarpoonHpublicApplicationpartnersidExists) | **GET** /HarpoonHpublicApplicationpartners/{id}/exists | Check whether a model instance exists in the data source.
*HarpoonHpublicApplicationpartnerApi* | [**harpoonHpublicApplicationpartnerExistsHeadHarpoonHpublicApplicationpartnersid**](docs/HarpoonHpublicApplicationpartnerApi.md#harpoonHpublicApplicationpartnerExistsHeadHarpoonHpublicApplicationpartnersid) | **HEAD** /HarpoonHpublicApplicationpartners/{id} | Check whether a model instance exists in the data source.
*HarpoonHpublicApplicationpartnerApi* | [**harpoonHpublicApplicationpartnerFind**](docs/HarpoonHpublicApplicationpartnerApi.md#harpoonHpublicApplicationpartnerFind) | **GET** /HarpoonHpublicApplicationpartners | Find all instances of the model matched by filter from the data source.
*HarpoonHpublicApplicationpartnerApi* | [**harpoonHpublicApplicationpartnerFindById**](docs/HarpoonHpublicApplicationpartnerApi.md#harpoonHpublicApplicationpartnerFindById) | **GET** /HarpoonHpublicApplicationpartners/{id} | Find a model instance by {{id}} from the data source.
*HarpoonHpublicApplicationpartnerApi* | [**harpoonHpublicApplicationpartnerFindOne**](docs/HarpoonHpublicApplicationpartnerApi.md#harpoonHpublicApplicationpartnerFindOne) | **GET** /HarpoonHpublicApplicationpartners/findOne | Find first instance of the model matched by filter from the data source.
*HarpoonHpublicApplicationpartnerApi* | [**harpoonHpublicApplicationpartnerPrototypeGetUdropshipVendor**](docs/HarpoonHpublicApplicationpartnerApi.md#harpoonHpublicApplicationpartnerPrototypeGetUdropshipVendor) | **GET** /HarpoonHpublicApplicationpartners/{id}/udropshipVendor | Fetches belongsTo relation udropshipVendor.
*HarpoonHpublicApplicationpartnerApi* | [**harpoonHpublicApplicationpartnerPrototypeUpdateAttributesPatchHarpoonHpublicApplicationpartnersid**](docs/HarpoonHpublicApplicationpartnerApi.md#harpoonHpublicApplicationpartnerPrototypeUpdateAttributesPatchHarpoonHpublicApplicationpartnersid) | **PATCH** /HarpoonHpublicApplicationpartners/{id} | Patch attributes for a model instance and persist it into the data source.
*HarpoonHpublicApplicationpartnerApi* | [**harpoonHpublicApplicationpartnerPrototypeUpdateAttributesPutHarpoonHpublicApplicationpartnersid**](docs/HarpoonHpublicApplicationpartnerApi.md#harpoonHpublicApplicationpartnerPrototypeUpdateAttributesPutHarpoonHpublicApplicationpartnersid) | **PUT** /HarpoonHpublicApplicationpartners/{id} | Patch attributes for a model instance and persist it into the data source.
*HarpoonHpublicApplicationpartnerApi* | [**harpoonHpublicApplicationpartnerReplaceById**](docs/HarpoonHpublicApplicationpartnerApi.md#harpoonHpublicApplicationpartnerReplaceById) | **POST** /HarpoonHpublicApplicationpartners/{id}/replace | Replace attributes for a model instance and persist it into the data source.
*HarpoonHpublicApplicationpartnerApi* | [**harpoonHpublicApplicationpartnerReplaceOrCreate**](docs/HarpoonHpublicApplicationpartnerApi.md#harpoonHpublicApplicationpartnerReplaceOrCreate) | **POST** /HarpoonHpublicApplicationpartners/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
*HarpoonHpublicApplicationpartnerApi* | [**harpoonHpublicApplicationpartnerUpdateAll**](docs/HarpoonHpublicApplicationpartnerApi.md#harpoonHpublicApplicationpartnerUpdateAll) | **POST** /HarpoonHpublicApplicationpartners/update | Update instances of the model matched by {{where}} from the data source.
*HarpoonHpublicApplicationpartnerApi* | [**harpoonHpublicApplicationpartnerUpsertPatchHarpoonHpublicApplicationpartners**](docs/HarpoonHpublicApplicationpartnerApi.md#harpoonHpublicApplicationpartnerUpsertPatchHarpoonHpublicApplicationpartners) | **PATCH** /HarpoonHpublicApplicationpartners | Patch an existing model instance or insert a new one into the data source.
*HarpoonHpublicApplicationpartnerApi* | [**harpoonHpublicApplicationpartnerUpsertPutHarpoonHpublicApplicationpartners**](docs/HarpoonHpublicApplicationpartnerApi.md#harpoonHpublicApplicationpartnerUpsertPutHarpoonHpublicApplicationpartners) | **PUT** /HarpoonHpublicApplicationpartners | Patch an existing model instance or insert a new one into the data source.
*HarpoonHpublicApplicationpartnerApi* | [**harpoonHpublicApplicationpartnerUpsertWithWhere**](docs/HarpoonHpublicApplicationpartnerApi.md#harpoonHpublicApplicationpartnerUpsertWithWhere) | **POST** /HarpoonHpublicApplicationpartners/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.
*OfferApi* | [**offerFind**](docs/OfferApi.md#offerFind) | **GET** /Offers | Find all instances of the model matched by filter from the data source.
*OfferApi* | [**offerFindById**](docs/OfferApi.md#offerFindById) | **GET** /Offers/{id} | Find a model instance by {{id}} from the data source.
*OfferApi* | [**offerReplaceById**](docs/OfferApi.md#offerReplaceById) | **POST** /Offers/{id}/replace | Replace attributes for a model instance and persist it into the data source.
*OfferApi* | [**offerReplaceOrCreate**](docs/OfferApi.md#offerReplaceOrCreate) | **POST** /Offers/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
*OfferApi* | [**offerUpsertWithWhere**](docs/OfferApi.md#offerUpsertWithWhere) | **POST** /Offers/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.
*PlayerSourceApi* | [**playerSourceCount**](docs/PlayerSourceApi.md#playerSourceCount) | **GET** /PlayerSources/count | Count instances of the model matched by where from the data source.
*PlayerSourceApi* | [**playerSourceCreate**](docs/PlayerSourceApi.md#playerSourceCreate) | **POST** /PlayerSources | Create a new instance of the model and persist it into the data source.
*PlayerSourceApi* | [**playerSourceCreateChangeStreamGetPlayerSourcesChangeStream**](docs/PlayerSourceApi.md#playerSourceCreateChangeStreamGetPlayerSourcesChangeStream) | **GET** /PlayerSources/change-stream | Create a change stream.
*PlayerSourceApi* | [**playerSourceCreateChangeStreamPostPlayerSourcesChangeStream**](docs/PlayerSourceApi.md#playerSourceCreateChangeStreamPostPlayerSourcesChangeStream) | **POST** /PlayerSources/change-stream | Create a change stream.
*PlayerSourceApi* | [**playerSourceDeleteById**](docs/PlayerSourceApi.md#playerSourceDeleteById) | **DELETE** /PlayerSources/{id} | Delete a model instance by {{id}} from the data source.
*PlayerSourceApi* | [**playerSourceExistsGetPlayerSourcesidExists**](docs/PlayerSourceApi.md#playerSourceExistsGetPlayerSourcesidExists) | **GET** /PlayerSources/{id}/exists | Check whether a model instance exists in the data source.
*PlayerSourceApi* | [**playerSourceExistsHeadPlayerSourcesid**](docs/PlayerSourceApi.md#playerSourceExistsHeadPlayerSourcesid) | **HEAD** /PlayerSources/{id} | Check whether a model instance exists in the data source.
*PlayerSourceApi* | [**playerSourceFind**](docs/PlayerSourceApi.md#playerSourceFind) | **GET** /PlayerSources | Find all instances of the model matched by filter from the data source.
*PlayerSourceApi* | [**playerSourceFindById**](docs/PlayerSourceApi.md#playerSourceFindById) | **GET** /PlayerSources/{id} | Find a model instance by {{id}} from the data source.
*PlayerSourceApi* | [**playerSourceFindOne**](docs/PlayerSourceApi.md#playerSourceFindOne) | **GET** /PlayerSources/findOne | Find first instance of the model matched by filter from the data source.
*PlayerSourceApi* | [**playerSourcePrototypeGetPlaylistItem**](docs/PlayerSourceApi.md#playerSourcePrototypeGetPlaylistItem) | **GET** /PlayerSources/{id}/playlistItem | Fetches belongsTo relation playlistItem.
*PlayerSourceApi* | [**playerSourcePrototypeUpdateAttributesPatchPlayerSourcesid**](docs/PlayerSourceApi.md#playerSourcePrototypeUpdateAttributesPatchPlayerSourcesid) | **PATCH** /PlayerSources/{id} | Patch attributes for a model instance and persist it into the data source.
*PlayerSourceApi* | [**playerSourcePrototypeUpdateAttributesPutPlayerSourcesid**](docs/PlayerSourceApi.md#playerSourcePrototypeUpdateAttributesPutPlayerSourcesid) | **PUT** /PlayerSources/{id} | Patch attributes for a model instance and persist it into the data source.
*PlayerSourceApi* | [**playerSourceReplaceById**](docs/PlayerSourceApi.md#playerSourceReplaceById) | **POST** /PlayerSources/{id}/replace | Replace attributes for a model instance and persist it into the data source.
*PlayerSourceApi* | [**playerSourceReplaceOrCreate**](docs/PlayerSourceApi.md#playerSourceReplaceOrCreate) | **POST** /PlayerSources/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
*PlayerSourceApi* | [**playerSourceUpdateAll**](docs/PlayerSourceApi.md#playerSourceUpdateAll) | **POST** /PlayerSources/update | Update instances of the model matched by {{where}} from the data source.
*PlayerSourceApi* | [**playerSourceUpsertPatchPlayerSources**](docs/PlayerSourceApi.md#playerSourceUpsertPatchPlayerSources) | **PATCH** /PlayerSources | Patch an existing model instance or insert a new one into the data source.
*PlayerSourceApi* | [**playerSourceUpsertPutPlayerSources**](docs/PlayerSourceApi.md#playerSourceUpsertPutPlayerSources) | **PUT** /PlayerSources | Patch an existing model instance or insert a new one into the data source.
*PlayerSourceApi* | [**playerSourceUpsertWithWhere**](docs/PlayerSourceApi.md#playerSourceUpsertWithWhere) | **POST** /PlayerSources/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.
*PlayerTrackApi* | [**playerTrackCount**](docs/PlayerTrackApi.md#playerTrackCount) | **GET** /PlayerTracks/count | Count instances of the model matched by where from the data source.
*PlayerTrackApi* | [**playerTrackCreate**](docs/PlayerTrackApi.md#playerTrackCreate) | **POST** /PlayerTracks | Create a new instance of the model and persist it into the data source.
*PlayerTrackApi* | [**playerTrackCreateChangeStreamGetPlayerTracksChangeStream**](docs/PlayerTrackApi.md#playerTrackCreateChangeStreamGetPlayerTracksChangeStream) | **GET** /PlayerTracks/change-stream | Create a change stream.
*PlayerTrackApi* | [**playerTrackCreateChangeStreamPostPlayerTracksChangeStream**](docs/PlayerTrackApi.md#playerTrackCreateChangeStreamPostPlayerTracksChangeStream) | **POST** /PlayerTracks/change-stream | Create a change stream.
*PlayerTrackApi* | [**playerTrackDeleteById**](docs/PlayerTrackApi.md#playerTrackDeleteById) | **DELETE** /PlayerTracks/{id} | Delete a model instance by {{id}} from the data source.
*PlayerTrackApi* | [**playerTrackExistsGetPlayerTracksidExists**](docs/PlayerTrackApi.md#playerTrackExistsGetPlayerTracksidExists) | **GET** /PlayerTracks/{id}/exists | Check whether a model instance exists in the data source.
*PlayerTrackApi* | [**playerTrackExistsHeadPlayerTracksid**](docs/PlayerTrackApi.md#playerTrackExistsHeadPlayerTracksid) | **HEAD** /PlayerTracks/{id} | Check whether a model instance exists in the data source.
*PlayerTrackApi* | [**playerTrackFind**](docs/PlayerTrackApi.md#playerTrackFind) | **GET** /PlayerTracks | Find all instances of the model matched by filter from the data source.
*PlayerTrackApi* | [**playerTrackFindById**](docs/PlayerTrackApi.md#playerTrackFindById) | **GET** /PlayerTracks/{id} | Find a model instance by {{id}} from the data source.
*PlayerTrackApi* | [**playerTrackFindOne**](docs/PlayerTrackApi.md#playerTrackFindOne) | **GET** /PlayerTracks/findOne | Find first instance of the model matched by filter from the data source.
*PlayerTrackApi* | [**playerTrackPrototypeGetPlaylistItem**](docs/PlayerTrackApi.md#playerTrackPrototypeGetPlaylistItem) | **GET** /PlayerTracks/{id}/playlistItem | Fetches belongsTo relation playlistItem.
*PlayerTrackApi* | [**playerTrackPrototypeUpdateAttributesPatchPlayerTracksid**](docs/PlayerTrackApi.md#playerTrackPrototypeUpdateAttributesPatchPlayerTracksid) | **PATCH** /PlayerTracks/{id} | Patch attributes for a model instance and persist it into the data source.
*PlayerTrackApi* | [**playerTrackPrototypeUpdateAttributesPutPlayerTracksid**](docs/PlayerTrackApi.md#playerTrackPrototypeUpdateAttributesPutPlayerTracksid) | **PUT** /PlayerTracks/{id} | Patch attributes for a model instance and persist it into the data source.
*PlayerTrackApi* | [**playerTrackReplaceById**](docs/PlayerTrackApi.md#playerTrackReplaceById) | **POST** /PlayerTracks/{id}/replace | Replace attributes for a model instance and persist it into the data source.
*PlayerTrackApi* | [**playerTrackReplaceOrCreate**](docs/PlayerTrackApi.md#playerTrackReplaceOrCreate) | **POST** /PlayerTracks/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
*PlayerTrackApi* | [**playerTrackUpdateAll**](docs/PlayerTrackApi.md#playerTrackUpdateAll) | **POST** /PlayerTracks/update | Update instances of the model matched by {{where}} from the data source.
*PlayerTrackApi* | [**playerTrackUpsertPatchPlayerTracks**](docs/PlayerTrackApi.md#playerTrackUpsertPatchPlayerTracks) | **PATCH** /PlayerTracks | Patch an existing model instance or insert a new one into the data source.
*PlayerTrackApi* | [**playerTrackUpsertPutPlayerTracks**](docs/PlayerTrackApi.md#playerTrackUpsertPutPlayerTracks) | **PUT** /PlayerTracks | Patch an existing model instance or insert a new one into the data source.
*PlayerTrackApi* | [**playerTrackUpsertWithWhere**](docs/PlayerTrackApi.md#playerTrackUpsertWithWhere) | **POST** /PlayerTracks/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.
*PlaylistApi* | [**playlistCount**](docs/PlaylistApi.md#playlistCount) | **GET** /Playlists/count | Count instances of the model matched by where from the data source.
*PlaylistApi* | [**playlistCreate**](docs/PlaylistApi.md#playlistCreate) | **POST** /Playlists | Create a new instance of the model and persist it into the data source.
*PlaylistApi* | [**playlistCreateChangeStreamGetPlaylistsChangeStream**](docs/PlaylistApi.md#playlistCreateChangeStreamGetPlaylistsChangeStream) | **GET** /Playlists/change-stream | Create a change stream.
*PlaylistApi* | [**playlistCreateChangeStreamPostPlaylistsChangeStream**](docs/PlaylistApi.md#playlistCreateChangeStreamPostPlaylistsChangeStream) | **POST** /Playlists/change-stream | Create a change stream.
*PlaylistApi* | [**playlistDeleteById**](docs/PlaylistApi.md#playlistDeleteById) | **DELETE** /Playlists/{id} | Delete a model instance by {{id}} from the data source.
*PlaylistApi* | [**playlistExistsGetPlaylistsidExists**](docs/PlaylistApi.md#playlistExistsGetPlaylistsidExists) | **GET** /Playlists/{id}/exists | Check whether a model instance exists in the data source.
*PlaylistApi* | [**playlistExistsHeadPlaylistsid**](docs/PlaylistApi.md#playlistExistsHeadPlaylistsid) | **HEAD** /Playlists/{id} | Check whether a model instance exists in the data source.
*PlaylistApi* | [**playlistFind**](docs/PlaylistApi.md#playlistFind) | **GET** /Playlists | Find all instances of the model matched by filter from the data source.
*PlaylistApi* | [**playlistFindById**](docs/PlaylistApi.md#playlistFindById) | **GET** /Playlists/{id} | Find a model instance by {{id}} from the data source.
*PlaylistApi* | [**playlistFindOne**](docs/PlaylistApi.md#playlistFindOne) | **GET** /Playlists/findOne | Find first instance of the model matched by filter from the data source.
*PlaylistApi* | [**playlistPrototypeCountPlaylistItems**](docs/PlaylistApi.md#playlistPrototypeCountPlaylistItems) | **GET** /Playlists/{id}/playlistItems/count | Counts playlistItems of Playlist.
*PlaylistApi* | [**playlistPrototypeCreatePlaylistItems**](docs/PlaylistApi.md#playlistPrototypeCreatePlaylistItems) | **POST** /Playlists/{id}/playlistItems | Creates a new instance in playlistItems of this model.
*PlaylistApi* | [**playlistPrototypeDeletePlaylistItems**](docs/PlaylistApi.md#playlistPrototypeDeletePlaylistItems) | **DELETE** /Playlists/{id}/playlistItems | Deletes all playlistItems of this model.
*PlaylistApi* | [**playlistPrototypeDestroyByIdPlaylistItems**](docs/PlaylistApi.md#playlistPrototypeDestroyByIdPlaylistItems) | **DELETE** /Playlists/{id}/playlistItems/{fk} | Delete a related item by id for playlistItems.
*PlaylistApi* | [**playlistPrototypeFindByIdPlaylistItems**](docs/PlaylistApi.md#playlistPrototypeFindByIdPlaylistItems) | **GET** /Playlists/{id}/playlistItems/{fk} | Find a related item by id for playlistItems.
*PlaylistApi* | [**playlistPrototypeGetPlaylistItems**](docs/PlaylistApi.md#playlistPrototypeGetPlaylistItems) | **GET** /Playlists/{id}/playlistItems | Queries playlistItems of Playlist.
*PlaylistApi* | [**playlistPrototypeUpdateAttributesPatchPlaylistsid**](docs/PlaylistApi.md#playlistPrototypeUpdateAttributesPatchPlaylistsid) | **PATCH** /Playlists/{id} | Patch attributes for a model instance and persist it into the data source.
*PlaylistApi* | [**playlistPrototypeUpdateAttributesPutPlaylistsid**](docs/PlaylistApi.md#playlistPrototypeUpdateAttributesPutPlaylistsid) | **PUT** /Playlists/{id} | Patch attributes for a model instance and persist it into the data source.
*PlaylistApi* | [**playlistPrototypeUpdateByIdPlaylistItems**](docs/PlaylistApi.md#playlistPrototypeUpdateByIdPlaylistItems) | **PUT** /Playlists/{id}/playlistItems/{fk} | Update a related item by id for playlistItems.
*PlaylistApi* | [**playlistReplaceById**](docs/PlaylistApi.md#playlistReplaceById) | **POST** /Playlists/{id}/replace | Replace attributes for a model instance and persist it into the data source.
*PlaylistApi* | [**playlistReplaceOrCreate**](docs/PlaylistApi.md#playlistReplaceOrCreate) | **POST** /Playlists/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
*PlaylistApi* | [**playlistUpdateAll**](docs/PlaylistApi.md#playlistUpdateAll) | **POST** /Playlists/update | Update instances of the model matched by {{where}} from the data source.
*PlaylistApi* | [**playlistUpsertPatchPlaylists**](docs/PlaylistApi.md#playlistUpsertPatchPlaylists) | **PATCH** /Playlists | Patch an existing model instance or insert a new one into the data source.
*PlaylistApi* | [**playlistUpsertPutPlaylists**](docs/PlaylistApi.md#playlistUpsertPutPlaylists) | **PUT** /Playlists | Patch an existing model instance or insert a new one into the data source.
*PlaylistApi* | [**playlistUpsertWithWhere**](docs/PlaylistApi.md#playlistUpsertWithWhere) | **POST** /Playlists/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.
*PlaylistItemApi* | [**playlistItemCount**](docs/PlaylistItemApi.md#playlistItemCount) | **GET** /PlaylistItems/count | Count instances of the model matched by where from the data source.
*PlaylistItemApi* | [**playlistItemCreate**](docs/PlaylistItemApi.md#playlistItemCreate) | **POST** /PlaylistItems | Create a new instance of the model and persist it into the data source.
*PlaylistItemApi* | [**playlistItemCreateChangeStreamGetPlaylistItemsChangeStream**](docs/PlaylistItemApi.md#playlistItemCreateChangeStreamGetPlaylistItemsChangeStream) | **GET** /PlaylistItems/change-stream | Create a change stream.
*PlaylistItemApi* | [**playlistItemCreateChangeStreamPostPlaylistItemsChangeStream**](docs/PlaylistItemApi.md#playlistItemCreateChangeStreamPostPlaylistItemsChangeStream) | **POST** /PlaylistItems/change-stream | Create a change stream.
*PlaylistItemApi* | [**playlistItemDeleteById**](docs/PlaylistItemApi.md#playlistItemDeleteById) | **DELETE** /PlaylistItems/{id} | Delete a model instance by {{id}} from the data source.
*PlaylistItemApi* | [**playlistItemExistsGetPlaylistItemsidExists**](docs/PlaylistItemApi.md#playlistItemExistsGetPlaylistItemsidExists) | **GET** /PlaylistItems/{id}/exists | Check whether a model instance exists in the data source.
*PlaylistItemApi* | [**playlistItemExistsHeadPlaylistItemsid**](docs/PlaylistItemApi.md#playlistItemExistsHeadPlaylistItemsid) | **HEAD** /PlaylistItems/{id} | Check whether a model instance exists in the data source.
*PlaylistItemApi* | [**playlistItemFind**](docs/PlaylistItemApi.md#playlistItemFind) | **GET** /PlaylistItems | Find all instances of the model matched by filter from the data source.
*PlaylistItemApi* | [**playlistItemFindById**](docs/PlaylistItemApi.md#playlistItemFindById) | **GET** /PlaylistItems/{id} | Find a model instance by {{id}} from the data source.
*PlaylistItemApi* | [**playlistItemFindOne**](docs/PlaylistItemApi.md#playlistItemFindOne) | **GET** /PlaylistItems/findOne | Find first instance of the model matched by filter from the data source.
*PlaylistItemApi* | [**playlistItemPrototypeCountPlayerSources**](docs/PlaylistItemApi.md#playlistItemPrototypeCountPlayerSources) | **GET** /PlaylistItems/{id}/playerSources/count | Counts playerSources of PlaylistItem.
*PlaylistItemApi* | [**playlistItemPrototypeCountPlayerTracks**](docs/PlaylistItemApi.md#playlistItemPrototypeCountPlayerTracks) | **GET** /PlaylistItems/{id}/playerTracks/count | Counts playerTracks of PlaylistItem.
*PlaylistItemApi* | [**playlistItemPrototypeCountRadioShows**](docs/PlaylistItemApi.md#playlistItemPrototypeCountRadioShows) | **GET** /PlaylistItems/{id}/radioShows/count | Counts radioShows of PlaylistItem.
*PlaylistItemApi* | [**playlistItemPrototypeCreatePlayerSources**](docs/PlaylistItemApi.md#playlistItemPrototypeCreatePlayerSources) | **POST** /PlaylistItems/{id}/playerSources | Creates a new instance in playerSources of this model.
*PlaylistItemApi* | [**playlistItemPrototypeCreatePlayerTracks**](docs/PlaylistItemApi.md#playlistItemPrototypeCreatePlayerTracks) | **POST** /PlaylistItems/{id}/playerTracks | Creates a new instance in playerTracks of this model.
*PlaylistItemApi* | [**playlistItemPrototypeCreateRadioShows**](docs/PlaylistItemApi.md#playlistItemPrototypeCreateRadioShows) | **POST** /PlaylistItems/{id}/radioShows | Creates a new instance in radioShows of this model.
*PlaylistItemApi* | [**playlistItemPrototypeDeletePlayerSources**](docs/PlaylistItemApi.md#playlistItemPrototypeDeletePlayerSources) | **DELETE** /PlaylistItems/{id}/playerSources | Deletes all playerSources of this model.
*PlaylistItemApi* | [**playlistItemPrototypeDeletePlayerTracks**](docs/PlaylistItemApi.md#playlistItemPrototypeDeletePlayerTracks) | **DELETE** /PlaylistItems/{id}/playerTracks | Deletes all playerTracks of this model.
*PlaylistItemApi* | [**playlistItemPrototypeDeleteRadioShows**](docs/PlaylistItemApi.md#playlistItemPrototypeDeleteRadioShows) | **DELETE** /PlaylistItems/{id}/radioShows | Deletes all radioShows of this model.
*PlaylistItemApi* | [**playlistItemPrototypeDestroyByIdPlayerSources**](docs/PlaylistItemApi.md#playlistItemPrototypeDestroyByIdPlayerSources) | **DELETE** /PlaylistItems/{id}/playerSources/{fk} | Delete a related item by id for playerSources.
*PlaylistItemApi* | [**playlistItemPrototypeDestroyByIdPlayerTracks**](docs/PlaylistItemApi.md#playlistItemPrototypeDestroyByIdPlayerTracks) | **DELETE** /PlaylistItems/{id}/playerTracks/{fk} | Delete a related item by id for playerTracks.
*PlaylistItemApi* | [**playlistItemPrototypeDestroyByIdRadioShows**](docs/PlaylistItemApi.md#playlistItemPrototypeDestroyByIdRadioShows) | **DELETE** /PlaylistItems/{id}/radioShows/{fk} | Delete a related item by id for radioShows.
*PlaylistItemApi* | [**playlistItemPrototypeFindByIdPlayerSources**](docs/PlaylistItemApi.md#playlistItemPrototypeFindByIdPlayerSources) | **GET** /PlaylistItems/{id}/playerSources/{fk} | Find a related item by id for playerSources.
*PlaylistItemApi* | [**playlistItemPrototypeFindByIdPlayerTracks**](docs/PlaylistItemApi.md#playlistItemPrototypeFindByIdPlayerTracks) | **GET** /PlaylistItems/{id}/playerTracks/{fk} | Find a related item by id for playerTracks.
*PlaylistItemApi* | [**playlistItemPrototypeFindByIdRadioShows**](docs/PlaylistItemApi.md#playlistItemPrototypeFindByIdRadioShows) | **GET** /PlaylistItems/{id}/radioShows/{fk} | Find a related item by id for radioShows.
*PlaylistItemApi* | [**playlistItemPrototypeGetPlayerSources**](docs/PlaylistItemApi.md#playlistItemPrototypeGetPlayerSources) | **GET** /PlaylistItems/{id}/playerSources | Queries playerSources of PlaylistItem.
*PlaylistItemApi* | [**playlistItemPrototypeGetPlayerTracks**](docs/PlaylistItemApi.md#playlistItemPrototypeGetPlayerTracks) | **GET** /PlaylistItems/{id}/playerTracks | Queries playerTracks of PlaylistItem.
*PlaylistItemApi* | [**playlistItemPrototypeGetPlaylist**](docs/PlaylistItemApi.md#playlistItemPrototypeGetPlaylist) | **GET** /PlaylistItems/{id}/playlist | Fetches belongsTo relation playlist.
*PlaylistItemApi* | [**playlistItemPrototypeGetRadioShows**](docs/PlaylistItemApi.md#playlistItemPrototypeGetRadioShows) | **GET** /PlaylistItems/{id}/radioShows | Queries radioShows of PlaylistItem.
*PlaylistItemApi* | [**playlistItemPrototypeUpdateAttributesPatchPlaylistItemsid**](docs/PlaylistItemApi.md#playlistItemPrototypeUpdateAttributesPatchPlaylistItemsid) | **PATCH** /PlaylistItems/{id} | Patch attributes for a model instance and persist it into the data source.
*PlaylistItemApi* | [**playlistItemPrototypeUpdateAttributesPutPlaylistItemsid**](docs/PlaylistItemApi.md#playlistItemPrototypeUpdateAttributesPutPlaylistItemsid) | **PUT** /PlaylistItems/{id} | Patch attributes for a model instance and persist it into the data source.
*PlaylistItemApi* | [**playlistItemPrototypeUpdateByIdPlayerSources**](docs/PlaylistItemApi.md#playlistItemPrototypeUpdateByIdPlayerSources) | **PUT** /PlaylistItems/{id}/playerSources/{fk} | Update a related item by id for playerSources.
*PlaylistItemApi* | [**playlistItemPrototypeUpdateByIdPlayerTracks**](docs/PlaylistItemApi.md#playlistItemPrototypeUpdateByIdPlayerTracks) | **PUT** /PlaylistItems/{id}/playerTracks/{fk} | Update a related item by id for playerTracks.
*PlaylistItemApi* | [**playlistItemPrototypeUpdateByIdRadioShows**](docs/PlaylistItemApi.md#playlistItemPrototypeUpdateByIdRadioShows) | **PUT** /PlaylistItems/{id}/radioShows/{fk} | Update a related item by id for radioShows.
*PlaylistItemApi* | [**playlistItemReplaceById**](docs/PlaylistItemApi.md#playlistItemReplaceById) | **POST** /PlaylistItems/{id}/replace | Replace attributes for a model instance and persist it into the data source.
*PlaylistItemApi* | [**playlistItemReplaceOrCreate**](docs/PlaylistItemApi.md#playlistItemReplaceOrCreate) | **POST** /PlaylistItems/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
*PlaylistItemApi* | [**playlistItemUpdateAll**](docs/PlaylistItemApi.md#playlistItemUpdateAll) | **POST** /PlaylistItems/update | Update instances of the model matched by {{where}} from the data source.
*PlaylistItemApi* | [**playlistItemUpsertPatchPlaylistItems**](docs/PlaylistItemApi.md#playlistItemUpsertPatchPlaylistItems) | **PATCH** /PlaylistItems | Patch an existing model instance or insert a new one into the data source.
*PlaylistItemApi* | [**playlistItemUpsertPutPlaylistItems**](docs/PlaylistItemApi.md#playlistItemUpsertPutPlaylistItems) | **PUT** /PlaylistItems | Patch an existing model instance or insert a new one into the data source.
*PlaylistItemApi* | [**playlistItemUpsertWithWhere**](docs/PlaylistItemApi.md#playlistItemUpsertWithWhere) | **POST** /PlaylistItems/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.
*RadioPlaySessionApi* | [**radioPlaySessionCount**](docs/RadioPlaySessionApi.md#radioPlaySessionCount) | **GET** /RadioPlaySessions/count | Count instances of the model matched by where from the data source.
*RadioPlaySessionApi* | [**radioPlaySessionCreate**](docs/RadioPlaySessionApi.md#radioPlaySessionCreate) | **POST** /RadioPlaySessions | Create a new instance of the model and persist it into the data source.
*RadioPlaySessionApi* | [**radioPlaySessionCreateChangeStreamGetRadioPlaySessionsChangeStream**](docs/RadioPlaySessionApi.md#radioPlaySessionCreateChangeStreamGetRadioPlaySessionsChangeStream) | **GET** /RadioPlaySessions/change-stream | Create a change stream.
*RadioPlaySessionApi* | [**radioPlaySessionCreateChangeStreamPostRadioPlaySessionsChangeStream**](docs/RadioPlaySessionApi.md#radioPlaySessionCreateChangeStreamPostRadioPlaySessionsChangeStream) | **POST** /RadioPlaySessions/change-stream | Create a change stream.
*RadioPlaySessionApi* | [**radioPlaySessionDeleteById**](docs/RadioPlaySessionApi.md#radioPlaySessionDeleteById) | **DELETE** /RadioPlaySessions/{id} | Delete a model instance by {{id}} from the data source.
*RadioPlaySessionApi* | [**radioPlaySessionExistsGetRadioPlaySessionsidExists**](docs/RadioPlaySessionApi.md#radioPlaySessionExistsGetRadioPlaySessionsidExists) | **GET** /RadioPlaySessions/{id}/exists | Check whether a model instance exists in the data source.
*RadioPlaySessionApi* | [**radioPlaySessionExistsHeadRadioPlaySessionsid**](docs/RadioPlaySessionApi.md#radioPlaySessionExistsHeadRadioPlaySessionsid) | **HEAD** /RadioPlaySessions/{id} | Check whether a model instance exists in the data source.
*RadioPlaySessionApi* | [**radioPlaySessionFind**](docs/RadioPlaySessionApi.md#radioPlaySessionFind) | **GET** /RadioPlaySessions | Find all instances of the model matched by filter from the data source.
*RadioPlaySessionApi* | [**radioPlaySessionFindById**](docs/RadioPlaySessionApi.md#radioPlaySessionFindById) | **GET** /RadioPlaySessions/{id} | Find a model instance by {{id}} from the data source.
*RadioPlaySessionApi* | [**radioPlaySessionFindOne**](docs/RadioPlaySessionApi.md#radioPlaySessionFindOne) | **GET** /RadioPlaySessions/findOne | Find first instance of the model matched by filter from the data source.
*RadioPlaySessionApi* | [**radioPlaySessionPrototypeUpdateAttributesPatchRadioPlaySessionsid**](docs/RadioPlaySessionApi.md#radioPlaySessionPrototypeUpdateAttributesPatchRadioPlaySessionsid) | **PATCH** /RadioPlaySessions/{id} | Patch attributes for a model instance and persist it into the data source.
*RadioPlaySessionApi* | [**radioPlaySessionPrototypeUpdateAttributesPutRadioPlaySessionsid**](docs/RadioPlaySessionApi.md#radioPlaySessionPrototypeUpdateAttributesPutRadioPlaySessionsid) | **PUT** /RadioPlaySessions/{id} | Patch attributes for a model instance and persist it into the data source.
*RadioPlaySessionApi* | [**radioPlaySessionReplaceById**](docs/RadioPlaySessionApi.md#radioPlaySessionReplaceById) | **POST** /RadioPlaySessions/{id}/replace | Replace attributes for a model instance and persist it into the data source.
*RadioPlaySessionApi* | [**radioPlaySessionReplaceOrCreate**](docs/RadioPlaySessionApi.md#radioPlaySessionReplaceOrCreate) | **POST** /RadioPlaySessions/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
*RadioPlaySessionApi* | [**radioPlaySessionUpdateAll**](docs/RadioPlaySessionApi.md#radioPlaySessionUpdateAll) | **POST** /RadioPlaySessions/update | Update instances of the model matched by {{where}} from the data source.
*RadioPlaySessionApi* | [**radioPlaySessionUpsertPatchRadioPlaySessions**](docs/RadioPlaySessionApi.md#radioPlaySessionUpsertPatchRadioPlaySessions) | **PATCH** /RadioPlaySessions | Patch an existing model instance or insert a new one into the data source.
*RadioPlaySessionApi* | [**radioPlaySessionUpsertPutRadioPlaySessions**](docs/RadioPlaySessionApi.md#radioPlaySessionUpsertPutRadioPlaySessions) | **PUT** /RadioPlaySessions | Patch an existing model instance or insert a new one into the data source.
*RadioPlaySessionApi* | [**radioPlaySessionUpsertWithWhere**](docs/RadioPlaySessionApi.md#radioPlaySessionUpsertWithWhere) | **POST** /RadioPlaySessions/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.
*RadioPresenterApi* | [**radioPresenterCount**](docs/RadioPresenterApi.md#radioPresenterCount) | **GET** /RadioPresenters/count | Count instances of the model matched by where from the data source.
*RadioPresenterApi* | [**radioPresenterCreate**](docs/RadioPresenterApi.md#radioPresenterCreate) | **POST** /RadioPresenters | Create a new instance of the model and persist it into the data source.
*RadioPresenterApi* | [**radioPresenterCreateChangeStreamGetRadioPresentersChangeStream**](docs/RadioPresenterApi.md#radioPresenterCreateChangeStreamGetRadioPresentersChangeStream) | **GET** /RadioPresenters/change-stream | Create a change stream.
*RadioPresenterApi* | [**radioPresenterCreateChangeStreamPostRadioPresentersChangeStream**](docs/RadioPresenterApi.md#radioPresenterCreateChangeStreamPostRadioPresentersChangeStream) | **POST** /RadioPresenters/change-stream | Create a change stream.
*RadioPresenterApi* | [**radioPresenterDeleteById**](docs/RadioPresenterApi.md#radioPresenterDeleteById) | **DELETE** /RadioPresenters/{id} | Delete a model instance by {{id}} from the data source.
*RadioPresenterApi* | [**radioPresenterExistsGetRadioPresentersidExists**](docs/RadioPresenterApi.md#radioPresenterExistsGetRadioPresentersidExists) | **GET** /RadioPresenters/{id}/exists | Check whether a model instance exists in the data source.
*RadioPresenterApi* | [**radioPresenterExistsHeadRadioPresentersid**](docs/RadioPresenterApi.md#radioPresenterExistsHeadRadioPresentersid) | **HEAD** /RadioPresenters/{id} | Check whether a model instance exists in the data source.
*RadioPresenterApi* | [**radioPresenterFind**](docs/RadioPresenterApi.md#radioPresenterFind) | **GET** /RadioPresenters | Find all instances of the model matched by filter from the data source.
*RadioPresenterApi* | [**radioPresenterFindById**](docs/RadioPresenterApi.md#radioPresenterFindById) | **GET** /RadioPresenters/{id} | Find a model instance by {{id}} from the data source.
*RadioPresenterApi* | [**radioPresenterFindOne**](docs/RadioPresenterApi.md#radioPresenterFindOne) | **GET** /RadioPresenters/findOne | Find first instance of the model matched by filter from the data source.
*RadioPresenterApi* | [**radioPresenterPrototypeCountRadioShows**](docs/RadioPresenterApi.md#radioPresenterPrototypeCountRadioShows) | **GET** /RadioPresenters/{id}/radioShows/count | Counts radioShows of RadioPresenter.
*RadioPresenterApi* | [**radioPresenterPrototypeCreateRadioShows**](docs/RadioPresenterApi.md#radioPresenterPrototypeCreateRadioShows) | **POST** /RadioPresenters/{id}/radioShows | Creates a new instance in radioShows of this model.
*RadioPresenterApi* | [**radioPresenterPrototypeDeleteRadioShows**](docs/RadioPresenterApi.md#radioPresenterPrototypeDeleteRadioShows) | **DELETE** /RadioPresenters/{id}/radioShows | Deletes all radioShows of this model.
*RadioPresenterApi* | [**radioPresenterPrototypeDestroyByIdRadioShows**](docs/RadioPresenterApi.md#radioPresenterPrototypeDestroyByIdRadioShows) | **DELETE** /RadioPresenters/{id}/radioShows/{fk} | Delete a related item by id for radioShows.
*RadioPresenterApi* | [**radioPresenterPrototypeExistsRadioShows**](docs/RadioPresenterApi.md#radioPresenterPrototypeExistsRadioShows) | **HEAD** /RadioPresenters/{id}/radioShows/rel/{fk} | Check the existence of radioShows relation to an item by id.
*RadioPresenterApi* | [**radioPresenterPrototypeFindByIdRadioShows**](docs/RadioPresenterApi.md#radioPresenterPrototypeFindByIdRadioShows) | **GET** /RadioPresenters/{id}/radioShows/{fk} | Find a related item by id for radioShows.
*RadioPresenterApi* | [**radioPresenterPrototypeGetRadioShows**](docs/RadioPresenterApi.md#radioPresenterPrototypeGetRadioShows) | **GET** /RadioPresenters/{id}/radioShows | Queries radioShows of RadioPresenter.
*RadioPresenterApi* | [**radioPresenterPrototypeLinkRadioShows**](docs/RadioPresenterApi.md#radioPresenterPrototypeLinkRadioShows) | **PUT** /RadioPresenters/{id}/radioShows/rel/{fk} | Add a related item by id for radioShows.
*RadioPresenterApi* | [**radioPresenterPrototypeUnlinkRadioShows**](docs/RadioPresenterApi.md#radioPresenterPrototypeUnlinkRadioShows) | **DELETE** /RadioPresenters/{id}/radioShows/rel/{fk} | Remove the radioShows relation to an item by id.
*RadioPresenterApi* | [**radioPresenterPrototypeUpdateAttributesPatchRadioPresentersid**](docs/RadioPresenterApi.md#radioPresenterPrototypeUpdateAttributesPatchRadioPresentersid) | **PATCH** /RadioPresenters/{id} | Patch attributes for a model instance and persist it into the data source.
*RadioPresenterApi* | [**radioPresenterPrototypeUpdateAttributesPutRadioPresentersid**](docs/RadioPresenterApi.md#radioPresenterPrototypeUpdateAttributesPutRadioPresentersid) | **PUT** /RadioPresenters/{id} | Patch attributes for a model instance and persist it into the data source.
*RadioPresenterApi* | [**radioPresenterPrototypeUpdateByIdRadioShows**](docs/RadioPresenterApi.md#radioPresenterPrototypeUpdateByIdRadioShows) | **PUT** /RadioPresenters/{id}/radioShows/{fk} | Update a related item by id for radioShows.
*RadioPresenterApi* | [**radioPresenterReplaceById**](docs/RadioPresenterApi.md#radioPresenterReplaceById) | **POST** /RadioPresenters/{id}/replace | Replace attributes for a model instance and persist it into the data source.
*RadioPresenterApi* | [**radioPresenterReplaceOrCreate**](docs/RadioPresenterApi.md#radioPresenterReplaceOrCreate) | **POST** /RadioPresenters/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
*RadioPresenterApi* | [**radioPresenterUpdateAll**](docs/RadioPresenterApi.md#radioPresenterUpdateAll) | **POST** /RadioPresenters/update | Update instances of the model matched by {{where}} from the data source.
*RadioPresenterApi* | [**radioPresenterUploadImage**](docs/RadioPresenterApi.md#radioPresenterUploadImage) | **POST** /RadioPresenters/{id}/file/image | 
*RadioPresenterApi* | [**radioPresenterUpsertPatchRadioPresenters**](docs/RadioPresenterApi.md#radioPresenterUpsertPatchRadioPresenters) | **PATCH** /RadioPresenters | Patch an existing model instance or insert a new one into the data source.
*RadioPresenterApi* | [**radioPresenterUpsertPutRadioPresenters**](docs/RadioPresenterApi.md#radioPresenterUpsertPutRadioPresenters) | **PUT** /RadioPresenters | Patch an existing model instance or insert a new one into the data source.
*RadioPresenterApi* | [**radioPresenterUpsertWithWhere**](docs/RadioPresenterApi.md#radioPresenterUpsertWithWhere) | **POST** /RadioPresenters/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.
*RadioPresenterRadioShowApi* | [**radioPresenterRadioShowCount**](docs/RadioPresenterRadioShowApi.md#radioPresenterRadioShowCount) | **GET** /RadioPresenterRadioShows/count | Count instances of the model matched by where from the data source.
*RadioPresenterRadioShowApi* | [**radioPresenterRadioShowCreate**](docs/RadioPresenterRadioShowApi.md#radioPresenterRadioShowCreate) | **POST** /RadioPresenterRadioShows | Create a new instance of the model and persist it into the data source.
*RadioPresenterRadioShowApi* | [**radioPresenterRadioShowCreateChangeStreamGetRadioPresenterRadioShowsChangeStream**](docs/RadioPresenterRadioShowApi.md#radioPresenterRadioShowCreateChangeStreamGetRadioPresenterRadioShowsChangeStream) | **GET** /RadioPresenterRadioShows/change-stream | Create a change stream.
*RadioPresenterRadioShowApi* | [**radioPresenterRadioShowCreateChangeStreamPostRadioPresenterRadioShowsChangeStream**](docs/RadioPresenterRadioShowApi.md#radioPresenterRadioShowCreateChangeStreamPostRadioPresenterRadioShowsChangeStream) | **POST** /RadioPresenterRadioShows/change-stream | Create a change stream.
*RadioPresenterRadioShowApi* | [**radioPresenterRadioShowDeleteById**](docs/RadioPresenterRadioShowApi.md#radioPresenterRadioShowDeleteById) | **DELETE** /RadioPresenterRadioShows/{id} | Delete a model instance by {{id}} from the data source.
*RadioPresenterRadioShowApi* | [**radioPresenterRadioShowExistsGetRadioPresenterRadioShowsidExists**](docs/RadioPresenterRadioShowApi.md#radioPresenterRadioShowExistsGetRadioPresenterRadioShowsidExists) | **GET** /RadioPresenterRadioShows/{id}/exists | Check whether a model instance exists in the data source.
*RadioPresenterRadioShowApi* | [**radioPresenterRadioShowExistsHeadRadioPresenterRadioShowsid**](docs/RadioPresenterRadioShowApi.md#radioPresenterRadioShowExistsHeadRadioPresenterRadioShowsid) | **HEAD** /RadioPresenterRadioShows/{id} | Check whether a model instance exists in the data source.
*RadioPresenterRadioShowApi* | [**radioPresenterRadioShowFind**](docs/RadioPresenterRadioShowApi.md#radioPresenterRadioShowFind) | **GET** /RadioPresenterRadioShows | Find all instances of the model matched by filter from the data source.
*RadioPresenterRadioShowApi* | [**radioPresenterRadioShowFindById**](docs/RadioPresenterRadioShowApi.md#radioPresenterRadioShowFindById) | **GET** /RadioPresenterRadioShows/{id} | Find a model instance by {{id}} from the data source.
*RadioPresenterRadioShowApi* | [**radioPresenterRadioShowFindOne**](docs/RadioPresenterRadioShowApi.md#radioPresenterRadioShowFindOne) | **GET** /RadioPresenterRadioShows/findOne | Find first instance of the model matched by filter from the data source.
*RadioPresenterRadioShowApi* | [**radioPresenterRadioShowPrototypeGetRadioPresenter**](docs/RadioPresenterRadioShowApi.md#radioPresenterRadioShowPrototypeGetRadioPresenter) | **GET** /RadioPresenterRadioShows/{id}/radioPresenter | Fetches belongsTo relation radioPresenter.
*RadioPresenterRadioShowApi* | [**radioPresenterRadioShowPrototypeGetRadioShow**](docs/RadioPresenterRadioShowApi.md#radioPresenterRadioShowPrototypeGetRadioShow) | **GET** /RadioPresenterRadioShows/{id}/radioShow | Fetches belongsTo relation radioShow.
*RadioPresenterRadioShowApi* | [**radioPresenterRadioShowPrototypeUpdateAttributesPatchRadioPresenterRadioShowsid**](docs/RadioPresenterRadioShowApi.md#radioPresenterRadioShowPrototypeUpdateAttributesPatchRadioPresenterRadioShowsid) | **PATCH** /RadioPresenterRadioShows/{id} | Patch attributes for a model instance and persist it into the data source.
*RadioPresenterRadioShowApi* | [**radioPresenterRadioShowPrototypeUpdateAttributesPutRadioPresenterRadioShowsid**](docs/RadioPresenterRadioShowApi.md#radioPresenterRadioShowPrototypeUpdateAttributesPutRadioPresenterRadioShowsid) | **PUT** /RadioPresenterRadioShows/{id} | Patch attributes for a model instance and persist it into the data source.
*RadioPresenterRadioShowApi* | [**radioPresenterRadioShowReplaceById**](docs/RadioPresenterRadioShowApi.md#radioPresenterRadioShowReplaceById) | **POST** /RadioPresenterRadioShows/{id}/replace | Replace attributes for a model instance and persist it into the data source.
*RadioPresenterRadioShowApi* | [**radioPresenterRadioShowReplaceOrCreate**](docs/RadioPresenterRadioShowApi.md#radioPresenterRadioShowReplaceOrCreate) | **POST** /RadioPresenterRadioShows/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
*RadioPresenterRadioShowApi* | [**radioPresenterRadioShowUpdateAll**](docs/RadioPresenterRadioShowApi.md#radioPresenterRadioShowUpdateAll) | **POST** /RadioPresenterRadioShows/update | Update instances of the model matched by {{where}} from the data source.
*RadioPresenterRadioShowApi* | [**radioPresenterRadioShowUpsertPatchRadioPresenterRadioShows**](docs/RadioPresenterRadioShowApi.md#radioPresenterRadioShowUpsertPatchRadioPresenterRadioShows) | **PATCH** /RadioPresenterRadioShows | Patch an existing model instance or insert a new one into the data source.
*RadioPresenterRadioShowApi* | [**radioPresenterRadioShowUpsertPutRadioPresenterRadioShows**](docs/RadioPresenterRadioShowApi.md#radioPresenterRadioShowUpsertPutRadioPresenterRadioShows) | **PUT** /RadioPresenterRadioShows | Patch an existing model instance or insert a new one into the data source.
*RadioPresenterRadioShowApi* | [**radioPresenterRadioShowUpsertWithWhere**](docs/RadioPresenterRadioShowApi.md#radioPresenterRadioShowUpsertWithWhere) | **POST** /RadioPresenterRadioShows/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.
*RadioShowApi* | [**radioShowCount**](docs/RadioShowApi.md#radioShowCount) | **GET** /RadioShows/count | Count instances of the model matched by where from the data source.
*RadioShowApi* | [**radioShowCreate**](docs/RadioShowApi.md#radioShowCreate) | **POST** /RadioShows | Create a new instance of the model and persist it into the data source.
*RadioShowApi* | [**radioShowCreateChangeStreamGetRadioShowsChangeStream**](docs/RadioShowApi.md#radioShowCreateChangeStreamGetRadioShowsChangeStream) | **GET** /RadioShows/change-stream | Create a change stream.
*RadioShowApi* | [**radioShowCreateChangeStreamPostRadioShowsChangeStream**](docs/RadioShowApi.md#radioShowCreateChangeStreamPostRadioShowsChangeStream) | **POST** /RadioShows/change-stream | Create a change stream.
*RadioShowApi* | [**radioShowDeleteById**](docs/RadioShowApi.md#radioShowDeleteById) | **DELETE** /RadioShows/{id} | Delete a model instance by {{id}} from the data source.
*RadioShowApi* | [**radioShowExistsGetRadioShowsidExists**](docs/RadioShowApi.md#radioShowExistsGetRadioShowsidExists) | **GET** /RadioShows/{id}/exists | Check whether a model instance exists in the data source.
*RadioShowApi* | [**radioShowExistsHeadRadioShowsid**](docs/RadioShowApi.md#radioShowExistsHeadRadioShowsid) | **HEAD** /RadioShows/{id} | Check whether a model instance exists in the data source.
*RadioShowApi* | [**radioShowFind**](docs/RadioShowApi.md#radioShowFind) | **GET** /RadioShows | Find all instances of the model matched by filter from the data source.
*RadioShowApi* | [**radioShowFindById**](docs/RadioShowApi.md#radioShowFindById) | **GET** /RadioShows/{id} | Find a model instance by {{id}} from the data source.
*RadioShowApi* | [**radioShowFindOne**](docs/RadioShowApi.md#radioShowFindOne) | **GET** /RadioShows/findOne | Find first instance of the model matched by filter from the data source.
*RadioShowApi* | [**radioShowPrototypeCountRadioPresenters**](docs/RadioShowApi.md#radioShowPrototypeCountRadioPresenters) | **GET** /RadioShows/{id}/radioPresenters/count | Counts radioPresenters of RadioShow.
*RadioShowApi* | [**radioShowPrototypeCountRadioShowTimes**](docs/RadioShowApi.md#radioShowPrototypeCountRadioShowTimes) | **GET** /RadioShows/{id}/radioShowTimes/count | Counts radioShowTimes of RadioShow.
*RadioShowApi* | [**radioShowPrototypeCreateRadioPresenters**](docs/RadioShowApi.md#radioShowPrototypeCreateRadioPresenters) | **POST** /RadioShows/{id}/radioPresenters | Creates a new instance in radioPresenters of this model.
*RadioShowApi* | [**radioShowPrototypeCreateRadioShowTimes**](docs/RadioShowApi.md#radioShowPrototypeCreateRadioShowTimes) | **POST** /RadioShows/{id}/radioShowTimes | Creates a new instance in radioShowTimes of this model.
*RadioShowApi* | [**radioShowPrototypeDeleteRadioPresenters**](docs/RadioShowApi.md#radioShowPrototypeDeleteRadioPresenters) | **DELETE** /RadioShows/{id}/radioPresenters | Deletes all radioPresenters of this model.
*RadioShowApi* | [**radioShowPrototypeDeleteRadioShowTimes**](docs/RadioShowApi.md#radioShowPrototypeDeleteRadioShowTimes) | **DELETE** /RadioShows/{id}/radioShowTimes | Deletes all radioShowTimes of this model.
*RadioShowApi* | [**radioShowPrototypeDestroyByIdRadioPresenters**](docs/RadioShowApi.md#radioShowPrototypeDestroyByIdRadioPresenters) | **DELETE** /RadioShows/{id}/radioPresenters/{fk} | Delete a related item by id for radioPresenters.
*RadioShowApi* | [**radioShowPrototypeDestroyByIdRadioShowTimes**](docs/RadioShowApi.md#radioShowPrototypeDestroyByIdRadioShowTimes) | **DELETE** /RadioShows/{id}/radioShowTimes/{fk} | Delete a related item by id for radioShowTimes.
*RadioShowApi* | [**radioShowPrototypeExistsRadioPresenters**](docs/RadioShowApi.md#radioShowPrototypeExistsRadioPresenters) | **HEAD** /RadioShows/{id}/radioPresenters/rel/{fk} | Check the existence of radioPresenters relation to an item by id.
*RadioShowApi* | [**radioShowPrototypeFindByIdRadioPresenters**](docs/RadioShowApi.md#radioShowPrototypeFindByIdRadioPresenters) | **GET** /RadioShows/{id}/radioPresenters/{fk} | Find a related item by id for radioPresenters.
*RadioShowApi* | [**radioShowPrototypeFindByIdRadioShowTimes**](docs/RadioShowApi.md#radioShowPrototypeFindByIdRadioShowTimes) | **GET** /RadioShows/{id}/radioShowTimes/{fk} | Find a related item by id for radioShowTimes.
*RadioShowApi* | [**radioShowPrototypeGetPlaylistItem**](docs/RadioShowApi.md#radioShowPrototypeGetPlaylistItem) | **GET** /RadioShows/{id}/playlistItem | Fetches belongsTo relation playlistItem.
*RadioShowApi* | [**radioShowPrototypeGetRadioPresenters**](docs/RadioShowApi.md#radioShowPrototypeGetRadioPresenters) | **GET** /RadioShows/{id}/radioPresenters | Queries radioPresenters of RadioShow.
*RadioShowApi* | [**radioShowPrototypeGetRadioShowTimes**](docs/RadioShowApi.md#radioShowPrototypeGetRadioShowTimes) | **GET** /RadioShows/{id}/radioShowTimes | Queries radioShowTimes of RadioShow.
*RadioShowApi* | [**radioShowPrototypeGetRadioStream**](docs/RadioShowApi.md#radioShowPrototypeGetRadioStream) | **GET** /RadioShows/{id}/radioStream | Fetches belongsTo relation radioStream.
*RadioShowApi* | [**radioShowPrototypeLinkRadioPresenters**](docs/RadioShowApi.md#radioShowPrototypeLinkRadioPresenters) | **PUT** /RadioShows/{id}/radioPresenters/rel/{fk} | Add a related item by id for radioPresenters.
*RadioShowApi* | [**radioShowPrototypeUnlinkRadioPresenters**](docs/RadioShowApi.md#radioShowPrototypeUnlinkRadioPresenters) | **DELETE** /RadioShows/{id}/radioPresenters/rel/{fk} | Remove the radioPresenters relation to an item by id.
*RadioShowApi* | [**radioShowPrototypeUpdateAttributesPatchRadioShowsid**](docs/RadioShowApi.md#radioShowPrototypeUpdateAttributesPatchRadioShowsid) | **PATCH** /RadioShows/{id} | Patch attributes for a model instance and persist it into the data source.
*RadioShowApi* | [**radioShowPrototypeUpdateAttributesPutRadioShowsid**](docs/RadioShowApi.md#radioShowPrototypeUpdateAttributesPutRadioShowsid) | **PUT** /RadioShows/{id} | Patch attributes for a model instance and persist it into the data source.
*RadioShowApi* | [**radioShowPrototypeUpdateByIdRadioPresenters**](docs/RadioShowApi.md#radioShowPrototypeUpdateByIdRadioPresenters) | **PUT** /RadioShows/{id}/radioPresenters/{fk} | Update a related item by id for radioPresenters.
*RadioShowApi* | [**radioShowPrototypeUpdateByIdRadioShowTimes**](docs/RadioShowApi.md#radioShowPrototypeUpdateByIdRadioShowTimes) | **PUT** /RadioShows/{id}/radioShowTimes/{fk} | Update a related item by id for radioShowTimes.
*RadioShowApi* | [**radioShowReplaceById**](docs/RadioShowApi.md#radioShowReplaceById) | **POST** /RadioShows/{id}/replace | Replace attributes for a model instance and persist it into the data source.
*RadioShowApi* | [**radioShowReplaceOrCreate**](docs/RadioShowApi.md#radioShowReplaceOrCreate) | **POST** /RadioShows/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
*RadioShowApi* | [**radioShowUpdateAll**](docs/RadioShowApi.md#radioShowUpdateAll) | **POST** /RadioShows/update | Update instances of the model matched by {{where}} from the data source.
*RadioShowApi* | [**radioShowUploadFile**](docs/RadioShowApi.md#radioShowUploadFile) | **POST** /RadioShows/{id}/file | Upload File to Radio Show
*RadioShowApi* | [**radioShowUpsertPatchRadioShows**](docs/RadioShowApi.md#radioShowUpsertPatchRadioShows) | **PATCH** /RadioShows | Patch an existing model instance or insert a new one into the data source.
*RadioShowApi* | [**radioShowUpsertPutRadioShows**](docs/RadioShowApi.md#radioShowUpsertPutRadioShows) | **PUT** /RadioShows | Patch an existing model instance or insert a new one into the data source.
*RadioShowApi* | [**radioShowUpsertWithWhere**](docs/RadioShowApi.md#radioShowUpsertWithWhere) | **POST** /RadioShows/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.
*RadioShowTimeApi* | [**radioShowTimeCount**](docs/RadioShowTimeApi.md#radioShowTimeCount) | **GET** /RadioShowTimes/count | Count instances of the model matched by where from the data source.
*RadioShowTimeApi* | [**radioShowTimeCreate**](docs/RadioShowTimeApi.md#radioShowTimeCreate) | **POST** /RadioShowTimes | Create a new instance of the model and persist it into the data source.
*RadioShowTimeApi* | [**radioShowTimeCreateChangeStreamGetRadioShowTimesChangeStream**](docs/RadioShowTimeApi.md#radioShowTimeCreateChangeStreamGetRadioShowTimesChangeStream) | **GET** /RadioShowTimes/change-stream | Create a change stream.
*RadioShowTimeApi* | [**radioShowTimeCreateChangeStreamPostRadioShowTimesChangeStream**](docs/RadioShowTimeApi.md#radioShowTimeCreateChangeStreamPostRadioShowTimesChangeStream) | **POST** /RadioShowTimes/change-stream | Create a change stream.
*RadioShowTimeApi* | [**radioShowTimeDeleteById**](docs/RadioShowTimeApi.md#radioShowTimeDeleteById) | **DELETE** /RadioShowTimes/{id} | Delete a model instance by {{id}} from the data source.
*RadioShowTimeApi* | [**radioShowTimeExistsGetRadioShowTimesidExists**](docs/RadioShowTimeApi.md#radioShowTimeExistsGetRadioShowTimesidExists) | **GET** /RadioShowTimes/{id}/exists | Check whether a model instance exists in the data source.
*RadioShowTimeApi* | [**radioShowTimeExistsHeadRadioShowTimesid**](docs/RadioShowTimeApi.md#radioShowTimeExistsHeadRadioShowTimesid) | **HEAD** /RadioShowTimes/{id} | Check whether a model instance exists in the data source.
*RadioShowTimeApi* | [**radioShowTimeFind**](docs/RadioShowTimeApi.md#radioShowTimeFind) | **GET** /RadioShowTimes | Find all instances of the model matched by filter from the data source.
*RadioShowTimeApi* | [**radioShowTimeFindById**](docs/RadioShowTimeApi.md#radioShowTimeFindById) | **GET** /RadioShowTimes/{id} | Find a model instance by {{id}} from the data source.
*RadioShowTimeApi* | [**radioShowTimeFindOne**](docs/RadioShowTimeApi.md#radioShowTimeFindOne) | **GET** /RadioShowTimes/findOne | Find first instance of the model matched by filter from the data source.
*RadioShowTimeApi* | [**radioShowTimePrototypeGetRadioShow**](docs/RadioShowTimeApi.md#radioShowTimePrototypeGetRadioShow) | **GET** /RadioShowTimes/{id}/radioShow | Fetches belongsTo relation radioShow.
*RadioShowTimeApi* | [**radioShowTimePrototypeUpdateAttributesPatchRadioShowTimesid**](docs/RadioShowTimeApi.md#radioShowTimePrototypeUpdateAttributesPatchRadioShowTimesid) | **PATCH** /RadioShowTimes/{id} | Patch attributes for a model instance and persist it into the data source.
*RadioShowTimeApi* | [**radioShowTimePrototypeUpdateAttributesPutRadioShowTimesid**](docs/RadioShowTimeApi.md#radioShowTimePrototypeUpdateAttributesPutRadioShowTimesid) | **PUT** /RadioShowTimes/{id} | Patch attributes for a model instance and persist it into the data source.
*RadioShowTimeApi* | [**radioShowTimeReplaceById**](docs/RadioShowTimeApi.md#radioShowTimeReplaceById) | **POST** /RadioShowTimes/{id}/replace | Replace attributes for a model instance and persist it into the data source.
*RadioShowTimeApi* | [**radioShowTimeReplaceOrCreate**](docs/RadioShowTimeApi.md#radioShowTimeReplaceOrCreate) | **POST** /RadioShowTimes/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
*RadioShowTimeApi* | [**radioShowTimeUpdateAll**](docs/RadioShowTimeApi.md#radioShowTimeUpdateAll) | **POST** /RadioShowTimes/update | Update instances of the model matched by {{where}} from the data source.
*RadioShowTimeApi* | [**radioShowTimeUpsertPatchRadioShowTimes**](docs/RadioShowTimeApi.md#radioShowTimeUpsertPatchRadioShowTimes) | **PATCH** /RadioShowTimes | Patch an existing model instance or insert a new one into the data source.
*RadioShowTimeApi* | [**radioShowTimeUpsertPutRadioShowTimes**](docs/RadioShowTimeApi.md#radioShowTimeUpsertPutRadioShowTimes) | **PUT** /RadioShowTimes | Patch an existing model instance or insert a new one into the data source.
*RadioShowTimeApi* | [**radioShowTimeUpsertWithWhere**](docs/RadioShowTimeApi.md#radioShowTimeUpsertWithWhere) | **POST** /RadioShowTimes/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.
*RadioStreamApi* | [**radioStreamCount**](docs/RadioStreamApi.md#radioStreamCount) | **GET** /RadioStreams/count | Count instances of the model matched by where from the data source.
*RadioStreamApi* | [**radioStreamCreate**](docs/RadioStreamApi.md#radioStreamCreate) | **POST** /RadioStreams | Create a new instance of the model and persist it into the data source.
*RadioStreamApi* | [**radioStreamCreateChangeStreamGetRadioStreamsChangeStream**](docs/RadioStreamApi.md#radioStreamCreateChangeStreamGetRadioStreamsChangeStream) | **GET** /RadioStreams/change-stream | Create a change stream.
*RadioStreamApi* | [**radioStreamCreateChangeStreamPostRadioStreamsChangeStream**](docs/RadioStreamApi.md#radioStreamCreateChangeStreamPostRadioStreamsChangeStream) | **POST** /RadioStreams/change-stream | Create a change stream.
*RadioStreamApi* | [**radioStreamDeleteById**](docs/RadioStreamApi.md#radioStreamDeleteById) | **DELETE** /RadioStreams/{id} | Delete a model instance by {{id}} from the data source.
*RadioStreamApi* | [**radioStreamExistsGetRadioStreamsidExists**](docs/RadioStreamApi.md#radioStreamExistsGetRadioStreamsidExists) | **GET** /RadioStreams/{id}/exists | Check whether a model instance exists in the data source.
*RadioStreamApi* | [**radioStreamExistsHeadRadioStreamsid**](docs/RadioStreamApi.md#radioStreamExistsHeadRadioStreamsid) | **HEAD** /RadioStreams/{id} | Check whether a model instance exists in the data source.
*RadioStreamApi* | [**radioStreamFind**](docs/RadioStreamApi.md#radioStreamFind) | **GET** /RadioStreams | Find all instances of the model matched by filter from the data source.
*RadioStreamApi* | [**radioStreamFindById**](docs/RadioStreamApi.md#radioStreamFindById) | **GET** /RadioStreams/{id} | Find a model instance by {{id}} from the data source.
*RadioStreamApi* | [**radioStreamFindOne**](docs/RadioStreamApi.md#radioStreamFindOne) | **GET** /RadioStreams/findOne | Find first instance of the model matched by filter from the data source.
*RadioStreamApi* | [**radioStreamPrototypeCountRadioShows**](docs/RadioStreamApi.md#radioStreamPrototypeCountRadioShows) | **GET** /RadioStreams/{id}/radioShows/count | Counts radioShows of RadioStream.
*RadioStreamApi* | [**radioStreamPrototypeCreateRadioShows**](docs/RadioStreamApi.md#radioStreamPrototypeCreateRadioShows) | **POST** /RadioStreams/{id}/radioShows | Creates a new instance in radioShows of this model.
*RadioStreamApi* | [**radioStreamPrototypeDeleteRadioShows**](docs/RadioStreamApi.md#radioStreamPrototypeDeleteRadioShows) | **DELETE** /RadioStreams/{id}/radioShows | Deletes all radioShows of this model.
*RadioStreamApi* | [**radioStreamPrototypeDestroyByIdRadioShows**](docs/RadioStreamApi.md#radioStreamPrototypeDestroyByIdRadioShows) | **DELETE** /RadioStreams/{id}/radioShows/{fk} | Delete a related item by id for radioShows.
*RadioStreamApi* | [**radioStreamPrototypeFindByIdRadioShows**](docs/RadioStreamApi.md#radioStreamPrototypeFindByIdRadioShows) | **GET** /RadioStreams/{id}/radioShows/{fk} | Find a related item by id for radioShows.
*RadioStreamApi* | [**radioStreamPrototypeGetRadioShows**](docs/RadioStreamApi.md#radioStreamPrototypeGetRadioShows) | **GET** /RadioStreams/{id}/radioShows | Queries radioShows of RadioStream.
*RadioStreamApi* | [**radioStreamPrototypeUpdateAttributesPatchRadioStreamsid**](docs/RadioStreamApi.md#radioStreamPrototypeUpdateAttributesPatchRadioStreamsid) | **PATCH** /RadioStreams/{id} | Patch attributes for a model instance and persist it into the data source.
*RadioStreamApi* | [**radioStreamPrototypeUpdateAttributesPutRadioStreamsid**](docs/RadioStreamApi.md#radioStreamPrototypeUpdateAttributesPutRadioStreamsid) | **PUT** /RadioStreams/{id} | Patch attributes for a model instance and persist it into the data source.
*RadioStreamApi* | [**radioStreamPrototypeUpdateByIdRadioShows**](docs/RadioStreamApi.md#radioStreamPrototypeUpdateByIdRadioShows) | **PUT** /RadioStreams/{id}/radioShows/{fk} | Update a related item by id for radioShows.
*RadioStreamApi* | [**radioStreamRadioShowCurrent**](docs/RadioStreamApi.md#radioStreamRadioShowCurrent) | **GET** /RadioStreams/{id}/radioShows/current | 
*RadioStreamApi* | [**radioStreamRadioShowSchedule**](docs/RadioStreamApi.md#radioStreamRadioShowSchedule) | **GET** /RadioStreams/{id}/radioShows/schedule | 
*RadioStreamApi* | [**radioStreamReplaceById**](docs/RadioStreamApi.md#radioStreamReplaceById) | **POST** /RadioStreams/{id}/replace | Replace attributes for a model instance and persist it into the data source.
*RadioStreamApi* | [**radioStreamReplaceOrCreate**](docs/RadioStreamApi.md#radioStreamReplaceOrCreate) | **POST** /RadioStreams/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
*RadioStreamApi* | [**radioStreamUpdateAll**](docs/RadioStreamApi.md#radioStreamUpdateAll) | **POST** /RadioStreams/update | Update instances of the model matched by {{where}} from the data source.
*RadioStreamApi* | [**radioStreamUpsertPatchRadioStreams**](docs/RadioStreamApi.md#radioStreamUpsertPatchRadioStreams) | **PATCH** /RadioStreams | Patch an existing model instance or insert a new one into the data source.
*RadioStreamApi* | [**radioStreamUpsertPutRadioStreams**](docs/RadioStreamApi.md#radioStreamUpsertPutRadioStreams) | **PUT** /RadioStreams | Patch an existing model instance or insert a new one into the data source.
*RadioStreamApi* | [**radioStreamUpsertWithWhere**](docs/RadioStreamApi.md#radioStreamUpsertWithWhere) | **POST** /RadioStreams/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.
*RssFeedApi* | [**rssFeedCount**](docs/RssFeedApi.md#rssFeedCount) | **GET** /RssFeeds/count | Count instances of the model matched by where from the data source.
*RssFeedApi* | [**rssFeedCreate**](docs/RssFeedApi.md#rssFeedCreate) | **POST** /RssFeeds | Create a new instance of the model and persist it into the data source.
*RssFeedApi* | [**rssFeedCreateChangeStreamGetRssFeedsChangeStream**](docs/RssFeedApi.md#rssFeedCreateChangeStreamGetRssFeedsChangeStream) | **GET** /RssFeeds/change-stream | Create a change stream.
*RssFeedApi* | [**rssFeedCreateChangeStreamPostRssFeedsChangeStream**](docs/RssFeedApi.md#rssFeedCreateChangeStreamPostRssFeedsChangeStream) | **POST** /RssFeeds/change-stream | Create a change stream.
*RssFeedApi* | [**rssFeedDeleteById**](docs/RssFeedApi.md#rssFeedDeleteById) | **DELETE** /RssFeeds/{id} | Delete a model instance by {{id}} from the data source.
*RssFeedApi* | [**rssFeedExistsGetRssFeedsidExists**](docs/RssFeedApi.md#rssFeedExistsGetRssFeedsidExists) | **GET** /RssFeeds/{id}/exists | Check whether a model instance exists in the data source.
*RssFeedApi* | [**rssFeedExistsHeadRssFeedsid**](docs/RssFeedApi.md#rssFeedExistsHeadRssFeedsid) | **HEAD** /RssFeeds/{id} | Check whether a model instance exists in the data source.
*RssFeedApi* | [**rssFeedFind**](docs/RssFeedApi.md#rssFeedFind) | **GET** /RssFeeds | Find all instances of the model matched by filter from the data source.
*RssFeedApi* | [**rssFeedFindById**](docs/RssFeedApi.md#rssFeedFindById) | **GET** /RssFeeds/{id} | Find a model instance by {{id}} from the data source.
*RssFeedApi* | [**rssFeedFindOne**](docs/RssFeedApi.md#rssFeedFindOne) | **GET** /RssFeeds/findOne | Find first instance of the model matched by filter from the data source.
*RssFeedApi* | [**rssFeedPrototypeGetRssFeedGroup**](docs/RssFeedApi.md#rssFeedPrototypeGetRssFeedGroup) | **GET** /RssFeeds/{id}/rssFeedGroup | Fetches belongsTo relation rssFeedGroup.
*RssFeedApi* | [**rssFeedPrototypeUpdateAttributesPatchRssFeedsid**](docs/RssFeedApi.md#rssFeedPrototypeUpdateAttributesPatchRssFeedsid) | **PATCH** /RssFeeds/{id} | Patch attributes for a model instance and persist it into the data source.
*RssFeedApi* | [**rssFeedPrototypeUpdateAttributesPutRssFeedsid**](docs/RssFeedApi.md#rssFeedPrototypeUpdateAttributesPutRssFeedsid) | **PUT** /RssFeeds/{id} | Patch attributes for a model instance and persist it into the data source.
*RssFeedApi* | [**rssFeedReplaceById**](docs/RssFeedApi.md#rssFeedReplaceById) | **POST** /RssFeeds/{id}/replace | Replace attributes for a model instance and persist it into the data source.
*RssFeedApi* | [**rssFeedReplaceOrCreate**](docs/RssFeedApi.md#rssFeedReplaceOrCreate) | **POST** /RssFeeds/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
*RssFeedApi* | [**rssFeedUpdateAll**](docs/RssFeedApi.md#rssFeedUpdateAll) | **POST** /RssFeeds/update | Update instances of the model matched by {{where}} from the data source.
*RssFeedApi* | [**rssFeedUpsertPatchRssFeeds**](docs/RssFeedApi.md#rssFeedUpsertPatchRssFeeds) | **PATCH** /RssFeeds | Patch an existing model instance or insert a new one into the data source.
*RssFeedApi* | [**rssFeedUpsertPutRssFeeds**](docs/RssFeedApi.md#rssFeedUpsertPutRssFeeds) | **PUT** /RssFeeds | Patch an existing model instance or insert a new one into the data source.
*RssFeedApi* | [**rssFeedUpsertWithWhere**](docs/RssFeedApi.md#rssFeedUpsertWithWhere) | **POST** /RssFeeds/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.
*RssFeedGroupApi* | [**rssFeedGroupCount**](docs/RssFeedGroupApi.md#rssFeedGroupCount) | **GET** /RssFeedGroups/count | Count instances of the model matched by where from the data source.
*RssFeedGroupApi* | [**rssFeedGroupCreate**](docs/RssFeedGroupApi.md#rssFeedGroupCreate) | **POST** /RssFeedGroups | Create a new instance of the model and persist it into the data source.
*RssFeedGroupApi* | [**rssFeedGroupCreateChangeStreamGetRssFeedGroupsChangeStream**](docs/RssFeedGroupApi.md#rssFeedGroupCreateChangeStreamGetRssFeedGroupsChangeStream) | **GET** /RssFeedGroups/change-stream | Create a change stream.
*RssFeedGroupApi* | [**rssFeedGroupCreateChangeStreamPostRssFeedGroupsChangeStream**](docs/RssFeedGroupApi.md#rssFeedGroupCreateChangeStreamPostRssFeedGroupsChangeStream) | **POST** /RssFeedGroups/change-stream | Create a change stream.
*RssFeedGroupApi* | [**rssFeedGroupDeleteById**](docs/RssFeedGroupApi.md#rssFeedGroupDeleteById) | **DELETE** /RssFeedGroups/{id} | Delete a model instance by {{id}} from the data source.
*RssFeedGroupApi* | [**rssFeedGroupExistsGetRssFeedGroupsidExists**](docs/RssFeedGroupApi.md#rssFeedGroupExistsGetRssFeedGroupsidExists) | **GET** /RssFeedGroups/{id}/exists | Check whether a model instance exists in the data source.
*RssFeedGroupApi* | [**rssFeedGroupExistsHeadRssFeedGroupsid**](docs/RssFeedGroupApi.md#rssFeedGroupExistsHeadRssFeedGroupsid) | **HEAD** /RssFeedGroups/{id} | Check whether a model instance exists in the data source.
*RssFeedGroupApi* | [**rssFeedGroupFind**](docs/RssFeedGroupApi.md#rssFeedGroupFind) | **GET** /RssFeedGroups | Find all instances of the model matched by filter from the data source.
*RssFeedGroupApi* | [**rssFeedGroupFindById**](docs/RssFeedGroupApi.md#rssFeedGroupFindById) | **GET** /RssFeedGroups/{id} | Find a model instance by {{id}} from the data source.
*RssFeedGroupApi* | [**rssFeedGroupFindOne**](docs/RssFeedGroupApi.md#rssFeedGroupFindOne) | **GET** /RssFeedGroups/findOne | Find first instance of the model matched by filter from the data source.
*RssFeedGroupApi* | [**rssFeedGroupPrototypeCountRssFeeds**](docs/RssFeedGroupApi.md#rssFeedGroupPrototypeCountRssFeeds) | **GET** /RssFeedGroups/{id}/rssFeeds/count | Counts rssFeeds of RssFeedGroup.
*RssFeedGroupApi* | [**rssFeedGroupPrototypeCreateRssFeeds**](docs/RssFeedGroupApi.md#rssFeedGroupPrototypeCreateRssFeeds) | **POST** /RssFeedGroups/{id}/rssFeeds | Creates a new instance in rssFeeds of this model.
*RssFeedGroupApi* | [**rssFeedGroupPrototypeDeleteRssFeeds**](docs/RssFeedGroupApi.md#rssFeedGroupPrototypeDeleteRssFeeds) | **DELETE** /RssFeedGroups/{id}/rssFeeds | Deletes all rssFeeds of this model.
*RssFeedGroupApi* | [**rssFeedGroupPrototypeDestroyByIdRssFeeds**](docs/RssFeedGroupApi.md#rssFeedGroupPrototypeDestroyByIdRssFeeds) | **DELETE** /RssFeedGroups/{id}/rssFeeds/{fk} | Delete a related item by id for rssFeeds.
*RssFeedGroupApi* | [**rssFeedGroupPrototypeFindByIdRssFeeds**](docs/RssFeedGroupApi.md#rssFeedGroupPrototypeFindByIdRssFeeds) | **GET** /RssFeedGroups/{id}/rssFeeds/{fk} | Find a related item by id for rssFeeds.
*RssFeedGroupApi* | [**rssFeedGroupPrototypeGetRssFeeds**](docs/RssFeedGroupApi.md#rssFeedGroupPrototypeGetRssFeeds) | **GET** /RssFeedGroups/{id}/rssFeeds | Queries rssFeeds of RssFeedGroup.
*RssFeedGroupApi* | [**rssFeedGroupPrototypeUpdateAttributesPatchRssFeedGroupsid**](docs/RssFeedGroupApi.md#rssFeedGroupPrototypeUpdateAttributesPatchRssFeedGroupsid) | **PATCH** /RssFeedGroups/{id} | Patch attributes for a model instance and persist it into the data source.
*RssFeedGroupApi* | [**rssFeedGroupPrototypeUpdateAttributesPutRssFeedGroupsid**](docs/RssFeedGroupApi.md#rssFeedGroupPrototypeUpdateAttributesPutRssFeedGroupsid) | **PUT** /RssFeedGroups/{id} | Patch attributes for a model instance and persist it into the data source.
*RssFeedGroupApi* | [**rssFeedGroupPrototypeUpdateByIdRssFeeds**](docs/RssFeedGroupApi.md#rssFeedGroupPrototypeUpdateByIdRssFeeds) | **PUT** /RssFeedGroups/{id}/rssFeeds/{fk} | Update a related item by id for rssFeeds.
*RssFeedGroupApi* | [**rssFeedGroupReplaceById**](docs/RssFeedGroupApi.md#rssFeedGroupReplaceById) | **POST** /RssFeedGroups/{id}/replace | Replace attributes for a model instance and persist it into the data source.
*RssFeedGroupApi* | [**rssFeedGroupReplaceOrCreate**](docs/RssFeedGroupApi.md#rssFeedGroupReplaceOrCreate) | **POST** /RssFeedGroups/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
*RssFeedGroupApi* | [**rssFeedGroupUpdateAll**](docs/RssFeedGroupApi.md#rssFeedGroupUpdateAll) | **POST** /RssFeedGroups/update | Update instances of the model matched by {{where}} from the data source.
*RssFeedGroupApi* | [**rssFeedGroupUpsertPatchRssFeedGroups**](docs/RssFeedGroupApi.md#rssFeedGroupUpsertPatchRssFeedGroups) | **PATCH** /RssFeedGroups | Patch an existing model instance or insert a new one into the data source.
*RssFeedGroupApi* | [**rssFeedGroupUpsertPutRssFeedGroups**](docs/RssFeedGroupApi.md#rssFeedGroupUpsertPutRssFeedGroups) | **PUT** /RssFeedGroups | Patch an existing model instance or insert a new one into the data source.
*RssFeedGroupApi* | [**rssFeedGroupUpsertWithWhere**](docs/RssFeedGroupApi.md#rssFeedGroupUpsertWithWhere) | **POST** /RssFeedGroups/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.
*StripeCouponApi* | [**stripeCouponCount**](docs/StripeCouponApi.md#stripeCouponCount) | **GET** /StripeCoupons/count | Count instances of the model matched by where from the data source.
*StripeCouponApi* | [**stripeCouponCreate**](docs/StripeCouponApi.md#stripeCouponCreate) | **POST** /StripeCoupons | Create a new instance of the model and persist it into the data source.
*StripeCouponApi* | [**stripeCouponCreateChangeStreamGetStripeCouponsChangeStream**](docs/StripeCouponApi.md#stripeCouponCreateChangeStreamGetStripeCouponsChangeStream) | **GET** /StripeCoupons/change-stream | Create a change stream.
*StripeCouponApi* | [**stripeCouponCreateChangeStreamPostStripeCouponsChangeStream**](docs/StripeCouponApi.md#stripeCouponCreateChangeStreamPostStripeCouponsChangeStream) | **POST** /StripeCoupons/change-stream | Create a change stream.
*StripeCouponApi* | [**stripeCouponDeleteById**](docs/StripeCouponApi.md#stripeCouponDeleteById) | **DELETE** /StripeCoupons/{id} | Delete a model instance by {{id}} from the data source.
*StripeCouponApi* | [**stripeCouponExistsGetStripeCouponsidExists**](docs/StripeCouponApi.md#stripeCouponExistsGetStripeCouponsidExists) | **GET** /StripeCoupons/{id}/exists | Check whether a model instance exists in the data source.
*StripeCouponApi* | [**stripeCouponExistsHeadStripeCouponsid**](docs/StripeCouponApi.md#stripeCouponExistsHeadStripeCouponsid) | **HEAD** /StripeCoupons/{id} | Check whether a model instance exists in the data source.
*StripeCouponApi* | [**stripeCouponFind**](docs/StripeCouponApi.md#stripeCouponFind) | **GET** /StripeCoupons | Find all instances of the model matched by filter from the data source.
*StripeCouponApi* | [**stripeCouponFindById**](docs/StripeCouponApi.md#stripeCouponFindById) | **GET** /StripeCoupons/{id} | Find a model instance by {{id}} from the data source.
*StripeCouponApi* | [**stripeCouponFindOne**](docs/StripeCouponApi.md#stripeCouponFindOne) | **GET** /StripeCoupons/findOne | Find first instance of the model matched by filter from the data source.
*StripeCouponApi* | [**stripeCouponPrototypeUpdateAttributesPatchStripeCouponsid**](docs/StripeCouponApi.md#stripeCouponPrototypeUpdateAttributesPatchStripeCouponsid) | **PATCH** /StripeCoupons/{id} | Patch attributes for a model instance and persist it into the data source.
*StripeCouponApi* | [**stripeCouponPrototypeUpdateAttributesPutStripeCouponsid**](docs/StripeCouponApi.md#stripeCouponPrototypeUpdateAttributesPutStripeCouponsid) | **PUT** /StripeCoupons/{id} | Patch attributes for a model instance and persist it into the data source.
*StripeCouponApi* | [**stripeCouponReplaceById**](docs/StripeCouponApi.md#stripeCouponReplaceById) | **POST** /StripeCoupons/{id}/replace | Replace attributes for a model instance and persist it into the data source.
*StripeCouponApi* | [**stripeCouponReplaceOrCreate**](docs/StripeCouponApi.md#stripeCouponReplaceOrCreate) | **POST** /StripeCoupons/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
*StripeCouponApi* | [**stripeCouponUpdateAll**](docs/StripeCouponApi.md#stripeCouponUpdateAll) | **POST** /StripeCoupons/update | Update instances of the model matched by {{where}} from the data source.
*StripeCouponApi* | [**stripeCouponUpsertPatchStripeCoupons**](docs/StripeCouponApi.md#stripeCouponUpsertPatchStripeCoupons) | **PATCH** /StripeCoupons | Patch an existing model instance or insert a new one into the data source.
*StripeCouponApi* | [**stripeCouponUpsertPutStripeCoupons**](docs/StripeCouponApi.md#stripeCouponUpsertPutStripeCoupons) | **PUT** /StripeCoupons | Patch an existing model instance or insert a new one into the data source.
*StripeCouponApi* | [**stripeCouponUpsertWithWhere**](docs/StripeCouponApi.md#stripeCouponUpsertWithWhere) | **POST** /StripeCoupons/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.
*StripeDiscountApi* | [**stripeDiscountCount**](docs/StripeDiscountApi.md#stripeDiscountCount) | **GET** /StripeDiscounts/count | Count instances of the model matched by where from the data source.
*StripeDiscountApi* | [**stripeDiscountCreate**](docs/StripeDiscountApi.md#stripeDiscountCreate) | **POST** /StripeDiscounts | Create a new instance of the model and persist it into the data source.
*StripeDiscountApi* | [**stripeDiscountCreateChangeStreamGetStripeDiscountsChangeStream**](docs/StripeDiscountApi.md#stripeDiscountCreateChangeStreamGetStripeDiscountsChangeStream) | **GET** /StripeDiscounts/change-stream | Create a change stream.
*StripeDiscountApi* | [**stripeDiscountCreateChangeStreamPostStripeDiscountsChangeStream**](docs/StripeDiscountApi.md#stripeDiscountCreateChangeStreamPostStripeDiscountsChangeStream) | **POST** /StripeDiscounts/change-stream | Create a change stream.
*StripeDiscountApi* | [**stripeDiscountDeleteById**](docs/StripeDiscountApi.md#stripeDiscountDeleteById) | **DELETE** /StripeDiscounts/{id} | Delete a model instance by {{id}} from the data source.
*StripeDiscountApi* | [**stripeDiscountExistsGetStripeDiscountsidExists**](docs/StripeDiscountApi.md#stripeDiscountExistsGetStripeDiscountsidExists) | **GET** /StripeDiscounts/{id}/exists | Check whether a model instance exists in the data source.
*StripeDiscountApi* | [**stripeDiscountExistsHeadStripeDiscountsid**](docs/StripeDiscountApi.md#stripeDiscountExistsHeadStripeDiscountsid) | **HEAD** /StripeDiscounts/{id} | Check whether a model instance exists in the data source.
*StripeDiscountApi* | [**stripeDiscountFind**](docs/StripeDiscountApi.md#stripeDiscountFind) | **GET** /StripeDiscounts | Find all instances of the model matched by filter from the data source.
*StripeDiscountApi* | [**stripeDiscountFindById**](docs/StripeDiscountApi.md#stripeDiscountFindById) | **GET** /StripeDiscounts/{id} | Find a model instance by {{id}} from the data source.
*StripeDiscountApi* | [**stripeDiscountFindOne**](docs/StripeDiscountApi.md#stripeDiscountFindOne) | **GET** /StripeDiscounts/findOne | Find first instance of the model matched by filter from the data source.
*StripeDiscountApi* | [**stripeDiscountPrototypeUpdateAttributesPatchStripeDiscountsid**](docs/StripeDiscountApi.md#stripeDiscountPrototypeUpdateAttributesPatchStripeDiscountsid) | **PATCH** /StripeDiscounts/{id} | Patch attributes for a model instance and persist it into the data source.
*StripeDiscountApi* | [**stripeDiscountPrototypeUpdateAttributesPutStripeDiscountsid**](docs/StripeDiscountApi.md#stripeDiscountPrototypeUpdateAttributesPutStripeDiscountsid) | **PUT** /StripeDiscounts/{id} | Patch attributes for a model instance and persist it into the data source.
*StripeDiscountApi* | [**stripeDiscountReplaceById**](docs/StripeDiscountApi.md#stripeDiscountReplaceById) | **POST** /StripeDiscounts/{id}/replace | Replace attributes for a model instance and persist it into the data source.
*StripeDiscountApi* | [**stripeDiscountReplaceOrCreate**](docs/StripeDiscountApi.md#stripeDiscountReplaceOrCreate) | **POST** /StripeDiscounts/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
*StripeDiscountApi* | [**stripeDiscountUpdateAll**](docs/StripeDiscountApi.md#stripeDiscountUpdateAll) | **POST** /StripeDiscounts/update | Update instances of the model matched by {{where}} from the data source.
*StripeDiscountApi* | [**stripeDiscountUpsertPatchStripeDiscounts**](docs/StripeDiscountApi.md#stripeDiscountUpsertPatchStripeDiscounts) | **PATCH** /StripeDiscounts | Patch an existing model instance or insert a new one into the data source.
*StripeDiscountApi* | [**stripeDiscountUpsertPutStripeDiscounts**](docs/StripeDiscountApi.md#stripeDiscountUpsertPutStripeDiscounts) | **PUT** /StripeDiscounts | Patch an existing model instance or insert a new one into the data source.
*StripeDiscountApi* | [**stripeDiscountUpsertWithWhere**](docs/StripeDiscountApi.md#stripeDiscountUpsertWithWhere) | **POST** /StripeDiscounts/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.
*StripeInvoiceApi* | [**stripeInvoiceCount**](docs/StripeInvoiceApi.md#stripeInvoiceCount) | **GET** /StripeInvoices/count | Count instances of the model matched by where from the data source.
*StripeInvoiceApi* | [**stripeInvoiceCreate**](docs/StripeInvoiceApi.md#stripeInvoiceCreate) | **POST** /StripeInvoices | Create a new instance of the model and persist it into the data source.
*StripeInvoiceApi* | [**stripeInvoiceCreateChangeStreamGetStripeInvoicesChangeStream**](docs/StripeInvoiceApi.md#stripeInvoiceCreateChangeStreamGetStripeInvoicesChangeStream) | **GET** /StripeInvoices/change-stream | Create a change stream.
*StripeInvoiceApi* | [**stripeInvoiceCreateChangeStreamPostStripeInvoicesChangeStream**](docs/StripeInvoiceApi.md#stripeInvoiceCreateChangeStreamPostStripeInvoicesChangeStream) | **POST** /StripeInvoices/change-stream | Create a change stream.
*StripeInvoiceApi* | [**stripeInvoiceDeleteById**](docs/StripeInvoiceApi.md#stripeInvoiceDeleteById) | **DELETE** /StripeInvoices/{id} | Delete a model instance by {{id}} from the data source.
*StripeInvoiceApi* | [**stripeInvoiceExistsGetStripeInvoicesidExists**](docs/StripeInvoiceApi.md#stripeInvoiceExistsGetStripeInvoicesidExists) | **GET** /StripeInvoices/{id}/exists | Check whether a model instance exists in the data source.
*StripeInvoiceApi* | [**stripeInvoiceExistsHeadStripeInvoicesid**](docs/StripeInvoiceApi.md#stripeInvoiceExistsHeadStripeInvoicesid) | **HEAD** /StripeInvoices/{id} | Check whether a model instance exists in the data source.
*StripeInvoiceApi* | [**stripeInvoiceFind**](docs/StripeInvoiceApi.md#stripeInvoiceFind) | **GET** /StripeInvoices | Find all instances of the model matched by filter from the data source.
*StripeInvoiceApi* | [**stripeInvoiceFindById**](docs/StripeInvoiceApi.md#stripeInvoiceFindById) | **GET** /StripeInvoices/{id} | Find a model instance by {{id}} from the data source.
*StripeInvoiceApi* | [**stripeInvoiceFindOne**](docs/StripeInvoiceApi.md#stripeInvoiceFindOne) | **GET** /StripeInvoices/findOne | Find first instance of the model matched by filter from the data source.
*StripeInvoiceApi* | [**stripeInvoicePrototypeUpdateAttributesPatchStripeInvoicesid**](docs/StripeInvoiceApi.md#stripeInvoicePrototypeUpdateAttributesPatchStripeInvoicesid) | **PATCH** /StripeInvoices/{id} | Patch attributes for a model instance and persist it into the data source.
*StripeInvoiceApi* | [**stripeInvoicePrototypeUpdateAttributesPutStripeInvoicesid**](docs/StripeInvoiceApi.md#stripeInvoicePrototypeUpdateAttributesPutStripeInvoicesid) | **PUT** /StripeInvoices/{id} | Patch attributes for a model instance and persist it into the data source.
*StripeInvoiceApi* | [**stripeInvoiceReplaceById**](docs/StripeInvoiceApi.md#stripeInvoiceReplaceById) | **POST** /StripeInvoices/{id}/replace | Replace attributes for a model instance and persist it into the data source.
*StripeInvoiceApi* | [**stripeInvoiceReplaceOrCreate**](docs/StripeInvoiceApi.md#stripeInvoiceReplaceOrCreate) | **POST** /StripeInvoices/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
*StripeInvoiceApi* | [**stripeInvoiceUpdateAll**](docs/StripeInvoiceApi.md#stripeInvoiceUpdateAll) | **POST** /StripeInvoices/update | Update instances of the model matched by {{where}} from the data source.
*StripeInvoiceApi* | [**stripeInvoiceUpsertPatchStripeInvoices**](docs/StripeInvoiceApi.md#stripeInvoiceUpsertPatchStripeInvoices) | **PATCH** /StripeInvoices | Patch an existing model instance or insert a new one into the data source.
*StripeInvoiceApi* | [**stripeInvoiceUpsertPutStripeInvoices**](docs/StripeInvoiceApi.md#stripeInvoiceUpsertPutStripeInvoices) | **PUT** /StripeInvoices | Patch an existing model instance or insert a new one into the data source.
*StripeInvoiceApi* | [**stripeInvoiceUpsertWithWhere**](docs/StripeInvoiceApi.md#stripeInvoiceUpsertWithWhere) | **POST** /StripeInvoices/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.
*StripeInvoiceItemApi* | [**stripeInvoiceItemCount**](docs/StripeInvoiceItemApi.md#stripeInvoiceItemCount) | **GET** /StripeInvoiceItems/count | Count instances of the model matched by where from the data source.
*StripeInvoiceItemApi* | [**stripeInvoiceItemCreate**](docs/StripeInvoiceItemApi.md#stripeInvoiceItemCreate) | **POST** /StripeInvoiceItems | Create a new instance of the model and persist it into the data source.
*StripeInvoiceItemApi* | [**stripeInvoiceItemCreateChangeStreamGetStripeInvoiceItemsChangeStream**](docs/StripeInvoiceItemApi.md#stripeInvoiceItemCreateChangeStreamGetStripeInvoiceItemsChangeStream) | **GET** /StripeInvoiceItems/change-stream | Create a change stream.
*StripeInvoiceItemApi* | [**stripeInvoiceItemCreateChangeStreamPostStripeInvoiceItemsChangeStream**](docs/StripeInvoiceItemApi.md#stripeInvoiceItemCreateChangeStreamPostStripeInvoiceItemsChangeStream) | **POST** /StripeInvoiceItems/change-stream | Create a change stream.
*StripeInvoiceItemApi* | [**stripeInvoiceItemDeleteById**](docs/StripeInvoiceItemApi.md#stripeInvoiceItemDeleteById) | **DELETE** /StripeInvoiceItems/{id} | Delete a model instance by {{id}} from the data source.
*StripeInvoiceItemApi* | [**stripeInvoiceItemExistsGetStripeInvoiceItemsidExists**](docs/StripeInvoiceItemApi.md#stripeInvoiceItemExistsGetStripeInvoiceItemsidExists) | **GET** /StripeInvoiceItems/{id}/exists | Check whether a model instance exists in the data source.
*StripeInvoiceItemApi* | [**stripeInvoiceItemExistsHeadStripeInvoiceItemsid**](docs/StripeInvoiceItemApi.md#stripeInvoiceItemExistsHeadStripeInvoiceItemsid) | **HEAD** /StripeInvoiceItems/{id} | Check whether a model instance exists in the data source.
*StripeInvoiceItemApi* | [**stripeInvoiceItemFind**](docs/StripeInvoiceItemApi.md#stripeInvoiceItemFind) | **GET** /StripeInvoiceItems | Find all instances of the model matched by filter from the data source.
*StripeInvoiceItemApi* | [**stripeInvoiceItemFindById**](docs/StripeInvoiceItemApi.md#stripeInvoiceItemFindById) | **GET** /StripeInvoiceItems/{id} | Find a model instance by {{id}} from the data source.
*StripeInvoiceItemApi* | [**stripeInvoiceItemFindOne**](docs/StripeInvoiceItemApi.md#stripeInvoiceItemFindOne) | **GET** /StripeInvoiceItems/findOne | Find first instance of the model matched by filter from the data source.
*StripeInvoiceItemApi* | [**stripeInvoiceItemPrototypeUpdateAttributesPatchStripeInvoiceItemsid**](docs/StripeInvoiceItemApi.md#stripeInvoiceItemPrototypeUpdateAttributesPatchStripeInvoiceItemsid) | **PATCH** /StripeInvoiceItems/{id} | Patch attributes for a model instance and persist it into the data source.
*StripeInvoiceItemApi* | [**stripeInvoiceItemPrototypeUpdateAttributesPutStripeInvoiceItemsid**](docs/StripeInvoiceItemApi.md#stripeInvoiceItemPrototypeUpdateAttributesPutStripeInvoiceItemsid) | **PUT** /StripeInvoiceItems/{id} | Patch attributes for a model instance and persist it into the data source.
*StripeInvoiceItemApi* | [**stripeInvoiceItemReplaceById**](docs/StripeInvoiceItemApi.md#stripeInvoiceItemReplaceById) | **POST** /StripeInvoiceItems/{id}/replace | Replace attributes for a model instance and persist it into the data source.
*StripeInvoiceItemApi* | [**stripeInvoiceItemReplaceOrCreate**](docs/StripeInvoiceItemApi.md#stripeInvoiceItemReplaceOrCreate) | **POST** /StripeInvoiceItems/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
*StripeInvoiceItemApi* | [**stripeInvoiceItemUpdateAll**](docs/StripeInvoiceItemApi.md#stripeInvoiceItemUpdateAll) | **POST** /StripeInvoiceItems/update | Update instances of the model matched by {{where}} from the data source.
*StripeInvoiceItemApi* | [**stripeInvoiceItemUpsertPatchStripeInvoiceItems**](docs/StripeInvoiceItemApi.md#stripeInvoiceItemUpsertPatchStripeInvoiceItems) | **PATCH** /StripeInvoiceItems | Patch an existing model instance or insert a new one into the data source.
*StripeInvoiceItemApi* | [**stripeInvoiceItemUpsertPutStripeInvoiceItems**](docs/StripeInvoiceItemApi.md#stripeInvoiceItemUpsertPutStripeInvoiceItems) | **PUT** /StripeInvoiceItems | Patch an existing model instance or insert a new one into the data source.
*StripeInvoiceItemApi* | [**stripeInvoiceItemUpsertWithWhere**](docs/StripeInvoiceItemApi.md#stripeInvoiceItemUpsertWithWhere) | **POST** /StripeInvoiceItems/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.
*StripePlanApi* | [**stripePlanCount**](docs/StripePlanApi.md#stripePlanCount) | **GET** /StripePlans/count | Count instances of the model matched by where from the data source.
*StripePlanApi* | [**stripePlanCreate**](docs/StripePlanApi.md#stripePlanCreate) | **POST** /StripePlans | Create a new instance of the model and persist it into the data source.
*StripePlanApi* | [**stripePlanCreateChangeStreamGetStripePlansChangeStream**](docs/StripePlanApi.md#stripePlanCreateChangeStreamGetStripePlansChangeStream) | **GET** /StripePlans/change-stream | Create a change stream.
*StripePlanApi* | [**stripePlanCreateChangeStreamPostStripePlansChangeStream**](docs/StripePlanApi.md#stripePlanCreateChangeStreamPostStripePlansChangeStream) | **POST** /StripePlans/change-stream | Create a change stream.
*StripePlanApi* | [**stripePlanDeleteById**](docs/StripePlanApi.md#stripePlanDeleteById) | **DELETE** /StripePlans/{id} | Delete a model instance by {{id}} from the data source.
*StripePlanApi* | [**stripePlanExistsGetStripePlansidExists**](docs/StripePlanApi.md#stripePlanExistsGetStripePlansidExists) | **GET** /StripePlans/{id}/exists | Check whether a model instance exists in the data source.
*StripePlanApi* | [**stripePlanExistsHeadStripePlansid**](docs/StripePlanApi.md#stripePlanExistsHeadStripePlansid) | **HEAD** /StripePlans/{id} | Check whether a model instance exists in the data source.
*StripePlanApi* | [**stripePlanFind**](docs/StripePlanApi.md#stripePlanFind) | **GET** /StripePlans | Find all instances of the model matched by filter from the data source.
*StripePlanApi* | [**stripePlanFindById**](docs/StripePlanApi.md#stripePlanFindById) | **GET** /StripePlans/{id} | Find a model instance by {{id}} from the data source.
*StripePlanApi* | [**stripePlanFindOne**](docs/StripePlanApi.md#stripePlanFindOne) | **GET** /StripePlans/findOne | Find first instance of the model matched by filter from the data source.
*StripePlanApi* | [**stripePlanPrototypeUpdateAttributesPatchStripePlansid**](docs/StripePlanApi.md#stripePlanPrototypeUpdateAttributesPatchStripePlansid) | **PATCH** /StripePlans/{id} | Patch attributes for a model instance and persist it into the data source.
*StripePlanApi* | [**stripePlanPrototypeUpdateAttributesPutStripePlansid**](docs/StripePlanApi.md#stripePlanPrototypeUpdateAttributesPutStripePlansid) | **PUT** /StripePlans/{id} | Patch attributes for a model instance and persist it into the data source.
*StripePlanApi* | [**stripePlanReplaceById**](docs/StripePlanApi.md#stripePlanReplaceById) | **POST** /StripePlans/{id}/replace | Replace attributes for a model instance and persist it into the data source.
*StripePlanApi* | [**stripePlanReplaceOrCreate**](docs/StripePlanApi.md#stripePlanReplaceOrCreate) | **POST** /StripePlans/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
*StripePlanApi* | [**stripePlanUpdateAll**](docs/StripePlanApi.md#stripePlanUpdateAll) | **POST** /StripePlans/update | Update instances of the model matched by {{where}} from the data source.
*StripePlanApi* | [**stripePlanUpsertPatchStripePlans**](docs/StripePlanApi.md#stripePlanUpsertPatchStripePlans) | **PATCH** /StripePlans | Patch an existing model instance or insert a new one into the data source.
*StripePlanApi* | [**stripePlanUpsertPutStripePlans**](docs/StripePlanApi.md#stripePlanUpsertPutStripePlans) | **PUT** /StripePlans | Patch an existing model instance or insert a new one into the data source.
*StripePlanApi* | [**stripePlanUpsertWithWhere**](docs/StripePlanApi.md#stripePlanUpsertWithWhere) | **POST** /StripePlans/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.
*StripeSubscriptionApi* | [**stripeSubscriptionCount**](docs/StripeSubscriptionApi.md#stripeSubscriptionCount) | **GET** /StripeSubscriptions/count | Count instances of the model matched by where from the data source.
*StripeSubscriptionApi* | [**stripeSubscriptionCreate**](docs/StripeSubscriptionApi.md#stripeSubscriptionCreate) | **POST** /StripeSubscriptions | Create a new instance of the model and persist it into the data source.
*StripeSubscriptionApi* | [**stripeSubscriptionCreateChangeStreamGetStripeSubscriptionsChangeStream**](docs/StripeSubscriptionApi.md#stripeSubscriptionCreateChangeStreamGetStripeSubscriptionsChangeStream) | **GET** /StripeSubscriptions/change-stream | Create a change stream.
*StripeSubscriptionApi* | [**stripeSubscriptionCreateChangeStreamPostStripeSubscriptionsChangeStream**](docs/StripeSubscriptionApi.md#stripeSubscriptionCreateChangeStreamPostStripeSubscriptionsChangeStream) | **POST** /StripeSubscriptions/change-stream | Create a change stream.
*StripeSubscriptionApi* | [**stripeSubscriptionDeleteById**](docs/StripeSubscriptionApi.md#stripeSubscriptionDeleteById) | **DELETE** /StripeSubscriptions/{id} | Delete a model instance by {{id}} from the data source.
*StripeSubscriptionApi* | [**stripeSubscriptionExistsGetStripeSubscriptionsidExists**](docs/StripeSubscriptionApi.md#stripeSubscriptionExistsGetStripeSubscriptionsidExists) | **GET** /StripeSubscriptions/{id}/exists | Check whether a model instance exists in the data source.
*StripeSubscriptionApi* | [**stripeSubscriptionExistsHeadStripeSubscriptionsid**](docs/StripeSubscriptionApi.md#stripeSubscriptionExistsHeadStripeSubscriptionsid) | **HEAD** /StripeSubscriptions/{id} | Check whether a model instance exists in the data source.
*StripeSubscriptionApi* | [**stripeSubscriptionFind**](docs/StripeSubscriptionApi.md#stripeSubscriptionFind) | **GET** /StripeSubscriptions | Find all instances of the model matched by filter from the data source.
*StripeSubscriptionApi* | [**stripeSubscriptionFindById**](docs/StripeSubscriptionApi.md#stripeSubscriptionFindById) | **GET** /StripeSubscriptions/{id} | Find a model instance by {{id}} from the data source.
*StripeSubscriptionApi* | [**stripeSubscriptionFindOne**](docs/StripeSubscriptionApi.md#stripeSubscriptionFindOne) | **GET** /StripeSubscriptions/findOne | Find first instance of the model matched by filter from the data source.
*StripeSubscriptionApi* | [**stripeSubscriptionPrototypeUpdateAttributesPatchStripeSubscriptionsid**](docs/StripeSubscriptionApi.md#stripeSubscriptionPrototypeUpdateAttributesPatchStripeSubscriptionsid) | **PATCH** /StripeSubscriptions/{id} | Patch attributes for a model instance and persist it into the data source.
*StripeSubscriptionApi* | [**stripeSubscriptionPrototypeUpdateAttributesPutStripeSubscriptionsid**](docs/StripeSubscriptionApi.md#stripeSubscriptionPrototypeUpdateAttributesPutStripeSubscriptionsid) | **PUT** /StripeSubscriptions/{id} | Patch attributes for a model instance and persist it into the data source.
*StripeSubscriptionApi* | [**stripeSubscriptionReplaceById**](docs/StripeSubscriptionApi.md#stripeSubscriptionReplaceById) | **POST** /StripeSubscriptions/{id}/replace | Replace attributes for a model instance and persist it into the data source.
*StripeSubscriptionApi* | [**stripeSubscriptionReplaceOrCreate**](docs/StripeSubscriptionApi.md#stripeSubscriptionReplaceOrCreate) | **POST** /StripeSubscriptions/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
*StripeSubscriptionApi* | [**stripeSubscriptionUpdateAll**](docs/StripeSubscriptionApi.md#stripeSubscriptionUpdateAll) | **POST** /StripeSubscriptions/update | Update instances of the model matched by {{where}} from the data source.
*StripeSubscriptionApi* | [**stripeSubscriptionUpsertPatchStripeSubscriptions**](docs/StripeSubscriptionApi.md#stripeSubscriptionUpsertPatchStripeSubscriptions) | **PATCH** /StripeSubscriptions | Patch an existing model instance or insert a new one into the data source.
*StripeSubscriptionApi* | [**stripeSubscriptionUpsertPutStripeSubscriptions**](docs/StripeSubscriptionApi.md#stripeSubscriptionUpsertPutStripeSubscriptions) | **PUT** /StripeSubscriptions | Patch an existing model instance or insert a new one into the data source.
*StripeSubscriptionApi* | [**stripeSubscriptionUpsertWithWhere**](docs/StripeSubscriptionApi.md#stripeSubscriptionUpsertWithWhere) | **POST** /StripeSubscriptions/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.
*UdropshipVendorApi* | [**udropshipVendorCount**](docs/UdropshipVendorApi.md#udropshipVendorCount) | **GET** /UdropshipVendors/count | Count instances of the model matched by where from the data source.
*UdropshipVendorApi* | [**udropshipVendorCreate**](docs/UdropshipVendorApi.md#udropshipVendorCreate) | **POST** /UdropshipVendors | Create a new instance of the model and persist it into the data source.
*UdropshipVendorApi* | [**udropshipVendorCreateChangeStreamGetUdropshipVendorsChangeStream**](docs/UdropshipVendorApi.md#udropshipVendorCreateChangeStreamGetUdropshipVendorsChangeStream) | **GET** /UdropshipVendors/change-stream | Create a change stream.
*UdropshipVendorApi* | [**udropshipVendorCreateChangeStreamPostUdropshipVendorsChangeStream**](docs/UdropshipVendorApi.md#udropshipVendorCreateChangeStreamPostUdropshipVendorsChangeStream) | **POST** /UdropshipVendors/change-stream | Create a change stream.
*UdropshipVendorApi* | [**udropshipVendorDeleteById**](docs/UdropshipVendorApi.md#udropshipVendorDeleteById) | **DELETE** /UdropshipVendors/{id} | Delete a model instance by {{id}} from the data source.
*UdropshipVendorApi* | [**udropshipVendorExistsGetUdropshipVendorsidExists**](docs/UdropshipVendorApi.md#udropshipVendorExistsGetUdropshipVendorsidExists) | **GET** /UdropshipVendors/{id}/exists | Check whether a model instance exists in the data source.
*UdropshipVendorApi* | [**udropshipVendorExistsHeadUdropshipVendorsid**](docs/UdropshipVendorApi.md#udropshipVendorExistsHeadUdropshipVendorsid) | **HEAD** /UdropshipVendors/{id} | Check whether a model instance exists in the data source.
*UdropshipVendorApi* | [**udropshipVendorFind**](docs/UdropshipVendorApi.md#udropshipVendorFind) | **GET** /UdropshipVendors | Find all instances of the model matched by filter from the data source.
*UdropshipVendorApi* | [**udropshipVendorFindById**](docs/UdropshipVendorApi.md#udropshipVendorFindById) | **GET** /UdropshipVendors/{id} | Find a model instance by {{id}} from the data source.
*UdropshipVendorApi* | [**udropshipVendorFindOne**](docs/UdropshipVendorApi.md#udropshipVendorFindOne) | **GET** /UdropshipVendors/findOne | Find first instance of the model matched by filter from the data source.
*UdropshipVendorApi* | [**udropshipVendorPrototypeCountCatalogProducts**](docs/UdropshipVendorApi.md#udropshipVendorPrototypeCountCatalogProducts) | **GET** /UdropshipVendors/{id}/catalogProducts/count | Counts catalogProducts of UdropshipVendor.
*UdropshipVendorApi* | [**udropshipVendorPrototypeCountHarpoonHpublicApplicationpartners**](docs/UdropshipVendorApi.md#udropshipVendorPrototypeCountHarpoonHpublicApplicationpartners) | **GET** /UdropshipVendors/{id}/harpoonHpublicApplicationpartners/count | Counts harpoonHpublicApplicationpartners of UdropshipVendor.
*UdropshipVendorApi* | [**udropshipVendorPrototypeCountHarpoonHpublicv12VendorAppCategories**](docs/UdropshipVendorApi.md#udropshipVendorPrototypeCountHarpoonHpublicv12VendorAppCategories) | **GET** /UdropshipVendors/{id}/harpoonHpublicv12VendorAppCategories/count | Counts harpoonHpublicv12VendorAppCategories of UdropshipVendor.
*UdropshipVendorApi* | [**udropshipVendorPrototypeCountPartners**](docs/UdropshipVendorApi.md#udropshipVendorPrototypeCountPartners) | **GET** /UdropshipVendors/{id}/partners/count | Counts partners of UdropshipVendor.
*UdropshipVendorApi* | [**udropshipVendorPrototypeCountUdropshipVendorProducts**](docs/UdropshipVendorApi.md#udropshipVendorPrototypeCountUdropshipVendorProducts) | **GET** /UdropshipVendors/{id}/udropshipVendorProducts/count | Counts udropshipVendorProducts of UdropshipVendor.
*UdropshipVendorApi* | [**udropshipVendorPrototypeCountVendorLocations**](docs/UdropshipVendorApi.md#udropshipVendorPrototypeCountVendorLocations) | **GET** /UdropshipVendors/{id}/vendorLocations/count | Counts vendorLocations of UdropshipVendor.
*UdropshipVendorApi* | [**udropshipVendorPrototypeCreateCatalogProducts**](docs/UdropshipVendorApi.md#udropshipVendorPrototypeCreateCatalogProducts) | **POST** /UdropshipVendors/{id}/catalogProducts | Creates a new instance in catalogProducts of this model.
*UdropshipVendorApi* | [**udropshipVendorPrototypeCreateConfig**](docs/UdropshipVendorApi.md#udropshipVendorPrototypeCreateConfig) | **POST** /UdropshipVendors/{id}/config | Creates a new instance in config of this model.
*UdropshipVendorApi* | [**udropshipVendorPrototypeCreateHarpoonHpublicApplicationpartners**](docs/UdropshipVendorApi.md#udropshipVendorPrototypeCreateHarpoonHpublicApplicationpartners) | **POST** /UdropshipVendors/{id}/harpoonHpublicApplicationpartners | Creates a new instance in harpoonHpublicApplicationpartners of this model.
*UdropshipVendorApi* | [**udropshipVendorPrototypeCreateHarpoonHpublicv12VendorAppCategories**](docs/UdropshipVendorApi.md#udropshipVendorPrototypeCreateHarpoonHpublicv12VendorAppCategories) | **POST** /UdropshipVendors/{id}/harpoonHpublicv12VendorAppCategories | Creates a new instance in harpoonHpublicv12VendorAppCategories of this model.
*UdropshipVendorApi* | [**udropshipVendorPrototypeCreatePartners**](docs/UdropshipVendorApi.md#udropshipVendorPrototypeCreatePartners) | **POST** /UdropshipVendors/{id}/partners | Creates a new instance in partners of this model.
*UdropshipVendorApi* | [**udropshipVendorPrototypeCreateUdropshipVendorProducts**](docs/UdropshipVendorApi.md#udropshipVendorPrototypeCreateUdropshipVendorProducts) | **POST** /UdropshipVendors/{id}/udropshipVendorProducts | Creates a new instance in udropshipVendorProducts of this model.
*UdropshipVendorApi* | [**udropshipVendorPrototypeCreateVendorLocations**](docs/UdropshipVendorApi.md#udropshipVendorPrototypeCreateVendorLocations) | **POST** /UdropshipVendors/{id}/vendorLocations | Creates a new instance in vendorLocations of this model.
*UdropshipVendorApi* | [**udropshipVendorPrototypeDeleteCatalogProducts**](docs/UdropshipVendorApi.md#udropshipVendorPrototypeDeleteCatalogProducts) | **DELETE** /UdropshipVendors/{id}/catalogProducts | Deletes all catalogProducts of this model.
*UdropshipVendorApi* | [**udropshipVendorPrototypeDeleteHarpoonHpublicApplicationpartners**](docs/UdropshipVendorApi.md#udropshipVendorPrototypeDeleteHarpoonHpublicApplicationpartners) | **DELETE** /UdropshipVendors/{id}/harpoonHpublicApplicationpartners | Deletes all harpoonHpublicApplicationpartners of this model.
*UdropshipVendorApi* | [**udropshipVendorPrototypeDeleteHarpoonHpublicv12VendorAppCategories**](docs/UdropshipVendorApi.md#udropshipVendorPrototypeDeleteHarpoonHpublicv12VendorAppCategories) | **DELETE** /UdropshipVendors/{id}/harpoonHpublicv12VendorAppCategories | Deletes all harpoonHpublicv12VendorAppCategories of this model.
*UdropshipVendorApi* | [**udropshipVendorPrototypeDeletePartners**](docs/UdropshipVendorApi.md#udropshipVendorPrototypeDeletePartners) | **DELETE** /UdropshipVendors/{id}/partners | Deletes all partners of this model.
*UdropshipVendorApi* | [**udropshipVendorPrototypeDeleteUdropshipVendorProducts**](docs/UdropshipVendorApi.md#udropshipVendorPrototypeDeleteUdropshipVendorProducts) | **DELETE** /UdropshipVendors/{id}/udropshipVendorProducts | Deletes all udropshipVendorProducts of this model.
*UdropshipVendorApi* | [**udropshipVendorPrototypeDeleteVendorLocations**](docs/UdropshipVendorApi.md#udropshipVendorPrototypeDeleteVendorLocations) | **DELETE** /UdropshipVendors/{id}/vendorLocations | Deletes all vendorLocations of this model.
*UdropshipVendorApi* | [**udropshipVendorPrototypeDestroyByIdCatalogProducts**](docs/UdropshipVendorApi.md#udropshipVendorPrototypeDestroyByIdCatalogProducts) | **DELETE** /UdropshipVendors/{id}/catalogProducts/{fk} | Delete a related item by id for catalogProducts.
*UdropshipVendorApi* | [**udropshipVendorPrototypeDestroyByIdHarpoonHpublicApplicationpartners**](docs/UdropshipVendorApi.md#udropshipVendorPrototypeDestroyByIdHarpoonHpublicApplicationpartners) | **DELETE** /UdropshipVendors/{id}/harpoonHpublicApplicationpartners/{fk} | Delete a related item by id for harpoonHpublicApplicationpartners.
*UdropshipVendorApi* | [**udropshipVendorPrototypeDestroyByIdHarpoonHpublicv12VendorAppCategories**](docs/UdropshipVendorApi.md#udropshipVendorPrototypeDestroyByIdHarpoonHpublicv12VendorAppCategories) | **DELETE** /UdropshipVendors/{id}/harpoonHpublicv12VendorAppCategories/{fk} | Delete a related item by id for harpoonHpublicv12VendorAppCategories.
*UdropshipVendorApi* | [**udropshipVendorPrototypeDestroyByIdPartners**](docs/UdropshipVendorApi.md#udropshipVendorPrototypeDestroyByIdPartners) | **DELETE** /UdropshipVendors/{id}/partners/{fk} | Delete a related item by id for partners.
*UdropshipVendorApi* | [**udropshipVendorPrototypeDestroyByIdUdropshipVendorProducts**](docs/UdropshipVendorApi.md#udropshipVendorPrototypeDestroyByIdUdropshipVendorProducts) | **DELETE** /UdropshipVendors/{id}/udropshipVendorProducts/{fk} | Delete a related item by id for udropshipVendorProducts.
*UdropshipVendorApi* | [**udropshipVendorPrototypeDestroyByIdVendorLocations**](docs/UdropshipVendorApi.md#udropshipVendorPrototypeDestroyByIdVendorLocations) | **DELETE** /UdropshipVendors/{id}/vendorLocations/{fk} | Delete a related item by id for vendorLocations.
*UdropshipVendorApi* | [**udropshipVendorPrototypeDestroyConfig**](docs/UdropshipVendorApi.md#udropshipVendorPrototypeDestroyConfig) | **DELETE** /UdropshipVendors/{id}/config | Deletes config of this model.
*UdropshipVendorApi* | [**udropshipVendorPrototypeExistsCatalogProducts**](docs/UdropshipVendorApi.md#udropshipVendorPrototypeExistsCatalogProducts) | **HEAD** /UdropshipVendors/{id}/catalogProducts/rel/{fk} | Check the existence of catalogProducts relation to an item by id.
*UdropshipVendorApi* | [**udropshipVendorPrototypeExistsPartners**](docs/UdropshipVendorApi.md#udropshipVendorPrototypeExistsPartners) | **HEAD** /UdropshipVendors/{id}/partners/rel/{fk} | Check the existence of partners relation to an item by id.
*UdropshipVendorApi* | [**udropshipVendorPrototypeFindByIdCatalogProducts**](docs/UdropshipVendorApi.md#udropshipVendorPrototypeFindByIdCatalogProducts) | **GET** /UdropshipVendors/{id}/catalogProducts/{fk} | Find a related item by id for catalogProducts.
*UdropshipVendorApi* | [**udropshipVendorPrototypeFindByIdHarpoonHpublicApplicationpartners**](docs/UdropshipVendorApi.md#udropshipVendorPrototypeFindByIdHarpoonHpublicApplicationpartners) | **GET** /UdropshipVendors/{id}/harpoonHpublicApplicationpartners/{fk} | Find a related item by id for harpoonHpublicApplicationpartners.
*UdropshipVendorApi* | [**udropshipVendorPrototypeFindByIdHarpoonHpublicv12VendorAppCategories**](docs/UdropshipVendorApi.md#udropshipVendorPrototypeFindByIdHarpoonHpublicv12VendorAppCategories) | **GET** /UdropshipVendors/{id}/harpoonHpublicv12VendorAppCategories/{fk} | Find a related item by id for harpoonHpublicv12VendorAppCategories.
*UdropshipVendorApi* | [**udropshipVendorPrototypeFindByIdPartners**](docs/UdropshipVendorApi.md#udropshipVendorPrototypeFindByIdPartners) | **GET** /UdropshipVendors/{id}/partners/{fk} | Find a related item by id for partners.
*UdropshipVendorApi* | [**udropshipVendorPrototypeFindByIdUdropshipVendorProducts**](docs/UdropshipVendorApi.md#udropshipVendorPrototypeFindByIdUdropshipVendorProducts) | **GET** /UdropshipVendors/{id}/udropshipVendorProducts/{fk} | Find a related item by id for udropshipVendorProducts.
*UdropshipVendorApi* | [**udropshipVendorPrototypeFindByIdVendorLocations**](docs/UdropshipVendorApi.md#udropshipVendorPrototypeFindByIdVendorLocations) | **GET** /UdropshipVendors/{id}/vendorLocations/{fk} | Find a related item by id for vendorLocations.
*UdropshipVendorApi* | [**udropshipVendorPrototypeGetCatalogProducts**](docs/UdropshipVendorApi.md#udropshipVendorPrototypeGetCatalogProducts) | **GET** /UdropshipVendors/{id}/catalogProducts | Queries catalogProducts of UdropshipVendor.
*UdropshipVendorApi* | [**udropshipVendorPrototypeGetConfig**](docs/UdropshipVendorApi.md#udropshipVendorPrototypeGetConfig) | **GET** /UdropshipVendors/{id}/config | Fetches hasOne relation config.
*UdropshipVendorApi* | [**udropshipVendorPrototypeGetHarpoonHpublicApplicationpartners**](docs/UdropshipVendorApi.md#udropshipVendorPrototypeGetHarpoonHpublicApplicationpartners) | **GET** /UdropshipVendors/{id}/harpoonHpublicApplicationpartners | Queries harpoonHpublicApplicationpartners of UdropshipVendor.
*UdropshipVendorApi* | [**udropshipVendorPrototypeGetHarpoonHpublicv12VendorAppCategories**](docs/UdropshipVendorApi.md#udropshipVendorPrototypeGetHarpoonHpublicv12VendorAppCategories) | **GET** /UdropshipVendors/{id}/harpoonHpublicv12VendorAppCategories | Queries harpoonHpublicv12VendorAppCategories of UdropshipVendor.
*UdropshipVendorApi* | [**udropshipVendorPrototypeGetPartners**](docs/UdropshipVendorApi.md#udropshipVendorPrototypeGetPartners) | **GET** /UdropshipVendors/{id}/partners | Queries partners of UdropshipVendor.
*UdropshipVendorApi* | [**udropshipVendorPrototypeGetUdropshipVendorProducts**](docs/UdropshipVendorApi.md#udropshipVendorPrototypeGetUdropshipVendorProducts) | **GET** /UdropshipVendors/{id}/udropshipVendorProducts | Queries udropshipVendorProducts of UdropshipVendor.
*UdropshipVendorApi* | [**udropshipVendorPrototypeGetVendorLocations**](docs/UdropshipVendorApi.md#udropshipVendorPrototypeGetVendorLocations) | **GET** /UdropshipVendors/{id}/vendorLocations | Queries vendorLocations of UdropshipVendor.
*UdropshipVendorApi* | [**udropshipVendorPrototypeLinkCatalogProducts**](docs/UdropshipVendorApi.md#udropshipVendorPrototypeLinkCatalogProducts) | **PUT** /UdropshipVendors/{id}/catalogProducts/rel/{fk} | Add a related item by id for catalogProducts.
*UdropshipVendorApi* | [**udropshipVendorPrototypeLinkPartners**](docs/UdropshipVendorApi.md#udropshipVendorPrototypeLinkPartners) | **PUT** /UdropshipVendors/{id}/partners/rel/{fk} | Add a related item by id for partners.
*UdropshipVendorApi* | [**udropshipVendorPrototypeUnlinkCatalogProducts**](docs/UdropshipVendorApi.md#udropshipVendorPrototypeUnlinkCatalogProducts) | **DELETE** /UdropshipVendors/{id}/catalogProducts/rel/{fk} | Remove the catalogProducts relation to an item by id.
*UdropshipVendorApi* | [**udropshipVendorPrototypeUnlinkPartners**](docs/UdropshipVendorApi.md#udropshipVendorPrototypeUnlinkPartners) | **DELETE** /UdropshipVendors/{id}/partners/rel/{fk} | Remove the partners relation to an item by id.
*UdropshipVendorApi* | [**udropshipVendorPrototypeUpdateAttributesPatchUdropshipVendorsid**](docs/UdropshipVendorApi.md#udropshipVendorPrototypeUpdateAttributesPatchUdropshipVendorsid) | **PATCH** /UdropshipVendors/{id} | Patch attributes for a model instance and persist it into the data source.
*UdropshipVendorApi* | [**udropshipVendorPrototypeUpdateAttributesPutUdropshipVendorsid**](docs/UdropshipVendorApi.md#udropshipVendorPrototypeUpdateAttributesPutUdropshipVendorsid) | **PUT** /UdropshipVendors/{id} | Patch attributes for a model instance and persist it into the data source.
*UdropshipVendorApi* | [**udropshipVendorPrototypeUpdateByIdCatalogProducts**](docs/UdropshipVendorApi.md#udropshipVendorPrototypeUpdateByIdCatalogProducts) | **PUT** /UdropshipVendors/{id}/catalogProducts/{fk} | Update a related item by id for catalogProducts.
*UdropshipVendorApi* | [**udropshipVendorPrototypeUpdateByIdHarpoonHpublicApplicationpartners**](docs/UdropshipVendorApi.md#udropshipVendorPrototypeUpdateByIdHarpoonHpublicApplicationpartners) | **PUT** /UdropshipVendors/{id}/harpoonHpublicApplicationpartners/{fk} | Update a related item by id for harpoonHpublicApplicationpartners.
*UdropshipVendorApi* | [**udropshipVendorPrototypeUpdateByIdHarpoonHpublicv12VendorAppCategories**](docs/UdropshipVendorApi.md#udropshipVendorPrototypeUpdateByIdHarpoonHpublicv12VendorAppCategories) | **PUT** /UdropshipVendors/{id}/harpoonHpublicv12VendorAppCategories/{fk} | Update a related item by id for harpoonHpublicv12VendorAppCategories.
*UdropshipVendorApi* | [**udropshipVendorPrototypeUpdateByIdPartners**](docs/UdropshipVendorApi.md#udropshipVendorPrototypeUpdateByIdPartners) | **PUT** /UdropshipVendors/{id}/partners/{fk} | Update a related item by id for partners.
*UdropshipVendorApi* | [**udropshipVendorPrototypeUpdateByIdUdropshipVendorProducts**](docs/UdropshipVendorApi.md#udropshipVendorPrototypeUpdateByIdUdropshipVendorProducts) | **PUT** /UdropshipVendors/{id}/udropshipVendorProducts/{fk} | Update a related item by id for udropshipVendorProducts.
*UdropshipVendorApi* | [**udropshipVendorPrototypeUpdateByIdVendorLocations**](docs/UdropshipVendorApi.md#udropshipVendorPrototypeUpdateByIdVendorLocations) | **PUT** /UdropshipVendors/{id}/vendorLocations/{fk} | Update a related item by id for vendorLocations.
*UdropshipVendorApi* | [**udropshipVendorPrototypeUpdateConfig**](docs/UdropshipVendorApi.md#udropshipVendorPrototypeUpdateConfig) | **PUT** /UdropshipVendors/{id}/config | Update config of this model.
*UdropshipVendorApi* | [**udropshipVendorReplaceById**](docs/UdropshipVendorApi.md#udropshipVendorReplaceById) | **POST** /UdropshipVendors/{id}/replace | Replace attributes for a model instance and persist it into the data source.
*UdropshipVendorApi* | [**udropshipVendorReplaceOrCreate**](docs/UdropshipVendorApi.md#udropshipVendorReplaceOrCreate) | **POST** /UdropshipVendors/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
*UdropshipVendorApi* | [**udropshipVendorUpdateAll**](docs/UdropshipVendorApi.md#udropshipVendorUpdateAll) | **POST** /UdropshipVendors/update | Update instances of the model matched by {{where}} from the data source.
*UdropshipVendorApi* | [**udropshipVendorUpsertPatchUdropshipVendors**](docs/UdropshipVendorApi.md#udropshipVendorUpsertPatchUdropshipVendors) | **PATCH** /UdropshipVendors | Patch an existing model instance or insert a new one into the data source.
*UdropshipVendorApi* | [**udropshipVendorUpsertPutUdropshipVendors**](docs/UdropshipVendorApi.md#udropshipVendorUpsertPutUdropshipVendors) | **PUT** /UdropshipVendors | Patch an existing model instance or insert a new one into the data source.
*UdropshipVendorApi* | [**udropshipVendorUpsertWithWhere**](docs/UdropshipVendorApi.md#udropshipVendorUpsertWithWhere) | **POST** /UdropshipVendors/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.


## Documentation for Models

 - [Address](docs/Address.md)
 - [Analytic](docs/Analytic.md)
 - [AnalyticRequest](docs/AnalyticRequest.md)
 - [AndroidKeystoreConfig](docs/AndroidKeystoreConfig.md)
 - [AnonymousModel8](docs/AnonymousModel8.md)
 - [App](docs/App.md)
 - [AppAd](docs/AppAd.md)
 - [AppColor](docs/AppColor.md)
 - [AppConfig](docs/AppConfig.md)
 - [AppConnectTo](docs/AppConnectTo.md)
 - [AuthToken](docs/AuthToken.md)
 - [AwCollpurCoupon](docs/AwCollpurCoupon.md)
 - [AwCollpurDeal](docs/AwCollpurDeal.md)
 - [AwCollpurDealPurchases](docs/AwCollpurDealPurchases.md)
 - [AwEventbookingEvent](docs/AwEventbookingEvent.md)
 - [AwEventbookingEventAttribute](docs/AwEventbookingEventAttribute.md)
 - [AwEventbookingEventTicket](docs/AwEventbookingEventTicket.md)
 - [AwEventbookingEventTicketAttribute](docs/AwEventbookingEventTicketAttribute.md)
 - [AwEventbookingOrderHistory](docs/AwEventbookingOrderHistory.md)
 - [AwEventbookingTicket](docs/AwEventbookingTicket.md)
 - [BatchConfig](docs/BatchConfig.md)
 - [BranchConfig](docs/BranchConfig.md)
 - [Brand](docs/Brand.md)
 - [BrandFeed](docs/BrandFeed.md)
 - [Card](docs/Card.md)
 - [CatalogCategory](docs/CatalogCategory.md)
 - [CatalogCategoryProduct](docs/CatalogCategoryProduct.md)
 - [CatalogInventoryStockItem](docs/CatalogInventoryStockItem.md)
 - [CatalogProduct](docs/CatalogProduct.md)
 - [Category](docs/Category.md)
 - [CheckoutAgreement](docs/CheckoutAgreement.md)
 - [Competition](docs/Competition.md)
 - [CompetitionAnswerData](docs/CompetitionAnswerData.md)
 - [CompetitionAttendee](docs/CompetitionAttendee.md)
 - [CompetitionChance](docs/CompetitionChance.md)
 - [CompetitionCheckout](docs/CompetitionCheckout.md)
 - [CompetitionCheckoutChanceData](docs/CompetitionCheckoutChanceData.md)
 - [CompetitionCheckoutData](docs/CompetitionCheckoutData.md)
 - [CompetitionPurchase](docs/CompetitionPurchase.md)
 - [CompetitionRedeemData](docs/CompetitionRedeemData.md)
 - [CompetitionWinner](docs/CompetitionWinner.md)
 - [Contact](docs/Contact.md)
 - [CoreWebsite](docs/CoreWebsite.md)
 - [Coupon](docs/Coupon.md)
 - [CouponCheckout](docs/CouponCheckout.md)
 - [CouponCheckoutData](docs/CouponCheckoutData.md)
 - [CouponPurchase](docs/CouponPurchase.md)
 - [CouponPurchaseCode](docs/CouponPurchaseCode.md)
 - [CouponRedeemData](docs/CouponRedeemData.md)
 - [Customer](docs/Customer.md)
 - [CustomerActivity](docs/CustomerActivity.md)
 - [CustomerBadge](docs/CustomerBadge.md)
 - [CustomerCardCreateData](docs/CustomerCardCreateData.md)
 - [CustomerConnection](docs/CustomerConnection.md)
 - [CustomerConnectionFacebook](docs/CustomerConnectionFacebook.md)
 - [CustomerConnectionTwitter](docs/CustomerConnectionTwitter.md)
 - [CustomerLoginCredentials](docs/CustomerLoginCredentials.md)
 - [CustomerNotification](docs/CustomerNotification.md)
 - [CustomerPasswordResetCredentials](docs/CustomerPasswordResetCredentials.md)
 - [CustomerPasswordUpdateCredentials](docs/CustomerPasswordUpdateCredentials.md)
 - [CustomerPrivacy](docs/CustomerPrivacy.md)
 - [CustomerRefreshTokenCredentials](docs/CustomerRefreshTokenCredentials.md)
 - [CustomerTokenFromAuthorizationCodeCredentials](docs/CustomerTokenFromAuthorizationCodeCredentials.md)
 - [CustomerUpdateData](docs/CustomerUpdateData.md)
 - [Deal](docs/Deal.md)
 - [DealGroup](docs/DealGroup.md)
 - [DealGroupCheckout](docs/DealGroupCheckout.md)
 - [DealGroupCheckoutData](docs/DealGroupCheckoutData.md)
 - [DealGroupPurchase](docs/DealGroupPurchase.md)
 - [DealGroupRedeemData](docs/DealGroupRedeemData.md)
 - [DealPaid](docs/DealPaid.md)
 - [DealPaidCheckout](docs/DealPaidCheckout.md)
 - [DealPaidCheckoutData](docs/DealPaidCheckoutData.md)
 - [DealPaidPurchase](docs/DealPaidPurchase.md)
 - [DealPaidRedeemData](docs/DealPaidRedeemData.md)
 - [Event](docs/Event.md)
 - [EventAttendee](docs/EventAttendee.md)
 - [EventCheckout](docs/EventCheckout.md)
 - [EventCheckoutData](docs/EventCheckoutData.md)
 - [EventCheckoutTicketData](docs/EventCheckoutTicketData.md)
 - [EventPurchase](docs/EventPurchase.md)
 - [EventRedeemData](docs/EventRedeemData.md)
 - [EventTicket](docs/EventTicket.md)
 - [FacebookConfig](docs/FacebookConfig.md)
 - [FacebookEvent](docs/FacebookEvent.md)
 - [Follower](docs/Follower.md)
 - [Following](docs/Following.md)
 - [FormRow](docs/FormRow.md)
 - [FormSection](docs/FormSection.md)
 - [FormSelectorOption](docs/FormSelectorOption.md)
 - [GeoLocation](docs/GeoLocation.md)
 - [GoogleAnalyticsTracking](docs/GoogleAnalyticsTracking.md)
 - [HarpoonHpublicApplicationpartner](docs/HarpoonHpublicApplicationpartner.md)
 - [HarpoonHpublicBrandvenue](docs/HarpoonHpublicBrandvenue.md)
 - [HarpoonHpublicVendorconfig](docs/HarpoonHpublicVendorconfig.md)
 - [HarpoonHpublicv12VendorAppCategory](docs/HarpoonHpublicv12VendorAppCategory.md)
 - [InlineResponse200](docs/InlineResponse200.md)
 - [InlineResponse2001](docs/InlineResponse2001.md)
 - [InlineResponse2002](docs/InlineResponse2002.md)
 - [InlineResponse2003](docs/InlineResponse2003.md)
 - [LoginExternal](docs/LoginExternal.md)
 - [MagentoFileUpload](docs/MagentoFileUpload.md)
 - [MagentoImageUpload](docs/MagentoImageUpload.md)
 - [MenuItem](docs/MenuItem.md)
 - [NotificationFrom](docs/NotificationFrom.md)
 - [NotificationRelated](docs/NotificationRelated.md)
 - [Offer](docs/Offer.md)
 - [OfferClosestPurchase](docs/OfferClosestPurchase.md)
 - [PlayerConfig](docs/PlayerConfig.md)
 - [PlayerSource](docs/PlayerSource.md)
 - [PlayerTrack](docs/PlayerTrack.md)
 - [Playlist](docs/Playlist.md)
 - [PlaylistItem](docs/PlaylistItem.md)
 - [Product](docs/Product.md)
 - [PushWooshConfig](docs/PushWooshConfig.md)
 - [RadioPlaySession](docs/RadioPlaySession.md)
 - [RadioPresenter](docs/RadioPresenter.md)
 - [RadioPresenterRadioShow](docs/RadioPresenterRadioShow.md)
 - [RadioShow](docs/RadioShow.md)
 - [RadioShowTime](docs/RadioShowTime.md)
 - [RadioStream](docs/RadioStream.md)
 - [RssFeed](docs/RssFeed.md)
 - [RssFeedGroup](docs/RssFeedGroup.md)
 - [RssTags](docs/RssTags.md)
 - [StripeCoupon](docs/StripeCoupon.md)
 - [StripeDiscount](docs/StripeDiscount.md)
 - [StripeInvoice](docs/StripeInvoice.md)
 - [StripeInvoiceItem](docs/StripeInvoiceItem.md)
 - [StripePlan](docs/StripePlan.md)
 - [StripeSubscription](docs/StripeSubscription.md)
 - [TritonConfig](docs/TritonConfig.md)
 - [TwitterConfig](docs/TwitterConfig.md)
 - [UdropshipVendor](docs/UdropshipVendor.md)
 - [UdropshipVendorPartner](docs/UdropshipVendorPartner.md)
 - [UdropshipVendorProduct](docs/UdropshipVendorProduct.md)
 - [UdropshipVendorProductAssoc](docs/UdropshipVendorProductAssoc.md)
 - [UrbanAirshipConfig](docs/UrbanAirshipConfig.md)
 - [Venue](docs/Venue.md)


## Documentation for Authorization

Authentication schemes defined for the API:
### access_token

- **Type**: API key
- **API key parameter name**: access_token
- **Location**: URL query string


## Recommendation

It's recommended to create an instance of `ApiClient` per thread in a multithreaded environment to avoid any potential issues.

## Author



