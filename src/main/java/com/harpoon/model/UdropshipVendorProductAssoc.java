/**
 * harpoon-api
 * Harpoon API to integrate with all the Harpoon services.  You can find out more about Harpoon      at <a href='https://harpoonconnect.com'>https://harpoonconnect.com</a>, #harpoonConnect.
 *
 * OpenAPI spec version: 1.1.1
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.harpoon.model;


import io.swagger.annotations.*;
import com.google.gson.annotations.SerializedName;


@ApiModel(description = "")
public class UdropshipVendorProductAssoc  {
  
  @SerializedName("isUdmulti")
  private Double isUdmulti = null;
  @SerializedName("id")
  private Double id = null;
  @SerializedName("isAttribute")
  private Double isAttribute = null;
  @SerializedName("productId")
  private Double productId = null;
  @SerializedName("vendorId")
  private Double vendorId = null;
  @SerializedName("udropshipVendor")
  private Object udropshipVendor = null;
  @SerializedName("catalogProduct")
  private Object catalogProduct = null;

  /**
   **/
  @ApiModelProperty(value = "")
  public Double getIsUdmulti() {
    return isUdmulti;
  }
  public void setIsUdmulti(Double isUdmulti) {
    this.isUdmulti = isUdmulti;
  }

  /**
   **/
  @ApiModelProperty(required = true, value = "")
  public Double getId() {
    return id;
  }
  public void setId(Double id) {
    this.id = id;
  }

  /**
   **/
  @ApiModelProperty(required = true, value = "")
  public Double getIsAttribute() {
    return isAttribute;
  }
  public void setIsAttribute(Double isAttribute) {
    this.isAttribute = isAttribute;
  }

  /**
   **/
  @ApiModelProperty(required = true, value = "")
  public Double getProductId() {
    return productId;
  }
  public void setProductId(Double productId) {
    this.productId = productId;
  }

  /**
   **/
  @ApiModelProperty(required = true, value = "")
  public Double getVendorId() {
    return vendorId;
  }
  public void setVendorId(Double vendorId) {
    this.vendorId = vendorId;
  }

  /**
   **/
  @ApiModelProperty(value = "")
  public Object getUdropshipVendor() {
    return udropshipVendor;
  }
  public void setUdropshipVendor(Object udropshipVendor) {
    this.udropshipVendor = udropshipVendor;
  }

  /**
   **/
  @ApiModelProperty(value = "")
  public Object getCatalogProduct() {
    return catalogProduct;
  }
  public void setCatalogProduct(Object catalogProduct) {
    this.catalogProduct = catalogProduct;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    UdropshipVendorProductAssoc udropshipVendorProductAssoc = (UdropshipVendorProductAssoc) o;
    return (this.isUdmulti == null ? udropshipVendorProductAssoc.isUdmulti == null : this.isUdmulti.equals(udropshipVendorProductAssoc.isUdmulti)) &&
        (this.id == null ? udropshipVendorProductAssoc.id == null : this.id.equals(udropshipVendorProductAssoc.id)) &&
        (this.isAttribute == null ? udropshipVendorProductAssoc.isAttribute == null : this.isAttribute.equals(udropshipVendorProductAssoc.isAttribute)) &&
        (this.productId == null ? udropshipVendorProductAssoc.productId == null : this.productId.equals(udropshipVendorProductAssoc.productId)) &&
        (this.vendorId == null ? udropshipVendorProductAssoc.vendorId == null : this.vendorId.equals(udropshipVendorProductAssoc.vendorId)) &&
        (this.udropshipVendor == null ? udropshipVendorProductAssoc.udropshipVendor == null : this.udropshipVendor.equals(udropshipVendorProductAssoc.udropshipVendor)) &&
        (this.catalogProduct == null ? udropshipVendorProductAssoc.catalogProduct == null : this.catalogProduct.equals(udropshipVendorProductAssoc.catalogProduct));
  }

  @Override
  public int hashCode() {
    int result = 17;
    result = 31 * result + (isUdmulti == null ? 0: isUdmulti.hashCode());
    result = 31 * result + (id == null ? 0: id.hashCode());
    result = 31 * result + (isAttribute == null ? 0: isAttribute.hashCode());
    result = 31 * result + (productId == null ? 0: productId.hashCode());
    result = 31 * result + (vendorId == null ? 0: vendorId.hashCode());
    result = 31 * result + (udropshipVendor == null ? 0: udropshipVendor.hashCode());
    result = 31 * result + (catalogProduct == null ? 0: catalogProduct.hashCode());
    return result;
  }

  @Override
  public String toString()  {
    StringBuilder sb = new StringBuilder();
    sb.append("class UdropshipVendorProductAssoc {\n");
    
    sb.append("  isUdmulti: ").append(isUdmulti).append("\n");
    sb.append("  id: ").append(id).append("\n");
    sb.append("  isAttribute: ").append(isAttribute).append("\n");
    sb.append("  productId: ").append(productId).append("\n");
    sb.append("  vendorId: ").append(vendorId).append("\n");
    sb.append("  udropshipVendor: ").append(udropshipVendor).append("\n");
    sb.append("  catalogProduct: ").append(catalogProduct).append("\n");
    sb.append("}\n");
    return sb.toString();
  }
}
