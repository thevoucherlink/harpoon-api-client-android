/**
 * harpoon-api
 * Harpoon API to integrate with all the Harpoon services.  You can find out more about Harpoon      at <a href='https://harpoonconnect.com'>https://harpoonconnect.com</a>, #harpoonConnect.
 *
 * OpenAPI spec version: 1.1.1
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.harpoon.model;


import io.swagger.annotations.*;
import com.google.gson.annotations.SerializedName;


@ApiModel(description = "")
public class CompetitionRedeemData  {
  
  @SerializedName("chanceId")
  private Double chanceId = null;

  /**
   * Competition purchase id
   **/
  @ApiModelProperty(required = true, value = "Competition purchase id")
  public Double getChanceId() {
    return chanceId;
  }
  public void setChanceId(Double chanceId) {
    this.chanceId = chanceId;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    CompetitionRedeemData competitionRedeemData = (CompetitionRedeemData) o;
    return (this.chanceId == null ? competitionRedeemData.chanceId == null : this.chanceId.equals(competitionRedeemData.chanceId));
  }

  @Override
  public int hashCode() {
    int result = 17;
    result = 31 * result + (chanceId == null ? 0: chanceId.hashCode());
    return result;
  }

  @Override
  public String toString()  {
    StringBuilder sb = new StringBuilder();
    sb.append("class CompetitionRedeemData {\n");
    
    sb.append("  chanceId: ").append(chanceId).append("\n");
    sb.append("}\n");
    return sb.toString();
  }
}
