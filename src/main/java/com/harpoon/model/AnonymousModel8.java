/**
 * harpoon-api
 * Harpoon API to integrate with all the Harpoon services.  You can find out more about Harpoon      at <a href='https://harpoonconnect.com'>https://harpoonconnect.com</a>, #harpoonConnect.
 *
 * OpenAPI spec version: 1.1.1
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.harpoon.model;


import io.swagger.annotations.*;
import com.google.gson.annotations.SerializedName;


@ApiModel(description = "")
public class AnonymousModel8  {
  
  @SerializedName("brandId")
  private Double brandId = null;
  @SerializedName("competitionId")
  private Double competitionId = null;
  @SerializedName("couponId")
  private Double couponId = null;
  @SerializedName("customerId")
  private Double customerId = null;
  @SerializedName("eventId")
  private Double eventId = null;
  @SerializedName("dealPaid")
  private Double dealPaid = null;
  @SerializedName("dealGroup")
  private Double dealGroup = null;

  /**
   **/
  @ApiModelProperty(value = "")
  public Double getBrandId() {
    return brandId;
  }
  public void setBrandId(Double brandId) {
    this.brandId = brandId;
  }

  /**
   **/
  @ApiModelProperty(value = "")
  public Double getCompetitionId() {
    return competitionId;
  }
  public void setCompetitionId(Double competitionId) {
    this.competitionId = competitionId;
  }

  /**
   **/
  @ApiModelProperty(value = "")
  public Double getCouponId() {
    return couponId;
  }
  public void setCouponId(Double couponId) {
    this.couponId = couponId;
  }

  /**
   **/
  @ApiModelProperty(value = "")
  public Double getCustomerId() {
    return customerId;
  }
  public void setCustomerId(Double customerId) {
    this.customerId = customerId;
  }

  /**
   **/
  @ApiModelProperty(value = "")
  public Double getEventId() {
    return eventId;
  }
  public void setEventId(Double eventId) {
    this.eventId = eventId;
  }

  /**
   **/
  @ApiModelProperty(value = "")
  public Double getDealPaid() {
    return dealPaid;
  }
  public void setDealPaid(Double dealPaid) {
    this.dealPaid = dealPaid;
  }

  /**
   **/
  @ApiModelProperty(value = "")
  public Double getDealGroup() {
    return dealGroup;
  }
  public void setDealGroup(Double dealGroup) {
    this.dealGroup = dealGroup;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    AnonymousModel8 anonymousModel8 = (AnonymousModel8) o;
    return (this.brandId == null ? anonymousModel8.brandId == null : this.brandId.equals(anonymousModel8.brandId)) &&
        (this.competitionId == null ? anonymousModel8.competitionId == null : this.competitionId.equals(anonymousModel8.competitionId)) &&
        (this.couponId == null ? anonymousModel8.couponId == null : this.couponId.equals(anonymousModel8.couponId)) &&
        (this.customerId == null ? anonymousModel8.customerId == null : this.customerId.equals(anonymousModel8.customerId)) &&
        (this.eventId == null ? anonymousModel8.eventId == null : this.eventId.equals(anonymousModel8.eventId)) &&
        (this.dealPaid == null ? anonymousModel8.dealPaid == null : this.dealPaid.equals(anonymousModel8.dealPaid)) &&
        (this.dealGroup == null ? anonymousModel8.dealGroup == null : this.dealGroup.equals(anonymousModel8.dealGroup));
  }

  @Override
  public int hashCode() {
    int result = 17;
    result = 31 * result + (brandId == null ? 0: brandId.hashCode());
    result = 31 * result + (competitionId == null ? 0: competitionId.hashCode());
    result = 31 * result + (couponId == null ? 0: couponId.hashCode());
    result = 31 * result + (customerId == null ? 0: customerId.hashCode());
    result = 31 * result + (eventId == null ? 0: eventId.hashCode());
    result = 31 * result + (dealPaid == null ? 0: dealPaid.hashCode());
    result = 31 * result + (dealGroup == null ? 0: dealGroup.hashCode());
    return result;
  }

  @Override
  public String toString()  {
    StringBuilder sb = new StringBuilder();
    sb.append("class AnonymousModel8 {\n");
    
    sb.append("  brandId: ").append(brandId).append("\n");
    sb.append("  competitionId: ").append(competitionId).append("\n");
    sb.append("  couponId: ").append(couponId).append("\n");
    sb.append("  customerId: ").append(customerId).append("\n");
    sb.append("  eventId: ").append(eventId).append("\n");
    sb.append("  dealPaid: ").append(dealPaid).append("\n");
    sb.append("  dealGroup: ").append(dealGroup).append("\n");
    sb.append("}\n");
    return sb.toString();
  }
}
