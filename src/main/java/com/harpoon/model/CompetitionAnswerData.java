/**
 * harpoon-api
 * Harpoon API to integrate with all the Harpoon services.  You can find out more about Harpoon      at <a href='https://harpoonconnect.com'>https://harpoonconnect.com</a>, #harpoonConnect.
 *
 * OpenAPI spec version: 1.1.1
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.harpoon.model;


import io.swagger.annotations.*;
import com.google.gson.annotations.SerializedName;


@ApiModel(description = "")
public class CompetitionAnswerData  {
  
  @SerializedName("competitionAnswer")
  private String competitionAnswer = null;

  /**
   * Competition Answer
   **/
  @ApiModelProperty(required = true, value = "Competition Answer")
  public String getCompetitionAnswer() {
    return competitionAnswer;
  }
  public void setCompetitionAnswer(String competitionAnswer) {
    this.competitionAnswer = competitionAnswer;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    CompetitionAnswerData competitionAnswerData = (CompetitionAnswerData) o;
    return (this.competitionAnswer == null ? competitionAnswerData.competitionAnswer == null : this.competitionAnswer.equals(competitionAnswerData.competitionAnswer));
  }

  @Override
  public int hashCode() {
    int result = 17;
    result = 31 * result + (competitionAnswer == null ? 0: competitionAnswer.hashCode());
    return result;
  }

  @Override
  public String toString()  {
    StringBuilder sb = new StringBuilder();
    sb.append("class CompetitionAnswerData {\n");
    
    sb.append("  competitionAnswer: ").append(competitionAnswer).append("\n");
    sb.append("}\n");
    return sb.toString();
  }
}
