/**
 * harpoon-api
 * Harpoon API to integrate with all the Harpoon services.  You can find out more about Harpoon      at <a href='https://harpoonconnect.com'>https://harpoonconnect.com</a>, #harpoonConnect.
 *
 * OpenAPI spec version: 1.1.1
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.harpoon.model;

import com.harpoon.model.Contact;
import java.util.*;

import io.swagger.annotations.*;
import com.google.gson.annotations.SerializedName;


@ApiModel(description = "")
public class RadioPresenter  {
  
  @SerializedName("name")
  private String name = null;
  @SerializedName("image")
  private String image = null;
  @SerializedName("contact")
  private Contact contact = null;
  @SerializedName("id")
  private Double id = null;
  @SerializedName("radioShows")
  private List<Object> radioShows = null;

  /**
   **/
  @ApiModelProperty(required = true, value = "")
  public String getName() {
    return name;
  }
  public void setName(String name) {
    this.name = name;
  }

  /**
   **/
  @ApiModelProperty(value = "")
  public String getImage() {
    return image;
  }
  public void setImage(String image) {
    this.image = image;
  }

  /**
   * Contacts for this presenter
   **/
  @ApiModelProperty(value = "Contacts for this presenter")
  public Contact getContact() {
    return contact;
  }
  public void setContact(Contact contact) {
    this.contact = contact;
  }

  /**
   **/
  @ApiModelProperty(value = "")
  public Double getId() {
    return id;
  }
  public void setId(Double id) {
    this.id = id;
  }

  /**
   **/
  @ApiModelProperty(value = "")
  public List<Object> getRadioShows() {
    return radioShows;
  }
  public void setRadioShows(List<Object> radioShows) {
    this.radioShows = radioShows;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    RadioPresenter radioPresenter = (RadioPresenter) o;
    return (this.name == null ? radioPresenter.name == null : this.name.equals(radioPresenter.name)) &&
        (this.image == null ? radioPresenter.image == null : this.image.equals(radioPresenter.image)) &&
        (this.contact == null ? radioPresenter.contact == null : this.contact.equals(radioPresenter.contact)) &&
        (this.id == null ? radioPresenter.id == null : this.id.equals(radioPresenter.id)) &&
        (this.radioShows == null ? radioPresenter.radioShows == null : this.radioShows.equals(radioPresenter.radioShows));
  }

  @Override
  public int hashCode() {
    int result = 17;
    result = 31 * result + (name == null ? 0: name.hashCode());
    result = 31 * result + (image == null ? 0: image.hashCode());
    result = 31 * result + (contact == null ? 0: contact.hashCode());
    result = 31 * result + (id == null ? 0: id.hashCode());
    result = 31 * result + (radioShows == null ? 0: radioShows.hashCode());
    return result;
  }

  @Override
  public String toString()  {
    StringBuilder sb = new StringBuilder();
    sb.append("class RadioPresenter {\n");
    
    sb.append("  name: ").append(name).append("\n");
    sb.append("  image: ").append(image).append("\n");
    sb.append("  contact: ").append(contact).append("\n");
    sb.append("  id: ").append(id).append("\n");
    sb.append("  radioShows: ").append(radioShows).append("\n");
    sb.append("}\n");
    return sb.toString();
  }
}
