/**
 * harpoon-api
 * Harpoon API to integrate with all the Harpoon services.  You can find out more about Harpoon      at <a href='https://harpoonconnect.com'>https://harpoonconnect.com</a>, #harpoonConnect.
 *
 * OpenAPI spec version: 1.1.1
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.harpoon.model;

import com.harpoon.model.Brand;
import com.harpoon.model.Customer;

import io.swagger.annotations.*;
import com.google.gson.annotations.SerializedName;


@ApiModel(description = "")
public class NotificationFrom  {
  
  @SerializedName("customer")
  private Customer customer = null;
  @SerializedName("brand")
  private Brand brand = null;
  @SerializedName("id")
  private Double id = null;

  /**
   **/
  @ApiModelProperty(value = "")
  public Customer getCustomer() {
    return customer;
  }
  public void setCustomer(Customer customer) {
    this.customer = customer;
  }

  /**
   **/
  @ApiModelProperty(value = "")
  public Brand getBrand() {
    return brand;
  }
  public void setBrand(Brand brand) {
    this.brand = brand;
  }

  /**
   **/
  @ApiModelProperty(value = "")
  public Double getId() {
    return id;
  }
  public void setId(Double id) {
    this.id = id;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    NotificationFrom notificationFrom = (NotificationFrom) o;
    return (this.customer == null ? notificationFrom.customer == null : this.customer.equals(notificationFrom.customer)) &&
        (this.brand == null ? notificationFrom.brand == null : this.brand.equals(notificationFrom.brand)) &&
        (this.id == null ? notificationFrom.id == null : this.id.equals(notificationFrom.id));
  }

  @Override
  public int hashCode() {
    int result = 17;
    result = 31 * result + (customer == null ? 0: customer.hashCode());
    result = 31 * result + (brand == null ? 0: brand.hashCode());
    result = 31 * result + (id == null ? 0: id.hashCode());
    return result;
  }

  @Override
  public String toString()  {
    StringBuilder sb = new StringBuilder();
    sb.append("class NotificationFrom {\n");
    
    sb.append("  customer: ").append(customer).append("\n");
    sb.append("  brand: ").append(brand).append("\n");
    sb.append("  id: ").append(id).append("\n");
    sb.append("}\n");
    return sb.toString();
  }
}
