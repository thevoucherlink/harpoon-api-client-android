/**
 * harpoon-api
 * Harpoon API to integrate with all the Harpoon services.  You can find out more about Harpoon      at <a href='https://harpoonconnect.com'>https://harpoonconnect.com</a>, #harpoonConnect.
 *
 * OpenAPI spec version: 1.1.1
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.harpoon.model;


import io.swagger.annotations.*;
import com.google.gson.annotations.SerializedName;


@ApiModel(description = "")
public class CoreWebsite  {
  
  @SerializedName("id")
  private Double id = null;
  @SerializedName("code")
  private String code = null;
  @SerializedName("name")
  private String name = null;
  @SerializedName("sortOrder")
  private Double sortOrder = null;
  @SerializedName("defaultGroupId")
  private Double defaultGroupId = null;
  @SerializedName("isDefault")
  private Double isDefault = null;

  /**
   **/
  @ApiModelProperty(required = true, value = "")
  public Double getId() {
    return id;
  }
  public void setId(Double id) {
    this.id = id;
  }

  /**
   **/
  @ApiModelProperty(value = "")
  public String getCode() {
    return code;
  }
  public void setCode(String code) {
    this.code = code;
  }

  /**
   **/
  @ApiModelProperty(value = "")
  public String getName() {
    return name;
  }
  public void setName(String name) {
    this.name = name;
  }

  /**
   **/
  @ApiModelProperty(required = true, value = "")
  public Double getSortOrder() {
    return sortOrder;
  }
  public void setSortOrder(Double sortOrder) {
    this.sortOrder = sortOrder;
  }

  /**
   **/
  @ApiModelProperty(required = true, value = "")
  public Double getDefaultGroupId() {
    return defaultGroupId;
  }
  public void setDefaultGroupId(Double defaultGroupId) {
    this.defaultGroupId = defaultGroupId;
  }

  /**
   **/
  @ApiModelProperty(value = "")
  public Double getIsDefault() {
    return isDefault;
  }
  public void setIsDefault(Double isDefault) {
    this.isDefault = isDefault;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    CoreWebsite coreWebsite = (CoreWebsite) o;
    return (this.id == null ? coreWebsite.id == null : this.id.equals(coreWebsite.id)) &&
        (this.code == null ? coreWebsite.code == null : this.code.equals(coreWebsite.code)) &&
        (this.name == null ? coreWebsite.name == null : this.name.equals(coreWebsite.name)) &&
        (this.sortOrder == null ? coreWebsite.sortOrder == null : this.sortOrder.equals(coreWebsite.sortOrder)) &&
        (this.defaultGroupId == null ? coreWebsite.defaultGroupId == null : this.defaultGroupId.equals(coreWebsite.defaultGroupId)) &&
        (this.isDefault == null ? coreWebsite.isDefault == null : this.isDefault.equals(coreWebsite.isDefault));
  }

  @Override
  public int hashCode() {
    int result = 17;
    result = 31 * result + (id == null ? 0: id.hashCode());
    result = 31 * result + (code == null ? 0: code.hashCode());
    result = 31 * result + (name == null ? 0: name.hashCode());
    result = 31 * result + (sortOrder == null ? 0: sortOrder.hashCode());
    result = 31 * result + (defaultGroupId == null ? 0: defaultGroupId.hashCode());
    result = 31 * result + (isDefault == null ? 0: isDefault.hashCode());
    return result;
  }

  @Override
  public String toString()  {
    StringBuilder sb = new StringBuilder();
    sb.append("class CoreWebsite {\n");
    
    sb.append("  id: ").append(id).append("\n");
    sb.append("  code: ").append(code).append("\n");
    sb.append("  name: ").append(name).append("\n");
    sb.append("  sortOrder: ").append(sortOrder).append("\n");
    sb.append("  defaultGroupId: ").append(defaultGroupId).append("\n");
    sb.append("  isDefault: ").append(isDefault).append("\n");
    sb.append("}\n");
    return sb.toString();
  }
}
