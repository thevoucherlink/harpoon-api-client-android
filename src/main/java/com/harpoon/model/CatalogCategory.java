/**
 * harpoon-api
 * Harpoon API to integrate with all the Harpoon services.  You can find out more about Harpoon      at <a href='https://harpoonconnect.com'>https://harpoonconnect.com</a>, #harpoonConnect.
 *
 * OpenAPI spec version: 1.1.1
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.harpoon.model;

import java.util.*;
import java.util.Date;

import io.swagger.annotations.*;
import com.google.gson.annotations.SerializedName;


@ApiModel(description = "")
public class CatalogCategory  {
  
  @SerializedName("id")
  private Double id = null;
  @SerializedName("parentId")
  private Double parentId = null;
  @SerializedName("createdAt")
  private String createdAt = null;
  @SerializedName("updatedAt")
  private String updatedAt = null;
  @SerializedName("path")
  private String path = null;
  @SerializedName("position")
  private Double position = null;
  @SerializedName("level")
  private Double level = null;
  @SerializedName("childrenCount")
  private Double childrenCount = null;
  @SerializedName("storeId")
  private Double storeId = null;
  @SerializedName("allChildren")
  private String allChildren = null;
  @SerializedName("availableSortBy")
  private String availableSortBy = null;
  @SerializedName("children")
  private String children = null;
  @SerializedName("customApplyToProducts")
  private Double customApplyToProducts = null;
  @SerializedName("customDesign")
  private String customDesign = null;
  @SerializedName("customDesignFrom")
  private String customDesignFrom = null;
  @SerializedName("customDesignTo")
  private String customDesignTo = null;
  @SerializedName("customLayoutUpdate")
  private String customLayoutUpdate = null;
  @SerializedName("customUseParentSettings")
  private Double customUseParentSettings = null;
  @SerializedName("defaultSortBy")
  private String defaultSortBy = null;
  @SerializedName("description")
  private String description = null;
  @SerializedName("displayMode")
  private String displayMode = null;
  @SerializedName("filterPriceRange")
  private String filterPriceRange = null;
  @SerializedName("image")
  private String image = null;
  @SerializedName("includeInMenu")
  private Double includeInMenu = null;
  @SerializedName("isActive")
  private Double isActive = null;
  @SerializedName("isAnchor")
  private Double isAnchor = null;
  @SerializedName("landingPage")
  private Double landingPage = null;
  @SerializedName("metaDescription")
  private String metaDescription = null;
  @SerializedName("metaKeywords")
  private String metaKeywords = null;
  @SerializedName("metaTitle")
  private String metaTitle = null;
  @SerializedName("name")
  private String name = null;
  @SerializedName("pageLayout")
  private String pageLayout = null;
  @SerializedName("pathInStore")
  private String pathInStore = null;
  @SerializedName("thumbnail")
  private String thumbnail = null;
  @SerializedName("ummCatLabel")
  private String ummCatLabel = null;
  @SerializedName("ummCatTarget")
  private String ummCatTarget = null;
  @SerializedName("ummDdBlocks")
  private String ummDdBlocks = null;
  @SerializedName("ummDdColumns")
  private Double ummDdColumns = null;
  @SerializedName("ummDdProportions")
  private String ummDdProportions = null;
  @SerializedName("ummDdType")
  private String ummDdType = null;
  @SerializedName("ummDdWidth")
  private String ummDdWidth = null;
  @SerializedName("urlKey")
  private String urlKey = null;
  @SerializedName("urlPath")
  private String urlPath = null;
  @SerializedName("catalogProducts")
  private List<Object> catalogProducts = null;

  /**
   **/
  @ApiModelProperty(required = true, value = "")
  public Double getId() {
    return id;
  }
  public void setId(Double id) {
    this.id = id;
  }

  /**
   **/
  @ApiModelProperty(required = true, value = "")
  public Double getParentId() {
    return parentId;
  }
  public void setParentId(Double parentId) {
    this.parentId = parentId;
  }

  /**
   **/
  @ApiModelProperty(value = "")
  public String getCreatedAt() {
    return createdAt;
  }
  public void setCreatedAt(Date createdAt) {
    this.createdAt = createdAt.toString();
  }

  /**
   **/
  @ApiModelProperty(value = "")
  public String getUpdatedAt() {
    return updatedAt;
  }
  public void setUpdatedAt(Date updatedAt) {
    this.updatedAt = updatedAt.toString();
  }

  /**
   **/
  @ApiModelProperty(required = true, value = "")
  public String getPath() {
    return path;
  }
  public void setPath(String path) {
    this.path = path;
  }

  /**
   **/
  @ApiModelProperty(required = true, value = "")
  public Double getPosition() {
    return position;
  }
  public void setPosition(Double position) {
    this.position = position;
  }

  /**
   **/
  @ApiModelProperty(required = true, value = "")
  public Double getLevel() {
    return level;
  }
  public void setLevel(Double level) {
    this.level = level;
  }

  /**
   **/
  @ApiModelProperty(required = true, value = "")
  public Double getChildrenCount() {
    return childrenCount;
  }
  public void setChildrenCount(Double childrenCount) {
    this.childrenCount = childrenCount;
  }

  /**
   **/
  @ApiModelProperty(required = true, value = "")
  public Double getStoreId() {
    return storeId;
  }
  public void setStoreId(Double storeId) {
    this.storeId = storeId;
  }

  /**
   **/
  @ApiModelProperty(value = "")
  public String getAllChildren() {
    return allChildren;
  }
  public void setAllChildren(String allChildren) {
    this.allChildren = allChildren;
  }

  /**
   **/
  @ApiModelProperty(value = "")
  public String getAvailableSortBy() {
    return availableSortBy;
  }
  public void setAvailableSortBy(String availableSortBy) {
    this.availableSortBy = availableSortBy;
  }

  /**
   **/
  @ApiModelProperty(value = "")
  public String getChildren() {
    return children;
  }
  public void setChildren(String children) {
    this.children = children;
  }

  /**
   **/
  @ApiModelProperty(value = "")
  public Double getCustomApplyToProducts() {
    return customApplyToProducts;
  }
  public void setCustomApplyToProducts(Double customApplyToProducts) {
    this.customApplyToProducts = customApplyToProducts;
  }

  /**
   **/
  @ApiModelProperty(value = "")
  public String getCustomDesign() {
    return customDesign;
  }
  public void setCustomDesign(String customDesign) {
    this.customDesign = customDesign;
  }

  /**
   **/
  @ApiModelProperty(value = "")
  public String getCustomDesignFrom() {
    return customDesignFrom;
  }
  public void setCustomDesignFrom(Date customDesignFrom) {
    this.customDesignFrom = customDesignFrom.toString();
  }

  /**
   **/
  @ApiModelProperty(value = "")
  public String getCustomDesignTo() {
    return customDesignTo;
  }
  public void setCustomDesignTo(Date customDesignTo) {
    this.customDesignTo = customDesignTo.toString();
  }

  /**
   **/
  @ApiModelProperty(value = "")
  public String getCustomLayoutUpdate() {
    return customLayoutUpdate;
  }
  public void setCustomLayoutUpdate(String customLayoutUpdate) {
    this.customLayoutUpdate = customLayoutUpdate;
  }

  /**
   **/
  @ApiModelProperty(value = "")
  public Double getCustomUseParentSettings() {
    return customUseParentSettings;
  }
  public void setCustomUseParentSettings(Double customUseParentSettings) {
    this.customUseParentSettings = customUseParentSettings;
  }

  /**
   **/
  @ApiModelProperty(value = "")
  public String getDefaultSortBy() {
    return defaultSortBy;
  }
  public void setDefaultSortBy(String defaultSortBy) {
    this.defaultSortBy = defaultSortBy;
  }

  /**
   **/
  @ApiModelProperty(value = "")
  public String getDescription() {
    return description;
  }
  public void setDescription(String description) {
    this.description = description;
  }

  /**
   **/
  @ApiModelProperty(value = "")
  public String getDisplayMode() {
    return displayMode;
  }
  public void setDisplayMode(String displayMode) {
    this.displayMode = displayMode;
  }

  /**
   **/
  @ApiModelProperty(value = "")
  public String getFilterPriceRange() {
    return filterPriceRange;
  }
  public void setFilterPriceRange(String filterPriceRange) {
    this.filterPriceRange = filterPriceRange;
  }

  /**
   **/
  @ApiModelProperty(value = "")
  public String getImage() {
    return image;
  }
  public void setImage(String image) {
    this.image = image;
  }

  /**
   **/
  @ApiModelProperty(value = "")
  public Double getIncludeInMenu() {
    return includeInMenu;
  }
  public void setIncludeInMenu(Double includeInMenu) {
    this.includeInMenu = includeInMenu;
  }

  /**
   **/
  @ApiModelProperty(value = "")
  public Double getIsActive() {
    return isActive;
  }
  public void setIsActive(Double isActive) {
    this.isActive = isActive;
  }

  /**
   **/
  @ApiModelProperty(value = "")
  public Double getIsAnchor() {
    return isAnchor;
  }
  public void setIsAnchor(Double isAnchor) {
    this.isAnchor = isAnchor;
  }

  /**
   **/
  @ApiModelProperty(value = "")
  public Double getLandingPage() {
    return landingPage;
  }
  public void setLandingPage(Double landingPage) {
    this.landingPage = landingPage;
  }

  /**
   **/
  @ApiModelProperty(value = "")
  public String getMetaDescription() {
    return metaDescription;
  }
  public void setMetaDescription(String metaDescription) {
    this.metaDescription = metaDescription;
  }

  /**
   **/
  @ApiModelProperty(value = "")
  public String getMetaKeywords() {
    return metaKeywords;
  }
  public void setMetaKeywords(String metaKeywords) {
    this.metaKeywords = metaKeywords;
  }

  /**
   **/
  @ApiModelProperty(value = "")
  public String getMetaTitle() {
    return metaTitle;
  }
  public void setMetaTitle(String metaTitle) {
    this.metaTitle = metaTitle;
  }

  /**
   **/
  @ApiModelProperty(value = "")
  public String getName() {
    return name;
  }
  public void setName(String name) {
    this.name = name;
  }

  /**
   **/
  @ApiModelProperty(value = "")
  public String getPageLayout() {
    return pageLayout;
  }
  public void setPageLayout(String pageLayout) {
    this.pageLayout = pageLayout;
  }

  /**
   **/
  @ApiModelProperty(value = "")
  public String getPathInStore() {
    return pathInStore;
  }
  public void setPathInStore(String pathInStore) {
    this.pathInStore = pathInStore;
  }

  /**
   **/
  @ApiModelProperty(value = "")
  public String getThumbnail() {
    return thumbnail;
  }
  public void setThumbnail(String thumbnail) {
    this.thumbnail = thumbnail;
  }

  /**
   **/
  @ApiModelProperty(value = "")
  public String getUmmCatLabel() {
    return ummCatLabel;
  }
  public void setUmmCatLabel(String ummCatLabel) {
    this.ummCatLabel = ummCatLabel;
  }

  /**
   **/
  @ApiModelProperty(value = "")
  public String getUmmCatTarget() {
    return ummCatTarget;
  }
  public void setUmmCatTarget(String ummCatTarget) {
    this.ummCatTarget = ummCatTarget;
  }

  /**
   **/
  @ApiModelProperty(value = "")
  public String getUmmDdBlocks() {
    return ummDdBlocks;
  }
  public void setUmmDdBlocks(String ummDdBlocks) {
    this.ummDdBlocks = ummDdBlocks;
  }

  /**
   **/
  @ApiModelProperty(value = "")
  public Double getUmmDdColumns() {
    return ummDdColumns;
  }
  public void setUmmDdColumns(Double ummDdColumns) {
    this.ummDdColumns = ummDdColumns;
  }

  /**
   **/
  @ApiModelProperty(value = "")
  public String getUmmDdProportions() {
    return ummDdProportions;
  }
  public void setUmmDdProportions(String ummDdProportions) {
    this.ummDdProportions = ummDdProportions;
  }

  /**
   **/
  @ApiModelProperty(value = "")
  public String getUmmDdType() {
    return ummDdType;
  }
  public void setUmmDdType(String ummDdType) {
    this.ummDdType = ummDdType;
  }

  /**
   **/
  @ApiModelProperty(value = "")
  public String getUmmDdWidth() {
    return ummDdWidth;
  }
  public void setUmmDdWidth(String ummDdWidth) {
    this.ummDdWidth = ummDdWidth;
  }

  /**
   **/
  @ApiModelProperty(value = "")
  public String getUrlKey() {
    return urlKey;
  }
  public void setUrlKey(String urlKey) {
    this.urlKey = urlKey;
  }

  /**
   **/
  @ApiModelProperty(value = "")
  public String getUrlPath() {
    return urlPath;
  }
  public void setUrlPath(String urlPath) {
    this.urlPath = urlPath;
  }

  /**
   **/
  @ApiModelProperty(value = "")
  public List<Object> getCatalogProducts() {
    return catalogProducts;
  }
  public void setCatalogProducts(List<Object> catalogProducts) {
    this.catalogProducts = catalogProducts;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    CatalogCategory catalogCategory = (CatalogCategory) o;
    return (this.id == null ? catalogCategory.id == null : this.id.equals(catalogCategory.id)) &&
        (this.parentId == null ? catalogCategory.parentId == null : this.parentId.equals(catalogCategory.parentId)) &&
        (this.createdAt == null ? catalogCategory.createdAt == null : this.createdAt.equals(catalogCategory.createdAt)) &&
        (this.updatedAt == null ? catalogCategory.updatedAt == null : this.updatedAt.equals(catalogCategory.updatedAt)) &&
        (this.path == null ? catalogCategory.path == null : this.path.equals(catalogCategory.path)) &&
        (this.position == null ? catalogCategory.position == null : this.position.equals(catalogCategory.position)) &&
        (this.level == null ? catalogCategory.level == null : this.level.equals(catalogCategory.level)) &&
        (this.childrenCount == null ? catalogCategory.childrenCount == null : this.childrenCount.equals(catalogCategory.childrenCount)) &&
        (this.storeId == null ? catalogCategory.storeId == null : this.storeId.equals(catalogCategory.storeId)) &&
        (this.allChildren == null ? catalogCategory.allChildren == null : this.allChildren.equals(catalogCategory.allChildren)) &&
        (this.availableSortBy == null ? catalogCategory.availableSortBy == null : this.availableSortBy.equals(catalogCategory.availableSortBy)) &&
        (this.children == null ? catalogCategory.children == null : this.children.equals(catalogCategory.children)) &&
        (this.customApplyToProducts == null ? catalogCategory.customApplyToProducts == null : this.customApplyToProducts.equals(catalogCategory.customApplyToProducts)) &&
        (this.customDesign == null ? catalogCategory.customDesign == null : this.customDesign.equals(catalogCategory.customDesign)) &&
        (this.customDesignFrom == null ? catalogCategory.customDesignFrom == null : this.customDesignFrom.equals(catalogCategory.customDesignFrom)) &&
        (this.customDesignTo == null ? catalogCategory.customDesignTo == null : this.customDesignTo.equals(catalogCategory.customDesignTo)) &&
        (this.customLayoutUpdate == null ? catalogCategory.customLayoutUpdate == null : this.customLayoutUpdate.equals(catalogCategory.customLayoutUpdate)) &&
        (this.customUseParentSettings == null ? catalogCategory.customUseParentSettings == null : this.customUseParentSettings.equals(catalogCategory.customUseParentSettings)) &&
        (this.defaultSortBy == null ? catalogCategory.defaultSortBy == null : this.defaultSortBy.equals(catalogCategory.defaultSortBy)) &&
        (this.description == null ? catalogCategory.description == null : this.description.equals(catalogCategory.description)) &&
        (this.displayMode == null ? catalogCategory.displayMode == null : this.displayMode.equals(catalogCategory.displayMode)) &&
        (this.filterPriceRange == null ? catalogCategory.filterPriceRange == null : this.filterPriceRange.equals(catalogCategory.filterPriceRange)) &&
        (this.image == null ? catalogCategory.image == null : this.image.equals(catalogCategory.image)) &&
        (this.includeInMenu == null ? catalogCategory.includeInMenu == null : this.includeInMenu.equals(catalogCategory.includeInMenu)) &&
        (this.isActive == null ? catalogCategory.isActive == null : this.isActive.equals(catalogCategory.isActive)) &&
        (this.isAnchor == null ? catalogCategory.isAnchor == null : this.isAnchor.equals(catalogCategory.isAnchor)) &&
        (this.landingPage == null ? catalogCategory.landingPage == null : this.landingPage.equals(catalogCategory.landingPage)) &&
        (this.metaDescription == null ? catalogCategory.metaDescription == null : this.metaDescription.equals(catalogCategory.metaDescription)) &&
        (this.metaKeywords == null ? catalogCategory.metaKeywords == null : this.metaKeywords.equals(catalogCategory.metaKeywords)) &&
        (this.metaTitle == null ? catalogCategory.metaTitle == null : this.metaTitle.equals(catalogCategory.metaTitle)) &&
        (this.name == null ? catalogCategory.name == null : this.name.equals(catalogCategory.name)) &&
        (this.pageLayout == null ? catalogCategory.pageLayout == null : this.pageLayout.equals(catalogCategory.pageLayout)) &&
        (this.pathInStore == null ? catalogCategory.pathInStore == null : this.pathInStore.equals(catalogCategory.pathInStore)) &&
        (this.thumbnail == null ? catalogCategory.thumbnail == null : this.thumbnail.equals(catalogCategory.thumbnail)) &&
        (this.ummCatLabel == null ? catalogCategory.ummCatLabel == null : this.ummCatLabel.equals(catalogCategory.ummCatLabel)) &&
        (this.ummCatTarget == null ? catalogCategory.ummCatTarget == null : this.ummCatTarget.equals(catalogCategory.ummCatTarget)) &&
        (this.ummDdBlocks == null ? catalogCategory.ummDdBlocks == null : this.ummDdBlocks.equals(catalogCategory.ummDdBlocks)) &&
        (this.ummDdColumns == null ? catalogCategory.ummDdColumns == null : this.ummDdColumns.equals(catalogCategory.ummDdColumns)) &&
        (this.ummDdProportions == null ? catalogCategory.ummDdProportions == null : this.ummDdProportions.equals(catalogCategory.ummDdProportions)) &&
        (this.ummDdType == null ? catalogCategory.ummDdType == null : this.ummDdType.equals(catalogCategory.ummDdType)) &&
        (this.ummDdWidth == null ? catalogCategory.ummDdWidth == null : this.ummDdWidth.equals(catalogCategory.ummDdWidth)) &&
        (this.urlKey == null ? catalogCategory.urlKey == null : this.urlKey.equals(catalogCategory.urlKey)) &&
        (this.urlPath == null ? catalogCategory.urlPath == null : this.urlPath.equals(catalogCategory.urlPath)) &&
        (this.catalogProducts == null ? catalogCategory.catalogProducts == null : this.catalogProducts.equals(catalogCategory.catalogProducts));
  }

  @Override
  public int hashCode() {
    int result = 17;
    result = 31 * result + (id == null ? 0: id.hashCode());
    result = 31 * result + (parentId == null ? 0: parentId.hashCode());
    result = 31 * result + (createdAt == null ? 0: createdAt.hashCode());
    result = 31 * result + (updatedAt == null ? 0: updatedAt.hashCode());
    result = 31 * result + (path == null ? 0: path.hashCode());
    result = 31 * result + (position == null ? 0: position.hashCode());
    result = 31 * result + (level == null ? 0: level.hashCode());
    result = 31 * result + (childrenCount == null ? 0: childrenCount.hashCode());
    result = 31 * result + (storeId == null ? 0: storeId.hashCode());
    result = 31 * result + (allChildren == null ? 0: allChildren.hashCode());
    result = 31 * result + (availableSortBy == null ? 0: availableSortBy.hashCode());
    result = 31 * result + (children == null ? 0: children.hashCode());
    result = 31 * result + (customApplyToProducts == null ? 0: customApplyToProducts.hashCode());
    result = 31 * result + (customDesign == null ? 0: customDesign.hashCode());
    result = 31 * result + (customDesignFrom == null ? 0: customDesignFrom.hashCode());
    result = 31 * result + (customDesignTo == null ? 0: customDesignTo.hashCode());
    result = 31 * result + (customLayoutUpdate == null ? 0: customLayoutUpdate.hashCode());
    result = 31 * result + (customUseParentSettings == null ? 0: customUseParentSettings.hashCode());
    result = 31 * result + (defaultSortBy == null ? 0: defaultSortBy.hashCode());
    result = 31 * result + (description == null ? 0: description.hashCode());
    result = 31 * result + (displayMode == null ? 0: displayMode.hashCode());
    result = 31 * result + (filterPriceRange == null ? 0: filterPriceRange.hashCode());
    result = 31 * result + (image == null ? 0: image.hashCode());
    result = 31 * result + (includeInMenu == null ? 0: includeInMenu.hashCode());
    result = 31 * result + (isActive == null ? 0: isActive.hashCode());
    result = 31 * result + (isAnchor == null ? 0: isAnchor.hashCode());
    result = 31 * result + (landingPage == null ? 0: landingPage.hashCode());
    result = 31 * result + (metaDescription == null ? 0: metaDescription.hashCode());
    result = 31 * result + (metaKeywords == null ? 0: metaKeywords.hashCode());
    result = 31 * result + (metaTitle == null ? 0: metaTitle.hashCode());
    result = 31 * result + (name == null ? 0: name.hashCode());
    result = 31 * result + (pageLayout == null ? 0: pageLayout.hashCode());
    result = 31 * result + (pathInStore == null ? 0: pathInStore.hashCode());
    result = 31 * result + (thumbnail == null ? 0: thumbnail.hashCode());
    result = 31 * result + (ummCatLabel == null ? 0: ummCatLabel.hashCode());
    result = 31 * result + (ummCatTarget == null ? 0: ummCatTarget.hashCode());
    result = 31 * result + (ummDdBlocks == null ? 0: ummDdBlocks.hashCode());
    result = 31 * result + (ummDdColumns == null ? 0: ummDdColumns.hashCode());
    result = 31 * result + (ummDdProportions == null ? 0: ummDdProportions.hashCode());
    result = 31 * result + (ummDdType == null ? 0: ummDdType.hashCode());
    result = 31 * result + (ummDdWidth == null ? 0: ummDdWidth.hashCode());
    result = 31 * result + (urlKey == null ? 0: urlKey.hashCode());
    result = 31 * result + (urlPath == null ? 0: urlPath.hashCode());
    result = 31 * result + (catalogProducts == null ? 0: catalogProducts.hashCode());
    return result;
  }

  @Override
  public String toString()  {
    StringBuilder sb = new StringBuilder();
    sb.append("class CatalogCategory {\n");
    
    sb.append("  id: ").append(id).append("\n");
    sb.append("  parentId: ").append(parentId).append("\n");
    sb.append("  createdAt: ").append(createdAt).append("\n");
    sb.append("  updatedAt: ").append(updatedAt).append("\n");
    sb.append("  path: ").append(path).append("\n");
    sb.append("  position: ").append(position).append("\n");
    sb.append("  level: ").append(level).append("\n");
    sb.append("  childrenCount: ").append(childrenCount).append("\n");
    sb.append("  storeId: ").append(storeId).append("\n");
    sb.append("  allChildren: ").append(allChildren).append("\n");
    sb.append("  availableSortBy: ").append(availableSortBy).append("\n");
    sb.append("  children: ").append(children).append("\n");
    sb.append("  customApplyToProducts: ").append(customApplyToProducts).append("\n");
    sb.append("  customDesign: ").append(customDesign).append("\n");
    sb.append("  customDesignFrom: ").append(customDesignFrom).append("\n");
    sb.append("  customDesignTo: ").append(customDesignTo).append("\n");
    sb.append("  customLayoutUpdate: ").append(customLayoutUpdate).append("\n");
    sb.append("  customUseParentSettings: ").append(customUseParentSettings).append("\n");
    sb.append("  defaultSortBy: ").append(defaultSortBy).append("\n");
    sb.append("  description: ").append(description).append("\n");
    sb.append("  displayMode: ").append(displayMode).append("\n");
    sb.append("  filterPriceRange: ").append(filterPriceRange).append("\n");
    sb.append("  image: ").append(image).append("\n");
    sb.append("  includeInMenu: ").append(includeInMenu).append("\n");
    sb.append("  isActive: ").append(isActive).append("\n");
    sb.append("  isAnchor: ").append(isAnchor).append("\n");
    sb.append("  landingPage: ").append(landingPage).append("\n");
    sb.append("  metaDescription: ").append(metaDescription).append("\n");
    sb.append("  metaKeywords: ").append(metaKeywords).append("\n");
    sb.append("  metaTitle: ").append(metaTitle).append("\n");
    sb.append("  name: ").append(name).append("\n");
    sb.append("  pageLayout: ").append(pageLayout).append("\n");
    sb.append("  pathInStore: ").append(pathInStore).append("\n");
    sb.append("  thumbnail: ").append(thumbnail).append("\n");
    sb.append("  ummCatLabel: ").append(ummCatLabel).append("\n");
    sb.append("  ummCatTarget: ").append(ummCatTarget).append("\n");
    sb.append("  ummDdBlocks: ").append(ummDdBlocks).append("\n");
    sb.append("  ummDdColumns: ").append(ummDdColumns).append("\n");
    sb.append("  ummDdProportions: ").append(ummDdProportions).append("\n");
    sb.append("  ummDdType: ").append(ummDdType).append("\n");
    sb.append("  ummDdWidth: ").append(ummDdWidth).append("\n");
    sb.append("  urlKey: ").append(urlKey).append("\n");
    sb.append("  urlPath: ").append(urlPath).append("\n");
    sb.append("  catalogProducts: ").append(catalogProducts).append("\n");
    sb.append("}\n");
    return sb.toString();
  }
}
