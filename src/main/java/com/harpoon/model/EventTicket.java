/**
 * harpoon-api
 * Harpoon API to integrate with all the Harpoon services.  You can find out more about Harpoon      at <a href='https://harpoonconnect.com'>https://harpoonconnect.com</a>, #harpoonConnect.
 *
 * OpenAPI spec version: 1.1.1
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.harpoon.model;


import io.swagger.annotations.*;
import com.google.gson.annotations.SerializedName;


@ApiModel(description = "")
public class EventTicket  {
  
  @SerializedName("name")
  private String name = null;
  @SerializedName("price")
  private Double price = 0.0;
  @SerializedName("qtyBought")
  private Double qtyBought = 0.0;
  @SerializedName("qtyTotal")
  private Double qtyTotal = 0.0;
  @SerializedName("qtyLeft")
  private Double qtyLeft = 0.0;
  @SerializedName("id")
  private Double id = null;

  /**
   **/
  @ApiModelProperty(value = "")
  public String getName() {
    return name;
  }
  public void setName(String name) {
    this.name = name;
  }

  /**
   **/
  @ApiModelProperty(value = "")
  public Double getPrice() {
    return price;
  }
  public void setPrice(Double price) {
    this.price = price;
  }

  /**
   **/
  @ApiModelProperty(value = "")
  public Double getQtyBought() {
    return qtyBought;
  }
  public void setQtyBought(Double qtyBought) {
    this.qtyBought = qtyBought;
  }

  /**
   **/
  @ApiModelProperty(value = "")
  public Double getQtyTotal() {
    return qtyTotal;
  }
  public void setQtyTotal(Double qtyTotal) {
    this.qtyTotal = qtyTotal;
  }

  /**
   **/
  @ApiModelProperty(value = "")
  public Double getQtyLeft() {
    return qtyLeft;
  }
  public void setQtyLeft(Double qtyLeft) {
    this.qtyLeft = qtyLeft;
  }

  /**
   **/
  @ApiModelProperty(value = "")
  public Double getId() {
    return id;
  }
  public void setId(Double id) {
    this.id = id;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    EventTicket eventTicket = (EventTicket) o;
    return (this.name == null ? eventTicket.name == null : this.name.equals(eventTicket.name)) &&
        (this.price == null ? eventTicket.price == null : this.price.equals(eventTicket.price)) &&
        (this.qtyBought == null ? eventTicket.qtyBought == null : this.qtyBought.equals(eventTicket.qtyBought)) &&
        (this.qtyTotal == null ? eventTicket.qtyTotal == null : this.qtyTotal.equals(eventTicket.qtyTotal)) &&
        (this.qtyLeft == null ? eventTicket.qtyLeft == null : this.qtyLeft.equals(eventTicket.qtyLeft)) &&
        (this.id == null ? eventTicket.id == null : this.id.equals(eventTicket.id));
  }

  @Override
  public int hashCode() {
    int result = 17;
    result = 31 * result + (name == null ? 0: name.hashCode());
    result = 31 * result + (price == null ? 0: price.hashCode());
    result = 31 * result + (qtyBought == null ? 0: qtyBought.hashCode());
    result = 31 * result + (qtyTotal == null ? 0: qtyTotal.hashCode());
    result = 31 * result + (qtyLeft == null ? 0: qtyLeft.hashCode());
    result = 31 * result + (id == null ? 0: id.hashCode());
    return result;
  }

  @Override
  public String toString()  {
    StringBuilder sb = new StringBuilder();
    sb.append("class EventTicket {\n");
    
    sb.append("  name: ").append(name).append("\n");
    sb.append("  price: ").append(price).append("\n");
    sb.append("  qtyBought: ").append(qtyBought).append("\n");
    sb.append("  qtyTotal: ").append(qtyTotal).append("\n");
    sb.append("  qtyLeft: ").append(qtyLeft).append("\n");
    sb.append("  id: ").append(id).append("\n");
    sb.append("}\n");
    return sb.toString();
  }
}
