
# FacebookEvent

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**node** | **String** |  |  [optional]
**category** | **String** |  |  [optional]
**updatedTime** | **String** |  |  [optional]
**maybeCount** | **Double** |  |  [optional]
**noreplyCount** | **Double** |  |  [optional]
**attendingCount** | **Double** |  |  [optional]
**place** | **Object** |  |  [optional]
**id** | **Double** |  |  [optional]



