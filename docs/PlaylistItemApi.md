# PlaylistItemApi

All URIs are relative to *https://apicdn.harpoonconnect.com/v1.1.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**playlistItemCount**](PlaylistItemApi.md#playlistItemCount) | **GET** /PlaylistItems/count | Count instances of the model matched by where from the data source.
[**playlistItemCreate**](PlaylistItemApi.md#playlistItemCreate) | **POST** /PlaylistItems | Create a new instance of the model and persist it into the data source.
[**playlistItemCreateChangeStreamGetPlaylistItemsChangeStream**](PlaylistItemApi.md#playlistItemCreateChangeStreamGetPlaylistItemsChangeStream) | **GET** /PlaylistItems/change-stream | Create a change stream.
[**playlistItemCreateChangeStreamPostPlaylistItemsChangeStream**](PlaylistItemApi.md#playlistItemCreateChangeStreamPostPlaylistItemsChangeStream) | **POST** /PlaylistItems/change-stream | Create a change stream.
[**playlistItemDeleteById**](PlaylistItemApi.md#playlistItemDeleteById) | **DELETE** /PlaylistItems/{id} | Delete a model instance by {{id}} from the data source.
[**playlistItemExistsGetPlaylistItemsidExists**](PlaylistItemApi.md#playlistItemExistsGetPlaylistItemsidExists) | **GET** /PlaylistItems/{id}/exists | Check whether a model instance exists in the data source.
[**playlistItemExistsHeadPlaylistItemsid**](PlaylistItemApi.md#playlistItemExistsHeadPlaylistItemsid) | **HEAD** /PlaylistItems/{id} | Check whether a model instance exists in the data source.
[**playlistItemFind**](PlaylistItemApi.md#playlistItemFind) | **GET** /PlaylistItems | Find all instances of the model matched by filter from the data source.
[**playlistItemFindById**](PlaylistItemApi.md#playlistItemFindById) | **GET** /PlaylistItems/{id} | Find a model instance by {{id}} from the data source.
[**playlistItemFindOne**](PlaylistItemApi.md#playlistItemFindOne) | **GET** /PlaylistItems/findOne | Find first instance of the model matched by filter from the data source.
[**playlistItemPrototypeCountPlayerSources**](PlaylistItemApi.md#playlistItemPrototypeCountPlayerSources) | **GET** /PlaylistItems/{id}/playerSources/count | Counts playerSources of PlaylistItem.
[**playlistItemPrototypeCountPlayerTracks**](PlaylistItemApi.md#playlistItemPrototypeCountPlayerTracks) | **GET** /PlaylistItems/{id}/playerTracks/count | Counts playerTracks of PlaylistItem.
[**playlistItemPrototypeCountRadioShows**](PlaylistItemApi.md#playlistItemPrototypeCountRadioShows) | **GET** /PlaylistItems/{id}/radioShows/count | Counts radioShows of PlaylistItem.
[**playlistItemPrototypeCreatePlayerSources**](PlaylistItemApi.md#playlistItemPrototypeCreatePlayerSources) | **POST** /PlaylistItems/{id}/playerSources | Creates a new instance in playerSources of this model.
[**playlistItemPrototypeCreatePlayerTracks**](PlaylistItemApi.md#playlistItemPrototypeCreatePlayerTracks) | **POST** /PlaylistItems/{id}/playerTracks | Creates a new instance in playerTracks of this model.
[**playlistItemPrototypeCreateRadioShows**](PlaylistItemApi.md#playlistItemPrototypeCreateRadioShows) | **POST** /PlaylistItems/{id}/radioShows | Creates a new instance in radioShows of this model.
[**playlistItemPrototypeDeletePlayerSources**](PlaylistItemApi.md#playlistItemPrototypeDeletePlayerSources) | **DELETE** /PlaylistItems/{id}/playerSources | Deletes all playerSources of this model.
[**playlistItemPrototypeDeletePlayerTracks**](PlaylistItemApi.md#playlistItemPrototypeDeletePlayerTracks) | **DELETE** /PlaylistItems/{id}/playerTracks | Deletes all playerTracks of this model.
[**playlistItemPrototypeDeleteRadioShows**](PlaylistItemApi.md#playlistItemPrototypeDeleteRadioShows) | **DELETE** /PlaylistItems/{id}/radioShows | Deletes all radioShows of this model.
[**playlistItemPrototypeDestroyByIdPlayerSources**](PlaylistItemApi.md#playlistItemPrototypeDestroyByIdPlayerSources) | **DELETE** /PlaylistItems/{id}/playerSources/{fk} | Delete a related item by id for playerSources.
[**playlistItemPrototypeDestroyByIdPlayerTracks**](PlaylistItemApi.md#playlistItemPrototypeDestroyByIdPlayerTracks) | **DELETE** /PlaylistItems/{id}/playerTracks/{fk} | Delete a related item by id for playerTracks.
[**playlistItemPrototypeDestroyByIdRadioShows**](PlaylistItemApi.md#playlistItemPrototypeDestroyByIdRadioShows) | **DELETE** /PlaylistItems/{id}/radioShows/{fk} | Delete a related item by id for radioShows.
[**playlistItemPrototypeFindByIdPlayerSources**](PlaylistItemApi.md#playlistItemPrototypeFindByIdPlayerSources) | **GET** /PlaylistItems/{id}/playerSources/{fk} | Find a related item by id for playerSources.
[**playlistItemPrototypeFindByIdPlayerTracks**](PlaylistItemApi.md#playlistItemPrototypeFindByIdPlayerTracks) | **GET** /PlaylistItems/{id}/playerTracks/{fk} | Find a related item by id for playerTracks.
[**playlistItemPrototypeFindByIdRadioShows**](PlaylistItemApi.md#playlistItemPrototypeFindByIdRadioShows) | **GET** /PlaylistItems/{id}/radioShows/{fk} | Find a related item by id for radioShows.
[**playlistItemPrototypeGetPlayerSources**](PlaylistItemApi.md#playlistItemPrototypeGetPlayerSources) | **GET** /PlaylistItems/{id}/playerSources | Queries playerSources of PlaylistItem.
[**playlistItemPrototypeGetPlayerTracks**](PlaylistItemApi.md#playlistItemPrototypeGetPlayerTracks) | **GET** /PlaylistItems/{id}/playerTracks | Queries playerTracks of PlaylistItem.
[**playlistItemPrototypeGetPlaylist**](PlaylistItemApi.md#playlistItemPrototypeGetPlaylist) | **GET** /PlaylistItems/{id}/playlist | Fetches belongsTo relation playlist.
[**playlistItemPrototypeGetRadioShows**](PlaylistItemApi.md#playlistItemPrototypeGetRadioShows) | **GET** /PlaylistItems/{id}/radioShows | Queries radioShows of PlaylistItem.
[**playlistItemPrototypeUpdateAttributesPatchPlaylistItemsid**](PlaylistItemApi.md#playlistItemPrototypeUpdateAttributesPatchPlaylistItemsid) | **PATCH** /PlaylistItems/{id} | Patch attributes for a model instance and persist it into the data source.
[**playlistItemPrototypeUpdateAttributesPutPlaylistItemsid**](PlaylistItemApi.md#playlistItemPrototypeUpdateAttributesPutPlaylistItemsid) | **PUT** /PlaylistItems/{id} | Patch attributes for a model instance and persist it into the data source.
[**playlistItemPrototypeUpdateByIdPlayerSources**](PlaylistItemApi.md#playlistItemPrototypeUpdateByIdPlayerSources) | **PUT** /PlaylistItems/{id}/playerSources/{fk} | Update a related item by id for playerSources.
[**playlistItemPrototypeUpdateByIdPlayerTracks**](PlaylistItemApi.md#playlistItemPrototypeUpdateByIdPlayerTracks) | **PUT** /PlaylistItems/{id}/playerTracks/{fk} | Update a related item by id for playerTracks.
[**playlistItemPrototypeUpdateByIdRadioShows**](PlaylistItemApi.md#playlistItemPrototypeUpdateByIdRadioShows) | **PUT** /PlaylistItems/{id}/radioShows/{fk} | Update a related item by id for radioShows.
[**playlistItemReplaceById**](PlaylistItemApi.md#playlistItemReplaceById) | **POST** /PlaylistItems/{id}/replace | Replace attributes for a model instance and persist it into the data source.
[**playlistItemReplaceOrCreate**](PlaylistItemApi.md#playlistItemReplaceOrCreate) | **POST** /PlaylistItems/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
[**playlistItemUpdateAll**](PlaylistItemApi.md#playlistItemUpdateAll) | **POST** /PlaylistItems/update | Update instances of the model matched by {{where}} from the data source.
[**playlistItemUpsertPatchPlaylistItems**](PlaylistItemApi.md#playlistItemUpsertPatchPlaylistItems) | **PATCH** /PlaylistItems | Patch an existing model instance or insert a new one into the data source.
[**playlistItemUpsertPutPlaylistItems**](PlaylistItemApi.md#playlistItemUpsertPutPlaylistItems) | **PUT** /PlaylistItems | Patch an existing model instance or insert a new one into the data source.
[**playlistItemUpsertWithWhere**](PlaylistItemApi.md#playlistItemUpsertWithWhere) | **POST** /PlaylistItems/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.


<a name="playlistItemCount"></a>
# **playlistItemCount**
> InlineResponse2001 playlistItemCount(where)

Count instances of the model matched by where from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.PlaylistItemApi;

PlaylistItemApi apiInstance = new PlaylistItemApi();
String where = "where_example"; // String | Criteria to match model instances
try {
    InlineResponse2001 result = apiInstance.playlistItemCount(where);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PlaylistItemApi#playlistItemCount");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional]

### Return type

[**InlineResponse2001**](InlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playlistItemCreate"></a>
# **playlistItemCreate**
> PlaylistItem playlistItemCreate(data)

Create a new instance of the model and persist it into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.PlaylistItemApi;

PlaylistItemApi apiInstance = new PlaylistItemApi();
PlaylistItem data = new PlaylistItem(); // PlaylistItem | Model instance data
try {
    PlaylistItem result = apiInstance.playlistItemCreate(data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PlaylistItemApi#playlistItemCreate");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**PlaylistItem**](PlaylistItem.md)| Model instance data | [optional]

### Return type

[**PlaylistItem**](PlaylistItem.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playlistItemCreateChangeStreamGetPlaylistItemsChangeStream"></a>
# **playlistItemCreateChangeStreamGetPlaylistItemsChangeStream**
> File playlistItemCreateChangeStreamGetPlaylistItemsChangeStream(options)

Create a change stream.

### Example
```java
// Import classes:
//import com.harpoon.api.PlaylistItemApi;

PlaylistItemApi apiInstance = new PlaylistItemApi();
String options = "options_example"; // String | 
try {
    File result = apiInstance.playlistItemCreateChangeStreamGetPlaylistItemsChangeStream(options);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PlaylistItemApi#playlistItemCreateChangeStreamGetPlaylistItemsChangeStream");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **String**|  | [optional]

### Return type

[**File**](File.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playlistItemCreateChangeStreamPostPlaylistItemsChangeStream"></a>
# **playlistItemCreateChangeStreamPostPlaylistItemsChangeStream**
> File playlistItemCreateChangeStreamPostPlaylistItemsChangeStream(options)

Create a change stream.

### Example
```java
// Import classes:
//import com.harpoon.api.PlaylistItemApi;

PlaylistItemApi apiInstance = new PlaylistItemApi();
String options = "options_example"; // String | 
try {
    File result = apiInstance.playlistItemCreateChangeStreamPostPlaylistItemsChangeStream(options);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PlaylistItemApi#playlistItemCreateChangeStreamPostPlaylistItemsChangeStream");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **String**|  | [optional]

### Return type

[**File**](File.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playlistItemDeleteById"></a>
# **playlistItemDeleteById**
> Object playlistItemDeleteById(id)

Delete a model instance by {{id}} from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.PlaylistItemApi;

PlaylistItemApi apiInstance = new PlaylistItemApi();
String id = "id_example"; // String | Model id
try {
    Object result = apiInstance.playlistItemDeleteById(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PlaylistItemApi#playlistItemDeleteById");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |

### Return type

**Object**

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playlistItemExistsGetPlaylistItemsidExists"></a>
# **playlistItemExistsGetPlaylistItemsidExists**
> InlineResponse2003 playlistItemExistsGetPlaylistItemsidExists(id)

Check whether a model instance exists in the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.PlaylistItemApi;

PlaylistItemApi apiInstance = new PlaylistItemApi();
String id = "id_example"; // String | Model id
try {
    InlineResponse2003 result = apiInstance.playlistItemExistsGetPlaylistItemsidExists(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PlaylistItemApi#playlistItemExistsGetPlaylistItemsidExists");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |

### Return type

[**InlineResponse2003**](InlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playlistItemExistsHeadPlaylistItemsid"></a>
# **playlistItemExistsHeadPlaylistItemsid**
> InlineResponse2003 playlistItemExistsHeadPlaylistItemsid(id)

Check whether a model instance exists in the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.PlaylistItemApi;

PlaylistItemApi apiInstance = new PlaylistItemApi();
String id = "id_example"; // String | Model id
try {
    InlineResponse2003 result = apiInstance.playlistItemExistsHeadPlaylistItemsid(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PlaylistItemApi#playlistItemExistsHeadPlaylistItemsid");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |

### Return type

[**InlineResponse2003**](InlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playlistItemFind"></a>
# **playlistItemFind**
> List&lt;PlaylistItem&gt; playlistItemFind(filter)

Find all instances of the model matched by filter from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.PlaylistItemApi;

PlaylistItemApi apiInstance = new PlaylistItemApi();
String filter = "filter_example"; // String | Filter defining fields, where, include, order, offset, and limit
try {
    List<PlaylistItem> result = apiInstance.playlistItemFind(filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PlaylistItemApi#playlistItemFind");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **String**| Filter defining fields, where, include, order, offset, and limit | [optional]

### Return type

[**List&lt;PlaylistItem&gt;**](PlaylistItem.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playlistItemFindById"></a>
# **playlistItemFindById**
> PlaylistItem playlistItemFindById(id, filter)

Find a model instance by {{id}} from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.PlaylistItemApi;

PlaylistItemApi apiInstance = new PlaylistItemApi();
String id = "id_example"; // String | Model id
String filter = "filter_example"; // String | Filter defining fields and include
try {
    PlaylistItem result = apiInstance.playlistItemFindById(id, filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PlaylistItemApi#playlistItemFindById");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |
 **filter** | **String**| Filter defining fields and include | [optional]

### Return type

[**PlaylistItem**](PlaylistItem.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playlistItemFindOne"></a>
# **playlistItemFindOne**
> PlaylistItem playlistItemFindOne(filter)

Find first instance of the model matched by filter from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.PlaylistItemApi;

PlaylistItemApi apiInstance = new PlaylistItemApi();
String filter = "filter_example"; // String | Filter defining fields, where, include, order, offset, and limit
try {
    PlaylistItem result = apiInstance.playlistItemFindOne(filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PlaylistItemApi#playlistItemFindOne");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **String**| Filter defining fields, where, include, order, offset, and limit | [optional]

### Return type

[**PlaylistItem**](PlaylistItem.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playlistItemPrototypeCountPlayerSources"></a>
# **playlistItemPrototypeCountPlayerSources**
> InlineResponse2001 playlistItemPrototypeCountPlayerSources(id, where)

Counts playerSources of PlaylistItem.

### Example
```java
// Import classes:
//import com.harpoon.api.PlaylistItemApi;

PlaylistItemApi apiInstance = new PlaylistItemApi();
String id = "id_example"; // String | PlaylistItem id
String where = "where_example"; // String | Criteria to match model instances
try {
    InlineResponse2001 result = apiInstance.playlistItemPrototypeCountPlayerSources(id, where);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PlaylistItemApi#playlistItemPrototypeCountPlayerSources");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| PlaylistItem id |
 **where** | **String**| Criteria to match model instances | [optional]

### Return type

[**InlineResponse2001**](InlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playlistItemPrototypeCountPlayerTracks"></a>
# **playlistItemPrototypeCountPlayerTracks**
> InlineResponse2001 playlistItemPrototypeCountPlayerTracks(id, where)

Counts playerTracks of PlaylistItem.

### Example
```java
// Import classes:
//import com.harpoon.api.PlaylistItemApi;

PlaylistItemApi apiInstance = new PlaylistItemApi();
String id = "id_example"; // String | PlaylistItem id
String where = "where_example"; // String | Criteria to match model instances
try {
    InlineResponse2001 result = apiInstance.playlistItemPrototypeCountPlayerTracks(id, where);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PlaylistItemApi#playlistItemPrototypeCountPlayerTracks");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| PlaylistItem id |
 **where** | **String**| Criteria to match model instances | [optional]

### Return type

[**InlineResponse2001**](InlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playlistItemPrototypeCountRadioShows"></a>
# **playlistItemPrototypeCountRadioShows**
> InlineResponse2001 playlistItemPrototypeCountRadioShows(id, where)

Counts radioShows of PlaylistItem.

### Example
```java
// Import classes:
//import com.harpoon.api.PlaylistItemApi;

PlaylistItemApi apiInstance = new PlaylistItemApi();
String id = "id_example"; // String | PlaylistItem id
String where = "where_example"; // String | Criteria to match model instances
try {
    InlineResponse2001 result = apiInstance.playlistItemPrototypeCountRadioShows(id, where);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PlaylistItemApi#playlistItemPrototypeCountRadioShows");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| PlaylistItem id |
 **where** | **String**| Criteria to match model instances | [optional]

### Return type

[**InlineResponse2001**](InlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playlistItemPrototypeCreatePlayerSources"></a>
# **playlistItemPrototypeCreatePlayerSources**
> PlayerSource playlistItemPrototypeCreatePlayerSources(id, data)

Creates a new instance in playerSources of this model.

### Example
```java
// Import classes:
//import com.harpoon.api.PlaylistItemApi;

PlaylistItemApi apiInstance = new PlaylistItemApi();
String id = "id_example"; // String | PlaylistItem id
PlayerSource data = new PlayerSource(); // PlayerSource | 
try {
    PlayerSource result = apiInstance.playlistItemPrototypeCreatePlayerSources(id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PlaylistItemApi#playlistItemPrototypeCreatePlayerSources");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| PlaylistItem id |
 **data** | [**PlayerSource**](PlayerSource.md)|  | [optional]

### Return type

[**PlayerSource**](PlayerSource.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playlistItemPrototypeCreatePlayerTracks"></a>
# **playlistItemPrototypeCreatePlayerTracks**
> PlayerTrack playlistItemPrototypeCreatePlayerTracks(id, data)

Creates a new instance in playerTracks of this model.

### Example
```java
// Import classes:
//import com.harpoon.api.PlaylistItemApi;

PlaylistItemApi apiInstance = new PlaylistItemApi();
String id = "id_example"; // String | PlaylistItem id
PlayerTrack data = new PlayerTrack(); // PlayerTrack | 
try {
    PlayerTrack result = apiInstance.playlistItemPrototypeCreatePlayerTracks(id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PlaylistItemApi#playlistItemPrototypeCreatePlayerTracks");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| PlaylistItem id |
 **data** | [**PlayerTrack**](PlayerTrack.md)|  | [optional]

### Return type

[**PlayerTrack**](PlayerTrack.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playlistItemPrototypeCreateRadioShows"></a>
# **playlistItemPrototypeCreateRadioShows**
> RadioShow playlistItemPrototypeCreateRadioShows(id, data)

Creates a new instance in radioShows of this model.

### Example
```java
// Import classes:
//import com.harpoon.api.PlaylistItemApi;

PlaylistItemApi apiInstance = new PlaylistItemApi();
String id = "id_example"; // String | PlaylistItem id
RadioShow data = new RadioShow(); // RadioShow | 
try {
    RadioShow result = apiInstance.playlistItemPrototypeCreateRadioShows(id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PlaylistItemApi#playlistItemPrototypeCreateRadioShows");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| PlaylistItem id |
 **data** | [**RadioShow**](RadioShow.md)|  | [optional]

### Return type

[**RadioShow**](RadioShow.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playlistItemPrototypeDeletePlayerSources"></a>
# **playlistItemPrototypeDeletePlayerSources**
> playlistItemPrototypeDeletePlayerSources(id)

Deletes all playerSources of this model.

### Example
```java
// Import classes:
//import com.harpoon.api.PlaylistItemApi;

PlaylistItemApi apiInstance = new PlaylistItemApi();
String id = "id_example"; // String | PlaylistItem id
try {
    apiInstance.playlistItemPrototypeDeletePlayerSources(id);
} catch (ApiException e) {
    System.err.println("Exception when calling PlaylistItemApi#playlistItemPrototypeDeletePlayerSources");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| PlaylistItem id |

### Return type

null (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playlistItemPrototypeDeletePlayerTracks"></a>
# **playlistItemPrototypeDeletePlayerTracks**
> playlistItemPrototypeDeletePlayerTracks(id)

Deletes all playerTracks of this model.

### Example
```java
// Import classes:
//import com.harpoon.api.PlaylistItemApi;

PlaylistItemApi apiInstance = new PlaylistItemApi();
String id = "id_example"; // String | PlaylistItem id
try {
    apiInstance.playlistItemPrototypeDeletePlayerTracks(id);
} catch (ApiException e) {
    System.err.println("Exception when calling PlaylistItemApi#playlistItemPrototypeDeletePlayerTracks");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| PlaylistItem id |

### Return type

null (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playlistItemPrototypeDeleteRadioShows"></a>
# **playlistItemPrototypeDeleteRadioShows**
> playlistItemPrototypeDeleteRadioShows(id)

Deletes all radioShows of this model.

### Example
```java
// Import classes:
//import com.harpoon.api.PlaylistItemApi;

PlaylistItemApi apiInstance = new PlaylistItemApi();
String id = "id_example"; // String | PlaylistItem id
try {
    apiInstance.playlistItemPrototypeDeleteRadioShows(id);
} catch (ApiException e) {
    System.err.println("Exception when calling PlaylistItemApi#playlistItemPrototypeDeleteRadioShows");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| PlaylistItem id |

### Return type

null (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playlistItemPrototypeDestroyByIdPlayerSources"></a>
# **playlistItemPrototypeDestroyByIdPlayerSources**
> playlistItemPrototypeDestroyByIdPlayerSources(fk, id)

Delete a related item by id for playerSources.

### Example
```java
// Import classes:
//import com.harpoon.api.PlaylistItemApi;

PlaylistItemApi apiInstance = new PlaylistItemApi();
String fk = "fk_example"; // String | Foreign key for playerSources
String id = "id_example"; // String | PlaylistItem id
try {
    apiInstance.playlistItemPrototypeDestroyByIdPlayerSources(fk, id);
} catch (ApiException e) {
    System.err.println("Exception when calling PlaylistItemApi#playlistItemPrototypeDestroyByIdPlayerSources");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for playerSources |
 **id** | **String**| PlaylistItem id |

### Return type

null (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playlistItemPrototypeDestroyByIdPlayerTracks"></a>
# **playlistItemPrototypeDestroyByIdPlayerTracks**
> playlistItemPrototypeDestroyByIdPlayerTracks(fk, id)

Delete a related item by id for playerTracks.

### Example
```java
// Import classes:
//import com.harpoon.api.PlaylistItemApi;

PlaylistItemApi apiInstance = new PlaylistItemApi();
String fk = "fk_example"; // String | Foreign key for playerTracks
String id = "id_example"; // String | PlaylistItem id
try {
    apiInstance.playlistItemPrototypeDestroyByIdPlayerTracks(fk, id);
} catch (ApiException e) {
    System.err.println("Exception when calling PlaylistItemApi#playlistItemPrototypeDestroyByIdPlayerTracks");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for playerTracks |
 **id** | **String**| PlaylistItem id |

### Return type

null (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playlistItemPrototypeDestroyByIdRadioShows"></a>
# **playlistItemPrototypeDestroyByIdRadioShows**
> playlistItemPrototypeDestroyByIdRadioShows(fk, id)

Delete a related item by id for radioShows.

### Example
```java
// Import classes:
//import com.harpoon.api.PlaylistItemApi;

PlaylistItemApi apiInstance = new PlaylistItemApi();
String fk = "fk_example"; // String | Foreign key for radioShows
String id = "id_example"; // String | PlaylistItem id
try {
    apiInstance.playlistItemPrototypeDestroyByIdRadioShows(fk, id);
} catch (ApiException e) {
    System.err.println("Exception when calling PlaylistItemApi#playlistItemPrototypeDestroyByIdRadioShows");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for radioShows |
 **id** | **String**| PlaylistItem id |

### Return type

null (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playlistItemPrototypeFindByIdPlayerSources"></a>
# **playlistItemPrototypeFindByIdPlayerSources**
> PlayerSource playlistItemPrototypeFindByIdPlayerSources(fk, id)

Find a related item by id for playerSources.

### Example
```java
// Import classes:
//import com.harpoon.api.PlaylistItemApi;

PlaylistItemApi apiInstance = new PlaylistItemApi();
String fk = "fk_example"; // String | Foreign key for playerSources
String id = "id_example"; // String | PlaylistItem id
try {
    PlayerSource result = apiInstance.playlistItemPrototypeFindByIdPlayerSources(fk, id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PlaylistItemApi#playlistItemPrototypeFindByIdPlayerSources");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for playerSources |
 **id** | **String**| PlaylistItem id |

### Return type

[**PlayerSource**](PlayerSource.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playlistItemPrototypeFindByIdPlayerTracks"></a>
# **playlistItemPrototypeFindByIdPlayerTracks**
> PlayerTrack playlistItemPrototypeFindByIdPlayerTracks(fk, id)

Find a related item by id for playerTracks.

### Example
```java
// Import classes:
//import com.harpoon.api.PlaylistItemApi;

PlaylistItemApi apiInstance = new PlaylistItemApi();
String fk = "fk_example"; // String | Foreign key for playerTracks
String id = "id_example"; // String | PlaylistItem id
try {
    PlayerTrack result = apiInstance.playlistItemPrototypeFindByIdPlayerTracks(fk, id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PlaylistItemApi#playlistItemPrototypeFindByIdPlayerTracks");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for playerTracks |
 **id** | **String**| PlaylistItem id |

### Return type

[**PlayerTrack**](PlayerTrack.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playlistItemPrototypeFindByIdRadioShows"></a>
# **playlistItemPrototypeFindByIdRadioShows**
> RadioShow playlistItemPrototypeFindByIdRadioShows(fk, id)

Find a related item by id for radioShows.

### Example
```java
// Import classes:
//import com.harpoon.api.PlaylistItemApi;

PlaylistItemApi apiInstance = new PlaylistItemApi();
String fk = "fk_example"; // String | Foreign key for radioShows
String id = "id_example"; // String | PlaylistItem id
try {
    RadioShow result = apiInstance.playlistItemPrototypeFindByIdRadioShows(fk, id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PlaylistItemApi#playlistItemPrototypeFindByIdRadioShows");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for radioShows |
 **id** | **String**| PlaylistItem id |

### Return type

[**RadioShow**](RadioShow.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playlistItemPrototypeGetPlayerSources"></a>
# **playlistItemPrototypeGetPlayerSources**
> List&lt;PlayerSource&gt; playlistItemPrototypeGetPlayerSources(id, filter)

Queries playerSources of PlaylistItem.

### Example
```java
// Import classes:
//import com.harpoon.api.PlaylistItemApi;

PlaylistItemApi apiInstance = new PlaylistItemApi();
String id = "id_example"; // String | PlaylistItem id
String filter = "filter_example"; // String | 
try {
    List<PlayerSource> result = apiInstance.playlistItemPrototypeGetPlayerSources(id, filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PlaylistItemApi#playlistItemPrototypeGetPlayerSources");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| PlaylistItem id |
 **filter** | **String**|  | [optional]

### Return type

[**List&lt;PlayerSource&gt;**](PlayerSource.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playlistItemPrototypeGetPlayerTracks"></a>
# **playlistItemPrototypeGetPlayerTracks**
> List&lt;PlayerTrack&gt; playlistItemPrototypeGetPlayerTracks(id, filter)

Queries playerTracks of PlaylistItem.

### Example
```java
// Import classes:
//import com.harpoon.api.PlaylistItemApi;

PlaylistItemApi apiInstance = new PlaylistItemApi();
String id = "id_example"; // String | PlaylistItem id
String filter = "filter_example"; // String | 
try {
    List<PlayerTrack> result = apiInstance.playlistItemPrototypeGetPlayerTracks(id, filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PlaylistItemApi#playlistItemPrototypeGetPlayerTracks");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| PlaylistItem id |
 **filter** | **String**|  | [optional]

### Return type

[**List&lt;PlayerTrack&gt;**](PlayerTrack.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playlistItemPrototypeGetPlaylist"></a>
# **playlistItemPrototypeGetPlaylist**
> Playlist playlistItemPrototypeGetPlaylist(id, refresh)

Fetches belongsTo relation playlist.

### Example
```java
// Import classes:
//import com.harpoon.api.PlaylistItemApi;

PlaylistItemApi apiInstance = new PlaylistItemApi();
String id = "id_example"; // String | PlaylistItem id
Boolean refresh = true; // Boolean | 
try {
    Playlist result = apiInstance.playlistItemPrototypeGetPlaylist(id, refresh);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PlaylistItemApi#playlistItemPrototypeGetPlaylist");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| PlaylistItem id |
 **refresh** | **Boolean**|  | [optional]

### Return type

[**Playlist**](Playlist.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playlistItemPrototypeGetRadioShows"></a>
# **playlistItemPrototypeGetRadioShows**
> List&lt;RadioShow&gt; playlistItemPrototypeGetRadioShows(id, filter)

Queries radioShows of PlaylistItem.

### Example
```java
// Import classes:
//import com.harpoon.api.PlaylistItemApi;

PlaylistItemApi apiInstance = new PlaylistItemApi();
String id = "id_example"; // String | PlaylistItem id
String filter = "filter_example"; // String | 
try {
    List<RadioShow> result = apiInstance.playlistItemPrototypeGetRadioShows(id, filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PlaylistItemApi#playlistItemPrototypeGetRadioShows");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| PlaylistItem id |
 **filter** | **String**|  | [optional]

### Return type

[**List&lt;RadioShow&gt;**](RadioShow.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playlistItemPrototypeUpdateAttributesPatchPlaylistItemsid"></a>
# **playlistItemPrototypeUpdateAttributesPatchPlaylistItemsid**
> PlaylistItem playlistItemPrototypeUpdateAttributesPatchPlaylistItemsid(id, data)

Patch attributes for a model instance and persist it into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.PlaylistItemApi;

PlaylistItemApi apiInstance = new PlaylistItemApi();
String id = "id_example"; // String | PlaylistItem id
PlaylistItem data = new PlaylistItem(); // PlaylistItem | An object of model property name/value pairs
try {
    PlaylistItem result = apiInstance.playlistItemPrototypeUpdateAttributesPatchPlaylistItemsid(id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PlaylistItemApi#playlistItemPrototypeUpdateAttributesPatchPlaylistItemsid");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| PlaylistItem id |
 **data** | [**PlaylistItem**](PlaylistItem.md)| An object of model property name/value pairs | [optional]

### Return type

[**PlaylistItem**](PlaylistItem.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playlistItemPrototypeUpdateAttributesPutPlaylistItemsid"></a>
# **playlistItemPrototypeUpdateAttributesPutPlaylistItemsid**
> PlaylistItem playlistItemPrototypeUpdateAttributesPutPlaylistItemsid(id, data)

Patch attributes for a model instance and persist it into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.PlaylistItemApi;

PlaylistItemApi apiInstance = new PlaylistItemApi();
String id = "id_example"; // String | PlaylistItem id
PlaylistItem data = new PlaylistItem(); // PlaylistItem | An object of model property name/value pairs
try {
    PlaylistItem result = apiInstance.playlistItemPrototypeUpdateAttributesPutPlaylistItemsid(id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PlaylistItemApi#playlistItemPrototypeUpdateAttributesPutPlaylistItemsid");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| PlaylistItem id |
 **data** | [**PlaylistItem**](PlaylistItem.md)| An object of model property name/value pairs | [optional]

### Return type

[**PlaylistItem**](PlaylistItem.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playlistItemPrototypeUpdateByIdPlayerSources"></a>
# **playlistItemPrototypeUpdateByIdPlayerSources**
> PlayerSource playlistItemPrototypeUpdateByIdPlayerSources(fk, id, data)

Update a related item by id for playerSources.

### Example
```java
// Import classes:
//import com.harpoon.api.PlaylistItemApi;

PlaylistItemApi apiInstance = new PlaylistItemApi();
String fk = "fk_example"; // String | Foreign key for playerSources
String id = "id_example"; // String | PlaylistItem id
PlayerSource data = new PlayerSource(); // PlayerSource | 
try {
    PlayerSource result = apiInstance.playlistItemPrototypeUpdateByIdPlayerSources(fk, id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PlaylistItemApi#playlistItemPrototypeUpdateByIdPlayerSources");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for playerSources |
 **id** | **String**| PlaylistItem id |
 **data** | [**PlayerSource**](PlayerSource.md)|  | [optional]

### Return type

[**PlayerSource**](PlayerSource.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playlistItemPrototypeUpdateByIdPlayerTracks"></a>
# **playlistItemPrototypeUpdateByIdPlayerTracks**
> PlayerTrack playlistItemPrototypeUpdateByIdPlayerTracks(fk, id, data)

Update a related item by id for playerTracks.

### Example
```java
// Import classes:
//import com.harpoon.api.PlaylistItemApi;

PlaylistItemApi apiInstance = new PlaylistItemApi();
String fk = "fk_example"; // String | Foreign key for playerTracks
String id = "id_example"; // String | PlaylistItem id
PlayerTrack data = new PlayerTrack(); // PlayerTrack | 
try {
    PlayerTrack result = apiInstance.playlistItemPrototypeUpdateByIdPlayerTracks(fk, id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PlaylistItemApi#playlistItemPrototypeUpdateByIdPlayerTracks");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for playerTracks |
 **id** | **String**| PlaylistItem id |
 **data** | [**PlayerTrack**](PlayerTrack.md)|  | [optional]

### Return type

[**PlayerTrack**](PlayerTrack.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playlistItemPrototypeUpdateByIdRadioShows"></a>
# **playlistItemPrototypeUpdateByIdRadioShows**
> RadioShow playlistItemPrototypeUpdateByIdRadioShows(fk, id, data)

Update a related item by id for radioShows.

### Example
```java
// Import classes:
//import com.harpoon.api.PlaylistItemApi;

PlaylistItemApi apiInstance = new PlaylistItemApi();
String fk = "fk_example"; // String | Foreign key for radioShows
String id = "id_example"; // String | PlaylistItem id
RadioShow data = new RadioShow(); // RadioShow | 
try {
    RadioShow result = apiInstance.playlistItemPrototypeUpdateByIdRadioShows(fk, id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PlaylistItemApi#playlistItemPrototypeUpdateByIdRadioShows");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for radioShows |
 **id** | **String**| PlaylistItem id |
 **data** | [**RadioShow**](RadioShow.md)|  | [optional]

### Return type

[**RadioShow**](RadioShow.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playlistItemReplaceById"></a>
# **playlistItemReplaceById**
> PlaylistItem playlistItemReplaceById(id, data)

Replace attributes for a model instance and persist it into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.PlaylistItemApi;

PlaylistItemApi apiInstance = new PlaylistItemApi();
String id = "id_example"; // String | Model id
PlaylistItem data = new PlaylistItem(); // PlaylistItem | Model instance data
try {
    PlaylistItem result = apiInstance.playlistItemReplaceById(id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PlaylistItemApi#playlistItemReplaceById");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |
 **data** | [**PlaylistItem**](PlaylistItem.md)| Model instance data | [optional]

### Return type

[**PlaylistItem**](PlaylistItem.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playlistItemReplaceOrCreate"></a>
# **playlistItemReplaceOrCreate**
> PlaylistItem playlistItemReplaceOrCreate(data)

Replace an existing model instance or insert a new one into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.PlaylistItemApi;

PlaylistItemApi apiInstance = new PlaylistItemApi();
PlaylistItem data = new PlaylistItem(); // PlaylistItem | Model instance data
try {
    PlaylistItem result = apiInstance.playlistItemReplaceOrCreate(data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PlaylistItemApi#playlistItemReplaceOrCreate");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**PlaylistItem**](PlaylistItem.md)| Model instance data | [optional]

### Return type

[**PlaylistItem**](PlaylistItem.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playlistItemUpdateAll"></a>
# **playlistItemUpdateAll**
> InlineResponse2002 playlistItemUpdateAll(where, data)

Update instances of the model matched by {{where}} from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.PlaylistItemApi;

PlaylistItemApi apiInstance = new PlaylistItemApi();
String where = "where_example"; // String | Criteria to match model instances
PlaylistItem data = new PlaylistItem(); // PlaylistItem | An object of model property name/value pairs
try {
    InlineResponse2002 result = apiInstance.playlistItemUpdateAll(where, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PlaylistItemApi#playlistItemUpdateAll");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional]
 **data** | [**PlaylistItem**](PlaylistItem.md)| An object of model property name/value pairs | [optional]

### Return type

[**InlineResponse2002**](InlineResponse2002.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playlistItemUpsertPatchPlaylistItems"></a>
# **playlistItemUpsertPatchPlaylistItems**
> PlaylistItem playlistItemUpsertPatchPlaylistItems(data)

Patch an existing model instance or insert a new one into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.PlaylistItemApi;

PlaylistItemApi apiInstance = new PlaylistItemApi();
PlaylistItem data = new PlaylistItem(); // PlaylistItem | Model instance data
try {
    PlaylistItem result = apiInstance.playlistItemUpsertPatchPlaylistItems(data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PlaylistItemApi#playlistItemUpsertPatchPlaylistItems");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**PlaylistItem**](PlaylistItem.md)| Model instance data | [optional]

### Return type

[**PlaylistItem**](PlaylistItem.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playlistItemUpsertPutPlaylistItems"></a>
# **playlistItemUpsertPutPlaylistItems**
> PlaylistItem playlistItemUpsertPutPlaylistItems(data)

Patch an existing model instance or insert a new one into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.PlaylistItemApi;

PlaylistItemApi apiInstance = new PlaylistItemApi();
PlaylistItem data = new PlaylistItem(); // PlaylistItem | Model instance data
try {
    PlaylistItem result = apiInstance.playlistItemUpsertPutPlaylistItems(data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PlaylistItemApi#playlistItemUpsertPutPlaylistItems");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**PlaylistItem**](PlaylistItem.md)| Model instance data | [optional]

### Return type

[**PlaylistItem**](PlaylistItem.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playlistItemUpsertWithWhere"></a>
# **playlistItemUpsertWithWhere**
> PlaylistItem playlistItemUpsertWithWhere(where, data)

Update an existing model instance or insert a new one into the data source based on the where criteria.

### Example
```java
// Import classes:
//import com.harpoon.api.PlaylistItemApi;

PlaylistItemApi apiInstance = new PlaylistItemApi();
String where = "where_example"; // String | Criteria to match model instances
PlaylistItem data = new PlaylistItem(); // PlaylistItem | An object of model property name/value pairs
try {
    PlaylistItem result = apiInstance.playlistItemUpsertWithWhere(where, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PlaylistItemApi#playlistItemUpsertWithWhere");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional]
 **data** | [**PlaylistItem**](PlaylistItem.md)| An object of model property name/value pairs | [optional]

### Return type

[**PlaylistItem**](PlaylistItem.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

