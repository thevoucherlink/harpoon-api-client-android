# OfferApi

All URIs are relative to *https://apicdn.harpoonconnect.com/v1.1.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**offerFind**](OfferApi.md#offerFind) | **GET** /Offers | Find all instances of the model matched by filter from the data source.
[**offerFindById**](OfferApi.md#offerFindById) | **GET** /Offers/{id} | Find a model instance by {{id}} from the data source.
[**offerReplaceById**](OfferApi.md#offerReplaceById) | **POST** /Offers/{id}/replace | Replace attributes for a model instance and persist it into the data source.
[**offerReplaceOrCreate**](OfferApi.md#offerReplaceOrCreate) | **POST** /Offers/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
[**offerUpsertWithWhere**](OfferApi.md#offerUpsertWithWhere) | **POST** /Offers/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.


<a name="offerFind"></a>
# **offerFind**
> List&lt;Offer&gt; offerFind(filter)

Find all instances of the model matched by filter from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.OfferApi;

OfferApi apiInstance = new OfferApi();
String filter = "filter_example"; // String | Filter defining fields, where, include, order, offset, and limit
try {
    List<Offer> result = apiInstance.offerFind(filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling OfferApi#offerFind");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **String**| Filter defining fields, where, include, order, offset, and limit | [optional]

### Return type

[**List&lt;Offer&gt;**](Offer.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="offerFindById"></a>
# **offerFindById**
> Offer offerFindById(id, filter)

Find a model instance by {{id}} from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.OfferApi;

OfferApi apiInstance = new OfferApi();
String id = "id_example"; // String | Model id
String filter = "filter_example"; // String | Filter defining fields and include
try {
    Offer result = apiInstance.offerFindById(id, filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling OfferApi#offerFindById");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |
 **filter** | **String**| Filter defining fields and include | [optional]

### Return type

[**Offer**](Offer.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="offerReplaceById"></a>
# **offerReplaceById**
> Offer offerReplaceById(id, data)

Replace attributes for a model instance and persist it into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.OfferApi;

OfferApi apiInstance = new OfferApi();
String id = "id_example"; // String | Model id
Offer data = new Offer(); // Offer | Model instance data
try {
    Offer result = apiInstance.offerReplaceById(id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling OfferApi#offerReplaceById");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |
 **data** | [**Offer**](Offer.md)| Model instance data | [optional]

### Return type

[**Offer**](Offer.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="offerReplaceOrCreate"></a>
# **offerReplaceOrCreate**
> Offer offerReplaceOrCreate(data)

Replace an existing model instance or insert a new one into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.OfferApi;

OfferApi apiInstance = new OfferApi();
Offer data = new Offer(); // Offer | Model instance data
try {
    Offer result = apiInstance.offerReplaceOrCreate(data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling OfferApi#offerReplaceOrCreate");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**Offer**](Offer.md)| Model instance data | [optional]

### Return type

[**Offer**](Offer.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="offerUpsertWithWhere"></a>
# **offerUpsertWithWhere**
> Offer offerUpsertWithWhere(where, data)

Update an existing model instance or insert a new one into the data source based on the where criteria.

### Example
```java
// Import classes:
//import com.harpoon.api.OfferApi;

OfferApi apiInstance = new OfferApi();
String where = "where_example"; // String | Criteria to match model instances
Offer data = new Offer(); // Offer | An object of model property name/value pairs
try {
    Offer result = apiInstance.offerUpsertWithWhere(where, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling OfferApi#offerUpsertWithWhere");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional]
 **data** | [**Offer**](Offer.md)| An object of model property name/value pairs | [optional]

### Return type

[**Offer**](Offer.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

