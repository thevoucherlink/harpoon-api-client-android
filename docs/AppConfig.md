
# AppConfig

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**imgAppLogo** | **String** | App logo |  [optional]
**imgSplashScreen** | **String** | App splash-screen |  [optional]
**imgBrandLogo** | **String** |  |  [optional]
**imgNavBarLogo** | **String** |  |  [optional]
**imgBrandPlaceholder** | **String** |  |  [optional]
**imgMenuBackground** | **String** |  |  [optional]
**featureRadio** | **Boolean** | Enable or disable the Radio feature within the app |  [optional]
**featureRadioAutoPlay** | **Boolean** | Enable or disable the Radio autoplay feature within the app |  [optional]
**featureNews** | **Boolean** |  |  [optional]
**featureEvents** | **Boolean** |  |  [optional]
**featureEventsCategorized** | **Boolean** | If true the Events list is categorised |  [optional]
**featureEventsAttendeeList** | **Boolean** | If true the user will have the possibility to see Events&#39; attendees |  [optional]
**featureCommunity** | **Boolean** |  |  [optional]
**featureCommunityCategorized** | **Boolean** | If true the Brand list is categories |  [optional]
**featureCompetitions** | **Boolean** |  |  [optional]
**featureCompetitionsAttendeeList** | **Boolean** | If true the user will have the possibility to see Competitions&#39; attendees |  [optional]
**featureOffers** | **Boolean** |  |  [optional]
**featureOffersCategorized** | **Boolean** | If true the Offer list is categories |  [optional]
**featureBrandFollow** | **Boolean** | If true the user will have the possibility to follow or unfollow Brands |  [optional]
**featureBrandFollowerList** | **Boolean** | If true the user will have the possibility to see Brands&#39; followers |  [optional]
**featureWordpressConnect** | **Boolean** |  |  [optional]
**featureLogin** | **Boolean** | If true the user will be asked to login before getting access to the app |  [optional]
**featureHarpoonLogin** | **Boolean** |  |  [optional]
**featureFacebookLogin** | **Boolean** | If true the user will have the possibility to login with Facebook |  [optional]
**featurePhoneVerification** | **Boolean** | If true the user will need to verify its account using a phone number |  [optional]
**featurePlayer** | **Boolean** | Show / Hide media player |  [optional]
**featurePlayerMainButton** | **Boolean** | Show / Hide main button |  [optional]
**featureGoogleAnalytics** | **Boolean** | If true the user actions will be tracked while using the app |  [optional]
**playerMainButtonTitle** | **String** | Media Player Main Button title |  [optional]
**playerConfig** | [**PlayerConfig**](PlayerConfig.md) | Media Player config |  [optional]
**menu** | [**List&lt;MenuItem&gt;**](MenuItem.md) |  |  [optional]
**color** | **String** |  |  [optional]
**colors** | [**AppColor**](AppColor.md) |  |  [optional]
**googleAppId** | **String** |  |  [optional]
**iOSAppId** | **String** |  |  [optional]
**googleAnalyticsTracking** | [**GoogleAnalyticsTracking**](GoogleAnalyticsTracking.md) |  |  [optional]
**loginExternal** | [**LoginExternal**](LoginExternal.md) |  |  [optional]
**rssWebViewCss** | **String** |  |  [optional]
**ads** | [**List&lt;AppAd&gt;**](AppAd.md) |  |  [optional]
**iOSAds** | [**List&lt;AppAd&gt;**](AppAd.md) |  |  [optional]
**androidAds** | [**List&lt;AppAd&gt;**](AppAd.md) |  |  [optional]
**pushNotificationProvider** | [**PushNotificationProviderEnum**](#PushNotificationProviderEnum) |  |  [optional]
**batchConfig** | [**BatchConfig**](BatchConfig.md) |  |  [optional]
**urbanAirshipConfig** | [**UrbanAirshipConfig**](UrbanAirshipConfig.md) |  |  [optional]
**pushWooshConfig** | [**PushWooshConfig**](PushWooshConfig.md) |  |  [optional]
**facebookConfig** | [**FacebookConfig**](FacebookConfig.md) |  |  [optional]
**twitterConfig** | [**TwitterConfig**](TwitterConfig.md) |  |  [optional]
**rssTags** | [**RssTags**](RssTags.md) |  |  [optional]
**connectTo** | [**AppConnectTo**](AppConnectTo.md) |  |  [optional]
**iOSVersion** | **String** |  |  [optional]
**iOSBuild** | **String** |  |  [optional]
**iOSGoogleServices** | **String** |  |  [optional]
**iOSTeamId** | **String** |  |  [optional]
**iOSAppleId** | **String** |  |  [optional]
**iOSPushNotificationPrivateKey** | **String** |  |  [optional]
**iOSPushNotificationCertificate** | **String** |  |  [optional]
**iOSPushNotificationPem** | **String** |  |  [optional]
**iOSPushNotificationPassword** | **String** |  |  [optional]
**iOSReleaseNotes** | **String** |  |  [optional]
**androidBuild** | **String** |  |  [optional]
**androidVersion** | **String** |  |  [optional]
**androidGoogleServices** | **String** |  |  [optional]
**androidGoogleApiKey** | **String** |  |  [optional]
**androidGoogleCloudMessagingSenderId** | **String** |  |  [optional]
**androidKey** | **String** |  |  [optional]
**androidKeystore** | **String** |  |  [optional]
**androidKeystoreConfig** | [**AndroidKeystoreConfig**](AndroidKeystoreConfig.md) |  |  [optional]
**androidApk** | **String** |  |  [optional]
**customerDetailsForm** | [**List&lt;FormSection&gt;**](FormSection.md) |  |  [optional]
**contact** | [**Contact**](Contact.md) |  |  [optional]
**imgSponsorMenu** | **String** |  |  [optional]
**imgSponsorSplash** | **String** |  |  [optional]
**audioSponsorPreRollAd** | **String** |  |  [optional]
**name** | **String** |  |  [optional]
**isChildConfig** | **Boolean** |  |  [optional]
**typeNewsFocusSource** | [**TypeNewsFocusSourceEnum**](#TypeNewsFocusSourceEnum) |  |  [optional]
**sponsorSplashImgDisplayTime** | **Double** |  |  [optional]
**radioSessionInterval** | **Double** |  |  [optional]
**featureRadioSession** | **Boolean** |  |  [optional]
**wordpressShareUrlStub** | **String** |  |  [optional]
**iosUriScheme** | **String** |  |  [optional]
**androidUriScheme** | **String** |  |  [optional]
**branchConfig** | [**BranchConfig**](BranchConfig.md) |  |  [optional]
**defaultRadioStreamId** | **Double** |  |  [optional]
**featureTritonPlayer** | **Boolean** | Enable or disable the Triton Player in App |  [optional]
**id** | **Double** |  |  [optional]
**appId** | **String** |  |  [optional]
**app** | **Object** |  |  [optional]


<a name="PushNotificationProviderEnum"></a>
## Enum: PushNotificationProviderEnum
Name | Value
---- | -----


<a name="TypeNewsFocusSourceEnum"></a>
## Enum: TypeNewsFocusSourceEnum
Name | Value
---- | -----



