# EventApi

All URIs are relative to *https://apicdn.harpoonconnect.com/v1.1.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**eventAttendees**](EventApi.md#eventAttendees) | **GET** /Events/{id}/attendees | 
[**eventCheckout**](EventApi.md#eventCheckout) | **POST** /Events/{id}/checkout | 
[**eventFind**](EventApi.md#eventFind) | **GET** /Events | Find all instances of the model matched by filter from the data source.
[**eventFindById**](EventApi.md#eventFindById) | **GET** /Events/{id} | Find a model instance by {{id}} from the data source.
[**eventFindInLater**](EventApi.md#eventFindInLater) | **GET** /Events/later | 
[**eventFindInThisMonth**](EventApi.md#eventFindInThisMonth) | **GET** /Events/thisMonth | 
[**eventFindOne**](EventApi.md#eventFindOne) | **GET** /Events/findOne | Find first instance of the model matched by filter from the data source.
[**eventRedeem**](EventApi.md#eventRedeem) | **POST** /Events/{id}/redeem | 
[**eventReplaceById**](EventApi.md#eventReplaceById) | **POST** /Events/{id}/replace | Replace attributes for a model instance and persist it into the data source.
[**eventReplaceOrCreate**](EventApi.md#eventReplaceOrCreate) | **POST** /Events/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
[**eventUpsertWithWhere**](EventApi.md#eventUpsertWithWhere) | **POST** /Events/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.
[**eventVenues**](EventApi.md#eventVenues) | **GET** /Events/{id}/venues | 


<a name="eventAttendees"></a>
# **eventAttendees**
> List&lt;EventAttendee&gt; eventAttendees(id, filter)



### Example
```java
// Import classes:
//import com.harpoon.api.EventApi;

EventApi apiInstance = new EventApi();
String id = "id_example"; // String | Model id
String filter = "filter_example"; // String | Filter defining fields and include
try {
    List<EventAttendee> result = apiInstance.eventAttendees(id, filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling EventApi#eventAttendees");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |
 **filter** | **String**| Filter defining fields and include | [optional]

### Return type

[**List&lt;EventAttendee&gt;**](EventAttendee.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="eventCheckout"></a>
# **eventCheckout**
> EventCheckout eventCheckout(id, data)



### Example
```java
// Import classes:
//import com.harpoon.api.EventApi;

EventApi apiInstance = new EventApi();
String id = "id_example"; // String | Model id
EventCheckoutData data = new EventCheckoutData(); // EventCheckoutData | 
try {
    EventCheckout result = apiInstance.eventCheckout(id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling EventApi#eventCheckout");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |
 **data** | [**EventCheckoutData**](EventCheckoutData.md)|  | [optional]

### Return type

[**EventCheckout**](EventCheckout.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="eventFind"></a>
# **eventFind**
> List&lt;Event&gt; eventFind(filter)

Find all instances of the model matched by filter from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.EventApi;

EventApi apiInstance = new EventApi();
String filter = "filter_example"; // String | Filter defining fields, where, include, order, offset, and limit
try {
    List<Event> result = apiInstance.eventFind(filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling EventApi#eventFind");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **String**| Filter defining fields, where, include, order, offset, and limit | [optional]

### Return type

[**List&lt;Event&gt;**](Event.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="eventFindById"></a>
# **eventFindById**
> Event eventFindById(id, filter)

Find a model instance by {{id}} from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.EventApi;

EventApi apiInstance = new EventApi();
String id = "id_example"; // String | Model id
String filter = "filter_example"; // String | Filter defining fields and include
try {
    Event result = apiInstance.eventFindById(id, filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling EventApi#eventFindById");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |
 **filter** | **String**| Filter defining fields and include | [optional]

### Return type

[**Event**](Event.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="eventFindInLater"></a>
# **eventFindInLater**
> List&lt;Event&gt; eventFindInLater(filter)



### Example
```java
// Import classes:
//import com.harpoon.api.EventApi;

EventApi apiInstance = new EventApi();
String filter = "filter_example"; // String | Filter defining fields and include
try {
    List<Event> result = apiInstance.eventFindInLater(filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling EventApi#eventFindInLater");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **String**| Filter defining fields and include | [optional]

### Return type

[**List&lt;Event&gt;**](Event.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="eventFindInThisMonth"></a>
# **eventFindInThisMonth**
> List&lt;Event&gt; eventFindInThisMonth(filter)



### Example
```java
// Import classes:
//import com.harpoon.api.EventApi;

EventApi apiInstance = new EventApi();
String filter = "filter_example"; // String | Filter defining fields and include
try {
    List<Event> result = apiInstance.eventFindInThisMonth(filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling EventApi#eventFindInThisMonth");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **String**| Filter defining fields and include | [optional]

### Return type

[**List&lt;Event&gt;**](Event.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="eventFindOne"></a>
# **eventFindOne**
> Event eventFindOne(filter)

Find first instance of the model matched by filter from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.EventApi;

EventApi apiInstance = new EventApi();
String filter = "filter_example"; // String | Filter defining fields, where, include, order, offset, and limit
try {
    Event result = apiInstance.eventFindOne(filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling EventApi#eventFindOne");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **String**| Filter defining fields, where, include, order, offset, and limit | [optional]

### Return type

[**Event**](Event.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="eventRedeem"></a>
# **eventRedeem**
> EventPurchase eventRedeem(id, data)



### Example
```java
// Import classes:
//import com.harpoon.api.EventApi;

EventApi apiInstance = new EventApi();
String id = "id_example"; // String | Model id
EventRedeemData data = new EventRedeemData(); // EventRedeemData | 
try {
    EventPurchase result = apiInstance.eventRedeem(id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling EventApi#eventRedeem");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |
 **data** | [**EventRedeemData**](EventRedeemData.md)|  | [optional]

### Return type

[**EventPurchase**](EventPurchase.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="eventReplaceById"></a>
# **eventReplaceById**
> Event eventReplaceById(id, data)

Replace attributes for a model instance and persist it into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.EventApi;

EventApi apiInstance = new EventApi();
String id = "id_example"; // String | Model id
Event data = new Event(); // Event | Model instance data
try {
    Event result = apiInstance.eventReplaceById(id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling EventApi#eventReplaceById");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |
 **data** | [**Event**](Event.md)| Model instance data | [optional]

### Return type

[**Event**](Event.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="eventReplaceOrCreate"></a>
# **eventReplaceOrCreate**
> Event eventReplaceOrCreate(data)

Replace an existing model instance or insert a new one into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.EventApi;

EventApi apiInstance = new EventApi();
Event data = new Event(); // Event | Model instance data
try {
    Event result = apiInstance.eventReplaceOrCreate(data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling EventApi#eventReplaceOrCreate");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**Event**](Event.md)| Model instance data | [optional]

### Return type

[**Event**](Event.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="eventUpsertWithWhere"></a>
# **eventUpsertWithWhere**
> Event eventUpsertWithWhere(where, data)

Update an existing model instance or insert a new one into the data source based on the where criteria.

### Example
```java
// Import classes:
//import com.harpoon.api.EventApi;

EventApi apiInstance = new EventApi();
String where = "where_example"; // String | Criteria to match model instances
Event data = new Event(); // Event | An object of model property name/value pairs
try {
    Event result = apiInstance.eventUpsertWithWhere(where, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling EventApi#eventUpsertWithWhere");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional]
 **data** | [**Event**](Event.md)| An object of model property name/value pairs | [optional]

### Return type

[**Event**](Event.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="eventVenues"></a>
# **eventVenues**
> List&lt;Venue&gt; eventVenues(id, filter)



### Example
```java
// Import classes:
//import com.harpoon.api.EventApi;

EventApi apiInstance = new EventApi();
String id = "id_example"; // String | Model id
String filter = "filter_example"; // String | Filter defining fields and include
try {
    List<Venue> result = apiInstance.eventVenues(id, filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling EventApi#eventVenues");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |
 **filter** | **String**| Filter defining fields and include | [optional]

### Return type

[**List&lt;Venue&gt;**](Venue.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

