# RadioStreamApi

All URIs are relative to *https://apicdn.harpoonconnect.com/v1.1.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**radioStreamCount**](RadioStreamApi.md#radioStreamCount) | **GET** /RadioStreams/count | Count instances of the model matched by where from the data source.
[**radioStreamCreate**](RadioStreamApi.md#radioStreamCreate) | **POST** /RadioStreams | Create a new instance of the model and persist it into the data source.
[**radioStreamCreateChangeStreamGetRadioStreamsChangeStream**](RadioStreamApi.md#radioStreamCreateChangeStreamGetRadioStreamsChangeStream) | **GET** /RadioStreams/change-stream | Create a change stream.
[**radioStreamCreateChangeStreamPostRadioStreamsChangeStream**](RadioStreamApi.md#radioStreamCreateChangeStreamPostRadioStreamsChangeStream) | **POST** /RadioStreams/change-stream | Create a change stream.
[**radioStreamDeleteById**](RadioStreamApi.md#radioStreamDeleteById) | **DELETE** /RadioStreams/{id} | Delete a model instance by {{id}} from the data source.
[**radioStreamExistsGetRadioStreamsidExists**](RadioStreamApi.md#radioStreamExistsGetRadioStreamsidExists) | **GET** /RadioStreams/{id}/exists | Check whether a model instance exists in the data source.
[**radioStreamExistsHeadRadioStreamsid**](RadioStreamApi.md#radioStreamExistsHeadRadioStreamsid) | **HEAD** /RadioStreams/{id} | Check whether a model instance exists in the data source.
[**radioStreamFind**](RadioStreamApi.md#radioStreamFind) | **GET** /RadioStreams | Find all instances of the model matched by filter from the data source.
[**radioStreamFindById**](RadioStreamApi.md#radioStreamFindById) | **GET** /RadioStreams/{id} | Find a model instance by {{id}} from the data source.
[**radioStreamFindOne**](RadioStreamApi.md#radioStreamFindOne) | **GET** /RadioStreams/findOne | Find first instance of the model matched by filter from the data source.
[**radioStreamPrototypeCountRadioShows**](RadioStreamApi.md#radioStreamPrototypeCountRadioShows) | **GET** /RadioStreams/{id}/radioShows/count | Counts radioShows of RadioStream.
[**radioStreamPrototypeCreateRadioShows**](RadioStreamApi.md#radioStreamPrototypeCreateRadioShows) | **POST** /RadioStreams/{id}/radioShows | Creates a new instance in radioShows of this model.
[**radioStreamPrototypeDeleteRadioShows**](RadioStreamApi.md#radioStreamPrototypeDeleteRadioShows) | **DELETE** /RadioStreams/{id}/radioShows | Deletes all radioShows of this model.
[**radioStreamPrototypeDestroyByIdRadioShows**](RadioStreamApi.md#radioStreamPrototypeDestroyByIdRadioShows) | **DELETE** /RadioStreams/{id}/radioShows/{fk} | Delete a related item by id for radioShows.
[**radioStreamPrototypeFindByIdRadioShows**](RadioStreamApi.md#radioStreamPrototypeFindByIdRadioShows) | **GET** /RadioStreams/{id}/radioShows/{fk} | Find a related item by id for radioShows.
[**radioStreamPrototypeGetRadioShows**](RadioStreamApi.md#radioStreamPrototypeGetRadioShows) | **GET** /RadioStreams/{id}/radioShows | Queries radioShows of RadioStream.
[**radioStreamPrototypeUpdateAttributesPatchRadioStreamsid**](RadioStreamApi.md#radioStreamPrototypeUpdateAttributesPatchRadioStreamsid) | **PATCH** /RadioStreams/{id} | Patch attributes for a model instance and persist it into the data source.
[**radioStreamPrototypeUpdateAttributesPutRadioStreamsid**](RadioStreamApi.md#radioStreamPrototypeUpdateAttributesPutRadioStreamsid) | **PUT** /RadioStreams/{id} | Patch attributes for a model instance and persist it into the data source.
[**radioStreamPrototypeUpdateByIdRadioShows**](RadioStreamApi.md#radioStreamPrototypeUpdateByIdRadioShows) | **PUT** /RadioStreams/{id}/radioShows/{fk} | Update a related item by id for radioShows.
[**radioStreamRadioShowCurrent**](RadioStreamApi.md#radioStreamRadioShowCurrent) | **GET** /RadioStreams/{id}/radioShows/current | 
[**radioStreamRadioShowSchedule**](RadioStreamApi.md#radioStreamRadioShowSchedule) | **GET** /RadioStreams/{id}/radioShows/schedule | 
[**radioStreamReplaceById**](RadioStreamApi.md#radioStreamReplaceById) | **POST** /RadioStreams/{id}/replace | Replace attributes for a model instance and persist it into the data source.
[**radioStreamReplaceOrCreate**](RadioStreamApi.md#radioStreamReplaceOrCreate) | **POST** /RadioStreams/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
[**radioStreamUpdateAll**](RadioStreamApi.md#radioStreamUpdateAll) | **POST** /RadioStreams/update | Update instances of the model matched by {{where}} from the data source.
[**radioStreamUpsertPatchRadioStreams**](RadioStreamApi.md#radioStreamUpsertPatchRadioStreams) | **PATCH** /RadioStreams | Patch an existing model instance or insert a new one into the data source.
[**radioStreamUpsertPutRadioStreams**](RadioStreamApi.md#radioStreamUpsertPutRadioStreams) | **PUT** /RadioStreams | Patch an existing model instance or insert a new one into the data source.
[**radioStreamUpsertWithWhere**](RadioStreamApi.md#radioStreamUpsertWithWhere) | **POST** /RadioStreams/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.


<a name="radioStreamCount"></a>
# **radioStreamCount**
> InlineResponse2001 radioStreamCount(where)

Count instances of the model matched by where from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.RadioStreamApi;

RadioStreamApi apiInstance = new RadioStreamApi();
String where = "where_example"; // String | Criteria to match model instances
try {
    InlineResponse2001 result = apiInstance.radioStreamCount(where);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RadioStreamApi#radioStreamCount");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional]

### Return type

[**InlineResponse2001**](InlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioStreamCreate"></a>
# **radioStreamCreate**
> RadioStream radioStreamCreate(data)

Create a new instance of the model and persist it into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.RadioStreamApi;

RadioStreamApi apiInstance = new RadioStreamApi();
RadioStream data = new RadioStream(); // RadioStream | Model instance data
try {
    RadioStream result = apiInstance.radioStreamCreate(data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RadioStreamApi#radioStreamCreate");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**RadioStream**](RadioStream.md)| Model instance data | [optional]

### Return type

[**RadioStream**](RadioStream.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioStreamCreateChangeStreamGetRadioStreamsChangeStream"></a>
# **radioStreamCreateChangeStreamGetRadioStreamsChangeStream**
> File radioStreamCreateChangeStreamGetRadioStreamsChangeStream(options)

Create a change stream.

### Example
```java
// Import classes:
//import com.harpoon.api.RadioStreamApi;

RadioStreamApi apiInstance = new RadioStreamApi();
String options = "options_example"; // String | 
try {
    File result = apiInstance.radioStreamCreateChangeStreamGetRadioStreamsChangeStream(options);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RadioStreamApi#radioStreamCreateChangeStreamGetRadioStreamsChangeStream");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **String**|  | [optional]

### Return type

[**File**](File.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioStreamCreateChangeStreamPostRadioStreamsChangeStream"></a>
# **radioStreamCreateChangeStreamPostRadioStreamsChangeStream**
> File radioStreamCreateChangeStreamPostRadioStreamsChangeStream(options)

Create a change stream.

### Example
```java
// Import classes:
//import com.harpoon.api.RadioStreamApi;

RadioStreamApi apiInstance = new RadioStreamApi();
String options = "options_example"; // String | 
try {
    File result = apiInstance.radioStreamCreateChangeStreamPostRadioStreamsChangeStream(options);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RadioStreamApi#radioStreamCreateChangeStreamPostRadioStreamsChangeStream");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **String**|  | [optional]

### Return type

[**File**](File.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioStreamDeleteById"></a>
# **radioStreamDeleteById**
> Object radioStreamDeleteById(id)

Delete a model instance by {{id}} from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.RadioStreamApi;

RadioStreamApi apiInstance = new RadioStreamApi();
String id = "id_example"; // String | Model id
try {
    Object result = apiInstance.radioStreamDeleteById(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RadioStreamApi#radioStreamDeleteById");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |

### Return type

**Object**

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioStreamExistsGetRadioStreamsidExists"></a>
# **radioStreamExistsGetRadioStreamsidExists**
> InlineResponse2003 radioStreamExistsGetRadioStreamsidExists(id)

Check whether a model instance exists in the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.RadioStreamApi;

RadioStreamApi apiInstance = new RadioStreamApi();
String id = "id_example"; // String | Model id
try {
    InlineResponse2003 result = apiInstance.radioStreamExistsGetRadioStreamsidExists(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RadioStreamApi#radioStreamExistsGetRadioStreamsidExists");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |

### Return type

[**InlineResponse2003**](InlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioStreamExistsHeadRadioStreamsid"></a>
# **radioStreamExistsHeadRadioStreamsid**
> InlineResponse2003 radioStreamExistsHeadRadioStreamsid(id)

Check whether a model instance exists in the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.RadioStreamApi;

RadioStreamApi apiInstance = new RadioStreamApi();
String id = "id_example"; // String | Model id
try {
    InlineResponse2003 result = apiInstance.radioStreamExistsHeadRadioStreamsid(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RadioStreamApi#radioStreamExistsHeadRadioStreamsid");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |

### Return type

[**InlineResponse2003**](InlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioStreamFind"></a>
# **radioStreamFind**
> List&lt;RadioStream&gt; radioStreamFind(filter)

Find all instances of the model matched by filter from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.RadioStreamApi;

RadioStreamApi apiInstance = new RadioStreamApi();
String filter = "filter_example"; // String | Filter defining fields, where, include, order, offset, and limit
try {
    List<RadioStream> result = apiInstance.radioStreamFind(filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RadioStreamApi#radioStreamFind");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **String**| Filter defining fields, where, include, order, offset, and limit | [optional]

### Return type

[**List&lt;RadioStream&gt;**](RadioStream.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioStreamFindById"></a>
# **radioStreamFindById**
> RadioStream radioStreamFindById(id, filter)

Find a model instance by {{id}} from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.RadioStreamApi;

RadioStreamApi apiInstance = new RadioStreamApi();
String id = "id_example"; // String | Model id
String filter = "filter_example"; // String | Filter defining fields and include
try {
    RadioStream result = apiInstance.radioStreamFindById(id, filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RadioStreamApi#radioStreamFindById");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |
 **filter** | **String**| Filter defining fields and include | [optional]

### Return type

[**RadioStream**](RadioStream.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioStreamFindOne"></a>
# **radioStreamFindOne**
> RadioStream radioStreamFindOne(filter)

Find first instance of the model matched by filter from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.RadioStreamApi;

RadioStreamApi apiInstance = new RadioStreamApi();
String filter = "filter_example"; // String | Filter defining fields, where, include, order, offset, and limit
try {
    RadioStream result = apiInstance.radioStreamFindOne(filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RadioStreamApi#radioStreamFindOne");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **String**| Filter defining fields, where, include, order, offset, and limit | [optional]

### Return type

[**RadioStream**](RadioStream.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioStreamPrototypeCountRadioShows"></a>
# **radioStreamPrototypeCountRadioShows**
> InlineResponse2001 radioStreamPrototypeCountRadioShows(id, where)

Counts radioShows of RadioStream.

### Example
```java
// Import classes:
//import com.harpoon.api.RadioStreamApi;

RadioStreamApi apiInstance = new RadioStreamApi();
String id = "id_example"; // String | RadioStream id
String where = "where_example"; // String | Criteria to match model instances
try {
    InlineResponse2001 result = apiInstance.radioStreamPrototypeCountRadioShows(id, where);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RadioStreamApi#radioStreamPrototypeCountRadioShows");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| RadioStream id |
 **where** | **String**| Criteria to match model instances | [optional]

### Return type

[**InlineResponse2001**](InlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioStreamPrototypeCreateRadioShows"></a>
# **radioStreamPrototypeCreateRadioShows**
> RadioShow radioStreamPrototypeCreateRadioShows(id, data)

Creates a new instance in radioShows of this model.

### Example
```java
// Import classes:
//import com.harpoon.api.RadioStreamApi;

RadioStreamApi apiInstance = new RadioStreamApi();
String id = "id_example"; // String | RadioStream id
RadioShow data = new RadioShow(); // RadioShow | 
try {
    RadioShow result = apiInstance.radioStreamPrototypeCreateRadioShows(id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RadioStreamApi#radioStreamPrototypeCreateRadioShows");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| RadioStream id |
 **data** | [**RadioShow**](RadioShow.md)|  | [optional]

### Return type

[**RadioShow**](RadioShow.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioStreamPrototypeDeleteRadioShows"></a>
# **radioStreamPrototypeDeleteRadioShows**
> radioStreamPrototypeDeleteRadioShows(id)

Deletes all radioShows of this model.

### Example
```java
// Import classes:
//import com.harpoon.api.RadioStreamApi;

RadioStreamApi apiInstance = new RadioStreamApi();
String id = "id_example"; // String | RadioStream id
try {
    apiInstance.radioStreamPrototypeDeleteRadioShows(id);
} catch (ApiException e) {
    System.err.println("Exception when calling RadioStreamApi#radioStreamPrototypeDeleteRadioShows");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| RadioStream id |

### Return type

null (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioStreamPrototypeDestroyByIdRadioShows"></a>
# **radioStreamPrototypeDestroyByIdRadioShows**
> radioStreamPrototypeDestroyByIdRadioShows(fk, id)

Delete a related item by id for radioShows.

### Example
```java
// Import classes:
//import com.harpoon.api.RadioStreamApi;

RadioStreamApi apiInstance = new RadioStreamApi();
String fk = "fk_example"; // String | Foreign key for radioShows
String id = "id_example"; // String | RadioStream id
try {
    apiInstance.radioStreamPrototypeDestroyByIdRadioShows(fk, id);
} catch (ApiException e) {
    System.err.println("Exception when calling RadioStreamApi#radioStreamPrototypeDestroyByIdRadioShows");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for radioShows |
 **id** | **String**| RadioStream id |

### Return type

null (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioStreamPrototypeFindByIdRadioShows"></a>
# **radioStreamPrototypeFindByIdRadioShows**
> RadioShow radioStreamPrototypeFindByIdRadioShows(fk, id)

Find a related item by id for radioShows.

### Example
```java
// Import classes:
//import com.harpoon.api.RadioStreamApi;

RadioStreamApi apiInstance = new RadioStreamApi();
String fk = "fk_example"; // String | Foreign key for radioShows
String id = "id_example"; // String | RadioStream id
try {
    RadioShow result = apiInstance.radioStreamPrototypeFindByIdRadioShows(fk, id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RadioStreamApi#radioStreamPrototypeFindByIdRadioShows");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for radioShows |
 **id** | **String**| RadioStream id |

### Return type

[**RadioShow**](RadioShow.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioStreamPrototypeGetRadioShows"></a>
# **radioStreamPrototypeGetRadioShows**
> List&lt;RadioShow&gt; radioStreamPrototypeGetRadioShows(id, filter)

Queries radioShows of RadioStream.

### Example
```java
// Import classes:
//import com.harpoon.api.RadioStreamApi;

RadioStreamApi apiInstance = new RadioStreamApi();
String id = "id_example"; // String | RadioStream id
String filter = "filter_example"; // String | 
try {
    List<RadioShow> result = apiInstance.radioStreamPrototypeGetRadioShows(id, filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RadioStreamApi#radioStreamPrototypeGetRadioShows");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| RadioStream id |
 **filter** | **String**|  | [optional]

### Return type

[**List&lt;RadioShow&gt;**](RadioShow.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioStreamPrototypeUpdateAttributesPatchRadioStreamsid"></a>
# **radioStreamPrototypeUpdateAttributesPatchRadioStreamsid**
> RadioStream radioStreamPrototypeUpdateAttributesPatchRadioStreamsid(id, data)

Patch attributes for a model instance and persist it into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.RadioStreamApi;

RadioStreamApi apiInstance = new RadioStreamApi();
String id = "id_example"; // String | RadioStream id
RadioStream data = new RadioStream(); // RadioStream | An object of model property name/value pairs
try {
    RadioStream result = apiInstance.radioStreamPrototypeUpdateAttributesPatchRadioStreamsid(id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RadioStreamApi#radioStreamPrototypeUpdateAttributesPatchRadioStreamsid");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| RadioStream id |
 **data** | [**RadioStream**](RadioStream.md)| An object of model property name/value pairs | [optional]

### Return type

[**RadioStream**](RadioStream.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioStreamPrototypeUpdateAttributesPutRadioStreamsid"></a>
# **radioStreamPrototypeUpdateAttributesPutRadioStreamsid**
> RadioStream radioStreamPrototypeUpdateAttributesPutRadioStreamsid(id, data)

Patch attributes for a model instance and persist it into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.RadioStreamApi;

RadioStreamApi apiInstance = new RadioStreamApi();
String id = "id_example"; // String | RadioStream id
RadioStream data = new RadioStream(); // RadioStream | An object of model property name/value pairs
try {
    RadioStream result = apiInstance.radioStreamPrototypeUpdateAttributesPutRadioStreamsid(id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RadioStreamApi#radioStreamPrototypeUpdateAttributesPutRadioStreamsid");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| RadioStream id |
 **data** | [**RadioStream**](RadioStream.md)| An object of model property name/value pairs | [optional]

### Return type

[**RadioStream**](RadioStream.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioStreamPrototypeUpdateByIdRadioShows"></a>
# **radioStreamPrototypeUpdateByIdRadioShows**
> RadioShow radioStreamPrototypeUpdateByIdRadioShows(fk, id, data)

Update a related item by id for radioShows.

### Example
```java
// Import classes:
//import com.harpoon.api.RadioStreamApi;

RadioStreamApi apiInstance = new RadioStreamApi();
String fk = "fk_example"; // String | Foreign key for radioShows
String id = "id_example"; // String | RadioStream id
RadioShow data = new RadioShow(); // RadioShow | 
try {
    RadioShow result = apiInstance.radioStreamPrototypeUpdateByIdRadioShows(fk, id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RadioStreamApi#radioStreamPrototypeUpdateByIdRadioShows");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for radioShows |
 **id** | **String**| RadioStream id |
 **data** | [**RadioShow**](RadioShow.md)|  | [optional]

### Return type

[**RadioShow**](RadioShow.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioStreamRadioShowCurrent"></a>
# **radioStreamRadioShowCurrent**
> RadioShow radioStreamRadioShowCurrent(id)



### Example
```java
// Import classes:
//import com.harpoon.api.RadioStreamApi;

RadioStreamApi apiInstance = new RadioStreamApi();
String id = "id_example"; // String | Model Id
try {
    RadioShow result = apiInstance.radioStreamRadioShowCurrent(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RadioStreamApi#radioStreamRadioShowCurrent");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model Id |

### Return type

[**RadioShow**](RadioShow.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioStreamRadioShowSchedule"></a>
# **radioStreamRadioShowSchedule**
> List&lt;RadioShow&gt; radioStreamRadioShowSchedule(id)



### Example
```java
// Import classes:
//import com.harpoon.api.RadioStreamApi;

RadioStreamApi apiInstance = new RadioStreamApi();
String id = "id_example"; // String | Model Id
try {
    List<RadioShow> result = apiInstance.radioStreamRadioShowSchedule(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RadioStreamApi#radioStreamRadioShowSchedule");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model Id |

### Return type

[**List&lt;RadioShow&gt;**](RadioShow.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioStreamReplaceById"></a>
# **radioStreamReplaceById**
> RadioStream radioStreamReplaceById(id, data)

Replace attributes for a model instance and persist it into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.RadioStreamApi;

RadioStreamApi apiInstance = new RadioStreamApi();
String id = "id_example"; // String | Model id
RadioStream data = new RadioStream(); // RadioStream | Model instance data
try {
    RadioStream result = apiInstance.radioStreamReplaceById(id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RadioStreamApi#radioStreamReplaceById");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |
 **data** | [**RadioStream**](RadioStream.md)| Model instance data | [optional]

### Return type

[**RadioStream**](RadioStream.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioStreamReplaceOrCreate"></a>
# **radioStreamReplaceOrCreate**
> RadioStream radioStreamReplaceOrCreate(data)

Replace an existing model instance or insert a new one into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.RadioStreamApi;

RadioStreamApi apiInstance = new RadioStreamApi();
RadioStream data = new RadioStream(); // RadioStream | Model instance data
try {
    RadioStream result = apiInstance.radioStreamReplaceOrCreate(data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RadioStreamApi#radioStreamReplaceOrCreate");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**RadioStream**](RadioStream.md)| Model instance data | [optional]

### Return type

[**RadioStream**](RadioStream.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioStreamUpdateAll"></a>
# **radioStreamUpdateAll**
> InlineResponse2002 radioStreamUpdateAll(where, data)

Update instances of the model matched by {{where}} from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.RadioStreamApi;

RadioStreamApi apiInstance = new RadioStreamApi();
String where = "where_example"; // String | Criteria to match model instances
RadioStream data = new RadioStream(); // RadioStream | An object of model property name/value pairs
try {
    InlineResponse2002 result = apiInstance.radioStreamUpdateAll(where, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RadioStreamApi#radioStreamUpdateAll");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional]
 **data** | [**RadioStream**](RadioStream.md)| An object of model property name/value pairs | [optional]

### Return type

[**InlineResponse2002**](InlineResponse2002.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioStreamUpsertPatchRadioStreams"></a>
# **radioStreamUpsertPatchRadioStreams**
> RadioStream radioStreamUpsertPatchRadioStreams(data)

Patch an existing model instance or insert a new one into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.RadioStreamApi;

RadioStreamApi apiInstance = new RadioStreamApi();
RadioStream data = new RadioStream(); // RadioStream | Model instance data
try {
    RadioStream result = apiInstance.radioStreamUpsertPatchRadioStreams(data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RadioStreamApi#radioStreamUpsertPatchRadioStreams");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**RadioStream**](RadioStream.md)| Model instance data | [optional]

### Return type

[**RadioStream**](RadioStream.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioStreamUpsertPutRadioStreams"></a>
# **radioStreamUpsertPutRadioStreams**
> RadioStream radioStreamUpsertPutRadioStreams(data)

Patch an existing model instance or insert a new one into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.RadioStreamApi;

RadioStreamApi apiInstance = new RadioStreamApi();
RadioStream data = new RadioStream(); // RadioStream | Model instance data
try {
    RadioStream result = apiInstance.radioStreamUpsertPutRadioStreams(data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RadioStreamApi#radioStreamUpsertPutRadioStreams");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**RadioStream**](RadioStream.md)| Model instance data | [optional]

### Return type

[**RadioStream**](RadioStream.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioStreamUpsertWithWhere"></a>
# **radioStreamUpsertWithWhere**
> RadioStream radioStreamUpsertWithWhere(where, data)

Update an existing model instance or insert a new one into the data source based on the where criteria.

### Example
```java
// Import classes:
//import com.harpoon.api.RadioStreamApi;

RadioStreamApi apiInstance = new RadioStreamApi();
String where = "where_example"; // String | Criteria to match model instances
RadioStream data = new RadioStream(); // RadioStream | An object of model property name/value pairs
try {
    RadioStream result = apiInstance.radioStreamUpsertWithWhere(where, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RadioStreamApi#radioStreamUpsertWithWhere");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional]
 **data** | [**RadioStream**](RadioStream.md)| An object of model property name/value pairs | [optional]

### Return type

[**RadioStream**](RadioStream.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

