
# Product

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** |  |  [optional]
**description** | **String** |  |  [optional]
**cover** | **String** |  |  [optional]
**price** | **Double** |  |  [optional]
**baseCurrency** | **String** |  |  [optional]
**id** | **Double** |  |  [optional]



