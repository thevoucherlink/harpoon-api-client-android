
# AwEventbookingTicket

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Double** |  | 
**eventTicketId** | **Double** |  | 
**orderItemId** | **Double** |  | 
**code** | **String** |  | 
**redeemed** | **Double** |  | 
**paymentStatus** | **Double** |  | 
**createdAt** | [**Date**](Date.md) |  |  [optional]
**redeemedAt** | [**Date**](Date.md) |  |  [optional]
**redeemedByTerminal** | **String** |  |  [optional]
**fee** | **String** |  | 
**isPassOnFee** | **Double** |  | 
**competitionwinnerId** | **Double** |  | 



