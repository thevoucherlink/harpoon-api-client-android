
# Category

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** |  |  [optional]
**productCount** | **Double** |  |  [optional]
**hasChildren** | **Boolean** |  |  [optional]
**parent** | [**Category**](Category.md) |  |  [optional]
**isPrimary** | **Boolean** |  |  [optional]
**children** | [**List&lt;Category&gt;**](Category.md) |  |  [optional]
**id** | **Double** |  |  [optional]



