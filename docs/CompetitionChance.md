
# CompetitionChance

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** |  |  [optional]
**price** | **Double** |  |  [optional]
**chanceCount** | **Double** |  |  [optional]
**qtyBought** | **Double** |  |  [optional]
**qtyTotal** | **Double** |  |  [optional]
**qtyLeft** | **Double** |  |  [optional]
**id** | **Double** |  |  [optional]



