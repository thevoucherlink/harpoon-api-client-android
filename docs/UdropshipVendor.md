
# UdropshipVendor

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Double** |  | 
**vendorName** | **String** |  | 
**vendorAttn** | **String** |  | 
**email** | **String** |  | 
**street** | **String** |  | 
**city** | **String** |  | 
**zip** | **String** |  |  [optional]
**countryId** | **String** |  | 
**regionId** | **Double** |  |  [optional]
**region** | **String** |  |  [optional]
**telephone** | **String** |  |  [optional]
**fax** | **String** |  |  [optional]
**status** | **Boolean** |  | 
**password** | **String** |  |  [optional]
**passwordHash** | **String** |  |  [optional]
**passwordEnc** | **String** |  |  [optional]
**carrierCode** | **String** |  |  [optional]
**notifyNewOrder** | **Double** |  | 
**labelType** | **String** |  | 
**testMode** | **Double** |  | 
**handlingFee** | **String** |  | 
**upsShipperNumber** | **String** |  |  [optional]
**customDataCombined** | **String** |  |  [optional]
**customVarsCombined** | **String** |  |  [optional]
**emailTemplate** | **Double** |  |  [optional]
**urlKey** | **String** |  |  [optional]
**randomHash** | **String** |  |  [optional]
**createdAt** | [**Date**](Date.md) |  |  [optional]
**notifyLowstock** | **Double** |  |  [optional]
**notifyLowstockQty** | **String** |  |  [optional]
**useHandlingFee** | **Double** |  |  [optional]
**useRatesFallback** | **Double** |  |  [optional]
**allowShippingExtraCharge** | **Double** |  |  [optional]
**defaultShippingExtraChargeSuffix** | **String** |  |  [optional]
**defaultShippingExtraChargeType** | **String** |  |  [optional]
**defaultShippingExtraCharge** | **String** |  |  [optional]
**isExtraChargeShippingDefault** | **Double** |  |  [optional]
**defaultShippingId** | **Double** |  |  [optional]
**billingUseShipping** | **Double** |  | 
**billingEmail** | **String** |  | 
**billingTelephone** | **String** |  |  [optional]
**billingFax** | **String** |  |  [optional]
**billingVendorAttn** | **String** |  | 
**billingStreet** | **String** |  | 
**billingCity** | **String** |  | 
**billingZip** | **String** |  |  [optional]
**billingCountryId** | **String** |  | 
**billingRegionId** | **Double** |  |  [optional]
**billingRegion** | **String** |  |  [optional]
**subdomainLevel** | **Double** |  | 
**updateStoreBaseUrl** | **Double** |  | 
**confirmation** | **String** |  |  [optional]
**confirmationSent** | **Double** |  |  [optional]
**rejectReason** | **String** |  |  [optional]
**backorderByAvailability** | **Double** |  |  [optional]
**useReservedQty** | **Double** |  |  [optional]
**tiercomRates** | **String** |  |  [optional]
**tiercomFixedRule** | **String** |  |  [optional]
**tiercomFixedRates** | **String** |  |  [optional]
**tiercomFixedCalcType** | **String** |  |  [optional]
**tiershipRates** | **String** |  |  [optional]
**tiershipSimpleRates** | **String** |  |  [optional]
**tiershipUseV2Rates** | **Double** |  |  [optional]
**vacationMode** | **Double** |  |  [optional]
**vacationEnd** | [**Date**](Date.md) |  |  [optional]
**vacationMessage** | **String** |  |  [optional]
**udmemberLimitProducts** | **Double** |  |  [optional]
**udmemberProfileId** | **Double** |  |  [optional]
**udmemberProfileRefid** | **String** |  |  [optional]
**udmemberMembershipCode** | **String** |  |  [optional]
**udmemberMembershipTitle** | **String** |  |  [optional]
**udmemberBillingType** | **String** |  |  [optional]
**udmemberHistory** | **String** |  |  [optional]
**udmemberProfileSyncOff** | **Double** |  |  [optional]
**udmemberAllowMicrosite** | **Double** |  |  [optional]
**udprodTemplateSku** | **String** |  |  [optional]
**vendorTaxClass** | **String** |  |  [optional]
**catalogProducts** | **List&lt;Object&gt;** |  |  [optional]
**vendorLocations** | **List&lt;Object&gt;** |  |  [optional]
**config** | **Object** |  |  [optional]
**partners** | **List&lt;Object&gt;** |  |  [optional]
**udropshipVendorProducts** | **List&lt;Object&gt;** |  |  [optional]
**harpoonHpublicApplicationpartners** | **List&lt;Object&gt;** |  |  [optional]
**harpoonHpublicv12VendorAppCategories** | **List&lt;Object&gt;** |  |  [optional]



