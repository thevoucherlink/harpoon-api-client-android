# AwCollpurCouponApi

All URIs are relative to *https://apicdn.harpoonconnect.com/v1.1.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**awCollpurCouponCount**](AwCollpurCouponApi.md#awCollpurCouponCount) | **GET** /AwCollpurCoupons/count | Count instances of the model matched by where from the data source.
[**awCollpurCouponCreate**](AwCollpurCouponApi.md#awCollpurCouponCreate) | **POST** /AwCollpurCoupons | Create a new instance of the model and persist it into the data source.
[**awCollpurCouponCreateChangeStreamGetAwCollpurCouponsChangeStream**](AwCollpurCouponApi.md#awCollpurCouponCreateChangeStreamGetAwCollpurCouponsChangeStream) | **GET** /AwCollpurCoupons/change-stream | Create a change stream.
[**awCollpurCouponCreateChangeStreamPostAwCollpurCouponsChangeStream**](AwCollpurCouponApi.md#awCollpurCouponCreateChangeStreamPostAwCollpurCouponsChangeStream) | **POST** /AwCollpurCoupons/change-stream | Create a change stream.
[**awCollpurCouponDeleteById**](AwCollpurCouponApi.md#awCollpurCouponDeleteById) | **DELETE** /AwCollpurCoupons/{id} | Delete a model instance by {{id}} from the data source.
[**awCollpurCouponExistsGetAwCollpurCouponsidExists**](AwCollpurCouponApi.md#awCollpurCouponExistsGetAwCollpurCouponsidExists) | **GET** /AwCollpurCoupons/{id}/exists | Check whether a model instance exists in the data source.
[**awCollpurCouponExistsHeadAwCollpurCouponsid**](AwCollpurCouponApi.md#awCollpurCouponExistsHeadAwCollpurCouponsid) | **HEAD** /AwCollpurCoupons/{id} | Check whether a model instance exists in the data source.
[**awCollpurCouponFind**](AwCollpurCouponApi.md#awCollpurCouponFind) | **GET** /AwCollpurCoupons | Find all instances of the model matched by filter from the data source.
[**awCollpurCouponFindById**](AwCollpurCouponApi.md#awCollpurCouponFindById) | **GET** /AwCollpurCoupons/{id} | Find a model instance by {{id}} from the data source.
[**awCollpurCouponFindOne**](AwCollpurCouponApi.md#awCollpurCouponFindOne) | **GET** /AwCollpurCoupons/findOne | Find first instance of the model matched by filter from the data source.
[**awCollpurCouponPrototypeUpdateAttributesPatchAwCollpurCouponsid**](AwCollpurCouponApi.md#awCollpurCouponPrototypeUpdateAttributesPatchAwCollpurCouponsid) | **PATCH** /AwCollpurCoupons/{id} | Patch attributes for a model instance and persist it into the data source.
[**awCollpurCouponPrototypeUpdateAttributesPutAwCollpurCouponsid**](AwCollpurCouponApi.md#awCollpurCouponPrototypeUpdateAttributesPutAwCollpurCouponsid) | **PUT** /AwCollpurCoupons/{id} | Patch attributes for a model instance and persist it into the data source.
[**awCollpurCouponReplaceById**](AwCollpurCouponApi.md#awCollpurCouponReplaceById) | **POST** /AwCollpurCoupons/{id}/replace | Replace attributes for a model instance and persist it into the data source.
[**awCollpurCouponReplaceOrCreate**](AwCollpurCouponApi.md#awCollpurCouponReplaceOrCreate) | **POST** /AwCollpurCoupons/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
[**awCollpurCouponUpdateAll**](AwCollpurCouponApi.md#awCollpurCouponUpdateAll) | **POST** /AwCollpurCoupons/update | Update instances of the model matched by {{where}} from the data source.
[**awCollpurCouponUpsertPatchAwCollpurCoupons**](AwCollpurCouponApi.md#awCollpurCouponUpsertPatchAwCollpurCoupons) | **PATCH** /AwCollpurCoupons | Patch an existing model instance or insert a new one into the data source.
[**awCollpurCouponUpsertPutAwCollpurCoupons**](AwCollpurCouponApi.md#awCollpurCouponUpsertPutAwCollpurCoupons) | **PUT** /AwCollpurCoupons | Patch an existing model instance or insert a new one into the data source.
[**awCollpurCouponUpsertWithWhere**](AwCollpurCouponApi.md#awCollpurCouponUpsertWithWhere) | **POST** /AwCollpurCoupons/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.


<a name="awCollpurCouponCount"></a>
# **awCollpurCouponCount**
> InlineResponse2001 awCollpurCouponCount(where)

Count instances of the model matched by where from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.AwCollpurCouponApi;

AwCollpurCouponApi apiInstance = new AwCollpurCouponApi();
String where = "where_example"; // String | Criteria to match model instances
try {
    InlineResponse2001 result = apiInstance.awCollpurCouponCount(where);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AwCollpurCouponApi#awCollpurCouponCount");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional]

### Return type

[**InlineResponse2001**](InlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awCollpurCouponCreate"></a>
# **awCollpurCouponCreate**
> AwCollpurCoupon awCollpurCouponCreate(data)

Create a new instance of the model and persist it into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.AwCollpurCouponApi;

AwCollpurCouponApi apiInstance = new AwCollpurCouponApi();
AwCollpurCoupon data = new AwCollpurCoupon(); // AwCollpurCoupon | Model instance data
try {
    AwCollpurCoupon result = apiInstance.awCollpurCouponCreate(data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AwCollpurCouponApi#awCollpurCouponCreate");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**AwCollpurCoupon**](AwCollpurCoupon.md)| Model instance data | [optional]

### Return type

[**AwCollpurCoupon**](AwCollpurCoupon.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awCollpurCouponCreateChangeStreamGetAwCollpurCouponsChangeStream"></a>
# **awCollpurCouponCreateChangeStreamGetAwCollpurCouponsChangeStream**
> File awCollpurCouponCreateChangeStreamGetAwCollpurCouponsChangeStream(options)

Create a change stream.

### Example
```java
// Import classes:
//import com.harpoon.api.AwCollpurCouponApi;

AwCollpurCouponApi apiInstance = new AwCollpurCouponApi();
String options = "options_example"; // String | 
try {
    File result = apiInstance.awCollpurCouponCreateChangeStreamGetAwCollpurCouponsChangeStream(options);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AwCollpurCouponApi#awCollpurCouponCreateChangeStreamGetAwCollpurCouponsChangeStream");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **String**|  | [optional]

### Return type

[**File**](File.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awCollpurCouponCreateChangeStreamPostAwCollpurCouponsChangeStream"></a>
# **awCollpurCouponCreateChangeStreamPostAwCollpurCouponsChangeStream**
> File awCollpurCouponCreateChangeStreamPostAwCollpurCouponsChangeStream(options)

Create a change stream.

### Example
```java
// Import classes:
//import com.harpoon.api.AwCollpurCouponApi;

AwCollpurCouponApi apiInstance = new AwCollpurCouponApi();
String options = "options_example"; // String | 
try {
    File result = apiInstance.awCollpurCouponCreateChangeStreamPostAwCollpurCouponsChangeStream(options);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AwCollpurCouponApi#awCollpurCouponCreateChangeStreamPostAwCollpurCouponsChangeStream");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **String**|  | [optional]

### Return type

[**File**](File.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awCollpurCouponDeleteById"></a>
# **awCollpurCouponDeleteById**
> Object awCollpurCouponDeleteById(id)

Delete a model instance by {{id}} from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.AwCollpurCouponApi;

AwCollpurCouponApi apiInstance = new AwCollpurCouponApi();
String id = "id_example"; // String | Model id
try {
    Object result = apiInstance.awCollpurCouponDeleteById(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AwCollpurCouponApi#awCollpurCouponDeleteById");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |

### Return type

**Object**

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awCollpurCouponExistsGetAwCollpurCouponsidExists"></a>
# **awCollpurCouponExistsGetAwCollpurCouponsidExists**
> InlineResponse2003 awCollpurCouponExistsGetAwCollpurCouponsidExists(id)

Check whether a model instance exists in the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.AwCollpurCouponApi;

AwCollpurCouponApi apiInstance = new AwCollpurCouponApi();
String id = "id_example"; // String | Model id
try {
    InlineResponse2003 result = apiInstance.awCollpurCouponExistsGetAwCollpurCouponsidExists(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AwCollpurCouponApi#awCollpurCouponExistsGetAwCollpurCouponsidExists");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |

### Return type

[**InlineResponse2003**](InlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awCollpurCouponExistsHeadAwCollpurCouponsid"></a>
# **awCollpurCouponExistsHeadAwCollpurCouponsid**
> InlineResponse2003 awCollpurCouponExistsHeadAwCollpurCouponsid(id)

Check whether a model instance exists in the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.AwCollpurCouponApi;

AwCollpurCouponApi apiInstance = new AwCollpurCouponApi();
String id = "id_example"; // String | Model id
try {
    InlineResponse2003 result = apiInstance.awCollpurCouponExistsHeadAwCollpurCouponsid(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AwCollpurCouponApi#awCollpurCouponExistsHeadAwCollpurCouponsid");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |

### Return type

[**InlineResponse2003**](InlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awCollpurCouponFind"></a>
# **awCollpurCouponFind**
> List&lt;AwCollpurCoupon&gt; awCollpurCouponFind(filter)

Find all instances of the model matched by filter from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.AwCollpurCouponApi;

AwCollpurCouponApi apiInstance = new AwCollpurCouponApi();
String filter = "filter_example"; // String | Filter defining fields, where, include, order, offset, and limit
try {
    List<AwCollpurCoupon> result = apiInstance.awCollpurCouponFind(filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AwCollpurCouponApi#awCollpurCouponFind");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **String**| Filter defining fields, where, include, order, offset, and limit | [optional]

### Return type

[**List&lt;AwCollpurCoupon&gt;**](AwCollpurCoupon.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awCollpurCouponFindById"></a>
# **awCollpurCouponFindById**
> AwCollpurCoupon awCollpurCouponFindById(id, filter)

Find a model instance by {{id}} from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.AwCollpurCouponApi;

AwCollpurCouponApi apiInstance = new AwCollpurCouponApi();
String id = "id_example"; // String | Model id
String filter = "filter_example"; // String | Filter defining fields and include
try {
    AwCollpurCoupon result = apiInstance.awCollpurCouponFindById(id, filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AwCollpurCouponApi#awCollpurCouponFindById");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |
 **filter** | **String**| Filter defining fields and include | [optional]

### Return type

[**AwCollpurCoupon**](AwCollpurCoupon.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awCollpurCouponFindOne"></a>
# **awCollpurCouponFindOne**
> AwCollpurCoupon awCollpurCouponFindOne(filter)

Find first instance of the model matched by filter from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.AwCollpurCouponApi;

AwCollpurCouponApi apiInstance = new AwCollpurCouponApi();
String filter = "filter_example"; // String | Filter defining fields, where, include, order, offset, and limit
try {
    AwCollpurCoupon result = apiInstance.awCollpurCouponFindOne(filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AwCollpurCouponApi#awCollpurCouponFindOne");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **String**| Filter defining fields, where, include, order, offset, and limit | [optional]

### Return type

[**AwCollpurCoupon**](AwCollpurCoupon.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awCollpurCouponPrototypeUpdateAttributesPatchAwCollpurCouponsid"></a>
# **awCollpurCouponPrototypeUpdateAttributesPatchAwCollpurCouponsid**
> AwCollpurCoupon awCollpurCouponPrototypeUpdateAttributesPatchAwCollpurCouponsid(id, data)

Patch attributes for a model instance and persist it into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.AwCollpurCouponApi;

AwCollpurCouponApi apiInstance = new AwCollpurCouponApi();
String id = "id_example"; // String | AwCollpurCoupon id
AwCollpurCoupon data = new AwCollpurCoupon(); // AwCollpurCoupon | An object of model property name/value pairs
try {
    AwCollpurCoupon result = apiInstance.awCollpurCouponPrototypeUpdateAttributesPatchAwCollpurCouponsid(id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AwCollpurCouponApi#awCollpurCouponPrototypeUpdateAttributesPatchAwCollpurCouponsid");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| AwCollpurCoupon id |
 **data** | [**AwCollpurCoupon**](AwCollpurCoupon.md)| An object of model property name/value pairs | [optional]

### Return type

[**AwCollpurCoupon**](AwCollpurCoupon.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awCollpurCouponPrototypeUpdateAttributesPutAwCollpurCouponsid"></a>
# **awCollpurCouponPrototypeUpdateAttributesPutAwCollpurCouponsid**
> AwCollpurCoupon awCollpurCouponPrototypeUpdateAttributesPutAwCollpurCouponsid(id, data)

Patch attributes for a model instance and persist it into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.AwCollpurCouponApi;

AwCollpurCouponApi apiInstance = new AwCollpurCouponApi();
String id = "id_example"; // String | AwCollpurCoupon id
AwCollpurCoupon data = new AwCollpurCoupon(); // AwCollpurCoupon | An object of model property name/value pairs
try {
    AwCollpurCoupon result = apiInstance.awCollpurCouponPrototypeUpdateAttributesPutAwCollpurCouponsid(id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AwCollpurCouponApi#awCollpurCouponPrototypeUpdateAttributesPutAwCollpurCouponsid");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| AwCollpurCoupon id |
 **data** | [**AwCollpurCoupon**](AwCollpurCoupon.md)| An object of model property name/value pairs | [optional]

### Return type

[**AwCollpurCoupon**](AwCollpurCoupon.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awCollpurCouponReplaceById"></a>
# **awCollpurCouponReplaceById**
> AwCollpurCoupon awCollpurCouponReplaceById(id, data)

Replace attributes for a model instance and persist it into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.AwCollpurCouponApi;

AwCollpurCouponApi apiInstance = new AwCollpurCouponApi();
String id = "id_example"; // String | Model id
AwCollpurCoupon data = new AwCollpurCoupon(); // AwCollpurCoupon | Model instance data
try {
    AwCollpurCoupon result = apiInstance.awCollpurCouponReplaceById(id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AwCollpurCouponApi#awCollpurCouponReplaceById");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |
 **data** | [**AwCollpurCoupon**](AwCollpurCoupon.md)| Model instance data | [optional]

### Return type

[**AwCollpurCoupon**](AwCollpurCoupon.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awCollpurCouponReplaceOrCreate"></a>
# **awCollpurCouponReplaceOrCreate**
> AwCollpurCoupon awCollpurCouponReplaceOrCreate(data)

Replace an existing model instance or insert a new one into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.AwCollpurCouponApi;

AwCollpurCouponApi apiInstance = new AwCollpurCouponApi();
AwCollpurCoupon data = new AwCollpurCoupon(); // AwCollpurCoupon | Model instance data
try {
    AwCollpurCoupon result = apiInstance.awCollpurCouponReplaceOrCreate(data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AwCollpurCouponApi#awCollpurCouponReplaceOrCreate");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**AwCollpurCoupon**](AwCollpurCoupon.md)| Model instance data | [optional]

### Return type

[**AwCollpurCoupon**](AwCollpurCoupon.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awCollpurCouponUpdateAll"></a>
# **awCollpurCouponUpdateAll**
> InlineResponse2002 awCollpurCouponUpdateAll(where, data)

Update instances of the model matched by {{where}} from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.AwCollpurCouponApi;

AwCollpurCouponApi apiInstance = new AwCollpurCouponApi();
String where = "where_example"; // String | Criteria to match model instances
AwCollpurCoupon data = new AwCollpurCoupon(); // AwCollpurCoupon | An object of model property name/value pairs
try {
    InlineResponse2002 result = apiInstance.awCollpurCouponUpdateAll(where, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AwCollpurCouponApi#awCollpurCouponUpdateAll");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional]
 **data** | [**AwCollpurCoupon**](AwCollpurCoupon.md)| An object of model property name/value pairs | [optional]

### Return type

[**InlineResponse2002**](InlineResponse2002.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awCollpurCouponUpsertPatchAwCollpurCoupons"></a>
# **awCollpurCouponUpsertPatchAwCollpurCoupons**
> AwCollpurCoupon awCollpurCouponUpsertPatchAwCollpurCoupons(data)

Patch an existing model instance or insert a new one into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.AwCollpurCouponApi;

AwCollpurCouponApi apiInstance = new AwCollpurCouponApi();
AwCollpurCoupon data = new AwCollpurCoupon(); // AwCollpurCoupon | Model instance data
try {
    AwCollpurCoupon result = apiInstance.awCollpurCouponUpsertPatchAwCollpurCoupons(data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AwCollpurCouponApi#awCollpurCouponUpsertPatchAwCollpurCoupons");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**AwCollpurCoupon**](AwCollpurCoupon.md)| Model instance data | [optional]

### Return type

[**AwCollpurCoupon**](AwCollpurCoupon.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awCollpurCouponUpsertPutAwCollpurCoupons"></a>
# **awCollpurCouponUpsertPutAwCollpurCoupons**
> AwCollpurCoupon awCollpurCouponUpsertPutAwCollpurCoupons(data)

Patch an existing model instance or insert a new one into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.AwCollpurCouponApi;

AwCollpurCouponApi apiInstance = new AwCollpurCouponApi();
AwCollpurCoupon data = new AwCollpurCoupon(); // AwCollpurCoupon | Model instance data
try {
    AwCollpurCoupon result = apiInstance.awCollpurCouponUpsertPutAwCollpurCoupons(data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AwCollpurCouponApi#awCollpurCouponUpsertPutAwCollpurCoupons");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**AwCollpurCoupon**](AwCollpurCoupon.md)| Model instance data | [optional]

### Return type

[**AwCollpurCoupon**](AwCollpurCoupon.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awCollpurCouponUpsertWithWhere"></a>
# **awCollpurCouponUpsertWithWhere**
> AwCollpurCoupon awCollpurCouponUpsertWithWhere(where, data)

Update an existing model instance or insert a new one into the data source based on the where criteria.

### Example
```java
// Import classes:
//import com.harpoon.api.AwCollpurCouponApi;

AwCollpurCouponApi apiInstance = new AwCollpurCouponApi();
String where = "where_example"; // String | Criteria to match model instances
AwCollpurCoupon data = new AwCollpurCoupon(); // AwCollpurCoupon | An object of model property name/value pairs
try {
    AwCollpurCoupon result = apiInstance.awCollpurCouponUpsertWithWhere(where, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AwCollpurCouponApi#awCollpurCouponUpsertWithWhere");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional]
 **data** | [**AwCollpurCoupon**](AwCollpurCoupon.md)| An object of model property name/value pairs | [optional]

### Return type

[**AwCollpurCoupon**](AwCollpurCoupon.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

