# AwCollpurDealPurchasesApi

All URIs are relative to *https://apicdn.harpoonconnect.com/v1.1.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**awCollpurDealPurchasesCount**](AwCollpurDealPurchasesApi.md#awCollpurDealPurchasesCount) | **GET** /AwCollpurDealPurchases/count | Count instances of the model matched by where from the data source.
[**awCollpurDealPurchasesCreate**](AwCollpurDealPurchasesApi.md#awCollpurDealPurchasesCreate) | **POST** /AwCollpurDealPurchases | Create a new instance of the model and persist it into the data source.
[**awCollpurDealPurchasesCreateChangeStreamGetAwCollpurDealPurchasesChangeStream**](AwCollpurDealPurchasesApi.md#awCollpurDealPurchasesCreateChangeStreamGetAwCollpurDealPurchasesChangeStream) | **GET** /AwCollpurDealPurchases/change-stream | Create a change stream.
[**awCollpurDealPurchasesCreateChangeStreamPostAwCollpurDealPurchasesChangeStream**](AwCollpurDealPurchasesApi.md#awCollpurDealPurchasesCreateChangeStreamPostAwCollpurDealPurchasesChangeStream) | **POST** /AwCollpurDealPurchases/change-stream | Create a change stream.
[**awCollpurDealPurchasesDeleteById**](AwCollpurDealPurchasesApi.md#awCollpurDealPurchasesDeleteById) | **DELETE** /AwCollpurDealPurchases/{id} | Delete a model instance by {{id}} from the data source.
[**awCollpurDealPurchasesExistsGetAwCollpurDealPurchasesidExists**](AwCollpurDealPurchasesApi.md#awCollpurDealPurchasesExistsGetAwCollpurDealPurchasesidExists) | **GET** /AwCollpurDealPurchases/{id}/exists | Check whether a model instance exists in the data source.
[**awCollpurDealPurchasesExistsHeadAwCollpurDealPurchasesid**](AwCollpurDealPurchasesApi.md#awCollpurDealPurchasesExistsHeadAwCollpurDealPurchasesid) | **HEAD** /AwCollpurDealPurchases/{id} | Check whether a model instance exists in the data source.
[**awCollpurDealPurchasesFind**](AwCollpurDealPurchasesApi.md#awCollpurDealPurchasesFind) | **GET** /AwCollpurDealPurchases | Find all instances of the model matched by filter from the data source.
[**awCollpurDealPurchasesFindById**](AwCollpurDealPurchasesApi.md#awCollpurDealPurchasesFindById) | **GET** /AwCollpurDealPurchases/{id} | Find a model instance by {{id}} from the data source.
[**awCollpurDealPurchasesFindOne**](AwCollpurDealPurchasesApi.md#awCollpurDealPurchasesFindOne) | **GET** /AwCollpurDealPurchases/findOne | Find first instance of the model matched by filter from the data source.
[**awCollpurDealPurchasesPrototypeCreateAwCollpurCoupon**](AwCollpurDealPurchasesApi.md#awCollpurDealPurchasesPrototypeCreateAwCollpurCoupon) | **POST** /AwCollpurDealPurchases/{id}/awCollpurCoupon | Creates a new instance in awCollpurCoupon of this model.
[**awCollpurDealPurchasesPrototypeDestroyAwCollpurCoupon**](AwCollpurDealPurchasesApi.md#awCollpurDealPurchasesPrototypeDestroyAwCollpurCoupon) | **DELETE** /AwCollpurDealPurchases/{id}/awCollpurCoupon | Deletes awCollpurCoupon of this model.
[**awCollpurDealPurchasesPrototypeGetAwCollpurCoupon**](AwCollpurDealPurchasesApi.md#awCollpurDealPurchasesPrototypeGetAwCollpurCoupon) | **GET** /AwCollpurDealPurchases/{id}/awCollpurCoupon | Fetches hasOne relation awCollpurCoupon.
[**awCollpurDealPurchasesPrototypeUpdateAttributesPatchAwCollpurDealPurchasesid**](AwCollpurDealPurchasesApi.md#awCollpurDealPurchasesPrototypeUpdateAttributesPatchAwCollpurDealPurchasesid) | **PATCH** /AwCollpurDealPurchases/{id} | Patch attributes for a model instance and persist it into the data source.
[**awCollpurDealPurchasesPrototypeUpdateAttributesPutAwCollpurDealPurchasesid**](AwCollpurDealPurchasesApi.md#awCollpurDealPurchasesPrototypeUpdateAttributesPutAwCollpurDealPurchasesid) | **PUT** /AwCollpurDealPurchases/{id} | Patch attributes for a model instance and persist it into the data source.
[**awCollpurDealPurchasesPrototypeUpdateAwCollpurCoupon**](AwCollpurDealPurchasesApi.md#awCollpurDealPurchasesPrototypeUpdateAwCollpurCoupon) | **PUT** /AwCollpurDealPurchases/{id}/awCollpurCoupon | Update awCollpurCoupon of this model.
[**awCollpurDealPurchasesReplaceById**](AwCollpurDealPurchasesApi.md#awCollpurDealPurchasesReplaceById) | **POST** /AwCollpurDealPurchases/{id}/replace | Replace attributes for a model instance and persist it into the data source.
[**awCollpurDealPurchasesReplaceOrCreate**](AwCollpurDealPurchasesApi.md#awCollpurDealPurchasesReplaceOrCreate) | **POST** /AwCollpurDealPurchases/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
[**awCollpurDealPurchasesUpdateAll**](AwCollpurDealPurchasesApi.md#awCollpurDealPurchasesUpdateAll) | **POST** /AwCollpurDealPurchases/update | Update instances of the model matched by {{where}} from the data source.
[**awCollpurDealPurchasesUpsertPatchAwCollpurDealPurchases**](AwCollpurDealPurchasesApi.md#awCollpurDealPurchasesUpsertPatchAwCollpurDealPurchases) | **PATCH** /AwCollpurDealPurchases | Patch an existing model instance or insert a new one into the data source.
[**awCollpurDealPurchasesUpsertPutAwCollpurDealPurchases**](AwCollpurDealPurchasesApi.md#awCollpurDealPurchasesUpsertPutAwCollpurDealPurchases) | **PUT** /AwCollpurDealPurchases | Patch an existing model instance or insert a new one into the data source.
[**awCollpurDealPurchasesUpsertWithWhere**](AwCollpurDealPurchasesApi.md#awCollpurDealPurchasesUpsertWithWhere) | **POST** /AwCollpurDealPurchases/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.


<a name="awCollpurDealPurchasesCount"></a>
# **awCollpurDealPurchasesCount**
> InlineResponse2001 awCollpurDealPurchasesCount(where)

Count instances of the model matched by where from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.AwCollpurDealPurchasesApi;

AwCollpurDealPurchasesApi apiInstance = new AwCollpurDealPurchasesApi();
String where = "where_example"; // String | Criteria to match model instances
try {
    InlineResponse2001 result = apiInstance.awCollpurDealPurchasesCount(where);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AwCollpurDealPurchasesApi#awCollpurDealPurchasesCount");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional]

### Return type

[**InlineResponse2001**](InlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awCollpurDealPurchasesCreate"></a>
# **awCollpurDealPurchasesCreate**
> AwCollpurDealPurchases awCollpurDealPurchasesCreate(data)

Create a new instance of the model and persist it into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.AwCollpurDealPurchasesApi;

AwCollpurDealPurchasesApi apiInstance = new AwCollpurDealPurchasesApi();
AwCollpurDealPurchases data = new AwCollpurDealPurchases(); // AwCollpurDealPurchases | Model instance data
try {
    AwCollpurDealPurchases result = apiInstance.awCollpurDealPurchasesCreate(data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AwCollpurDealPurchasesApi#awCollpurDealPurchasesCreate");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**AwCollpurDealPurchases**](AwCollpurDealPurchases.md)| Model instance data | [optional]

### Return type

[**AwCollpurDealPurchases**](AwCollpurDealPurchases.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awCollpurDealPurchasesCreateChangeStreamGetAwCollpurDealPurchasesChangeStream"></a>
# **awCollpurDealPurchasesCreateChangeStreamGetAwCollpurDealPurchasesChangeStream**
> File awCollpurDealPurchasesCreateChangeStreamGetAwCollpurDealPurchasesChangeStream(options)

Create a change stream.

### Example
```java
// Import classes:
//import com.harpoon.api.AwCollpurDealPurchasesApi;

AwCollpurDealPurchasesApi apiInstance = new AwCollpurDealPurchasesApi();
String options = "options_example"; // String | 
try {
    File result = apiInstance.awCollpurDealPurchasesCreateChangeStreamGetAwCollpurDealPurchasesChangeStream(options);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AwCollpurDealPurchasesApi#awCollpurDealPurchasesCreateChangeStreamGetAwCollpurDealPurchasesChangeStream");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **String**|  | [optional]

### Return type

[**File**](File.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awCollpurDealPurchasesCreateChangeStreamPostAwCollpurDealPurchasesChangeStream"></a>
# **awCollpurDealPurchasesCreateChangeStreamPostAwCollpurDealPurchasesChangeStream**
> File awCollpurDealPurchasesCreateChangeStreamPostAwCollpurDealPurchasesChangeStream(options)

Create a change stream.

### Example
```java
// Import classes:
//import com.harpoon.api.AwCollpurDealPurchasesApi;

AwCollpurDealPurchasesApi apiInstance = new AwCollpurDealPurchasesApi();
String options = "options_example"; // String | 
try {
    File result = apiInstance.awCollpurDealPurchasesCreateChangeStreamPostAwCollpurDealPurchasesChangeStream(options);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AwCollpurDealPurchasesApi#awCollpurDealPurchasesCreateChangeStreamPostAwCollpurDealPurchasesChangeStream");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **String**|  | [optional]

### Return type

[**File**](File.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awCollpurDealPurchasesDeleteById"></a>
# **awCollpurDealPurchasesDeleteById**
> Object awCollpurDealPurchasesDeleteById(id)

Delete a model instance by {{id}} from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.AwCollpurDealPurchasesApi;

AwCollpurDealPurchasesApi apiInstance = new AwCollpurDealPurchasesApi();
String id = "id_example"; // String | Model id
try {
    Object result = apiInstance.awCollpurDealPurchasesDeleteById(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AwCollpurDealPurchasesApi#awCollpurDealPurchasesDeleteById");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |

### Return type

**Object**

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awCollpurDealPurchasesExistsGetAwCollpurDealPurchasesidExists"></a>
# **awCollpurDealPurchasesExistsGetAwCollpurDealPurchasesidExists**
> InlineResponse2003 awCollpurDealPurchasesExistsGetAwCollpurDealPurchasesidExists(id)

Check whether a model instance exists in the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.AwCollpurDealPurchasesApi;

AwCollpurDealPurchasesApi apiInstance = new AwCollpurDealPurchasesApi();
String id = "id_example"; // String | Model id
try {
    InlineResponse2003 result = apiInstance.awCollpurDealPurchasesExistsGetAwCollpurDealPurchasesidExists(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AwCollpurDealPurchasesApi#awCollpurDealPurchasesExistsGetAwCollpurDealPurchasesidExists");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |

### Return type

[**InlineResponse2003**](InlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awCollpurDealPurchasesExistsHeadAwCollpurDealPurchasesid"></a>
# **awCollpurDealPurchasesExistsHeadAwCollpurDealPurchasesid**
> InlineResponse2003 awCollpurDealPurchasesExistsHeadAwCollpurDealPurchasesid(id)

Check whether a model instance exists in the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.AwCollpurDealPurchasesApi;

AwCollpurDealPurchasesApi apiInstance = new AwCollpurDealPurchasesApi();
String id = "id_example"; // String | Model id
try {
    InlineResponse2003 result = apiInstance.awCollpurDealPurchasesExistsHeadAwCollpurDealPurchasesid(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AwCollpurDealPurchasesApi#awCollpurDealPurchasesExistsHeadAwCollpurDealPurchasesid");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |

### Return type

[**InlineResponse2003**](InlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awCollpurDealPurchasesFind"></a>
# **awCollpurDealPurchasesFind**
> List&lt;AwCollpurDealPurchases&gt; awCollpurDealPurchasesFind(filter)

Find all instances of the model matched by filter from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.AwCollpurDealPurchasesApi;

AwCollpurDealPurchasesApi apiInstance = new AwCollpurDealPurchasesApi();
String filter = "filter_example"; // String | Filter defining fields, where, include, order, offset, and limit
try {
    List<AwCollpurDealPurchases> result = apiInstance.awCollpurDealPurchasesFind(filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AwCollpurDealPurchasesApi#awCollpurDealPurchasesFind");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **String**| Filter defining fields, where, include, order, offset, and limit | [optional]

### Return type

[**List&lt;AwCollpurDealPurchases&gt;**](AwCollpurDealPurchases.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awCollpurDealPurchasesFindById"></a>
# **awCollpurDealPurchasesFindById**
> AwCollpurDealPurchases awCollpurDealPurchasesFindById(id, filter)

Find a model instance by {{id}} from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.AwCollpurDealPurchasesApi;

AwCollpurDealPurchasesApi apiInstance = new AwCollpurDealPurchasesApi();
String id = "id_example"; // String | Model id
String filter = "filter_example"; // String | Filter defining fields and include
try {
    AwCollpurDealPurchases result = apiInstance.awCollpurDealPurchasesFindById(id, filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AwCollpurDealPurchasesApi#awCollpurDealPurchasesFindById");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |
 **filter** | **String**| Filter defining fields and include | [optional]

### Return type

[**AwCollpurDealPurchases**](AwCollpurDealPurchases.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awCollpurDealPurchasesFindOne"></a>
# **awCollpurDealPurchasesFindOne**
> AwCollpurDealPurchases awCollpurDealPurchasesFindOne(filter)

Find first instance of the model matched by filter from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.AwCollpurDealPurchasesApi;

AwCollpurDealPurchasesApi apiInstance = new AwCollpurDealPurchasesApi();
String filter = "filter_example"; // String | Filter defining fields, where, include, order, offset, and limit
try {
    AwCollpurDealPurchases result = apiInstance.awCollpurDealPurchasesFindOne(filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AwCollpurDealPurchasesApi#awCollpurDealPurchasesFindOne");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **String**| Filter defining fields, where, include, order, offset, and limit | [optional]

### Return type

[**AwCollpurDealPurchases**](AwCollpurDealPurchases.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awCollpurDealPurchasesPrototypeCreateAwCollpurCoupon"></a>
# **awCollpurDealPurchasesPrototypeCreateAwCollpurCoupon**
> AwCollpurCoupon awCollpurDealPurchasesPrototypeCreateAwCollpurCoupon(id, data)

Creates a new instance in awCollpurCoupon of this model.

### Example
```java
// Import classes:
//import com.harpoon.api.AwCollpurDealPurchasesApi;

AwCollpurDealPurchasesApi apiInstance = new AwCollpurDealPurchasesApi();
String id = "id_example"; // String | AwCollpurDealPurchases id
AwCollpurCoupon data = new AwCollpurCoupon(); // AwCollpurCoupon | 
try {
    AwCollpurCoupon result = apiInstance.awCollpurDealPurchasesPrototypeCreateAwCollpurCoupon(id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AwCollpurDealPurchasesApi#awCollpurDealPurchasesPrototypeCreateAwCollpurCoupon");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| AwCollpurDealPurchases id |
 **data** | [**AwCollpurCoupon**](AwCollpurCoupon.md)|  | [optional]

### Return type

[**AwCollpurCoupon**](AwCollpurCoupon.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awCollpurDealPurchasesPrototypeDestroyAwCollpurCoupon"></a>
# **awCollpurDealPurchasesPrototypeDestroyAwCollpurCoupon**
> awCollpurDealPurchasesPrototypeDestroyAwCollpurCoupon(id)

Deletes awCollpurCoupon of this model.

### Example
```java
// Import classes:
//import com.harpoon.api.AwCollpurDealPurchasesApi;

AwCollpurDealPurchasesApi apiInstance = new AwCollpurDealPurchasesApi();
String id = "id_example"; // String | AwCollpurDealPurchases id
try {
    apiInstance.awCollpurDealPurchasesPrototypeDestroyAwCollpurCoupon(id);
} catch (ApiException e) {
    System.err.println("Exception when calling AwCollpurDealPurchasesApi#awCollpurDealPurchasesPrototypeDestroyAwCollpurCoupon");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| AwCollpurDealPurchases id |

### Return type

null (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awCollpurDealPurchasesPrototypeGetAwCollpurCoupon"></a>
# **awCollpurDealPurchasesPrototypeGetAwCollpurCoupon**
> AwCollpurCoupon awCollpurDealPurchasesPrototypeGetAwCollpurCoupon(id, refresh)

Fetches hasOne relation awCollpurCoupon.

### Example
```java
// Import classes:
//import com.harpoon.api.AwCollpurDealPurchasesApi;

AwCollpurDealPurchasesApi apiInstance = new AwCollpurDealPurchasesApi();
String id = "id_example"; // String | AwCollpurDealPurchases id
Boolean refresh = true; // Boolean | 
try {
    AwCollpurCoupon result = apiInstance.awCollpurDealPurchasesPrototypeGetAwCollpurCoupon(id, refresh);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AwCollpurDealPurchasesApi#awCollpurDealPurchasesPrototypeGetAwCollpurCoupon");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| AwCollpurDealPurchases id |
 **refresh** | **Boolean**|  | [optional]

### Return type

[**AwCollpurCoupon**](AwCollpurCoupon.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awCollpurDealPurchasesPrototypeUpdateAttributesPatchAwCollpurDealPurchasesid"></a>
# **awCollpurDealPurchasesPrototypeUpdateAttributesPatchAwCollpurDealPurchasesid**
> AwCollpurDealPurchases awCollpurDealPurchasesPrototypeUpdateAttributesPatchAwCollpurDealPurchasesid(id, data)

Patch attributes for a model instance and persist it into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.AwCollpurDealPurchasesApi;

AwCollpurDealPurchasesApi apiInstance = new AwCollpurDealPurchasesApi();
String id = "id_example"; // String | AwCollpurDealPurchases id
AwCollpurDealPurchases data = new AwCollpurDealPurchases(); // AwCollpurDealPurchases | An object of model property name/value pairs
try {
    AwCollpurDealPurchases result = apiInstance.awCollpurDealPurchasesPrototypeUpdateAttributesPatchAwCollpurDealPurchasesid(id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AwCollpurDealPurchasesApi#awCollpurDealPurchasesPrototypeUpdateAttributesPatchAwCollpurDealPurchasesid");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| AwCollpurDealPurchases id |
 **data** | [**AwCollpurDealPurchases**](AwCollpurDealPurchases.md)| An object of model property name/value pairs | [optional]

### Return type

[**AwCollpurDealPurchases**](AwCollpurDealPurchases.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awCollpurDealPurchasesPrototypeUpdateAttributesPutAwCollpurDealPurchasesid"></a>
# **awCollpurDealPurchasesPrototypeUpdateAttributesPutAwCollpurDealPurchasesid**
> AwCollpurDealPurchases awCollpurDealPurchasesPrototypeUpdateAttributesPutAwCollpurDealPurchasesid(id, data)

Patch attributes for a model instance and persist it into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.AwCollpurDealPurchasesApi;

AwCollpurDealPurchasesApi apiInstance = new AwCollpurDealPurchasesApi();
String id = "id_example"; // String | AwCollpurDealPurchases id
AwCollpurDealPurchases data = new AwCollpurDealPurchases(); // AwCollpurDealPurchases | An object of model property name/value pairs
try {
    AwCollpurDealPurchases result = apiInstance.awCollpurDealPurchasesPrototypeUpdateAttributesPutAwCollpurDealPurchasesid(id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AwCollpurDealPurchasesApi#awCollpurDealPurchasesPrototypeUpdateAttributesPutAwCollpurDealPurchasesid");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| AwCollpurDealPurchases id |
 **data** | [**AwCollpurDealPurchases**](AwCollpurDealPurchases.md)| An object of model property name/value pairs | [optional]

### Return type

[**AwCollpurDealPurchases**](AwCollpurDealPurchases.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awCollpurDealPurchasesPrototypeUpdateAwCollpurCoupon"></a>
# **awCollpurDealPurchasesPrototypeUpdateAwCollpurCoupon**
> AwCollpurCoupon awCollpurDealPurchasesPrototypeUpdateAwCollpurCoupon(id, data)

Update awCollpurCoupon of this model.

### Example
```java
// Import classes:
//import com.harpoon.api.AwCollpurDealPurchasesApi;

AwCollpurDealPurchasesApi apiInstance = new AwCollpurDealPurchasesApi();
String id = "id_example"; // String | AwCollpurDealPurchases id
AwCollpurCoupon data = new AwCollpurCoupon(); // AwCollpurCoupon | 
try {
    AwCollpurCoupon result = apiInstance.awCollpurDealPurchasesPrototypeUpdateAwCollpurCoupon(id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AwCollpurDealPurchasesApi#awCollpurDealPurchasesPrototypeUpdateAwCollpurCoupon");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| AwCollpurDealPurchases id |
 **data** | [**AwCollpurCoupon**](AwCollpurCoupon.md)|  | [optional]

### Return type

[**AwCollpurCoupon**](AwCollpurCoupon.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awCollpurDealPurchasesReplaceById"></a>
# **awCollpurDealPurchasesReplaceById**
> AwCollpurDealPurchases awCollpurDealPurchasesReplaceById(id, data)

Replace attributes for a model instance and persist it into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.AwCollpurDealPurchasesApi;

AwCollpurDealPurchasesApi apiInstance = new AwCollpurDealPurchasesApi();
String id = "id_example"; // String | Model id
AwCollpurDealPurchases data = new AwCollpurDealPurchases(); // AwCollpurDealPurchases | Model instance data
try {
    AwCollpurDealPurchases result = apiInstance.awCollpurDealPurchasesReplaceById(id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AwCollpurDealPurchasesApi#awCollpurDealPurchasesReplaceById");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |
 **data** | [**AwCollpurDealPurchases**](AwCollpurDealPurchases.md)| Model instance data | [optional]

### Return type

[**AwCollpurDealPurchases**](AwCollpurDealPurchases.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awCollpurDealPurchasesReplaceOrCreate"></a>
# **awCollpurDealPurchasesReplaceOrCreate**
> AwCollpurDealPurchases awCollpurDealPurchasesReplaceOrCreate(data)

Replace an existing model instance or insert a new one into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.AwCollpurDealPurchasesApi;

AwCollpurDealPurchasesApi apiInstance = new AwCollpurDealPurchasesApi();
AwCollpurDealPurchases data = new AwCollpurDealPurchases(); // AwCollpurDealPurchases | Model instance data
try {
    AwCollpurDealPurchases result = apiInstance.awCollpurDealPurchasesReplaceOrCreate(data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AwCollpurDealPurchasesApi#awCollpurDealPurchasesReplaceOrCreate");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**AwCollpurDealPurchases**](AwCollpurDealPurchases.md)| Model instance data | [optional]

### Return type

[**AwCollpurDealPurchases**](AwCollpurDealPurchases.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awCollpurDealPurchasesUpdateAll"></a>
# **awCollpurDealPurchasesUpdateAll**
> InlineResponse2002 awCollpurDealPurchasesUpdateAll(where, data)

Update instances of the model matched by {{where}} from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.AwCollpurDealPurchasesApi;

AwCollpurDealPurchasesApi apiInstance = new AwCollpurDealPurchasesApi();
String where = "where_example"; // String | Criteria to match model instances
AwCollpurDealPurchases data = new AwCollpurDealPurchases(); // AwCollpurDealPurchases | An object of model property name/value pairs
try {
    InlineResponse2002 result = apiInstance.awCollpurDealPurchasesUpdateAll(where, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AwCollpurDealPurchasesApi#awCollpurDealPurchasesUpdateAll");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional]
 **data** | [**AwCollpurDealPurchases**](AwCollpurDealPurchases.md)| An object of model property name/value pairs | [optional]

### Return type

[**InlineResponse2002**](InlineResponse2002.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awCollpurDealPurchasesUpsertPatchAwCollpurDealPurchases"></a>
# **awCollpurDealPurchasesUpsertPatchAwCollpurDealPurchases**
> AwCollpurDealPurchases awCollpurDealPurchasesUpsertPatchAwCollpurDealPurchases(data)

Patch an existing model instance or insert a new one into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.AwCollpurDealPurchasesApi;

AwCollpurDealPurchasesApi apiInstance = new AwCollpurDealPurchasesApi();
AwCollpurDealPurchases data = new AwCollpurDealPurchases(); // AwCollpurDealPurchases | Model instance data
try {
    AwCollpurDealPurchases result = apiInstance.awCollpurDealPurchasesUpsertPatchAwCollpurDealPurchases(data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AwCollpurDealPurchasesApi#awCollpurDealPurchasesUpsertPatchAwCollpurDealPurchases");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**AwCollpurDealPurchases**](AwCollpurDealPurchases.md)| Model instance data | [optional]

### Return type

[**AwCollpurDealPurchases**](AwCollpurDealPurchases.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awCollpurDealPurchasesUpsertPutAwCollpurDealPurchases"></a>
# **awCollpurDealPurchasesUpsertPutAwCollpurDealPurchases**
> AwCollpurDealPurchases awCollpurDealPurchasesUpsertPutAwCollpurDealPurchases(data)

Patch an existing model instance or insert a new one into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.AwCollpurDealPurchasesApi;

AwCollpurDealPurchasesApi apiInstance = new AwCollpurDealPurchasesApi();
AwCollpurDealPurchases data = new AwCollpurDealPurchases(); // AwCollpurDealPurchases | Model instance data
try {
    AwCollpurDealPurchases result = apiInstance.awCollpurDealPurchasesUpsertPutAwCollpurDealPurchases(data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AwCollpurDealPurchasesApi#awCollpurDealPurchasesUpsertPutAwCollpurDealPurchases");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**AwCollpurDealPurchases**](AwCollpurDealPurchases.md)| Model instance data | [optional]

### Return type

[**AwCollpurDealPurchases**](AwCollpurDealPurchases.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awCollpurDealPurchasesUpsertWithWhere"></a>
# **awCollpurDealPurchasesUpsertWithWhere**
> AwCollpurDealPurchases awCollpurDealPurchasesUpsertWithWhere(where, data)

Update an existing model instance or insert a new one into the data source based on the where criteria.

### Example
```java
// Import classes:
//import com.harpoon.api.AwCollpurDealPurchasesApi;

AwCollpurDealPurchasesApi apiInstance = new AwCollpurDealPurchasesApi();
String where = "where_example"; // String | Criteria to match model instances
AwCollpurDealPurchases data = new AwCollpurDealPurchases(); // AwCollpurDealPurchases | An object of model property name/value pairs
try {
    AwCollpurDealPurchases result = apiInstance.awCollpurDealPurchasesUpsertWithWhere(where, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AwCollpurDealPurchasesApi#awCollpurDealPurchasesUpsertWithWhere");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional]
 **data** | [**AwCollpurDealPurchases**](AwCollpurDealPurchases.md)| An object of model property name/value pairs | [optional]

### Return type

[**AwCollpurDealPurchases**](AwCollpurDealPurchases.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

