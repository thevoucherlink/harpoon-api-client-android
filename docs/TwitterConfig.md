
# TwitterConfig

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**apiKey** | **String** |  | 
**apiSecret** | **String** |  | 
**id** | **Double** |  |  [optional]



