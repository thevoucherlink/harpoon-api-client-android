
# FormSection

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**title** | **String** |  |  [optional]
**rows** | [**List&lt;FormRow&gt;**](FormRow.md) |  | 
**id** | **Double** |  |  [optional]



