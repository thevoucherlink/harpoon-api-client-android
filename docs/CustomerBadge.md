
# CustomerBadge

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**offer** | **Double** |  |  [optional]
**event** | **Double** |  |  [optional]
**dealAll** | **Double** |  |  [optional]
**dealCoupon** | **Double** |  |  [optional]
**dealSimple** | **Double** |  |  [optional]
**dealGroup** | **Double** |  |  [optional]
**competition** | **Double** |  |  [optional]
**walletOfferAvailable** | **Double** |  |  [optional]
**walletOfferNew** | **Double** |  |  [optional]
**walletOfferExpiring** | **Double** |  |  [optional]
**walletOfferExpired** | **Double** |  |  [optional]
**notificationUnread** | **Double** |  |  [optional]
**brandFeed** | **Double** |  |  [optional]
**brand** | **Double** |  |  [optional]
**brandActivity** | **Double** |  |  [optional]
**all** | **Double** |  |  [optional]
**id** | **Double** |  |  [optional]



