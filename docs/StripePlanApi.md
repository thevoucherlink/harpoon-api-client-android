# StripePlanApi

All URIs are relative to *https://apicdn.harpoonconnect.com/v1.1.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**stripePlanCount**](StripePlanApi.md#stripePlanCount) | **GET** /StripePlans/count | Count instances of the model matched by where from the data source.
[**stripePlanCreate**](StripePlanApi.md#stripePlanCreate) | **POST** /StripePlans | Create a new instance of the model and persist it into the data source.
[**stripePlanCreateChangeStreamGetStripePlansChangeStream**](StripePlanApi.md#stripePlanCreateChangeStreamGetStripePlansChangeStream) | **GET** /StripePlans/change-stream | Create a change stream.
[**stripePlanCreateChangeStreamPostStripePlansChangeStream**](StripePlanApi.md#stripePlanCreateChangeStreamPostStripePlansChangeStream) | **POST** /StripePlans/change-stream | Create a change stream.
[**stripePlanDeleteById**](StripePlanApi.md#stripePlanDeleteById) | **DELETE** /StripePlans/{id} | Delete a model instance by {{id}} from the data source.
[**stripePlanExistsGetStripePlansidExists**](StripePlanApi.md#stripePlanExistsGetStripePlansidExists) | **GET** /StripePlans/{id}/exists | Check whether a model instance exists in the data source.
[**stripePlanExistsHeadStripePlansid**](StripePlanApi.md#stripePlanExistsHeadStripePlansid) | **HEAD** /StripePlans/{id} | Check whether a model instance exists in the data source.
[**stripePlanFind**](StripePlanApi.md#stripePlanFind) | **GET** /StripePlans | Find all instances of the model matched by filter from the data source.
[**stripePlanFindById**](StripePlanApi.md#stripePlanFindById) | **GET** /StripePlans/{id} | Find a model instance by {{id}} from the data source.
[**stripePlanFindOne**](StripePlanApi.md#stripePlanFindOne) | **GET** /StripePlans/findOne | Find first instance of the model matched by filter from the data source.
[**stripePlanPrototypeUpdateAttributesPatchStripePlansid**](StripePlanApi.md#stripePlanPrototypeUpdateAttributesPatchStripePlansid) | **PATCH** /StripePlans/{id} | Patch attributes for a model instance and persist it into the data source.
[**stripePlanPrototypeUpdateAttributesPutStripePlansid**](StripePlanApi.md#stripePlanPrototypeUpdateAttributesPutStripePlansid) | **PUT** /StripePlans/{id} | Patch attributes for a model instance and persist it into the data source.
[**stripePlanReplaceById**](StripePlanApi.md#stripePlanReplaceById) | **POST** /StripePlans/{id}/replace | Replace attributes for a model instance and persist it into the data source.
[**stripePlanReplaceOrCreate**](StripePlanApi.md#stripePlanReplaceOrCreate) | **POST** /StripePlans/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
[**stripePlanUpdateAll**](StripePlanApi.md#stripePlanUpdateAll) | **POST** /StripePlans/update | Update instances of the model matched by {{where}} from the data source.
[**stripePlanUpsertPatchStripePlans**](StripePlanApi.md#stripePlanUpsertPatchStripePlans) | **PATCH** /StripePlans | Patch an existing model instance or insert a new one into the data source.
[**stripePlanUpsertPutStripePlans**](StripePlanApi.md#stripePlanUpsertPutStripePlans) | **PUT** /StripePlans | Patch an existing model instance or insert a new one into the data source.
[**stripePlanUpsertWithWhere**](StripePlanApi.md#stripePlanUpsertWithWhere) | **POST** /StripePlans/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.


<a name="stripePlanCount"></a>
# **stripePlanCount**
> InlineResponse2001 stripePlanCount(where)

Count instances of the model matched by where from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.StripePlanApi;

StripePlanApi apiInstance = new StripePlanApi();
String where = "where_example"; // String | Criteria to match model instances
try {
    InlineResponse2001 result = apiInstance.stripePlanCount(where);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling StripePlanApi#stripePlanCount");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional]

### Return type

[**InlineResponse2001**](InlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripePlanCreate"></a>
# **stripePlanCreate**
> StripePlan stripePlanCreate(data)

Create a new instance of the model and persist it into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.StripePlanApi;

StripePlanApi apiInstance = new StripePlanApi();
StripePlan data = new StripePlan(); // StripePlan | Model instance data
try {
    StripePlan result = apiInstance.stripePlanCreate(data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling StripePlanApi#stripePlanCreate");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**StripePlan**](StripePlan.md)| Model instance data | [optional]

### Return type

[**StripePlan**](StripePlan.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripePlanCreateChangeStreamGetStripePlansChangeStream"></a>
# **stripePlanCreateChangeStreamGetStripePlansChangeStream**
> File stripePlanCreateChangeStreamGetStripePlansChangeStream(options)

Create a change stream.

### Example
```java
// Import classes:
//import com.harpoon.api.StripePlanApi;

StripePlanApi apiInstance = new StripePlanApi();
String options = "options_example"; // String | 
try {
    File result = apiInstance.stripePlanCreateChangeStreamGetStripePlansChangeStream(options);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling StripePlanApi#stripePlanCreateChangeStreamGetStripePlansChangeStream");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **String**|  | [optional]

### Return type

[**File**](File.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripePlanCreateChangeStreamPostStripePlansChangeStream"></a>
# **stripePlanCreateChangeStreamPostStripePlansChangeStream**
> File stripePlanCreateChangeStreamPostStripePlansChangeStream(options)

Create a change stream.

### Example
```java
// Import classes:
//import com.harpoon.api.StripePlanApi;

StripePlanApi apiInstance = new StripePlanApi();
String options = "options_example"; // String | 
try {
    File result = apiInstance.stripePlanCreateChangeStreamPostStripePlansChangeStream(options);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling StripePlanApi#stripePlanCreateChangeStreamPostStripePlansChangeStream");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **String**|  | [optional]

### Return type

[**File**](File.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripePlanDeleteById"></a>
# **stripePlanDeleteById**
> Object stripePlanDeleteById(id)

Delete a model instance by {{id}} from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.StripePlanApi;

StripePlanApi apiInstance = new StripePlanApi();
String id = "id_example"; // String | Model id
try {
    Object result = apiInstance.stripePlanDeleteById(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling StripePlanApi#stripePlanDeleteById");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |

### Return type

**Object**

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripePlanExistsGetStripePlansidExists"></a>
# **stripePlanExistsGetStripePlansidExists**
> InlineResponse2003 stripePlanExistsGetStripePlansidExists(id)

Check whether a model instance exists in the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.StripePlanApi;

StripePlanApi apiInstance = new StripePlanApi();
String id = "id_example"; // String | Model id
try {
    InlineResponse2003 result = apiInstance.stripePlanExistsGetStripePlansidExists(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling StripePlanApi#stripePlanExistsGetStripePlansidExists");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |

### Return type

[**InlineResponse2003**](InlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripePlanExistsHeadStripePlansid"></a>
# **stripePlanExistsHeadStripePlansid**
> InlineResponse2003 stripePlanExistsHeadStripePlansid(id)

Check whether a model instance exists in the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.StripePlanApi;

StripePlanApi apiInstance = new StripePlanApi();
String id = "id_example"; // String | Model id
try {
    InlineResponse2003 result = apiInstance.stripePlanExistsHeadStripePlansid(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling StripePlanApi#stripePlanExistsHeadStripePlansid");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |

### Return type

[**InlineResponse2003**](InlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripePlanFind"></a>
# **stripePlanFind**
> List&lt;StripePlan&gt; stripePlanFind(filter)

Find all instances of the model matched by filter from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.StripePlanApi;

StripePlanApi apiInstance = new StripePlanApi();
String filter = "filter_example"; // String | Filter defining fields, where, include, order, offset, and limit
try {
    List<StripePlan> result = apiInstance.stripePlanFind(filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling StripePlanApi#stripePlanFind");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **String**| Filter defining fields, where, include, order, offset, and limit | [optional]

### Return type

[**List&lt;StripePlan&gt;**](StripePlan.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripePlanFindById"></a>
# **stripePlanFindById**
> StripePlan stripePlanFindById(id, filter)

Find a model instance by {{id}} from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.StripePlanApi;

StripePlanApi apiInstance = new StripePlanApi();
String id = "id_example"; // String | Model id
String filter = "filter_example"; // String | Filter defining fields and include
try {
    StripePlan result = apiInstance.stripePlanFindById(id, filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling StripePlanApi#stripePlanFindById");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |
 **filter** | **String**| Filter defining fields and include | [optional]

### Return type

[**StripePlan**](StripePlan.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripePlanFindOne"></a>
# **stripePlanFindOne**
> StripePlan stripePlanFindOne(filter)

Find first instance of the model matched by filter from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.StripePlanApi;

StripePlanApi apiInstance = new StripePlanApi();
String filter = "filter_example"; // String | Filter defining fields, where, include, order, offset, and limit
try {
    StripePlan result = apiInstance.stripePlanFindOne(filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling StripePlanApi#stripePlanFindOne");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **String**| Filter defining fields, where, include, order, offset, and limit | [optional]

### Return type

[**StripePlan**](StripePlan.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripePlanPrototypeUpdateAttributesPatchStripePlansid"></a>
# **stripePlanPrototypeUpdateAttributesPatchStripePlansid**
> StripePlan stripePlanPrototypeUpdateAttributesPatchStripePlansid(id, data)

Patch attributes for a model instance and persist it into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.StripePlanApi;

StripePlanApi apiInstance = new StripePlanApi();
String id = "id_example"; // String | StripePlan id
StripePlan data = new StripePlan(); // StripePlan | An object of model property name/value pairs
try {
    StripePlan result = apiInstance.stripePlanPrototypeUpdateAttributesPatchStripePlansid(id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling StripePlanApi#stripePlanPrototypeUpdateAttributesPatchStripePlansid");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| StripePlan id |
 **data** | [**StripePlan**](StripePlan.md)| An object of model property name/value pairs | [optional]

### Return type

[**StripePlan**](StripePlan.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripePlanPrototypeUpdateAttributesPutStripePlansid"></a>
# **stripePlanPrototypeUpdateAttributesPutStripePlansid**
> StripePlan stripePlanPrototypeUpdateAttributesPutStripePlansid(id, data)

Patch attributes for a model instance and persist it into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.StripePlanApi;

StripePlanApi apiInstance = new StripePlanApi();
String id = "id_example"; // String | StripePlan id
StripePlan data = new StripePlan(); // StripePlan | An object of model property name/value pairs
try {
    StripePlan result = apiInstance.stripePlanPrototypeUpdateAttributesPutStripePlansid(id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling StripePlanApi#stripePlanPrototypeUpdateAttributesPutStripePlansid");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| StripePlan id |
 **data** | [**StripePlan**](StripePlan.md)| An object of model property name/value pairs | [optional]

### Return type

[**StripePlan**](StripePlan.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripePlanReplaceById"></a>
# **stripePlanReplaceById**
> StripePlan stripePlanReplaceById(id, data)

Replace attributes for a model instance and persist it into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.StripePlanApi;

StripePlanApi apiInstance = new StripePlanApi();
String id = "id_example"; // String | Model id
StripePlan data = new StripePlan(); // StripePlan | Model instance data
try {
    StripePlan result = apiInstance.stripePlanReplaceById(id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling StripePlanApi#stripePlanReplaceById");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |
 **data** | [**StripePlan**](StripePlan.md)| Model instance data | [optional]

### Return type

[**StripePlan**](StripePlan.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripePlanReplaceOrCreate"></a>
# **stripePlanReplaceOrCreate**
> StripePlan stripePlanReplaceOrCreate(data)

Replace an existing model instance or insert a new one into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.StripePlanApi;

StripePlanApi apiInstance = new StripePlanApi();
StripePlan data = new StripePlan(); // StripePlan | Model instance data
try {
    StripePlan result = apiInstance.stripePlanReplaceOrCreate(data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling StripePlanApi#stripePlanReplaceOrCreate");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**StripePlan**](StripePlan.md)| Model instance data | [optional]

### Return type

[**StripePlan**](StripePlan.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripePlanUpdateAll"></a>
# **stripePlanUpdateAll**
> InlineResponse2002 stripePlanUpdateAll(where, data)

Update instances of the model matched by {{where}} from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.StripePlanApi;

StripePlanApi apiInstance = new StripePlanApi();
String where = "where_example"; // String | Criteria to match model instances
StripePlan data = new StripePlan(); // StripePlan | An object of model property name/value pairs
try {
    InlineResponse2002 result = apiInstance.stripePlanUpdateAll(where, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling StripePlanApi#stripePlanUpdateAll");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional]
 **data** | [**StripePlan**](StripePlan.md)| An object of model property name/value pairs | [optional]

### Return type

[**InlineResponse2002**](InlineResponse2002.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripePlanUpsertPatchStripePlans"></a>
# **stripePlanUpsertPatchStripePlans**
> StripePlan stripePlanUpsertPatchStripePlans(data)

Patch an existing model instance or insert a new one into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.StripePlanApi;

StripePlanApi apiInstance = new StripePlanApi();
StripePlan data = new StripePlan(); // StripePlan | Model instance data
try {
    StripePlan result = apiInstance.stripePlanUpsertPatchStripePlans(data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling StripePlanApi#stripePlanUpsertPatchStripePlans");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**StripePlan**](StripePlan.md)| Model instance data | [optional]

### Return type

[**StripePlan**](StripePlan.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripePlanUpsertPutStripePlans"></a>
# **stripePlanUpsertPutStripePlans**
> StripePlan stripePlanUpsertPutStripePlans(data)

Patch an existing model instance or insert a new one into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.StripePlanApi;

StripePlanApi apiInstance = new StripePlanApi();
StripePlan data = new StripePlan(); // StripePlan | Model instance data
try {
    StripePlan result = apiInstance.stripePlanUpsertPutStripePlans(data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling StripePlanApi#stripePlanUpsertPutStripePlans");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**StripePlan**](StripePlan.md)| Model instance data | [optional]

### Return type

[**StripePlan**](StripePlan.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripePlanUpsertWithWhere"></a>
# **stripePlanUpsertWithWhere**
> StripePlan stripePlanUpsertWithWhere(where, data)

Update an existing model instance or insert a new one into the data source based on the where criteria.

### Example
```java
// Import classes:
//import com.harpoon.api.StripePlanApi;

StripePlanApi apiInstance = new StripePlanApi();
String where = "where_example"; // String | Criteria to match model instances
StripePlan data = new StripePlan(); // StripePlan | An object of model property name/value pairs
try {
    StripePlan result = apiInstance.stripePlanUpsertWithWhere(where, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling StripePlanApi#stripePlanUpsertWithWhere");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional]
 **data** | [**StripePlan**](StripePlan.md)| An object of model property name/value pairs | [optional]

### Return type

[**StripePlan**](StripePlan.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

