# AppApi

All URIs are relative to *https://apicdn.harpoonconnect.com/v1.1.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**appAnalyticTrack**](AppApi.md#appAnalyticTrack) | **POST** /Apps/{id}/analytics/track | 
[**appCount**](AppApi.md#appCount) | **GET** /Apps/count | Count instances of the model matched by where from the data source.
[**appCreateChangeStreamGetAppsChangeStream**](AppApi.md#appCreateChangeStreamGetAppsChangeStream) | **GET** /Apps/change-stream | Create a change stream.
[**appCreateChangeStreamPostAppsChangeStream**](AppApi.md#appCreateChangeStreamPostAppsChangeStream) | **POST** /Apps/change-stream | Create a change stream.
[**appCreateWithAuthentication**](AppApi.md#appCreateWithAuthentication) | **POST** /Apps | Create a new instance of the model and persist it into the data source.
[**appDeleteById**](AppApi.md#appDeleteById) | **DELETE** /Apps/{id} | Delete a model instance by {{id}} from the data source.
[**appExistsGetAppsidExists**](AppApi.md#appExistsGetAppsidExists) | **GET** /Apps/{id}/exists | Check whether a model instance exists in the data source.
[**appExistsHeadAppsid**](AppApi.md#appExistsHeadAppsid) | **HEAD** /Apps/{id} | Check whether a model instance exists in the data source.
[**appFind**](AppApi.md#appFind) | **GET** /Apps | Find all instances of the model matched by filter from the data source.
[**appFindById**](AppApi.md#appFindById) | **GET** /Apps/{id} | Find a model instance by {{id}} from the data source.
[**appFindByIdForVersion**](AppApi.md#appFindByIdForVersion) | **GET** /Apps/{id}/{appOs}/{appVersion} | 
[**appFindOne**](AppApi.md#appFindOne) | **GET** /Apps/findOne | Find first instance of the model matched by filter from the data source.
[**appPrototypeCountAppConfigs**](AppApi.md#appPrototypeCountAppConfigs) | **GET** /Apps/{id}/appConfigs/count | Counts appConfigs of App.
[**appPrototypeCountApps**](AppApi.md#appPrototypeCountApps) | **GET** /Apps/{id}/apps/count | Counts apps of App.
[**appPrototypeCountPlaylists**](AppApi.md#appPrototypeCountPlaylists) | **GET** /Apps/{id}/playlists/count | Counts playlists of App.
[**appPrototypeCountRadioStreams**](AppApi.md#appPrototypeCountRadioStreams) | **GET** /Apps/{id}/radioStreams/count | Counts radioStreams of App.
[**appPrototypeCountRssFeedGroups**](AppApi.md#appPrototypeCountRssFeedGroups) | **GET** /Apps/{id}/rssFeedGroups/count | Counts rssFeedGroups of App.
[**appPrototypeCountRssFeeds**](AppApi.md#appPrototypeCountRssFeeds) | **GET** /Apps/{id}/rssFeeds/count | Counts rssFeeds of App.
[**appPrototypeCreateAppConfigs**](AppApi.md#appPrototypeCreateAppConfigs) | **POST** /Apps/{id}/appConfigs | Creates a new instance in appConfigs of this model.
[**appPrototypeCreateApps**](AppApi.md#appPrototypeCreateApps) | **POST** /Apps/{id}/apps | Creates a new instance in apps of this model.
[**appPrototypeCreateCustomers**](AppApi.md#appPrototypeCreateCustomers) | **POST** /Apps/{id}/customers | Creates a new instance in customers of this model.
[**appPrototypeCreatePlaylists**](AppApi.md#appPrototypeCreatePlaylists) | **POST** /Apps/{id}/playlists | Creates a new instance in playlists of this model.
[**appPrototypeCreateRadioStreams**](AppApi.md#appPrototypeCreateRadioStreams) | **POST** /Apps/{id}/radioStreams | Creates a new instance in radioStreams of this model.
[**appPrototypeCreateRssFeedGroups**](AppApi.md#appPrototypeCreateRssFeedGroups) | **POST** /Apps/{id}/rssFeedGroups | Creates a new instance in rssFeedGroups of this model.
[**appPrototypeCreateRssFeeds**](AppApi.md#appPrototypeCreateRssFeeds) | **POST** /Apps/{id}/rssFeeds | Creates a new instance in rssFeeds of this model.
[**appPrototypeDeleteAppConfigs**](AppApi.md#appPrototypeDeleteAppConfigs) | **DELETE** /Apps/{id}/appConfigs | Deletes all appConfigs of this model.
[**appPrototypeDeleteApps**](AppApi.md#appPrototypeDeleteApps) | **DELETE** /Apps/{id}/apps | Deletes all apps of this model.
[**appPrototypeDeletePlaylists**](AppApi.md#appPrototypeDeletePlaylists) | **DELETE** /Apps/{id}/playlists | Deletes all playlists of this model.
[**appPrototypeDeleteRadioStreams**](AppApi.md#appPrototypeDeleteRadioStreams) | **DELETE** /Apps/{id}/radioStreams | Deletes all radioStreams of this model.
[**appPrototypeDeleteRssFeedGroups**](AppApi.md#appPrototypeDeleteRssFeedGroups) | **DELETE** /Apps/{id}/rssFeedGroups | Deletes all rssFeedGroups of this model.
[**appPrototypeDeleteRssFeeds**](AppApi.md#appPrototypeDeleteRssFeeds) | **DELETE** /Apps/{id}/rssFeeds | Deletes all rssFeeds of this model.
[**appPrototypeDestroyByIdAppConfigs**](AppApi.md#appPrototypeDestroyByIdAppConfigs) | **DELETE** /Apps/{id}/appConfigs/{fk} | Delete a related item by id for appConfigs.
[**appPrototypeDestroyByIdApps**](AppApi.md#appPrototypeDestroyByIdApps) | **DELETE** /Apps/{id}/apps/{fk} | Delete a related item by id for apps.
[**appPrototypeDestroyByIdPlaylists**](AppApi.md#appPrototypeDestroyByIdPlaylists) | **DELETE** /Apps/{id}/playlists/{fk} | Delete a related item by id for playlists.
[**appPrototypeDestroyByIdRadioStreams**](AppApi.md#appPrototypeDestroyByIdRadioStreams) | **DELETE** /Apps/{id}/radioStreams/{fk} | Delete a related item by id for radioStreams.
[**appPrototypeDestroyByIdRssFeedGroups**](AppApi.md#appPrototypeDestroyByIdRssFeedGroups) | **DELETE** /Apps/{id}/rssFeedGroups/{fk} | Delete a related item by id for rssFeedGroups.
[**appPrototypeDestroyByIdRssFeeds**](AppApi.md#appPrototypeDestroyByIdRssFeeds) | **DELETE** /Apps/{id}/rssFeeds/{fk} | Delete a related item by id for rssFeeds.
[**appPrototypeFindByIdAppConfigs**](AppApi.md#appPrototypeFindByIdAppConfigs) | **GET** /Apps/{id}/appConfigs/{fk} | Find a related item by id for appConfigs.
[**appPrototypeFindByIdApps**](AppApi.md#appPrototypeFindByIdApps) | **GET** /Apps/{id}/apps/{fk} | Find a related item by id for apps.
[**appPrototypeFindByIdCustomers**](AppApi.md#appPrototypeFindByIdCustomers) | **GET** /Apps/{id}/customers/{fk} | Find a related item by id for customers.
[**appPrototypeFindByIdPlaylists**](AppApi.md#appPrototypeFindByIdPlaylists) | **GET** /Apps/{id}/playlists/{fk} | Find a related item by id for playlists.
[**appPrototypeFindByIdRadioStreams**](AppApi.md#appPrototypeFindByIdRadioStreams) | **GET** /Apps/{id}/radioStreams/{fk} | Find a related item by id for radioStreams.
[**appPrototypeFindByIdRssFeedGroups**](AppApi.md#appPrototypeFindByIdRssFeedGroups) | **GET** /Apps/{id}/rssFeedGroups/{fk} | Find a related item by id for rssFeedGroups.
[**appPrototypeFindByIdRssFeeds**](AppApi.md#appPrototypeFindByIdRssFeeds) | **GET** /Apps/{id}/rssFeeds/{fk} | Find a related item by id for rssFeeds.
[**appPrototypeGetAppConfigs**](AppApi.md#appPrototypeGetAppConfigs) | **GET** /Apps/{id}/appConfigs | Queries appConfigs of App.
[**appPrototypeGetApps**](AppApi.md#appPrototypeGetApps) | **GET** /Apps/{id}/apps | Queries apps of App.
[**appPrototypeGetCustomers**](AppApi.md#appPrototypeGetCustomers) | **GET** /Apps/{id}/customers | Queries customers of App.
[**appPrototypeGetParent**](AppApi.md#appPrototypeGetParent) | **GET** /Apps/{id}/parent | Fetches belongsTo relation parent.
[**appPrototypeGetPlaylists**](AppApi.md#appPrototypeGetPlaylists) | **GET** /Apps/{id}/playlists | Queries playlists of App.
[**appPrototypeGetRadioStreams**](AppApi.md#appPrototypeGetRadioStreams) | **GET** /Apps/{id}/radioStreams | Queries radioStreams of App.
[**appPrototypeGetRssFeedGroups**](AppApi.md#appPrototypeGetRssFeedGroups) | **GET** /Apps/{id}/rssFeedGroups | Queries rssFeedGroups of App.
[**appPrototypeGetRssFeeds**](AppApi.md#appPrototypeGetRssFeeds) | **GET** /Apps/{id}/rssFeeds | Queries rssFeeds of App.
[**appPrototypeUpdateAttributesPatchAppsid**](AppApi.md#appPrototypeUpdateAttributesPatchAppsid) | **PATCH** /Apps/{id} | Patch attributes for a model instance and persist it into the data source.
[**appPrototypeUpdateAttributesPutAppsid**](AppApi.md#appPrototypeUpdateAttributesPutAppsid) | **PUT** /Apps/{id} | Patch attributes for a model instance and persist it into the data source.
[**appPrototypeUpdateByIdAppConfigs**](AppApi.md#appPrototypeUpdateByIdAppConfigs) | **PUT** /Apps/{id}/appConfigs/{fk} | Update a related item by id for appConfigs.
[**appPrototypeUpdateByIdApps**](AppApi.md#appPrototypeUpdateByIdApps) | **PUT** /Apps/{id}/apps/{fk} | Update a related item by id for apps.
[**appPrototypeUpdateByIdCustomers**](AppApi.md#appPrototypeUpdateByIdCustomers) | **PUT** /Apps/{id}/customers/{fk} | Update a related item by id for customers.
[**appPrototypeUpdateByIdPlaylists**](AppApi.md#appPrototypeUpdateByIdPlaylists) | **PUT** /Apps/{id}/playlists/{fk} | Update a related item by id for playlists.
[**appPrototypeUpdateByIdRadioStreams**](AppApi.md#appPrototypeUpdateByIdRadioStreams) | **PUT** /Apps/{id}/radioStreams/{fk} | Update a related item by id for radioStreams.
[**appPrototypeUpdateByIdRssFeedGroups**](AppApi.md#appPrototypeUpdateByIdRssFeedGroups) | **PUT** /Apps/{id}/rssFeedGroups/{fk} | Update a related item by id for rssFeedGroups.
[**appPrototypeUpdateByIdRssFeeds**](AppApi.md#appPrototypeUpdateByIdRssFeeds) | **PUT** /Apps/{id}/rssFeeds/{fk} | Update a related item by id for rssFeeds.
[**appReplaceById**](AppApi.md#appReplaceById) | **POST** /Apps/{id}/replace | Replace attributes for a model instance and persist it into the data source.
[**appReplaceOrCreate**](AppApi.md#appReplaceOrCreate) | **POST** /Apps/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
[**appUpdateAll**](AppApi.md#appUpdateAll) | **POST** /Apps/update | Update instances of the model matched by {{where}} from the data source.
[**appUploadFile**](AppApi.md#appUploadFile) | **POST** /Apps/{id}/file/{type} | 
[**appUpsertPatchApps**](AppApi.md#appUpsertPatchApps) | **PATCH** /Apps | Patch an existing model instance or insert a new one into the data source.
[**appUpsertPutApps**](AppApi.md#appUpsertPutApps) | **PUT** /Apps | Patch an existing model instance or insert a new one into the data source.
[**appUpsertWithWhere**](AppApi.md#appUpsertWithWhere) | **POST** /Apps/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.
[**appVerify**](AppApi.md#appVerify) | **GET** /Apps/verify | 


<a name="appAnalyticTrack"></a>
# **appAnalyticTrack**
> Object appAnalyticTrack(id, data)



### Example
```java
// Import classes:
//import com.harpoon.api.AppApi;

AppApi apiInstance = new AppApi();
String id = "id_example"; // String | 
AnalyticRequest data = new AnalyticRequest(); // AnalyticRequest | 
try {
    Object result = apiInstance.appAnalyticTrack(id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AppApi#appAnalyticTrack");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**|  |
 **data** | [**AnalyticRequest**](AnalyticRequest.md)|  | [optional]

### Return type

**Object**

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="appCount"></a>
# **appCount**
> InlineResponse2001 appCount(where)

Count instances of the model matched by where from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.AppApi;

AppApi apiInstance = new AppApi();
String where = "where_example"; // String | Criteria to match model instances
try {
    InlineResponse2001 result = apiInstance.appCount(where);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AppApi#appCount");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional]

### Return type

[**InlineResponse2001**](InlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="appCreateChangeStreamGetAppsChangeStream"></a>
# **appCreateChangeStreamGetAppsChangeStream**
> File appCreateChangeStreamGetAppsChangeStream(options)

Create a change stream.

### Example
```java
// Import classes:
//import com.harpoon.api.AppApi;

AppApi apiInstance = new AppApi();
String options = "options_example"; // String | 
try {
    File result = apiInstance.appCreateChangeStreamGetAppsChangeStream(options);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AppApi#appCreateChangeStreamGetAppsChangeStream");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **String**|  | [optional]

### Return type

[**File**](File.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="appCreateChangeStreamPostAppsChangeStream"></a>
# **appCreateChangeStreamPostAppsChangeStream**
> File appCreateChangeStreamPostAppsChangeStream(options)

Create a change stream.

### Example
```java
// Import classes:
//import com.harpoon.api.AppApi;

AppApi apiInstance = new AppApi();
String options = "options_example"; // String | 
try {
    File result = apiInstance.appCreateChangeStreamPostAppsChangeStream(options);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AppApi#appCreateChangeStreamPostAppsChangeStream");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **String**|  | [optional]

### Return type

[**File**](File.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="appCreateWithAuthentication"></a>
# **appCreateWithAuthentication**
> App appCreateWithAuthentication(data)

Create a new instance of the model and persist it into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.AppApi;

AppApi apiInstance = new AppApi();
App data = new App(); // App | Model instance data
try {
    App result = apiInstance.appCreateWithAuthentication(data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AppApi#appCreateWithAuthentication");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**App**](App.md)| Model instance data | [optional]

### Return type

[**App**](App.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="appDeleteById"></a>
# **appDeleteById**
> Object appDeleteById(id)

Delete a model instance by {{id}} from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.AppApi;

AppApi apiInstance = new AppApi();
String id = "id_example"; // String | Model id
try {
    Object result = apiInstance.appDeleteById(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AppApi#appDeleteById");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |

### Return type

**Object**

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="appExistsGetAppsidExists"></a>
# **appExistsGetAppsidExists**
> InlineResponse2003 appExistsGetAppsidExists(id)

Check whether a model instance exists in the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.AppApi;

AppApi apiInstance = new AppApi();
String id = "id_example"; // String | Model id
try {
    InlineResponse2003 result = apiInstance.appExistsGetAppsidExists(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AppApi#appExistsGetAppsidExists");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |

### Return type

[**InlineResponse2003**](InlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="appExistsHeadAppsid"></a>
# **appExistsHeadAppsid**
> InlineResponse2003 appExistsHeadAppsid(id)

Check whether a model instance exists in the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.AppApi;

AppApi apiInstance = new AppApi();
String id = "id_example"; // String | Model id
try {
    InlineResponse2003 result = apiInstance.appExistsHeadAppsid(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AppApi#appExistsHeadAppsid");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |

### Return type

[**InlineResponse2003**](InlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="appFind"></a>
# **appFind**
> List&lt;App&gt; appFind(filter)

Find all instances of the model matched by filter from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.AppApi;

AppApi apiInstance = new AppApi();
String filter = "filter_example"; // String | Filter defining fields, where, include, order, offset, and limit
try {
    List<App> result = apiInstance.appFind(filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AppApi#appFind");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **String**| Filter defining fields, where, include, order, offset, and limit | [optional]

### Return type

[**List&lt;App&gt;**](App.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="appFindById"></a>
# **appFindById**
> App appFindById(id, filter)

Find a model instance by {{id}} from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.AppApi;

AppApi apiInstance = new AppApi();
String id = "id_example"; // String | Model id
String filter = "filter_example"; // String | Filter defining fields and include
try {
    App result = apiInstance.appFindById(id, filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AppApi#appFindById");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |
 **filter** | **String**| Filter defining fields and include | [optional]

### Return type

[**App**](App.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="appFindByIdForVersion"></a>
# **appFindByIdForVersion**
> App appFindByIdForVersion(id, appOs, appVersion)



### Example
```java
// Import classes:
//import com.harpoon.api.AppApi;

AppApi apiInstance = new AppApi();
String id = "id_example"; // String | 
String appOs = "appOs_example"; // String | Either \"ios\" or \"android\"
String appVersion = "appVersion_example"; // String | e.g. 1.2.3
try {
    App result = apiInstance.appFindByIdForVersion(id, appOs, appVersion);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AppApi#appFindByIdForVersion");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**|  |
 **appOs** | **String**| Either \&quot;ios\&quot; or \&quot;android\&quot; |
 **appVersion** | **String**| e.g. 1.2.3 |

### Return type

[**App**](App.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="appFindOne"></a>
# **appFindOne**
> App appFindOne(filter)

Find first instance of the model matched by filter from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.AppApi;

AppApi apiInstance = new AppApi();
String filter = "filter_example"; // String | Filter defining fields, where, include, order, offset, and limit
try {
    App result = apiInstance.appFindOne(filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AppApi#appFindOne");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **String**| Filter defining fields, where, include, order, offset, and limit | [optional]

### Return type

[**App**](App.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="appPrototypeCountAppConfigs"></a>
# **appPrototypeCountAppConfigs**
> InlineResponse2001 appPrototypeCountAppConfigs(id, where)

Counts appConfigs of App.

### Example
```java
// Import classes:
//import com.harpoon.api.AppApi;

AppApi apiInstance = new AppApi();
String id = "id_example"; // String | App id
String where = "where_example"; // String | Criteria to match model instances
try {
    InlineResponse2001 result = apiInstance.appPrototypeCountAppConfigs(id, where);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AppApi#appPrototypeCountAppConfigs");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| App id |
 **where** | **String**| Criteria to match model instances | [optional]

### Return type

[**InlineResponse2001**](InlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="appPrototypeCountApps"></a>
# **appPrototypeCountApps**
> InlineResponse2001 appPrototypeCountApps(id, where)

Counts apps of App.

### Example
```java
// Import classes:
//import com.harpoon.api.AppApi;

AppApi apiInstance = new AppApi();
String id = "id_example"; // String | App id
String where = "where_example"; // String | Criteria to match model instances
try {
    InlineResponse2001 result = apiInstance.appPrototypeCountApps(id, where);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AppApi#appPrototypeCountApps");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| App id |
 **where** | **String**| Criteria to match model instances | [optional]

### Return type

[**InlineResponse2001**](InlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="appPrototypeCountPlaylists"></a>
# **appPrototypeCountPlaylists**
> InlineResponse2001 appPrototypeCountPlaylists(id, where)

Counts playlists of App.

### Example
```java
// Import classes:
//import com.harpoon.api.AppApi;

AppApi apiInstance = new AppApi();
String id = "id_example"; // String | App id
String where = "where_example"; // String | Criteria to match model instances
try {
    InlineResponse2001 result = apiInstance.appPrototypeCountPlaylists(id, where);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AppApi#appPrototypeCountPlaylists");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| App id |
 **where** | **String**| Criteria to match model instances | [optional]

### Return type

[**InlineResponse2001**](InlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="appPrototypeCountRadioStreams"></a>
# **appPrototypeCountRadioStreams**
> InlineResponse2001 appPrototypeCountRadioStreams(id, where)

Counts radioStreams of App.

### Example
```java
// Import classes:
//import com.harpoon.api.AppApi;

AppApi apiInstance = new AppApi();
String id = "id_example"; // String | App id
String where = "where_example"; // String | Criteria to match model instances
try {
    InlineResponse2001 result = apiInstance.appPrototypeCountRadioStreams(id, where);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AppApi#appPrototypeCountRadioStreams");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| App id |
 **where** | **String**| Criteria to match model instances | [optional]

### Return type

[**InlineResponse2001**](InlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="appPrototypeCountRssFeedGroups"></a>
# **appPrototypeCountRssFeedGroups**
> InlineResponse2001 appPrototypeCountRssFeedGroups(id, where)

Counts rssFeedGroups of App.

### Example
```java
// Import classes:
//import com.harpoon.api.AppApi;

AppApi apiInstance = new AppApi();
String id = "id_example"; // String | App id
String where = "where_example"; // String | Criteria to match model instances
try {
    InlineResponse2001 result = apiInstance.appPrototypeCountRssFeedGroups(id, where);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AppApi#appPrototypeCountRssFeedGroups");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| App id |
 **where** | **String**| Criteria to match model instances | [optional]

### Return type

[**InlineResponse2001**](InlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="appPrototypeCountRssFeeds"></a>
# **appPrototypeCountRssFeeds**
> InlineResponse2001 appPrototypeCountRssFeeds(id, where)

Counts rssFeeds of App.

### Example
```java
// Import classes:
//import com.harpoon.api.AppApi;

AppApi apiInstance = new AppApi();
String id = "id_example"; // String | App id
String where = "where_example"; // String | Criteria to match model instances
try {
    InlineResponse2001 result = apiInstance.appPrototypeCountRssFeeds(id, where);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AppApi#appPrototypeCountRssFeeds");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| App id |
 **where** | **String**| Criteria to match model instances | [optional]

### Return type

[**InlineResponse2001**](InlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="appPrototypeCreateAppConfigs"></a>
# **appPrototypeCreateAppConfigs**
> AppConfig appPrototypeCreateAppConfigs(id, data)

Creates a new instance in appConfigs of this model.

### Example
```java
// Import classes:
//import com.harpoon.api.AppApi;

AppApi apiInstance = new AppApi();
String id = "id_example"; // String | App id
AppConfig data = new AppConfig(); // AppConfig | 
try {
    AppConfig result = apiInstance.appPrototypeCreateAppConfigs(id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AppApi#appPrototypeCreateAppConfigs");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| App id |
 **data** | [**AppConfig**](AppConfig.md)|  | [optional]

### Return type

[**AppConfig**](AppConfig.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="appPrototypeCreateApps"></a>
# **appPrototypeCreateApps**
> App appPrototypeCreateApps(id, data)

Creates a new instance in apps of this model.

### Example
```java
// Import classes:
//import com.harpoon.api.AppApi;

AppApi apiInstance = new AppApi();
String id = "id_example"; // String | App id
App data = new App(); // App | 
try {
    App result = apiInstance.appPrototypeCreateApps(id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AppApi#appPrototypeCreateApps");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| App id |
 **data** | [**App**](App.md)|  | [optional]

### Return type

[**App**](App.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="appPrototypeCreateCustomers"></a>
# **appPrototypeCreateCustomers**
> Customer appPrototypeCreateCustomers(id, data)

Creates a new instance in customers of this model.

### Example
```java
// Import classes:
//import com.harpoon.api.AppApi;

AppApi apiInstance = new AppApi();
String id = "id_example"; // String | App id
Customer data = new Customer(); // Customer | 
try {
    Customer result = apiInstance.appPrototypeCreateCustomers(id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AppApi#appPrototypeCreateCustomers");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| App id |
 **data** | [**Customer**](Customer.md)|  | [optional]

### Return type

[**Customer**](Customer.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="appPrototypeCreatePlaylists"></a>
# **appPrototypeCreatePlaylists**
> Playlist appPrototypeCreatePlaylists(id, data)

Creates a new instance in playlists of this model.

### Example
```java
// Import classes:
//import com.harpoon.api.AppApi;

AppApi apiInstance = new AppApi();
String id = "id_example"; // String | App id
Playlist data = new Playlist(); // Playlist | 
try {
    Playlist result = apiInstance.appPrototypeCreatePlaylists(id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AppApi#appPrototypeCreatePlaylists");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| App id |
 **data** | [**Playlist**](Playlist.md)|  | [optional]

### Return type

[**Playlist**](Playlist.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="appPrototypeCreateRadioStreams"></a>
# **appPrototypeCreateRadioStreams**
> RadioStream appPrototypeCreateRadioStreams(id, data)

Creates a new instance in radioStreams of this model.

### Example
```java
// Import classes:
//import com.harpoon.api.AppApi;

AppApi apiInstance = new AppApi();
String id = "id_example"; // String | App id
RadioStream data = new RadioStream(); // RadioStream | 
try {
    RadioStream result = apiInstance.appPrototypeCreateRadioStreams(id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AppApi#appPrototypeCreateRadioStreams");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| App id |
 **data** | [**RadioStream**](RadioStream.md)|  | [optional]

### Return type

[**RadioStream**](RadioStream.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="appPrototypeCreateRssFeedGroups"></a>
# **appPrototypeCreateRssFeedGroups**
> RssFeedGroup appPrototypeCreateRssFeedGroups(id, data)

Creates a new instance in rssFeedGroups of this model.

### Example
```java
// Import classes:
//import com.harpoon.api.AppApi;

AppApi apiInstance = new AppApi();
String id = "id_example"; // String | App id
RssFeedGroup data = new RssFeedGroup(); // RssFeedGroup | 
try {
    RssFeedGroup result = apiInstance.appPrototypeCreateRssFeedGroups(id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AppApi#appPrototypeCreateRssFeedGroups");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| App id |
 **data** | [**RssFeedGroup**](RssFeedGroup.md)|  | [optional]

### Return type

[**RssFeedGroup**](RssFeedGroup.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="appPrototypeCreateRssFeeds"></a>
# **appPrototypeCreateRssFeeds**
> RssFeed appPrototypeCreateRssFeeds(id, data)

Creates a new instance in rssFeeds of this model.

### Example
```java
// Import classes:
//import com.harpoon.api.AppApi;

AppApi apiInstance = new AppApi();
String id = "id_example"; // String | App id
RssFeed data = new RssFeed(); // RssFeed | 
try {
    RssFeed result = apiInstance.appPrototypeCreateRssFeeds(id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AppApi#appPrototypeCreateRssFeeds");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| App id |
 **data** | [**RssFeed**](RssFeed.md)|  | [optional]

### Return type

[**RssFeed**](RssFeed.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="appPrototypeDeleteAppConfigs"></a>
# **appPrototypeDeleteAppConfigs**
> appPrototypeDeleteAppConfigs(id)

Deletes all appConfigs of this model.

### Example
```java
// Import classes:
//import com.harpoon.api.AppApi;

AppApi apiInstance = new AppApi();
String id = "id_example"; // String | App id
try {
    apiInstance.appPrototypeDeleteAppConfigs(id);
} catch (ApiException e) {
    System.err.println("Exception when calling AppApi#appPrototypeDeleteAppConfigs");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| App id |

### Return type

null (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="appPrototypeDeleteApps"></a>
# **appPrototypeDeleteApps**
> appPrototypeDeleteApps(id)

Deletes all apps of this model.

### Example
```java
// Import classes:
//import com.harpoon.api.AppApi;

AppApi apiInstance = new AppApi();
String id = "id_example"; // String | App id
try {
    apiInstance.appPrototypeDeleteApps(id);
} catch (ApiException e) {
    System.err.println("Exception when calling AppApi#appPrototypeDeleteApps");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| App id |

### Return type

null (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="appPrototypeDeletePlaylists"></a>
# **appPrototypeDeletePlaylists**
> appPrototypeDeletePlaylists(id)

Deletes all playlists of this model.

### Example
```java
// Import classes:
//import com.harpoon.api.AppApi;

AppApi apiInstance = new AppApi();
String id = "id_example"; // String | App id
try {
    apiInstance.appPrototypeDeletePlaylists(id);
} catch (ApiException e) {
    System.err.println("Exception when calling AppApi#appPrototypeDeletePlaylists");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| App id |

### Return type

null (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="appPrototypeDeleteRadioStreams"></a>
# **appPrototypeDeleteRadioStreams**
> appPrototypeDeleteRadioStreams(id)

Deletes all radioStreams of this model.

### Example
```java
// Import classes:
//import com.harpoon.api.AppApi;

AppApi apiInstance = new AppApi();
String id = "id_example"; // String | App id
try {
    apiInstance.appPrototypeDeleteRadioStreams(id);
} catch (ApiException e) {
    System.err.println("Exception when calling AppApi#appPrototypeDeleteRadioStreams");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| App id |

### Return type

null (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="appPrototypeDeleteRssFeedGroups"></a>
# **appPrototypeDeleteRssFeedGroups**
> appPrototypeDeleteRssFeedGroups(id)

Deletes all rssFeedGroups of this model.

### Example
```java
// Import classes:
//import com.harpoon.api.AppApi;

AppApi apiInstance = new AppApi();
String id = "id_example"; // String | App id
try {
    apiInstance.appPrototypeDeleteRssFeedGroups(id);
} catch (ApiException e) {
    System.err.println("Exception when calling AppApi#appPrototypeDeleteRssFeedGroups");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| App id |

### Return type

null (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="appPrototypeDeleteRssFeeds"></a>
# **appPrototypeDeleteRssFeeds**
> appPrototypeDeleteRssFeeds(id)

Deletes all rssFeeds of this model.

### Example
```java
// Import classes:
//import com.harpoon.api.AppApi;

AppApi apiInstance = new AppApi();
String id = "id_example"; // String | App id
try {
    apiInstance.appPrototypeDeleteRssFeeds(id);
} catch (ApiException e) {
    System.err.println("Exception when calling AppApi#appPrototypeDeleteRssFeeds");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| App id |

### Return type

null (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="appPrototypeDestroyByIdAppConfigs"></a>
# **appPrototypeDestroyByIdAppConfigs**
> appPrototypeDestroyByIdAppConfigs(fk, id)

Delete a related item by id for appConfigs.

### Example
```java
// Import classes:
//import com.harpoon.api.AppApi;

AppApi apiInstance = new AppApi();
String fk = "fk_example"; // String | Foreign key for appConfigs
String id = "id_example"; // String | App id
try {
    apiInstance.appPrototypeDestroyByIdAppConfigs(fk, id);
} catch (ApiException e) {
    System.err.println("Exception when calling AppApi#appPrototypeDestroyByIdAppConfigs");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for appConfigs |
 **id** | **String**| App id |

### Return type

null (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="appPrototypeDestroyByIdApps"></a>
# **appPrototypeDestroyByIdApps**
> appPrototypeDestroyByIdApps(fk, id)

Delete a related item by id for apps.

### Example
```java
// Import classes:
//import com.harpoon.api.AppApi;

AppApi apiInstance = new AppApi();
String fk = "fk_example"; // String | Foreign key for apps
String id = "id_example"; // String | App id
try {
    apiInstance.appPrototypeDestroyByIdApps(fk, id);
} catch (ApiException e) {
    System.err.println("Exception when calling AppApi#appPrototypeDestroyByIdApps");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for apps |
 **id** | **String**| App id |

### Return type

null (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="appPrototypeDestroyByIdPlaylists"></a>
# **appPrototypeDestroyByIdPlaylists**
> appPrototypeDestroyByIdPlaylists(fk, id)

Delete a related item by id for playlists.

### Example
```java
// Import classes:
//import com.harpoon.api.AppApi;

AppApi apiInstance = new AppApi();
String fk = "fk_example"; // String | Foreign key for playlists
String id = "id_example"; // String | App id
try {
    apiInstance.appPrototypeDestroyByIdPlaylists(fk, id);
} catch (ApiException e) {
    System.err.println("Exception when calling AppApi#appPrototypeDestroyByIdPlaylists");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for playlists |
 **id** | **String**| App id |

### Return type

null (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="appPrototypeDestroyByIdRadioStreams"></a>
# **appPrototypeDestroyByIdRadioStreams**
> appPrototypeDestroyByIdRadioStreams(fk, id)

Delete a related item by id for radioStreams.

### Example
```java
// Import classes:
//import com.harpoon.api.AppApi;

AppApi apiInstance = new AppApi();
String fk = "fk_example"; // String | Foreign key for radioStreams
String id = "id_example"; // String | App id
try {
    apiInstance.appPrototypeDestroyByIdRadioStreams(fk, id);
} catch (ApiException e) {
    System.err.println("Exception when calling AppApi#appPrototypeDestroyByIdRadioStreams");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for radioStreams |
 **id** | **String**| App id |

### Return type

null (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="appPrototypeDestroyByIdRssFeedGroups"></a>
# **appPrototypeDestroyByIdRssFeedGroups**
> appPrototypeDestroyByIdRssFeedGroups(fk, id)

Delete a related item by id for rssFeedGroups.

### Example
```java
// Import classes:
//import com.harpoon.api.AppApi;

AppApi apiInstance = new AppApi();
String fk = "fk_example"; // String | Foreign key for rssFeedGroups
String id = "id_example"; // String | App id
try {
    apiInstance.appPrototypeDestroyByIdRssFeedGroups(fk, id);
} catch (ApiException e) {
    System.err.println("Exception when calling AppApi#appPrototypeDestroyByIdRssFeedGroups");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for rssFeedGroups |
 **id** | **String**| App id |

### Return type

null (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="appPrototypeDestroyByIdRssFeeds"></a>
# **appPrototypeDestroyByIdRssFeeds**
> appPrototypeDestroyByIdRssFeeds(fk, id)

Delete a related item by id for rssFeeds.

### Example
```java
// Import classes:
//import com.harpoon.api.AppApi;

AppApi apiInstance = new AppApi();
String fk = "fk_example"; // String | Foreign key for rssFeeds
String id = "id_example"; // String | App id
try {
    apiInstance.appPrototypeDestroyByIdRssFeeds(fk, id);
} catch (ApiException e) {
    System.err.println("Exception when calling AppApi#appPrototypeDestroyByIdRssFeeds");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for rssFeeds |
 **id** | **String**| App id |

### Return type

null (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="appPrototypeFindByIdAppConfigs"></a>
# **appPrototypeFindByIdAppConfigs**
> AppConfig appPrototypeFindByIdAppConfigs(fk, id)

Find a related item by id for appConfigs.

### Example
```java
// Import classes:
//import com.harpoon.api.AppApi;

AppApi apiInstance = new AppApi();
String fk = "fk_example"; // String | Foreign key for appConfigs
String id = "id_example"; // String | App id
try {
    AppConfig result = apiInstance.appPrototypeFindByIdAppConfigs(fk, id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AppApi#appPrototypeFindByIdAppConfigs");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for appConfigs |
 **id** | **String**| App id |

### Return type

[**AppConfig**](AppConfig.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="appPrototypeFindByIdApps"></a>
# **appPrototypeFindByIdApps**
> App appPrototypeFindByIdApps(fk, id)

Find a related item by id for apps.

### Example
```java
// Import classes:
//import com.harpoon.api.AppApi;

AppApi apiInstance = new AppApi();
String fk = "fk_example"; // String | Foreign key for apps
String id = "id_example"; // String | App id
try {
    App result = apiInstance.appPrototypeFindByIdApps(fk, id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AppApi#appPrototypeFindByIdApps");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for apps |
 **id** | **String**| App id |

### Return type

[**App**](App.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="appPrototypeFindByIdCustomers"></a>
# **appPrototypeFindByIdCustomers**
> Customer appPrototypeFindByIdCustomers(fk, id)

Find a related item by id for customers.

### Example
```java
// Import classes:
//import com.harpoon.api.AppApi;

AppApi apiInstance = new AppApi();
String fk = "fk_example"; // String | Foreign key for customers
String id = "id_example"; // String | App id
try {
    Customer result = apiInstance.appPrototypeFindByIdCustomers(fk, id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AppApi#appPrototypeFindByIdCustomers");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for customers |
 **id** | **String**| App id |

### Return type

[**Customer**](Customer.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="appPrototypeFindByIdPlaylists"></a>
# **appPrototypeFindByIdPlaylists**
> Playlist appPrototypeFindByIdPlaylists(fk, id)

Find a related item by id for playlists.

### Example
```java
// Import classes:
//import com.harpoon.api.AppApi;

AppApi apiInstance = new AppApi();
String fk = "fk_example"; // String | Foreign key for playlists
String id = "id_example"; // String | App id
try {
    Playlist result = apiInstance.appPrototypeFindByIdPlaylists(fk, id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AppApi#appPrototypeFindByIdPlaylists");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for playlists |
 **id** | **String**| App id |

### Return type

[**Playlist**](Playlist.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="appPrototypeFindByIdRadioStreams"></a>
# **appPrototypeFindByIdRadioStreams**
> RadioStream appPrototypeFindByIdRadioStreams(fk, id)

Find a related item by id for radioStreams.

### Example
```java
// Import classes:
//import com.harpoon.api.AppApi;

AppApi apiInstance = new AppApi();
String fk = "fk_example"; // String | Foreign key for radioStreams
String id = "id_example"; // String | App id
try {
    RadioStream result = apiInstance.appPrototypeFindByIdRadioStreams(fk, id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AppApi#appPrototypeFindByIdRadioStreams");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for radioStreams |
 **id** | **String**| App id |

### Return type

[**RadioStream**](RadioStream.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="appPrototypeFindByIdRssFeedGroups"></a>
# **appPrototypeFindByIdRssFeedGroups**
> RssFeedGroup appPrototypeFindByIdRssFeedGroups(fk, id)

Find a related item by id for rssFeedGroups.

### Example
```java
// Import classes:
//import com.harpoon.api.AppApi;

AppApi apiInstance = new AppApi();
String fk = "fk_example"; // String | Foreign key for rssFeedGroups
String id = "id_example"; // String | App id
try {
    RssFeedGroup result = apiInstance.appPrototypeFindByIdRssFeedGroups(fk, id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AppApi#appPrototypeFindByIdRssFeedGroups");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for rssFeedGroups |
 **id** | **String**| App id |

### Return type

[**RssFeedGroup**](RssFeedGroup.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="appPrototypeFindByIdRssFeeds"></a>
# **appPrototypeFindByIdRssFeeds**
> RssFeed appPrototypeFindByIdRssFeeds(fk, id)

Find a related item by id for rssFeeds.

### Example
```java
// Import classes:
//import com.harpoon.api.AppApi;

AppApi apiInstance = new AppApi();
String fk = "fk_example"; // String | Foreign key for rssFeeds
String id = "id_example"; // String | App id
try {
    RssFeed result = apiInstance.appPrototypeFindByIdRssFeeds(fk, id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AppApi#appPrototypeFindByIdRssFeeds");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for rssFeeds |
 **id** | **String**| App id |

### Return type

[**RssFeed**](RssFeed.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="appPrototypeGetAppConfigs"></a>
# **appPrototypeGetAppConfigs**
> List&lt;AppConfig&gt; appPrototypeGetAppConfigs(id, filter)

Queries appConfigs of App.

### Example
```java
// Import classes:
//import com.harpoon.api.AppApi;

AppApi apiInstance = new AppApi();
String id = "id_example"; // String | App id
String filter = "filter_example"; // String | 
try {
    List<AppConfig> result = apiInstance.appPrototypeGetAppConfigs(id, filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AppApi#appPrototypeGetAppConfigs");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| App id |
 **filter** | **String**|  | [optional]

### Return type

[**List&lt;AppConfig&gt;**](AppConfig.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="appPrototypeGetApps"></a>
# **appPrototypeGetApps**
> List&lt;App&gt; appPrototypeGetApps(id, filter)

Queries apps of App.

### Example
```java
// Import classes:
//import com.harpoon.api.AppApi;

AppApi apiInstance = new AppApi();
String id = "id_example"; // String | App id
String filter = "filter_example"; // String | 
try {
    List<App> result = apiInstance.appPrototypeGetApps(id, filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AppApi#appPrototypeGetApps");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| App id |
 **filter** | **String**|  | [optional]

### Return type

[**List&lt;App&gt;**](App.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="appPrototypeGetCustomers"></a>
# **appPrototypeGetCustomers**
> List&lt;Customer&gt; appPrototypeGetCustomers(id, filter)

Queries customers of App.

### Example
```java
// Import classes:
//import com.harpoon.api.AppApi;

AppApi apiInstance = new AppApi();
String id = "id_example"; // String | App id
String filter = "filter_example"; // String | 
try {
    List<Customer> result = apiInstance.appPrototypeGetCustomers(id, filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AppApi#appPrototypeGetCustomers");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| App id |
 **filter** | **String**|  | [optional]

### Return type

[**List&lt;Customer&gt;**](Customer.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="appPrototypeGetParent"></a>
# **appPrototypeGetParent**
> App appPrototypeGetParent(id, refresh)

Fetches belongsTo relation parent.

### Example
```java
// Import classes:
//import com.harpoon.api.AppApi;

AppApi apiInstance = new AppApi();
String id = "id_example"; // String | App id
Boolean refresh = true; // Boolean | 
try {
    App result = apiInstance.appPrototypeGetParent(id, refresh);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AppApi#appPrototypeGetParent");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| App id |
 **refresh** | **Boolean**|  | [optional]

### Return type

[**App**](App.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="appPrototypeGetPlaylists"></a>
# **appPrototypeGetPlaylists**
> List&lt;Playlist&gt; appPrototypeGetPlaylists(id, filter)

Queries playlists of App.

### Example
```java
// Import classes:
//import com.harpoon.api.AppApi;

AppApi apiInstance = new AppApi();
String id = "id_example"; // String | App id
String filter = "filter_example"; // String | 
try {
    List<Playlist> result = apiInstance.appPrototypeGetPlaylists(id, filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AppApi#appPrototypeGetPlaylists");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| App id |
 **filter** | **String**|  | [optional]

### Return type

[**List&lt;Playlist&gt;**](Playlist.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="appPrototypeGetRadioStreams"></a>
# **appPrototypeGetRadioStreams**
> List&lt;RadioStream&gt; appPrototypeGetRadioStreams(id, filter)

Queries radioStreams of App.

### Example
```java
// Import classes:
//import com.harpoon.api.AppApi;

AppApi apiInstance = new AppApi();
String id = "id_example"; // String | App id
String filter = "filter_example"; // String | 
try {
    List<RadioStream> result = apiInstance.appPrototypeGetRadioStreams(id, filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AppApi#appPrototypeGetRadioStreams");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| App id |
 **filter** | **String**|  | [optional]

### Return type

[**List&lt;RadioStream&gt;**](RadioStream.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="appPrototypeGetRssFeedGroups"></a>
# **appPrototypeGetRssFeedGroups**
> List&lt;RssFeedGroup&gt; appPrototypeGetRssFeedGroups(id, filter)

Queries rssFeedGroups of App.

### Example
```java
// Import classes:
//import com.harpoon.api.AppApi;

AppApi apiInstance = new AppApi();
String id = "id_example"; // String | App id
String filter = "filter_example"; // String | 
try {
    List<RssFeedGroup> result = apiInstance.appPrototypeGetRssFeedGroups(id, filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AppApi#appPrototypeGetRssFeedGroups");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| App id |
 **filter** | **String**|  | [optional]

### Return type

[**List&lt;RssFeedGroup&gt;**](RssFeedGroup.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="appPrototypeGetRssFeeds"></a>
# **appPrototypeGetRssFeeds**
> List&lt;RssFeed&gt; appPrototypeGetRssFeeds(id, filter)

Queries rssFeeds of App.

### Example
```java
// Import classes:
//import com.harpoon.api.AppApi;

AppApi apiInstance = new AppApi();
String id = "id_example"; // String | App id
String filter = "filter_example"; // String | 
try {
    List<RssFeed> result = apiInstance.appPrototypeGetRssFeeds(id, filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AppApi#appPrototypeGetRssFeeds");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| App id |
 **filter** | **String**|  | [optional]

### Return type

[**List&lt;RssFeed&gt;**](RssFeed.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="appPrototypeUpdateAttributesPatchAppsid"></a>
# **appPrototypeUpdateAttributesPatchAppsid**
> App appPrototypeUpdateAttributesPatchAppsid(id, data)

Patch attributes for a model instance and persist it into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.AppApi;

AppApi apiInstance = new AppApi();
String id = "id_example"; // String | App id
App data = new App(); // App | An object of model property name/value pairs
try {
    App result = apiInstance.appPrototypeUpdateAttributesPatchAppsid(id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AppApi#appPrototypeUpdateAttributesPatchAppsid");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| App id |
 **data** | [**App**](App.md)| An object of model property name/value pairs | [optional]

### Return type

[**App**](App.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="appPrototypeUpdateAttributesPutAppsid"></a>
# **appPrototypeUpdateAttributesPutAppsid**
> App appPrototypeUpdateAttributesPutAppsid(id, data)

Patch attributes for a model instance and persist it into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.AppApi;

AppApi apiInstance = new AppApi();
String id = "id_example"; // String | App id
App data = new App(); // App | An object of model property name/value pairs
try {
    App result = apiInstance.appPrototypeUpdateAttributesPutAppsid(id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AppApi#appPrototypeUpdateAttributesPutAppsid");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| App id |
 **data** | [**App**](App.md)| An object of model property name/value pairs | [optional]

### Return type

[**App**](App.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="appPrototypeUpdateByIdAppConfigs"></a>
# **appPrototypeUpdateByIdAppConfigs**
> AppConfig appPrototypeUpdateByIdAppConfigs(fk, id, data)

Update a related item by id for appConfigs.

### Example
```java
// Import classes:
//import com.harpoon.api.AppApi;

AppApi apiInstance = new AppApi();
String fk = "fk_example"; // String | Foreign key for appConfigs
String id = "id_example"; // String | App id
AppConfig data = new AppConfig(); // AppConfig | 
try {
    AppConfig result = apiInstance.appPrototypeUpdateByIdAppConfigs(fk, id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AppApi#appPrototypeUpdateByIdAppConfigs");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for appConfigs |
 **id** | **String**| App id |
 **data** | [**AppConfig**](AppConfig.md)|  | [optional]

### Return type

[**AppConfig**](AppConfig.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="appPrototypeUpdateByIdApps"></a>
# **appPrototypeUpdateByIdApps**
> App appPrototypeUpdateByIdApps(fk, id, data)

Update a related item by id for apps.

### Example
```java
// Import classes:
//import com.harpoon.api.AppApi;

AppApi apiInstance = new AppApi();
String fk = "fk_example"; // String | Foreign key for apps
String id = "id_example"; // String | App id
App data = new App(); // App | 
try {
    App result = apiInstance.appPrototypeUpdateByIdApps(fk, id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AppApi#appPrototypeUpdateByIdApps");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for apps |
 **id** | **String**| App id |
 **data** | [**App**](App.md)|  | [optional]

### Return type

[**App**](App.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="appPrototypeUpdateByIdCustomers"></a>
# **appPrototypeUpdateByIdCustomers**
> Customer appPrototypeUpdateByIdCustomers(fk, id, data)

Update a related item by id for customers.

### Example
```java
// Import classes:
//import com.harpoon.api.AppApi;

AppApi apiInstance = new AppApi();
String fk = "fk_example"; // String | Foreign key for customers
String id = "id_example"; // String | App id
Customer data = new Customer(); // Customer | 
try {
    Customer result = apiInstance.appPrototypeUpdateByIdCustomers(fk, id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AppApi#appPrototypeUpdateByIdCustomers");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for customers |
 **id** | **String**| App id |
 **data** | [**Customer**](Customer.md)|  | [optional]

### Return type

[**Customer**](Customer.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="appPrototypeUpdateByIdPlaylists"></a>
# **appPrototypeUpdateByIdPlaylists**
> Playlist appPrototypeUpdateByIdPlaylists(fk, id, data)

Update a related item by id for playlists.

### Example
```java
// Import classes:
//import com.harpoon.api.AppApi;

AppApi apiInstance = new AppApi();
String fk = "fk_example"; // String | Foreign key for playlists
String id = "id_example"; // String | App id
Playlist data = new Playlist(); // Playlist | 
try {
    Playlist result = apiInstance.appPrototypeUpdateByIdPlaylists(fk, id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AppApi#appPrototypeUpdateByIdPlaylists");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for playlists |
 **id** | **String**| App id |
 **data** | [**Playlist**](Playlist.md)|  | [optional]

### Return type

[**Playlist**](Playlist.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="appPrototypeUpdateByIdRadioStreams"></a>
# **appPrototypeUpdateByIdRadioStreams**
> RadioStream appPrototypeUpdateByIdRadioStreams(fk, id, data)

Update a related item by id for radioStreams.

### Example
```java
// Import classes:
//import com.harpoon.api.AppApi;

AppApi apiInstance = new AppApi();
String fk = "fk_example"; // String | Foreign key for radioStreams
String id = "id_example"; // String | App id
RadioStream data = new RadioStream(); // RadioStream | 
try {
    RadioStream result = apiInstance.appPrototypeUpdateByIdRadioStreams(fk, id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AppApi#appPrototypeUpdateByIdRadioStreams");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for radioStreams |
 **id** | **String**| App id |
 **data** | [**RadioStream**](RadioStream.md)|  | [optional]

### Return type

[**RadioStream**](RadioStream.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="appPrototypeUpdateByIdRssFeedGroups"></a>
# **appPrototypeUpdateByIdRssFeedGroups**
> RssFeedGroup appPrototypeUpdateByIdRssFeedGroups(fk, id, data)

Update a related item by id for rssFeedGroups.

### Example
```java
// Import classes:
//import com.harpoon.api.AppApi;

AppApi apiInstance = new AppApi();
String fk = "fk_example"; // String | Foreign key for rssFeedGroups
String id = "id_example"; // String | App id
RssFeedGroup data = new RssFeedGroup(); // RssFeedGroup | 
try {
    RssFeedGroup result = apiInstance.appPrototypeUpdateByIdRssFeedGroups(fk, id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AppApi#appPrototypeUpdateByIdRssFeedGroups");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for rssFeedGroups |
 **id** | **String**| App id |
 **data** | [**RssFeedGroup**](RssFeedGroup.md)|  | [optional]

### Return type

[**RssFeedGroup**](RssFeedGroup.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="appPrototypeUpdateByIdRssFeeds"></a>
# **appPrototypeUpdateByIdRssFeeds**
> RssFeed appPrototypeUpdateByIdRssFeeds(fk, id, data)

Update a related item by id for rssFeeds.

### Example
```java
// Import classes:
//import com.harpoon.api.AppApi;

AppApi apiInstance = new AppApi();
String fk = "fk_example"; // String | Foreign key for rssFeeds
String id = "id_example"; // String | App id
RssFeed data = new RssFeed(); // RssFeed | 
try {
    RssFeed result = apiInstance.appPrototypeUpdateByIdRssFeeds(fk, id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AppApi#appPrototypeUpdateByIdRssFeeds");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for rssFeeds |
 **id** | **String**| App id |
 **data** | [**RssFeed**](RssFeed.md)|  | [optional]

### Return type

[**RssFeed**](RssFeed.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="appReplaceById"></a>
# **appReplaceById**
> App appReplaceById(id, data)

Replace attributes for a model instance and persist it into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.AppApi;

AppApi apiInstance = new AppApi();
String id = "id_example"; // String | Model id
App data = new App(); // App | Model instance data
try {
    App result = apiInstance.appReplaceById(id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AppApi#appReplaceById");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |
 **data** | [**App**](App.md)| Model instance data | [optional]

### Return type

[**App**](App.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="appReplaceOrCreate"></a>
# **appReplaceOrCreate**
> App appReplaceOrCreate(data)

Replace an existing model instance or insert a new one into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.AppApi;

AppApi apiInstance = new AppApi();
App data = new App(); // App | Model instance data
try {
    App result = apiInstance.appReplaceOrCreate(data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AppApi#appReplaceOrCreate");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**App**](App.md)| Model instance data | [optional]

### Return type

[**App**](App.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="appUpdateAll"></a>
# **appUpdateAll**
> InlineResponse2002 appUpdateAll(where, data)

Update instances of the model matched by {{where}} from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.AppApi;

AppApi apiInstance = new AppApi();
String where = "where_example"; // String | Criteria to match model instances
App data = new App(); // App | An object of model property name/value pairs
try {
    InlineResponse2002 result = apiInstance.appUpdateAll(where, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AppApi#appUpdateAll");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional]
 **data** | [**App**](App.md)| An object of model property name/value pairs | [optional]

### Return type

[**InlineResponse2002**](InlineResponse2002.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="appUploadFile"></a>
# **appUploadFile**
> MagentoFileUpload appUploadFile(id, type, data)



### Example
```java
// Import classes:
//import com.harpoon.api.AppApi;

AppApi apiInstance = new AppApi();
String id = "id_example"; // String | Application Id
String type = "type_example"; // String | Corresponds to the type of file being uploaded (androidGoogleServices, androidKey, androidKeystore, androidApk, iOSGoogleServices, iOSPushNotificationPrivateKey, iOSPushNotificationCertificate, iOSPushNotificationPem, imgAppLogo, imgSplashScreen, imgMenuBackground, imgBrandPlaceholder, imgNavbarLogo,imgSponsorMenu, imgSponsorSplash, imgMenuItem, imgRadioStream, imgRadioShow, imgRadioPresenter).
MagentoFileUpload data = new MagentoFileUpload(); // MagentoFileUpload | Corresponds to the file you're uploading formatted as an object.
try {
    MagentoFileUpload result = apiInstance.appUploadFile(id, type, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AppApi#appUploadFile");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Application Id |
 **type** | **String**| Corresponds to the type of file being uploaded (androidGoogleServices, androidKey, androidKeystore, androidApk, iOSGoogleServices, iOSPushNotificationPrivateKey, iOSPushNotificationCertificate, iOSPushNotificationPem, imgAppLogo, imgSplashScreen, imgMenuBackground, imgBrandPlaceholder, imgNavbarLogo,imgSponsorMenu, imgSponsorSplash, imgMenuItem, imgRadioStream, imgRadioShow, imgRadioPresenter). |
 **data** | [**MagentoFileUpload**](MagentoFileUpload.md)| Corresponds to the file you&#39;re uploading formatted as an object. | [optional]

### Return type

[**MagentoFileUpload**](MagentoFileUpload.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="appUpsertPatchApps"></a>
# **appUpsertPatchApps**
> App appUpsertPatchApps(data)

Patch an existing model instance or insert a new one into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.AppApi;

AppApi apiInstance = new AppApi();
App data = new App(); // App | Model instance data
try {
    App result = apiInstance.appUpsertPatchApps(data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AppApi#appUpsertPatchApps");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**App**](App.md)| Model instance data | [optional]

### Return type

[**App**](App.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="appUpsertPutApps"></a>
# **appUpsertPutApps**
> App appUpsertPutApps(data)

Patch an existing model instance or insert a new one into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.AppApi;

AppApi apiInstance = new AppApi();
App data = new App(); // App | Model instance data
try {
    App result = apiInstance.appUpsertPutApps(data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AppApi#appUpsertPutApps");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**App**](App.md)| Model instance data | [optional]

### Return type

[**App**](App.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="appUpsertWithWhere"></a>
# **appUpsertWithWhere**
> App appUpsertWithWhere(where, data)

Update an existing model instance or insert a new one into the data source based on the where criteria.

### Example
```java
// Import classes:
//import com.harpoon.api.AppApi;

AppApi apiInstance = new AppApi();
String where = "where_example"; // String | Criteria to match model instances
App data = new App(); // App | An object of model property name/value pairs
try {
    App result = apiInstance.appUpsertWithWhere(where, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AppApi#appUpsertWithWhere");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional]
 **data** | [**App**](App.md)| An object of model property name/value pairs | [optional]

### Return type

[**App**](App.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="appVerify"></a>
# **appVerify**
> Object appVerify()



### Example
```java
// Import classes:
//import com.harpoon.api.AppApi;

AppApi apiInstance = new AppApi();
try {
    Object result = apiInstance.appVerify();
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AppApi#appVerify");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

**Object**

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

