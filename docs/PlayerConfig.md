
# PlayerConfig

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**mute** | **Boolean** | Available on the following SDKs: JS, iOS |  [optional]
**autostart** | **Boolean** | Available on the following SDKs: JS, iOS, Android |  [optional]
**repeat** | **Boolean** | Available on the following SDKs: JS, iOS, Android |  [optional]
**controls** | **Boolean** | Available on the following SDKs: JS, iOS, Android |  [optional]
**visualPlaylist** | **Boolean** | Available on the following SDKs: JS (as visualplaylist) |  [optional]
**displayTitle** | **Boolean** | Available on the following SDKs: JS (as displaytitle) |  [optional]
**displayDescription** | **Boolean** | Available on the following SDKs: JS (as displaydescription) |  [optional]
**stretching** | **String** | Available on the following SDKs: JS, iOS, Android (as stretch) |  [optional]
**hlshtml** | **Boolean** | Available on the following SDKs: JS |  [optional]
**primary** | **String** | Available on the following SDKs: JS |  [optional]
**flashPlayer** | **String** | Available on the following SDKs: JS (as flashplayer) |  [optional]
**baseSkinPath** | **String** | Available on the following SDKs: JS (as base) |  [optional]
**preload** | **String** | Available on the following SDKs: JS |  [optional]
**playerAdConfig** | **Object** | Available on the following SDKs: JS, iOS, Android |  [optional]
**playerCaptionConfig** | **Object** | Available on the following SDKs: JS, iOS, Android |  [optional]
**skinName** | **String** | Available on the following SDKs: JS (as skin.name), iOS, Android (as premiumSkin) |  [optional]
**skinCss** | **String** | Available on the following SDKs: JS (as skin.url), iOS (as skinUrl), Android (as cssSkin) |  [optional]
**skinActive** | **String** | Value must be a valid HEX color. Available on the following SDKs: JS (as skin.active), iOS |  [optional]
**skinInactive** | **String** | Value must be a valid HEX color. Available on the following SDKs: JS (as skin.inactive), iOS |  [optional]
**skinBackground** | **String** | Value must be a valid HEX color. Available on the following SDKs: JS (as skin.background), iOS |  [optional]
**id** | **Double** |  |  [optional]



