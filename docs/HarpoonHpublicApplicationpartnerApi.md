# HarpoonHpublicApplicationpartnerApi

All URIs are relative to *https://apicdn.harpoonconnect.com/v1.1.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**harpoonHpublicApplicationpartnerCount**](HarpoonHpublicApplicationpartnerApi.md#harpoonHpublicApplicationpartnerCount) | **GET** /HarpoonHpublicApplicationpartners/count | Count instances of the model matched by where from the data source.
[**harpoonHpublicApplicationpartnerCreate**](HarpoonHpublicApplicationpartnerApi.md#harpoonHpublicApplicationpartnerCreate) | **POST** /HarpoonHpublicApplicationpartners | Create a new instance of the model and persist it into the data source.
[**harpoonHpublicApplicationpartnerCreateChangeStreamGetHarpoonHpublicApplicationpartnersChangeStream**](HarpoonHpublicApplicationpartnerApi.md#harpoonHpublicApplicationpartnerCreateChangeStreamGetHarpoonHpublicApplicationpartnersChangeStream) | **GET** /HarpoonHpublicApplicationpartners/change-stream | Create a change stream.
[**harpoonHpublicApplicationpartnerCreateChangeStreamPostHarpoonHpublicApplicationpartnersChangeStream**](HarpoonHpublicApplicationpartnerApi.md#harpoonHpublicApplicationpartnerCreateChangeStreamPostHarpoonHpublicApplicationpartnersChangeStream) | **POST** /HarpoonHpublicApplicationpartners/change-stream | Create a change stream.
[**harpoonHpublicApplicationpartnerDeleteById**](HarpoonHpublicApplicationpartnerApi.md#harpoonHpublicApplicationpartnerDeleteById) | **DELETE** /HarpoonHpublicApplicationpartners/{id} | Delete a model instance by {{id}} from the data source.
[**harpoonHpublicApplicationpartnerExistsGetHarpoonHpublicApplicationpartnersidExists**](HarpoonHpublicApplicationpartnerApi.md#harpoonHpublicApplicationpartnerExistsGetHarpoonHpublicApplicationpartnersidExists) | **GET** /HarpoonHpublicApplicationpartners/{id}/exists | Check whether a model instance exists in the data source.
[**harpoonHpublicApplicationpartnerExistsHeadHarpoonHpublicApplicationpartnersid**](HarpoonHpublicApplicationpartnerApi.md#harpoonHpublicApplicationpartnerExistsHeadHarpoonHpublicApplicationpartnersid) | **HEAD** /HarpoonHpublicApplicationpartners/{id} | Check whether a model instance exists in the data source.
[**harpoonHpublicApplicationpartnerFind**](HarpoonHpublicApplicationpartnerApi.md#harpoonHpublicApplicationpartnerFind) | **GET** /HarpoonHpublicApplicationpartners | Find all instances of the model matched by filter from the data source.
[**harpoonHpublicApplicationpartnerFindById**](HarpoonHpublicApplicationpartnerApi.md#harpoonHpublicApplicationpartnerFindById) | **GET** /HarpoonHpublicApplicationpartners/{id} | Find a model instance by {{id}} from the data source.
[**harpoonHpublicApplicationpartnerFindOne**](HarpoonHpublicApplicationpartnerApi.md#harpoonHpublicApplicationpartnerFindOne) | **GET** /HarpoonHpublicApplicationpartners/findOne | Find first instance of the model matched by filter from the data source.
[**harpoonHpublicApplicationpartnerPrototypeGetUdropshipVendor**](HarpoonHpublicApplicationpartnerApi.md#harpoonHpublicApplicationpartnerPrototypeGetUdropshipVendor) | **GET** /HarpoonHpublicApplicationpartners/{id}/udropshipVendor | Fetches belongsTo relation udropshipVendor.
[**harpoonHpublicApplicationpartnerPrototypeUpdateAttributesPatchHarpoonHpublicApplicationpartnersid**](HarpoonHpublicApplicationpartnerApi.md#harpoonHpublicApplicationpartnerPrototypeUpdateAttributesPatchHarpoonHpublicApplicationpartnersid) | **PATCH** /HarpoonHpublicApplicationpartners/{id} | Patch attributes for a model instance and persist it into the data source.
[**harpoonHpublicApplicationpartnerPrototypeUpdateAttributesPutHarpoonHpublicApplicationpartnersid**](HarpoonHpublicApplicationpartnerApi.md#harpoonHpublicApplicationpartnerPrototypeUpdateAttributesPutHarpoonHpublicApplicationpartnersid) | **PUT** /HarpoonHpublicApplicationpartners/{id} | Patch attributes for a model instance and persist it into the data source.
[**harpoonHpublicApplicationpartnerReplaceById**](HarpoonHpublicApplicationpartnerApi.md#harpoonHpublicApplicationpartnerReplaceById) | **POST** /HarpoonHpublicApplicationpartners/{id}/replace | Replace attributes for a model instance and persist it into the data source.
[**harpoonHpublicApplicationpartnerReplaceOrCreate**](HarpoonHpublicApplicationpartnerApi.md#harpoonHpublicApplicationpartnerReplaceOrCreate) | **POST** /HarpoonHpublicApplicationpartners/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
[**harpoonHpublicApplicationpartnerUpdateAll**](HarpoonHpublicApplicationpartnerApi.md#harpoonHpublicApplicationpartnerUpdateAll) | **POST** /HarpoonHpublicApplicationpartners/update | Update instances of the model matched by {{where}} from the data source.
[**harpoonHpublicApplicationpartnerUpsertPatchHarpoonHpublicApplicationpartners**](HarpoonHpublicApplicationpartnerApi.md#harpoonHpublicApplicationpartnerUpsertPatchHarpoonHpublicApplicationpartners) | **PATCH** /HarpoonHpublicApplicationpartners | Patch an existing model instance or insert a new one into the data source.
[**harpoonHpublicApplicationpartnerUpsertPutHarpoonHpublicApplicationpartners**](HarpoonHpublicApplicationpartnerApi.md#harpoonHpublicApplicationpartnerUpsertPutHarpoonHpublicApplicationpartners) | **PUT** /HarpoonHpublicApplicationpartners | Patch an existing model instance or insert a new one into the data source.
[**harpoonHpublicApplicationpartnerUpsertWithWhere**](HarpoonHpublicApplicationpartnerApi.md#harpoonHpublicApplicationpartnerUpsertWithWhere) | **POST** /HarpoonHpublicApplicationpartners/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.


<a name="harpoonHpublicApplicationpartnerCount"></a>
# **harpoonHpublicApplicationpartnerCount**
> InlineResponse2001 harpoonHpublicApplicationpartnerCount(where)

Count instances of the model matched by where from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.HarpoonHpublicApplicationpartnerApi;

HarpoonHpublicApplicationpartnerApi apiInstance = new HarpoonHpublicApplicationpartnerApi();
String where = "where_example"; // String | Criteria to match model instances
try {
    InlineResponse2001 result = apiInstance.harpoonHpublicApplicationpartnerCount(where);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling HarpoonHpublicApplicationpartnerApi#harpoonHpublicApplicationpartnerCount");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional]

### Return type

[**InlineResponse2001**](InlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="harpoonHpublicApplicationpartnerCreate"></a>
# **harpoonHpublicApplicationpartnerCreate**
> HarpoonHpublicApplicationpartner harpoonHpublicApplicationpartnerCreate(data)

Create a new instance of the model and persist it into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.HarpoonHpublicApplicationpartnerApi;

HarpoonHpublicApplicationpartnerApi apiInstance = new HarpoonHpublicApplicationpartnerApi();
HarpoonHpublicApplicationpartner data = new HarpoonHpublicApplicationpartner(); // HarpoonHpublicApplicationpartner | Model instance data
try {
    HarpoonHpublicApplicationpartner result = apiInstance.harpoonHpublicApplicationpartnerCreate(data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling HarpoonHpublicApplicationpartnerApi#harpoonHpublicApplicationpartnerCreate");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**HarpoonHpublicApplicationpartner**](HarpoonHpublicApplicationpartner.md)| Model instance data | [optional]

### Return type

[**HarpoonHpublicApplicationpartner**](HarpoonHpublicApplicationpartner.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="harpoonHpublicApplicationpartnerCreateChangeStreamGetHarpoonHpublicApplicationpartnersChangeStream"></a>
# **harpoonHpublicApplicationpartnerCreateChangeStreamGetHarpoonHpublicApplicationpartnersChangeStream**
> File harpoonHpublicApplicationpartnerCreateChangeStreamGetHarpoonHpublicApplicationpartnersChangeStream(options)

Create a change stream.

### Example
```java
// Import classes:
//import com.harpoon.api.HarpoonHpublicApplicationpartnerApi;

HarpoonHpublicApplicationpartnerApi apiInstance = new HarpoonHpublicApplicationpartnerApi();
String options = "options_example"; // String | 
try {
    File result = apiInstance.harpoonHpublicApplicationpartnerCreateChangeStreamGetHarpoonHpublicApplicationpartnersChangeStream(options);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling HarpoonHpublicApplicationpartnerApi#harpoonHpublicApplicationpartnerCreateChangeStreamGetHarpoonHpublicApplicationpartnersChangeStream");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **String**|  | [optional]

### Return type

[**File**](File.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="harpoonHpublicApplicationpartnerCreateChangeStreamPostHarpoonHpublicApplicationpartnersChangeStream"></a>
# **harpoonHpublicApplicationpartnerCreateChangeStreamPostHarpoonHpublicApplicationpartnersChangeStream**
> File harpoonHpublicApplicationpartnerCreateChangeStreamPostHarpoonHpublicApplicationpartnersChangeStream(options)

Create a change stream.

### Example
```java
// Import classes:
//import com.harpoon.api.HarpoonHpublicApplicationpartnerApi;

HarpoonHpublicApplicationpartnerApi apiInstance = new HarpoonHpublicApplicationpartnerApi();
String options = "options_example"; // String | 
try {
    File result = apiInstance.harpoonHpublicApplicationpartnerCreateChangeStreamPostHarpoonHpublicApplicationpartnersChangeStream(options);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling HarpoonHpublicApplicationpartnerApi#harpoonHpublicApplicationpartnerCreateChangeStreamPostHarpoonHpublicApplicationpartnersChangeStream");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **String**|  | [optional]

### Return type

[**File**](File.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="harpoonHpublicApplicationpartnerDeleteById"></a>
# **harpoonHpublicApplicationpartnerDeleteById**
> Object harpoonHpublicApplicationpartnerDeleteById(id)

Delete a model instance by {{id}} from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.HarpoonHpublicApplicationpartnerApi;

HarpoonHpublicApplicationpartnerApi apiInstance = new HarpoonHpublicApplicationpartnerApi();
String id = "id_example"; // String | Model id
try {
    Object result = apiInstance.harpoonHpublicApplicationpartnerDeleteById(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling HarpoonHpublicApplicationpartnerApi#harpoonHpublicApplicationpartnerDeleteById");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |

### Return type

**Object**

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="harpoonHpublicApplicationpartnerExistsGetHarpoonHpublicApplicationpartnersidExists"></a>
# **harpoonHpublicApplicationpartnerExistsGetHarpoonHpublicApplicationpartnersidExists**
> InlineResponse2003 harpoonHpublicApplicationpartnerExistsGetHarpoonHpublicApplicationpartnersidExists(id)

Check whether a model instance exists in the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.HarpoonHpublicApplicationpartnerApi;

HarpoonHpublicApplicationpartnerApi apiInstance = new HarpoonHpublicApplicationpartnerApi();
String id = "id_example"; // String | Model id
try {
    InlineResponse2003 result = apiInstance.harpoonHpublicApplicationpartnerExistsGetHarpoonHpublicApplicationpartnersidExists(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling HarpoonHpublicApplicationpartnerApi#harpoonHpublicApplicationpartnerExistsGetHarpoonHpublicApplicationpartnersidExists");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |

### Return type

[**InlineResponse2003**](InlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="harpoonHpublicApplicationpartnerExistsHeadHarpoonHpublicApplicationpartnersid"></a>
# **harpoonHpublicApplicationpartnerExistsHeadHarpoonHpublicApplicationpartnersid**
> InlineResponse2003 harpoonHpublicApplicationpartnerExistsHeadHarpoonHpublicApplicationpartnersid(id)

Check whether a model instance exists in the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.HarpoonHpublicApplicationpartnerApi;

HarpoonHpublicApplicationpartnerApi apiInstance = new HarpoonHpublicApplicationpartnerApi();
String id = "id_example"; // String | Model id
try {
    InlineResponse2003 result = apiInstance.harpoonHpublicApplicationpartnerExistsHeadHarpoonHpublicApplicationpartnersid(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling HarpoonHpublicApplicationpartnerApi#harpoonHpublicApplicationpartnerExistsHeadHarpoonHpublicApplicationpartnersid");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |

### Return type

[**InlineResponse2003**](InlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="harpoonHpublicApplicationpartnerFind"></a>
# **harpoonHpublicApplicationpartnerFind**
> List&lt;HarpoonHpublicApplicationpartner&gt; harpoonHpublicApplicationpartnerFind(filter)

Find all instances of the model matched by filter from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.HarpoonHpublicApplicationpartnerApi;

HarpoonHpublicApplicationpartnerApi apiInstance = new HarpoonHpublicApplicationpartnerApi();
String filter = "filter_example"; // String | Filter defining fields, where, include, order, offset, and limit
try {
    List<HarpoonHpublicApplicationpartner> result = apiInstance.harpoonHpublicApplicationpartnerFind(filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling HarpoonHpublicApplicationpartnerApi#harpoonHpublicApplicationpartnerFind");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **String**| Filter defining fields, where, include, order, offset, and limit | [optional]

### Return type

[**List&lt;HarpoonHpublicApplicationpartner&gt;**](HarpoonHpublicApplicationpartner.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="harpoonHpublicApplicationpartnerFindById"></a>
# **harpoonHpublicApplicationpartnerFindById**
> HarpoonHpublicApplicationpartner harpoonHpublicApplicationpartnerFindById(id, filter)

Find a model instance by {{id}} from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.HarpoonHpublicApplicationpartnerApi;

HarpoonHpublicApplicationpartnerApi apiInstance = new HarpoonHpublicApplicationpartnerApi();
String id = "id_example"; // String | Model id
String filter = "filter_example"; // String | Filter defining fields and include
try {
    HarpoonHpublicApplicationpartner result = apiInstance.harpoonHpublicApplicationpartnerFindById(id, filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling HarpoonHpublicApplicationpartnerApi#harpoonHpublicApplicationpartnerFindById");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |
 **filter** | **String**| Filter defining fields and include | [optional]

### Return type

[**HarpoonHpublicApplicationpartner**](HarpoonHpublicApplicationpartner.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="harpoonHpublicApplicationpartnerFindOne"></a>
# **harpoonHpublicApplicationpartnerFindOne**
> HarpoonHpublicApplicationpartner harpoonHpublicApplicationpartnerFindOne(filter)

Find first instance of the model matched by filter from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.HarpoonHpublicApplicationpartnerApi;

HarpoonHpublicApplicationpartnerApi apiInstance = new HarpoonHpublicApplicationpartnerApi();
String filter = "filter_example"; // String | Filter defining fields, where, include, order, offset, and limit
try {
    HarpoonHpublicApplicationpartner result = apiInstance.harpoonHpublicApplicationpartnerFindOne(filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling HarpoonHpublicApplicationpartnerApi#harpoonHpublicApplicationpartnerFindOne");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **String**| Filter defining fields, where, include, order, offset, and limit | [optional]

### Return type

[**HarpoonHpublicApplicationpartner**](HarpoonHpublicApplicationpartner.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="harpoonHpublicApplicationpartnerPrototypeGetUdropshipVendor"></a>
# **harpoonHpublicApplicationpartnerPrototypeGetUdropshipVendor**
> UdropshipVendor harpoonHpublicApplicationpartnerPrototypeGetUdropshipVendor(id, refresh)

Fetches belongsTo relation udropshipVendor.

### Example
```java
// Import classes:
//import com.harpoon.api.HarpoonHpublicApplicationpartnerApi;

HarpoonHpublicApplicationpartnerApi apiInstance = new HarpoonHpublicApplicationpartnerApi();
String id = "id_example"; // String | HarpoonHpublicApplicationpartner id
Boolean refresh = true; // Boolean | 
try {
    UdropshipVendor result = apiInstance.harpoonHpublicApplicationpartnerPrototypeGetUdropshipVendor(id, refresh);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling HarpoonHpublicApplicationpartnerApi#harpoonHpublicApplicationpartnerPrototypeGetUdropshipVendor");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| HarpoonHpublicApplicationpartner id |
 **refresh** | **Boolean**|  | [optional]

### Return type

[**UdropshipVendor**](UdropshipVendor.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="harpoonHpublicApplicationpartnerPrototypeUpdateAttributesPatchHarpoonHpublicApplicationpartnersid"></a>
# **harpoonHpublicApplicationpartnerPrototypeUpdateAttributesPatchHarpoonHpublicApplicationpartnersid**
> HarpoonHpublicApplicationpartner harpoonHpublicApplicationpartnerPrototypeUpdateAttributesPatchHarpoonHpublicApplicationpartnersid(id, data)

Patch attributes for a model instance and persist it into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.HarpoonHpublicApplicationpartnerApi;

HarpoonHpublicApplicationpartnerApi apiInstance = new HarpoonHpublicApplicationpartnerApi();
String id = "id_example"; // String | HarpoonHpublicApplicationpartner id
HarpoonHpublicApplicationpartner data = new HarpoonHpublicApplicationpartner(); // HarpoonHpublicApplicationpartner | An object of model property name/value pairs
try {
    HarpoonHpublicApplicationpartner result = apiInstance.harpoonHpublicApplicationpartnerPrototypeUpdateAttributesPatchHarpoonHpublicApplicationpartnersid(id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling HarpoonHpublicApplicationpartnerApi#harpoonHpublicApplicationpartnerPrototypeUpdateAttributesPatchHarpoonHpublicApplicationpartnersid");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| HarpoonHpublicApplicationpartner id |
 **data** | [**HarpoonHpublicApplicationpartner**](HarpoonHpublicApplicationpartner.md)| An object of model property name/value pairs | [optional]

### Return type

[**HarpoonHpublicApplicationpartner**](HarpoonHpublicApplicationpartner.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="harpoonHpublicApplicationpartnerPrototypeUpdateAttributesPutHarpoonHpublicApplicationpartnersid"></a>
# **harpoonHpublicApplicationpartnerPrototypeUpdateAttributesPutHarpoonHpublicApplicationpartnersid**
> HarpoonHpublicApplicationpartner harpoonHpublicApplicationpartnerPrototypeUpdateAttributesPutHarpoonHpublicApplicationpartnersid(id, data)

Patch attributes for a model instance and persist it into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.HarpoonHpublicApplicationpartnerApi;

HarpoonHpublicApplicationpartnerApi apiInstance = new HarpoonHpublicApplicationpartnerApi();
String id = "id_example"; // String | HarpoonHpublicApplicationpartner id
HarpoonHpublicApplicationpartner data = new HarpoonHpublicApplicationpartner(); // HarpoonHpublicApplicationpartner | An object of model property name/value pairs
try {
    HarpoonHpublicApplicationpartner result = apiInstance.harpoonHpublicApplicationpartnerPrototypeUpdateAttributesPutHarpoonHpublicApplicationpartnersid(id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling HarpoonHpublicApplicationpartnerApi#harpoonHpublicApplicationpartnerPrototypeUpdateAttributesPutHarpoonHpublicApplicationpartnersid");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| HarpoonHpublicApplicationpartner id |
 **data** | [**HarpoonHpublicApplicationpartner**](HarpoonHpublicApplicationpartner.md)| An object of model property name/value pairs | [optional]

### Return type

[**HarpoonHpublicApplicationpartner**](HarpoonHpublicApplicationpartner.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="harpoonHpublicApplicationpartnerReplaceById"></a>
# **harpoonHpublicApplicationpartnerReplaceById**
> HarpoonHpublicApplicationpartner harpoonHpublicApplicationpartnerReplaceById(id, data)

Replace attributes for a model instance and persist it into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.HarpoonHpublicApplicationpartnerApi;

HarpoonHpublicApplicationpartnerApi apiInstance = new HarpoonHpublicApplicationpartnerApi();
String id = "id_example"; // String | Model id
HarpoonHpublicApplicationpartner data = new HarpoonHpublicApplicationpartner(); // HarpoonHpublicApplicationpartner | Model instance data
try {
    HarpoonHpublicApplicationpartner result = apiInstance.harpoonHpublicApplicationpartnerReplaceById(id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling HarpoonHpublicApplicationpartnerApi#harpoonHpublicApplicationpartnerReplaceById");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |
 **data** | [**HarpoonHpublicApplicationpartner**](HarpoonHpublicApplicationpartner.md)| Model instance data | [optional]

### Return type

[**HarpoonHpublicApplicationpartner**](HarpoonHpublicApplicationpartner.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="harpoonHpublicApplicationpartnerReplaceOrCreate"></a>
# **harpoonHpublicApplicationpartnerReplaceOrCreate**
> HarpoonHpublicApplicationpartner harpoonHpublicApplicationpartnerReplaceOrCreate(data)

Replace an existing model instance or insert a new one into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.HarpoonHpublicApplicationpartnerApi;

HarpoonHpublicApplicationpartnerApi apiInstance = new HarpoonHpublicApplicationpartnerApi();
HarpoonHpublicApplicationpartner data = new HarpoonHpublicApplicationpartner(); // HarpoonHpublicApplicationpartner | Model instance data
try {
    HarpoonHpublicApplicationpartner result = apiInstance.harpoonHpublicApplicationpartnerReplaceOrCreate(data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling HarpoonHpublicApplicationpartnerApi#harpoonHpublicApplicationpartnerReplaceOrCreate");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**HarpoonHpublicApplicationpartner**](HarpoonHpublicApplicationpartner.md)| Model instance data | [optional]

### Return type

[**HarpoonHpublicApplicationpartner**](HarpoonHpublicApplicationpartner.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="harpoonHpublicApplicationpartnerUpdateAll"></a>
# **harpoonHpublicApplicationpartnerUpdateAll**
> InlineResponse2002 harpoonHpublicApplicationpartnerUpdateAll(where, data)

Update instances of the model matched by {{where}} from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.HarpoonHpublicApplicationpartnerApi;

HarpoonHpublicApplicationpartnerApi apiInstance = new HarpoonHpublicApplicationpartnerApi();
String where = "where_example"; // String | Criteria to match model instances
HarpoonHpublicApplicationpartner data = new HarpoonHpublicApplicationpartner(); // HarpoonHpublicApplicationpartner | An object of model property name/value pairs
try {
    InlineResponse2002 result = apiInstance.harpoonHpublicApplicationpartnerUpdateAll(where, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling HarpoonHpublicApplicationpartnerApi#harpoonHpublicApplicationpartnerUpdateAll");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional]
 **data** | [**HarpoonHpublicApplicationpartner**](HarpoonHpublicApplicationpartner.md)| An object of model property name/value pairs | [optional]

### Return type

[**InlineResponse2002**](InlineResponse2002.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="harpoonHpublicApplicationpartnerUpsertPatchHarpoonHpublicApplicationpartners"></a>
# **harpoonHpublicApplicationpartnerUpsertPatchHarpoonHpublicApplicationpartners**
> HarpoonHpublicApplicationpartner harpoonHpublicApplicationpartnerUpsertPatchHarpoonHpublicApplicationpartners(data)

Patch an existing model instance or insert a new one into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.HarpoonHpublicApplicationpartnerApi;

HarpoonHpublicApplicationpartnerApi apiInstance = new HarpoonHpublicApplicationpartnerApi();
HarpoonHpublicApplicationpartner data = new HarpoonHpublicApplicationpartner(); // HarpoonHpublicApplicationpartner | Model instance data
try {
    HarpoonHpublicApplicationpartner result = apiInstance.harpoonHpublicApplicationpartnerUpsertPatchHarpoonHpublicApplicationpartners(data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling HarpoonHpublicApplicationpartnerApi#harpoonHpublicApplicationpartnerUpsertPatchHarpoonHpublicApplicationpartners");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**HarpoonHpublicApplicationpartner**](HarpoonHpublicApplicationpartner.md)| Model instance data | [optional]

### Return type

[**HarpoonHpublicApplicationpartner**](HarpoonHpublicApplicationpartner.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="harpoonHpublicApplicationpartnerUpsertPutHarpoonHpublicApplicationpartners"></a>
# **harpoonHpublicApplicationpartnerUpsertPutHarpoonHpublicApplicationpartners**
> HarpoonHpublicApplicationpartner harpoonHpublicApplicationpartnerUpsertPutHarpoonHpublicApplicationpartners(data)

Patch an existing model instance or insert a new one into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.HarpoonHpublicApplicationpartnerApi;

HarpoonHpublicApplicationpartnerApi apiInstance = new HarpoonHpublicApplicationpartnerApi();
HarpoonHpublicApplicationpartner data = new HarpoonHpublicApplicationpartner(); // HarpoonHpublicApplicationpartner | Model instance data
try {
    HarpoonHpublicApplicationpartner result = apiInstance.harpoonHpublicApplicationpartnerUpsertPutHarpoonHpublicApplicationpartners(data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling HarpoonHpublicApplicationpartnerApi#harpoonHpublicApplicationpartnerUpsertPutHarpoonHpublicApplicationpartners");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**HarpoonHpublicApplicationpartner**](HarpoonHpublicApplicationpartner.md)| Model instance data | [optional]

### Return type

[**HarpoonHpublicApplicationpartner**](HarpoonHpublicApplicationpartner.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="harpoonHpublicApplicationpartnerUpsertWithWhere"></a>
# **harpoonHpublicApplicationpartnerUpsertWithWhere**
> HarpoonHpublicApplicationpartner harpoonHpublicApplicationpartnerUpsertWithWhere(where, data)

Update an existing model instance or insert a new one into the data source based on the where criteria.

### Example
```java
// Import classes:
//import com.harpoon.api.HarpoonHpublicApplicationpartnerApi;

HarpoonHpublicApplicationpartnerApi apiInstance = new HarpoonHpublicApplicationpartnerApi();
String where = "where_example"; // String | Criteria to match model instances
HarpoonHpublicApplicationpartner data = new HarpoonHpublicApplicationpartner(); // HarpoonHpublicApplicationpartner | An object of model property name/value pairs
try {
    HarpoonHpublicApplicationpartner result = apiInstance.harpoonHpublicApplicationpartnerUpsertWithWhere(where, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling HarpoonHpublicApplicationpartnerApi#harpoonHpublicApplicationpartnerUpsertWithWhere");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional]
 **data** | [**HarpoonHpublicApplicationpartner**](HarpoonHpublicApplicationpartner.md)| An object of model property name/value pairs | [optional]

### Return type

[**HarpoonHpublicApplicationpartner**](HarpoonHpublicApplicationpartner.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

