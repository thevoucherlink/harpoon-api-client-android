# RadioPresenterApi

All URIs are relative to *https://apicdn.harpoonconnect.com/v1.1.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**radioPresenterCount**](RadioPresenterApi.md#radioPresenterCount) | **GET** /RadioPresenters/count | Count instances of the model matched by where from the data source.
[**radioPresenterCreate**](RadioPresenterApi.md#radioPresenterCreate) | **POST** /RadioPresenters | Create a new instance of the model and persist it into the data source.
[**radioPresenterCreateChangeStreamGetRadioPresentersChangeStream**](RadioPresenterApi.md#radioPresenterCreateChangeStreamGetRadioPresentersChangeStream) | **GET** /RadioPresenters/change-stream | Create a change stream.
[**radioPresenterCreateChangeStreamPostRadioPresentersChangeStream**](RadioPresenterApi.md#radioPresenterCreateChangeStreamPostRadioPresentersChangeStream) | **POST** /RadioPresenters/change-stream | Create a change stream.
[**radioPresenterDeleteById**](RadioPresenterApi.md#radioPresenterDeleteById) | **DELETE** /RadioPresenters/{id} | Delete a model instance by {{id}} from the data source.
[**radioPresenterExistsGetRadioPresentersidExists**](RadioPresenterApi.md#radioPresenterExistsGetRadioPresentersidExists) | **GET** /RadioPresenters/{id}/exists | Check whether a model instance exists in the data source.
[**radioPresenterExistsHeadRadioPresentersid**](RadioPresenterApi.md#radioPresenterExistsHeadRadioPresentersid) | **HEAD** /RadioPresenters/{id} | Check whether a model instance exists in the data source.
[**radioPresenterFind**](RadioPresenterApi.md#radioPresenterFind) | **GET** /RadioPresenters | Find all instances of the model matched by filter from the data source.
[**radioPresenterFindById**](RadioPresenterApi.md#radioPresenterFindById) | **GET** /RadioPresenters/{id} | Find a model instance by {{id}} from the data source.
[**radioPresenterFindOne**](RadioPresenterApi.md#radioPresenterFindOne) | **GET** /RadioPresenters/findOne | Find first instance of the model matched by filter from the data source.
[**radioPresenterPrototypeCountRadioShows**](RadioPresenterApi.md#radioPresenterPrototypeCountRadioShows) | **GET** /RadioPresenters/{id}/radioShows/count | Counts radioShows of RadioPresenter.
[**radioPresenterPrototypeCreateRadioShows**](RadioPresenterApi.md#radioPresenterPrototypeCreateRadioShows) | **POST** /RadioPresenters/{id}/radioShows | Creates a new instance in radioShows of this model.
[**radioPresenterPrototypeDeleteRadioShows**](RadioPresenterApi.md#radioPresenterPrototypeDeleteRadioShows) | **DELETE** /RadioPresenters/{id}/radioShows | Deletes all radioShows of this model.
[**radioPresenterPrototypeDestroyByIdRadioShows**](RadioPresenterApi.md#radioPresenterPrototypeDestroyByIdRadioShows) | **DELETE** /RadioPresenters/{id}/radioShows/{fk} | Delete a related item by id for radioShows.
[**radioPresenterPrototypeExistsRadioShows**](RadioPresenterApi.md#radioPresenterPrototypeExistsRadioShows) | **HEAD** /RadioPresenters/{id}/radioShows/rel/{fk} | Check the existence of radioShows relation to an item by id.
[**radioPresenterPrototypeFindByIdRadioShows**](RadioPresenterApi.md#radioPresenterPrototypeFindByIdRadioShows) | **GET** /RadioPresenters/{id}/radioShows/{fk} | Find a related item by id for radioShows.
[**radioPresenterPrototypeGetRadioShows**](RadioPresenterApi.md#radioPresenterPrototypeGetRadioShows) | **GET** /RadioPresenters/{id}/radioShows | Queries radioShows of RadioPresenter.
[**radioPresenterPrototypeLinkRadioShows**](RadioPresenterApi.md#radioPresenterPrototypeLinkRadioShows) | **PUT** /RadioPresenters/{id}/radioShows/rel/{fk} | Add a related item by id for radioShows.
[**radioPresenterPrototypeUnlinkRadioShows**](RadioPresenterApi.md#radioPresenterPrototypeUnlinkRadioShows) | **DELETE** /RadioPresenters/{id}/radioShows/rel/{fk} | Remove the radioShows relation to an item by id.
[**radioPresenterPrototypeUpdateAttributesPatchRadioPresentersid**](RadioPresenterApi.md#radioPresenterPrototypeUpdateAttributesPatchRadioPresentersid) | **PATCH** /RadioPresenters/{id} | Patch attributes for a model instance and persist it into the data source.
[**radioPresenterPrototypeUpdateAttributesPutRadioPresentersid**](RadioPresenterApi.md#radioPresenterPrototypeUpdateAttributesPutRadioPresentersid) | **PUT** /RadioPresenters/{id} | Patch attributes for a model instance and persist it into the data source.
[**radioPresenterPrototypeUpdateByIdRadioShows**](RadioPresenterApi.md#radioPresenterPrototypeUpdateByIdRadioShows) | **PUT** /RadioPresenters/{id}/radioShows/{fk} | Update a related item by id for radioShows.
[**radioPresenterReplaceById**](RadioPresenterApi.md#radioPresenterReplaceById) | **POST** /RadioPresenters/{id}/replace | Replace attributes for a model instance and persist it into the data source.
[**radioPresenterReplaceOrCreate**](RadioPresenterApi.md#radioPresenterReplaceOrCreate) | **POST** /RadioPresenters/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
[**radioPresenterUpdateAll**](RadioPresenterApi.md#radioPresenterUpdateAll) | **POST** /RadioPresenters/update | Update instances of the model matched by {{where}} from the data source.
[**radioPresenterUploadImage**](RadioPresenterApi.md#radioPresenterUploadImage) | **POST** /RadioPresenters/{id}/file/image | 
[**radioPresenterUpsertPatchRadioPresenters**](RadioPresenterApi.md#radioPresenterUpsertPatchRadioPresenters) | **PATCH** /RadioPresenters | Patch an existing model instance or insert a new one into the data source.
[**radioPresenterUpsertPutRadioPresenters**](RadioPresenterApi.md#radioPresenterUpsertPutRadioPresenters) | **PUT** /RadioPresenters | Patch an existing model instance or insert a new one into the data source.
[**radioPresenterUpsertWithWhere**](RadioPresenterApi.md#radioPresenterUpsertWithWhere) | **POST** /RadioPresenters/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.


<a name="radioPresenterCount"></a>
# **radioPresenterCount**
> InlineResponse2001 radioPresenterCount(where)

Count instances of the model matched by where from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.RadioPresenterApi;

RadioPresenterApi apiInstance = new RadioPresenterApi();
String where = "where_example"; // String | Criteria to match model instances
try {
    InlineResponse2001 result = apiInstance.radioPresenterCount(where);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RadioPresenterApi#radioPresenterCount");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional]

### Return type

[**InlineResponse2001**](InlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioPresenterCreate"></a>
# **radioPresenterCreate**
> RadioPresenter radioPresenterCreate(data)

Create a new instance of the model and persist it into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.RadioPresenterApi;

RadioPresenterApi apiInstance = new RadioPresenterApi();
RadioPresenter data = new RadioPresenter(); // RadioPresenter | Model instance data
try {
    RadioPresenter result = apiInstance.radioPresenterCreate(data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RadioPresenterApi#radioPresenterCreate");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**RadioPresenter**](RadioPresenter.md)| Model instance data | [optional]

### Return type

[**RadioPresenter**](RadioPresenter.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioPresenterCreateChangeStreamGetRadioPresentersChangeStream"></a>
# **radioPresenterCreateChangeStreamGetRadioPresentersChangeStream**
> File radioPresenterCreateChangeStreamGetRadioPresentersChangeStream(options)

Create a change stream.

### Example
```java
// Import classes:
//import com.harpoon.api.RadioPresenterApi;

RadioPresenterApi apiInstance = new RadioPresenterApi();
String options = "options_example"; // String | 
try {
    File result = apiInstance.radioPresenterCreateChangeStreamGetRadioPresentersChangeStream(options);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RadioPresenterApi#radioPresenterCreateChangeStreamGetRadioPresentersChangeStream");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **String**|  | [optional]

### Return type

[**File**](File.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioPresenterCreateChangeStreamPostRadioPresentersChangeStream"></a>
# **radioPresenterCreateChangeStreamPostRadioPresentersChangeStream**
> File radioPresenterCreateChangeStreamPostRadioPresentersChangeStream(options)

Create a change stream.

### Example
```java
// Import classes:
//import com.harpoon.api.RadioPresenterApi;

RadioPresenterApi apiInstance = new RadioPresenterApi();
String options = "options_example"; // String | 
try {
    File result = apiInstance.radioPresenterCreateChangeStreamPostRadioPresentersChangeStream(options);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RadioPresenterApi#radioPresenterCreateChangeStreamPostRadioPresentersChangeStream");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **String**|  | [optional]

### Return type

[**File**](File.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioPresenterDeleteById"></a>
# **radioPresenterDeleteById**
> Object radioPresenterDeleteById(id)

Delete a model instance by {{id}} from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.RadioPresenterApi;

RadioPresenterApi apiInstance = new RadioPresenterApi();
String id = "id_example"; // String | Model id
try {
    Object result = apiInstance.radioPresenterDeleteById(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RadioPresenterApi#radioPresenterDeleteById");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |

### Return type

**Object**

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioPresenterExistsGetRadioPresentersidExists"></a>
# **radioPresenterExistsGetRadioPresentersidExists**
> InlineResponse2003 radioPresenterExistsGetRadioPresentersidExists(id)

Check whether a model instance exists in the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.RadioPresenterApi;

RadioPresenterApi apiInstance = new RadioPresenterApi();
String id = "id_example"; // String | Model id
try {
    InlineResponse2003 result = apiInstance.radioPresenterExistsGetRadioPresentersidExists(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RadioPresenterApi#radioPresenterExistsGetRadioPresentersidExists");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |

### Return type

[**InlineResponse2003**](InlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioPresenterExistsHeadRadioPresentersid"></a>
# **radioPresenterExistsHeadRadioPresentersid**
> InlineResponse2003 radioPresenterExistsHeadRadioPresentersid(id)

Check whether a model instance exists in the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.RadioPresenterApi;

RadioPresenterApi apiInstance = new RadioPresenterApi();
String id = "id_example"; // String | Model id
try {
    InlineResponse2003 result = apiInstance.radioPresenterExistsHeadRadioPresentersid(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RadioPresenterApi#radioPresenterExistsHeadRadioPresentersid");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |

### Return type

[**InlineResponse2003**](InlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioPresenterFind"></a>
# **radioPresenterFind**
> List&lt;RadioPresenter&gt; radioPresenterFind(filter)

Find all instances of the model matched by filter from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.RadioPresenterApi;

RadioPresenterApi apiInstance = new RadioPresenterApi();
String filter = "filter_example"; // String | Filter defining fields, where, include, order, offset, and limit
try {
    List<RadioPresenter> result = apiInstance.radioPresenterFind(filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RadioPresenterApi#radioPresenterFind");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **String**| Filter defining fields, where, include, order, offset, and limit | [optional]

### Return type

[**List&lt;RadioPresenter&gt;**](RadioPresenter.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioPresenterFindById"></a>
# **radioPresenterFindById**
> RadioPresenter radioPresenterFindById(id, filter)

Find a model instance by {{id}} from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.RadioPresenterApi;

RadioPresenterApi apiInstance = new RadioPresenterApi();
String id = "id_example"; // String | Model id
String filter = "filter_example"; // String | Filter defining fields and include
try {
    RadioPresenter result = apiInstance.radioPresenterFindById(id, filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RadioPresenterApi#radioPresenterFindById");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |
 **filter** | **String**| Filter defining fields and include | [optional]

### Return type

[**RadioPresenter**](RadioPresenter.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioPresenterFindOne"></a>
# **radioPresenterFindOne**
> RadioPresenter radioPresenterFindOne(filter)

Find first instance of the model matched by filter from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.RadioPresenterApi;

RadioPresenterApi apiInstance = new RadioPresenterApi();
String filter = "filter_example"; // String | Filter defining fields, where, include, order, offset, and limit
try {
    RadioPresenter result = apiInstance.radioPresenterFindOne(filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RadioPresenterApi#radioPresenterFindOne");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **String**| Filter defining fields, where, include, order, offset, and limit | [optional]

### Return type

[**RadioPresenter**](RadioPresenter.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioPresenterPrototypeCountRadioShows"></a>
# **radioPresenterPrototypeCountRadioShows**
> InlineResponse2001 radioPresenterPrototypeCountRadioShows(id, where)

Counts radioShows of RadioPresenter.

### Example
```java
// Import classes:
//import com.harpoon.api.RadioPresenterApi;

RadioPresenterApi apiInstance = new RadioPresenterApi();
String id = "id_example"; // String | RadioPresenter id
String where = "where_example"; // String | Criteria to match model instances
try {
    InlineResponse2001 result = apiInstance.radioPresenterPrototypeCountRadioShows(id, where);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RadioPresenterApi#radioPresenterPrototypeCountRadioShows");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| RadioPresenter id |
 **where** | **String**| Criteria to match model instances | [optional]

### Return type

[**InlineResponse2001**](InlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioPresenterPrototypeCreateRadioShows"></a>
# **radioPresenterPrototypeCreateRadioShows**
> RadioShow radioPresenterPrototypeCreateRadioShows(id, data)

Creates a new instance in radioShows of this model.

### Example
```java
// Import classes:
//import com.harpoon.api.RadioPresenterApi;

RadioPresenterApi apiInstance = new RadioPresenterApi();
String id = "id_example"; // String | RadioPresenter id
RadioShow data = new RadioShow(); // RadioShow | 
try {
    RadioShow result = apiInstance.radioPresenterPrototypeCreateRadioShows(id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RadioPresenterApi#radioPresenterPrototypeCreateRadioShows");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| RadioPresenter id |
 **data** | [**RadioShow**](RadioShow.md)|  | [optional]

### Return type

[**RadioShow**](RadioShow.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioPresenterPrototypeDeleteRadioShows"></a>
# **radioPresenterPrototypeDeleteRadioShows**
> radioPresenterPrototypeDeleteRadioShows(id)

Deletes all radioShows of this model.

### Example
```java
// Import classes:
//import com.harpoon.api.RadioPresenterApi;

RadioPresenterApi apiInstance = new RadioPresenterApi();
String id = "id_example"; // String | RadioPresenter id
try {
    apiInstance.radioPresenterPrototypeDeleteRadioShows(id);
} catch (ApiException e) {
    System.err.println("Exception when calling RadioPresenterApi#radioPresenterPrototypeDeleteRadioShows");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| RadioPresenter id |

### Return type

null (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioPresenterPrototypeDestroyByIdRadioShows"></a>
# **radioPresenterPrototypeDestroyByIdRadioShows**
> radioPresenterPrototypeDestroyByIdRadioShows(fk, id)

Delete a related item by id for radioShows.

### Example
```java
// Import classes:
//import com.harpoon.api.RadioPresenterApi;

RadioPresenterApi apiInstance = new RadioPresenterApi();
String fk = "fk_example"; // String | Foreign key for radioShows
String id = "id_example"; // String | RadioPresenter id
try {
    apiInstance.radioPresenterPrototypeDestroyByIdRadioShows(fk, id);
} catch (ApiException e) {
    System.err.println("Exception when calling RadioPresenterApi#radioPresenterPrototypeDestroyByIdRadioShows");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for radioShows |
 **id** | **String**| RadioPresenter id |

### Return type

null (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioPresenterPrototypeExistsRadioShows"></a>
# **radioPresenterPrototypeExistsRadioShows**
> Boolean radioPresenterPrototypeExistsRadioShows(fk, id)

Check the existence of radioShows relation to an item by id.

### Example
```java
// Import classes:
//import com.harpoon.api.RadioPresenterApi;

RadioPresenterApi apiInstance = new RadioPresenterApi();
String fk = "fk_example"; // String | Foreign key for radioShows
String id = "id_example"; // String | RadioPresenter id
try {
    Boolean result = apiInstance.radioPresenterPrototypeExistsRadioShows(fk, id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RadioPresenterApi#radioPresenterPrototypeExistsRadioShows");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for radioShows |
 **id** | **String**| RadioPresenter id |

### Return type

**Boolean**

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioPresenterPrototypeFindByIdRadioShows"></a>
# **radioPresenterPrototypeFindByIdRadioShows**
> RadioShow radioPresenterPrototypeFindByIdRadioShows(fk, id)

Find a related item by id for radioShows.

### Example
```java
// Import classes:
//import com.harpoon.api.RadioPresenterApi;

RadioPresenterApi apiInstance = new RadioPresenterApi();
String fk = "fk_example"; // String | Foreign key for radioShows
String id = "id_example"; // String | RadioPresenter id
try {
    RadioShow result = apiInstance.radioPresenterPrototypeFindByIdRadioShows(fk, id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RadioPresenterApi#radioPresenterPrototypeFindByIdRadioShows");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for radioShows |
 **id** | **String**| RadioPresenter id |

### Return type

[**RadioShow**](RadioShow.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioPresenterPrototypeGetRadioShows"></a>
# **radioPresenterPrototypeGetRadioShows**
> List&lt;RadioShow&gt; radioPresenterPrototypeGetRadioShows(id, filter)

Queries radioShows of RadioPresenter.

### Example
```java
// Import classes:
//import com.harpoon.api.RadioPresenterApi;

RadioPresenterApi apiInstance = new RadioPresenterApi();
String id = "id_example"; // String | RadioPresenter id
String filter = "filter_example"; // String | 
try {
    List<RadioShow> result = apiInstance.radioPresenterPrototypeGetRadioShows(id, filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RadioPresenterApi#radioPresenterPrototypeGetRadioShows");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| RadioPresenter id |
 **filter** | **String**|  | [optional]

### Return type

[**List&lt;RadioShow&gt;**](RadioShow.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioPresenterPrototypeLinkRadioShows"></a>
# **radioPresenterPrototypeLinkRadioShows**
> RadioPresenterRadioShow radioPresenterPrototypeLinkRadioShows(fk, id, data)

Add a related item by id for radioShows.

### Example
```java
// Import classes:
//import com.harpoon.api.RadioPresenterApi;

RadioPresenterApi apiInstance = new RadioPresenterApi();
String fk = "fk_example"; // String | Foreign key for radioShows
String id = "id_example"; // String | RadioPresenter id
RadioPresenterRadioShow data = new RadioPresenterRadioShow(); // RadioPresenterRadioShow | 
try {
    RadioPresenterRadioShow result = apiInstance.radioPresenterPrototypeLinkRadioShows(fk, id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RadioPresenterApi#radioPresenterPrototypeLinkRadioShows");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for radioShows |
 **id** | **String**| RadioPresenter id |
 **data** | [**RadioPresenterRadioShow**](RadioPresenterRadioShow.md)|  | [optional]

### Return type

[**RadioPresenterRadioShow**](RadioPresenterRadioShow.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioPresenterPrototypeUnlinkRadioShows"></a>
# **radioPresenterPrototypeUnlinkRadioShows**
> radioPresenterPrototypeUnlinkRadioShows(fk, id)

Remove the radioShows relation to an item by id.

### Example
```java
// Import classes:
//import com.harpoon.api.RadioPresenterApi;

RadioPresenterApi apiInstance = new RadioPresenterApi();
String fk = "fk_example"; // String | Foreign key for radioShows
String id = "id_example"; // String | RadioPresenter id
try {
    apiInstance.radioPresenterPrototypeUnlinkRadioShows(fk, id);
} catch (ApiException e) {
    System.err.println("Exception when calling RadioPresenterApi#radioPresenterPrototypeUnlinkRadioShows");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for radioShows |
 **id** | **String**| RadioPresenter id |

### Return type

null (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioPresenterPrototypeUpdateAttributesPatchRadioPresentersid"></a>
# **radioPresenterPrototypeUpdateAttributesPatchRadioPresentersid**
> RadioPresenter radioPresenterPrototypeUpdateAttributesPatchRadioPresentersid(id, data)

Patch attributes for a model instance and persist it into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.RadioPresenterApi;

RadioPresenterApi apiInstance = new RadioPresenterApi();
String id = "id_example"; // String | RadioPresenter id
RadioPresenter data = new RadioPresenter(); // RadioPresenter | An object of model property name/value pairs
try {
    RadioPresenter result = apiInstance.radioPresenterPrototypeUpdateAttributesPatchRadioPresentersid(id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RadioPresenterApi#radioPresenterPrototypeUpdateAttributesPatchRadioPresentersid");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| RadioPresenter id |
 **data** | [**RadioPresenter**](RadioPresenter.md)| An object of model property name/value pairs | [optional]

### Return type

[**RadioPresenter**](RadioPresenter.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioPresenterPrototypeUpdateAttributesPutRadioPresentersid"></a>
# **radioPresenterPrototypeUpdateAttributesPutRadioPresentersid**
> RadioPresenter radioPresenterPrototypeUpdateAttributesPutRadioPresentersid(id, data)

Patch attributes for a model instance and persist it into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.RadioPresenterApi;

RadioPresenterApi apiInstance = new RadioPresenterApi();
String id = "id_example"; // String | RadioPresenter id
RadioPresenter data = new RadioPresenter(); // RadioPresenter | An object of model property name/value pairs
try {
    RadioPresenter result = apiInstance.radioPresenterPrototypeUpdateAttributesPutRadioPresentersid(id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RadioPresenterApi#radioPresenterPrototypeUpdateAttributesPutRadioPresentersid");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| RadioPresenter id |
 **data** | [**RadioPresenter**](RadioPresenter.md)| An object of model property name/value pairs | [optional]

### Return type

[**RadioPresenter**](RadioPresenter.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioPresenterPrototypeUpdateByIdRadioShows"></a>
# **radioPresenterPrototypeUpdateByIdRadioShows**
> RadioShow radioPresenterPrototypeUpdateByIdRadioShows(fk, id, data)

Update a related item by id for radioShows.

### Example
```java
// Import classes:
//import com.harpoon.api.RadioPresenterApi;

RadioPresenterApi apiInstance = new RadioPresenterApi();
String fk = "fk_example"; // String | Foreign key for radioShows
String id = "id_example"; // String | RadioPresenter id
RadioShow data = new RadioShow(); // RadioShow | 
try {
    RadioShow result = apiInstance.radioPresenterPrototypeUpdateByIdRadioShows(fk, id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RadioPresenterApi#radioPresenterPrototypeUpdateByIdRadioShows");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for radioShows |
 **id** | **String**| RadioPresenter id |
 **data** | [**RadioShow**](RadioShow.md)|  | [optional]

### Return type

[**RadioShow**](RadioShow.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioPresenterReplaceById"></a>
# **radioPresenterReplaceById**
> RadioPresenter radioPresenterReplaceById(id, data)

Replace attributes for a model instance and persist it into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.RadioPresenterApi;

RadioPresenterApi apiInstance = new RadioPresenterApi();
String id = "id_example"; // String | Model id
RadioPresenter data = new RadioPresenter(); // RadioPresenter | Model instance data
try {
    RadioPresenter result = apiInstance.radioPresenterReplaceById(id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RadioPresenterApi#radioPresenterReplaceById");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |
 **data** | [**RadioPresenter**](RadioPresenter.md)| Model instance data | [optional]

### Return type

[**RadioPresenter**](RadioPresenter.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioPresenterReplaceOrCreate"></a>
# **radioPresenterReplaceOrCreate**
> RadioPresenter radioPresenterReplaceOrCreate(data)

Replace an existing model instance or insert a new one into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.RadioPresenterApi;

RadioPresenterApi apiInstance = new RadioPresenterApi();
RadioPresenter data = new RadioPresenter(); // RadioPresenter | Model instance data
try {
    RadioPresenter result = apiInstance.radioPresenterReplaceOrCreate(data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RadioPresenterApi#radioPresenterReplaceOrCreate");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**RadioPresenter**](RadioPresenter.md)| Model instance data | [optional]

### Return type

[**RadioPresenter**](RadioPresenter.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioPresenterUpdateAll"></a>
# **radioPresenterUpdateAll**
> InlineResponse2002 radioPresenterUpdateAll(where, data)

Update instances of the model matched by {{where}} from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.RadioPresenterApi;

RadioPresenterApi apiInstance = new RadioPresenterApi();
String where = "where_example"; // String | Criteria to match model instances
RadioPresenter data = new RadioPresenter(); // RadioPresenter | An object of model property name/value pairs
try {
    InlineResponse2002 result = apiInstance.radioPresenterUpdateAll(where, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RadioPresenterApi#radioPresenterUpdateAll");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional]
 **data** | [**RadioPresenter**](RadioPresenter.md)| An object of model property name/value pairs | [optional]

### Return type

[**InlineResponse2002**](InlineResponse2002.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioPresenterUploadImage"></a>
# **radioPresenterUploadImage**
> MagentoFileUpload radioPresenterUploadImage(id, data)



### Example
```java
// Import classes:
//import com.harpoon.api.RadioPresenterApi;

RadioPresenterApi apiInstance = new RadioPresenterApi();
String id = "id_example"; // String | Radio Presenter Id
MagentoFileUpload data = new MagentoFileUpload(); // MagentoFileUpload | Corresponds to the file you're uploading formatted as an object.
try {
    MagentoFileUpload result = apiInstance.radioPresenterUploadImage(id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RadioPresenterApi#radioPresenterUploadImage");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Radio Presenter Id |
 **data** | [**MagentoFileUpload**](MagentoFileUpload.md)| Corresponds to the file you&#39;re uploading formatted as an object. | [optional]

### Return type

[**MagentoFileUpload**](MagentoFileUpload.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioPresenterUpsertPatchRadioPresenters"></a>
# **radioPresenterUpsertPatchRadioPresenters**
> RadioPresenter radioPresenterUpsertPatchRadioPresenters(data)

Patch an existing model instance or insert a new one into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.RadioPresenterApi;

RadioPresenterApi apiInstance = new RadioPresenterApi();
RadioPresenter data = new RadioPresenter(); // RadioPresenter | Model instance data
try {
    RadioPresenter result = apiInstance.radioPresenterUpsertPatchRadioPresenters(data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RadioPresenterApi#radioPresenterUpsertPatchRadioPresenters");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**RadioPresenter**](RadioPresenter.md)| Model instance data | [optional]

### Return type

[**RadioPresenter**](RadioPresenter.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioPresenterUpsertPutRadioPresenters"></a>
# **radioPresenterUpsertPutRadioPresenters**
> RadioPresenter radioPresenterUpsertPutRadioPresenters(data)

Patch an existing model instance or insert a new one into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.RadioPresenterApi;

RadioPresenterApi apiInstance = new RadioPresenterApi();
RadioPresenter data = new RadioPresenter(); // RadioPresenter | Model instance data
try {
    RadioPresenter result = apiInstance.radioPresenterUpsertPutRadioPresenters(data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RadioPresenterApi#radioPresenterUpsertPutRadioPresenters");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**RadioPresenter**](RadioPresenter.md)| Model instance data | [optional]

### Return type

[**RadioPresenter**](RadioPresenter.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioPresenterUpsertWithWhere"></a>
# **radioPresenterUpsertWithWhere**
> RadioPresenter radioPresenterUpsertWithWhere(where, data)

Update an existing model instance or insert a new one into the data source based on the where criteria.

### Example
```java
// Import classes:
//import com.harpoon.api.RadioPresenterApi;

RadioPresenterApi apiInstance = new RadioPresenterApi();
String where = "where_example"; // String | Criteria to match model instances
RadioPresenter data = new RadioPresenter(); // RadioPresenter | An object of model property name/value pairs
try {
    RadioPresenter result = apiInstance.radioPresenterUpsertWithWhere(where, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RadioPresenterApi#radioPresenterUpsertWithWhere");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional]
 **data** | [**RadioPresenter**](RadioPresenter.md)| An object of model property name/value pairs | [optional]

### Return type

[**RadioPresenter**](RadioPresenter.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

