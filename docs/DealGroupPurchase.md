
# DealGroupPurchase

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**orderId** | **Double** |  |  [optional]
**name** | **String** |  |  [optional]
**code** | [**CouponPurchaseCode**](CouponPurchaseCode.md) |  |  [optional]
**isAvailable** | **Boolean** |  |  [optional]
**createdAt** | [**Date**](Date.md) |  |  [optional]
**expiredAt** | [**Date**](Date.md) |  |  [optional]
**redeemedAt** | [**Date**](Date.md) |  |  [optional]
**redeemedByTerminal** | **String** |  |  [optional]
**redemptionType** | **String** |  |  [optional]
**status** | **String** |  |  [optional]
**unlockTime** | **Double** |  |  [optional]
**id** | **Double** |  |  [optional]



