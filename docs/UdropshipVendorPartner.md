
# UdropshipVendorPartner

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**status** | **String** |  | 
**invitedEmail** | **String** |  | 
**invitedAt** | [**Date**](Date.md) |  | 
**acceptedAt** | [**Date**](Date.md) |  | 
**createdAt** | [**Date**](Date.md) |  | 
**updatedAt** | [**Date**](Date.md) |  | 
**configList** | **Double** |  | 
**configFeed** | **Double** |  | 
**configNeedFollow** | **Double** |  | 
**configAcceptEvent** | **Double** |  | 
**configAcceptCoupon** | **Double** |  | 
**configAcceptDealsimple** | **Double** |  | 
**configAcceptDealgroup** | **Double** |  | 
**configAcceptNotificationpush** | **Double** |  | 
**configAcceptNotificationbeacon** | **Double** |  | 
**isOwner** | **Double** |  | 
**vendorId** | **Double** |  | 
**partnerId** | **Double** |  | 
**id** | **Double** |  | 
**udropshipVendor** | **Object** |  |  [optional]



