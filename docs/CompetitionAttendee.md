
# CompetitionAttendee

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**joinedAt** | [**Date**](Date.md) |  |  [optional]
**email** | **String** |  | 
**firstName** | **String** |  | 
**lastName** | **String** |  | 
**dob** | **String** |  |  [optional]
**gender** | **String** |  |  [optional]
**password** | **String** |  | 
**profilePicture** | **String** |  |  [optional]
**cover** | **String** |  |  [optional]
**profilePictureUpload** | [**MagentoImageUpload**](MagentoImageUpload.md) |  |  [optional]
**coverUpload** | [**MagentoImageUpload**](MagentoImageUpload.md) |  |  [optional]
**metadata** | **Object** |  |  [optional]
**connection** | [**CustomerConnection**](CustomerConnection.md) |  |  [optional]
**followingCount** | **Double** |  |  [optional]
**followerCount** | **Double** |  |  [optional]
**isFollowed** | **Boolean** |  |  [optional]
**isFollower** | **Boolean** |  |  [optional]
**notificationCount** | **Double** |  |  [optional]
**authorizationCode** | **String** |  |  [optional]
**id** | **Double** |  |  [optional]



