
# StripeInvoice

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**object** | **String** |  |  [optional]
**amountDue** | **Double** |  |  [optional]
**applicationFee** | **Double** |  |  [optional]
**attemptCount** | **Double** |  |  [optional]
**attempted** | **Boolean** |  |  [optional]
**charge** | **String** |  |  [optional]
**closed** | **Boolean** |  |  [optional]
**currency** | **String** |  |  [optional]
**date** | **Double** |  |  [optional]
**description** | **String** |  |  [optional]
**endingBalance** | **Double** |  |  [optional]
**forgiven** | **Boolean** |  |  [optional]
**livemode** | **Boolean** |  |  [optional]
**metadata** | **String** |  |  [optional]
**nextPaymentAttempt** | **Double** |  |  [optional]
**paid** | **Boolean** |  |  [optional]
**periodEnd** | **Double** |  |  [optional]
**periodStart** | **Double** |  |  [optional]
**receiptNumber** | **String** |  |  [optional]
**subscriptionProrationDate** | **Double** |  |  [optional]
**subtotal** | **Double** |  |  [optional]
**tax** | **Double** |  |  [optional]
**taxPercent** | **Double** |  |  [optional]
**total** | **Double** |  |  [optional]
**webhooksDeliveredAt** | **Double** |  |  [optional]
**customer** | **String** |  | 
**discount** | [**StripeDiscount**](StripeDiscount.md) |  |  [optional]
**statmentDescriptor** | **String** |  |  [optional]
**subscription** | [**StripeSubscription**](StripeSubscription.md) |  |  [optional]
**lines** | [**List&lt;StripeInvoiceItem&gt;**](StripeInvoiceItem.md) |  |  [optional]
**id** | **Double** |  |  [optional]



