# CouponApi

All URIs are relative to *https://apicdn.harpoonconnect.com/v1.1.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**couponCheckout**](CouponApi.md#couponCheckout) | **POST** /Coupons/{id}/checkout | 
[**couponFind**](CouponApi.md#couponFind) | **GET** /Coupons | Find all instances of the model matched by filter from the data source.
[**couponFindById**](CouponApi.md#couponFindById) | **GET** /Coupons/{id} | Find a model instance by {{id}} from the data source.
[**couponFindOne**](CouponApi.md#couponFindOne) | **GET** /Coupons/findOne | Find first instance of the model matched by filter from the data source.
[**couponRedeem**](CouponApi.md#couponRedeem) | **POST** /Coupons/{id}/redeem | 
[**couponReplaceById**](CouponApi.md#couponReplaceById) | **POST** /Coupons/{id}/replace | Replace attributes for a model instance and persist it into the data source.
[**couponReplaceOrCreate**](CouponApi.md#couponReplaceOrCreate) | **POST** /Coupons/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
[**couponUpsertWithWhere**](CouponApi.md#couponUpsertWithWhere) | **POST** /Coupons/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.
[**couponVenues**](CouponApi.md#couponVenues) | **GET** /Coupons/{id}/venues | 


<a name="couponCheckout"></a>
# **couponCheckout**
> CouponCheckout couponCheckout(id, data)



### Example
```java
// Import classes:
//import com.harpoon.api.CouponApi;

CouponApi apiInstance = new CouponApi();
String id = "id_example"; // String | Model id
CouponCheckoutData data = new CouponCheckoutData(); // CouponCheckoutData | 
try {
    CouponCheckout result = apiInstance.couponCheckout(id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CouponApi#couponCheckout");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |
 **data** | [**CouponCheckoutData**](CouponCheckoutData.md)|  | [optional]

### Return type

[**CouponCheckout**](CouponCheckout.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="couponFind"></a>
# **couponFind**
> List&lt;Coupon&gt; couponFind(filter)

Find all instances of the model matched by filter from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.CouponApi;

CouponApi apiInstance = new CouponApi();
String filter = "filter_example"; // String | Filter defining fields, where, include, order, offset, and limit
try {
    List<Coupon> result = apiInstance.couponFind(filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CouponApi#couponFind");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **String**| Filter defining fields, where, include, order, offset, and limit | [optional]

### Return type

[**List&lt;Coupon&gt;**](Coupon.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="couponFindById"></a>
# **couponFindById**
> Coupon couponFindById(id, filter)

Find a model instance by {{id}} from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.CouponApi;

CouponApi apiInstance = new CouponApi();
String id = "id_example"; // String | Model id
String filter = "filter_example"; // String | Filter defining fields and include
try {
    Coupon result = apiInstance.couponFindById(id, filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CouponApi#couponFindById");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |
 **filter** | **String**| Filter defining fields and include | [optional]

### Return type

[**Coupon**](Coupon.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="couponFindOne"></a>
# **couponFindOne**
> Coupon couponFindOne(filter)

Find first instance of the model matched by filter from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.CouponApi;

CouponApi apiInstance = new CouponApi();
String filter = "filter_example"; // String | Filter defining fields, where, include, order, offset, and limit
try {
    Coupon result = apiInstance.couponFindOne(filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CouponApi#couponFindOne");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **String**| Filter defining fields, where, include, order, offset, and limit | [optional]

### Return type

[**Coupon**](Coupon.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="couponRedeem"></a>
# **couponRedeem**
> CouponPurchase couponRedeem(id, data)



### Example
```java
// Import classes:
//import com.harpoon.api.CouponApi;

CouponApi apiInstance = new CouponApi();
String id = "id_example"; // String | Model id
CouponRedeemData data = new CouponRedeemData(); // CouponRedeemData | 
try {
    CouponPurchase result = apiInstance.couponRedeem(id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CouponApi#couponRedeem");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |
 **data** | [**CouponRedeemData**](CouponRedeemData.md)|  | [optional]

### Return type

[**CouponPurchase**](CouponPurchase.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="couponReplaceById"></a>
# **couponReplaceById**
> Coupon couponReplaceById(id, data)

Replace attributes for a model instance and persist it into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.CouponApi;

CouponApi apiInstance = new CouponApi();
String id = "id_example"; // String | Model id
Coupon data = new Coupon(); // Coupon | Model instance data
try {
    Coupon result = apiInstance.couponReplaceById(id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CouponApi#couponReplaceById");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |
 **data** | [**Coupon**](Coupon.md)| Model instance data | [optional]

### Return type

[**Coupon**](Coupon.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="couponReplaceOrCreate"></a>
# **couponReplaceOrCreate**
> Coupon couponReplaceOrCreate(data)

Replace an existing model instance or insert a new one into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.CouponApi;

CouponApi apiInstance = new CouponApi();
Coupon data = new Coupon(); // Coupon | Model instance data
try {
    Coupon result = apiInstance.couponReplaceOrCreate(data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CouponApi#couponReplaceOrCreate");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**Coupon**](Coupon.md)| Model instance data | [optional]

### Return type

[**Coupon**](Coupon.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="couponUpsertWithWhere"></a>
# **couponUpsertWithWhere**
> Coupon couponUpsertWithWhere(where, data)

Update an existing model instance or insert a new one into the data source based on the where criteria.

### Example
```java
// Import classes:
//import com.harpoon.api.CouponApi;

CouponApi apiInstance = new CouponApi();
String where = "where_example"; // String | Criteria to match model instances
Coupon data = new Coupon(); // Coupon | An object of model property name/value pairs
try {
    Coupon result = apiInstance.couponUpsertWithWhere(where, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CouponApi#couponUpsertWithWhere");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional]
 **data** | [**Coupon**](Coupon.md)| An object of model property name/value pairs | [optional]

### Return type

[**Coupon**](Coupon.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="couponVenues"></a>
# **couponVenues**
> List&lt;Venue&gt; couponVenues(id, filter)



### Example
```java
// Import classes:
//import com.harpoon.api.CouponApi;

CouponApi apiInstance = new CouponApi();
String id = "id_example"; // String | Model id
String filter = "filter_example"; // String | Filter defining fields and include
try {
    List<Venue> result = apiInstance.couponVenues(id, filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CouponApi#couponVenues");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |
 **filter** | **String**| Filter defining fields and include | [optional]

### Return type

[**List&lt;Venue&gt;**](Venue.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

