
# RadioPresenterRadioShow

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Double** |  |  [optional]
**radioPresenterId** | **Double** |  |  [optional]
**radioShowId** | **Double** |  |  [optional]
**radioPresenter** | **Object** |  |  [optional]
**radioShow** | **Object** |  |  [optional]



