# RadioPresenterRadioShowApi

All URIs are relative to *https://apicdn.harpoonconnect.com/v1.1.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**radioPresenterRadioShowCount**](RadioPresenterRadioShowApi.md#radioPresenterRadioShowCount) | **GET** /RadioPresenterRadioShows/count | Count instances of the model matched by where from the data source.
[**radioPresenterRadioShowCreate**](RadioPresenterRadioShowApi.md#radioPresenterRadioShowCreate) | **POST** /RadioPresenterRadioShows | Create a new instance of the model and persist it into the data source.
[**radioPresenterRadioShowCreateChangeStreamGetRadioPresenterRadioShowsChangeStream**](RadioPresenterRadioShowApi.md#radioPresenterRadioShowCreateChangeStreamGetRadioPresenterRadioShowsChangeStream) | **GET** /RadioPresenterRadioShows/change-stream | Create a change stream.
[**radioPresenterRadioShowCreateChangeStreamPostRadioPresenterRadioShowsChangeStream**](RadioPresenterRadioShowApi.md#radioPresenterRadioShowCreateChangeStreamPostRadioPresenterRadioShowsChangeStream) | **POST** /RadioPresenterRadioShows/change-stream | Create a change stream.
[**radioPresenterRadioShowDeleteById**](RadioPresenterRadioShowApi.md#radioPresenterRadioShowDeleteById) | **DELETE** /RadioPresenterRadioShows/{id} | Delete a model instance by {{id}} from the data source.
[**radioPresenterRadioShowExistsGetRadioPresenterRadioShowsidExists**](RadioPresenterRadioShowApi.md#radioPresenterRadioShowExistsGetRadioPresenterRadioShowsidExists) | **GET** /RadioPresenterRadioShows/{id}/exists | Check whether a model instance exists in the data source.
[**radioPresenterRadioShowExistsHeadRadioPresenterRadioShowsid**](RadioPresenterRadioShowApi.md#radioPresenterRadioShowExistsHeadRadioPresenterRadioShowsid) | **HEAD** /RadioPresenterRadioShows/{id} | Check whether a model instance exists in the data source.
[**radioPresenterRadioShowFind**](RadioPresenterRadioShowApi.md#radioPresenterRadioShowFind) | **GET** /RadioPresenterRadioShows | Find all instances of the model matched by filter from the data source.
[**radioPresenterRadioShowFindById**](RadioPresenterRadioShowApi.md#radioPresenterRadioShowFindById) | **GET** /RadioPresenterRadioShows/{id} | Find a model instance by {{id}} from the data source.
[**radioPresenterRadioShowFindOne**](RadioPresenterRadioShowApi.md#radioPresenterRadioShowFindOne) | **GET** /RadioPresenterRadioShows/findOne | Find first instance of the model matched by filter from the data source.
[**radioPresenterRadioShowPrototypeGetRadioPresenter**](RadioPresenterRadioShowApi.md#radioPresenterRadioShowPrototypeGetRadioPresenter) | **GET** /RadioPresenterRadioShows/{id}/radioPresenter | Fetches belongsTo relation radioPresenter.
[**radioPresenterRadioShowPrototypeGetRadioShow**](RadioPresenterRadioShowApi.md#radioPresenterRadioShowPrototypeGetRadioShow) | **GET** /RadioPresenterRadioShows/{id}/radioShow | Fetches belongsTo relation radioShow.
[**radioPresenterRadioShowPrototypeUpdateAttributesPatchRadioPresenterRadioShowsid**](RadioPresenterRadioShowApi.md#radioPresenterRadioShowPrototypeUpdateAttributesPatchRadioPresenterRadioShowsid) | **PATCH** /RadioPresenterRadioShows/{id} | Patch attributes for a model instance and persist it into the data source.
[**radioPresenterRadioShowPrototypeUpdateAttributesPutRadioPresenterRadioShowsid**](RadioPresenterRadioShowApi.md#radioPresenterRadioShowPrototypeUpdateAttributesPutRadioPresenterRadioShowsid) | **PUT** /RadioPresenterRadioShows/{id} | Patch attributes for a model instance and persist it into the data source.
[**radioPresenterRadioShowReplaceById**](RadioPresenterRadioShowApi.md#radioPresenterRadioShowReplaceById) | **POST** /RadioPresenterRadioShows/{id}/replace | Replace attributes for a model instance and persist it into the data source.
[**radioPresenterRadioShowReplaceOrCreate**](RadioPresenterRadioShowApi.md#radioPresenterRadioShowReplaceOrCreate) | **POST** /RadioPresenterRadioShows/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
[**radioPresenterRadioShowUpdateAll**](RadioPresenterRadioShowApi.md#radioPresenterRadioShowUpdateAll) | **POST** /RadioPresenterRadioShows/update | Update instances of the model matched by {{where}} from the data source.
[**radioPresenterRadioShowUpsertPatchRadioPresenterRadioShows**](RadioPresenterRadioShowApi.md#radioPresenterRadioShowUpsertPatchRadioPresenterRadioShows) | **PATCH** /RadioPresenterRadioShows | Patch an existing model instance or insert a new one into the data source.
[**radioPresenterRadioShowUpsertPutRadioPresenterRadioShows**](RadioPresenterRadioShowApi.md#radioPresenterRadioShowUpsertPutRadioPresenterRadioShows) | **PUT** /RadioPresenterRadioShows | Patch an existing model instance or insert a new one into the data source.
[**radioPresenterRadioShowUpsertWithWhere**](RadioPresenterRadioShowApi.md#radioPresenterRadioShowUpsertWithWhere) | **POST** /RadioPresenterRadioShows/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.


<a name="radioPresenterRadioShowCount"></a>
# **radioPresenterRadioShowCount**
> InlineResponse2001 radioPresenterRadioShowCount(where)

Count instances of the model matched by where from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.RadioPresenterRadioShowApi;

RadioPresenterRadioShowApi apiInstance = new RadioPresenterRadioShowApi();
String where = "where_example"; // String | Criteria to match model instances
try {
    InlineResponse2001 result = apiInstance.radioPresenterRadioShowCount(where);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RadioPresenterRadioShowApi#radioPresenterRadioShowCount");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional]

### Return type

[**InlineResponse2001**](InlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioPresenterRadioShowCreate"></a>
# **radioPresenterRadioShowCreate**
> RadioPresenterRadioShow radioPresenterRadioShowCreate(data)

Create a new instance of the model and persist it into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.RadioPresenterRadioShowApi;

RadioPresenterRadioShowApi apiInstance = new RadioPresenterRadioShowApi();
RadioPresenterRadioShow data = new RadioPresenterRadioShow(); // RadioPresenterRadioShow | Model instance data
try {
    RadioPresenterRadioShow result = apiInstance.radioPresenterRadioShowCreate(data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RadioPresenterRadioShowApi#radioPresenterRadioShowCreate");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**RadioPresenterRadioShow**](RadioPresenterRadioShow.md)| Model instance data | [optional]

### Return type

[**RadioPresenterRadioShow**](RadioPresenterRadioShow.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioPresenterRadioShowCreateChangeStreamGetRadioPresenterRadioShowsChangeStream"></a>
# **radioPresenterRadioShowCreateChangeStreamGetRadioPresenterRadioShowsChangeStream**
> File radioPresenterRadioShowCreateChangeStreamGetRadioPresenterRadioShowsChangeStream(options)

Create a change stream.

### Example
```java
// Import classes:
//import com.harpoon.api.RadioPresenterRadioShowApi;

RadioPresenterRadioShowApi apiInstance = new RadioPresenterRadioShowApi();
String options = "options_example"; // String | 
try {
    File result = apiInstance.radioPresenterRadioShowCreateChangeStreamGetRadioPresenterRadioShowsChangeStream(options);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RadioPresenterRadioShowApi#radioPresenterRadioShowCreateChangeStreamGetRadioPresenterRadioShowsChangeStream");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **String**|  | [optional]

### Return type

[**File**](File.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioPresenterRadioShowCreateChangeStreamPostRadioPresenterRadioShowsChangeStream"></a>
# **radioPresenterRadioShowCreateChangeStreamPostRadioPresenterRadioShowsChangeStream**
> File radioPresenterRadioShowCreateChangeStreamPostRadioPresenterRadioShowsChangeStream(options)

Create a change stream.

### Example
```java
// Import classes:
//import com.harpoon.api.RadioPresenterRadioShowApi;

RadioPresenterRadioShowApi apiInstance = new RadioPresenterRadioShowApi();
String options = "options_example"; // String | 
try {
    File result = apiInstance.radioPresenterRadioShowCreateChangeStreamPostRadioPresenterRadioShowsChangeStream(options);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RadioPresenterRadioShowApi#radioPresenterRadioShowCreateChangeStreamPostRadioPresenterRadioShowsChangeStream");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **String**|  | [optional]

### Return type

[**File**](File.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioPresenterRadioShowDeleteById"></a>
# **radioPresenterRadioShowDeleteById**
> Object radioPresenterRadioShowDeleteById(id)

Delete a model instance by {{id}} from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.RadioPresenterRadioShowApi;

RadioPresenterRadioShowApi apiInstance = new RadioPresenterRadioShowApi();
String id = "id_example"; // String | Model id
try {
    Object result = apiInstance.radioPresenterRadioShowDeleteById(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RadioPresenterRadioShowApi#radioPresenterRadioShowDeleteById");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |

### Return type

**Object**

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioPresenterRadioShowExistsGetRadioPresenterRadioShowsidExists"></a>
# **radioPresenterRadioShowExistsGetRadioPresenterRadioShowsidExists**
> InlineResponse2003 radioPresenterRadioShowExistsGetRadioPresenterRadioShowsidExists(id)

Check whether a model instance exists in the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.RadioPresenterRadioShowApi;

RadioPresenterRadioShowApi apiInstance = new RadioPresenterRadioShowApi();
String id = "id_example"; // String | Model id
try {
    InlineResponse2003 result = apiInstance.radioPresenterRadioShowExistsGetRadioPresenterRadioShowsidExists(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RadioPresenterRadioShowApi#radioPresenterRadioShowExistsGetRadioPresenterRadioShowsidExists");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |

### Return type

[**InlineResponse2003**](InlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioPresenterRadioShowExistsHeadRadioPresenterRadioShowsid"></a>
# **radioPresenterRadioShowExistsHeadRadioPresenterRadioShowsid**
> InlineResponse2003 radioPresenterRadioShowExistsHeadRadioPresenterRadioShowsid(id)

Check whether a model instance exists in the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.RadioPresenterRadioShowApi;

RadioPresenterRadioShowApi apiInstance = new RadioPresenterRadioShowApi();
String id = "id_example"; // String | Model id
try {
    InlineResponse2003 result = apiInstance.radioPresenterRadioShowExistsHeadRadioPresenterRadioShowsid(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RadioPresenterRadioShowApi#radioPresenterRadioShowExistsHeadRadioPresenterRadioShowsid");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |

### Return type

[**InlineResponse2003**](InlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioPresenterRadioShowFind"></a>
# **radioPresenterRadioShowFind**
> List&lt;RadioPresenterRadioShow&gt; radioPresenterRadioShowFind(filter)

Find all instances of the model matched by filter from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.RadioPresenterRadioShowApi;

RadioPresenterRadioShowApi apiInstance = new RadioPresenterRadioShowApi();
String filter = "filter_example"; // String | Filter defining fields, where, include, order, offset, and limit
try {
    List<RadioPresenterRadioShow> result = apiInstance.radioPresenterRadioShowFind(filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RadioPresenterRadioShowApi#radioPresenterRadioShowFind");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **String**| Filter defining fields, where, include, order, offset, and limit | [optional]

### Return type

[**List&lt;RadioPresenterRadioShow&gt;**](RadioPresenterRadioShow.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioPresenterRadioShowFindById"></a>
# **radioPresenterRadioShowFindById**
> RadioPresenterRadioShow radioPresenterRadioShowFindById(id, filter)

Find a model instance by {{id}} from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.RadioPresenterRadioShowApi;

RadioPresenterRadioShowApi apiInstance = new RadioPresenterRadioShowApi();
String id = "id_example"; // String | Model id
String filter = "filter_example"; // String | Filter defining fields and include
try {
    RadioPresenterRadioShow result = apiInstance.radioPresenterRadioShowFindById(id, filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RadioPresenterRadioShowApi#radioPresenterRadioShowFindById");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |
 **filter** | **String**| Filter defining fields and include | [optional]

### Return type

[**RadioPresenterRadioShow**](RadioPresenterRadioShow.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioPresenterRadioShowFindOne"></a>
# **radioPresenterRadioShowFindOne**
> RadioPresenterRadioShow radioPresenterRadioShowFindOne(filter)

Find first instance of the model matched by filter from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.RadioPresenterRadioShowApi;

RadioPresenterRadioShowApi apiInstance = new RadioPresenterRadioShowApi();
String filter = "filter_example"; // String | Filter defining fields, where, include, order, offset, and limit
try {
    RadioPresenterRadioShow result = apiInstance.radioPresenterRadioShowFindOne(filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RadioPresenterRadioShowApi#radioPresenterRadioShowFindOne");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **String**| Filter defining fields, where, include, order, offset, and limit | [optional]

### Return type

[**RadioPresenterRadioShow**](RadioPresenterRadioShow.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioPresenterRadioShowPrototypeGetRadioPresenter"></a>
# **radioPresenterRadioShowPrototypeGetRadioPresenter**
> RadioPresenter radioPresenterRadioShowPrototypeGetRadioPresenter(id, refresh)

Fetches belongsTo relation radioPresenter.

### Example
```java
// Import classes:
//import com.harpoon.api.RadioPresenterRadioShowApi;

RadioPresenterRadioShowApi apiInstance = new RadioPresenterRadioShowApi();
String id = "id_example"; // String | RadioPresenterRadioShow id
Boolean refresh = true; // Boolean | 
try {
    RadioPresenter result = apiInstance.radioPresenterRadioShowPrototypeGetRadioPresenter(id, refresh);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RadioPresenterRadioShowApi#radioPresenterRadioShowPrototypeGetRadioPresenter");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| RadioPresenterRadioShow id |
 **refresh** | **Boolean**|  | [optional]

### Return type

[**RadioPresenter**](RadioPresenter.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioPresenterRadioShowPrototypeGetRadioShow"></a>
# **radioPresenterRadioShowPrototypeGetRadioShow**
> RadioShow radioPresenterRadioShowPrototypeGetRadioShow(id, refresh)

Fetches belongsTo relation radioShow.

### Example
```java
// Import classes:
//import com.harpoon.api.RadioPresenterRadioShowApi;

RadioPresenterRadioShowApi apiInstance = new RadioPresenterRadioShowApi();
String id = "id_example"; // String | RadioPresenterRadioShow id
Boolean refresh = true; // Boolean | 
try {
    RadioShow result = apiInstance.radioPresenterRadioShowPrototypeGetRadioShow(id, refresh);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RadioPresenterRadioShowApi#radioPresenterRadioShowPrototypeGetRadioShow");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| RadioPresenterRadioShow id |
 **refresh** | **Boolean**|  | [optional]

### Return type

[**RadioShow**](RadioShow.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioPresenterRadioShowPrototypeUpdateAttributesPatchRadioPresenterRadioShowsid"></a>
# **radioPresenterRadioShowPrototypeUpdateAttributesPatchRadioPresenterRadioShowsid**
> RadioPresenterRadioShow radioPresenterRadioShowPrototypeUpdateAttributesPatchRadioPresenterRadioShowsid(id, data)

Patch attributes for a model instance and persist it into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.RadioPresenterRadioShowApi;

RadioPresenterRadioShowApi apiInstance = new RadioPresenterRadioShowApi();
String id = "id_example"; // String | RadioPresenterRadioShow id
RadioPresenterRadioShow data = new RadioPresenterRadioShow(); // RadioPresenterRadioShow | An object of model property name/value pairs
try {
    RadioPresenterRadioShow result = apiInstance.radioPresenterRadioShowPrototypeUpdateAttributesPatchRadioPresenterRadioShowsid(id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RadioPresenterRadioShowApi#radioPresenterRadioShowPrototypeUpdateAttributesPatchRadioPresenterRadioShowsid");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| RadioPresenterRadioShow id |
 **data** | [**RadioPresenterRadioShow**](RadioPresenterRadioShow.md)| An object of model property name/value pairs | [optional]

### Return type

[**RadioPresenterRadioShow**](RadioPresenterRadioShow.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioPresenterRadioShowPrototypeUpdateAttributesPutRadioPresenterRadioShowsid"></a>
# **radioPresenterRadioShowPrototypeUpdateAttributesPutRadioPresenterRadioShowsid**
> RadioPresenterRadioShow radioPresenterRadioShowPrototypeUpdateAttributesPutRadioPresenterRadioShowsid(id, data)

Patch attributes for a model instance and persist it into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.RadioPresenterRadioShowApi;

RadioPresenterRadioShowApi apiInstance = new RadioPresenterRadioShowApi();
String id = "id_example"; // String | RadioPresenterRadioShow id
RadioPresenterRadioShow data = new RadioPresenterRadioShow(); // RadioPresenterRadioShow | An object of model property name/value pairs
try {
    RadioPresenterRadioShow result = apiInstance.radioPresenterRadioShowPrototypeUpdateAttributesPutRadioPresenterRadioShowsid(id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RadioPresenterRadioShowApi#radioPresenterRadioShowPrototypeUpdateAttributesPutRadioPresenterRadioShowsid");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| RadioPresenterRadioShow id |
 **data** | [**RadioPresenterRadioShow**](RadioPresenterRadioShow.md)| An object of model property name/value pairs | [optional]

### Return type

[**RadioPresenterRadioShow**](RadioPresenterRadioShow.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioPresenterRadioShowReplaceById"></a>
# **radioPresenterRadioShowReplaceById**
> RadioPresenterRadioShow radioPresenterRadioShowReplaceById(id, data)

Replace attributes for a model instance and persist it into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.RadioPresenterRadioShowApi;

RadioPresenterRadioShowApi apiInstance = new RadioPresenterRadioShowApi();
String id = "id_example"; // String | Model id
RadioPresenterRadioShow data = new RadioPresenterRadioShow(); // RadioPresenterRadioShow | Model instance data
try {
    RadioPresenterRadioShow result = apiInstance.radioPresenterRadioShowReplaceById(id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RadioPresenterRadioShowApi#radioPresenterRadioShowReplaceById");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |
 **data** | [**RadioPresenterRadioShow**](RadioPresenterRadioShow.md)| Model instance data | [optional]

### Return type

[**RadioPresenterRadioShow**](RadioPresenterRadioShow.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioPresenterRadioShowReplaceOrCreate"></a>
# **radioPresenterRadioShowReplaceOrCreate**
> RadioPresenterRadioShow radioPresenterRadioShowReplaceOrCreate(data)

Replace an existing model instance or insert a new one into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.RadioPresenterRadioShowApi;

RadioPresenterRadioShowApi apiInstance = new RadioPresenterRadioShowApi();
RadioPresenterRadioShow data = new RadioPresenterRadioShow(); // RadioPresenterRadioShow | Model instance data
try {
    RadioPresenterRadioShow result = apiInstance.radioPresenterRadioShowReplaceOrCreate(data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RadioPresenterRadioShowApi#radioPresenterRadioShowReplaceOrCreate");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**RadioPresenterRadioShow**](RadioPresenterRadioShow.md)| Model instance data | [optional]

### Return type

[**RadioPresenterRadioShow**](RadioPresenterRadioShow.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioPresenterRadioShowUpdateAll"></a>
# **radioPresenterRadioShowUpdateAll**
> InlineResponse2002 radioPresenterRadioShowUpdateAll(where, data)

Update instances of the model matched by {{where}} from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.RadioPresenterRadioShowApi;

RadioPresenterRadioShowApi apiInstance = new RadioPresenterRadioShowApi();
String where = "where_example"; // String | Criteria to match model instances
RadioPresenterRadioShow data = new RadioPresenterRadioShow(); // RadioPresenterRadioShow | An object of model property name/value pairs
try {
    InlineResponse2002 result = apiInstance.radioPresenterRadioShowUpdateAll(where, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RadioPresenterRadioShowApi#radioPresenterRadioShowUpdateAll");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional]
 **data** | [**RadioPresenterRadioShow**](RadioPresenterRadioShow.md)| An object of model property name/value pairs | [optional]

### Return type

[**InlineResponse2002**](InlineResponse2002.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioPresenterRadioShowUpsertPatchRadioPresenterRadioShows"></a>
# **radioPresenterRadioShowUpsertPatchRadioPresenterRadioShows**
> RadioPresenterRadioShow radioPresenterRadioShowUpsertPatchRadioPresenterRadioShows(data)

Patch an existing model instance or insert a new one into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.RadioPresenterRadioShowApi;

RadioPresenterRadioShowApi apiInstance = new RadioPresenterRadioShowApi();
RadioPresenterRadioShow data = new RadioPresenterRadioShow(); // RadioPresenterRadioShow | Model instance data
try {
    RadioPresenterRadioShow result = apiInstance.radioPresenterRadioShowUpsertPatchRadioPresenterRadioShows(data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RadioPresenterRadioShowApi#radioPresenterRadioShowUpsertPatchRadioPresenterRadioShows");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**RadioPresenterRadioShow**](RadioPresenterRadioShow.md)| Model instance data | [optional]

### Return type

[**RadioPresenterRadioShow**](RadioPresenterRadioShow.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioPresenterRadioShowUpsertPutRadioPresenterRadioShows"></a>
# **radioPresenterRadioShowUpsertPutRadioPresenterRadioShows**
> RadioPresenterRadioShow radioPresenterRadioShowUpsertPutRadioPresenterRadioShows(data)

Patch an existing model instance or insert a new one into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.RadioPresenterRadioShowApi;

RadioPresenterRadioShowApi apiInstance = new RadioPresenterRadioShowApi();
RadioPresenterRadioShow data = new RadioPresenterRadioShow(); // RadioPresenterRadioShow | Model instance data
try {
    RadioPresenterRadioShow result = apiInstance.radioPresenterRadioShowUpsertPutRadioPresenterRadioShows(data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RadioPresenterRadioShowApi#radioPresenterRadioShowUpsertPutRadioPresenterRadioShows");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**RadioPresenterRadioShow**](RadioPresenterRadioShow.md)| Model instance data | [optional]

### Return type

[**RadioPresenterRadioShow**](RadioPresenterRadioShow.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioPresenterRadioShowUpsertWithWhere"></a>
# **radioPresenterRadioShowUpsertWithWhere**
> RadioPresenterRadioShow radioPresenterRadioShowUpsertWithWhere(where, data)

Update an existing model instance or insert a new one into the data source based on the where criteria.

### Example
```java
// Import classes:
//import com.harpoon.api.RadioPresenterRadioShowApi;

RadioPresenterRadioShowApi apiInstance = new RadioPresenterRadioShowApi();
String where = "where_example"; // String | Criteria to match model instances
RadioPresenterRadioShow data = new RadioPresenterRadioShow(); // RadioPresenterRadioShow | An object of model property name/value pairs
try {
    RadioPresenterRadioShow result = apiInstance.radioPresenterRadioShowUpsertWithWhere(where, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RadioPresenterRadioShowApi#radioPresenterRadioShowUpsertWithWhere");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional]
 **data** | [**RadioPresenterRadioShow**](RadioPresenterRadioShow.md)| An object of model property name/value pairs | [optional]

### Return type

[**RadioPresenterRadioShow**](RadioPresenterRadioShow.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

