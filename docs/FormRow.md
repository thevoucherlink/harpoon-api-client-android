
# FormRow

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**metadataKey** | **String** |  | 
**type** | [**TypeEnum**](#TypeEnum) |  | 
**title** | **String** |  | 
**defaultValue** | **String** |  |  [optional]
**placeholder** | **String** |  |  [optional]
**hidden** | **Boolean** |  |  [optional]
**required** | **Boolean** |  |  [optional]
**validationRegExp** | **String** |  |  [optional]
**validationMessage** | **String** |  |  [optional]
**selectorOptions** | [**List&lt;FormSelectorOption&gt;**](FormSelectorOption.md) |  |  [optional]


<a name="TypeEnum"></a>
## Enum: TypeEnum
Name | Value
---- | -----



