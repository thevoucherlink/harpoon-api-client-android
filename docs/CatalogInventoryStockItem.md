
# CatalogInventoryStockItem

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**backorders** | **Double** |  | 
**enableQtyIncrements** | **Double** |  | 
**isDecimalDivided** | **Double** |  | 
**isQtyDecimal** | **Double** |  | 
**isInStock** | **Double** |  | 
**id** | **Double** |  | 
**lowStockDate** | [**Date**](Date.md) |  |  [optional]
**manageStock** | **Double** |  | 
**minQty** | **String** |  | 
**maxSaleQty** | **String** |  | 
**minSaleQty** | **String** |  | 
**notifyStockQty** | **String** |  |  [optional]
**productId** | **Double** |  | 
**qty** | **String** |  | 
**stockId** | **Double** |  | 
**useConfigBackorders** | **Double** |  | 
**qtyIncrements** | **String** |  | 
**stockStatusChangedAuto** | **Double** |  | 
**useConfigEnableQtyInc** | **Double** |  | 
**useConfigMaxSaleQty** | **Double** |  | 
**useConfigManageStock** | **Double** |  | 
**useConfigMinQty** | **Double** |  | 
**useConfigNotifyStockQty** | **Double** |  | 
**useConfigMinSaleQty** | **Double** |  | 
**useConfigQtyIncrements** | **Double** |  | 



