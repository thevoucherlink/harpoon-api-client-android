
# RadioShow

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** |  | 
**description** | **String** |  |  [optional]
**content** | **String** |  |  [optional]
**contentType** | [**ContentTypeEnum**](#ContentTypeEnum) |  |  [optional]
**contact** | [**Contact**](Contact.md) | Contacts for this show |  [optional]
**starts** | [**Date**](Date.md) | When the Show starts of being public |  [optional]
**ends** | [**Date**](Date.md) | When the Show ceases of being public |  [optional]
**type** | **String** |  |  [optional]
**radioShowTimeCurrent** | [**RadioShowTime**](RadioShowTime.md) |  |  [optional]
**imgShow** | **String** |  |  [optional]
**sponsorTrack** | **String** | Url of the sponsor MP3 to be played |  [optional]
**id** | **Double** |  |  [optional]
**radioStreamId** | **Double** |  |  [optional]
**playlistItemId** | **Double** |  |  [optional]
**radioPresenters** | **List&lt;Object&gt;** |  |  [optional]
**radioStream** | **Object** |  |  [optional]
**radioShowTimes** | **List&lt;Object&gt;** |  |  [optional]
**playlistItem** | **Object** |  |  [optional]


<a name="ContentTypeEnum"></a>
## Enum: ContentTypeEnum
Name | Value
---- | -----



