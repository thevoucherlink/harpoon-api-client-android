
# PlaylistItem

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**title** | **String** | Stream title |  [optional]
**shortDescription** | **String** | Stream short description (on SDKs is under desc) |  [optional]
**image** | **String** | Playlist image |  [optional]
**file** | **String** | Stream file |  [optional]
**type** | **String** | Stream type (used only in JS SDK) |  [optional]
**mediaType** | **String** | Stream media type (e.g. video , audio , radioStream) |  [optional]
**mediaId** | **Double** | Ad media id (on SDKs is under mediaid) |  [optional]
**order** | **Double** | Sort order index |  [optional]
**id** | **Double** |  |  [optional]
**playlistId** | **Double** |  |  [optional]
**playlist** | **Object** |  |  [optional]
**playerSources** | **List&lt;Object&gt;** |  |  [optional]
**playerTracks** | **List&lt;Object&gt;** |  |  [optional]
**radioShows** | **List&lt;Object&gt;** |  |  [optional]



