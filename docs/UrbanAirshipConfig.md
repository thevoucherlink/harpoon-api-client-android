
# UrbanAirshipConfig

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**developmentAppKey** | **String** |  | 
**developmentAppSecret** | **String** |  |  [optional]
**developmentLogLevel** | [**DevelopmentLogLevelEnum**](#DevelopmentLogLevelEnum) |  |  [optional]
**productionAppKey** | **String** |  |  [optional]
**productionAppSecret** | **String** |  |  [optional]
**productionLogLevel** | [**ProductionLogLevelEnum**](#ProductionLogLevelEnum) |  |  [optional]
**inProduction** | **Boolean** |  |  [optional]


<a name="DevelopmentLogLevelEnum"></a>
## Enum: DevelopmentLogLevelEnum
Name | Value
---- | -----


<a name="ProductionLogLevelEnum"></a>
## Enum: ProductionLogLevelEnum
Name | Value
---- | -----



