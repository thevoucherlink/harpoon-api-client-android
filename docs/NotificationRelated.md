
# NotificationRelated

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**customerId** | **Double** |  |  [optional]
**brandId** | **Double** |  |  [optional]
**venueId** | **Double** |  |  [optional]
**competitionId** | **Double** |  |  [optional]
**couponId** | **Double** |  |  [optional]
**eventId** | **Double** |  |  [optional]
**dealPaidId** | **Double** |  |  [optional]
**dealGroupId** | **Double** |  |  [optional]
**id** | **Double** |  |  [optional]



