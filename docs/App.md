
# App

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  | 
**internalId** | **Double** | Id used internally in Harpoon |  [optional]
**vendorId** | **Double** | Vendor App owner |  [optional]
**name** | **String** | Name of the application | 
**category** | **String** |  |  [optional]
**description** | **String** |  |  [optional]
**keywords** | **String** |  |  [optional]
**copyright** | **String** |  |  [optional]
**realm** | **String** |  |  [optional]
**bundle** | **String** | Bundle Identifier of the application, com.{company name}.{project name} |  [optional]
**urlScheme** | **String** | URL Scheme used for deeplinking |  [optional]
**brandFollowOffer** | **Boolean** | Customer need to follow the Brand to display the Offers outside of the Brand profile page |  [optional]
**privacyUrl** | **String** | URL linking to the Privacy page for the app |  [optional]
**termsOfServiceUrl** | **String** | URL linking to the Terms of Service page for the app |  [optional]
**restrictionAge** | **Double** | Restrict the use of the app to just the customer with at least the value specified, 0 &#x3D; no limit |  [optional]
**storeAndroidUrl** | **String** | URL linking to the Google Play page for the app |  [optional]
**storeIosUrl** | **String** | URL linking to the iTunes App Store page for the app |  [optional]
**typeNewsFeed** | [**TypeNewsFeedEnum**](#TypeNewsFeedEnum) | News Feed source, nativeFeed/rss |  [optional]
**clientSecret** | **String** |  |  [optional]
**grantTypes** | **List&lt;String&gt;** |  |  [optional]
**scopes** | **List&lt;String&gt;** |  |  [optional]
**status** | [**StatusEnum**](#StatusEnum) | Status of the application, production/sandbox/disabled |  [optional]
**created** | [**Date**](Date.md) |  |  [optional]
**modified** | [**Date**](Date.md) |  |  [optional]
**styleOfferList** | [**StyleOfferListEnum**](#StyleOfferListEnum) | Size of the Offer List Item, big/small |  [optional]
**appConfig** | [**AppConfig**](AppConfig.md) |  |  [optional]
**clientToken** | **String** |  |  [optional]
**multiConfigTitle** | **String** |  |  [optional]
**multiConfig** | [**List&lt;AppConfig&gt;**](AppConfig.md) |  |  [optional]
**appId** | **String** |  |  [optional]
**parentId** | **String** |  |  [optional]
**rssFeeds** | **List&lt;Object&gt;** |  |  [optional]
**rssFeedGroups** | **List&lt;Object&gt;** |  |  [optional]
**radioStreams** | **List&lt;Object&gt;** |  |  [optional]
**customers** | **List&lt;Object&gt;** |  |  [optional]
**appConfigs** | **List&lt;Object&gt;** |  |  [optional]
**playlists** | **List&lt;Object&gt;** |  |  [optional]
**apps** | **List&lt;Object&gt;** |  |  [optional]
**parent** | **Object** |  |  [optional]


<a name="TypeNewsFeedEnum"></a>
## Enum: TypeNewsFeedEnum
Name | Value
---- | -----


<a name="StatusEnum"></a>
## Enum: StatusEnum
Name | Value
---- | -----


<a name="StyleOfferListEnum"></a>
## Enum: StyleOfferListEnum
Name | Value
---- | -----



