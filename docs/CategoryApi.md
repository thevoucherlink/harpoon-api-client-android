# CategoryApi

All URIs are relative to *https://apicdn.harpoonconnect.com/v1.1.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**categoryFindBrandCategories**](CategoryApi.md#categoryFindBrandCategories) | **GET** /Categories/types/brand | 
[**categoryFindCampaignTypeCategories**](CategoryApi.md#categoryFindCampaignTypeCategories) | **GET** /Categories/types/campaignType | 
[**categoryFindOfferCategories**](CategoryApi.md#categoryFindOfferCategories) | **GET** /Categories/types/offer | 
[**categoryReplaceById**](CategoryApi.md#categoryReplaceById) | **POST** /Categories/{id}/replace | Replace attributes for a model instance and persist it into the data source.
[**categoryReplaceOrCreate**](CategoryApi.md#categoryReplaceOrCreate) | **POST** /Categories/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
[**categoryUpsertWithWhere**](CategoryApi.md#categoryUpsertWithWhere) | **POST** /Categories/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.


<a name="categoryFindBrandCategories"></a>
# **categoryFindBrandCategories**
> List&lt;Category&gt; categoryFindBrandCategories(filter)



### Example
```java
// Import classes:
//import com.harpoon.api.CategoryApi;

CategoryApi apiInstance = new CategoryApi();
String filter = "filter_example"; // String | Filter defining fields and include
try {
    List<Category> result = apiInstance.categoryFindBrandCategories(filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CategoryApi#categoryFindBrandCategories");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **String**| Filter defining fields and include | [optional]

### Return type

[**List&lt;Category&gt;**](Category.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="categoryFindCampaignTypeCategories"></a>
# **categoryFindCampaignTypeCategories**
> List&lt;Category&gt; categoryFindCampaignTypeCategories(filter)



### Example
```java
// Import classes:
//import com.harpoon.api.CategoryApi;

CategoryApi apiInstance = new CategoryApi();
String filter = "filter_example"; // String | Filter defining fields and include
try {
    List<Category> result = apiInstance.categoryFindCampaignTypeCategories(filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CategoryApi#categoryFindCampaignTypeCategories");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **String**| Filter defining fields and include | [optional]

### Return type

[**List&lt;Category&gt;**](Category.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="categoryFindOfferCategories"></a>
# **categoryFindOfferCategories**
> List&lt;Category&gt; categoryFindOfferCategories(filter)



### Example
```java
// Import classes:
//import com.harpoon.api.CategoryApi;

CategoryApi apiInstance = new CategoryApi();
String filter = "filter_example"; // String | Filter defining fields and include
try {
    List<Category> result = apiInstance.categoryFindOfferCategories(filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CategoryApi#categoryFindOfferCategories");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **String**| Filter defining fields and include | [optional]

### Return type

[**List&lt;Category&gt;**](Category.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="categoryReplaceById"></a>
# **categoryReplaceById**
> Category categoryReplaceById(id, data)

Replace attributes for a model instance and persist it into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.CategoryApi;

CategoryApi apiInstance = new CategoryApi();
String id = "id_example"; // String | Model id
Category data = new Category(); // Category | Model instance data
try {
    Category result = apiInstance.categoryReplaceById(id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CategoryApi#categoryReplaceById");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |
 **data** | [**Category**](Category.md)| Model instance data | [optional]

### Return type

[**Category**](Category.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="categoryReplaceOrCreate"></a>
# **categoryReplaceOrCreate**
> Category categoryReplaceOrCreate(data)

Replace an existing model instance or insert a new one into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.CategoryApi;

CategoryApi apiInstance = new CategoryApi();
Category data = new Category(); // Category | Model instance data
try {
    Category result = apiInstance.categoryReplaceOrCreate(data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CategoryApi#categoryReplaceOrCreate");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**Category**](Category.md)| Model instance data | [optional]

### Return type

[**Category**](Category.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="categoryUpsertWithWhere"></a>
# **categoryUpsertWithWhere**
> Category categoryUpsertWithWhere(where, data)

Update an existing model instance or insert a new one into the data source based on the where criteria.

### Example
```java
// Import classes:
//import com.harpoon.api.CategoryApi;

CategoryApi apiInstance = new CategoryApi();
String where = "where_example"; // String | Criteria to match model instances
Category data = new Category(); // Category | An object of model property name/value pairs
try {
    Category result = apiInstance.categoryUpsertWithWhere(where, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CategoryApi#categoryUpsertWithWhere");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional]
 **data** | [**Category**](Category.md)| An object of model property name/value pairs | [optional]

### Return type

[**Category**](Category.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

