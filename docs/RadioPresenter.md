
# RadioPresenter

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** |  | 
**image** | **String** |  |  [optional]
**contact** | [**Contact**](Contact.md) | Contacts for this presenter |  [optional]
**id** | **Double** |  |  [optional]
**radioShows** | **List&lt;Object&gt;** |  |  [optional]



