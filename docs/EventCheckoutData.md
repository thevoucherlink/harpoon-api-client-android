
# EventCheckoutData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**tickets** | [**List&lt;EventCheckoutTicketData&gt;**](EventCheckoutTicketData.md) | Tickets to checkout | 



