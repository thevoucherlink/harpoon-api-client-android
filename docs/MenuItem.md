
# MenuItem

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** |  |  [optional]
**url** | **String** |  |  [optional]
**image** | **String** |  |  [optional]
**visibility** | **Boolean** |  |  [optional]
**sortOrder** | **Double** |  |  [optional]
**pageAction** | **Boolean** |  |  [optional]
**pageActionIcon** | **String** |  |  [optional]
**pageActionLink** | **String** |  |  [optional]
**styleCss** | **String** |  |  [optional]
**featureRadioDontShowOnCamera** | **Boolean** |  |  [optional]
**id** | **Double** |  |  [optional]



