# StripeDiscountApi

All URIs are relative to *https://apicdn.harpoonconnect.com/v1.1.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**stripeDiscountCount**](StripeDiscountApi.md#stripeDiscountCount) | **GET** /StripeDiscounts/count | Count instances of the model matched by where from the data source.
[**stripeDiscountCreate**](StripeDiscountApi.md#stripeDiscountCreate) | **POST** /StripeDiscounts | Create a new instance of the model and persist it into the data source.
[**stripeDiscountCreateChangeStreamGetStripeDiscountsChangeStream**](StripeDiscountApi.md#stripeDiscountCreateChangeStreamGetStripeDiscountsChangeStream) | **GET** /StripeDiscounts/change-stream | Create a change stream.
[**stripeDiscountCreateChangeStreamPostStripeDiscountsChangeStream**](StripeDiscountApi.md#stripeDiscountCreateChangeStreamPostStripeDiscountsChangeStream) | **POST** /StripeDiscounts/change-stream | Create a change stream.
[**stripeDiscountDeleteById**](StripeDiscountApi.md#stripeDiscountDeleteById) | **DELETE** /StripeDiscounts/{id} | Delete a model instance by {{id}} from the data source.
[**stripeDiscountExistsGetStripeDiscountsidExists**](StripeDiscountApi.md#stripeDiscountExistsGetStripeDiscountsidExists) | **GET** /StripeDiscounts/{id}/exists | Check whether a model instance exists in the data source.
[**stripeDiscountExistsHeadStripeDiscountsid**](StripeDiscountApi.md#stripeDiscountExistsHeadStripeDiscountsid) | **HEAD** /StripeDiscounts/{id} | Check whether a model instance exists in the data source.
[**stripeDiscountFind**](StripeDiscountApi.md#stripeDiscountFind) | **GET** /StripeDiscounts | Find all instances of the model matched by filter from the data source.
[**stripeDiscountFindById**](StripeDiscountApi.md#stripeDiscountFindById) | **GET** /StripeDiscounts/{id} | Find a model instance by {{id}} from the data source.
[**stripeDiscountFindOne**](StripeDiscountApi.md#stripeDiscountFindOne) | **GET** /StripeDiscounts/findOne | Find first instance of the model matched by filter from the data source.
[**stripeDiscountPrototypeUpdateAttributesPatchStripeDiscountsid**](StripeDiscountApi.md#stripeDiscountPrototypeUpdateAttributesPatchStripeDiscountsid) | **PATCH** /StripeDiscounts/{id} | Patch attributes for a model instance and persist it into the data source.
[**stripeDiscountPrototypeUpdateAttributesPutStripeDiscountsid**](StripeDiscountApi.md#stripeDiscountPrototypeUpdateAttributesPutStripeDiscountsid) | **PUT** /StripeDiscounts/{id} | Patch attributes for a model instance and persist it into the data source.
[**stripeDiscountReplaceById**](StripeDiscountApi.md#stripeDiscountReplaceById) | **POST** /StripeDiscounts/{id}/replace | Replace attributes for a model instance and persist it into the data source.
[**stripeDiscountReplaceOrCreate**](StripeDiscountApi.md#stripeDiscountReplaceOrCreate) | **POST** /StripeDiscounts/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
[**stripeDiscountUpdateAll**](StripeDiscountApi.md#stripeDiscountUpdateAll) | **POST** /StripeDiscounts/update | Update instances of the model matched by {{where}} from the data source.
[**stripeDiscountUpsertPatchStripeDiscounts**](StripeDiscountApi.md#stripeDiscountUpsertPatchStripeDiscounts) | **PATCH** /StripeDiscounts | Patch an existing model instance or insert a new one into the data source.
[**stripeDiscountUpsertPutStripeDiscounts**](StripeDiscountApi.md#stripeDiscountUpsertPutStripeDiscounts) | **PUT** /StripeDiscounts | Patch an existing model instance or insert a new one into the data source.
[**stripeDiscountUpsertWithWhere**](StripeDiscountApi.md#stripeDiscountUpsertWithWhere) | **POST** /StripeDiscounts/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.


<a name="stripeDiscountCount"></a>
# **stripeDiscountCount**
> InlineResponse2001 stripeDiscountCount(where)

Count instances of the model matched by where from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.StripeDiscountApi;

StripeDiscountApi apiInstance = new StripeDiscountApi();
String where = "where_example"; // String | Criteria to match model instances
try {
    InlineResponse2001 result = apiInstance.stripeDiscountCount(where);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling StripeDiscountApi#stripeDiscountCount");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional]

### Return type

[**InlineResponse2001**](InlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripeDiscountCreate"></a>
# **stripeDiscountCreate**
> StripeDiscount stripeDiscountCreate(data)

Create a new instance of the model and persist it into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.StripeDiscountApi;

StripeDiscountApi apiInstance = new StripeDiscountApi();
StripeDiscount data = new StripeDiscount(); // StripeDiscount | Model instance data
try {
    StripeDiscount result = apiInstance.stripeDiscountCreate(data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling StripeDiscountApi#stripeDiscountCreate");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**StripeDiscount**](StripeDiscount.md)| Model instance data | [optional]

### Return type

[**StripeDiscount**](StripeDiscount.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripeDiscountCreateChangeStreamGetStripeDiscountsChangeStream"></a>
# **stripeDiscountCreateChangeStreamGetStripeDiscountsChangeStream**
> File stripeDiscountCreateChangeStreamGetStripeDiscountsChangeStream(options)

Create a change stream.

### Example
```java
// Import classes:
//import com.harpoon.api.StripeDiscountApi;

StripeDiscountApi apiInstance = new StripeDiscountApi();
String options = "options_example"; // String | 
try {
    File result = apiInstance.stripeDiscountCreateChangeStreamGetStripeDiscountsChangeStream(options);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling StripeDiscountApi#stripeDiscountCreateChangeStreamGetStripeDiscountsChangeStream");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **String**|  | [optional]

### Return type

[**File**](File.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripeDiscountCreateChangeStreamPostStripeDiscountsChangeStream"></a>
# **stripeDiscountCreateChangeStreamPostStripeDiscountsChangeStream**
> File stripeDiscountCreateChangeStreamPostStripeDiscountsChangeStream(options)

Create a change stream.

### Example
```java
// Import classes:
//import com.harpoon.api.StripeDiscountApi;

StripeDiscountApi apiInstance = new StripeDiscountApi();
String options = "options_example"; // String | 
try {
    File result = apiInstance.stripeDiscountCreateChangeStreamPostStripeDiscountsChangeStream(options);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling StripeDiscountApi#stripeDiscountCreateChangeStreamPostStripeDiscountsChangeStream");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **String**|  | [optional]

### Return type

[**File**](File.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripeDiscountDeleteById"></a>
# **stripeDiscountDeleteById**
> Object stripeDiscountDeleteById(id)

Delete a model instance by {{id}} from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.StripeDiscountApi;

StripeDiscountApi apiInstance = new StripeDiscountApi();
String id = "id_example"; // String | Model id
try {
    Object result = apiInstance.stripeDiscountDeleteById(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling StripeDiscountApi#stripeDiscountDeleteById");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |

### Return type

**Object**

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripeDiscountExistsGetStripeDiscountsidExists"></a>
# **stripeDiscountExistsGetStripeDiscountsidExists**
> InlineResponse2003 stripeDiscountExistsGetStripeDiscountsidExists(id)

Check whether a model instance exists in the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.StripeDiscountApi;

StripeDiscountApi apiInstance = new StripeDiscountApi();
String id = "id_example"; // String | Model id
try {
    InlineResponse2003 result = apiInstance.stripeDiscountExistsGetStripeDiscountsidExists(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling StripeDiscountApi#stripeDiscountExistsGetStripeDiscountsidExists");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |

### Return type

[**InlineResponse2003**](InlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripeDiscountExistsHeadStripeDiscountsid"></a>
# **stripeDiscountExistsHeadStripeDiscountsid**
> InlineResponse2003 stripeDiscountExistsHeadStripeDiscountsid(id)

Check whether a model instance exists in the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.StripeDiscountApi;

StripeDiscountApi apiInstance = new StripeDiscountApi();
String id = "id_example"; // String | Model id
try {
    InlineResponse2003 result = apiInstance.stripeDiscountExistsHeadStripeDiscountsid(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling StripeDiscountApi#stripeDiscountExistsHeadStripeDiscountsid");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |

### Return type

[**InlineResponse2003**](InlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripeDiscountFind"></a>
# **stripeDiscountFind**
> List&lt;StripeDiscount&gt; stripeDiscountFind(filter)

Find all instances of the model matched by filter from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.StripeDiscountApi;

StripeDiscountApi apiInstance = new StripeDiscountApi();
String filter = "filter_example"; // String | Filter defining fields, where, include, order, offset, and limit
try {
    List<StripeDiscount> result = apiInstance.stripeDiscountFind(filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling StripeDiscountApi#stripeDiscountFind");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **String**| Filter defining fields, where, include, order, offset, and limit | [optional]

### Return type

[**List&lt;StripeDiscount&gt;**](StripeDiscount.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripeDiscountFindById"></a>
# **stripeDiscountFindById**
> StripeDiscount stripeDiscountFindById(id, filter)

Find a model instance by {{id}} from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.StripeDiscountApi;

StripeDiscountApi apiInstance = new StripeDiscountApi();
String id = "id_example"; // String | Model id
String filter = "filter_example"; // String | Filter defining fields and include
try {
    StripeDiscount result = apiInstance.stripeDiscountFindById(id, filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling StripeDiscountApi#stripeDiscountFindById");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |
 **filter** | **String**| Filter defining fields and include | [optional]

### Return type

[**StripeDiscount**](StripeDiscount.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripeDiscountFindOne"></a>
# **stripeDiscountFindOne**
> StripeDiscount stripeDiscountFindOne(filter)

Find first instance of the model matched by filter from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.StripeDiscountApi;

StripeDiscountApi apiInstance = new StripeDiscountApi();
String filter = "filter_example"; // String | Filter defining fields, where, include, order, offset, and limit
try {
    StripeDiscount result = apiInstance.stripeDiscountFindOne(filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling StripeDiscountApi#stripeDiscountFindOne");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **String**| Filter defining fields, where, include, order, offset, and limit | [optional]

### Return type

[**StripeDiscount**](StripeDiscount.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripeDiscountPrototypeUpdateAttributesPatchStripeDiscountsid"></a>
# **stripeDiscountPrototypeUpdateAttributesPatchStripeDiscountsid**
> StripeDiscount stripeDiscountPrototypeUpdateAttributesPatchStripeDiscountsid(id, data)

Patch attributes for a model instance and persist it into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.StripeDiscountApi;

StripeDiscountApi apiInstance = new StripeDiscountApi();
String id = "id_example"; // String | StripeDiscount id
StripeDiscount data = new StripeDiscount(); // StripeDiscount | An object of model property name/value pairs
try {
    StripeDiscount result = apiInstance.stripeDiscountPrototypeUpdateAttributesPatchStripeDiscountsid(id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling StripeDiscountApi#stripeDiscountPrototypeUpdateAttributesPatchStripeDiscountsid");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| StripeDiscount id |
 **data** | [**StripeDiscount**](StripeDiscount.md)| An object of model property name/value pairs | [optional]

### Return type

[**StripeDiscount**](StripeDiscount.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripeDiscountPrototypeUpdateAttributesPutStripeDiscountsid"></a>
# **stripeDiscountPrototypeUpdateAttributesPutStripeDiscountsid**
> StripeDiscount stripeDiscountPrototypeUpdateAttributesPutStripeDiscountsid(id, data)

Patch attributes for a model instance and persist it into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.StripeDiscountApi;

StripeDiscountApi apiInstance = new StripeDiscountApi();
String id = "id_example"; // String | StripeDiscount id
StripeDiscount data = new StripeDiscount(); // StripeDiscount | An object of model property name/value pairs
try {
    StripeDiscount result = apiInstance.stripeDiscountPrototypeUpdateAttributesPutStripeDiscountsid(id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling StripeDiscountApi#stripeDiscountPrototypeUpdateAttributesPutStripeDiscountsid");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| StripeDiscount id |
 **data** | [**StripeDiscount**](StripeDiscount.md)| An object of model property name/value pairs | [optional]

### Return type

[**StripeDiscount**](StripeDiscount.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripeDiscountReplaceById"></a>
# **stripeDiscountReplaceById**
> StripeDiscount stripeDiscountReplaceById(id, data)

Replace attributes for a model instance and persist it into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.StripeDiscountApi;

StripeDiscountApi apiInstance = new StripeDiscountApi();
String id = "id_example"; // String | Model id
StripeDiscount data = new StripeDiscount(); // StripeDiscount | Model instance data
try {
    StripeDiscount result = apiInstance.stripeDiscountReplaceById(id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling StripeDiscountApi#stripeDiscountReplaceById");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |
 **data** | [**StripeDiscount**](StripeDiscount.md)| Model instance data | [optional]

### Return type

[**StripeDiscount**](StripeDiscount.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripeDiscountReplaceOrCreate"></a>
# **stripeDiscountReplaceOrCreate**
> StripeDiscount stripeDiscountReplaceOrCreate(data)

Replace an existing model instance or insert a new one into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.StripeDiscountApi;

StripeDiscountApi apiInstance = new StripeDiscountApi();
StripeDiscount data = new StripeDiscount(); // StripeDiscount | Model instance data
try {
    StripeDiscount result = apiInstance.stripeDiscountReplaceOrCreate(data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling StripeDiscountApi#stripeDiscountReplaceOrCreate");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**StripeDiscount**](StripeDiscount.md)| Model instance data | [optional]

### Return type

[**StripeDiscount**](StripeDiscount.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripeDiscountUpdateAll"></a>
# **stripeDiscountUpdateAll**
> InlineResponse2002 stripeDiscountUpdateAll(where, data)

Update instances of the model matched by {{where}} from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.StripeDiscountApi;

StripeDiscountApi apiInstance = new StripeDiscountApi();
String where = "where_example"; // String | Criteria to match model instances
StripeDiscount data = new StripeDiscount(); // StripeDiscount | An object of model property name/value pairs
try {
    InlineResponse2002 result = apiInstance.stripeDiscountUpdateAll(where, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling StripeDiscountApi#stripeDiscountUpdateAll");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional]
 **data** | [**StripeDiscount**](StripeDiscount.md)| An object of model property name/value pairs | [optional]

### Return type

[**InlineResponse2002**](InlineResponse2002.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripeDiscountUpsertPatchStripeDiscounts"></a>
# **stripeDiscountUpsertPatchStripeDiscounts**
> StripeDiscount stripeDiscountUpsertPatchStripeDiscounts(data)

Patch an existing model instance or insert a new one into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.StripeDiscountApi;

StripeDiscountApi apiInstance = new StripeDiscountApi();
StripeDiscount data = new StripeDiscount(); // StripeDiscount | Model instance data
try {
    StripeDiscount result = apiInstance.stripeDiscountUpsertPatchStripeDiscounts(data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling StripeDiscountApi#stripeDiscountUpsertPatchStripeDiscounts");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**StripeDiscount**](StripeDiscount.md)| Model instance data | [optional]

### Return type

[**StripeDiscount**](StripeDiscount.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripeDiscountUpsertPutStripeDiscounts"></a>
# **stripeDiscountUpsertPutStripeDiscounts**
> StripeDiscount stripeDiscountUpsertPutStripeDiscounts(data)

Patch an existing model instance or insert a new one into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.StripeDiscountApi;

StripeDiscountApi apiInstance = new StripeDiscountApi();
StripeDiscount data = new StripeDiscount(); // StripeDiscount | Model instance data
try {
    StripeDiscount result = apiInstance.stripeDiscountUpsertPutStripeDiscounts(data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling StripeDiscountApi#stripeDiscountUpsertPutStripeDiscounts");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**StripeDiscount**](StripeDiscount.md)| Model instance data | [optional]

### Return type

[**StripeDiscount**](StripeDiscount.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripeDiscountUpsertWithWhere"></a>
# **stripeDiscountUpsertWithWhere**
> StripeDiscount stripeDiscountUpsertWithWhere(where, data)

Update an existing model instance or insert a new one into the data source based on the where criteria.

### Example
```java
// Import classes:
//import com.harpoon.api.StripeDiscountApi;

StripeDiscountApi apiInstance = new StripeDiscountApi();
String where = "where_example"; // String | Criteria to match model instances
StripeDiscount data = new StripeDiscount(); // StripeDiscount | An object of model property name/value pairs
try {
    StripeDiscount result = apiInstance.stripeDiscountUpsertWithWhere(where, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling StripeDiscountApi#stripeDiscountUpsertWithWhere");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional]
 **data** | [**StripeDiscount**](StripeDiscount.md)| An object of model property name/value pairs | [optional]

### Return type

[**StripeDiscount**](StripeDiscount.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

