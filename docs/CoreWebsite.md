
# CoreWebsite

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Double** |  | 
**code** | **String** |  |  [optional]
**name** | **String** |  |  [optional]
**sortOrder** | **Double** |  | 
**defaultGroupId** | **Double** |  | 
**isDefault** | **Double** |  |  [optional]



