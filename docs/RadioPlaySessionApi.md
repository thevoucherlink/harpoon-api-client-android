# RadioPlaySessionApi

All URIs are relative to *https://apicdn.harpoonconnect.com/v1.1.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**radioPlaySessionCount**](RadioPlaySessionApi.md#radioPlaySessionCount) | **GET** /RadioPlaySessions/count | Count instances of the model matched by where from the data source.
[**radioPlaySessionCreate**](RadioPlaySessionApi.md#radioPlaySessionCreate) | **POST** /RadioPlaySessions | Create a new instance of the model and persist it into the data source.
[**radioPlaySessionCreateChangeStreamGetRadioPlaySessionsChangeStream**](RadioPlaySessionApi.md#radioPlaySessionCreateChangeStreamGetRadioPlaySessionsChangeStream) | **GET** /RadioPlaySessions/change-stream | Create a change stream.
[**radioPlaySessionCreateChangeStreamPostRadioPlaySessionsChangeStream**](RadioPlaySessionApi.md#radioPlaySessionCreateChangeStreamPostRadioPlaySessionsChangeStream) | **POST** /RadioPlaySessions/change-stream | Create a change stream.
[**radioPlaySessionDeleteById**](RadioPlaySessionApi.md#radioPlaySessionDeleteById) | **DELETE** /RadioPlaySessions/{id} | Delete a model instance by {{id}} from the data source.
[**radioPlaySessionExistsGetRadioPlaySessionsidExists**](RadioPlaySessionApi.md#radioPlaySessionExistsGetRadioPlaySessionsidExists) | **GET** /RadioPlaySessions/{id}/exists | Check whether a model instance exists in the data source.
[**radioPlaySessionExistsHeadRadioPlaySessionsid**](RadioPlaySessionApi.md#radioPlaySessionExistsHeadRadioPlaySessionsid) | **HEAD** /RadioPlaySessions/{id} | Check whether a model instance exists in the data source.
[**radioPlaySessionFind**](RadioPlaySessionApi.md#radioPlaySessionFind) | **GET** /RadioPlaySessions | Find all instances of the model matched by filter from the data source.
[**radioPlaySessionFindById**](RadioPlaySessionApi.md#radioPlaySessionFindById) | **GET** /RadioPlaySessions/{id} | Find a model instance by {{id}} from the data source.
[**radioPlaySessionFindOne**](RadioPlaySessionApi.md#radioPlaySessionFindOne) | **GET** /RadioPlaySessions/findOne | Find first instance of the model matched by filter from the data source.
[**radioPlaySessionPrototypeUpdateAttributesPatchRadioPlaySessionsid**](RadioPlaySessionApi.md#radioPlaySessionPrototypeUpdateAttributesPatchRadioPlaySessionsid) | **PATCH** /RadioPlaySessions/{id} | Patch attributes for a model instance and persist it into the data source.
[**radioPlaySessionPrototypeUpdateAttributesPutRadioPlaySessionsid**](RadioPlaySessionApi.md#radioPlaySessionPrototypeUpdateAttributesPutRadioPlaySessionsid) | **PUT** /RadioPlaySessions/{id} | Patch attributes for a model instance and persist it into the data source.
[**radioPlaySessionReplaceById**](RadioPlaySessionApi.md#radioPlaySessionReplaceById) | **POST** /RadioPlaySessions/{id}/replace | Replace attributes for a model instance and persist it into the data source.
[**radioPlaySessionReplaceOrCreate**](RadioPlaySessionApi.md#radioPlaySessionReplaceOrCreate) | **POST** /RadioPlaySessions/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
[**radioPlaySessionUpdateAll**](RadioPlaySessionApi.md#radioPlaySessionUpdateAll) | **POST** /RadioPlaySessions/update | Update instances of the model matched by {{where}} from the data source.
[**radioPlaySessionUpsertPatchRadioPlaySessions**](RadioPlaySessionApi.md#radioPlaySessionUpsertPatchRadioPlaySessions) | **PATCH** /RadioPlaySessions | Patch an existing model instance or insert a new one into the data source.
[**radioPlaySessionUpsertPutRadioPlaySessions**](RadioPlaySessionApi.md#radioPlaySessionUpsertPutRadioPlaySessions) | **PUT** /RadioPlaySessions | Patch an existing model instance or insert a new one into the data source.
[**radioPlaySessionUpsertWithWhere**](RadioPlaySessionApi.md#radioPlaySessionUpsertWithWhere) | **POST** /RadioPlaySessions/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.


<a name="radioPlaySessionCount"></a>
# **radioPlaySessionCount**
> InlineResponse2001 radioPlaySessionCount(where)

Count instances of the model matched by where from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.RadioPlaySessionApi;

RadioPlaySessionApi apiInstance = new RadioPlaySessionApi();
String where = "where_example"; // String | Criteria to match model instances
try {
    InlineResponse2001 result = apiInstance.radioPlaySessionCount(where);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RadioPlaySessionApi#radioPlaySessionCount");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional]

### Return type

[**InlineResponse2001**](InlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioPlaySessionCreate"></a>
# **radioPlaySessionCreate**
> RadioPlaySession radioPlaySessionCreate(data)

Create a new instance of the model and persist it into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.RadioPlaySessionApi;

RadioPlaySessionApi apiInstance = new RadioPlaySessionApi();
RadioPlaySession data = new RadioPlaySession(); // RadioPlaySession | Model instance data
try {
    RadioPlaySession result = apiInstance.radioPlaySessionCreate(data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RadioPlaySessionApi#radioPlaySessionCreate");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**RadioPlaySession**](RadioPlaySession.md)| Model instance data | [optional]

### Return type

[**RadioPlaySession**](RadioPlaySession.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioPlaySessionCreateChangeStreamGetRadioPlaySessionsChangeStream"></a>
# **radioPlaySessionCreateChangeStreamGetRadioPlaySessionsChangeStream**
> File radioPlaySessionCreateChangeStreamGetRadioPlaySessionsChangeStream(options)

Create a change stream.

### Example
```java
// Import classes:
//import com.harpoon.api.RadioPlaySessionApi;

RadioPlaySessionApi apiInstance = new RadioPlaySessionApi();
String options = "options_example"; // String | 
try {
    File result = apiInstance.radioPlaySessionCreateChangeStreamGetRadioPlaySessionsChangeStream(options);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RadioPlaySessionApi#radioPlaySessionCreateChangeStreamGetRadioPlaySessionsChangeStream");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **String**|  | [optional]

### Return type

[**File**](File.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioPlaySessionCreateChangeStreamPostRadioPlaySessionsChangeStream"></a>
# **radioPlaySessionCreateChangeStreamPostRadioPlaySessionsChangeStream**
> File radioPlaySessionCreateChangeStreamPostRadioPlaySessionsChangeStream(options)

Create a change stream.

### Example
```java
// Import classes:
//import com.harpoon.api.RadioPlaySessionApi;

RadioPlaySessionApi apiInstance = new RadioPlaySessionApi();
String options = "options_example"; // String | 
try {
    File result = apiInstance.radioPlaySessionCreateChangeStreamPostRadioPlaySessionsChangeStream(options);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RadioPlaySessionApi#radioPlaySessionCreateChangeStreamPostRadioPlaySessionsChangeStream");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **String**|  | [optional]

### Return type

[**File**](File.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioPlaySessionDeleteById"></a>
# **radioPlaySessionDeleteById**
> Object radioPlaySessionDeleteById(id)

Delete a model instance by {{id}} from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.RadioPlaySessionApi;

RadioPlaySessionApi apiInstance = new RadioPlaySessionApi();
String id = "id_example"; // String | Model id
try {
    Object result = apiInstance.radioPlaySessionDeleteById(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RadioPlaySessionApi#radioPlaySessionDeleteById");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |

### Return type

**Object**

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioPlaySessionExistsGetRadioPlaySessionsidExists"></a>
# **radioPlaySessionExistsGetRadioPlaySessionsidExists**
> InlineResponse2003 radioPlaySessionExistsGetRadioPlaySessionsidExists(id)

Check whether a model instance exists in the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.RadioPlaySessionApi;

RadioPlaySessionApi apiInstance = new RadioPlaySessionApi();
String id = "id_example"; // String | Model id
try {
    InlineResponse2003 result = apiInstance.radioPlaySessionExistsGetRadioPlaySessionsidExists(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RadioPlaySessionApi#radioPlaySessionExistsGetRadioPlaySessionsidExists");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |

### Return type

[**InlineResponse2003**](InlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioPlaySessionExistsHeadRadioPlaySessionsid"></a>
# **radioPlaySessionExistsHeadRadioPlaySessionsid**
> InlineResponse2003 radioPlaySessionExistsHeadRadioPlaySessionsid(id)

Check whether a model instance exists in the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.RadioPlaySessionApi;

RadioPlaySessionApi apiInstance = new RadioPlaySessionApi();
String id = "id_example"; // String | Model id
try {
    InlineResponse2003 result = apiInstance.radioPlaySessionExistsHeadRadioPlaySessionsid(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RadioPlaySessionApi#radioPlaySessionExistsHeadRadioPlaySessionsid");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |

### Return type

[**InlineResponse2003**](InlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioPlaySessionFind"></a>
# **radioPlaySessionFind**
> List&lt;RadioPlaySession&gt; radioPlaySessionFind(filter)

Find all instances of the model matched by filter from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.RadioPlaySessionApi;

RadioPlaySessionApi apiInstance = new RadioPlaySessionApi();
String filter = "filter_example"; // String | Filter defining fields, where, include, order, offset, and limit
try {
    List<RadioPlaySession> result = apiInstance.radioPlaySessionFind(filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RadioPlaySessionApi#radioPlaySessionFind");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **String**| Filter defining fields, where, include, order, offset, and limit | [optional]

### Return type

[**List&lt;RadioPlaySession&gt;**](RadioPlaySession.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioPlaySessionFindById"></a>
# **radioPlaySessionFindById**
> RadioPlaySession radioPlaySessionFindById(id, filter)

Find a model instance by {{id}} from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.RadioPlaySessionApi;

RadioPlaySessionApi apiInstance = new RadioPlaySessionApi();
String id = "id_example"; // String | Model id
String filter = "filter_example"; // String | Filter defining fields and include
try {
    RadioPlaySession result = apiInstance.radioPlaySessionFindById(id, filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RadioPlaySessionApi#radioPlaySessionFindById");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |
 **filter** | **String**| Filter defining fields and include | [optional]

### Return type

[**RadioPlaySession**](RadioPlaySession.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioPlaySessionFindOne"></a>
# **radioPlaySessionFindOne**
> RadioPlaySession radioPlaySessionFindOne(filter)

Find first instance of the model matched by filter from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.RadioPlaySessionApi;

RadioPlaySessionApi apiInstance = new RadioPlaySessionApi();
String filter = "filter_example"; // String | Filter defining fields, where, include, order, offset, and limit
try {
    RadioPlaySession result = apiInstance.radioPlaySessionFindOne(filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RadioPlaySessionApi#radioPlaySessionFindOne");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **String**| Filter defining fields, where, include, order, offset, and limit | [optional]

### Return type

[**RadioPlaySession**](RadioPlaySession.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioPlaySessionPrototypeUpdateAttributesPatchRadioPlaySessionsid"></a>
# **radioPlaySessionPrototypeUpdateAttributesPatchRadioPlaySessionsid**
> RadioPlaySession radioPlaySessionPrototypeUpdateAttributesPatchRadioPlaySessionsid(id, data)

Patch attributes for a model instance and persist it into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.RadioPlaySessionApi;

RadioPlaySessionApi apiInstance = new RadioPlaySessionApi();
String id = "id_example"; // String | RadioPlaySession id
RadioPlaySession data = new RadioPlaySession(); // RadioPlaySession | An object of model property name/value pairs
try {
    RadioPlaySession result = apiInstance.radioPlaySessionPrototypeUpdateAttributesPatchRadioPlaySessionsid(id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RadioPlaySessionApi#radioPlaySessionPrototypeUpdateAttributesPatchRadioPlaySessionsid");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| RadioPlaySession id |
 **data** | [**RadioPlaySession**](RadioPlaySession.md)| An object of model property name/value pairs | [optional]

### Return type

[**RadioPlaySession**](RadioPlaySession.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioPlaySessionPrototypeUpdateAttributesPutRadioPlaySessionsid"></a>
# **radioPlaySessionPrototypeUpdateAttributesPutRadioPlaySessionsid**
> RadioPlaySession radioPlaySessionPrototypeUpdateAttributesPutRadioPlaySessionsid(id, data)

Patch attributes for a model instance and persist it into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.RadioPlaySessionApi;

RadioPlaySessionApi apiInstance = new RadioPlaySessionApi();
String id = "id_example"; // String | RadioPlaySession id
RadioPlaySession data = new RadioPlaySession(); // RadioPlaySession | An object of model property name/value pairs
try {
    RadioPlaySession result = apiInstance.radioPlaySessionPrototypeUpdateAttributesPutRadioPlaySessionsid(id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RadioPlaySessionApi#radioPlaySessionPrototypeUpdateAttributesPutRadioPlaySessionsid");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| RadioPlaySession id |
 **data** | [**RadioPlaySession**](RadioPlaySession.md)| An object of model property name/value pairs | [optional]

### Return type

[**RadioPlaySession**](RadioPlaySession.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioPlaySessionReplaceById"></a>
# **radioPlaySessionReplaceById**
> RadioPlaySession radioPlaySessionReplaceById(id, data)

Replace attributes for a model instance and persist it into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.RadioPlaySessionApi;

RadioPlaySessionApi apiInstance = new RadioPlaySessionApi();
String id = "id_example"; // String | Model id
RadioPlaySession data = new RadioPlaySession(); // RadioPlaySession | Model instance data
try {
    RadioPlaySession result = apiInstance.radioPlaySessionReplaceById(id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RadioPlaySessionApi#radioPlaySessionReplaceById");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |
 **data** | [**RadioPlaySession**](RadioPlaySession.md)| Model instance data | [optional]

### Return type

[**RadioPlaySession**](RadioPlaySession.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioPlaySessionReplaceOrCreate"></a>
# **radioPlaySessionReplaceOrCreate**
> RadioPlaySession radioPlaySessionReplaceOrCreate(data)

Replace an existing model instance or insert a new one into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.RadioPlaySessionApi;

RadioPlaySessionApi apiInstance = new RadioPlaySessionApi();
RadioPlaySession data = new RadioPlaySession(); // RadioPlaySession | Model instance data
try {
    RadioPlaySession result = apiInstance.radioPlaySessionReplaceOrCreate(data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RadioPlaySessionApi#radioPlaySessionReplaceOrCreate");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**RadioPlaySession**](RadioPlaySession.md)| Model instance data | [optional]

### Return type

[**RadioPlaySession**](RadioPlaySession.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioPlaySessionUpdateAll"></a>
# **radioPlaySessionUpdateAll**
> InlineResponse2002 radioPlaySessionUpdateAll(where, data)

Update instances of the model matched by {{where}} from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.RadioPlaySessionApi;

RadioPlaySessionApi apiInstance = new RadioPlaySessionApi();
String where = "where_example"; // String | Criteria to match model instances
RadioPlaySession data = new RadioPlaySession(); // RadioPlaySession | An object of model property name/value pairs
try {
    InlineResponse2002 result = apiInstance.radioPlaySessionUpdateAll(where, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RadioPlaySessionApi#radioPlaySessionUpdateAll");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional]
 **data** | [**RadioPlaySession**](RadioPlaySession.md)| An object of model property name/value pairs | [optional]

### Return type

[**InlineResponse2002**](InlineResponse2002.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioPlaySessionUpsertPatchRadioPlaySessions"></a>
# **radioPlaySessionUpsertPatchRadioPlaySessions**
> RadioPlaySession radioPlaySessionUpsertPatchRadioPlaySessions(data)

Patch an existing model instance or insert a new one into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.RadioPlaySessionApi;

RadioPlaySessionApi apiInstance = new RadioPlaySessionApi();
RadioPlaySession data = new RadioPlaySession(); // RadioPlaySession | Model instance data
try {
    RadioPlaySession result = apiInstance.radioPlaySessionUpsertPatchRadioPlaySessions(data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RadioPlaySessionApi#radioPlaySessionUpsertPatchRadioPlaySessions");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**RadioPlaySession**](RadioPlaySession.md)| Model instance data | [optional]

### Return type

[**RadioPlaySession**](RadioPlaySession.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioPlaySessionUpsertPutRadioPlaySessions"></a>
# **radioPlaySessionUpsertPutRadioPlaySessions**
> RadioPlaySession radioPlaySessionUpsertPutRadioPlaySessions(data)

Patch an existing model instance or insert a new one into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.RadioPlaySessionApi;

RadioPlaySessionApi apiInstance = new RadioPlaySessionApi();
RadioPlaySession data = new RadioPlaySession(); // RadioPlaySession | Model instance data
try {
    RadioPlaySession result = apiInstance.radioPlaySessionUpsertPutRadioPlaySessions(data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RadioPlaySessionApi#radioPlaySessionUpsertPutRadioPlaySessions");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**RadioPlaySession**](RadioPlaySession.md)| Model instance data | [optional]

### Return type

[**RadioPlaySession**](RadioPlaySession.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioPlaySessionUpsertWithWhere"></a>
# **radioPlaySessionUpsertWithWhere**
> RadioPlaySession radioPlaySessionUpsertWithWhere(where, data)

Update an existing model instance or insert a new one into the data source based on the where criteria.

### Example
```java
// Import classes:
//import com.harpoon.api.RadioPlaySessionApi;

RadioPlaySessionApi apiInstance = new RadioPlaySessionApi();
String where = "where_example"; // String | Criteria to match model instances
RadioPlaySession data = new RadioPlaySession(); // RadioPlaySession | An object of model property name/value pairs
try {
    RadioPlaySession result = apiInstance.radioPlaySessionUpsertWithWhere(where, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RadioPlaySessionApi#radioPlaySessionUpsertWithWhere");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional]
 **data** | [**RadioPlaySession**](RadioPlaySession.md)| An object of model property name/value pairs | [optional]

### Return type

[**RadioPlaySession**](RadioPlaySession.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

