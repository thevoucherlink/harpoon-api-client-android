
# Coupon

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | [**Category**](Category.md) |  |  [optional]
**price** | **Double** |  |  [optional]
**hasClaimed** | **Boolean** |  |  [optional]
**qtyLeft** | **Double** |  |  [optional]
**qtyTotal** | **Double** |  |  [optional]
**qtyClaimed** | **Double** |  |  [optional]
**name** | **String** |  |  [optional]
**description** | **String** |  |  [optional]
**cover** | **String** |  |  [optional]
**campaignType** | [**Category**](Category.md) |  |  [optional]
**category** | [**Category**](Category.md) |  |  [optional]
**topic** | [**Category**](Category.md) |  |  [optional]
**alias** | **String** |  |  [optional]
**from** | [**Date**](Date.md) |  |  [optional]
**to** | [**Date**](Date.md) |  |  [optional]
**baseCurrency** | **String** |  |  [optional]
**priceText** | **String** |  |  [optional]
**bannerText** | **String** |  |  [optional]
**checkoutLink** | **String** |  |  [optional]
**nearestVenue** | [**Venue**](Venue.md) |  |  [optional]
**actionText** | **String** |  |  [optional]
**status** | **String** |  |  [optional]
**collectionNotes** | **String** |  |  [optional]
**termsConditions** | **String** |  |  [optional]
**locationLink** | **String** |  |  [optional]
**altLink** | **String** |  |  [optional]
**redemptionType** | **String** |  |  [optional]
**brand** | [**Brand**](Brand.md) |  |  [optional]
**closestPurchase** | [**OfferClosestPurchase**](OfferClosestPurchase.md) |  |  [optional]
**isFeatured** | **Boolean** |  |  [optional]
**qtyPerOrder** | **Double** |  |  [optional]
**shareLink** | **String** |  |  [optional]
**id** | **Double** |  |  [optional]



