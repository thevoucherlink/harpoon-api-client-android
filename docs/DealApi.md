# DealApi

All URIs are relative to *https://apicdn.harpoonconnect.com/v1.1.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**dealFind**](DealApi.md#dealFind) | **GET** /Deals | Find all instances of the model matched by filter from the data source.
[**dealFindById**](DealApi.md#dealFindById) | **GET** /Deals/{id} | Find a model instance by {{id}} from the data source.
[**dealFindOne**](DealApi.md#dealFindOne) | **GET** /Deals/findOne | Find first instance of the model matched by filter from the data source.
[**dealReplaceById**](DealApi.md#dealReplaceById) | **POST** /Deals/{id}/replace | Replace attributes for a model instance and persist it into the data source.
[**dealReplaceOrCreate**](DealApi.md#dealReplaceOrCreate) | **POST** /Deals/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
[**dealUpsertWithWhere**](DealApi.md#dealUpsertWithWhere) | **POST** /Deals/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.


<a name="dealFind"></a>
# **dealFind**
> List&lt;Deal&gt; dealFind(filter)

Find all instances of the model matched by filter from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.DealApi;

DealApi apiInstance = new DealApi();
String filter = "filter_example"; // String | Filter defining fields, where, include, order, offset, and limit
try {
    List<Deal> result = apiInstance.dealFind(filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DealApi#dealFind");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **String**| Filter defining fields, where, include, order, offset, and limit | [optional]

### Return type

[**List&lt;Deal&gt;**](Deal.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="dealFindById"></a>
# **dealFindById**
> Deal dealFindById(id, filter)

Find a model instance by {{id}} from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.DealApi;

DealApi apiInstance = new DealApi();
String id = "id_example"; // String | Model id
String filter = "filter_example"; // String | Filter defining fields and include
try {
    Deal result = apiInstance.dealFindById(id, filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DealApi#dealFindById");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |
 **filter** | **String**| Filter defining fields and include | [optional]

### Return type

[**Deal**](Deal.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="dealFindOne"></a>
# **dealFindOne**
> Deal dealFindOne(filter)

Find first instance of the model matched by filter from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.DealApi;

DealApi apiInstance = new DealApi();
String filter = "filter_example"; // String | Filter defining fields, where, include, order, offset, and limit
try {
    Deal result = apiInstance.dealFindOne(filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DealApi#dealFindOne");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **String**| Filter defining fields, where, include, order, offset, and limit | [optional]

### Return type

[**Deal**](Deal.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="dealReplaceById"></a>
# **dealReplaceById**
> Deal dealReplaceById(id, data)

Replace attributes for a model instance and persist it into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.DealApi;

DealApi apiInstance = new DealApi();
String id = "id_example"; // String | Model id
Deal data = new Deal(); // Deal | Model instance data
try {
    Deal result = apiInstance.dealReplaceById(id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DealApi#dealReplaceById");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |
 **data** | [**Deal**](Deal.md)| Model instance data | [optional]

### Return type

[**Deal**](Deal.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="dealReplaceOrCreate"></a>
# **dealReplaceOrCreate**
> Deal dealReplaceOrCreate(data)

Replace an existing model instance or insert a new one into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.DealApi;

DealApi apiInstance = new DealApi();
Deal data = new Deal(); // Deal | Model instance data
try {
    Deal result = apiInstance.dealReplaceOrCreate(data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DealApi#dealReplaceOrCreate");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**Deal**](Deal.md)| Model instance data | [optional]

### Return type

[**Deal**](Deal.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="dealUpsertWithWhere"></a>
# **dealUpsertWithWhere**
> Deal dealUpsertWithWhere(where, data)

Update an existing model instance or insert a new one into the data source based on the where criteria.

### Example
```java
// Import classes:
//import com.harpoon.api.DealApi;

DealApi apiInstance = new DealApi();
String where = "where_example"; // String | Criteria to match model instances
Deal data = new Deal(); // Deal | An object of model property name/value pairs
try {
    Deal result = apiInstance.dealUpsertWithWhere(where, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DealApi#dealUpsertWithWhere");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional]
 **data** | [**Deal**](Deal.md)| An object of model property name/value pairs | [optional]

### Return type

[**Deal**](Deal.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

