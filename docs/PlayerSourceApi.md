# PlayerSourceApi

All URIs are relative to *https://apicdn.harpoonconnect.com/v1.1.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**playerSourceCount**](PlayerSourceApi.md#playerSourceCount) | **GET** /PlayerSources/count | Count instances of the model matched by where from the data source.
[**playerSourceCreate**](PlayerSourceApi.md#playerSourceCreate) | **POST** /PlayerSources | Create a new instance of the model and persist it into the data source.
[**playerSourceCreateChangeStreamGetPlayerSourcesChangeStream**](PlayerSourceApi.md#playerSourceCreateChangeStreamGetPlayerSourcesChangeStream) | **GET** /PlayerSources/change-stream | Create a change stream.
[**playerSourceCreateChangeStreamPostPlayerSourcesChangeStream**](PlayerSourceApi.md#playerSourceCreateChangeStreamPostPlayerSourcesChangeStream) | **POST** /PlayerSources/change-stream | Create a change stream.
[**playerSourceDeleteById**](PlayerSourceApi.md#playerSourceDeleteById) | **DELETE** /PlayerSources/{id} | Delete a model instance by {{id}} from the data source.
[**playerSourceExistsGetPlayerSourcesidExists**](PlayerSourceApi.md#playerSourceExistsGetPlayerSourcesidExists) | **GET** /PlayerSources/{id}/exists | Check whether a model instance exists in the data source.
[**playerSourceExistsHeadPlayerSourcesid**](PlayerSourceApi.md#playerSourceExistsHeadPlayerSourcesid) | **HEAD** /PlayerSources/{id} | Check whether a model instance exists in the data source.
[**playerSourceFind**](PlayerSourceApi.md#playerSourceFind) | **GET** /PlayerSources | Find all instances of the model matched by filter from the data source.
[**playerSourceFindById**](PlayerSourceApi.md#playerSourceFindById) | **GET** /PlayerSources/{id} | Find a model instance by {{id}} from the data source.
[**playerSourceFindOne**](PlayerSourceApi.md#playerSourceFindOne) | **GET** /PlayerSources/findOne | Find first instance of the model matched by filter from the data source.
[**playerSourcePrototypeGetPlaylistItem**](PlayerSourceApi.md#playerSourcePrototypeGetPlaylistItem) | **GET** /PlayerSources/{id}/playlistItem | Fetches belongsTo relation playlistItem.
[**playerSourcePrototypeUpdateAttributesPatchPlayerSourcesid**](PlayerSourceApi.md#playerSourcePrototypeUpdateAttributesPatchPlayerSourcesid) | **PATCH** /PlayerSources/{id} | Patch attributes for a model instance and persist it into the data source.
[**playerSourcePrototypeUpdateAttributesPutPlayerSourcesid**](PlayerSourceApi.md#playerSourcePrototypeUpdateAttributesPutPlayerSourcesid) | **PUT** /PlayerSources/{id} | Patch attributes for a model instance and persist it into the data source.
[**playerSourceReplaceById**](PlayerSourceApi.md#playerSourceReplaceById) | **POST** /PlayerSources/{id}/replace | Replace attributes for a model instance and persist it into the data source.
[**playerSourceReplaceOrCreate**](PlayerSourceApi.md#playerSourceReplaceOrCreate) | **POST** /PlayerSources/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
[**playerSourceUpdateAll**](PlayerSourceApi.md#playerSourceUpdateAll) | **POST** /PlayerSources/update | Update instances of the model matched by {{where}} from the data source.
[**playerSourceUpsertPatchPlayerSources**](PlayerSourceApi.md#playerSourceUpsertPatchPlayerSources) | **PATCH** /PlayerSources | Patch an existing model instance or insert a new one into the data source.
[**playerSourceUpsertPutPlayerSources**](PlayerSourceApi.md#playerSourceUpsertPutPlayerSources) | **PUT** /PlayerSources | Patch an existing model instance or insert a new one into the data source.
[**playerSourceUpsertWithWhere**](PlayerSourceApi.md#playerSourceUpsertWithWhere) | **POST** /PlayerSources/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.


<a name="playerSourceCount"></a>
# **playerSourceCount**
> InlineResponse2001 playerSourceCount(where)

Count instances of the model matched by where from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.PlayerSourceApi;

PlayerSourceApi apiInstance = new PlayerSourceApi();
String where = "where_example"; // String | Criteria to match model instances
try {
    InlineResponse2001 result = apiInstance.playerSourceCount(where);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PlayerSourceApi#playerSourceCount");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional]

### Return type

[**InlineResponse2001**](InlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playerSourceCreate"></a>
# **playerSourceCreate**
> PlayerSource playerSourceCreate(data)

Create a new instance of the model and persist it into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.PlayerSourceApi;

PlayerSourceApi apiInstance = new PlayerSourceApi();
PlayerSource data = new PlayerSource(); // PlayerSource | Model instance data
try {
    PlayerSource result = apiInstance.playerSourceCreate(data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PlayerSourceApi#playerSourceCreate");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**PlayerSource**](PlayerSource.md)| Model instance data | [optional]

### Return type

[**PlayerSource**](PlayerSource.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playerSourceCreateChangeStreamGetPlayerSourcesChangeStream"></a>
# **playerSourceCreateChangeStreamGetPlayerSourcesChangeStream**
> File playerSourceCreateChangeStreamGetPlayerSourcesChangeStream(options)

Create a change stream.

### Example
```java
// Import classes:
//import com.harpoon.api.PlayerSourceApi;

PlayerSourceApi apiInstance = new PlayerSourceApi();
String options = "options_example"; // String | 
try {
    File result = apiInstance.playerSourceCreateChangeStreamGetPlayerSourcesChangeStream(options);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PlayerSourceApi#playerSourceCreateChangeStreamGetPlayerSourcesChangeStream");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **String**|  | [optional]

### Return type

[**File**](File.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playerSourceCreateChangeStreamPostPlayerSourcesChangeStream"></a>
# **playerSourceCreateChangeStreamPostPlayerSourcesChangeStream**
> File playerSourceCreateChangeStreamPostPlayerSourcesChangeStream(options)

Create a change stream.

### Example
```java
// Import classes:
//import com.harpoon.api.PlayerSourceApi;

PlayerSourceApi apiInstance = new PlayerSourceApi();
String options = "options_example"; // String | 
try {
    File result = apiInstance.playerSourceCreateChangeStreamPostPlayerSourcesChangeStream(options);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PlayerSourceApi#playerSourceCreateChangeStreamPostPlayerSourcesChangeStream");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **String**|  | [optional]

### Return type

[**File**](File.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playerSourceDeleteById"></a>
# **playerSourceDeleteById**
> Object playerSourceDeleteById(id)

Delete a model instance by {{id}} from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.PlayerSourceApi;

PlayerSourceApi apiInstance = new PlayerSourceApi();
String id = "id_example"; // String | Model id
try {
    Object result = apiInstance.playerSourceDeleteById(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PlayerSourceApi#playerSourceDeleteById");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |

### Return type

**Object**

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playerSourceExistsGetPlayerSourcesidExists"></a>
# **playerSourceExistsGetPlayerSourcesidExists**
> InlineResponse2003 playerSourceExistsGetPlayerSourcesidExists(id)

Check whether a model instance exists in the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.PlayerSourceApi;

PlayerSourceApi apiInstance = new PlayerSourceApi();
String id = "id_example"; // String | Model id
try {
    InlineResponse2003 result = apiInstance.playerSourceExistsGetPlayerSourcesidExists(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PlayerSourceApi#playerSourceExistsGetPlayerSourcesidExists");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |

### Return type

[**InlineResponse2003**](InlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playerSourceExistsHeadPlayerSourcesid"></a>
# **playerSourceExistsHeadPlayerSourcesid**
> InlineResponse2003 playerSourceExistsHeadPlayerSourcesid(id)

Check whether a model instance exists in the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.PlayerSourceApi;

PlayerSourceApi apiInstance = new PlayerSourceApi();
String id = "id_example"; // String | Model id
try {
    InlineResponse2003 result = apiInstance.playerSourceExistsHeadPlayerSourcesid(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PlayerSourceApi#playerSourceExistsHeadPlayerSourcesid");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |

### Return type

[**InlineResponse2003**](InlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playerSourceFind"></a>
# **playerSourceFind**
> List&lt;PlayerSource&gt; playerSourceFind(filter)

Find all instances of the model matched by filter from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.PlayerSourceApi;

PlayerSourceApi apiInstance = new PlayerSourceApi();
String filter = "filter_example"; // String | Filter defining fields, where, include, order, offset, and limit
try {
    List<PlayerSource> result = apiInstance.playerSourceFind(filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PlayerSourceApi#playerSourceFind");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **String**| Filter defining fields, where, include, order, offset, and limit | [optional]

### Return type

[**List&lt;PlayerSource&gt;**](PlayerSource.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playerSourceFindById"></a>
# **playerSourceFindById**
> PlayerSource playerSourceFindById(id, filter)

Find a model instance by {{id}} from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.PlayerSourceApi;

PlayerSourceApi apiInstance = new PlayerSourceApi();
String id = "id_example"; // String | Model id
String filter = "filter_example"; // String | Filter defining fields and include
try {
    PlayerSource result = apiInstance.playerSourceFindById(id, filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PlayerSourceApi#playerSourceFindById");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |
 **filter** | **String**| Filter defining fields and include | [optional]

### Return type

[**PlayerSource**](PlayerSource.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playerSourceFindOne"></a>
# **playerSourceFindOne**
> PlayerSource playerSourceFindOne(filter)

Find first instance of the model matched by filter from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.PlayerSourceApi;

PlayerSourceApi apiInstance = new PlayerSourceApi();
String filter = "filter_example"; // String | Filter defining fields, where, include, order, offset, and limit
try {
    PlayerSource result = apiInstance.playerSourceFindOne(filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PlayerSourceApi#playerSourceFindOne");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **String**| Filter defining fields, where, include, order, offset, and limit | [optional]

### Return type

[**PlayerSource**](PlayerSource.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playerSourcePrototypeGetPlaylistItem"></a>
# **playerSourcePrototypeGetPlaylistItem**
> PlaylistItem playerSourcePrototypeGetPlaylistItem(id, refresh)

Fetches belongsTo relation playlistItem.

### Example
```java
// Import classes:
//import com.harpoon.api.PlayerSourceApi;

PlayerSourceApi apiInstance = new PlayerSourceApi();
String id = "id_example"; // String | PlayerSource id
Boolean refresh = true; // Boolean | 
try {
    PlaylistItem result = apiInstance.playerSourcePrototypeGetPlaylistItem(id, refresh);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PlayerSourceApi#playerSourcePrototypeGetPlaylistItem");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| PlayerSource id |
 **refresh** | **Boolean**|  | [optional]

### Return type

[**PlaylistItem**](PlaylistItem.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playerSourcePrototypeUpdateAttributesPatchPlayerSourcesid"></a>
# **playerSourcePrototypeUpdateAttributesPatchPlayerSourcesid**
> PlayerSource playerSourcePrototypeUpdateAttributesPatchPlayerSourcesid(id, data)

Patch attributes for a model instance and persist it into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.PlayerSourceApi;

PlayerSourceApi apiInstance = new PlayerSourceApi();
String id = "id_example"; // String | PlayerSource id
PlayerSource data = new PlayerSource(); // PlayerSource | An object of model property name/value pairs
try {
    PlayerSource result = apiInstance.playerSourcePrototypeUpdateAttributesPatchPlayerSourcesid(id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PlayerSourceApi#playerSourcePrototypeUpdateAttributesPatchPlayerSourcesid");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| PlayerSource id |
 **data** | [**PlayerSource**](PlayerSource.md)| An object of model property name/value pairs | [optional]

### Return type

[**PlayerSource**](PlayerSource.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playerSourcePrototypeUpdateAttributesPutPlayerSourcesid"></a>
# **playerSourcePrototypeUpdateAttributesPutPlayerSourcesid**
> PlayerSource playerSourcePrototypeUpdateAttributesPutPlayerSourcesid(id, data)

Patch attributes for a model instance and persist it into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.PlayerSourceApi;

PlayerSourceApi apiInstance = new PlayerSourceApi();
String id = "id_example"; // String | PlayerSource id
PlayerSource data = new PlayerSource(); // PlayerSource | An object of model property name/value pairs
try {
    PlayerSource result = apiInstance.playerSourcePrototypeUpdateAttributesPutPlayerSourcesid(id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PlayerSourceApi#playerSourcePrototypeUpdateAttributesPutPlayerSourcesid");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| PlayerSource id |
 **data** | [**PlayerSource**](PlayerSource.md)| An object of model property name/value pairs | [optional]

### Return type

[**PlayerSource**](PlayerSource.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playerSourceReplaceById"></a>
# **playerSourceReplaceById**
> PlayerSource playerSourceReplaceById(id, data)

Replace attributes for a model instance and persist it into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.PlayerSourceApi;

PlayerSourceApi apiInstance = new PlayerSourceApi();
String id = "id_example"; // String | Model id
PlayerSource data = new PlayerSource(); // PlayerSource | Model instance data
try {
    PlayerSource result = apiInstance.playerSourceReplaceById(id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PlayerSourceApi#playerSourceReplaceById");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |
 **data** | [**PlayerSource**](PlayerSource.md)| Model instance data | [optional]

### Return type

[**PlayerSource**](PlayerSource.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playerSourceReplaceOrCreate"></a>
# **playerSourceReplaceOrCreate**
> PlayerSource playerSourceReplaceOrCreate(data)

Replace an existing model instance or insert a new one into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.PlayerSourceApi;

PlayerSourceApi apiInstance = new PlayerSourceApi();
PlayerSource data = new PlayerSource(); // PlayerSource | Model instance data
try {
    PlayerSource result = apiInstance.playerSourceReplaceOrCreate(data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PlayerSourceApi#playerSourceReplaceOrCreate");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**PlayerSource**](PlayerSource.md)| Model instance data | [optional]

### Return type

[**PlayerSource**](PlayerSource.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playerSourceUpdateAll"></a>
# **playerSourceUpdateAll**
> InlineResponse2002 playerSourceUpdateAll(where, data)

Update instances of the model matched by {{where}} from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.PlayerSourceApi;

PlayerSourceApi apiInstance = new PlayerSourceApi();
String where = "where_example"; // String | Criteria to match model instances
PlayerSource data = new PlayerSource(); // PlayerSource | An object of model property name/value pairs
try {
    InlineResponse2002 result = apiInstance.playerSourceUpdateAll(where, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PlayerSourceApi#playerSourceUpdateAll");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional]
 **data** | [**PlayerSource**](PlayerSource.md)| An object of model property name/value pairs | [optional]

### Return type

[**InlineResponse2002**](InlineResponse2002.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playerSourceUpsertPatchPlayerSources"></a>
# **playerSourceUpsertPatchPlayerSources**
> PlayerSource playerSourceUpsertPatchPlayerSources(data)

Patch an existing model instance or insert a new one into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.PlayerSourceApi;

PlayerSourceApi apiInstance = new PlayerSourceApi();
PlayerSource data = new PlayerSource(); // PlayerSource | Model instance data
try {
    PlayerSource result = apiInstance.playerSourceUpsertPatchPlayerSources(data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PlayerSourceApi#playerSourceUpsertPatchPlayerSources");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**PlayerSource**](PlayerSource.md)| Model instance data | [optional]

### Return type

[**PlayerSource**](PlayerSource.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playerSourceUpsertPutPlayerSources"></a>
# **playerSourceUpsertPutPlayerSources**
> PlayerSource playerSourceUpsertPutPlayerSources(data)

Patch an existing model instance or insert a new one into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.PlayerSourceApi;

PlayerSourceApi apiInstance = new PlayerSourceApi();
PlayerSource data = new PlayerSource(); // PlayerSource | Model instance data
try {
    PlayerSource result = apiInstance.playerSourceUpsertPutPlayerSources(data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PlayerSourceApi#playerSourceUpsertPutPlayerSources");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**PlayerSource**](PlayerSource.md)| Model instance data | [optional]

### Return type

[**PlayerSource**](PlayerSource.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playerSourceUpsertWithWhere"></a>
# **playerSourceUpsertWithWhere**
> PlayerSource playerSourceUpsertWithWhere(where, data)

Update an existing model instance or insert a new one into the data source based on the where criteria.

### Example
```java
// Import classes:
//import com.harpoon.api.PlayerSourceApi;

PlayerSourceApi apiInstance = new PlayerSourceApi();
String where = "where_example"; // String | Criteria to match model instances
PlayerSource data = new PlayerSource(); // PlayerSource | An object of model property name/value pairs
try {
    PlayerSource result = apiInstance.playerSourceUpsertWithWhere(where, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PlayerSourceApi#playerSourceUpsertWithWhere");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional]
 **data** | [**PlayerSource**](PlayerSource.md)| An object of model property name/value pairs | [optional]

### Return type

[**PlayerSource**](PlayerSource.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

