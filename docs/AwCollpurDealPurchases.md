
# AwCollpurDealPurchases

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Double** |  | 
**dealId** | **Double** |  | 
**orderId** | **Double** |  | 
**orderItemId** | **Double** |  | 
**qtyPurchased** | **Double** |  | 
**qtyWithCoupons** | **Double** |  | 
**customerName** | **String** |  | 
**customerId** | **Double** |  | 
**purchaseDateTime** | [**Date**](Date.md) |  |  [optional]
**shippingAmount** | **String** |  | 
**qtyOrdered** | **String** |  | 
**refundState** | **Double** |  |  [optional]
**isSuccessedFlag** | **Double** |  |  [optional]
**awCollpurCoupon** | **Object** |  |  [optional]



