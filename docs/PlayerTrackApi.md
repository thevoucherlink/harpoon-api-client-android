# PlayerTrackApi

All URIs are relative to *https://apicdn.harpoonconnect.com/v1.1.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**playerTrackCount**](PlayerTrackApi.md#playerTrackCount) | **GET** /PlayerTracks/count | Count instances of the model matched by where from the data source.
[**playerTrackCreate**](PlayerTrackApi.md#playerTrackCreate) | **POST** /PlayerTracks | Create a new instance of the model and persist it into the data source.
[**playerTrackCreateChangeStreamGetPlayerTracksChangeStream**](PlayerTrackApi.md#playerTrackCreateChangeStreamGetPlayerTracksChangeStream) | **GET** /PlayerTracks/change-stream | Create a change stream.
[**playerTrackCreateChangeStreamPostPlayerTracksChangeStream**](PlayerTrackApi.md#playerTrackCreateChangeStreamPostPlayerTracksChangeStream) | **POST** /PlayerTracks/change-stream | Create a change stream.
[**playerTrackDeleteById**](PlayerTrackApi.md#playerTrackDeleteById) | **DELETE** /PlayerTracks/{id} | Delete a model instance by {{id}} from the data source.
[**playerTrackExistsGetPlayerTracksidExists**](PlayerTrackApi.md#playerTrackExistsGetPlayerTracksidExists) | **GET** /PlayerTracks/{id}/exists | Check whether a model instance exists in the data source.
[**playerTrackExistsHeadPlayerTracksid**](PlayerTrackApi.md#playerTrackExistsHeadPlayerTracksid) | **HEAD** /PlayerTracks/{id} | Check whether a model instance exists in the data source.
[**playerTrackFind**](PlayerTrackApi.md#playerTrackFind) | **GET** /PlayerTracks | Find all instances of the model matched by filter from the data source.
[**playerTrackFindById**](PlayerTrackApi.md#playerTrackFindById) | **GET** /PlayerTracks/{id} | Find a model instance by {{id}} from the data source.
[**playerTrackFindOne**](PlayerTrackApi.md#playerTrackFindOne) | **GET** /PlayerTracks/findOne | Find first instance of the model matched by filter from the data source.
[**playerTrackPrototypeGetPlaylistItem**](PlayerTrackApi.md#playerTrackPrototypeGetPlaylistItem) | **GET** /PlayerTracks/{id}/playlistItem | Fetches belongsTo relation playlistItem.
[**playerTrackPrototypeUpdateAttributesPatchPlayerTracksid**](PlayerTrackApi.md#playerTrackPrototypeUpdateAttributesPatchPlayerTracksid) | **PATCH** /PlayerTracks/{id} | Patch attributes for a model instance and persist it into the data source.
[**playerTrackPrototypeUpdateAttributesPutPlayerTracksid**](PlayerTrackApi.md#playerTrackPrototypeUpdateAttributesPutPlayerTracksid) | **PUT** /PlayerTracks/{id} | Patch attributes for a model instance and persist it into the data source.
[**playerTrackReplaceById**](PlayerTrackApi.md#playerTrackReplaceById) | **POST** /PlayerTracks/{id}/replace | Replace attributes for a model instance and persist it into the data source.
[**playerTrackReplaceOrCreate**](PlayerTrackApi.md#playerTrackReplaceOrCreate) | **POST** /PlayerTracks/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
[**playerTrackUpdateAll**](PlayerTrackApi.md#playerTrackUpdateAll) | **POST** /PlayerTracks/update | Update instances of the model matched by {{where}} from the data source.
[**playerTrackUpsertPatchPlayerTracks**](PlayerTrackApi.md#playerTrackUpsertPatchPlayerTracks) | **PATCH** /PlayerTracks | Patch an existing model instance or insert a new one into the data source.
[**playerTrackUpsertPutPlayerTracks**](PlayerTrackApi.md#playerTrackUpsertPutPlayerTracks) | **PUT** /PlayerTracks | Patch an existing model instance or insert a new one into the data source.
[**playerTrackUpsertWithWhere**](PlayerTrackApi.md#playerTrackUpsertWithWhere) | **POST** /PlayerTracks/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.


<a name="playerTrackCount"></a>
# **playerTrackCount**
> InlineResponse2001 playerTrackCount(where)

Count instances of the model matched by where from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.PlayerTrackApi;

PlayerTrackApi apiInstance = new PlayerTrackApi();
String where = "where_example"; // String | Criteria to match model instances
try {
    InlineResponse2001 result = apiInstance.playerTrackCount(where);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PlayerTrackApi#playerTrackCount");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional]

### Return type

[**InlineResponse2001**](InlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playerTrackCreate"></a>
# **playerTrackCreate**
> PlayerTrack playerTrackCreate(data)

Create a new instance of the model and persist it into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.PlayerTrackApi;

PlayerTrackApi apiInstance = new PlayerTrackApi();
PlayerTrack data = new PlayerTrack(); // PlayerTrack | Model instance data
try {
    PlayerTrack result = apiInstance.playerTrackCreate(data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PlayerTrackApi#playerTrackCreate");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**PlayerTrack**](PlayerTrack.md)| Model instance data | [optional]

### Return type

[**PlayerTrack**](PlayerTrack.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playerTrackCreateChangeStreamGetPlayerTracksChangeStream"></a>
# **playerTrackCreateChangeStreamGetPlayerTracksChangeStream**
> File playerTrackCreateChangeStreamGetPlayerTracksChangeStream(options)

Create a change stream.

### Example
```java
// Import classes:
//import com.harpoon.api.PlayerTrackApi;

PlayerTrackApi apiInstance = new PlayerTrackApi();
String options = "options_example"; // String | 
try {
    File result = apiInstance.playerTrackCreateChangeStreamGetPlayerTracksChangeStream(options);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PlayerTrackApi#playerTrackCreateChangeStreamGetPlayerTracksChangeStream");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **String**|  | [optional]

### Return type

[**File**](File.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playerTrackCreateChangeStreamPostPlayerTracksChangeStream"></a>
# **playerTrackCreateChangeStreamPostPlayerTracksChangeStream**
> File playerTrackCreateChangeStreamPostPlayerTracksChangeStream(options)

Create a change stream.

### Example
```java
// Import classes:
//import com.harpoon.api.PlayerTrackApi;

PlayerTrackApi apiInstance = new PlayerTrackApi();
String options = "options_example"; // String | 
try {
    File result = apiInstance.playerTrackCreateChangeStreamPostPlayerTracksChangeStream(options);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PlayerTrackApi#playerTrackCreateChangeStreamPostPlayerTracksChangeStream");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **String**|  | [optional]

### Return type

[**File**](File.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playerTrackDeleteById"></a>
# **playerTrackDeleteById**
> Object playerTrackDeleteById(id)

Delete a model instance by {{id}} from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.PlayerTrackApi;

PlayerTrackApi apiInstance = new PlayerTrackApi();
String id = "id_example"; // String | Model id
try {
    Object result = apiInstance.playerTrackDeleteById(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PlayerTrackApi#playerTrackDeleteById");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |

### Return type

**Object**

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playerTrackExistsGetPlayerTracksidExists"></a>
# **playerTrackExistsGetPlayerTracksidExists**
> InlineResponse2003 playerTrackExistsGetPlayerTracksidExists(id)

Check whether a model instance exists in the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.PlayerTrackApi;

PlayerTrackApi apiInstance = new PlayerTrackApi();
String id = "id_example"; // String | Model id
try {
    InlineResponse2003 result = apiInstance.playerTrackExistsGetPlayerTracksidExists(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PlayerTrackApi#playerTrackExistsGetPlayerTracksidExists");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |

### Return type

[**InlineResponse2003**](InlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playerTrackExistsHeadPlayerTracksid"></a>
# **playerTrackExistsHeadPlayerTracksid**
> InlineResponse2003 playerTrackExistsHeadPlayerTracksid(id)

Check whether a model instance exists in the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.PlayerTrackApi;

PlayerTrackApi apiInstance = new PlayerTrackApi();
String id = "id_example"; // String | Model id
try {
    InlineResponse2003 result = apiInstance.playerTrackExistsHeadPlayerTracksid(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PlayerTrackApi#playerTrackExistsHeadPlayerTracksid");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |

### Return type

[**InlineResponse2003**](InlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playerTrackFind"></a>
# **playerTrackFind**
> List&lt;PlayerTrack&gt; playerTrackFind(filter)

Find all instances of the model matched by filter from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.PlayerTrackApi;

PlayerTrackApi apiInstance = new PlayerTrackApi();
String filter = "filter_example"; // String | Filter defining fields, where, include, order, offset, and limit
try {
    List<PlayerTrack> result = apiInstance.playerTrackFind(filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PlayerTrackApi#playerTrackFind");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **String**| Filter defining fields, where, include, order, offset, and limit | [optional]

### Return type

[**List&lt;PlayerTrack&gt;**](PlayerTrack.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playerTrackFindById"></a>
# **playerTrackFindById**
> PlayerTrack playerTrackFindById(id, filter)

Find a model instance by {{id}} from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.PlayerTrackApi;

PlayerTrackApi apiInstance = new PlayerTrackApi();
String id = "id_example"; // String | Model id
String filter = "filter_example"; // String | Filter defining fields and include
try {
    PlayerTrack result = apiInstance.playerTrackFindById(id, filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PlayerTrackApi#playerTrackFindById");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |
 **filter** | **String**| Filter defining fields and include | [optional]

### Return type

[**PlayerTrack**](PlayerTrack.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playerTrackFindOne"></a>
# **playerTrackFindOne**
> PlayerTrack playerTrackFindOne(filter)

Find first instance of the model matched by filter from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.PlayerTrackApi;

PlayerTrackApi apiInstance = new PlayerTrackApi();
String filter = "filter_example"; // String | Filter defining fields, where, include, order, offset, and limit
try {
    PlayerTrack result = apiInstance.playerTrackFindOne(filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PlayerTrackApi#playerTrackFindOne");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **String**| Filter defining fields, where, include, order, offset, and limit | [optional]

### Return type

[**PlayerTrack**](PlayerTrack.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playerTrackPrototypeGetPlaylistItem"></a>
# **playerTrackPrototypeGetPlaylistItem**
> PlaylistItem playerTrackPrototypeGetPlaylistItem(id, refresh)

Fetches belongsTo relation playlistItem.

### Example
```java
// Import classes:
//import com.harpoon.api.PlayerTrackApi;

PlayerTrackApi apiInstance = new PlayerTrackApi();
String id = "id_example"; // String | PlayerTrack id
Boolean refresh = true; // Boolean | 
try {
    PlaylistItem result = apiInstance.playerTrackPrototypeGetPlaylistItem(id, refresh);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PlayerTrackApi#playerTrackPrototypeGetPlaylistItem");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| PlayerTrack id |
 **refresh** | **Boolean**|  | [optional]

### Return type

[**PlaylistItem**](PlaylistItem.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playerTrackPrototypeUpdateAttributesPatchPlayerTracksid"></a>
# **playerTrackPrototypeUpdateAttributesPatchPlayerTracksid**
> PlayerTrack playerTrackPrototypeUpdateAttributesPatchPlayerTracksid(id, data)

Patch attributes for a model instance and persist it into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.PlayerTrackApi;

PlayerTrackApi apiInstance = new PlayerTrackApi();
String id = "id_example"; // String | PlayerTrack id
PlayerTrack data = new PlayerTrack(); // PlayerTrack | An object of model property name/value pairs
try {
    PlayerTrack result = apiInstance.playerTrackPrototypeUpdateAttributesPatchPlayerTracksid(id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PlayerTrackApi#playerTrackPrototypeUpdateAttributesPatchPlayerTracksid");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| PlayerTrack id |
 **data** | [**PlayerTrack**](PlayerTrack.md)| An object of model property name/value pairs | [optional]

### Return type

[**PlayerTrack**](PlayerTrack.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playerTrackPrototypeUpdateAttributesPutPlayerTracksid"></a>
# **playerTrackPrototypeUpdateAttributesPutPlayerTracksid**
> PlayerTrack playerTrackPrototypeUpdateAttributesPutPlayerTracksid(id, data)

Patch attributes for a model instance and persist it into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.PlayerTrackApi;

PlayerTrackApi apiInstance = new PlayerTrackApi();
String id = "id_example"; // String | PlayerTrack id
PlayerTrack data = new PlayerTrack(); // PlayerTrack | An object of model property name/value pairs
try {
    PlayerTrack result = apiInstance.playerTrackPrototypeUpdateAttributesPutPlayerTracksid(id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PlayerTrackApi#playerTrackPrototypeUpdateAttributesPutPlayerTracksid");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| PlayerTrack id |
 **data** | [**PlayerTrack**](PlayerTrack.md)| An object of model property name/value pairs | [optional]

### Return type

[**PlayerTrack**](PlayerTrack.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playerTrackReplaceById"></a>
# **playerTrackReplaceById**
> PlayerTrack playerTrackReplaceById(id, data)

Replace attributes for a model instance and persist it into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.PlayerTrackApi;

PlayerTrackApi apiInstance = new PlayerTrackApi();
String id = "id_example"; // String | Model id
PlayerTrack data = new PlayerTrack(); // PlayerTrack | Model instance data
try {
    PlayerTrack result = apiInstance.playerTrackReplaceById(id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PlayerTrackApi#playerTrackReplaceById");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |
 **data** | [**PlayerTrack**](PlayerTrack.md)| Model instance data | [optional]

### Return type

[**PlayerTrack**](PlayerTrack.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playerTrackReplaceOrCreate"></a>
# **playerTrackReplaceOrCreate**
> PlayerTrack playerTrackReplaceOrCreate(data)

Replace an existing model instance or insert a new one into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.PlayerTrackApi;

PlayerTrackApi apiInstance = new PlayerTrackApi();
PlayerTrack data = new PlayerTrack(); // PlayerTrack | Model instance data
try {
    PlayerTrack result = apiInstance.playerTrackReplaceOrCreate(data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PlayerTrackApi#playerTrackReplaceOrCreate");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**PlayerTrack**](PlayerTrack.md)| Model instance data | [optional]

### Return type

[**PlayerTrack**](PlayerTrack.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playerTrackUpdateAll"></a>
# **playerTrackUpdateAll**
> InlineResponse2002 playerTrackUpdateAll(where, data)

Update instances of the model matched by {{where}} from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.PlayerTrackApi;

PlayerTrackApi apiInstance = new PlayerTrackApi();
String where = "where_example"; // String | Criteria to match model instances
PlayerTrack data = new PlayerTrack(); // PlayerTrack | An object of model property name/value pairs
try {
    InlineResponse2002 result = apiInstance.playerTrackUpdateAll(where, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PlayerTrackApi#playerTrackUpdateAll");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional]
 **data** | [**PlayerTrack**](PlayerTrack.md)| An object of model property name/value pairs | [optional]

### Return type

[**InlineResponse2002**](InlineResponse2002.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playerTrackUpsertPatchPlayerTracks"></a>
# **playerTrackUpsertPatchPlayerTracks**
> PlayerTrack playerTrackUpsertPatchPlayerTracks(data)

Patch an existing model instance or insert a new one into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.PlayerTrackApi;

PlayerTrackApi apiInstance = new PlayerTrackApi();
PlayerTrack data = new PlayerTrack(); // PlayerTrack | Model instance data
try {
    PlayerTrack result = apiInstance.playerTrackUpsertPatchPlayerTracks(data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PlayerTrackApi#playerTrackUpsertPatchPlayerTracks");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**PlayerTrack**](PlayerTrack.md)| Model instance data | [optional]

### Return type

[**PlayerTrack**](PlayerTrack.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playerTrackUpsertPutPlayerTracks"></a>
# **playerTrackUpsertPutPlayerTracks**
> PlayerTrack playerTrackUpsertPutPlayerTracks(data)

Patch an existing model instance or insert a new one into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.PlayerTrackApi;

PlayerTrackApi apiInstance = new PlayerTrackApi();
PlayerTrack data = new PlayerTrack(); // PlayerTrack | Model instance data
try {
    PlayerTrack result = apiInstance.playerTrackUpsertPutPlayerTracks(data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PlayerTrackApi#playerTrackUpsertPutPlayerTracks");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**PlayerTrack**](PlayerTrack.md)| Model instance data | [optional]

### Return type

[**PlayerTrack**](PlayerTrack.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playerTrackUpsertWithWhere"></a>
# **playerTrackUpsertWithWhere**
> PlayerTrack playerTrackUpsertWithWhere(where, data)

Update an existing model instance or insert a new one into the data source based on the where criteria.

### Example
```java
// Import classes:
//import com.harpoon.api.PlayerTrackApi;

PlayerTrackApi apiInstance = new PlayerTrackApi();
String where = "where_example"; // String | Criteria to match model instances
PlayerTrack data = new PlayerTrack(); // PlayerTrack | An object of model property name/value pairs
try {
    PlayerTrack result = apiInstance.playerTrackUpsertWithWhere(where, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PlayerTrackApi#playerTrackUpsertWithWhere");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional]
 **data** | [**PlayerTrack**](PlayerTrack.md)| An object of model property name/value pairs | [optional]

### Return type

[**PlayerTrack**](PlayerTrack.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

