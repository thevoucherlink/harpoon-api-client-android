
# RadioPlaySession

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**sessionTime** | **Double** |  |  [optional]
**playUpdateTime** | [**Date**](Date.md) |  |  [optional]
**radioStreamId** | **Double** |  |  [optional]
**radioShowId** | **Double** |  |  [optional]
**playEndTime** | [**Date**](Date.md) |  |  [optional]
**playStartTime** | [**Date**](Date.md) |  |  [optional]
**customerId** | **Double** |  |  [optional]
**id** | **Double** |  |  [optional]



