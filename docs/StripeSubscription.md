
# StripeSubscription

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**applicationFeePercent** | **Double** |  |  [optional]
**cancelAtPeriodEnd** | **Boolean** |  |  [optional]
**canceledAt** | **Double** |  |  [optional]
**currentPeriodEnd** | **Double** |  |  [optional]
**currentPeriodStart** | **Double** |  |  [optional]
**metadata** | **String** |  |  [optional]
**quantity** | **Double** |  |  [optional]
**start** | **Double** |  |  [optional]
**status** | **String** |  |  [optional]
**taxPercent** | **Double** |  |  [optional]
**trialEnd** | **Double** |  |  [optional]
**trialStart** | **Double** |  |  [optional]
**customer** | **String** |  | 
**discount** | [**StripeDiscount**](StripeDiscount.md) |  |  [optional]
**plan** | [**StripePlan**](StripePlan.md) |  | 
**object** | **String** |  |  [optional]
**endedAt** | **Double** |  |  [optional]
**id** | **Double** |  |  [optional]



