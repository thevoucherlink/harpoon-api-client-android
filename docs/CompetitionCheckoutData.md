
# CompetitionCheckoutData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**chances** | [**List&lt;CompetitionCheckoutChanceData&gt;**](CompetitionCheckoutChanceData.md) | Chances to checkout | 



