
# AwCollpurCoupon

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Double** |  | 
**dealId** | **Double** |  | 
**purchaseId** | **Double** |  | 
**couponCode** | **String** |  | 
**status** | **String** |  | 
**couponDeliveryDatetime** | [**Date**](Date.md) |  |  [optional]
**couponDateUpdated** | [**Date**](Date.md) |  |  [optional]
**redeemedAt** | [**Date**](Date.md) |  |  [optional]
**redeemedByTerminal** | **String** |  |  [optional]



