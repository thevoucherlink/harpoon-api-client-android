
# CompetitionWinner

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** |  |  [optional]
**position** | **String** |  |  [optional]
**chosenAt** | [**Date**](Date.md) |  |  [optional]
**id** | **Double** |  |  [optional]



