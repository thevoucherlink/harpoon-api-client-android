
# Playlist

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** | Playlist name |  [optional]
**shortDescription** | **String** | Playlist short description |  [optional]
**image** | **String** | Playlist image |  [optional]
**isMain** | **Boolean** | If Playlist is included in Main |  [optional]
**id** | **Double** |  |  [optional]
**appId** | **String** |  |  [optional]
**playlistItems** | **List&lt;Object&gt;** |  |  [optional]



