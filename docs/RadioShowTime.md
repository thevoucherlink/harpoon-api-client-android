
# RadioShowTime

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**startTime** | **String** | Time when the show starts, format HHmm |  [optional]
**endTime** | **String** | Time when the show ends, format HHmm |  [optional]
**weekday** | **Double** | Day of the week when the show is live, 1 &#x3D; Monday / 7 &#x3D; Sunday |  [optional]
**id** | **Double** |  |  [optional]
**radioShowId** | **Double** |  |  [optional]
**radioShow** | **Object** |  |  [optional]



