
# StripeCoupon

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**object** | **String** |  |  [optional]
**amountOff** | **Double** |  |  [optional]
**created** | **Double** |  |  [optional]
**currency** | **String** |  |  [optional]
**durationInMonths** | **Double** |  |  [optional]
**livemode** | **Boolean** |  |  [optional]
**maxRedemptions** | **Double** |  |  [optional]
**metadata** | **String** |  |  [optional]
**percentOff** | **Double** |  |  [optional]
**redeemBy** | **Double** |  |  [optional]
**timesRedeemed** | **Double** |  |  [optional]
**valid** | **Boolean** |  |  [optional]
**duration** | **String** |  | 
**id** | **Double** |  |  [optional]



