
# Competition

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**basePrice** | **Double** |  |  [optional]
**attendees** | [**List&lt;CompetitionAttendee&gt;**](CompetitionAttendee.md) |  |  [optional]
**attendeeCount** | **Double** |  |  [optional]
**hasJoined** | **Boolean** |  |  [optional]
**chanceCount** | **Double** |  |  [optional]
**isWinner** | **Boolean** |  |  [optional]
**facebook** | [**FacebookEvent**](FacebookEvent.md) |  |  [optional]
**chances** | [**List&lt;CompetitionChance&gt;**](CompetitionChance.md) |  |  [optional]
**earnMoreChancesURL** | **String** |  |  [optional]
**type** | [**Category**](Category.md) |  |  [optional]
**earnMoreChances** | **Boolean** |  |  [optional]
**competitionQuestion** | **String** |  |  [optional]
**competitionAnswer** | **String** |  |  [optional]
**name** | **String** |  |  [optional]
**description** | **String** |  |  [optional]
**cover** | **String** |  |  [optional]
**campaignType** | [**Category**](Category.md) |  |  [optional]
**category** | [**Category**](Category.md) |  |  [optional]
**topic** | [**Category**](Category.md) |  |  [optional]
**alias** | **String** |  |  [optional]
**from** | [**Date**](Date.md) |  |  [optional]
**to** | [**Date**](Date.md) |  |  [optional]
**baseCurrency** | **String** |  |  [optional]
**priceText** | **String** |  |  [optional]
**bannerText** | **String** |  |  [optional]
**checkoutLink** | **String** |  |  [optional]
**nearestVenue** | [**Venue**](Venue.md) |  |  [optional]
**actionText** | **String** |  |  [optional]
**status** | **String** |  |  [optional]
**collectionNotes** | **String** |  |  [optional]
**termsConditions** | **String** |  |  [optional]
**locationLink** | **String** |  |  [optional]
**altLink** | **String** |  |  [optional]
**redemptionType** | **String** |  |  [optional]
**brand** | [**Brand**](Brand.md) |  |  [optional]
**closestPurchase** | [**OfferClosestPurchase**](OfferClosestPurchase.md) |  |  [optional]
**isFeatured** | **Boolean** |  |  [optional]
**qtyPerOrder** | **Double** |  |  [optional]
**shareLink** | **String** |  |  [optional]
**id** | **Double** |  |  [optional]



