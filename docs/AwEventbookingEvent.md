
# AwEventbookingEvent

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Double** |  | 
**productId** | **Double** |  |  [optional]
**isEnabled** | **Double** |  |  [optional]
**eventStartDate** | [**Date**](Date.md) |  |  [optional]
**eventEndDate** | [**Date**](Date.md) |  |  [optional]
**dayCountBeforeSendReminderLetter** | **Double** |  |  [optional]
**isReminderSend** | **Double** |  | 
**isTermsEnabled** | **Double** |  |  [optional]
**generatePdfTickets** | **Double** |  | 
**redeemRoles** | **String** |  |  [optional]
**location** | **String** |  |  [optional]
**tickets** | **List&lt;Object&gt;** |  |  [optional]
**attributes** | **List&lt;Object&gt;** |  |  [optional]
**purchasedTickets** | **List&lt;Object&gt;** |  |  [optional]



