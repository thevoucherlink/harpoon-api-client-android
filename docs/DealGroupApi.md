# DealGroupApi

All URIs are relative to *https://apicdn.harpoonconnect.com/v1.1.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**dealGroupCheckout**](DealGroupApi.md#dealGroupCheckout) | **POST** /DealGroups/{id}/checkout | 
[**dealGroupFind**](DealGroupApi.md#dealGroupFind) | **GET** /DealGroups | Find all instances of the model matched by filter from the data source.
[**dealGroupFindById**](DealGroupApi.md#dealGroupFindById) | **GET** /DealGroups/{id} | Find a model instance by {{id}} from the data source.
[**dealGroupFindOne**](DealGroupApi.md#dealGroupFindOne) | **GET** /DealGroups/findOne | Find first instance of the model matched by filter from the data source.
[**dealGroupRedeem**](DealGroupApi.md#dealGroupRedeem) | **POST** /DealGroups/{id}/redeem | 
[**dealGroupReplaceById**](DealGroupApi.md#dealGroupReplaceById) | **POST** /DealGroups/{id}/replace | Replace attributes for a model instance and persist it into the data source.
[**dealGroupReplaceOrCreate**](DealGroupApi.md#dealGroupReplaceOrCreate) | **POST** /DealGroups/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
[**dealGroupUpsertWithWhere**](DealGroupApi.md#dealGroupUpsertWithWhere) | **POST** /DealGroups/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.
[**dealGroupVenues**](DealGroupApi.md#dealGroupVenues) | **GET** /DealGroups/{id}/venues | 


<a name="dealGroupCheckout"></a>
# **dealGroupCheckout**
> DealGroupCheckout dealGroupCheckout(id, data)



### Example
```java
// Import classes:
//import com.harpoon.api.DealGroupApi;

DealGroupApi apiInstance = new DealGroupApi();
String id = "id_example"; // String | Model id
DealGroupCheckoutData data = new DealGroupCheckoutData(); // DealGroupCheckoutData | 
try {
    DealGroupCheckout result = apiInstance.dealGroupCheckout(id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DealGroupApi#dealGroupCheckout");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |
 **data** | [**DealGroupCheckoutData**](DealGroupCheckoutData.md)|  | [optional]

### Return type

[**DealGroupCheckout**](DealGroupCheckout.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="dealGroupFind"></a>
# **dealGroupFind**
> List&lt;DealGroup&gt; dealGroupFind(filter)

Find all instances of the model matched by filter from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.DealGroupApi;

DealGroupApi apiInstance = new DealGroupApi();
String filter = "filter_example"; // String | Filter defining fields, where, include, order, offset, and limit
try {
    List<DealGroup> result = apiInstance.dealGroupFind(filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DealGroupApi#dealGroupFind");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **String**| Filter defining fields, where, include, order, offset, and limit | [optional]

### Return type

[**List&lt;DealGroup&gt;**](DealGroup.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="dealGroupFindById"></a>
# **dealGroupFindById**
> DealGroup dealGroupFindById(id, filter)

Find a model instance by {{id}} from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.DealGroupApi;

DealGroupApi apiInstance = new DealGroupApi();
String id = "id_example"; // String | Model id
String filter = "filter_example"; // String | Filter defining fields and include
try {
    DealGroup result = apiInstance.dealGroupFindById(id, filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DealGroupApi#dealGroupFindById");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |
 **filter** | **String**| Filter defining fields and include | [optional]

### Return type

[**DealGroup**](DealGroup.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="dealGroupFindOne"></a>
# **dealGroupFindOne**
> DealGroup dealGroupFindOne(filter)

Find first instance of the model matched by filter from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.DealGroupApi;

DealGroupApi apiInstance = new DealGroupApi();
String filter = "filter_example"; // String | Filter defining fields, where, include, order, offset, and limit
try {
    DealGroup result = apiInstance.dealGroupFindOne(filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DealGroupApi#dealGroupFindOne");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **String**| Filter defining fields, where, include, order, offset, and limit | [optional]

### Return type

[**DealGroup**](DealGroup.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="dealGroupRedeem"></a>
# **dealGroupRedeem**
> DealGroupPurchase dealGroupRedeem(id, data)



### Example
```java
// Import classes:
//import com.harpoon.api.DealGroupApi;

DealGroupApi apiInstance = new DealGroupApi();
String id = "id_example"; // String | Model id
DealGroupRedeemData data = new DealGroupRedeemData(); // DealGroupRedeemData | 
try {
    DealGroupPurchase result = apiInstance.dealGroupRedeem(id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DealGroupApi#dealGroupRedeem");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |
 **data** | [**DealGroupRedeemData**](DealGroupRedeemData.md)|  | [optional]

### Return type

[**DealGroupPurchase**](DealGroupPurchase.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="dealGroupReplaceById"></a>
# **dealGroupReplaceById**
> DealGroup dealGroupReplaceById(id, data)

Replace attributes for a model instance and persist it into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.DealGroupApi;

DealGroupApi apiInstance = new DealGroupApi();
String id = "id_example"; // String | Model id
DealGroup data = new DealGroup(); // DealGroup | Model instance data
try {
    DealGroup result = apiInstance.dealGroupReplaceById(id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DealGroupApi#dealGroupReplaceById");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |
 **data** | [**DealGroup**](DealGroup.md)| Model instance data | [optional]

### Return type

[**DealGroup**](DealGroup.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="dealGroupReplaceOrCreate"></a>
# **dealGroupReplaceOrCreate**
> DealGroup dealGroupReplaceOrCreate(data)

Replace an existing model instance or insert a new one into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.DealGroupApi;

DealGroupApi apiInstance = new DealGroupApi();
DealGroup data = new DealGroup(); // DealGroup | Model instance data
try {
    DealGroup result = apiInstance.dealGroupReplaceOrCreate(data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DealGroupApi#dealGroupReplaceOrCreate");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**DealGroup**](DealGroup.md)| Model instance data | [optional]

### Return type

[**DealGroup**](DealGroup.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="dealGroupUpsertWithWhere"></a>
# **dealGroupUpsertWithWhere**
> DealGroup dealGroupUpsertWithWhere(where, data)

Update an existing model instance or insert a new one into the data source based on the where criteria.

### Example
```java
// Import classes:
//import com.harpoon.api.DealGroupApi;

DealGroupApi apiInstance = new DealGroupApi();
String where = "where_example"; // String | Criteria to match model instances
DealGroup data = new DealGroup(); // DealGroup | An object of model property name/value pairs
try {
    DealGroup result = apiInstance.dealGroupUpsertWithWhere(where, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DealGroupApi#dealGroupUpsertWithWhere");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional]
 **data** | [**DealGroup**](DealGroup.md)| An object of model property name/value pairs | [optional]

### Return type

[**DealGroup**](DealGroup.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="dealGroupVenues"></a>
# **dealGroupVenues**
> List&lt;Venue&gt; dealGroupVenues(id, filter)



### Example
```java
// Import classes:
//import com.harpoon.api.DealGroupApi;

DealGroupApi apiInstance = new DealGroupApi();
String id = "id_example"; // String | Model id
String filter = "filter_example"; // String | Filter defining fields and include
try {
    List<Venue> result = apiInstance.dealGroupVenues(id, filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DealGroupApi#dealGroupVenues");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |
 **filter** | **String**| Filter defining fields and include | [optional]

### Return type

[**List&lt;Venue&gt;**](Venue.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

