# CustomerApi

All URIs are relative to *https://apicdn.harpoonconnect.com/v1.1.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**customerActivities**](CustomerApi.md#customerActivities) | **GET** /Customers/{id}/activities | Get Customer activities
[**customerBadge**](CustomerApi.md#customerBadge) | **GET** /Customers/{id}/badge | Get Customer badges
[**customerCardCreate**](CustomerApi.md#customerCardCreate) | **POST** /Customers/{id}/cards | Create a Card to a Customer
[**customerCardDelete**](CustomerApi.md#customerCardDelete) | **DELETE** /Customers/{id}/cards/{fk} | Remove a Card from a Customer
[**customerCardFindById**](CustomerApi.md#customerCardFindById) | **GET** /Customers/{id}/cards/{fk} | Get a specific Customer Card
[**customerCardUpdate**](CustomerApi.md#customerCardUpdate) | **PUT** /Customers/{id}/cards/{fk} | Update a Customer Card
[**customerCards**](CustomerApi.md#customerCards) | **GET** /Customers/{id}/cards | Get Customer Cards
[**customerCompetitionOrders**](CustomerApi.md#customerCompetitionOrders) | **GET** /Customers/{id}/competitions/{fk}/purchases | 
[**customerCouponOrders**](CustomerApi.md#customerCouponOrders) | **GET** /Customers/{id}/coupons/{fk}/purchases | 
[**customerCreate**](CustomerApi.md#customerCreate) | **POST** /Customers | Create a new instance of the model and persist it into the data source.
[**customerDealGroupOrders**](CustomerApi.md#customerDealGroupOrders) | **GET** /Customers/{id}/dealGroups/{fk}/purchases | 
[**customerDealPaidOrders**](CustomerApi.md#customerDealPaidOrders) | **GET** /Customers/{id}/dealPaids/{fk}/purchases | 
[**customerEventAttending**](CustomerApi.md#customerEventAttending) | **GET** /Customers/{id}/events/attending | 
[**customerEventOrders**](CustomerApi.md#customerEventOrders) | **GET** /Customers/{id}/events/{fk}/purchases | 
[**customerFind**](CustomerApi.md#customerFind) | **GET** /Customers | Find all instances of the model matched by filter from the data source.
[**customerFindById**](CustomerApi.md#customerFindById) | **GET** /Customers/{id} | Find a model instance by {{id}} from the data source.
[**customerFindOne**](CustomerApi.md#customerFindOne) | **GET** /Customers/findOne | Find first instance of the model matched by filter from the data source.
[**customerFollow**](CustomerApi.md#customerFollow) | **PUT** /Customers/{id}/followers/{fk} | Set Customer to follow of another Customer
[**customerFollowers**](CustomerApi.md#customerFollowers) | **GET** /Customers/{id}/followers | Get Customer followers
[**customerFollowings**](CustomerApi.md#customerFollowings) | **GET** /Customers/{id}/followings | Get Customers following a Customer
[**customerLogin**](CustomerApi.md#customerLogin) | **POST** /Customers/login | Login a Customer and receive Authorization Code
[**customerNotifications**](CustomerApi.md#customerNotifications) | **GET** /Customers/{id}/notifications | Get Customer Notifications
[**customerPasswordResetAlt**](CustomerApi.md#customerPasswordResetAlt) | **POST** /Customers/password/reset | Reset Customer password from just their known email
[**customerPasswordUpdate**](CustomerApi.md#customerPasswordUpdate) | **PUT** /Customers/{id}/password | Update the Customer password knowing it&#39;s id and current password
[**customerRefreshToken**](CustomerApi.md#customerRefreshToken) | **POST** /Customers/token/refresh | Use a Customer Refresh Access Token to receive a new Access Token
[**customerReplaceById**](CustomerApi.md#customerReplaceById) | **POST** /Customers/{id}/replace | Replace attributes for a model instance and persist it into the data source.
[**customerReplaceOrCreate**](CustomerApi.md#customerReplaceOrCreate) | **POST** /Customers/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
[**customerTokenFromAuthorizationCode**](CustomerApi.md#customerTokenFromAuthorizationCode) | **POST** /Customers/token/request | Authorize a Customer and receive an Access Token
[**customerUnfollow**](CustomerApi.md#customerUnfollow) | **DELETE** /Customers/{id}/followers/{fk} | Remove Customer from following of another Customer
[**customerUpdate**](CustomerApi.md#customerUpdate) | **PUT** /Customers/{id} | Update the Customer details knowing it&#39;s id
[**customerUpsertWithWhere**](CustomerApi.md#customerUpsertWithWhere) | **POST** /Customers/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.
[**customerVerify**](CustomerApi.md#customerVerify) | **GET** /Customers/token/verify | Verify that the request is authenticated with a Customer Access Token
[**customerWalletCompetition**](CustomerApi.md#customerWalletCompetition) | **GET** /Customers/{id}/wallet/competitions | 
[**customerWalletCoupon**](CustomerApi.md#customerWalletCoupon) | **GET** /Customers/{id}/wallet/coupons | 
[**customerWalletDeal**](CustomerApi.md#customerWalletDeal) | **GET** /Customers/{id}/wallet/deals | 
[**customerWalletDealGroup**](CustomerApi.md#customerWalletDealGroup) | **GET** /Customers/{id}/wallet/dealgroups | 
[**customerWalletDealPaid**](CustomerApi.md#customerWalletDealPaid) | **GET** /Customers/{id}/wallet/dealpaids | 
[**customerWalletEvent**](CustomerApi.md#customerWalletEvent) | **GET** /Customers/{id}/wallet/events | 
[**customerWalletOffer**](CustomerApi.md#customerWalletOffer) | **GET** /Customers/{id}/wallet/offers | 


<a name="customerActivities"></a>
# **customerActivities**
> List&lt;CustomerActivity&gt; customerActivities(id, filter)

Get Customer activities

### Example
```java
// Import classes:
//import com.harpoon.api.CustomerApi;

CustomerApi apiInstance = new CustomerApi();
String id = "id_example"; // String | Model id
String filter = "filter_example"; // String | Filter defining fields and include
try {
    List<CustomerActivity> result = apiInstance.customerActivities(id, filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CustomerApi#customerActivities");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |
 **filter** | **String**| Filter defining fields and include | [optional]

### Return type

[**List&lt;CustomerActivity&gt;**](CustomerActivity.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="customerBadge"></a>
# **customerBadge**
> CustomerBadge customerBadge(id)

Get Customer badges

### Example
```java
// Import classes:
//import com.harpoon.api.CustomerApi;

CustomerApi apiInstance = new CustomerApi();
String id = "id_example"; // String | Model id
try {
    CustomerBadge result = apiInstance.customerBadge(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CustomerApi#customerBadge");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |

### Return type

[**CustomerBadge**](CustomerBadge.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="customerCardCreate"></a>
# **customerCardCreate**
> Card customerCardCreate(id, data)

Create a Card to a Customer

### Example
```java
// Import classes:
//import com.harpoon.api.CustomerApi;

CustomerApi apiInstance = new CustomerApi();
String id = "id_example"; // String | Model id
CustomerCardCreateData data = new CustomerCardCreateData(); // CustomerCardCreateData | 
try {
    Card result = apiInstance.customerCardCreate(id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CustomerApi#customerCardCreate");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |
 **data** | [**CustomerCardCreateData**](CustomerCardCreateData.md)|  | [optional]

### Return type

[**Card**](Card.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="customerCardDelete"></a>
# **customerCardDelete**
> Object customerCardDelete(id, fk)

Remove a Card from a Customer

### Example
```java
// Import classes:
//import com.harpoon.api.CustomerApi;

CustomerApi apiInstance = new CustomerApi();
String id = "id_example"; // String | Model id
String fk = "fk_example"; // String | Card id
try {
    Object result = apiInstance.customerCardDelete(id, fk);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CustomerApi#customerCardDelete");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |
 **fk** | **String**| Card id |

### Return type

**Object**

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="customerCardFindById"></a>
# **customerCardFindById**
> Card customerCardFindById(id, fk, filter)

Get a specific Customer Card

### Example
```java
// Import classes:
//import com.harpoon.api.CustomerApi;

CustomerApi apiInstance = new CustomerApi();
String id = "id_example"; // String | Model id
String fk = "fk_example"; // String | Card id
String filter = "filter_example"; // String | Filter defining fields and include
try {
    Card result = apiInstance.customerCardFindById(id, fk, filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CustomerApi#customerCardFindById");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |
 **fk** | **String**| Card id |
 **filter** | **String**| Filter defining fields and include | [optional]

### Return type

[**Card**](Card.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="customerCardUpdate"></a>
# **customerCardUpdate**
> Card customerCardUpdate(id, fk, data)

Update a Customer Card

### Example
```java
// Import classes:
//import com.harpoon.api.CustomerApi;

CustomerApi apiInstance = new CustomerApi();
String id = "id_example"; // String | Model id
String fk = "fk_example"; // String | Card id
Card data = new Card(); // Card | 
try {
    Card result = apiInstance.customerCardUpdate(id, fk, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CustomerApi#customerCardUpdate");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |
 **fk** | **String**| Card id |
 **data** | [**Card**](Card.md)|  | [optional]

### Return type

[**Card**](Card.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="customerCards"></a>
# **customerCards**
> List&lt;Card&gt; customerCards(id, filter)

Get Customer Cards

### Example
```java
// Import classes:
//import com.harpoon.api.CustomerApi;

CustomerApi apiInstance = new CustomerApi();
String id = "id_example"; // String | Model id
String filter = "filter_example"; // String | Filter defining fields and include
try {
    List<Card> result = apiInstance.customerCards(id, filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CustomerApi#customerCards");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |
 **filter** | **String**| Filter defining fields and include | [optional]

### Return type

[**List&lt;Card&gt;**](Card.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="customerCompetitionOrders"></a>
# **customerCompetitionOrders**
> List&lt;CompetitionPurchase&gt; customerCompetitionOrders(id, fk, filter)



### Example
```java
// Import classes:
//import com.harpoon.api.CustomerApi;

CustomerApi apiInstance = new CustomerApi();
String id = "id_example"; // String | Model id
String fk = "fk_example"; // String | Model id
String filter = "filter_example"; // String | Filter defining fields and include
try {
    List<CompetitionPurchase> result = apiInstance.customerCompetitionOrders(id, fk, filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CustomerApi#customerCompetitionOrders");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |
 **fk** | **String**| Model id |
 **filter** | **String**| Filter defining fields and include | [optional]

### Return type

[**List&lt;CompetitionPurchase&gt;**](CompetitionPurchase.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="customerCouponOrders"></a>
# **customerCouponOrders**
> List&lt;CouponPurchase&gt; customerCouponOrders(id, fk, filter)



### Example
```java
// Import classes:
//import com.harpoon.api.CustomerApi;

CustomerApi apiInstance = new CustomerApi();
String id = "id_example"; // String | Model id
String fk = "fk_example"; // String | Model id
String filter = "filter_example"; // String | Filter defining fields and include
try {
    List<CouponPurchase> result = apiInstance.customerCouponOrders(id, fk, filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CustomerApi#customerCouponOrders");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |
 **fk** | **String**| Model id |
 **filter** | **String**| Filter defining fields and include | [optional]

### Return type

[**List&lt;CouponPurchase&gt;**](CouponPurchase.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="customerCreate"></a>
# **customerCreate**
> Customer customerCreate(data)

Create a new instance of the model and persist it into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.CustomerApi;

CustomerApi apiInstance = new CustomerApi();
Customer data = new Customer(); // Customer | Model instance data
try {
    Customer result = apiInstance.customerCreate(data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CustomerApi#customerCreate");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**Customer**](Customer.md)| Model instance data | [optional]

### Return type

[**Customer**](Customer.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="customerDealGroupOrders"></a>
# **customerDealGroupOrders**
> List&lt;DealGroupPurchase&gt; customerDealGroupOrders(id, fk, filter)



### Example
```java
// Import classes:
//import com.harpoon.api.CustomerApi;

CustomerApi apiInstance = new CustomerApi();
String id = "id_example"; // String | Model id
String fk = "fk_example"; // String | Model id
String filter = "filter_example"; // String | Filter defining fields and include
try {
    List<DealGroupPurchase> result = apiInstance.customerDealGroupOrders(id, fk, filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CustomerApi#customerDealGroupOrders");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |
 **fk** | **String**| Model id |
 **filter** | **String**| Filter defining fields and include | [optional]

### Return type

[**List&lt;DealGroupPurchase&gt;**](DealGroupPurchase.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="customerDealPaidOrders"></a>
# **customerDealPaidOrders**
> List&lt;DealPaidPurchase&gt; customerDealPaidOrders(id, fk, filter)



### Example
```java
// Import classes:
//import com.harpoon.api.CustomerApi;

CustomerApi apiInstance = new CustomerApi();
String id = "id_example"; // String | Model id
String fk = "fk_example"; // String | Model id
String filter = "filter_example"; // String | Filter defining fields and include
try {
    List<DealPaidPurchase> result = apiInstance.customerDealPaidOrders(id, fk, filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CustomerApi#customerDealPaidOrders");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |
 **fk** | **String**| Model id |
 **filter** | **String**| Filter defining fields and include | [optional]

### Return type

[**List&lt;DealPaidPurchase&gt;**](DealPaidPurchase.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="customerEventAttending"></a>
# **customerEventAttending**
> List&lt;Event&gt; customerEventAttending(id, filter)



### Example
```java
// Import classes:
//import com.harpoon.api.CustomerApi;

CustomerApi apiInstance = new CustomerApi();
String id = "id_example"; // String | Model id
String filter = "filter_example"; // String | Filter defining fields and include
try {
    List<Event> result = apiInstance.customerEventAttending(id, filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CustomerApi#customerEventAttending");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |
 **filter** | **String**| Filter defining fields and include | [optional]

### Return type

[**List&lt;Event&gt;**](Event.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="customerEventOrders"></a>
# **customerEventOrders**
> List&lt;EventPurchase&gt; customerEventOrders(id, fk, filter)



### Example
```java
// Import classes:
//import com.harpoon.api.CustomerApi;

CustomerApi apiInstance = new CustomerApi();
String id = "id_example"; // String | Model id
String fk = "fk_example"; // String | Model id
String filter = "filter_example"; // String | Filter defining fields and include
try {
    List<EventPurchase> result = apiInstance.customerEventOrders(id, fk, filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CustomerApi#customerEventOrders");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |
 **fk** | **String**| Model id |
 **filter** | **String**| Filter defining fields and include | [optional]

### Return type

[**List&lt;EventPurchase&gt;**](EventPurchase.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="customerFind"></a>
# **customerFind**
> List&lt;Customer&gt; customerFind(filter)

Find all instances of the model matched by filter from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.CustomerApi;

CustomerApi apiInstance = new CustomerApi();
String filter = "filter_example"; // String | Filter defining fields, where, include, order, offset, and limit
try {
    List<Customer> result = apiInstance.customerFind(filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CustomerApi#customerFind");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **String**| Filter defining fields, where, include, order, offset, and limit | [optional]

### Return type

[**List&lt;Customer&gt;**](Customer.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="customerFindById"></a>
# **customerFindById**
> Customer customerFindById(id, filter)

Find a model instance by {{id}} from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.CustomerApi;

CustomerApi apiInstance = new CustomerApi();
String id = "id_example"; // String | Model id
String filter = "filter_example"; // String | Filter defining fields and include
try {
    Customer result = apiInstance.customerFindById(id, filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CustomerApi#customerFindById");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |
 **filter** | **String**| Filter defining fields and include | [optional]

### Return type

[**Customer**](Customer.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="customerFindOne"></a>
# **customerFindOne**
> Customer customerFindOne(filter)

Find first instance of the model matched by filter from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.CustomerApi;

CustomerApi apiInstance = new CustomerApi();
String filter = "filter_example"; // String | Filter defining fields, where, include, order, offset, and limit
try {
    Customer result = apiInstance.customerFindOne(filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CustomerApi#customerFindOne");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **String**| Filter defining fields, where, include, order, offset, and limit | [optional]

### Return type

[**Customer**](Customer.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="customerFollow"></a>
# **customerFollow**
> Object customerFollow(id, fk)

Set Customer to follow of another Customer

### Example
```java
// Import classes:
//import com.harpoon.api.CustomerApi;

CustomerApi apiInstance = new CustomerApi();
String id = "id_example"; // String | Model id
String fk = "fk_example"; // String | Other Customer id
try {
    Object result = apiInstance.customerFollow(id, fk);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CustomerApi#customerFollow");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |
 **fk** | **String**| Other Customer id |

### Return type

**Object**

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="customerFollowers"></a>
# **customerFollowers**
> List&lt;Follower&gt; customerFollowers(id, filter)

Get Customer followers

### Example
```java
// Import classes:
//import com.harpoon.api.CustomerApi;

CustomerApi apiInstance = new CustomerApi();
String id = "id_example"; // String | Model id
String filter = "filter_example"; // String | Filter defining fields and include
try {
    List<Follower> result = apiInstance.customerFollowers(id, filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CustomerApi#customerFollowers");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |
 **filter** | **String**| Filter defining fields and include | [optional]

### Return type

[**List&lt;Follower&gt;**](Follower.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="customerFollowings"></a>
# **customerFollowings**
> List&lt;Following&gt; customerFollowings(id, filter)

Get Customers following a Customer

### Example
```java
// Import classes:
//import com.harpoon.api.CustomerApi;

CustomerApi apiInstance = new CustomerApi();
String id = "id_example"; // String | Model id
String filter = "filter_example"; // String | Filter defining fields and include
try {
    List<Following> result = apiInstance.customerFollowings(id, filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CustomerApi#customerFollowings");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |
 **filter** | **String**| Filter defining fields and include | [optional]

### Return type

[**List&lt;Following&gt;**](Following.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="customerLogin"></a>
# **customerLogin**
> Customer customerLogin(credentials)

Login a Customer and receive Authorization Code

### Example
```java
// Import classes:
//import com.harpoon.api.CustomerApi;

CustomerApi apiInstance = new CustomerApi();
CustomerLoginCredentials credentials = new CustomerLoginCredentials(); // CustomerLoginCredentials | Login via email and password
try {
    Customer result = apiInstance.customerLogin(credentials);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CustomerApi#customerLogin");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **credentials** | [**CustomerLoginCredentials**](CustomerLoginCredentials.md)| Login via email and password | [optional]

### Return type

[**Customer**](Customer.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="customerNotifications"></a>
# **customerNotifications**
> List&lt;CustomerNotification&gt; customerNotifications(id, filter)

Get Customer Notifications

### Example
```java
// Import classes:
//import com.harpoon.api.CustomerApi;

CustomerApi apiInstance = new CustomerApi();
String id = "id_example"; // String | Model id
String filter = "filter_example"; // String | Filter defining fields and include
try {
    List<CustomerNotification> result = apiInstance.customerNotifications(id, filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CustomerApi#customerNotifications");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |
 **filter** | **String**| Filter defining fields and include | [optional]

### Return type

[**List&lt;CustomerNotification&gt;**](CustomerNotification.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="customerPasswordResetAlt"></a>
# **customerPasswordResetAlt**
> Object customerPasswordResetAlt(credentials)

Reset Customer password from just their known email

### Example
```java
// Import classes:
//import com.harpoon.api.CustomerApi;

CustomerApi apiInstance = new CustomerApi();
CustomerPasswordResetCredentials credentials = new CustomerPasswordResetCredentials(); // CustomerPasswordResetCredentials | Customer credentials to reset the password
try {
    Object result = apiInstance.customerPasswordResetAlt(credentials);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CustomerApi#customerPasswordResetAlt");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **credentials** | [**CustomerPasswordResetCredentials**](CustomerPasswordResetCredentials.md)| Customer credentials to reset the password | [optional]

### Return type

**Object**

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="customerPasswordUpdate"></a>
# **customerPasswordUpdate**
> Customer customerPasswordUpdate(id, credentials)

Update the Customer password knowing it&#39;s id and current password

### Example
```java
// Import classes:
//import com.harpoon.api.CustomerApi;

CustomerApi apiInstance = new CustomerApi();
String id = "id_example"; // String | Model id
CustomerPasswordUpdateCredentials credentials = new CustomerPasswordUpdateCredentials(); // CustomerPasswordUpdateCredentials | Customer credentials to update the current password
try {
    Customer result = apiInstance.customerPasswordUpdate(id, credentials);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CustomerApi#customerPasswordUpdate");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |
 **credentials** | [**CustomerPasswordUpdateCredentials**](CustomerPasswordUpdateCredentials.md)| Customer credentials to update the current password | [optional]

### Return type

[**Customer**](Customer.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="customerRefreshToken"></a>
# **customerRefreshToken**
> AuthToken customerRefreshToken(credentials)

Use a Customer Refresh Access Token to receive a new Access Token

### Example
```java
// Import classes:
//import com.harpoon.api.CustomerApi;

CustomerApi apiInstance = new CustomerApi();
CustomerRefreshTokenCredentials credentials = new CustomerRefreshTokenCredentials(); // CustomerRefreshTokenCredentials | Customer credentials to refresh an expired token
try {
    AuthToken result = apiInstance.customerRefreshToken(credentials);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CustomerApi#customerRefreshToken");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **credentials** | [**CustomerRefreshTokenCredentials**](CustomerRefreshTokenCredentials.md)| Customer credentials to refresh an expired token | [optional]

### Return type

[**AuthToken**](AuthToken.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="customerReplaceById"></a>
# **customerReplaceById**
> Customer customerReplaceById(id, data)

Replace attributes for a model instance and persist it into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.CustomerApi;

CustomerApi apiInstance = new CustomerApi();
String id = "id_example"; // String | Model id
Customer data = new Customer(); // Customer | Model instance data
try {
    Customer result = apiInstance.customerReplaceById(id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CustomerApi#customerReplaceById");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |
 **data** | [**Customer**](Customer.md)| Model instance data | [optional]

### Return type

[**Customer**](Customer.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="customerReplaceOrCreate"></a>
# **customerReplaceOrCreate**
> Customer customerReplaceOrCreate(data)

Replace an existing model instance or insert a new one into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.CustomerApi;

CustomerApi apiInstance = new CustomerApi();
Customer data = new Customer(); // Customer | Model instance data
try {
    Customer result = apiInstance.customerReplaceOrCreate(data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CustomerApi#customerReplaceOrCreate");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**Customer**](Customer.md)| Model instance data | [optional]

### Return type

[**Customer**](Customer.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="customerTokenFromAuthorizationCode"></a>
# **customerTokenFromAuthorizationCode**
> AuthToken customerTokenFromAuthorizationCode(credentials)

Authorize a Customer and receive an Access Token

### Example
```java
// Import classes:
//import com.harpoon.api.CustomerApi;

CustomerApi apiInstance = new CustomerApi();
CustomerTokenFromAuthorizationCodeCredentials credentials = new CustomerTokenFromAuthorizationCodeCredentials(); // CustomerTokenFromAuthorizationCodeCredentials | Customer credentials to request authorized
try {
    AuthToken result = apiInstance.customerTokenFromAuthorizationCode(credentials);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CustomerApi#customerTokenFromAuthorizationCode");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **credentials** | [**CustomerTokenFromAuthorizationCodeCredentials**](CustomerTokenFromAuthorizationCodeCredentials.md)| Customer credentials to request authorized | [optional]

### Return type

[**AuthToken**](AuthToken.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="customerUnfollow"></a>
# **customerUnfollow**
> Object customerUnfollow(id, fk)

Remove Customer from following of another Customer

### Example
```java
// Import classes:
//import com.harpoon.api.CustomerApi;

CustomerApi apiInstance = new CustomerApi();
String id = "id_example"; // String | Model id
String fk = "fk_example"; // String | Other Customer id
try {
    Object result = apiInstance.customerUnfollow(id, fk);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CustomerApi#customerUnfollow");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |
 **fk** | **String**| Other Customer id |

### Return type

**Object**

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="customerUpdate"></a>
# **customerUpdate**
> Customer customerUpdate(id, data)

Update the Customer details knowing it&#39;s id

### Example
```java
// Import classes:
//import com.harpoon.api.CustomerApi;

CustomerApi apiInstance = new CustomerApi();
String id = "id_example"; // String | Model id
CustomerUpdateData data = new CustomerUpdateData(); // CustomerUpdateData | Customer data to be updated
try {
    Customer result = apiInstance.customerUpdate(id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CustomerApi#customerUpdate");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |
 **data** | [**CustomerUpdateData**](CustomerUpdateData.md)| Customer data to be updated | [optional]

### Return type

[**Customer**](Customer.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="customerUpsertWithWhere"></a>
# **customerUpsertWithWhere**
> Customer customerUpsertWithWhere(where, data)

Update an existing model instance or insert a new one into the data source based on the where criteria.

### Example
```java
// Import classes:
//import com.harpoon.api.CustomerApi;

CustomerApi apiInstance = new CustomerApi();
String where = "where_example"; // String | Criteria to match model instances
Customer data = new Customer(); // Customer | An object of model property name/value pairs
try {
    Customer result = apiInstance.customerUpsertWithWhere(where, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CustomerApi#customerUpsertWithWhere");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional]
 **data** | [**Customer**](Customer.md)| An object of model property name/value pairs | [optional]

### Return type

[**Customer**](Customer.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="customerVerify"></a>
# **customerVerify**
> Object customerVerify()

Verify that the request is authenticated with a Customer Access Token

### Example
```java
// Import classes:
//import com.harpoon.api.CustomerApi;

CustomerApi apiInstance = new CustomerApi();
try {
    Object result = apiInstance.customerVerify();
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CustomerApi#customerVerify");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

**Object**

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="customerWalletCompetition"></a>
# **customerWalletCompetition**
> List&lt;Competition&gt; customerWalletCompetition(id, filter)



### Example
```java
// Import classes:
//import com.harpoon.api.CustomerApi;

CustomerApi apiInstance = new CustomerApi();
String id = "id_example"; // String | Model id
String filter = "filter_example"; // String | Filter defining fields and include
try {
    List<Competition> result = apiInstance.customerWalletCompetition(id, filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CustomerApi#customerWalletCompetition");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |
 **filter** | **String**| Filter defining fields and include | [optional]

### Return type

[**List&lt;Competition&gt;**](Competition.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="customerWalletCoupon"></a>
# **customerWalletCoupon**
> List&lt;Coupon&gt; customerWalletCoupon(id, filter)



### Example
```java
// Import classes:
//import com.harpoon.api.CustomerApi;

CustomerApi apiInstance = new CustomerApi();
String id = "id_example"; // String | Model id
String filter = "filter_example"; // String | Filter defining fields and include
try {
    List<Coupon> result = apiInstance.customerWalletCoupon(id, filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CustomerApi#customerWalletCoupon");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |
 **filter** | **String**| Filter defining fields and include | [optional]

### Return type

[**List&lt;Coupon&gt;**](Coupon.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="customerWalletDeal"></a>
# **customerWalletDeal**
> List&lt;Offer&gt; customerWalletDeal(id, filter)



### Example
```java
// Import classes:
//import com.harpoon.api.CustomerApi;

CustomerApi apiInstance = new CustomerApi();
String id = "id_example"; // String | Model id
String filter = "filter_example"; // String | Filter defining fields and include
try {
    List<Offer> result = apiInstance.customerWalletDeal(id, filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CustomerApi#customerWalletDeal");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |
 **filter** | **String**| Filter defining fields and include | [optional]

### Return type

[**List&lt;Offer&gt;**](Offer.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="customerWalletDealGroup"></a>
# **customerWalletDealGroup**
> List&lt;DealGroup&gt; customerWalletDealGroup(id, filter)



### Example
```java
// Import classes:
//import com.harpoon.api.CustomerApi;

CustomerApi apiInstance = new CustomerApi();
String id = "id_example"; // String | Model id
String filter = "filter_example"; // String | Filter defining fields and include
try {
    List<DealGroup> result = apiInstance.customerWalletDealGroup(id, filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CustomerApi#customerWalletDealGroup");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |
 **filter** | **String**| Filter defining fields and include | [optional]

### Return type

[**List&lt;DealGroup&gt;**](DealGroup.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="customerWalletDealPaid"></a>
# **customerWalletDealPaid**
> List&lt;DealPaid&gt; customerWalletDealPaid(id, filter)



### Example
```java
// Import classes:
//import com.harpoon.api.CustomerApi;

CustomerApi apiInstance = new CustomerApi();
String id = "id_example"; // String | Model id
String filter = "filter_example"; // String | Filter defining fields and include
try {
    List<DealPaid> result = apiInstance.customerWalletDealPaid(id, filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CustomerApi#customerWalletDealPaid");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |
 **filter** | **String**| Filter defining fields and include | [optional]

### Return type

[**List&lt;DealPaid&gt;**](DealPaid.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="customerWalletEvent"></a>
# **customerWalletEvent**
> List&lt;Event&gt; customerWalletEvent(id, filter)



### Example
```java
// Import classes:
//import com.harpoon.api.CustomerApi;

CustomerApi apiInstance = new CustomerApi();
String id = "id_example"; // String | Model id
String filter = "filter_example"; // String | Filter defining fields and include
try {
    List<Event> result = apiInstance.customerWalletEvent(id, filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CustomerApi#customerWalletEvent");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |
 **filter** | **String**| Filter defining fields and include | [optional]

### Return type

[**List&lt;Event&gt;**](Event.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="customerWalletOffer"></a>
# **customerWalletOffer**
> List&lt;Offer&gt; customerWalletOffer(id, filter)



### Example
```java
// Import classes:
//import com.harpoon.api.CustomerApi;

CustomerApi apiInstance = new CustomerApi();
String id = "id_example"; // String | Model id
String filter = "filter_example"; // String | Filter defining fields and include
try {
    List<Offer> result = apiInstance.customerWalletOffer(id, filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CustomerApi#customerWalletOffer");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |
 **filter** | **String**| Filter defining fields and include | [optional]

### Return type

[**List&lt;Offer&gt;**](Offer.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

