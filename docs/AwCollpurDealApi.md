# AwCollpurDealApi

All URIs are relative to *https://apicdn.harpoonconnect.com/v1.1.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**awCollpurDealCount**](AwCollpurDealApi.md#awCollpurDealCount) | **GET** /AwCollpurDeals/count | Count instances of the model matched by where from the data source.
[**awCollpurDealCreate**](AwCollpurDealApi.md#awCollpurDealCreate) | **POST** /AwCollpurDeals | Create a new instance of the model and persist it into the data source.
[**awCollpurDealCreateChangeStreamGetAwCollpurDealsChangeStream**](AwCollpurDealApi.md#awCollpurDealCreateChangeStreamGetAwCollpurDealsChangeStream) | **GET** /AwCollpurDeals/change-stream | Create a change stream.
[**awCollpurDealCreateChangeStreamPostAwCollpurDealsChangeStream**](AwCollpurDealApi.md#awCollpurDealCreateChangeStreamPostAwCollpurDealsChangeStream) | **POST** /AwCollpurDeals/change-stream | Create a change stream.
[**awCollpurDealDeleteById**](AwCollpurDealApi.md#awCollpurDealDeleteById) | **DELETE** /AwCollpurDeals/{id} | Delete a model instance by {{id}} from the data source.
[**awCollpurDealExistsGetAwCollpurDealsidExists**](AwCollpurDealApi.md#awCollpurDealExistsGetAwCollpurDealsidExists) | **GET** /AwCollpurDeals/{id}/exists | Check whether a model instance exists in the data source.
[**awCollpurDealExistsHeadAwCollpurDealsid**](AwCollpurDealApi.md#awCollpurDealExistsHeadAwCollpurDealsid) | **HEAD** /AwCollpurDeals/{id} | Check whether a model instance exists in the data source.
[**awCollpurDealFind**](AwCollpurDealApi.md#awCollpurDealFind) | **GET** /AwCollpurDeals | Find all instances of the model matched by filter from the data source.
[**awCollpurDealFindById**](AwCollpurDealApi.md#awCollpurDealFindById) | **GET** /AwCollpurDeals/{id} | Find a model instance by {{id}} from the data source.
[**awCollpurDealFindOne**](AwCollpurDealApi.md#awCollpurDealFindOne) | **GET** /AwCollpurDeals/findOne | Find first instance of the model matched by filter from the data source.
[**awCollpurDealPrototypeCountAwCollpurDealPurchases**](AwCollpurDealApi.md#awCollpurDealPrototypeCountAwCollpurDealPurchases) | **GET** /AwCollpurDeals/{id}/awCollpurDealPurchases/count | Counts awCollpurDealPurchases of AwCollpurDeal.
[**awCollpurDealPrototypeCreateAwCollpurDealPurchases**](AwCollpurDealApi.md#awCollpurDealPrototypeCreateAwCollpurDealPurchases) | **POST** /AwCollpurDeals/{id}/awCollpurDealPurchases | Creates a new instance in awCollpurDealPurchases of this model.
[**awCollpurDealPrototypeDeleteAwCollpurDealPurchases**](AwCollpurDealApi.md#awCollpurDealPrototypeDeleteAwCollpurDealPurchases) | **DELETE** /AwCollpurDeals/{id}/awCollpurDealPurchases | Deletes all awCollpurDealPurchases of this model.
[**awCollpurDealPrototypeDestroyByIdAwCollpurDealPurchases**](AwCollpurDealApi.md#awCollpurDealPrototypeDestroyByIdAwCollpurDealPurchases) | **DELETE** /AwCollpurDeals/{id}/awCollpurDealPurchases/{fk} | Delete a related item by id for awCollpurDealPurchases.
[**awCollpurDealPrototypeFindByIdAwCollpurDealPurchases**](AwCollpurDealApi.md#awCollpurDealPrototypeFindByIdAwCollpurDealPurchases) | **GET** /AwCollpurDeals/{id}/awCollpurDealPurchases/{fk} | Find a related item by id for awCollpurDealPurchases.
[**awCollpurDealPrototypeGetAwCollpurDealPurchases**](AwCollpurDealApi.md#awCollpurDealPrototypeGetAwCollpurDealPurchases) | **GET** /AwCollpurDeals/{id}/awCollpurDealPurchases | Queries awCollpurDealPurchases of AwCollpurDeal.
[**awCollpurDealPrototypeUpdateAttributesPatchAwCollpurDealsid**](AwCollpurDealApi.md#awCollpurDealPrototypeUpdateAttributesPatchAwCollpurDealsid) | **PATCH** /AwCollpurDeals/{id} | Patch attributes for a model instance and persist it into the data source.
[**awCollpurDealPrototypeUpdateAttributesPutAwCollpurDealsid**](AwCollpurDealApi.md#awCollpurDealPrototypeUpdateAttributesPutAwCollpurDealsid) | **PUT** /AwCollpurDeals/{id} | Patch attributes for a model instance and persist it into the data source.
[**awCollpurDealPrototypeUpdateByIdAwCollpurDealPurchases**](AwCollpurDealApi.md#awCollpurDealPrototypeUpdateByIdAwCollpurDealPurchases) | **PUT** /AwCollpurDeals/{id}/awCollpurDealPurchases/{fk} | Update a related item by id for awCollpurDealPurchases.
[**awCollpurDealReplaceById**](AwCollpurDealApi.md#awCollpurDealReplaceById) | **POST** /AwCollpurDeals/{id}/replace | Replace attributes for a model instance and persist it into the data source.
[**awCollpurDealReplaceOrCreate**](AwCollpurDealApi.md#awCollpurDealReplaceOrCreate) | **POST** /AwCollpurDeals/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
[**awCollpurDealUpdateAll**](AwCollpurDealApi.md#awCollpurDealUpdateAll) | **POST** /AwCollpurDeals/update | Update instances of the model matched by {{where}} from the data source.
[**awCollpurDealUpsertPatchAwCollpurDeals**](AwCollpurDealApi.md#awCollpurDealUpsertPatchAwCollpurDeals) | **PATCH** /AwCollpurDeals | Patch an existing model instance or insert a new one into the data source.
[**awCollpurDealUpsertPutAwCollpurDeals**](AwCollpurDealApi.md#awCollpurDealUpsertPutAwCollpurDeals) | **PUT** /AwCollpurDeals | Patch an existing model instance or insert a new one into the data source.
[**awCollpurDealUpsertWithWhere**](AwCollpurDealApi.md#awCollpurDealUpsertWithWhere) | **POST** /AwCollpurDeals/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.


<a name="awCollpurDealCount"></a>
# **awCollpurDealCount**
> InlineResponse2001 awCollpurDealCount(where)

Count instances of the model matched by where from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.AwCollpurDealApi;

AwCollpurDealApi apiInstance = new AwCollpurDealApi();
String where = "where_example"; // String | Criteria to match model instances
try {
    InlineResponse2001 result = apiInstance.awCollpurDealCount(where);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AwCollpurDealApi#awCollpurDealCount");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional]

### Return type

[**InlineResponse2001**](InlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awCollpurDealCreate"></a>
# **awCollpurDealCreate**
> AwCollpurDeal awCollpurDealCreate(data)

Create a new instance of the model and persist it into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.AwCollpurDealApi;

AwCollpurDealApi apiInstance = new AwCollpurDealApi();
AwCollpurDeal data = new AwCollpurDeal(); // AwCollpurDeal | Model instance data
try {
    AwCollpurDeal result = apiInstance.awCollpurDealCreate(data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AwCollpurDealApi#awCollpurDealCreate");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**AwCollpurDeal**](AwCollpurDeal.md)| Model instance data | [optional]

### Return type

[**AwCollpurDeal**](AwCollpurDeal.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awCollpurDealCreateChangeStreamGetAwCollpurDealsChangeStream"></a>
# **awCollpurDealCreateChangeStreamGetAwCollpurDealsChangeStream**
> File awCollpurDealCreateChangeStreamGetAwCollpurDealsChangeStream(options)

Create a change stream.

### Example
```java
// Import classes:
//import com.harpoon.api.AwCollpurDealApi;

AwCollpurDealApi apiInstance = new AwCollpurDealApi();
String options = "options_example"; // String | 
try {
    File result = apiInstance.awCollpurDealCreateChangeStreamGetAwCollpurDealsChangeStream(options);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AwCollpurDealApi#awCollpurDealCreateChangeStreamGetAwCollpurDealsChangeStream");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **String**|  | [optional]

### Return type

[**File**](File.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awCollpurDealCreateChangeStreamPostAwCollpurDealsChangeStream"></a>
# **awCollpurDealCreateChangeStreamPostAwCollpurDealsChangeStream**
> File awCollpurDealCreateChangeStreamPostAwCollpurDealsChangeStream(options)

Create a change stream.

### Example
```java
// Import classes:
//import com.harpoon.api.AwCollpurDealApi;

AwCollpurDealApi apiInstance = new AwCollpurDealApi();
String options = "options_example"; // String | 
try {
    File result = apiInstance.awCollpurDealCreateChangeStreamPostAwCollpurDealsChangeStream(options);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AwCollpurDealApi#awCollpurDealCreateChangeStreamPostAwCollpurDealsChangeStream");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **String**|  | [optional]

### Return type

[**File**](File.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awCollpurDealDeleteById"></a>
# **awCollpurDealDeleteById**
> Object awCollpurDealDeleteById(id)

Delete a model instance by {{id}} from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.AwCollpurDealApi;

AwCollpurDealApi apiInstance = new AwCollpurDealApi();
String id = "id_example"; // String | Model id
try {
    Object result = apiInstance.awCollpurDealDeleteById(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AwCollpurDealApi#awCollpurDealDeleteById");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |

### Return type

**Object**

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awCollpurDealExistsGetAwCollpurDealsidExists"></a>
# **awCollpurDealExistsGetAwCollpurDealsidExists**
> InlineResponse2003 awCollpurDealExistsGetAwCollpurDealsidExists(id)

Check whether a model instance exists in the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.AwCollpurDealApi;

AwCollpurDealApi apiInstance = new AwCollpurDealApi();
String id = "id_example"; // String | Model id
try {
    InlineResponse2003 result = apiInstance.awCollpurDealExistsGetAwCollpurDealsidExists(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AwCollpurDealApi#awCollpurDealExistsGetAwCollpurDealsidExists");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |

### Return type

[**InlineResponse2003**](InlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awCollpurDealExistsHeadAwCollpurDealsid"></a>
# **awCollpurDealExistsHeadAwCollpurDealsid**
> InlineResponse2003 awCollpurDealExistsHeadAwCollpurDealsid(id)

Check whether a model instance exists in the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.AwCollpurDealApi;

AwCollpurDealApi apiInstance = new AwCollpurDealApi();
String id = "id_example"; // String | Model id
try {
    InlineResponse2003 result = apiInstance.awCollpurDealExistsHeadAwCollpurDealsid(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AwCollpurDealApi#awCollpurDealExistsHeadAwCollpurDealsid");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |

### Return type

[**InlineResponse2003**](InlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awCollpurDealFind"></a>
# **awCollpurDealFind**
> List&lt;AwCollpurDeal&gt; awCollpurDealFind(filter)

Find all instances of the model matched by filter from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.AwCollpurDealApi;

AwCollpurDealApi apiInstance = new AwCollpurDealApi();
String filter = "filter_example"; // String | Filter defining fields, where, include, order, offset, and limit
try {
    List<AwCollpurDeal> result = apiInstance.awCollpurDealFind(filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AwCollpurDealApi#awCollpurDealFind");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **String**| Filter defining fields, where, include, order, offset, and limit | [optional]

### Return type

[**List&lt;AwCollpurDeal&gt;**](AwCollpurDeal.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awCollpurDealFindById"></a>
# **awCollpurDealFindById**
> AwCollpurDeal awCollpurDealFindById(id, filter)

Find a model instance by {{id}} from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.AwCollpurDealApi;

AwCollpurDealApi apiInstance = new AwCollpurDealApi();
String id = "id_example"; // String | Model id
String filter = "filter_example"; // String | Filter defining fields and include
try {
    AwCollpurDeal result = apiInstance.awCollpurDealFindById(id, filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AwCollpurDealApi#awCollpurDealFindById");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |
 **filter** | **String**| Filter defining fields and include | [optional]

### Return type

[**AwCollpurDeal**](AwCollpurDeal.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awCollpurDealFindOne"></a>
# **awCollpurDealFindOne**
> AwCollpurDeal awCollpurDealFindOne(filter)

Find first instance of the model matched by filter from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.AwCollpurDealApi;

AwCollpurDealApi apiInstance = new AwCollpurDealApi();
String filter = "filter_example"; // String | Filter defining fields, where, include, order, offset, and limit
try {
    AwCollpurDeal result = apiInstance.awCollpurDealFindOne(filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AwCollpurDealApi#awCollpurDealFindOne");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **String**| Filter defining fields, where, include, order, offset, and limit | [optional]

### Return type

[**AwCollpurDeal**](AwCollpurDeal.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awCollpurDealPrototypeCountAwCollpurDealPurchases"></a>
# **awCollpurDealPrototypeCountAwCollpurDealPurchases**
> InlineResponse2001 awCollpurDealPrototypeCountAwCollpurDealPurchases(id, where)

Counts awCollpurDealPurchases of AwCollpurDeal.

### Example
```java
// Import classes:
//import com.harpoon.api.AwCollpurDealApi;

AwCollpurDealApi apiInstance = new AwCollpurDealApi();
String id = "id_example"; // String | AwCollpurDeal id
String where = "where_example"; // String | Criteria to match model instances
try {
    InlineResponse2001 result = apiInstance.awCollpurDealPrototypeCountAwCollpurDealPurchases(id, where);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AwCollpurDealApi#awCollpurDealPrototypeCountAwCollpurDealPurchases");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| AwCollpurDeal id |
 **where** | **String**| Criteria to match model instances | [optional]

### Return type

[**InlineResponse2001**](InlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awCollpurDealPrototypeCreateAwCollpurDealPurchases"></a>
# **awCollpurDealPrototypeCreateAwCollpurDealPurchases**
> AwCollpurDealPurchases awCollpurDealPrototypeCreateAwCollpurDealPurchases(id, data)

Creates a new instance in awCollpurDealPurchases of this model.

### Example
```java
// Import classes:
//import com.harpoon.api.AwCollpurDealApi;

AwCollpurDealApi apiInstance = new AwCollpurDealApi();
String id = "id_example"; // String | AwCollpurDeal id
AwCollpurDealPurchases data = new AwCollpurDealPurchases(); // AwCollpurDealPurchases | 
try {
    AwCollpurDealPurchases result = apiInstance.awCollpurDealPrototypeCreateAwCollpurDealPurchases(id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AwCollpurDealApi#awCollpurDealPrototypeCreateAwCollpurDealPurchases");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| AwCollpurDeal id |
 **data** | [**AwCollpurDealPurchases**](AwCollpurDealPurchases.md)|  | [optional]

### Return type

[**AwCollpurDealPurchases**](AwCollpurDealPurchases.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awCollpurDealPrototypeDeleteAwCollpurDealPurchases"></a>
# **awCollpurDealPrototypeDeleteAwCollpurDealPurchases**
> awCollpurDealPrototypeDeleteAwCollpurDealPurchases(id)

Deletes all awCollpurDealPurchases of this model.

### Example
```java
// Import classes:
//import com.harpoon.api.AwCollpurDealApi;

AwCollpurDealApi apiInstance = new AwCollpurDealApi();
String id = "id_example"; // String | AwCollpurDeal id
try {
    apiInstance.awCollpurDealPrototypeDeleteAwCollpurDealPurchases(id);
} catch (ApiException e) {
    System.err.println("Exception when calling AwCollpurDealApi#awCollpurDealPrototypeDeleteAwCollpurDealPurchases");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| AwCollpurDeal id |

### Return type

null (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awCollpurDealPrototypeDestroyByIdAwCollpurDealPurchases"></a>
# **awCollpurDealPrototypeDestroyByIdAwCollpurDealPurchases**
> awCollpurDealPrototypeDestroyByIdAwCollpurDealPurchases(fk, id)

Delete a related item by id for awCollpurDealPurchases.

### Example
```java
// Import classes:
//import com.harpoon.api.AwCollpurDealApi;

AwCollpurDealApi apiInstance = new AwCollpurDealApi();
String fk = "fk_example"; // String | Foreign key for awCollpurDealPurchases
String id = "id_example"; // String | AwCollpurDeal id
try {
    apiInstance.awCollpurDealPrototypeDestroyByIdAwCollpurDealPurchases(fk, id);
} catch (ApiException e) {
    System.err.println("Exception when calling AwCollpurDealApi#awCollpurDealPrototypeDestroyByIdAwCollpurDealPurchases");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for awCollpurDealPurchases |
 **id** | **String**| AwCollpurDeal id |

### Return type

null (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awCollpurDealPrototypeFindByIdAwCollpurDealPurchases"></a>
# **awCollpurDealPrototypeFindByIdAwCollpurDealPurchases**
> AwCollpurDealPurchases awCollpurDealPrototypeFindByIdAwCollpurDealPurchases(fk, id)

Find a related item by id for awCollpurDealPurchases.

### Example
```java
// Import classes:
//import com.harpoon.api.AwCollpurDealApi;

AwCollpurDealApi apiInstance = new AwCollpurDealApi();
String fk = "fk_example"; // String | Foreign key for awCollpurDealPurchases
String id = "id_example"; // String | AwCollpurDeal id
try {
    AwCollpurDealPurchases result = apiInstance.awCollpurDealPrototypeFindByIdAwCollpurDealPurchases(fk, id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AwCollpurDealApi#awCollpurDealPrototypeFindByIdAwCollpurDealPurchases");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for awCollpurDealPurchases |
 **id** | **String**| AwCollpurDeal id |

### Return type

[**AwCollpurDealPurchases**](AwCollpurDealPurchases.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awCollpurDealPrototypeGetAwCollpurDealPurchases"></a>
# **awCollpurDealPrototypeGetAwCollpurDealPurchases**
> List&lt;AwCollpurDealPurchases&gt; awCollpurDealPrototypeGetAwCollpurDealPurchases(id, filter)

Queries awCollpurDealPurchases of AwCollpurDeal.

### Example
```java
// Import classes:
//import com.harpoon.api.AwCollpurDealApi;

AwCollpurDealApi apiInstance = new AwCollpurDealApi();
String id = "id_example"; // String | AwCollpurDeal id
String filter = "filter_example"; // String | 
try {
    List<AwCollpurDealPurchases> result = apiInstance.awCollpurDealPrototypeGetAwCollpurDealPurchases(id, filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AwCollpurDealApi#awCollpurDealPrototypeGetAwCollpurDealPurchases");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| AwCollpurDeal id |
 **filter** | **String**|  | [optional]

### Return type

[**List&lt;AwCollpurDealPurchases&gt;**](AwCollpurDealPurchases.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awCollpurDealPrototypeUpdateAttributesPatchAwCollpurDealsid"></a>
# **awCollpurDealPrototypeUpdateAttributesPatchAwCollpurDealsid**
> AwCollpurDeal awCollpurDealPrototypeUpdateAttributesPatchAwCollpurDealsid(id, data)

Patch attributes for a model instance and persist it into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.AwCollpurDealApi;

AwCollpurDealApi apiInstance = new AwCollpurDealApi();
String id = "id_example"; // String | AwCollpurDeal id
AwCollpurDeal data = new AwCollpurDeal(); // AwCollpurDeal | An object of model property name/value pairs
try {
    AwCollpurDeal result = apiInstance.awCollpurDealPrototypeUpdateAttributesPatchAwCollpurDealsid(id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AwCollpurDealApi#awCollpurDealPrototypeUpdateAttributesPatchAwCollpurDealsid");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| AwCollpurDeal id |
 **data** | [**AwCollpurDeal**](AwCollpurDeal.md)| An object of model property name/value pairs | [optional]

### Return type

[**AwCollpurDeal**](AwCollpurDeal.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awCollpurDealPrototypeUpdateAttributesPutAwCollpurDealsid"></a>
# **awCollpurDealPrototypeUpdateAttributesPutAwCollpurDealsid**
> AwCollpurDeal awCollpurDealPrototypeUpdateAttributesPutAwCollpurDealsid(id, data)

Patch attributes for a model instance and persist it into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.AwCollpurDealApi;

AwCollpurDealApi apiInstance = new AwCollpurDealApi();
String id = "id_example"; // String | AwCollpurDeal id
AwCollpurDeal data = new AwCollpurDeal(); // AwCollpurDeal | An object of model property name/value pairs
try {
    AwCollpurDeal result = apiInstance.awCollpurDealPrototypeUpdateAttributesPutAwCollpurDealsid(id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AwCollpurDealApi#awCollpurDealPrototypeUpdateAttributesPutAwCollpurDealsid");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| AwCollpurDeal id |
 **data** | [**AwCollpurDeal**](AwCollpurDeal.md)| An object of model property name/value pairs | [optional]

### Return type

[**AwCollpurDeal**](AwCollpurDeal.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awCollpurDealPrototypeUpdateByIdAwCollpurDealPurchases"></a>
# **awCollpurDealPrototypeUpdateByIdAwCollpurDealPurchases**
> AwCollpurDealPurchases awCollpurDealPrototypeUpdateByIdAwCollpurDealPurchases(fk, id, data)

Update a related item by id for awCollpurDealPurchases.

### Example
```java
// Import classes:
//import com.harpoon.api.AwCollpurDealApi;

AwCollpurDealApi apiInstance = new AwCollpurDealApi();
String fk = "fk_example"; // String | Foreign key for awCollpurDealPurchases
String id = "id_example"; // String | AwCollpurDeal id
AwCollpurDealPurchases data = new AwCollpurDealPurchases(); // AwCollpurDealPurchases | 
try {
    AwCollpurDealPurchases result = apiInstance.awCollpurDealPrototypeUpdateByIdAwCollpurDealPurchases(fk, id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AwCollpurDealApi#awCollpurDealPrototypeUpdateByIdAwCollpurDealPurchases");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for awCollpurDealPurchases |
 **id** | **String**| AwCollpurDeal id |
 **data** | [**AwCollpurDealPurchases**](AwCollpurDealPurchases.md)|  | [optional]

### Return type

[**AwCollpurDealPurchases**](AwCollpurDealPurchases.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awCollpurDealReplaceById"></a>
# **awCollpurDealReplaceById**
> AwCollpurDeal awCollpurDealReplaceById(id, data)

Replace attributes for a model instance and persist it into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.AwCollpurDealApi;

AwCollpurDealApi apiInstance = new AwCollpurDealApi();
String id = "id_example"; // String | Model id
AwCollpurDeal data = new AwCollpurDeal(); // AwCollpurDeal | Model instance data
try {
    AwCollpurDeal result = apiInstance.awCollpurDealReplaceById(id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AwCollpurDealApi#awCollpurDealReplaceById");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |
 **data** | [**AwCollpurDeal**](AwCollpurDeal.md)| Model instance data | [optional]

### Return type

[**AwCollpurDeal**](AwCollpurDeal.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awCollpurDealReplaceOrCreate"></a>
# **awCollpurDealReplaceOrCreate**
> AwCollpurDeal awCollpurDealReplaceOrCreate(data)

Replace an existing model instance or insert a new one into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.AwCollpurDealApi;

AwCollpurDealApi apiInstance = new AwCollpurDealApi();
AwCollpurDeal data = new AwCollpurDeal(); // AwCollpurDeal | Model instance data
try {
    AwCollpurDeal result = apiInstance.awCollpurDealReplaceOrCreate(data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AwCollpurDealApi#awCollpurDealReplaceOrCreate");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**AwCollpurDeal**](AwCollpurDeal.md)| Model instance data | [optional]

### Return type

[**AwCollpurDeal**](AwCollpurDeal.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awCollpurDealUpdateAll"></a>
# **awCollpurDealUpdateAll**
> InlineResponse2002 awCollpurDealUpdateAll(where, data)

Update instances of the model matched by {{where}} from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.AwCollpurDealApi;

AwCollpurDealApi apiInstance = new AwCollpurDealApi();
String where = "where_example"; // String | Criteria to match model instances
AwCollpurDeal data = new AwCollpurDeal(); // AwCollpurDeal | An object of model property name/value pairs
try {
    InlineResponse2002 result = apiInstance.awCollpurDealUpdateAll(where, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AwCollpurDealApi#awCollpurDealUpdateAll");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional]
 **data** | [**AwCollpurDeal**](AwCollpurDeal.md)| An object of model property name/value pairs | [optional]

### Return type

[**InlineResponse2002**](InlineResponse2002.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awCollpurDealUpsertPatchAwCollpurDeals"></a>
# **awCollpurDealUpsertPatchAwCollpurDeals**
> AwCollpurDeal awCollpurDealUpsertPatchAwCollpurDeals(data)

Patch an existing model instance or insert a new one into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.AwCollpurDealApi;

AwCollpurDealApi apiInstance = new AwCollpurDealApi();
AwCollpurDeal data = new AwCollpurDeal(); // AwCollpurDeal | Model instance data
try {
    AwCollpurDeal result = apiInstance.awCollpurDealUpsertPatchAwCollpurDeals(data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AwCollpurDealApi#awCollpurDealUpsertPatchAwCollpurDeals");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**AwCollpurDeal**](AwCollpurDeal.md)| Model instance data | [optional]

### Return type

[**AwCollpurDeal**](AwCollpurDeal.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awCollpurDealUpsertPutAwCollpurDeals"></a>
# **awCollpurDealUpsertPutAwCollpurDeals**
> AwCollpurDeal awCollpurDealUpsertPutAwCollpurDeals(data)

Patch an existing model instance or insert a new one into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.AwCollpurDealApi;

AwCollpurDealApi apiInstance = new AwCollpurDealApi();
AwCollpurDeal data = new AwCollpurDeal(); // AwCollpurDeal | Model instance data
try {
    AwCollpurDeal result = apiInstance.awCollpurDealUpsertPutAwCollpurDeals(data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AwCollpurDealApi#awCollpurDealUpsertPutAwCollpurDeals");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**AwCollpurDeal**](AwCollpurDeal.md)| Model instance data | [optional]

### Return type

[**AwCollpurDeal**](AwCollpurDeal.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awCollpurDealUpsertWithWhere"></a>
# **awCollpurDealUpsertWithWhere**
> AwCollpurDeal awCollpurDealUpsertWithWhere(where, data)

Update an existing model instance or insert a new one into the data source based on the where criteria.

### Example
```java
// Import classes:
//import com.harpoon.api.AwCollpurDealApi;

AwCollpurDealApi apiInstance = new AwCollpurDealApi();
String where = "where_example"; // String | Criteria to match model instances
AwCollpurDeal data = new AwCollpurDeal(); // AwCollpurDeal | An object of model property name/value pairs
try {
    AwCollpurDeal result = apiInstance.awCollpurDealUpsertWithWhere(where, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AwCollpurDealApi#awCollpurDealUpsertWithWhere");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional]
 **data** | [**AwCollpurDeal**](AwCollpurDeal.md)| An object of model property name/value pairs | [optional]

### Return type

[**AwCollpurDeal**](AwCollpurDeal.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

