
# StripePlan

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**amount** | **String** |  |  [optional]
**currency** | **String** |  |  [optional]
**interval** | **String** |  |  [optional]
**intervalCount** | **Double** |  |  [optional]
**metadata** | **String** |  |  [optional]
**name** | **String** |  |  [optional]
**statementDescriptor** | **String** |  |  [optional]
**trialPeriodDays** | **Double** |  |  [optional]
**id** | **Double** |  |  [optional]



