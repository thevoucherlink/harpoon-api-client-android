
# RssFeedGroup

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** |  | 
**sortOrder** | **Double** |  |  [optional]
**id** | **Double** |  |  [optional]
**appId** | **String** |  |  [optional]
**rssFeeds** | **List&lt;Object&gt;** |  |  [optional]



