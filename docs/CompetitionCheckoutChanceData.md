
# CompetitionCheckoutChanceData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Double** | Chance to checkout | 
**qty** | **Double** | Quantity to checkout |  [optional]



