
# GeoLocation

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**latitude** | **Double** |  | 
**longitude** | **Double** |  | 
**id** | **Double** |  |  [optional]



