# StripeInvoiceApi

All URIs are relative to *https://apicdn.harpoonconnect.com/v1.1.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**stripeInvoiceCount**](StripeInvoiceApi.md#stripeInvoiceCount) | **GET** /StripeInvoices/count | Count instances of the model matched by where from the data source.
[**stripeInvoiceCreate**](StripeInvoiceApi.md#stripeInvoiceCreate) | **POST** /StripeInvoices | Create a new instance of the model and persist it into the data source.
[**stripeInvoiceCreateChangeStreamGetStripeInvoicesChangeStream**](StripeInvoiceApi.md#stripeInvoiceCreateChangeStreamGetStripeInvoicesChangeStream) | **GET** /StripeInvoices/change-stream | Create a change stream.
[**stripeInvoiceCreateChangeStreamPostStripeInvoicesChangeStream**](StripeInvoiceApi.md#stripeInvoiceCreateChangeStreamPostStripeInvoicesChangeStream) | **POST** /StripeInvoices/change-stream | Create a change stream.
[**stripeInvoiceDeleteById**](StripeInvoiceApi.md#stripeInvoiceDeleteById) | **DELETE** /StripeInvoices/{id} | Delete a model instance by {{id}} from the data source.
[**stripeInvoiceExistsGetStripeInvoicesidExists**](StripeInvoiceApi.md#stripeInvoiceExistsGetStripeInvoicesidExists) | **GET** /StripeInvoices/{id}/exists | Check whether a model instance exists in the data source.
[**stripeInvoiceExistsHeadStripeInvoicesid**](StripeInvoiceApi.md#stripeInvoiceExistsHeadStripeInvoicesid) | **HEAD** /StripeInvoices/{id} | Check whether a model instance exists in the data source.
[**stripeInvoiceFind**](StripeInvoiceApi.md#stripeInvoiceFind) | **GET** /StripeInvoices | Find all instances of the model matched by filter from the data source.
[**stripeInvoiceFindById**](StripeInvoiceApi.md#stripeInvoiceFindById) | **GET** /StripeInvoices/{id} | Find a model instance by {{id}} from the data source.
[**stripeInvoiceFindOne**](StripeInvoiceApi.md#stripeInvoiceFindOne) | **GET** /StripeInvoices/findOne | Find first instance of the model matched by filter from the data source.
[**stripeInvoicePrototypeUpdateAttributesPatchStripeInvoicesid**](StripeInvoiceApi.md#stripeInvoicePrototypeUpdateAttributesPatchStripeInvoicesid) | **PATCH** /StripeInvoices/{id} | Patch attributes for a model instance and persist it into the data source.
[**stripeInvoicePrototypeUpdateAttributesPutStripeInvoicesid**](StripeInvoiceApi.md#stripeInvoicePrototypeUpdateAttributesPutStripeInvoicesid) | **PUT** /StripeInvoices/{id} | Patch attributes for a model instance and persist it into the data source.
[**stripeInvoiceReplaceById**](StripeInvoiceApi.md#stripeInvoiceReplaceById) | **POST** /StripeInvoices/{id}/replace | Replace attributes for a model instance and persist it into the data source.
[**stripeInvoiceReplaceOrCreate**](StripeInvoiceApi.md#stripeInvoiceReplaceOrCreate) | **POST** /StripeInvoices/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
[**stripeInvoiceUpdateAll**](StripeInvoiceApi.md#stripeInvoiceUpdateAll) | **POST** /StripeInvoices/update | Update instances of the model matched by {{where}} from the data source.
[**stripeInvoiceUpsertPatchStripeInvoices**](StripeInvoiceApi.md#stripeInvoiceUpsertPatchStripeInvoices) | **PATCH** /StripeInvoices | Patch an existing model instance or insert a new one into the data source.
[**stripeInvoiceUpsertPutStripeInvoices**](StripeInvoiceApi.md#stripeInvoiceUpsertPutStripeInvoices) | **PUT** /StripeInvoices | Patch an existing model instance or insert a new one into the data source.
[**stripeInvoiceUpsertWithWhere**](StripeInvoiceApi.md#stripeInvoiceUpsertWithWhere) | **POST** /StripeInvoices/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.


<a name="stripeInvoiceCount"></a>
# **stripeInvoiceCount**
> InlineResponse2001 stripeInvoiceCount(where)

Count instances of the model matched by where from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.StripeInvoiceApi;

StripeInvoiceApi apiInstance = new StripeInvoiceApi();
String where = "where_example"; // String | Criteria to match model instances
try {
    InlineResponse2001 result = apiInstance.stripeInvoiceCount(where);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling StripeInvoiceApi#stripeInvoiceCount");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional]

### Return type

[**InlineResponse2001**](InlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripeInvoiceCreate"></a>
# **stripeInvoiceCreate**
> StripeInvoice stripeInvoiceCreate(data)

Create a new instance of the model and persist it into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.StripeInvoiceApi;

StripeInvoiceApi apiInstance = new StripeInvoiceApi();
StripeInvoice data = new StripeInvoice(); // StripeInvoice | Model instance data
try {
    StripeInvoice result = apiInstance.stripeInvoiceCreate(data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling StripeInvoiceApi#stripeInvoiceCreate");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**StripeInvoice**](StripeInvoice.md)| Model instance data | [optional]

### Return type

[**StripeInvoice**](StripeInvoice.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripeInvoiceCreateChangeStreamGetStripeInvoicesChangeStream"></a>
# **stripeInvoiceCreateChangeStreamGetStripeInvoicesChangeStream**
> File stripeInvoiceCreateChangeStreamGetStripeInvoicesChangeStream(options)

Create a change stream.

### Example
```java
// Import classes:
//import com.harpoon.api.StripeInvoiceApi;

StripeInvoiceApi apiInstance = new StripeInvoiceApi();
String options = "options_example"; // String | 
try {
    File result = apiInstance.stripeInvoiceCreateChangeStreamGetStripeInvoicesChangeStream(options);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling StripeInvoiceApi#stripeInvoiceCreateChangeStreamGetStripeInvoicesChangeStream");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **String**|  | [optional]

### Return type

[**File**](File.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripeInvoiceCreateChangeStreamPostStripeInvoicesChangeStream"></a>
# **stripeInvoiceCreateChangeStreamPostStripeInvoicesChangeStream**
> File stripeInvoiceCreateChangeStreamPostStripeInvoicesChangeStream(options)

Create a change stream.

### Example
```java
// Import classes:
//import com.harpoon.api.StripeInvoiceApi;

StripeInvoiceApi apiInstance = new StripeInvoiceApi();
String options = "options_example"; // String | 
try {
    File result = apiInstance.stripeInvoiceCreateChangeStreamPostStripeInvoicesChangeStream(options);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling StripeInvoiceApi#stripeInvoiceCreateChangeStreamPostStripeInvoicesChangeStream");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **String**|  | [optional]

### Return type

[**File**](File.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripeInvoiceDeleteById"></a>
# **stripeInvoiceDeleteById**
> Object stripeInvoiceDeleteById(id)

Delete a model instance by {{id}} from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.StripeInvoiceApi;

StripeInvoiceApi apiInstance = new StripeInvoiceApi();
String id = "id_example"; // String | Model id
try {
    Object result = apiInstance.stripeInvoiceDeleteById(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling StripeInvoiceApi#stripeInvoiceDeleteById");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |

### Return type

**Object**

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripeInvoiceExistsGetStripeInvoicesidExists"></a>
# **stripeInvoiceExistsGetStripeInvoicesidExists**
> InlineResponse2003 stripeInvoiceExistsGetStripeInvoicesidExists(id)

Check whether a model instance exists in the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.StripeInvoiceApi;

StripeInvoiceApi apiInstance = new StripeInvoiceApi();
String id = "id_example"; // String | Model id
try {
    InlineResponse2003 result = apiInstance.stripeInvoiceExistsGetStripeInvoicesidExists(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling StripeInvoiceApi#stripeInvoiceExistsGetStripeInvoicesidExists");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |

### Return type

[**InlineResponse2003**](InlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripeInvoiceExistsHeadStripeInvoicesid"></a>
# **stripeInvoiceExistsHeadStripeInvoicesid**
> InlineResponse2003 stripeInvoiceExistsHeadStripeInvoicesid(id)

Check whether a model instance exists in the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.StripeInvoiceApi;

StripeInvoiceApi apiInstance = new StripeInvoiceApi();
String id = "id_example"; // String | Model id
try {
    InlineResponse2003 result = apiInstance.stripeInvoiceExistsHeadStripeInvoicesid(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling StripeInvoiceApi#stripeInvoiceExistsHeadStripeInvoicesid");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |

### Return type

[**InlineResponse2003**](InlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripeInvoiceFind"></a>
# **stripeInvoiceFind**
> List&lt;StripeInvoice&gt; stripeInvoiceFind(filter)

Find all instances of the model matched by filter from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.StripeInvoiceApi;

StripeInvoiceApi apiInstance = new StripeInvoiceApi();
String filter = "filter_example"; // String | Filter defining fields, where, include, order, offset, and limit
try {
    List<StripeInvoice> result = apiInstance.stripeInvoiceFind(filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling StripeInvoiceApi#stripeInvoiceFind");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **String**| Filter defining fields, where, include, order, offset, and limit | [optional]

### Return type

[**List&lt;StripeInvoice&gt;**](StripeInvoice.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripeInvoiceFindById"></a>
# **stripeInvoiceFindById**
> StripeInvoice stripeInvoiceFindById(id, filter)

Find a model instance by {{id}} from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.StripeInvoiceApi;

StripeInvoiceApi apiInstance = new StripeInvoiceApi();
String id = "id_example"; // String | Model id
String filter = "filter_example"; // String | Filter defining fields and include
try {
    StripeInvoice result = apiInstance.stripeInvoiceFindById(id, filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling StripeInvoiceApi#stripeInvoiceFindById");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |
 **filter** | **String**| Filter defining fields and include | [optional]

### Return type

[**StripeInvoice**](StripeInvoice.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripeInvoiceFindOne"></a>
# **stripeInvoiceFindOne**
> StripeInvoice stripeInvoiceFindOne(filter)

Find first instance of the model matched by filter from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.StripeInvoiceApi;

StripeInvoiceApi apiInstance = new StripeInvoiceApi();
String filter = "filter_example"; // String | Filter defining fields, where, include, order, offset, and limit
try {
    StripeInvoice result = apiInstance.stripeInvoiceFindOne(filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling StripeInvoiceApi#stripeInvoiceFindOne");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **String**| Filter defining fields, where, include, order, offset, and limit | [optional]

### Return type

[**StripeInvoice**](StripeInvoice.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripeInvoicePrototypeUpdateAttributesPatchStripeInvoicesid"></a>
# **stripeInvoicePrototypeUpdateAttributesPatchStripeInvoicesid**
> StripeInvoice stripeInvoicePrototypeUpdateAttributesPatchStripeInvoicesid(id, data)

Patch attributes for a model instance and persist it into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.StripeInvoiceApi;

StripeInvoiceApi apiInstance = new StripeInvoiceApi();
String id = "id_example"; // String | StripeInvoice id
StripeInvoice data = new StripeInvoice(); // StripeInvoice | An object of model property name/value pairs
try {
    StripeInvoice result = apiInstance.stripeInvoicePrototypeUpdateAttributesPatchStripeInvoicesid(id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling StripeInvoiceApi#stripeInvoicePrototypeUpdateAttributesPatchStripeInvoicesid");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| StripeInvoice id |
 **data** | [**StripeInvoice**](StripeInvoice.md)| An object of model property name/value pairs | [optional]

### Return type

[**StripeInvoice**](StripeInvoice.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripeInvoicePrototypeUpdateAttributesPutStripeInvoicesid"></a>
# **stripeInvoicePrototypeUpdateAttributesPutStripeInvoicesid**
> StripeInvoice stripeInvoicePrototypeUpdateAttributesPutStripeInvoicesid(id, data)

Patch attributes for a model instance and persist it into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.StripeInvoiceApi;

StripeInvoiceApi apiInstance = new StripeInvoiceApi();
String id = "id_example"; // String | StripeInvoice id
StripeInvoice data = new StripeInvoice(); // StripeInvoice | An object of model property name/value pairs
try {
    StripeInvoice result = apiInstance.stripeInvoicePrototypeUpdateAttributesPutStripeInvoicesid(id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling StripeInvoiceApi#stripeInvoicePrototypeUpdateAttributesPutStripeInvoicesid");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| StripeInvoice id |
 **data** | [**StripeInvoice**](StripeInvoice.md)| An object of model property name/value pairs | [optional]

### Return type

[**StripeInvoice**](StripeInvoice.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripeInvoiceReplaceById"></a>
# **stripeInvoiceReplaceById**
> StripeInvoice stripeInvoiceReplaceById(id, data)

Replace attributes for a model instance and persist it into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.StripeInvoiceApi;

StripeInvoiceApi apiInstance = new StripeInvoiceApi();
String id = "id_example"; // String | Model id
StripeInvoice data = new StripeInvoice(); // StripeInvoice | Model instance data
try {
    StripeInvoice result = apiInstance.stripeInvoiceReplaceById(id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling StripeInvoiceApi#stripeInvoiceReplaceById");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |
 **data** | [**StripeInvoice**](StripeInvoice.md)| Model instance data | [optional]

### Return type

[**StripeInvoice**](StripeInvoice.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripeInvoiceReplaceOrCreate"></a>
# **stripeInvoiceReplaceOrCreate**
> StripeInvoice stripeInvoiceReplaceOrCreate(data)

Replace an existing model instance or insert a new one into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.StripeInvoiceApi;

StripeInvoiceApi apiInstance = new StripeInvoiceApi();
StripeInvoice data = new StripeInvoice(); // StripeInvoice | Model instance data
try {
    StripeInvoice result = apiInstance.stripeInvoiceReplaceOrCreate(data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling StripeInvoiceApi#stripeInvoiceReplaceOrCreate");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**StripeInvoice**](StripeInvoice.md)| Model instance data | [optional]

### Return type

[**StripeInvoice**](StripeInvoice.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripeInvoiceUpdateAll"></a>
# **stripeInvoiceUpdateAll**
> InlineResponse2002 stripeInvoiceUpdateAll(where, data)

Update instances of the model matched by {{where}} from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.StripeInvoiceApi;

StripeInvoiceApi apiInstance = new StripeInvoiceApi();
String where = "where_example"; // String | Criteria to match model instances
StripeInvoice data = new StripeInvoice(); // StripeInvoice | An object of model property name/value pairs
try {
    InlineResponse2002 result = apiInstance.stripeInvoiceUpdateAll(where, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling StripeInvoiceApi#stripeInvoiceUpdateAll");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional]
 **data** | [**StripeInvoice**](StripeInvoice.md)| An object of model property name/value pairs | [optional]

### Return type

[**InlineResponse2002**](InlineResponse2002.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripeInvoiceUpsertPatchStripeInvoices"></a>
# **stripeInvoiceUpsertPatchStripeInvoices**
> StripeInvoice stripeInvoiceUpsertPatchStripeInvoices(data)

Patch an existing model instance or insert a new one into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.StripeInvoiceApi;

StripeInvoiceApi apiInstance = new StripeInvoiceApi();
StripeInvoice data = new StripeInvoice(); // StripeInvoice | Model instance data
try {
    StripeInvoice result = apiInstance.stripeInvoiceUpsertPatchStripeInvoices(data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling StripeInvoiceApi#stripeInvoiceUpsertPatchStripeInvoices");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**StripeInvoice**](StripeInvoice.md)| Model instance data | [optional]

### Return type

[**StripeInvoice**](StripeInvoice.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripeInvoiceUpsertPutStripeInvoices"></a>
# **stripeInvoiceUpsertPutStripeInvoices**
> StripeInvoice stripeInvoiceUpsertPutStripeInvoices(data)

Patch an existing model instance or insert a new one into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.StripeInvoiceApi;

StripeInvoiceApi apiInstance = new StripeInvoiceApi();
StripeInvoice data = new StripeInvoice(); // StripeInvoice | Model instance data
try {
    StripeInvoice result = apiInstance.stripeInvoiceUpsertPutStripeInvoices(data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling StripeInvoiceApi#stripeInvoiceUpsertPutStripeInvoices");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**StripeInvoice**](StripeInvoice.md)| Model instance data | [optional]

### Return type

[**StripeInvoice**](StripeInvoice.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripeInvoiceUpsertWithWhere"></a>
# **stripeInvoiceUpsertWithWhere**
> StripeInvoice stripeInvoiceUpsertWithWhere(where, data)

Update an existing model instance or insert a new one into the data source based on the where criteria.

### Example
```java
// Import classes:
//import com.harpoon.api.StripeInvoiceApi;

StripeInvoiceApi apiInstance = new StripeInvoiceApi();
String where = "where_example"; // String | Criteria to match model instances
StripeInvoice data = new StripeInvoice(); // StripeInvoice | An object of model property name/value pairs
try {
    StripeInvoice result = apiInstance.stripeInvoiceUpsertWithWhere(where, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling StripeInvoiceApi#stripeInvoiceUpsertWithWhere");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional]
 **data** | [**StripeInvoice**](StripeInvoice.md)| An object of model property name/value pairs | [optional]

### Return type

[**StripeInvoice**](StripeInvoice.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

