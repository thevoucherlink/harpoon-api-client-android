
# CouponPurchaseCode

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | **String** |  |  [optional]
**text** | **String** |  |  [optional]
**image** | **String** |  |  [optional]
**id** | **Double** |  |  [optional]



