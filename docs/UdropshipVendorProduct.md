
# UdropshipVendorProduct

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Double** |  | 
**vendorId** | **Double** |  | 
**productId** | **Double** |  | 
**priority** | **Double** |  | 
**carrierCode** | **String** |  |  [optional]
**vendorSku** | **String** |  |  [optional]
**vendorCost** | **String** |  |  [optional]
**stockQty** | **String** |  |  [optional]
**backorders** | **Double** |  | 
**shippingPrice** | **String** |  |  [optional]
**status** | **Double** |  | 
**reservedQty** | **String** |  |  [optional]
**availState** | **String** |  |  [optional]
**availDate** | [**Date**](Date.md) |  |  [optional]
**relationshipStatus** | **String** |  |  [optional]
**udropshipVendor** | **Object** |  |  [optional]
**catalogProduct** | **Object** |  |  [optional]



