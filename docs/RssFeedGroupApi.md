# RssFeedGroupApi

All URIs are relative to *https://apicdn.harpoonconnect.com/v1.1.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**rssFeedGroupCount**](RssFeedGroupApi.md#rssFeedGroupCount) | **GET** /RssFeedGroups/count | Count instances of the model matched by where from the data source.
[**rssFeedGroupCreate**](RssFeedGroupApi.md#rssFeedGroupCreate) | **POST** /RssFeedGroups | Create a new instance of the model and persist it into the data source.
[**rssFeedGroupCreateChangeStreamGetRssFeedGroupsChangeStream**](RssFeedGroupApi.md#rssFeedGroupCreateChangeStreamGetRssFeedGroupsChangeStream) | **GET** /RssFeedGroups/change-stream | Create a change stream.
[**rssFeedGroupCreateChangeStreamPostRssFeedGroupsChangeStream**](RssFeedGroupApi.md#rssFeedGroupCreateChangeStreamPostRssFeedGroupsChangeStream) | **POST** /RssFeedGroups/change-stream | Create a change stream.
[**rssFeedGroupDeleteById**](RssFeedGroupApi.md#rssFeedGroupDeleteById) | **DELETE** /RssFeedGroups/{id} | Delete a model instance by {{id}} from the data source.
[**rssFeedGroupExistsGetRssFeedGroupsidExists**](RssFeedGroupApi.md#rssFeedGroupExistsGetRssFeedGroupsidExists) | **GET** /RssFeedGroups/{id}/exists | Check whether a model instance exists in the data source.
[**rssFeedGroupExistsHeadRssFeedGroupsid**](RssFeedGroupApi.md#rssFeedGroupExistsHeadRssFeedGroupsid) | **HEAD** /RssFeedGroups/{id} | Check whether a model instance exists in the data source.
[**rssFeedGroupFind**](RssFeedGroupApi.md#rssFeedGroupFind) | **GET** /RssFeedGroups | Find all instances of the model matched by filter from the data source.
[**rssFeedGroupFindById**](RssFeedGroupApi.md#rssFeedGroupFindById) | **GET** /RssFeedGroups/{id} | Find a model instance by {{id}} from the data source.
[**rssFeedGroupFindOne**](RssFeedGroupApi.md#rssFeedGroupFindOne) | **GET** /RssFeedGroups/findOne | Find first instance of the model matched by filter from the data source.
[**rssFeedGroupPrototypeCountRssFeeds**](RssFeedGroupApi.md#rssFeedGroupPrototypeCountRssFeeds) | **GET** /RssFeedGroups/{id}/rssFeeds/count | Counts rssFeeds of RssFeedGroup.
[**rssFeedGroupPrototypeCreateRssFeeds**](RssFeedGroupApi.md#rssFeedGroupPrototypeCreateRssFeeds) | **POST** /RssFeedGroups/{id}/rssFeeds | Creates a new instance in rssFeeds of this model.
[**rssFeedGroupPrototypeDeleteRssFeeds**](RssFeedGroupApi.md#rssFeedGroupPrototypeDeleteRssFeeds) | **DELETE** /RssFeedGroups/{id}/rssFeeds | Deletes all rssFeeds of this model.
[**rssFeedGroupPrototypeDestroyByIdRssFeeds**](RssFeedGroupApi.md#rssFeedGroupPrototypeDestroyByIdRssFeeds) | **DELETE** /RssFeedGroups/{id}/rssFeeds/{fk} | Delete a related item by id for rssFeeds.
[**rssFeedGroupPrototypeFindByIdRssFeeds**](RssFeedGroupApi.md#rssFeedGroupPrototypeFindByIdRssFeeds) | **GET** /RssFeedGroups/{id}/rssFeeds/{fk} | Find a related item by id for rssFeeds.
[**rssFeedGroupPrototypeGetRssFeeds**](RssFeedGroupApi.md#rssFeedGroupPrototypeGetRssFeeds) | **GET** /RssFeedGroups/{id}/rssFeeds | Queries rssFeeds of RssFeedGroup.
[**rssFeedGroupPrototypeUpdateAttributesPatchRssFeedGroupsid**](RssFeedGroupApi.md#rssFeedGroupPrototypeUpdateAttributesPatchRssFeedGroupsid) | **PATCH** /RssFeedGroups/{id} | Patch attributes for a model instance and persist it into the data source.
[**rssFeedGroupPrototypeUpdateAttributesPutRssFeedGroupsid**](RssFeedGroupApi.md#rssFeedGroupPrototypeUpdateAttributesPutRssFeedGroupsid) | **PUT** /RssFeedGroups/{id} | Patch attributes for a model instance and persist it into the data source.
[**rssFeedGroupPrototypeUpdateByIdRssFeeds**](RssFeedGroupApi.md#rssFeedGroupPrototypeUpdateByIdRssFeeds) | **PUT** /RssFeedGroups/{id}/rssFeeds/{fk} | Update a related item by id for rssFeeds.
[**rssFeedGroupReplaceById**](RssFeedGroupApi.md#rssFeedGroupReplaceById) | **POST** /RssFeedGroups/{id}/replace | Replace attributes for a model instance and persist it into the data source.
[**rssFeedGroupReplaceOrCreate**](RssFeedGroupApi.md#rssFeedGroupReplaceOrCreate) | **POST** /RssFeedGroups/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
[**rssFeedGroupUpdateAll**](RssFeedGroupApi.md#rssFeedGroupUpdateAll) | **POST** /RssFeedGroups/update | Update instances of the model matched by {{where}} from the data source.
[**rssFeedGroupUpsertPatchRssFeedGroups**](RssFeedGroupApi.md#rssFeedGroupUpsertPatchRssFeedGroups) | **PATCH** /RssFeedGroups | Patch an existing model instance or insert a new one into the data source.
[**rssFeedGroupUpsertPutRssFeedGroups**](RssFeedGroupApi.md#rssFeedGroupUpsertPutRssFeedGroups) | **PUT** /RssFeedGroups | Patch an existing model instance or insert a new one into the data source.
[**rssFeedGroupUpsertWithWhere**](RssFeedGroupApi.md#rssFeedGroupUpsertWithWhere) | **POST** /RssFeedGroups/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.


<a name="rssFeedGroupCount"></a>
# **rssFeedGroupCount**
> InlineResponse2001 rssFeedGroupCount(where)

Count instances of the model matched by where from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.RssFeedGroupApi;

RssFeedGroupApi apiInstance = new RssFeedGroupApi();
String where = "where_example"; // String | Criteria to match model instances
try {
    InlineResponse2001 result = apiInstance.rssFeedGroupCount(where);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RssFeedGroupApi#rssFeedGroupCount");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional]

### Return type

[**InlineResponse2001**](InlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="rssFeedGroupCreate"></a>
# **rssFeedGroupCreate**
> RssFeedGroup rssFeedGroupCreate(data)

Create a new instance of the model and persist it into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.RssFeedGroupApi;

RssFeedGroupApi apiInstance = new RssFeedGroupApi();
RssFeedGroup data = new RssFeedGroup(); // RssFeedGroup | Model instance data
try {
    RssFeedGroup result = apiInstance.rssFeedGroupCreate(data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RssFeedGroupApi#rssFeedGroupCreate");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**RssFeedGroup**](RssFeedGroup.md)| Model instance data | [optional]

### Return type

[**RssFeedGroup**](RssFeedGroup.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="rssFeedGroupCreateChangeStreamGetRssFeedGroupsChangeStream"></a>
# **rssFeedGroupCreateChangeStreamGetRssFeedGroupsChangeStream**
> File rssFeedGroupCreateChangeStreamGetRssFeedGroupsChangeStream(options)

Create a change stream.

### Example
```java
// Import classes:
//import com.harpoon.api.RssFeedGroupApi;

RssFeedGroupApi apiInstance = new RssFeedGroupApi();
String options = "options_example"; // String | 
try {
    File result = apiInstance.rssFeedGroupCreateChangeStreamGetRssFeedGroupsChangeStream(options);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RssFeedGroupApi#rssFeedGroupCreateChangeStreamGetRssFeedGroupsChangeStream");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **String**|  | [optional]

### Return type

[**File**](File.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="rssFeedGroupCreateChangeStreamPostRssFeedGroupsChangeStream"></a>
# **rssFeedGroupCreateChangeStreamPostRssFeedGroupsChangeStream**
> File rssFeedGroupCreateChangeStreamPostRssFeedGroupsChangeStream(options)

Create a change stream.

### Example
```java
// Import classes:
//import com.harpoon.api.RssFeedGroupApi;

RssFeedGroupApi apiInstance = new RssFeedGroupApi();
String options = "options_example"; // String | 
try {
    File result = apiInstance.rssFeedGroupCreateChangeStreamPostRssFeedGroupsChangeStream(options);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RssFeedGroupApi#rssFeedGroupCreateChangeStreamPostRssFeedGroupsChangeStream");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **String**|  | [optional]

### Return type

[**File**](File.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="rssFeedGroupDeleteById"></a>
# **rssFeedGroupDeleteById**
> Object rssFeedGroupDeleteById(id)

Delete a model instance by {{id}} from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.RssFeedGroupApi;

RssFeedGroupApi apiInstance = new RssFeedGroupApi();
String id = "id_example"; // String | Model id
try {
    Object result = apiInstance.rssFeedGroupDeleteById(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RssFeedGroupApi#rssFeedGroupDeleteById");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |

### Return type

**Object**

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="rssFeedGroupExistsGetRssFeedGroupsidExists"></a>
# **rssFeedGroupExistsGetRssFeedGroupsidExists**
> InlineResponse2003 rssFeedGroupExistsGetRssFeedGroupsidExists(id)

Check whether a model instance exists in the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.RssFeedGroupApi;

RssFeedGroupApi apiInstance = new RssFeedGroupApi();
String id = "id_example"; // String | Model id
try {
    InlineResponse2003 result = apiInstance.rssFeedGroupExistsGetRssFeedGroupsidExists(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RssFeedGroupApi#rssFeedGroupExistsGetRssFeedGroupsidExists");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |

### Return type

[**InlineResponse2003**](InlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="rssFeedGroupExistsHeadRssFeedGroupsid"></a>
# **rssFeedGroupExistsHeadRssFeedGroupsid**
> InlineResponse2003 rssFeedGroupExistsHeadRssFeedGroupsid(id)

Check whether a model instance exists in the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.RssFeedGroupApi;

RssFeedGroupApi apiInstance = new RssFeedGroupApi();
String id = "id_example"; // String | Model id
try {
    InlineResponse2003 result = apiInstance.rssFeedGroupExistsHeadRssFeedGroupsid(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RssFeedGroupApi#rssFeedGroupExistsHeadRssFeedGroupsid");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |

### Return type

[**InlineResponse2003**](InlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="rssFeedGroupFind"></a>
# **rssFeedGroupFind**
> List&lt;RssFeedGroup&gt; rssFeedGroupFind(filter)

Find all instances of the model matched by filter from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.RssFeedGroupApi;

RssFeedGroupApi apiInstance = new RssFeedGroupApi();
String filter = "filter_example"; // String | Filter defining fields, where, include, order, offset, and limit
try {
    List<RssFeedGroup> result = apiInstance.rssFeedGroupFind(filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RssFeedGroupApi#rssFeedGroupFind");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **String**| Filter defining fields, where, include, order, offset, and limit | [optional]

### Return type

[**List&lt;RssFeedGroup&gt;**](RssFeedGroup.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="rssFeedGroupFindById"></a>
# **rssFeedGroupFindById**
> RssFeedGroup rssFeedGroupFindById(id, filter)

Find a model instance by {{id}} from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.RssFeedGroupApi;

RssFeedGroupApi apiInstance = new RssFeedGroupApi();
String id = "id_example"; // String | Model id
String filter = "filter_example"; // String | Filter defining fields and include
try {
    RssFeedGroup result = apiInstance.rssFeedGroupFindById(id, filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RssFeedGroupApi#rssFeedGroupFindById");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |
 **filter** | **String**| Filter defining fields and include | [optional]

### Return type

[**RssFeedGroup**](RssFeedGroup.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="rssFeedGroupFindOne"></a>
# **rssFeedGroupFindOne**
> RssFeedGroup rssFeedGroupFindOne(filter)

Find first instance of the model matched by filter from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.RssFeedGroupApi;

RssFeedGroupApi apiInstance = new RssFeedGroupApi();
String filter = "filter_example"; // String | Filter defining fields, where, include, order, offset, and limit
try {
    RssFeedGroup result = apiInstance.rssFeedGroupFindOne(filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RssFeedGroupApi#rssFeedGroupFindOne");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **String**| Filter defining fields, where, include, order, offset, and limit | [optional]

### Return type

[**RssFeedGroup**](RssFeedGroup.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="rssFeedGroupPrototypeCountRssFeeds"></a>
# **rssFeedGroupPrototypeCountRssFeeds**
> InlineResponse2001 rssFeedGroupPrototypeCountRssFeeds(id, where)

Counts rssFeeds of RssFeedGroup.

### Example
```java
// Import classes:
//import com.harpoon.api.RssFeedGroupApi;

RssFeedGroupApi apiInstance = new RssFeedGroupApi();
String id = "id_example"; // String | RssFeedGroup id
String where = "where_example"; // String | Criteria to match model instances
try {
    InlineResponse2001 result = apiInstance.rssFeedGroupPrototypeCountRssFeeds(id, where);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RssFeedGroupApi#rssFeedGroupPrototypeCountRssFeeds");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| RssFeedGroup id |
 **where** | **String**| Criteria to match model instances | [optional]

### Return type

[**InlineResponse2001**](InlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="rssFeedGroupPrototypeCreateRssFeeds"></a>
# **rssFeedGroupPrototypeCreateRssFeeds**
> RssFeed rssFeedGroupPrototypeCreateRssFeeds(id, data)

Creates a new instance in rssFeeds of this model.

### Example
```java
// Import classes:
//import com.harpoon.api.RssFeedGroupApi;

RssFeedGroupApi apiInstance = new RssFeedGroupApi();
String id = "id_example"; // String | RssFeedGroup id
RssFeed data = new RssFeed(); // RssFeed | 
try {
    RssFeed result = apiInstance.rssFeedGroupPrototypeCreateRssFeeds(id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RssFeedGroupApi#rssFeedGroupPrototypeCreateRssFeeds");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| RssFeedGroup id |
 **data** | [**RssFeed**](RssFeed.md)|  | [optional]

### Return type

[**RssFeed**](RssFeed.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="rssFeedGroupPrototypeDeleteRssFeeds"></a>
# **rssFeedGroupPrototypeDeleteRssFeeds**
> rssFeedGroupPrototypeDeleteRssFeeds(id)

Deletes all rssFeeds of this model.

### Example
```java
// Import classes:
//import com.harpoon.api.RssFeedGroupApi;

RssFeedGroupApi apiInstance = new RssFeedGroupApi();
String id = "id_example"; // String | RssFeedGroup id
try {
    apiInstance.rssFeedGroupPrototypeDeleteRssFeeds(id);
} catch (ApiException e) {
    System.err.println("Exception when calling RssFeedGroupApi#rssFeedGroupPrototypeDeleteRssFeeds");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| RssFeedGroup id |

### Return type

null (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="rssFeedGroupPrototypeDestroyByIdRssFeeds"></a>
# **rssFeedGroupPrototypeDestroyByIdRssFeeds**
> rssFeedGroupPrototypeDestroyByIdRssFeeds(fk, id)

Delete a related item by id for rssFeeds.

### Example
```java
// Import classes:
//import com.harpoon.api.RssFeedGroupApi;

RssFeedGroupApi apiInstance = new RssFeedGroupApi();
String fk = "fk_example"; // String | Foreign key for rssFeeds
String id = "id_example"; // String | RssFeedGroup id
try {
    apiInstance.rssFeedGroupPrototypeDestroyByIdRssFeeds(fk, id);
} catch (ApiException e) {
    System.err.println("Exception when calling RssFeedGroupApi#rssFeedGroupPrototypeDestroyByIdRssFeeds");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for rssFeeds |
 **id** | **String**| RssFeedGroup id |

### Return type

null (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="rssFeedGroupPrototypeFindByIdRssFeeds"></a>
# **rssFeedGroupPrototypeFindByIdRssFeeds**
> RssFeed rssFeedGroupPrototypeFindByIdRssFeeds(fk, id)

Find a related item by id for rssFeeds.

### Example
```java
// Import classes:
//import com.harpoon.api.RssFeedGroupApi;

RssFeedGroupApi apiInstance = new RssFeedGroupApi();
String fk = "fk_example"; // String | Foreign key for rssFeeds
String id = "id_example"; // String | RssFeedGroup id
try {
    RssFeed result = apiInstance.rssFeedGroupPrototypeFindByIdRssFeeds(fk, id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RssFeedGroupApi#rssFeedGroupPrototypeFindByIdRssFeeds");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for rssFeeds |
 **id** | **String**| RssFeedGroup id |

### Return type

[**RssFeed**](RssFeed.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="rssFeedGroupPrototypeGetRssFeeds"></a>
# **rssFeedGroupPrototypeGetRssFeeds**
> List&lt;RssFeed&gt; rssFeedGroupPrototypeGetRssFeeds(id, filter)

Queries rssFeeds of RssFeedGroup.

### Example
```java
// Import classes:
//import com.harpoon.api.RssFeedGroupApi;

RssFeedGroupApi apiInstance = new RssFeedGroupApi();
String id = "id_example"; // String | RssFeedGroup id
String filter = "filter_example"; // String | 
try {
    List<RssFeed> result = apiInstance.rssFeedGroupPrototypeGetRssFeeds(id, filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RssFeedGroupApi#rssFeedGroupPrototypeGetRssFeeds");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| RssFeedGroup id |
 **filter** | **String**|  | [optional]

### Return type

[**List&lt;RssFeed&gt;**](RssFeed.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="rssFeedGroupPrototypeUpdateAttributesPatchRssFeedGroupsid"></a>
# **rssFeedGroupPrototypeUpdateAttributesPatchRssFeedGroupsid**
> RssFeedGroup rssFeedGroupPrototypeUpdateAttributesPatchRssFeedGroupsid(id, data)

Patch attributes for a model instance and persist it into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.RssFeedGroupApi;

RssFeedGroupApi apiInstance = new RssFeedGroupApi();
String id = "id_example"; // String | RssFeedGroup id
RssFeedGroup data = new RssFeedGroup(); // RssFeedGroup | An object of model property name/value pairs
try {
    RssFeedGroup result = apiInstance.rssFeedGroupPrototypeUpdateAttributesPatchRssFeedGroupsid(id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RssFeedGroupApi#rssFeedGroupPrototypeUpdateAttributesPatchRssFeedGroupsid");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| RssFeedGroup id |
 **data** | [**RssFeedGroup**](RssFeedGroup.md)| An object of model property name/value pairs | [optional]

### Return type

[**RssFeedGroup**](RssFeedGroup.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="rssFeedGroupPrototypeUpdateAttributesPutRssFeedGroupsid"></a>
# **rssFeedGroupPrototypeUpdateAttributesPutRssFeedGroupsid**
> RssFeedGroup rssFeedGroupPrototypeUpdateAttributesPutRssFeedGroupsid(id, data)

Patch attributes for a model instance and persist it into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.RssFeedGroupApi;

RssFeedGroupApi apiInstance = new RssFeedGroupApi();
String id = "id_example"; // String | RssFeedGroup id
RssFeedGroup data = new RssFeedGroup(); // RssFeedGroup | An object of model property name/value pairs
try {
    RssFeedGroup result = apiInstance.rssFeedGroupPrototypeUpdateAttributesPutRssFeedGroupsid(id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RssFeedGroupApi#rssFeedGroupPrototypeUpdateAttributesPutRssFeedGroupsid");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| RssFeedGroup id |
 **data** | [**RssFeedGroup**](RssFeedGroup.md)| An object of model property name/value pairs | [optional]

### Return type

[**RssFeedGroup**](RssFeedGroup.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="rssFeedGroupPrototypeUpdateByIdRssFeeds"></a>
# **rssFeedGroupPrototypeUpdateByIdRssFeeds**
> RssFeed rssFeedGroupPrototypeUpdateByIdRssFeeds(fk, id, data)

Update a related item by id for rssFeeds.

### Example
```java
// Import classes:
//import com.harpoon.api.RssFeedGroupApi;

RssFeedGroupApi apiInstance = new RssFeedGroupApi();
String fk = "fk_example"; // String | Foreign key for rssFeeds
String id = "id_example"; // String | RssFeedGroup id
RssFeed data = new RssFeed(); // RssFeed | 
try {
    RssFeed result = apiInstance.rssFeedGroupPrototypeUpdateByIdRssFeeds(fk, id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RssFeedGroupApi#rssFeedGroupPrototypeUpdateByIdRssFeeds");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for rssFeeds |
 **id** | **String**| RssFeedGroup id |
 **data** | [**RssFeed**](RssFeed.md)|  | [optional]

### Return type

[**RssFeed**](RssFeed.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="rssFeedGroupReplaceById"></a>
# **rssFeedGroupReplaceById**
> RssFeedGroup rssFeedGroupReplaceById(id, data)

Replace attributes for a model instance and persist it into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.RssFeedGroupApi;

RssFeedGroupApi apiInstance = new RssFeedGroupApi();
String id = "id_example"; // String | Model id
RssFeedGroup data = new RssFeedGroup(); // RssFeedGroup | Model instance data
try {
    RssFeedGroup result = apiInstance.rssFeedGroupReplaceById(id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RssFeedGroupApi#rssFeedGroupReplaceById");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |
 **data** | [**RssFeedGroup**](RssFeedGroup.md)| Model instance data | [optional]

### Return type

[**RssFeedGroup**](RssFeedGroup.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="rssFeedGroupReplaceOrCreate"></a>
# **rssFeedGroupReplaceOrCreate**
> RssFeedGroup rssFeedGroupReplaceOrCreate(data)

Replace an existing model instance or insert a new one into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.RssFeedGroupApi;

RssFeedGroupApi apiInstance = new RssFeedGroupApi();
RssFeedGroup data = new RssFeedGroup(); // RssFeedGroup | Model instance data
try {
    RssFeedGroup result = apiInstance.rssFeedGroupReplaceOrCreate(data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RssFeedGroupApi#rssFeedGroupReplaceOrCreate");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**RssFeedGroup**](RssFeedGroup.md)| Model instance data | [optional]

### Return type

[**RssFeedGroup**](RssFeedGroup.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="rssFeedGroupUpdateAll"></a>
# **rssFeedGroupUpdateAll**
> InlineResponse2002 rssFeedGroupUpdateAll(where, data)

Update instances of the model matched by {{where}} from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.RssFeedGroupApi;

RssFeedGroupApi apiInstance = new RssFeedGroupApi();
String where = "where_example"; // String | Criteria to match model instances
RssFeedGroup data = new RssFeedGroup(); // RssFeedGroup | An object of model property name/value pairs
try {
    InlineResponse2002 result = apiInstance.rssFeedGroupUpdateAll(where, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RssFeedGroupApi#rssFeedGroupUpdateAll");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional]
 **data** | [**RssFeedGroup**](RssFeedGroup.md)| An object of model property name/value pairs | [optional]

### Return type

[**InlineResponse2002**](InlineResponse2002.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="rssFeedGroupUpsertPatchRssFeedGroups"></a>
# **rssFeedGroupUpsertPatchRssFeedGroups**
> RssFeedGroup rssFeedGroupUpsertPatchRssFeedGroups(data)

Patch an existing model instance or insert a new one into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.RssFeedGroupApi;

RssFeedGroupApi apiInstance = new RssFeedGroupApi();
RssFeedGroup data = new RssFeedGroup(); // RssFeedGroup | Model instance data
try {
    RssFeedGroup result = apiInstance.rssFeedGroupUpsertPatchRssFeedGroups(data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RssFeedGroupApi#rssFeedGroupUpsertPatchRssFeedGroups");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**RssFeedGroup**](RssFeedGroup.md)| Model instance data | [optional]

### Return type

[**RssFeedGroup**](RssFeedGroup.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="rssFeedGroupUpsertPutRssFeedGroups"></a>
# **rssFeedGroupUpsertPutRssFeedGroups**
> RssFeedGroup rssFeedGroupUpsertPutRssFeedGroups(data)

Patch an existing model instance or insert a new one into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.RssFeedGroupApi;

RssFeedGroupApi apiInstance = new RssFeedGroupApi();
RssFeedGroup data = new RssFeedGroup(); // RssFeedGroup | Model instance data
try {
    RssFeedGroup result = apiInstance.rssFeedGroupUpsertPutRssFeedGroups(data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RssFeedGroupApi#rssFeedGroupUpsertPutRssFeedGroups");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**RssFeedGroup**](RssFeedGroup.md)| Model instance data | [optional]

### Return type

[**RssFeedGroup**](RssFeedGroup.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="rssFeedGroupUpsertWithWhere"></a>
# **rssFeedGroupUpsertWithWhere**
> RssFeedGroup rssFeedGroupUpsertWithWhere(where, data)

Update an existing model instance or insert a new one into the data source based on the where criteria.

### Example
```java
// Import classes:
//import com.harpoon.api.RssFeedGroupApi;

RssFeedGroupApi apiInstance = new RssFeedGroupApi();
String where = "where_example"; // String | Criteria to match model instances
RssFeedGroup data = new RssFeedGroup(); // RssFeedGroup | An object of model property name/value pairs
try {
    RssFeedGroup result = apiInstance.rssFeedGroupUpsertWithWhere(where, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RssFeedGroupApi#rssFeedGroupUpsertWithWhere");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional]
 **data** | [**RssFeedGroup**](RssFeedGroup.md)| An object of model property name/value pairs | [optional]

### Return type

[**RssFeedGroup**](RssFeedGroup.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

