# StripeInvoiceItemApi

All URIs are relative to *https://apicdn.harpoonconnect.com/v1.1.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**stripeInvoiceItemCount**](StripeInvoiceItemApi.md#stripeInvoiceItemCount) | **GET** /StripeInvoiceItems/count | Count instances of the model matched by where from the data source.
[**stripeInvoiceItemCreate**](StripeInvoiceItemApi.md#stripeInvoiceItemCreate) | **POST** /StripeInvoiceItems | Create a new instance of the model and persist it into the data source.
[**stripeInvoiceItemCreateChangeStreamGetStripeInvoiceItemsChangeStream**](StripeInvoiceItemApi.md#stripeInvoiceItemCreateChangeStreamGetStripeInvoiceItemsChangeStream) | **GET** /StripeInvoiceItems/change-stream | Create a change stream.
[**stripeInvoiceItemCreateChangeStreamPostStripeInvoiceItemsChangeStream**](StripeInvoiceItemApi.md#stripeInvoiceItemCreateChangeStreamPostStripeInvoiceItemsChangeStream) | **POST** /StripeInvoiceItems/change-stream | Create a change stream.
[**stripeInvoiceItemDeleteById**](StripeInvoiceItemApi.md#stripeInvoiceItemDeleteById) | **DELETE** /StripeInvoiceItems/{id} | Delete a model instance by {{id}} from the data source.
[**stripeInvoiceItemExistsGetStripeInvoiceItemsidExists**](StripeInvoiceItemApi.md#stripeInvoiceItemExistsGetStripeInvoiceItemsidExists) | **GET** /StripeInvoiceItems/{id}/exists | Check whether a model instance exists in the data source.
[**stripeInvoiceItemExistsHeadStripeInvoiceItemsid**](StripeInvoiceItemApi.md#stripeInvoiceItemExistsHeadStripeInvoiceItemsid) | **HEAD** /StripeInvoiceItems/{id} | Check whether a model instance exists in the data source.
[**stripeInvoiceItemFind**](StripeInvoiceItemApi.md#stripeInvoiceItemFind) | **GET** /StripeInvoiceItems | Find all instances of the model matched by filter from the data source.
[**stripeInvoiceItemFindById**](StripeInvoiceItemApi.md#stripeInvoiceItemFindById) | **GET** /StripeInvoiceItems/{id} | Find a model instance by {{id}} from the data source.
[**stripeInvoiceItemFindOne**](StripeInvoiceItemApi.md#stripeInvoiceItemFindOne) | **GET** /StripeInvoiceItems/findOne | Find first instance of the model matched by filter from the data source.
[**stripeInvoiceItemPrototypeUpdateAttributesPatchStripeInvoiceItemsid**](StripeInvoiceItemApi.md#stripeInvoiceItemPrototypeUpdateAttributesPatchStripeInvoiceItemsid) | **PATCH** /StripeInvoiceItems/{id} | Patch attributes for a model instance and persist it into the data source.
[**stripeInvoiceItemPrototypeUpdateAttributesPutStripeInvoiceItemsid**](StripeInvoiceItemApi.md#stripeInvoiceItemPrototypeUpdateAttributesPutStripeInvoiceItemsid) | **PUT** /StripeInvoiceItems/{id} | Patch attributes for a model instance and persist it into the data source.
[**stripeInvoiceItemReplaceById**](StripeInvoiceItemApi.md#stripeInvoiceItemReplaceById) | **POST** /StripeInvoiceItems/{id}/replace | Replace attributes for a model instance and persist it into the data source.
[**stripeInvoiceItemReplaceOrCreate**](StripeInvoiceItemApi.md#stripeInvoiceItemReplaceOrCreate) | **POST** /StripeInvoiceItems/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
[**stripeInvoiceItemUpdateAll**](StripeInvoiceItemApi.md#stripeInvoiceItemUpdateAll) | **POST** /StripeInvoiceItems/update | Update instances of the model matched by {{where}} from the data source.
[**stripeInvoiceItemUpsertPatchStripeInvoiceItems**](StripeInvoiceItemApi.md#stripeInvoiceItemUpsertPatchStripeInvoiceItems) | **PATCH** /StripeInvoiceItems | Patch an existing model instance or insert a new one into the data source.
[**stripeInvoiceItemUpsertPutStripeInvoiceItems**](StripeInvoiceItemApi.md#stripeInvoiceItemUpsertPutStripeInvoiceItems) | **PUT** /StripeInvoiceItems | Patch an existing model instance or insert a new one into the data source.
[**stripeInvoiceItemUpsertWithWhere**](StripeInvoiceItemApi.md#stripeInvoiceItemUpsertWithWhere) | **POST** /StripeInvoiceItems/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.


<a name="stripeInvoiceItemCount"></a>
# **stripeInvoiceItemCount**
> InlineResponse2001 stripeInvoiceItemCount(where)

Count instances of the model matched by where from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.StripeInvoiceItemApi;

StripeInvoiceItemApi apiInstance = new StripeInvoiceItemApi();
String where = "where_example"; // String | Criteria to match model instances
try {
    InlineResponse2001 result = apiInstance.stripeInvoiceItemCount(where);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling StripeInvoiceItemApi#stripeInvoiceItemCount");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional]

### Return type

[**InlineResponse2001**](InlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripeInvoiceItemCreate"></a>
# **stripeInvoiceItemCreate**
> StripeInvoiceItem stripeInvoiceItemCreate(data)

Create a new instance of the model and persist it into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.StripeInvoiceItemApi;

StripeInvoiceItemApi apiInstance = new StripeInvoiceItemApi();
StripeInvoiceItem data = new StripeInvoiceItem(); // StripeInvoiceItem | Model instance data
try {
    StripeInvoiceItem result = apiInstance.stripeInvoiceItemCreate(data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling StripeInvoiceItemApi#stripeInvoiceItemCreate");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**StripeInvoiceItem**](StripeInvoiceItem.md)| Model instance data | [optional]

### Return type

[**StripeInvoiceItem**](StripeInvoiceItem.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripeInvoiceItemCreateChangeStreamGetStripeInvoiceItemsChangeStream"></a>
# **stripeInvoiceItemCreateChangeStreamGetStripeInvoiceItemsChangeStream**
> File stripeInvoiceItemCreateChangeStreamGetStripeInvoiceItemsChangeStream(options)

Create a change stream.

### Example
```java
// Import classes:
//import com.harpoon.api.StripeInvoiceItemApi;

StripeInvoiceItemApi apiInstance = new StripeInvoiceItemApi();
String options = "options_example"; // String | 
try {
    File result = apiInstance.stripeInvoiceItemCreateChangeStreamGetStripeInvoiceItemsChangeStream(options);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling StripeInvoiceItemApi#stripeInvoiceItemCreateChangeStreamGetStripeInvoiceItemsChangeStream");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **String**|  | [optional]

### Return type

[**File**](File.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripeInvoiceItemCreateChangeStreamPostStripeInvoiceItemsChangeStream"></a>
# **stripeInvoiceItemCreateChangeStreamPostStripeInvoiceItemsChangeStream**
> File stripeInvoiceItemCreateChangeStreamPostStripeInvoiceItemsChangeStream(options)

Create a change stream.

### Example
```java
// Import classes:
//import com.harpoon.api.StripeInvoiceItemApi;

StripeInvoiceItemApi apiInstance = new StripeInvoiceItemApi();
String options = "options_example"; // String | 
try {
    File result = apiInstance.stripeInvoiceItemCreateChangeStreamPostStripeInvoiceItemsChangeStream(options);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling StripeInvoiceItemApi#stripeInvoiceItemCreateChangeStreamPostStripeInvoiceItemsChangeStream");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **String**|  | [optional]

### Return type

[**File**](File.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripeInvoiceItemDeleteById"></a>
# **stripeInvoiceItemDeleteById**
> Object stripeInvoiceItemDeleteById(id)

Delete a model instance by {{id}} from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.StripeInvoiceItemApi;

StripeInvoiceItemApi apiInstance = new StripeInvoiceItemApi();
String id = "id_example"; // String | Model id
try {
    Object result = apiInstance.stripeInvoiceItemDeleteById(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling StripeInvoiceItemApi#stripeInvoiceItemDeleteById");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |

### Return type

**Object**

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripeInvoiceItemExistsGetStripeInvoiceItemsidExists"></a>
# **stripeInvoiceItemExistsGetStripeInvoiceItemsidExists**
> InlineResponse2003 stripeInvoiceItemExistsGetStripeInvoiceItemsidExists(id)

Check whether a model instance exists in the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.StripeInvoiceItemApi;

StripeInvoiceItemApi apiInstance = new StripeInvoiceItemApi();
String id = "id_example"; // String | Model id
try {
    InlineResponse2003 result = apiInstance.stripeInvoiceItemExistsGetStripeInvoiceItemsidExists(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling StripeInvoiceItemApi#stripeInvoiceItemExistsGetStripeInvoiceItemsidExists");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |

### Return type

[**InlineResponse2003**](InlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripeInvoiceItemExistsHeadStripeInvoiceItemsid"></a>
# **stripeInvoiceItemExistsHeadStripeInvoiceItemsid**
> InlineResponse2003 stripeInvoiceItemExistsHeadStripeInvoiceItemsid(id)

Check whether a model instance exists in the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.StripeInvoiceItemApi;

StripeInvoiceItemApi apiInstance = new StripeInvoiceItemApi();
String id = "id_example"; // String | Model id
try {
    InlineResponse2003 result = apiInstance.stripeInvoiceItemExistsHeadStripeInvoiceItemsid(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling StripeInvoiceItemApi#stripeInvoiceItemExistsHeadStripeInvoiceItemsid");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |

### Return type

[**InlineResponse2003**](InlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripeInvoiceItemFind"></a>
# **stripeInvoiceItemFind**
> List&lt;StripeInvoiceItem&gt; stripeInvoiceItemFind(filter)

Find all instances of the model matched by filter from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.StripeInvoiceItemApi;

StripeInvoiceItemApi apiInstance = new StripeInvoiceItemApi();
String filter = "filter_example"; // String | Filter defining fields, where, include, order, offset, and limit
try {
    List<StripeInvoiceItem> result = apiInstance.stripeInvoiceItemFind(filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling StripeInvoiceItemApi#stripeInvoiceItemFind");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **String**| Filter defining fields, where, include, order, offset, and limit | [optional]

### Return type

[**List&lt;StripeInvoiceItem&gt;**](StripeInvoiceItem.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripeInvoiceItemFindById"></a>
# **stripeInvoiceItemFindById**
> StripeInvoiceItem stripeInvoiceItemFindById(id, filter)

Find a model instance by {{id}} from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.StripeInvoiceItemApi;

StripeInvoiceItemApi apiInstance = new StripeInvoiceItemApi();
String id = "id_example"; // String | Model id
String filter = "filter_example"; // String | Filter defining fields and include
try {
    StripeInvoiceItem result = apiInstance.stripeInvoiceItemFindById(id, filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling StripeInvoiceItemApi#stripeInvoiceItemFindById");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |
 **filter** | **String**| Filter defining fields and include | [optional]

### Return type

[**StripeInvoiceItem**](StripeInvoiceItem.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripeInvoiceItemFindOne"></a>
# **stripeInvoiceItemFindOne**
> StripeInvoiceItem stripeInvoiceItemFindOne(filter)

Find first instance of the model matched by filter from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.StripeInvoiceItemApi;

StripeInvoiceItemApi apiInstance = new StripeInvoiceItemApi();
String filter = "filter_example"; // String | Filter defining fields, where, include, order, offset, and limit
try {
    StripeInvoiceItem result = apiInstance.stripeInvoiceItemFindOne(filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling StripeInvoiceItemApi#stripeInvoiceItemFindOne");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **String**| Filter defining fields, where, include, order, offset, and limit | [optional]

### Return type

[**StripeInvoiceItem**](StripeInvoiceItem.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripeInvoiceItemPrototypeUpdateAttributesPatchStripeInvoiceItemsid"></a>
# **stripeInvoiceItemPrototypeUpdateAttributesPatchStripeInvoiceItemsid**
> StripeInvoiceItem stripeInvoiceItemPrototypeUpdateAttributesPatchStripeInvoiceItemsid(id, data)

Patch attributes for a model instance and persist it into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.StripeInvoiceItemApi;

StripeInvoiceItemApi apiInstance = new StripeInvoiceItemApi();
String id = "id_example"; // String | StripeInvoiceItem id
StripeInvoiceItem data = new StripeInvoiceItem(); // StripeInvoiceItem | An object of model property name/value pairs
try {
    StripeInvoiceItem result = apiInstance.stripeInvoiceItemPrototypeUpdateAttributesPatchStripeInvoiceItemsid(id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling StripeInvoiceItemApi#stripeInvoiceItemPrototypeUpdateAttributesPatchStripeInvoiceItemsid");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| StripeInvoiceItem id |
 **data** | [**StripeInvoiceItem**](StripeInvoiceItem.md)| An object of model property name/value pairs | [optional]

### Return type

[**StripeInvoiceItem**](StripeInvoiceItem.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripeInvoiceItemPrototypeUpdateAttributesPutStripeInvoiceItemsid"></a>
# **stripeInvoiceItemPrototypeUpdateAttributesPutStripeInvoiceItemsid**
> StripeInvoiceItem stripeInvoiceItemPrototypeUpdateAttributesPutStripeInvoiceItemsid(id, data)

Patch attributes for a model instance and persist it into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.StripeInvoiceItemApi;

StripeInvoiceItemApi apiInstance = new StripeInvoiceItemApi();
String id = "id_example"; // String | StripeInvoiceItem id
StripeInvoiceItem data = new StripeInvoiceItem(); // StripeInvoiceItem | An object of model property name/value pairs
try {
    StripeInvoiceItem result = apiInstance.stripeInvoiceItemPrototypeUpdateAttributesPutStripeInvoiceItemsid(id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling StripeInvoiceItemApi#stripeInvoiceItemPrototypeUpdateAttributesPutStripeInvoiceItemsid");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| StripeInvoiceItem id |
 **data** | [**StripeInvoiceItem**](StripeInvoiceItem.md)| An object of model property name/value pairs | [optional]

### Return type

[**StripeInvoiceItem**](StripeInvoiceItem.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripeInvoiceItemReplaceById"></a>
# **stripeInvoiceItemReplaceById**
> StripeInvoiceItem stripeInvoiceItemReplaceById(id, data)

Replace attributes for a model instance and persist it into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.StripeInvoiceItemApi;

StripeInvoiceItemApi apiInstance = new StripeInvoiceItemApi();
String id = "id_example"; // String | Model id
StripeInvoiceItem data = new StripeInvoiceItem(); // StripeInvoiceItem | Model instance data
try {
    StripeInvoiceItem result = apiInstance.stripeInvoiceItemReplaceById(id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling StripeInvoiceItemApi#stripeInvoiceItemReplaceById");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |
 **data** | [**StripeInvoiceItem**](StripeInvoiceItem.md)| Model instance data | [optional]

### Return type

[**StripeInvoiceItem**](StripeInvoiceItem.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripeInvoiceItemReplaceOrCreate"></a>
# **stripeInvoiceItemReplaceOrCreate**
> StripeInvoiceItem stripeInvoiceItemReplaceOrCreate(data)

Replace an existing model instance or insert a new one into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.StripeInvoiceItemApi;

StripeInvoiceItemApi apiInstance = new StripeInvoiceItemApi();
StripeInvoiceItem data = new StripeInvoiceItem(); // StripeInvoiceItem | Model instance data
try {
    StripeInvoiceItem result = apiInstance.stripeInvoiceItemReplaceOrCreate(data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling StripeInvoiceItemApi#stripeInvoiceItemReplaceOrCreate");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**StripeInvoiceItem**](StripeInvoiceItem.md)| Model instance data | [optional]

### Return type

[**StripeInvoiceItem**](StripeInvoiceItem.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripeInvoiceItemUpdateAll"></a>
# **stripeInvoiceItemUpdateAll**
> InlineResponse2002 stripeInvoiceItemUpdateAll(where, data)

Update instances of the model matched by {{where}} from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.StripeInvoiceItemApi;

StripeInvoiceItemApi apiInstance = new StripeInvoiceItemApi();
String where = "where_example"; // String | Criteria to match model instances
StripeInvoiceItem data = new StripeInvoiceItem(); // StripeInvoiceItem | An object of model property name/value pairs
try {
    InlineResponse2002 result = apiInstance.stripeInvoiceItemUpdateAll(where, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling StripeInvoiceItemApi#stripeInvoiceItemUpdateAll");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional]
 **data** | [**StripeInvoiceItem**](StripeInvoiceItem.md)| An object of model property name/value pairs | [optional]

### Return type

[**InlineResponse2002**](InlineResponse2002.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripeInvoiceItemUpsertPatchStripeInvoiceItems"></a>
# **stripeInvoiceItemUpsertPatchStripeInvoiceItems**
> StripeInvoiceItem stripeInvoiceItemUpsertPatchStripeInvoiceItems(data)

Patch an existing model instance or insert a new one into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.StripeInvoiceItemApi;

StripeInvoiceItemApi apiInstance = new StripeInvoiceItemApi();
StripeInvoiceItem data = new StripeInvoiceItem(); // StripeInvoiceItem | Model instance data
try {
    StripeInvoiceItem result = apiInstance.stripeInvoiceItemUpsertPatchStripeInvoiceItems(data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling StripeInvoiceItemApi#stripeInvoiceItemUpsertPatchStripeInvoiceItems");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**StripeInvoiceItem**](StripeInvoiceItem.md)| Model instance data | [optional]

### Return type

[**StripeInvoiceItem**](StripeInvoiceItem.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripeInvoiceItemUpsertPutStripeInvoiceItems"></a>
# **stripeInvoiceItemUpsertPutStripeInvoiceItems**
> StripeInvoiceItem stripeInvoiceItemUpsertPutStripeInvoiceItems(data)

Patch an existing model instance or insert a new one into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.StripeInvoiceItemApi;

StripeInvoiceItemApi apiInstance = new StripeInvoiceItemApi();
StripeInvoiceItem data = new StripeInvoiceItem(); // StripeInvoiceItem | Model instance data
try {
    StripeInvoiceItem result = apiInstance.stripeInvoiceItemUpsertPutStripeInvoiceItems(data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling StripeInvoiceItemApi#stripeInvoiceItemUpsertPutStripeInvoiceItems");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**StripeInvoiceItem**](StripeInvoiceItem.md)| Model instance data | [optional]

### Return type

[**StripeInvoiceItem**](StripeInvoiceItem.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripeInvoiceItemUpsertWithWhere"></a>
# **stripeInvoiceItemUpsertWithWhere**
> StripeInvoiceItem stripeInvoiceItemUpsertWithWhere(where, data)

Update an existing model instance or insert a new one into the data source based on the where criteria.

### Example
```java
// Import classes:
//import com.harpoon.api.StripeInvoiceItemApi;

StripeInvoiceItemApi apiInstance = new StripeInvoiceItemApi();
String where = "where_example"; // String | Criteria to match model instances
StripeInvoiceItem data = new StripeInvoiceItem(); // StripeInvoiceItem | An object of model property name/value pairs
try {
    StripeInvoiceItem result = apiInstance.stripeInvoiceItemUpsertWithWhere(where, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling StripeInvoiceItemApi#stripeInvoiceItemUpsertWithWhere");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional]
 **data** | [**StripeInvoiceItem**](StripeInvoiceItem.md)| An object of model property name/value pairs | [optional]

### Return type

[**StripeInvoiceItem**](StripeInvoiceItem.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

