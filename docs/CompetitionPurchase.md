
# CompetitionPurchase

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**orderId** | **Double** |  |  [optional]
**name** | **String** |  |  [optional]
**competition** | [**Competition**](Competition.md) |  |  [optional]
**redemptionType** | **String** |  |  [optional]
**unlockTime** | **Double** |  |  [optional]
**code** | **String** |  |  [optional]
**qrcode** | **String** |  |  [optional]
**isAvailable** | **Boolean** |  |  [optional]
**status** | **String** |  |  [optional]
**createdAt** | [**Date**](Date.md) |  |  [optional]
**expiredAt** | [**Date**](Date.md) |  |  [optional]
**redeemedAt** | [**Date**](Date.md) |  |  [optional]
**redeemedByTerminal** | **String** |  |  [optional]
**basePrice** | **Double** |  |  [optional]
**ticketPrice** | **Double** |  |  [optional]
**currency** | **String** |  |  [optional]
**attendee** | [**Customer**](Customer.md) |  |  [optional]
**chanceCount** | **Double** |  |  [optional]
**winner** | [**CompetitionWinner**](CompetitionWinner.md) |  |  [optional]
**id** | **Double** |  |  [optional]



