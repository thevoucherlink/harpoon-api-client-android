# BrandApi

All URIs are relative to *https://apicdn.harpoonconnect.com/v1.1.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**brandAllFeeds**](BrandApi.md#brandAllFeeds) | **GET** /Brands/feeds | 
[**brandCompetitions**](BrandApi.md#brandCompetitions) | **GET** /Brands/{id}/competitions | 
[**brandCoupons**](BrandApi.md#brandCoupons) | **GET** /Brands/{id}/coupons | 
[**brandDealGroups**](BrandApi.md#brandDealGroups) | **GET** /Brands/{id}/dealGroups | 
[**brandDealPaids**](BrandApi.md#brandDealPaids) | **GET** /Brands/{id}/dealPaids | 
[**brandDeals**](BrandApi.md#brandDeals) | **GET** /Brands/{id}/deals | 
[**brandEvents**](BrandApi.md#brandEvents) | **GET** /Brands/{id}/events | 
[**brandFeeds**](BrandApi.md#brandFeeds) | **GET** /Brands/{id}/feeds | 
[**brandFind**](BrandApi.md#brandFind) | **GET** /Brands | Find all instances of the model matched by filter from the data source.
[**brandFindById**](BrandApi.md#brandFindById) | **GET** /Brands/{id} | Find a model instance by {{id}} from the data source.
[**brandFindOne**](BrandApi.md#brandFindOne) | **GET** /Brands/findOne | Find first instance of the model matched by filter from the data source.
[**brandFollowers**](BrandApi.md#brandFollowers) | **GET** /Brands/{id}/followers | 
[**brandFollowersCreate**](BrandApi.md#brandFollowersCreate) | **PUT** /Brands/{id}/followers/me | 
[**brandFollowersDelete**](BrandApi.md#brandFollowersDelete) | **DELETE** /Brands/{id}/followers/me | 
[**brandOffers**](BrandApi.md#brandOffers) | **GET** /Brands/{id}/offers | 
[**brandReplaceById**](BrandApi.md#brandReplaceById) | **POST** /Brands/{id}/replace | Replace attributes for a model instance and persist it into the data source.
[**brandReplaceOrCreate**](BrandApi.md#brandReplaceOrCreate) | **POST** /Brands/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
[**brandUpsertWithWhere**](BrandApi.md#brandUpsertWithWhere) | **POST** /Brands/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.
[**brandVenues**](BrandApi.md#brandVenues) | **GET** /Brands/{id}/venues | 


<a name="brandAllFeeds"></a>
# **brandAllFeeds**
> List&lt;BrandFeed&gt; brandAllFeeds(filter)



### Example
```java
// Import classes:
//import com.harpoon.api.BrandApi;

BrandApi apiInstance = new BrandApi();
String filter = "filter_example"; // String | Filter defining fields and include
try {
    List<BrandFeed> result = apiInstance.brandAllFeeds(filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BrandApi#brandAllFeeds");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **String**| Filter defining fields and include | [optional]

### Return type

[**List&lt;BrandFeed&gt;**](BrandFeed.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="brandCompetitions"></a>
# **brandCompetitions**
> List&lt;Competition&gt; brandCompetitions(id, filter)



### Example
```java
// Import classes:
//import com.harpoon.api.BrandApi;

BrandApi apiInstance = new BrandApi();
String id = "id_example"; // String | Model id
String filter = "filter_example"; // String | Filter defining fields and include
try {
    List<Competition> result = apiInstance.brandCompetitions(id, filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BrandApi#brandCompetitions");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |
 **filter** | **String**| Filter defining fields and include | [optional]

### Return type

[**List&lt;Competition&gt;**](Competition.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="brandCoupons"></a>
# **brandCoupons**
> List&lt;Coupon&gt; brandCoupons(id, filter)



### Example
```java
// Import classes:
//import com.harpoon.api.BrandApi;

BrandApi apiInstance = new BrandApi();
String id = "id_example"; // String | Model id
String filter = "filter_example"; // String | Filter defining fields and include
try {
    List<Coupon> result = apiInstance.brandCoupons(id, filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BrandApi#brandCoupons");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |
 **filter** | **String**| Filter defining fields and include | [optional]

### Return type

[**List&lt;Coupon&gt;**](Coupon.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="brandDealGroups"></a>
# **brandDealGroups**
> List&lt;DealGroup&gt; brandDealGroups(id, filter)



### Example
```java
// Import classes:
//import com.harpoon.api.BrandApi;

BrandApi apiInstance = new BrandApi();
String id = "id_example"; // String | Model id
String filter = "filter_example"; // String | Filter defining fields and include
try {
    List<DealGroup> result = apiInstance.brandDealGroups(id, filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BrandApi#brandDealGroups");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |
 **filter** | **String**| Filter defining fields and include | [optional]

### Return type

[**List&lt;DealGroup&gt;**](DealGroup.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="brandDealPaids"></a>
# **brandDealPaids**
> List&lt;DealPaid&gt; brandDealPaids(id, filter)



### Example
```java
// Import classes:
//import com.harpoon.api.BrandApi;

BrandApi apiInstance = new BrandApi();
String id = "id_example"; // String | Model id
String filter = "filter_example"; // String | Filter defining fields and include
try {
    List<DealPaid> result = apiInstance.brandDealPaids(id, filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BrandApi#brandDealPaids");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |
 **filter** | **String**| Filter defining fields and include | [optional]

### Return type

[**List&lt;DealPaid&gt;**](DealPaid.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="brandDeals"></a>
# **brandDeals**
> List&lt;Deal&gt; brandDeals(id, filter)



### Example
```java
// Import classes:
//import com.harpoon.api.BrandApi;

BrandApi apiInstance = new BrandApi();
String id = "id_example"; // String | Model id
String filter = "filter_example"; // String | Filter defining fields and include
try {
    List<Deal> result = apiInstance.brandDeals(id, filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BrandApi#brandDeals");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |
 **filter** | **String**| Filter defining fields and include | [optional]

### Return type

[**List&lt;Deal&gt;**](Deal.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="brandEvents"></a>
# **brandEvents**
> List&lt;Event&gt; brandEvents(id, filter)



### Example
```java
// Import classes:
//import com.harpoon.api.BrandApi;

BrandApi apiInstance = new BrandApi();
String id = "id_example"; // String | Model id
String filter = "filter_example"; // String | Filter defining fields and include
try {
    List<Event> result = apiInstance.brandEvents(id, filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BrandApi#brandEvents");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |
 **filter** | **String**| Filter defining fields and include | [optional]

### Return type

[**List&lt;Event&gt;**](Event.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="brandFeeds"></a>
# **brandFeeds**
> List&lt;BrandFeed&gt; brandFeeds(id, filter)



### Example
```java
// Import classes:
//import com.harpoon.api.BrandApi;

BrandApi apiInstance = new BrandApi();
String id = "id_example"; // String | Model id
String filter = "filter_example"; // String | Filter defining fields and include
try {
    List<BrandFeed> result = apiInstance.brandFeeds(id, filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BrandApi#brandFeeds");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |
 **filter** | **String**| Filter defining fields and include | [optional]

### Return type

[**List&lt;BrandFeed&gt;**](BrandFeed.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="brandFind"></a>
# **brandFind**
> List&lt;Brand&gt; brandFind(filter)

Find all instances of the model matched by filter from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.BrandApi;

BrandApi apiInstance = new BrandApi();
String filter = "filter_example"; // String | Filter defining fields, where, include, order, offset, and limit
try {
    List<Brand> result = apiInstance.brandFind(filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BrandApi#brandFind");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **String**| Filter defining fields, where, include, order, offset, and limit | [optional]

### Return type

[**List&lt;Brand&gt;**](Brand.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="brandFindById"></a>
# **brandFindById**
> Brand brandFindById(id, filter)

Find a model instance by {{id}} from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.BrandApi;

BrandApi apiInstance = new BrandApi();
String id = "id_example"; // String | Model id
String filter = "filter_example"; // String | Filter defining fields and include
try {
    Brand result = apiInstance.brandFindById(id, filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BrandApi#brandFindById");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |
 **filter** | **String**| Filter defining fields and include | [optional]

### Return type

[**Brand**](Brand.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="brandFindOne"></a>
# **brandFindOne**
> Brand brandFindOne(filter)

Find first instance of the model matched by filter from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.BrandApi;

BrandApi apiInstance = new BrandApi();
String filter = "filter_example"; // String | Filter defining fields, where, include, order, offset, and limit
try {
    Brand result = apiInstance.brandFindOne(filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BrandApi#brandFindOne");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **String**| Filter defining fields, where, include, order, offset, and limit | [optional]

### Return type

[**Brand**](Brand.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="brandFollowers"></a>
# **brandFollowers**
> List&lt;Follower&gt; brandFollowers(id, filter)



### Example
```java
// Import classes:
//import com.harpoon.api.BrandApi;

BrandApi apiInstance = new BrandApi();
String id = "id_example"; // String | Model id
String filter = "filter_example"; // String | Filter defining fields and include
try {
    List<Follower> result = apiInstance.brandFollowers(id, filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BrandApi#brandFollowers");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |
 **filter** | **String**| Filter defining fields and include | [optional]

### Return type

[**List&lt;Follower&gt;**](Follower.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="brandFollowersCreate"></a>
# **brandFollowersCreate**
> Customer brandFollowersCreate(id)



### Example
```java
// Import classes:
//import com.harpoon.api.BrandApi;

BrandApi apiInstance = new BrandApi();
String id = "id_example"; // String | Model id
try {
    Customer result = apiInstance.brandFollowersCreate(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BrandApi#brandFollowersCreate");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |

### Return type

[**Customer**](Customer.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="brandFollowersDelete"></a>
# **brandFollowersDelete**
> Object brandFollowersDelete(id)



### Example
```java
// Import classes:
//import com.harpoon.api.BrandApi;

BrandApi apiInstance = new BrandApi();
String id = "id_example"; // String | Model id
try {
    Object result = apiInstance.brandFollowersDelete(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BrandApi#brandFollowersDelete");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |

### Return type

**Object**

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="brandOffers"></a>
# **brandOffers**
> List&lt;Offer&gt; brandOffers(id, filter)



### Example
```java
// Import classes:
//import com.harpoon.api.BrandApi;

BrandApi apiInstance = new BrandApi();
String id = "id_example"; // String | Model id
String filter = "filter_example"; // String | Filter defining fields and include
try {
    List<Offer> result = apiInstance.brandOffers(id, filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BrandApi#brandOffers");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |
 **filter** | **String**| Filter defining fields and include | [optional]

### Return type

[**List&lt;Offer&gt;**](Offer.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="brandReplaceById"></a>
# **brandReplaceById**
> Brand brandReplaceById(id, data)

Replace attributes for a model instance and persist it into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.BrandApi;

BrandApi apiInstance = new BrandApi();
String id = "id_example"; // String | Model id
Brand data = new Brand(); // Brand | Model instance data
try {
    Brand result = apiInstance.brandReplaceById(id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BrandApi#brandReplaceById");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |
 **data** | [**Brand**](Brand.md)| Model instance data | [optional]

### Return type

[**Brand**](Brand.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="brandReplaceOrCreate"></a>
# **brandReplaceOrCreate**
> Brand brandReplaceOrCreate(data)

Replace an existing model instance or insert a new one into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.BrandApi;

BrandApi apiInstance = new BrandApi();
Brand data = new Brand(); // Brand | Model instance data
try {
    Brand result = apiInstance.brandReplaceOrCreate(data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BrandApi#brandReplaceOrCreate");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**Brand**](Brand.md)| Model instance data | [optional]

### Return type

[**Brand**](Brand.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="brandUpsertWithWhere"></a>
# **brandUpsertWithWhere**
> Brand brandUpsertWithWhere(where, data)

Update an existing model instance or insert a new one into the data source based on the where criteria.

### Example
```java
// Import classes:
//import com.harpoon.api.BrandApi;

BrandApi apiInstance = new BrandApi();
String where = "where_example"; // String | Criteria to match model instances
Brand data = new Brand(); // Brand | An object of model property name/value pairs
try {
    Brand result = apiInstance.brandUpsertWithWhere(where, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BrandApi#brandUpsertWithWhere");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional]
 **data** | [**Brand**](Brand.md)| An object of model property name/value pairs | [optional]

### Return type

[**Brand**](Brand.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="brandVenues"></a>
# **brandVenues**
> List&lt;Venue&gt; brandVenues(id, filter)



### Example
```java
// Import classes:
//import com.harpoon.api.BrandApi;

BrandApi apiInstance = new BrandApi();
String id = "id_example"; // String | Model id
String filter = "filter_example"; // String | Filter defining fields and include
try {
    List<Venue> result = apiInstance.brandVenues(id, filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BrandApi#brandVenues");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |
 **filter** | **String**| Filter defining fields and include | [optional]

### Return type

[**List&lt;Venue&gt;**](Venue.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

