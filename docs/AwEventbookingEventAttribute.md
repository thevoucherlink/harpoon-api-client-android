
# AwEventbookingEventAttribute

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Double** |  | 
**eventId** | **Double** |  | 
**storeId** | **Double** |  |  [optional]
**attributeCode** | **String** |  |  [optional]
**value** | **String** |  |  [optional]



