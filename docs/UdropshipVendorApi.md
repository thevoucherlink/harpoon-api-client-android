# UdropshipVendorApi

All URIs are relative to *https://apicdn.harpoonconnect.com/v1.1.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**udropshipVendorCount**](UdropshipVendorApi.md#udropshipVendorCount) | **GET** /UdropshipVendors/count | Count instances of the model matched by where from the data source.
[**udropshipVendorCreate**](UdropshipVendorApi.md#udropshipVendorCreate) | **POST** /UdropshipVendors | Create a new instance of the model and persist it into the data source.
[**udropshipVendorCreateChangeStreamGetUdropshipVendorsChangeStream**](UdropshipVendorApi.md#udropshipVendorCreateChangeStreamGetUdropshipVendorsChangeStream) | **GET** /UdropshipVendors/change-stream | Create a change stream.
[**udropshipVendorCreateChangeStreamPostUdropshipVendorsChangeStream**](UdropshipVendorApi.md#udropshipVendorCreateChangeStreamPostUdropshipVendorsChangeStream) | **POST** /UdropshipVendors/change-stream | Create a change stream.
[**udropshipVendorDeleteById**](UdropshipVendorApi.md#udropshipVendorDeleteById) | **DELETE** /UdropshipVendors/{id} | Delete a model instance by {{id}} from the data source.
[**udropshipVendorExistsGetUdropshipVendorsidExists**](UdropshipVendorApi.md#udropshipVendorExistsGetUdropshipVendorsidExists) | **GET** /UdropshipVendors/{id}/exists | Check whether a model instance exists in the data source.
[**udropshipVendorExistsHeadUdropshipVendorsid**](UdropshipVendorApi.md#udropshipVendorExistsHeadUdropshipVendorsid) | **HEAD** /UdropshipVendors/{id} | Check whether a model instance exists in the data source.
[**udropshipVendorFind**](UdropshipVendorApi.md#udropshipVendorFind) | **GET** /UdropshipVendors | Find all instances of the model matched by filter from the data source.
[**udropshipVendorFindById**](UdropshipVendorApi.md#udropshipVendorFindById) | **GET** /UdropshipVendors/{id} | Find a model instance by {{id}} from the data source.
[**udropshipVendorFindOne**](UdropshipVendorApi.md#udropshipVendorFindOne) | **GET** /UdropshipVendors/findOne | Find first instance of the model matched by filter from the data source.
[**udropshipVendorPrototypeCountCatalogProducts**](UdropshipVendorApi.md#udropshipVendorPrototypeCountCatalogProducts) | **GET** /UdropshipVendors/{id}/catalogProducts/count | Counts catalogProducts of UdropshipVendor.
[**udropshipVendorPrototypeCountHarpoonHpublicApplicationpartners**](UdropshipVendorApi.md#udropshipVendorPrototypeCountHarpoonHpublicApplicationpartners) | **GET** /UdropshipVendors/{id}/harpoonHpublicApplicationpartners/count | Counts harpoonHpublicApplicationpartners of UdropshipVendor.
[**udropshipVendorPrototypeCountHarpoonHpublicv12VendorAppCategories**](UdropshipVendorApi.md#udropshipVendorPrototypeCountHarpoonHpublicv12VendorAppCategories) | **GET** /UdropshipVendors/{id}/harpoonHpublicv12VendorAppCategories/count | Counts harpoonHpublicv12VendorAppCategories of UdropshipVendor.
[**udropshipVendorPrototypeCountPartners**](UdropshipVendorApi.md#udropshipVendorPrototypeCountPartners) | **GET** /UdropshipVendors/{id}/partners/count | Counts partners of UdropshipVendor.
[**udropshipVendorPrototypeCountUdropshipVendorProducts**](UdropshipVendorApi.md#udropshipVendorPrototypeCountUdropshipVendorProducts) | **GET** /UdropshipVendors/{id}/udropshipVendorProducts/count | Counts udropshipVendorProducts of UdropshipVendor.
[**udropshipVendorPrototypeCountVendorLocations**](UdropshipVendorApi.md#udropshipVendorPrototypeCountVendorLocations) | **GET** /UdropshipVendors/{id}/vendorLocations/count | Counts vendorLocations of UdropshipVendor.
[**udropshipVendorPrototypeCreateCatalogProducts**](UdropshipVendorApi.md#udropshipVendorPrototypeCreateCatalogProducts) | **POST** /UdropshipVendors/{id}/catalogProducts | Creates a new instance in catalogProducts of this model.
[**udropshipVendorPrototypeCreateConfig**](UdropshipVendorApi.md#udropshipVendorPrototypeCreateConfig) | **POST** /UdropshipVendors/{id}/config | Creates a new instance in config of this model.
[**udropshipVendorPrototypeCreateHarpoonHpublicApplicationpartners**](UdropshipVendorApi.md#udropshipVendorPrototypeCreateHarpoonHpublicApplicationpartners) | **POST** /UdropshipVendors/{id}/harpoonHpublicApplicationpartners | Creates a new instance in harpoonHpublicApplicationpartners of this model.
[**udropshipVendorPrototypeCreateHarpoonHpublicv12VendorAppCategories**](UdropshipVendorApi.md#udropshipVendorPrototypeCreateHarpoonHpublicv12VendorAppCategories) | **POST** /UdropshipVendors/{id}/harpoonHpublicv12VendorAppCategories | Creates a new instance in harpoonHpublicv12VendorAppCategories of this model.
[**udropshipVendorPrototypeCreatePartners**](UdropshipVendorApi.md#udropshipVendorPrototypeCreatePartners) | **POST** /UdropshipVendors/{id}/partners | Creates a new instance in partners of this model.
[**udropshipVendorPrototypeCreateUdropshipVendorProducts**](UdropshipVendorApi.md#udropshipVendorPrototypeCreateUdropshipVendorProducts) | **POST** /UdropshipVendors/{id}/udropshipVendorProducts | Creates a new instance in udropshipVendorProducts of this model.
[**udropshipVendorPrototypeCreateVendorLocations**](UdropshipVendorApi.md#udropshipVendorPrototypeCreateVendorLocations) | **POST** /UdropshipVendors/{id}/vendorLocations | Creates a new instance in vendorLocations of this model.
[**udropshipVendorPrototypeDeleteCatalogProducts**](UdropshipVendorApi.md#udropshipVendorPrototypeDeleteCatalogProducts) | **DELETE** /UdropshipVendors/{id}/catalogProducts | Deletes all catalogProducts of this model.
[**udropshipVendorPrototypeDeleteHarpoonHpublicApplicationpartners**](UdropshipVendorApi.md#udropshipVendorPrototypeDeleteHarpoonHpublicApplicationpartners) | **DELETE** /UdropshipVendors/{id}/harpoonHpublicApplicationpartners | Deletes all harpoonHpublicApplicationpartners of this model.
[**udropshipVendorPrototypeDeleteHarpoonHpublicv12VendorAppCategories**](UdropshipVendorApi.md#udropshipVendorPrototypeDeleteHarpoonHpublicv12VendorAppCategories) | **DELETE** /UdropshipVendors/{id}/harpoonHpublicv12VendorAppCategories | Deletes all harpoonHpublicv12VendorAppCategories of this model.
[**udropshipVendorPrototypeDeletePartners**](UdropshipVendorApi.md#udropshipVendorPrototypeDeletePartners) | **DELETE** /UdropshipVendors/{id}/partners | Deletes all partners of this model.
[**udropshipVendorPrototypeDeleteUdropshipVendorProducts**](UdropshipVendorApi.md#udropshipVendorPrototypeDeleteUdropshipVendorProducts) | **DELETE** /UdropshipVendors/{id}/udropshipVendorProducts | Deletes all udropshipVendorProducts of this model.
[**udropshipVendorPrototypeDeleteVendorLocations**](UdropshipVendorApi.md#udropshipVendorPrototypeDeleteVendorLocations) | **DELETE** /UdropshipVendors/{id}/vendorLocations | Deletes all vendorLocations of this model.
[**udropshipVendorPrototypeDestroyByIdCatalogProducts**](UdropshipVendorApi.md#udropshipVendorPrototypeDestroyByIdCatalogProducts) | **DELETE** /UdropshipVendors/{id}/catalogProducts/{fk} | Delete a related item by id for catalogProducts.
[**udropshipVendorPrototypeDestroyByIdHarpoonHpublicApplicationpartners**](UdropshipVendorApi.md#udropshipVendorPrototypeDestroyByIdHarpoonHpublicApplicationpartners) | **DELETE** /UdropshipVendors/{id}/harpoonHpublicApplicationpartners/{fk} | Delete a related item by id for harpoonHpublicApplicationpartners.
[**udropshipVendorPrototypeDestroyByIdHarpoonHpublicv12VendorAppCategories**](UdropshipVendorApi.md#udropshipVendorPrototypeDestroyByIdHarpoonHpublicv12VendorAppCategories) | **DELETE** /UdropshipVendors/{id}/harpoonHpublicv12VendorAppCategories/{fk} | Delete a related item by id for harpoonHpublicv12VendorAppCategories.
[**udropshipVendorPrototypeDestroyByIdPartners**](UdropshipVendorApi.md#udropshipVendorPrototypeDestroyByIdPartners) | **DELETE** /UdropshipVendors/{id}/partners/{fk} | Delete a related item by id for partners.
[**udropshipVendorPrototypeDestroyByIdUdropshipVendorProducts**](UdropshipVendorApi.md#udropshipVendorPrototypeDestroyByIdUdropshipVendorProducts) | **DELETE** /UdropshipVendors/{id}/udropshipVendorProducts/{fk} | Delete a related item by id for udropshipVendorProducts.
[**udropshipVendorPrototypeDestroyByIdVendorLocations**](UdropshipVendorApi.md#udropshipVendorPrototypeDestroyByIdVendorLocations) | **DELETE** /UdropshipVendors/{id}/vendorLocations/{fk} | Delete a related item by id for vendorLocations.
[**udropshipVendorPrototypeDestroyConfig**](UdropshipVendorApi.md#udropshipVendorPrototypeDestroyConfig) | **DELETE** /UdropshipVendors/{id}/config | Deletes config of this model.
[**udropshipVendorPrototypeExistsCatalogProducts**](UdropshipVendorApi.md#udropshipVendorPrototypeExistsCatalogProducts) | **HEAD** /UdropshipVendors/{id}/catalogProducts/rel/{fk} | Check the existence of catalogProducts relation to an item by id.
[**udropshipVendorPrototypeExistsPartners**](UdropshipVendorApi.md#udropshipVendorPrototypeExistsPartners) | **HEAD** /UdropshipVendors/{id}/partners/rel/{fk} | Check the existence of partners relation to an item by id.
[**udropshipVendorPrototypeFindByIdCatalogProducts**](UdropshipVendorApi.md#udropshipVendorPrototypeFindByIdCatalogProducts) | **GET** /UdropshipVendors/{id}/catalogProducts/{fk} | Find a related item by id for catalogProducts.
[**udropshipVendorPrototypeFindByIdHarpoonHpublicApplicationpartners**](UdropshipVendorApi.md#udropshipVendorPrototypeFindByIdHarpoonHpublicApplicationpartners) | **GET** /UdropshipVendors/{id}/harpoonHpublicApplicationpartners/{fk} | Find a related item by id for harpoonHpublicApplicationpartners.
[**udropshipVendorPrototypeFindByIdHarpoonHpublicv12VendorAppCategories**](UdropshipVendorApi.md#udropshipVendorPrototypeFindByIdHarpoonHpublicv12VendorAppCategories) | **GET** /UdropshipVendors/{id}/harpoonHpublicv12VendorAppCategories/{fk} | Find a related item by id for harpoonHpublicv12VendorAppCategories.
[**udropshipVendorPrototypeFindByIdPartners**](UdropshipVendorApi.md#udropshipVendorPrototypeFindByIdPartners) | **GET** /UdropshipVendors/{id}/partners/{fk} | Find a related item by id for partners.
[**udropshipVendorPrototypeFindByIdUdropshipVendorProducts**](UdropshipVendorApi.md#udropshipVendorPrototypeFindByIdUdropshipVendorProducts) | **GET** /UdropshipVendors/{id}/udropshipVendorProducts/{fk} | Find a related item by id for udropshipVendorProducts.
[**udropshipVendorPrototypeFindByIdVendorLocations**](UdropshipVendorApi.md#udropshipVendorPrototypeFindByIdVendorLocations) | **GET** /UdropshipVendors/{id}/vendorLocations/{fk} | Find a related item by id for vendorLocations.
[**udropshipVendorPrototypeGetCatalogProducts**](UdropshipVendorApi.md#udropshipVendorPrototypeGetCatalogProducts) | **GET** /UdropshipVendors/{id}/catalogProducts | Queries catalogProducts of UdropshipVendor.
[**udropshipVendorPrototypeGetConfig**](UdropshipVendorApi.md#udropshipVendorPrototypeGetConfig) | **GET** /UdropshipVendors/{id}/config | Fetches hasOne relation config.
[**udropshipVendorPrototypeGetHarpoonHpublicApplicationpartners**](UdropshipVendorApi.md#udropshipVendorPrototypeGetHarpoonHpublicApplicationpartners) | **GET** /UdropshipVendors/{id}/harpoonHpublicApplicationpartners | Queries harpoonHpublicApplicationpartners of UdropshipVendor.
[**udropshipVendorPrototypeGetHarpoonHpublicv12VendorAppCategories**](UdropshipVendorApi.md#udropshipVendorPrototypeGetHarpoonHpublicv12VendorAppCategories) | **GET** /UdropshipVendors/{id}/harpoonHpublicv12VendorAppCategories | Queries harpoonHpublicv12VendorAppCategories of UdropshipVendor.
[**udropshipVendorPrototypeGetPartners**](UdropshipVendorApi.md#udropshipVendorPrototypeGetPartners) | **GET** /UdropshipVendors/{id}/partners | Queries partners of UdropshipVendor.
[**udropshipVendorPrototypeGetUdropshipVendorProducts**](UdropshipVendorApi.md#udropshipVendorPrototypeGetUdropshipVendorProducts) | **GET** /UdropshipVendors/{id}/udropshipVendorProducts | Queries udropshipVendorProducts of UdropshipVendor.
[**udropshipVendorPrototypeGetVendorLocations**](UdropshipVendorApi.md#udropshipVendorPrototypeGetVendorLocations) | **GET** /UdropshipVendors/{id}/vendorLocations | Queries vendorLocations of UdropshipVendor.
[**udropshipVendorPrototypeLinkCatalogProducts**](UdropshipVendorApi.md#udropshipVendorPrototypeLinkCatalogProducts) | **PUT** /UdropshipVendors/{id}/catalogProducts/rel/{fk} | Add a related item by id for catalogProducts.
[**udropshipVendorPrototypeLinkPartners**](UdropshipVendorApi.md#udropshipVendorPrototypeLinkPartners) | **PUT** /UdropshipVendors/{id}/partners/rel/{fk} | Add a related item by id for partners.
[**udropshipVendorPrototypeUnlinkCatalogProducts**](UdropshipVendorApi.md#udropshipVendorPrototypeUnlinkCatalogProducts) | **DELETE** /UdropshipVendors/{id}/catalogProducts/rel/{fk} | Remove the catalogProducts relation to an item by id.
[**udropshipVendorPrototypeUnlinkPartners**](UdropshipVendorApi.md#udropshipVendorPrototypeUnlinkPartners) | **DELETE** /UdropshipVendors/{id}/partners/rel/{fk} | Remove the partners relation to an item by id.
[**udropshipVendorPrototypeUpdateAttributesPatchUdropshipVendorsid**](UdropshipVendorApi.md#udropshipVendorPrototypeUpdateAttributesPatchUdropshipVendorsid) | **PATCH** /UdropshipVendors/{id} | Patch attributes for a model instance and persist it into the data source.
[**udropshipVendorPrototypeUpdateAttributesPutUdropshipVendorsid**](UdropshipVendorApi.md#udropshipVendorPrototypeUpdateAttributesPutUdropshipVendorsid) | **PUT** /UdropshipVendors/{id} | Patch attributes for a model instance and persist it into the data source.
[**udropshipVendorPrototypeUpdateByIdCatalogProducts**](UdropshipVendorApi.md#udropshipVendorPrototypeUpdateByIdCatalogProducts) | **PUT** /UdropshipVendors/{id}/catalogProducts/{fk} | Update a related item by id for catalogProducts.
[**udropshipVendorPrototypeUpdateByIdHarpoonHpublicApplicationpartners**](UdropshipVendorApi.md#udropshipVendorPrototypeUpdateByIdHarpoonHpublicApplicationpartners) | **PUT** /UdropshipVendors/{id}/harpoonHpublicApplicationpartners/{fk} | Update a related item by id for harpoonHpublicApplicationpartners.
[**udropshipVendorPrototypeUpdateByIdHarpoonHpublicv12VendorAppCategories**](UdropshipVendorApi.md#udropshipVendorPrototypeUpdateByIdHarpoonHpublicv12VendorAppCategories) | **PUT** /UdropshipVendors/{id}/harpoonHpublicv12VendorAppCategories/{fk} | Update a related item by id for harpoonHpublicv12VendorAppCategories.
[**udropshipVendorPrototypeUpdateByIdPartners**](UdropshipVendorApi.md#udropshipVendorPrototypeUpdateByIdPartners) | **PUT** /UdropshipVendors/{id}/partners/{fk} | Update a related item by id for partners.
[**udropshipVendorPrototypeUpdateByIdUdropshipVendorProducts**](UdropshipVendorApi.md#udropshipVendorPrototypeUpdateByIdUdropshipVendorProducts) | **PUT** /UdropshipVendors/{id}/udropshipVendorProducts/{fk} | Update a related item by id for udropshipVendorProducts.
[**udropshipVendorPrototypeUpdateByIdVendorLocations**](UdropshipVendorApi.md#udropshipVendorPrototypeUpdateByIdVendorLocations) | **PUT** /UdropshipVendors/{id}/vendorLocations/{fk} | Update a related item by id for vendorLocations.
[**udropshipVendorPrototypeUpdateConfig**](UdropshipVendorApi.md#udropshipVendorPrototypeUpdateConfig) | **PUT** /UdropshipVendors/{id}/config | Update config of this model.
[**udropshipVendorReplaceById**](UdropshipVendorApi.md#udropshipVendorReplaceById) | **POST** /UdropshipVendors/{id}/replace | Replace attributes for a model instance and persist it into the data source.
[**udropshipVendorReplaceOrCreate**](UdropshipVendorApi.md#udropshipVendorReplaceOrCreate) | **POST** /UdropshipVendors/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
[**udropshipVendorUpdateAll**](UdropshipVendorApi.md#udropshipVendorUpdateAll) | **POST** /UdropshipVendors/update | Update instances of the model matched by {{where}} from the data source.
[**udropshipVendorUpsertPatchUdropshipVendors**](UdropshipVendorApi.md#udropshipVendorUpsertPatchUdropshipVendors) | **PATCH** /UdropshipVendors | Patch an existing model instance or insert a new one into the data source.
[**udropshipVendorUpsertPutUdropshipVendors**](UdropshipVendorApi.md#udropshipVendorUpsertPutUdropshipVendors) | **PUT** /UdropshipVendors | Patch an existing model instance or insert a new one into the data source.
[**udropshipVendorUpsertWithWhere**](UdropshipVendorApi.md#udropshipVendorUpsertWithWhere) | **POST** /UdropshipVendors/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.


<a name="udropshipVendorCount"></a>
# **udropshipVendorCount**
> InlineResponse2001 udropshipVendorCount(where)

Count instances of the model matched by where from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.UdropshipVendorApi;

UdropshipVendorApi apiInstance = new UdropshipVendorApi();
String where = "where_example"; // String | Criteria to match model instances
try {
    InlineResponse2001 result = apiInstance.udropshipVendorCount(where);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling UdropshipVendorApi#udropshipVendorCount");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional]

### Return type

[**InlineResponse2001**](InlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="udropshipVendorCreate"></a>
# **udropshipVendorCreate**
> UdropshipVendor udropshipVendorCreate(data)

Create a new instance of the model and persist it into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.UdropshipVendorApi;

UdropshipVendorApi apiInstance = new UdropshipVendorApi();
UdropshipVendor data = new UdropshipVendor(); // UdropshipVendor | Model instance data
try {
    UdropshipVendor result = apiInstance.udropshipVendorCreate(data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling UdropshipVendorApi#udropshipVendorCreate");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**UdropshipVendor**](UdropshipVendor.md)| Model instance data | [optional]

### Return type

[**UdropshipVendor**](UdropshipVendor.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="udropshipVendorCreateChangeStreamGetUdropshipVendorsChangeStream"></a>
# **udropshipVendorCreateChangeStreamGetUdropshipVendorsChangeStream**
> File udropshipVendorCreateChangeStreamGetUdropshipVendorsChangeStream(options)

Create a change stream.

### Example
```java
// Import classes:
//import com.harpoon.api.UdropshipVendorApi;

UdropshipVendorApi apiInstance = new UdropshipVendorApi();
String options = "options_example"; // String | 
try {
    File result = apiInstance.udropshipVendorCreateChangeStreamGetUdropshipVendorsChangeStream(options);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling UdropshipVendorApi#udropshipVendorCreateChangeStreamGetUdropshipVendorsChangeStream");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **String**|  | [optional]

### Return type

[**File**](File.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="udropshipVendorCreateChangeStreamPostUdropshipVendorsChangeStream"></a>
# **udropshipVendorCreateChangeStreamPostUdropshipVendorsChangeStream**
> File udropshipVendorCreateChangeStreamPostUdropshipVendorsChangeStream(options)

Create a change stream.

### Example
```java
// Import classes:
//import com.harpoon.api.UdropshipVendorApi;

UdropshipVendorApi apiInstance = new UdropshipVendorApi();
String options = "options_example"; // String | 
try {
    File result = apiInstance.udropshipVendorCreateChangeStreamPostUdropshipVendorsChangeStream(options);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling UdropshipVendorApi#udropshipVendorCreateChangeStreamPostUdropshipVendorsChangeStream");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **String**|  | [optional]

### Return type

[**File**](File.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="udropshipVendorDeleteById"></a>
# **udropshipVendorDeleteById**
> Object udropshipVendorDeleteById(id)

Delete a model instance by {{id}} from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.UdropshipVendorApi;

UdropshipVendorApi apiInstance = new UdropshipVendorApi();
String id = "id_example"; // String | Model id
try {
    Object result = apiInstance.udropshipVendorDeleteById(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling UdropshipVendorApi#udropshipVendorDeleteById");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |

### Return type

**Object**

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="udropshipVendorExistsGetUdropshipVendorsidExists"></a>
# **udropshipVendorExistsGetUdropshipVendorsidExists**
> InlineResponse2003 udropshipVendorExistsGetUdropshipVendorsidExists(id)

Check whether a model instance exists in the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.UdropshipVendorApi;

UdropshipVendorApi apiInstance = new UdropshipVendorApi();
String id = "id_example"; // String | Model id
try {
    InlineResponse2003 result = apiInstance.udropshipVendorExistsGetUdropshipVendorsidExists(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling UdropshipVendorApi#udropshipVendorExistsGetUdropshipVendorsidExists");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |

### Return type

[**InlineResponse2003**](InlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="udropshipVendorExistsHeadUdropshipVendorsid"></a>
# **udropshipVendorExistsHeadUdropshipVendorsid**
> InlineResponse2003 udropshipVendorExistsHeadUdropshipVendorsid(id)

Check whether a model instance exists in the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.UdropshipVendorApi;

UdropshipVendorApi apiInstance = new UdropshipVendorApi();
String id = "id_example"; // String | Model id
try {
    InlineResponse2003 result = apiInstance.udropshipVendorExistsHeadUdropshipVendorsid(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling UdropshipVendorApi#udropshipVendorExistsHeadUdropshipVendorsid");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |

### Return type

[**InlineResponse2003**](InlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="udropshipVendorFind"></a>
# **udropshipVendorFind**
> List&lt;UdropshipVendor&gt; udropshipVendorFind(filter)

Find all instances of the model matched by filter from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.UdropshipVendorApi;

UdropshipVendorApi apiInstance = new UdropshipVendorApi();
String filter = "filter_example"; // String | Filter defining fields, where, include, order, offset, and limit
try {
    List<UdropshipVendor> result = apiInstance.udropshipVendorFind(filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling UdropshipVendorApi#udropshipVendorFind");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **String**| Filter defining fields, where, include, order, offset, and limit | [optional]

### Return type

[**List&lt;UdropshipVendor&gt;**](UdropshipVendor.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="udropshipVendorFindById"></a>
# **udropshipVendorFindById**
> UdropshipVendor udropshipVendorFindById(id, filter)

Find a model instance by {{id}} from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.UdropshipVendorApi;

UdropshipVendorApi apiInstance = new UdropshipVendorApi();
String id = "id_example"; // String | Model id
String filter = "filter_example"; // String | Filter defining fields and include
try {
    UdropshipVendor result = apiInstance.udropshipVendorFindById(id, filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling UdropshipVendorApi#udropshipVendorFindById");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |
 **filter** | **String**| Filter defining fields and include | [optional]

### Return type

[**UdropshipVendor**](UdropshipVendor.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="udropshipVendorFindOne"></a>
# **udropshipVendorFindOne**
> UdropshipVendor udropshipVendorFindOne(filter)

Find first instance of the model matched by filter from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.UdropshipVendorApi;

UdropshipVendorApi apiInstance = new UdropshipVendorApi();
String filter = "filter_example"; // String | Filter defining fields, where, include, order, offset, and limit
try {
    UdropshipVendor result = apiInstance.udropshipVendorFindOne(filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling UdropshipVendorApi#udropshipVendorFindOne");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **String**| Filter defining fields, where, include, order, offset, and limit | [optional]

### Return type

[**UdropshipVendor**](UdropshipVendor.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="udropshipVendorPrototypeCountCatalogProducts"></a>
# **udropshipVendorPrototypeCountCatalogProducts**
> InlineResponse2001 udropshipVendorPrototypeCountCatalogProducts(id, where)

Counts catalogProducts of UdropshipVendor.

### Example
```java
// Import classes:
//import com.harpoon.api.UdropshipVendorApi;

UdropshipVendorApi apiInstance = new UdropshipVendorApi();
String id = "id_example"; // String | UdropshipVendor id
String where = "where_example"; // String | Criteria to match model instances
try {
    InlineResponse2001 result = apiInstance.udropshipVendorPrototypeCountCatalogProducts(id, where);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling UdropshipVendorApi#udropshipVendorPrototypeCountCatalogProducts");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| UdropshipVendor id |
 **where** | **String**| Criteria to match model instances | [optional]

### Return type

[**InlineResponse2001**](InlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="udropshipVendorPrototypeCountHarpoonHpublicApplicationpartners"></a>
# **udropshipVendorPrototypeCountHarpoonHpublicApplicationpartners**
> InlineResponse2001 udropshipVendorPrototypeCountHarpoonHpublicApplicationpartners(id, where)

Counts harpoonHpublicApplicationpartners of UdropshipVendor.

### Example
```java
// Import classes:
//import com.harpoon.api.UdropshipVendorApi;

UdropshipVendorApi apiInstance = new UdropshipVendorApi();
String id = "id_example"; // String | UdropshipVendor id
String where = "where_example"; // String | Criteria to match model instances
try {
    InlineResponse2001 result = apiInstance.udropshipVendorPrototypeCountHarpoonHpublicApplicationpartners(id, where);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling UdropshipVendorApi#udropshipVendorPrototypeCountHarpoonHpublicApplicationpartners");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| UdropshipVendor id |
 **where** | **String**| Criteria to match model instances | [optional]

### Return type

[**InlineResponse2001**](InlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="udropshipVendorPrototypeCountHarpoonHpublicv12VendorAppCategories"></a>
# **udropshipVendorPrototypeCountHarpoonHpublicv12VendorAppCategories**
> InlineResponse2001 udropshipVendorPrototypeCountHarpoonHpublicv12VendorAppCategories(id, where)

Counts harpoonHpublicv12VendorAppCategories of UdropshipVendor.

### Example
```java
// Import classes:
//import com.harpoon.api.UdropshipVendorApi;

UdropshipVendorApi apiInstance = new UdropshipVendorApi();
String id = "id_example"; // String | UdropshipVendor id
String where = "where_example"; // String | Criteria to match model instances
try {
    InlineResponse2001 result = apiInstance.udropshipVendorPrototypeCountHarpoonHpublicv12VendorAppCategories(id, where);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling UdropshipVendorApi#udropshipVendorPrototypeCountHarpoonHpublicv12VendorAppCategories");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| UdropshipVendor id |
 **where** | **String**| Criteria to match model instances | [optional]

### Return type

[**InlineResponse2001**](InlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="udropshipVendorPrototypeCountPartners"></a>
# **udropshipVendorPrototypeCountPartners**
> InlineResponse2001 udropshipVendorPrototypeCountPartners(id, where)

Counts partners of UdropshipVendor.

### Example
```java
// Import classes:
//import com.harpoon.api.UdropshipVendorApi;

UdropshipVendorApi apiInstance = new UdropshipVendorApi();
String id = "id_example"; // String | UdropshipVendor id
String where = "where_example"; // String | Criteria to match model instances
try {
    InlineResponse2001 result = apiInstance.udropshipVendorPrototypeCountPartners(id, where);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling UdropshipVendorApi#udropshipVendorPrototypeCountPartners");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| UdropshipVendor id |
 **where** | **String**| Criteria to match model instances | [optional]

### Return type

[**InlineResponse2001**](InlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="udropshipVendorPrototypeCountUdropshipVendorProducts"></a>
# **udropshipVendorPrototypeCountUdropshipVendorProducts**
> InlineResponse2001 udropshipVendorPrototypeCountUdropshipVendorProducts(id, where)

Counts udropshipVendorProducts of UdropshipVendor.

### Example
```java
// Import classes:
//import com.harpoon.api.UdropshipVendorApi;

UdropshipVendorApi apiInstance = new UdropshipVendorApi();
String id = "id_example"; // String | UdropshipVendor id
String where = "where_example"; // String | Criteria to match model instances
try {
    InlineResponse2001 result = apiInstance.udropshipVendorPrototypeCountUdropshipVendorProducts(id, where);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling UdropshipVendorApi#udropshipVendorPrototypeCountUdropshipVendorProducts");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| UdropshipVendor id |
 **where** | **String**| Criteria to match model instances | [optional]

### Return type

[**InlineResponse2001**](InlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="udropshipVendorPrototypeCountVendorLocations"></a>
# **udropshipVendorPrototypeCountVendorLocations**
> InlineResponse2001 udropshipVendorPrototypeCountVendorLocations(id, where)

Counts vendorLocations of UdropshipVendor.

### Example
```java
// Import classes:
//import com.harpoon.api.UdropshipVendorApi;

UdropshipVendorApi apiInstance = new UdropshipVendorApi();
String id = "id_example"; // String | UdropshipVendor id
String where = "where_example"; // String | Criteria to match model instances
try {
    InlineResponse2001 result = apiInstance.udropshipVendorPrototypeCountVendorLocations(id, where);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling UdropshipVendorApi#udropshipVendorPrototypeCountVendorLocations");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| UdropshipVendor id |
 **where** | **String**| Criteria to match model instances | [optional]

### Return type

[**InlineResponse2001**](InlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="udropshipVendorPrototypeCreateCatalogProducts"></a>
# **udropshipVendorPrototypeCreateCatalogProducts**
> CatalogProduct udropshipVendorPrototypeCreateCatalogProducts(id, data)

Creates a new instance in catalogProducts of this model.

### Example
```java
// Import classes:
//import com.harpoon.api.UdropshipVendorApi;

UdropshipVendorApi apiInstance = new UdropshipVendorApi();
String id = "id_example"; // String | UdropshipVendor id
CatalogProduct data = new CatalogProduct(); // CatalogProduct | 
try {
    CatalogProduct result = apiInstance.udropshipVendorPrototypeCreateCatalogProducts(id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling UdropshipVendorApi#udropshipVendorPrototypeCreateCatalogProducts");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| UdropshipVendor id |
 **data** | [**CatalogProduct**](CatalogProduct.md)|  | [optional]

### Return type

[**CatalogProduct**](CatalogProduct.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="udropshipVendorPrototypeCreateConfig"></a>
# **udropshipVendorPrototypeCreateConfig**
> HarpoonHpublicVendorconfig udropshipVendorPrototypeCreateConfig(id, data)

Creates a new instance in config of this model.

### Example
```java
// Import classes:
//import com.harpoon.api.UdropshipVendorApi;

UdropshipVendorApi apiInstance = new UdropshipVendorApi();
String id = "id_example"; // String | UdropshipVendor id
HarpoonHpublicVendorconfig data = new HarpoonHpublicVendorconfig(); // HarpoonHpublicVendorconfig | 
try {
    HarpoonHpublicVendorconfig result = apiInstance.udropshipVendorPrototypeCreateConfig(id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling UdropshipVendorApi#udropshipVendorPrototypeCreateConfig");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| UdropshipVendor id |
 **data** | [**HarpoonHpublicVendorconfig**](HarpoonHpublicVendorconfig.md)|  | [optional]

### Return type

[**HarpoonHpublicVendorconfig**](HarpoonHpublicVendorconfig.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="udropshipVendorPrototypeCreateHarpoonHpublicApplicationpartners"></a>
# **udropshipVendorPrototypeCreateHarpoonHpublicApplicationpartners**
> HarpoonHpublicApplicationpartner udropshipVendorPrototypeCreateHarpoonHpublicApplicationpartners(id, data)

Creates a new instance in harpoonHpublicApplicationpartners of this model.

### Example
```java
// Import classes:
//import com.harpoon.api.UdropshipVendorApi;

UdropshipVendorApi apiInstance = new UdropshipVendorApi();
String id = "id_example"; // String | UdropshipVendor id
HarpoonHpublicApplicationpartner data = new HarpoonHpublicApplicationpartner(); // HarpoonHpublicApplicationpartner | 
try {
    HarpoonHpublicApplicationpartner result = apiInstance.udropshipVendorPrototypeCreateHarpoonHpublicApplicationpartners(id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling UdropshipVendorApi#udropshipVendorPrototypeCreateHarpoonHpublicApplicationpartners");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| UdropshipVendor id |
 **data** | [**HarpoonHpublicApplicationpartner**](HarpoonHpublicApplicationpartner.md)|  | [optional]

### Return type

[**HarpoonHpublicApplicationpartner**](HarpoonHpublicApplicationpartner.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="udropshipVendorPrototypeCreateHarpoonHpublicv12VendorAppCategories"></a>
# **udropshipVendorPrototypeCreateHarpoonHpublicv12VendorAppCategories**
> HarpoonHpublicv12VendorAppCategory udropshipVendorPrototypeCreateHarpoonHpublicv12VendorAppCategories(id, data)

Creates a new instance in harpoonHpublicv12VendorAppCategories of this model.

### Example
```java
// Import classes:
//import com.harpoon.api.UdropshipVendorApi;

UdropshipVendorApi apiInstance = new UdropshipVendorApi();
String id = "id_example"; // String | UdropshipVendor id
HarpoonHpublicv12VendorAppCategory data = new HarpoonHpublicv12VendorAppCategory(); // HarpoonHpublicv12VendorAppCategory | 
try {
    HarpoonHpublicv12VendorAppCategory result = apiInstance.udropshipVendorPrototypeCreateHarpoonHpublicv12VendorAppCategories(id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling UdropshipVendorApi#udropshipVendorPrototypeCreateHarpoonHpublicv12VendorAppCategories");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| UdropshipVendor id |
 **data** | [**HarpoonHpublicv12VendorAppCategory**](HarpoonHpublicv12VendorAppCategory.md)|  | [optional]

### Return type

[**HarpoonHpublicv12VendorAppCategory**](HarpoonHpublicv12VendorAppCategory.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="udropshipVendorPrototypeCreatePartners"></a>
# **udropshipVendorPrototypeCreatePartners**
> UdropshipVendor udropshipVendorPrototypeCreatePartners(id, data)

Creates a new instance in partners of this model.

### Example
```java
// Import classes:
//import com.harpoon.api.UdropshipVendorApi;

UdropshipVendorApi apiInstance = new UdropshipVendorApi();
String id = "id_example"; // String | UdropshipVendor id
UdropshipVendor data = new UdropshipVendor(); // UdropshipVendor | 
try {
    UdropshipVendor result = apiInstance.udropshipVendorPrototypeCreatePartners(id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling UdropshipVendorApi#udropshipVendorPrototypeCreatePartners");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| UdropshipVendor id |
 **data** | [**UdropshipVendor**](UdropshipVendor.md)|  | [optional]

### Return type

[**UdropshipVendor**](UdropshipVendor.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="udropshipVendorPrototypeCreateUdropshipVendorProducts"></a>
# **udropshipVendorPrototypeCreateUdropshipVendorProducts**
> UdropshipVendorProduct udropshipVendorPrototypeCreateUdropshipVendorProducts(id, data)

Creates a new instance in udropshipVendorProducts of this model.

### Example
```java
// Import classes:
//import com.harpoon.api.UdropshipVendorApi;

UdropshipVendorApi apiInstance = new UdropshipVendorApi();
String id = "id_example"; // String | UdropshipVendor id
UdropshipVendorProduct data = new UdropshipVendorProduct(); // UdropshipVendorProduct | 
try {
    UdropshipVendorProduct result = apiInstance.udropshipVendorPrototypeCreateUdropshipVendorProducts(id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling UdropshipVendorApi#udropshipVendorPrototypeCreateUdropshipVendorProducts");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| UdropshipVendor id |
 **data** | [**UdropshipVendorProduct**](UdropshipVendorProduct.md)|  | [optional]

### Return type

[**UdropshipVendorProduct**](UdropshipVendorProduct.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="udropshipVendorPrototypeCreateVendorLocations"></a>
# **udropshipVendorPrototypeCreateVendorLocations**
> HarpoonHpublicBrandvenue udropshipVendorPrototypeCreateVendorLocations(id, data)

Creates a new instance in vendorLocations of this model.

### Example
```java
// Import classes:
//import com.harpoon.api.UdropshipVendorApi;

UdropshipVendorApi apiInstance = new UdropshipVendorApi();
String id = "id_example"; // String | UdropshipVendor id
HarpoonHpublicBrandvenue data = new HarpoonHpublicBrandvenue(); // HarpoonHpublicBrandvenue | 
try {
    HarpoonHpublicBrandvenue result = apiInstance.udropshipVendorPrototypeCreateVendorLocations(id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling UdropshipVendorApi#udropshipVendorPrototypeCreateVendorLocations");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| UdropshipVendor id |
 **data** | [**HarpoonHpublicBrandvenue**](HarpoonHpublicBrandvenue.md)|  | [optional]

### Return type

[**HarpoonHpublicBrandvenue**](HarpoonHpublicBrandvenue.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="udropshipVendorPrototypeDeleteCatalogProducts"></a>
# **udropshipVendorPrototypeDeleteCatalogProducts**
> udropshipVendorPrototypeDeleteCatalogProducts(id)

Deletes all catalogProducts of this model.

### Example
```java
// Import classes:
//import com.harpoon.api.UdropshipVendorApi;

UdropshipVendorApi apiInstance = new UdropshipVendorApi();
String id = "id_example"; // String | UdropshipVendor id
try {
    apiInstance.udropshipVendorPrototypeDeleteCatalogProducts(id);
} catch (ApiException e) {
    System.err.println("Exception when calling UdropshipVendorApi#udropshipVendorPrototypeDeleteCatalogProducts");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| UdropshipVendor id |

### Return type

null (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="udropshipVendorPrototypeDeleteHarpoonHpublicApplicationpartners"></a>
# **udropshipVendorPrototypeDeleteHarpoonHpublicApplicationpartners**
> udropshipVendorPrototypeDeleteHarpoonHpublicApplicationpartners(id)

Deletes all harpoonHpublicApplicationpartners of this model.

### Example
```java
// Import classes:
//import com.harpoon.api.UdropshipVendorApi;

UdropshipVendorApi apiInstance = new UdropshipVendorApi();
String id = "id_example"; // String | UdropshipVendor id
try {
    apiInstance.udropshipVendorPrototypeDeleteHarpoonHpublicApplicationpartners(id);
} catch (ApiException e) {
    System.err.println("Exception when calling UdropshipVendorApi#udropshipVendorPrototypeDeleteHarpoonHpublicApplicationpartners");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| UdropshipVendor id |

### Return type

null (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="udropshipVendorPrototypeDeleteHarpoonHpublicv12VendorAppCategories"></a>
# **udropshipVendorPrototypeDeleteHarpoonHpublicv12VendorAppCategories**
> udropshipVendorPrototypeDeleteHarpoonHpublicv12VendorAppCategories(id)

Deletes all harpoonHpublicv12VendorAppCategories of this model.

### Example
```java
// Import classes:
//import com.harpoon.api.UdropshipVendorApi;

UdropshipVendorApi apiInstance = new UdropshipVendorApi();
String id = "id_example"; // String | UdropshipVendor id
try {
    apiInstance.udropshipVendorPrototypeDeleteHarpoonHpublicv12VendorAppCategories(id);
} catch (ApiException e) {
    System.err.println("Exception when calling UdropshipVendorApi#udropshipVendorPrototypeDeleteHarpoonHpublicv12VendorAppCategories");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| UdropshipVendor id |

### Return type

null (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="udropshipVendorPrototypeDeletePartners"></a>
# **udropshipVendorPrototypeDeletePartners**
> udropshipVendorPrototypeDeletePartners(id)

Deletes all partners of this model.

### Example
```java
// Import classes:
//import com.harpoon.api.UdropshipVendorApi;

UdropshipVendorApi apiInstance = new UdropshipVendorApi();
String id = "id_example"; // String | UdropshipVendor id
try {
    apiInstance.udropshipVendorPrototypeDeletePartners(id);
} catch (ApiException e) {
    System.err.println("Exception when calling UdropshipVendorApi#udropshipVendorPrototypeDeletePartners");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| UdropshipVendor id |

### Return type

null (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="udropshipVendorPrototypeDeleteUdropshipVendorProducts"></a>
# **udropshipVendorPrototypeDeleteUdropshipVendorProducts**
> udropshipVendorPrototypeDeleteUdropshipVendorProducts(id)

Deletes all udropshipVendorProducts of this model.

### Example
```java
// Import classes:
//import com.harpoon.api.UdropshipVendorApi;

UdropshipVendorApi apiInstance = new UdropshipVendorApi();
String id = "id_example"; // String | UdropshipVendor id
try {
    apiInstance.udropshipVendorPrototypeDeleteUdropshipVendorProducts(id);
} catch (ApiException e) {
    System.err.println("Exception when calling UdropshipVendorApi#udropshipVendorPrototypeDeleteUdropshipVendorProducts");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| UdropshipVendor id |

### Return type

null (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="udropshipVendorPrototypeDeleteVendorLocations"></a>
# **udropshipVendorPrototypeDeleteVendorLocations**
> udropshipVendorPrototypeDeleteVendorLocations(id)

Deletes all vendorLocations of this model.

### Example
```java
// Import classes:
//import com.harpoon.api.UdropshipVendorApi;

UdropshipVendorApi apiInstance = new UdropshipVendorApi();
String id = "id_example"; // String | UdropshipVendor id
try {
    apiInstance.udropshipVendorPrototypeDeleteVendorLocations(id);
} catch (ApiException e) {
    System.err.println("Exception when calling UdropshipVendorApi#udropshipVendorPrototypeDeleteVendorLocations");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| UdropshipVendor id |

### Return type

null (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="udropshipVendorPrototypeDestroyByIdCatalogProducts"></a>
# **udropshipVendorPrototypeDestroyByIdCatalogProducts**
> udropshipVendorPrototypeDestroyByIdCatalogProducts(fk, id)

Delete a related item by id for catalogProducts.

### Example
```java
// Import classes:
//import com.harpoon.api.UdropshipVendorApi;

UdropshipVendorApi apiInstance = new UdropshipVendorApi();
String fk = "fk_example"; // String | Foreign key for catalogProducts
String id = "id_example"; // String | UdropshipVendor id
try {
    apiInstance.udropshipVendorPrototypeDestroyByIdCatalogProducts(fk, id);
} catch (ApiException e) {
    System.err.println("Exception when calling UdropshipVendorApi#udropshipVendorPrototypeDestroyByIdCatalogProducts");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for catalogProducts |
 **id** | **String**| UdropshipVendor id |

### Return type

null (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="udropshipVendorPrototypeDestroyByIdHarpoonHpublicApplicationpartners"></a>
# **udropshipVendorPrototypeDestroyByIdHarpoonHpublicApplicationpartners**
> udropshipVendorPrototypeDestroyByIdHarpoonHpublicApplicationpartners(fk, id)

Delete a related item by id for harpoonHpublicApplicationpartners.

### Example
```java
// Import classes:
//import com.harpoon.api.UdropshipVendorApi;

UdropshipVendorApi apiInstance = new UdropshipVendorApi();
String fk = "fk_example"; // String | Foreign key for harpoonHpublicApplicationpartners
String id = "id_example"; // String | UdropshipVendor id
try {
    apiInstance.udropshipVendorPrototypeDestroyByIdHarpoonHpublicApplicationpartners(fk, id);
} catch (ApiException e) {
    System.err.println("Exception when calling UdropshipVendorApi#udropshipVendorPrototypeDestroyByIdHarpoonHpublicApplicationpartners");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for harpoonHpublicApplicationpartners |
 **id** | **String**| UdropshipVendor id |

### Return type

null (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="udropshipVendorPrototypeDestroyByIdHarpoonHpublicv12VendorAppCategories"></a>
# **udropshipVendorPrototypeDestroyByIdHarpoonHpublicv12VendorAppCategories**
> udropshipVendorPrototypeDestroyByIdHarpoonHpublicv12VendorAppCategories(fk, id)

Delete a related item by id for harpoonHpublicv12VendorAppCategories.

### Example
```java
// Import classes:
//import com.harpoon.api.UdropshipVendorApi;

UdropshipVendorApi apiInstance = new UdropshipVendorApi();
String fk = "fk_example"; // String | Foreign key for harpoonHpublicv12VendorAppCategories
String id = "id_example"; // String | UdropshipVendor id
try {
    apiInstance.udropshipVendorPrototypeDestroyByIdHarpoonHpublicv12VendorAppCategories(fk, id);
} catch (ApiException e) {
    System.err.println("Exception when calling UdropshipVendorApi#udropshipVendorPrototypeDestroyByIdHarpoonHpublicv12VendorAppCategories");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for harpoonHpublicv12VendorAppCategories |
 **id** | **String**| UdropshipVendor id |

### Return type

null (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="udropshipVendorPrototypeDestroyByIdPartners"></a>
# **udropshipVendorPrototypeDestroyByIdPartners**
> udropshipVendorPrototypeDestroyByIdPartners(fk, id)

Delete a related item by id for partners.

### Example
```java
// Import classes:
//import com.harpoon.api.UdropshipVendorApi;

UdropshipVendorApi apiInstance = new UdropshipVendorApi();
String fk = "fk_example"; // String | Foreign key for partners
String id = "id_example"; // String | UdropshipVendor id
try {
    apiInstance.udropshipVendorPrototypeDestroyByIdPartners(fk, id);
} catch (ApiException e) {
    System.err.println("Exception when calling UdropshipVendorApi#udropshipVendorPrototypeDestroyByIdPartners");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for partners |
 **id** | **String**| UdropshipVendor id |

### Return type

null (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="udropshipVendorPrototypeDestroyByIdUdropshipVendorProducts"></a>
# **udropshipVendorPrototypeDestroyByIdUdropshipVendorProducts**
> udropshipVendorPrototypeDestroyByIdUdropshipVendorProducts(fk, id)

Delete a related item by id for udropshipVendorProducts.

### Example
```java
// Import classes:
//import com.harpoon.api.UdropshipVendorApi;

UdropshipVendorApi apiInstance = new UdropshipVendorApi();
String fk = "fk_example"; // String | Foreign key for udropshipVendorProducts
String id = "id_example"; // String | UdropshipVendor id
try {
    apiInstance.udropshipVendorPrototypeDestroyByIdUdropshipVendorProducts(fk, id);
} catch (ApiException e) {
    System.err.println("Exception when calling UdropshipVendorApi#udropshipVendorPrototypeDestroyByIdUdropshipVendorProducts");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for udropshipVendorProducts |
 **id** | **String**| UdropshipVendor id |

### Return type

null (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="udropshipVendorPrototypeDestroyByIdVendorLocations"></a>
# **udropshipVendorPrototypeDestroyByIdVendorLocations**
> udropshipVendorPrototypeDestroyByIdVendorLocations(fk, id)

Delete a related item by id for vendorLocations.

### Example
```java
// Import classes:
//import com.harpoon.api.UdropshipVendorApi;

UdropshipVendorApi apiInstance = new UdropshipVendorApi();
String fk = "fk_example"; // String | Foreign key for vendorLocations
String id = "id_example"; // String | UdropshipVendor id
try {
    apiInstance.udropshipVendorPrototypeDestroyByIdVendorLocations(fk, id);
} catch (ApiException e) {
    System.err.println("Exception when calling UdropshipVendorApi#udropshipVendorPrototypeDestroyByIdVendorLocations");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for vendorLocations |
 **id** | **String**| UdropshipVendor id |

### Return type

null (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="udropshipVendorPrototypeDestroyConfig"></a>
# **udropshipVendorPrototypeDestroyConfig**
> udropshipVendorPrototypeDestroyConfig(id)

Deletes config of this model.

### Example
```java
// Import classes:
//import com.harpoon.api.UdropshipVendorApi;

UdropshipVendorApi apiInstance = new UdropshipVendorApi();
String id = "id_example"; // String | UdropshipVendor id
try {
    apiInstance.udropshipVendorPrototypeDestroyConfig(id);
} catch (ApiException e) {
    System.err.println("Exception when calling UdropshipVendorApi#udropshipVendorPrototypeDestroyConfig");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| UdropshipVendor id |

### Return type

null (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="udropshipVendorPrototypeExistsCatalogProducts"></a>
# **udropshipVendorPrototypeExistsCatalogProducts**
> Boolean udropshipVendorPrototypeExistsCatalogProducts(fk, id)

Check the existence of catalogProducts relation to an item by id.

### Example
```java
// Import classes:
//import com.harpoon.api.UdropshipVendorApi;

UdropshipVendorApi apiInstance = new UdropshipVendorApi();
String fk = "fk_example"; // String | Foreign key for catalogProducts
String id = "id_example"; // String | UdropshipVendor id
try {
    Boolean result = apiInstance.udropshipVendorPrototypeExistsCatalogProducts(fk, id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling UdropshipVendorApi#udropshipVendorPrototypeExistsCatalogProducts");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for catalogProducts |
 **id** | **String**| UdropshipVendor id |

### Return type

**Boolean**

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="udropshipVendorPrototypeExistsPartners"></a>
# **udropshipVendorPrototypeExistsPartners**
> Boolean udropshipVendorPrototypeExistsPartners(fk, id)

Check the existence of partners relation to an item by id.

### Example
```java
// Import classes:
//import com.harpoon.api.UdropshipVendorApi;

UdropshipVendorApi apiInstance = new UdropshipVendorApi();
String fk = "fk_example"; // String | Foreign key for partners
String id = "id_example"; // String | UdropshipVendor id
try {
    Boolean result = apiInstance.udropshipVendorPrototypeExistsPartners(fk, id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling UdropshipVendorApi#udropshipVendorPrototypeExistsPartners");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for partners |
 **id** | **String**| UdropshipVendor id |

### Return type

**Boolean**

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="udropshipVendorPrototypeFindByIdCatalogProducts"></a>
# **udropshipVendorPrototypeFindByIdCatalogProducts**
> CatalogProduct udropshipVendorPrototypeFindByIdCatalogProducts(fk, id)

Find a related item by id for catalogProducts.

### Example
```java
// Import classes:
//import com.harpoon.api.UdropshipVendorApi;

UdropshipVendorApi apiInstance = new UdropshipVendorApi();
String fk = "fk_example"; // String | Foreign key for catalogProducts
String id = "id_example"; // String | UdropshipVendor id
try {
    CatalogProduct result = apiInstance.udropshipVendorPrototypeFindByIdCatalogProducts(fk, id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling UdropshipVendorApi#udropshipVendorPrototypeFindByIdCatalogProducts");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for catalogProducts |
 **id** | **String**| UdropshipVendor id |

### Return type

[**CatalogProduct**](CatalogProduct.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="udropshipVendorPrototypeFindByIdHarpoonHpublicApplicationpartners"></a>
# **udropshipVendorPrototypeFindByIdHarpoonHpublicApplicationpartners**
> HarpoonHpublicApplicationpartner udropshipVendorPrototypeFindByIdHarpoonHpublicApplicationpartners(fk, id)

Find a related item by id for harpoonHpublicApplicationpartners.

### Example
```java
// Import classes:
//import com.harpoon.api.UdropshipVendorApi;

UdropshipVendorApi apiInstance = new UdropshipVendorApi();
String fk = "fk_example"; // String | Foreign key for harpoonHpublicApplicationpartners
String id = "id_example"; // String | UdropshipVendor id
try {
    HarpoonHpublicApplicationpartner result = apiInstance.udropshipVendorPrototypeFindByIdHarpoonHpublicApplicationpartners(fk, id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling UdropshipVendorApi#udropshipVendorPrototypeFindByIdHarpoonHpublicApplicationpartners");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for harpoonHpublicApplicationpartners |
 **id** | **String**| UdropshipVendor id |

### Return type

[**HarpoonHpublicApplicationpartner**](HarpoonHpublicApplicationpartner.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="udropshipVendorPrototypeFindByIdHarpoonHpublicv12VendorAppCategories"></a>
# **udropshipVendorPrototypeFindByIdHarpoonHpublicv12VendorAppCategories**
> HarpoonHpublicv12VendorAppCategory udropshipVendorPrototypeFindByIdHarpoonHpublicv12VendorAppCategories(fk, id)

Find a related item by id for harpoonHpublicv12VendorAppCategories.

### Example
```java
// Import classes:
//import com.harpoon.api.UdropshipVendorApi;

UdropshipVendorApi apiInstance = new UdropshipVendorApi();
String fk = "fk_example"; // String | Foreign key for harpoonHpublicv12VendorAppCategories
String id = "id_example"; // String | UdropshipVendor id
try {
    HarpoonHpublicv12VendorAppCategory result = apiInstance.udropshipVendorPrototypeFindByIdHarpoonHpublicv12VendorAppCategories(fk, id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling UdropshipVendorApi#udropshipVendorPrototypeFindByIdHarpoonHpublicv12VendorAppCategories");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for harpoonHpublicv12VendorAppCategories |
 **id** | **String**| UdropshipVendor id |

### Return type

[**HarpoonHpublicv12VendorAppCategory**](HarpoonHpublicv12VendorAppCategory.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="udropshipVendorPrototypeFindByIdPartners"></a>
# **udropshipVendorPrototypeFindByIdPartners**
> UdropshipVendor udropshipVendorPrototypeFindByIdPartners(fk, id)

Find a related item by id for partners.

### Example
```java
// Import classes:
//import com.harpoon.api.UdropshipVendorApi;

UdropshipVendorApi apiInstance = new UdropshipVendorApi();
String fk = "fk_example"; // String | Foreign key for partners
String id = "id_example"; // String | UdropshipVendor id
try {
    UdropshipVendor result = apiInstance.udropshipVendorPrototypeFindByIdPartners(fk, id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling UdropshipVendorApi#udropshipVendorPrototypeFindByIdPartners");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for partners |
 **id** | **String**| UdropshipVendor id |

### Return type

[**UdropshipVendor**](UdropshipVendor.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="udropshipVendorPrototypeFindByIdUdropshipVendorProducts"></a>
# **udropshipVendorPrototypeFindByIdUdropshipVendorProducts**
> UdropshipVendorProduct udropshipVendorPrototypeFindByIdUdropshipVendorProducts(fk, id)

Find a related item by id for udropshipVendorProducts.

### Example
```java
// Import classes:
//import com.harpoon.api.UdropshipVendorApi;

UdropshipVendorApi apiInstance = new UdropshipVendorApi();
String fk = "fk_example"; // String | Foreign key for udropshipVendorProducts
String id = "id_example"; // String | UdropshipVendor id
try {
    UdropshipVendorProduct result = apiInstance.udropshipVendorPrototypeFindByIdUdropshipVendorProducts(fk, id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling UdropshipVendorApi#udropshipVendorPrototypeFindByIdUdropshipVendorProducts");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for udropshipVendorProducts |
 **id** | **String**| UdropshipVendor id |

### Return type

[**UdropshipVendorProduct**](UdropshipVendorProduct.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="udropshipVendorPrototypeFindByIdVendorLocations"></a>
# **udropshipVendorPrototypeFindByIdVendorLocations**
> HarpoonHpublicBrandvenue udropshipVendorPrototypeFindByIdVendorLocations(fk, id)

Find a related item by id for vendorLocations.

### Example
```java
// Import classes:
//import com.harpoon.api.UdropshipVendorApi;

UdropshipVendorApi apiInstance = new UdropshipVendorApi();
String fk = "fk_example"; // String | Foreign key for vendorLocations
String id = "id_example"; // String | UdropshipVendor id
try {
    HarpoonHpublicBrandvenue result = apiInstance.udropshipVendorPrototypeFindByIdVendorLocations(fk, id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling UdropshipVendorApi#udropshipVendorPrototypeFindByIdVendorLocations");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for vendorLocations |
 **id** | **String**| UdropshipVendor id |

### Return type

[**HarpoonHpublicBrandvenue**](HarpoonHpublicBrandvenue.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="udropshipVendorPrototypeGetCatalogProducts"></a>
# **udropshipVendorPrototypeGetCatalogProducts**
> List&lt;CatalogProduct&gt; udropshipVendorPrototypeGetCatalogProducts(id, filter)

Queries catalogProducts of UdropshipVendor.

### Example
```java
// Import classes:
//import com.harpoon.api.UdropshipVendorApi;

UdropshipVendorApi apiInstance = new UdropshipVendorApi();
String id = "id_example"; // String | UdropshipVendor id
String filter = "filter_example"; // String | 
try {
    List<CatalogProduct> result = apiInstance.udropshipVendorPrototypeGetCatalogProducts(id, filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling UdropshipVendorApi#udropshipVendorPrototypeGetCatalogProducts");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| UdropshipVendor id |
 **filter** | **String**|  | [optional]

### Return type

[**List&lt;CatalogProduct&gt;**](CatalogProduct.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="udropshipVendorPrototypeGetConfig"></a>
# **udropshipVendorPrototypeGetConfig**
> HarpoonHpublicVendorconfig udropshipVendorPrototypeGetConfig(id, refresh)

Fetches hasOne relation config.

### Example
```java
// Import classes:
//import com.harpoon.api.UdropshipVendorApi;

UdropshipVendorApi apiInstance = new UdropshipVendorApi();
String id = "id_example"; // String | UdropshipVendor id
Boolean refresh = true; // Boolean | 
try {
    HarpoonHpublicVendorconfig result = apiInstance.udropshipVendorPrototypeGetConfig(id, refresh);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling UdropshipVendorApi#udropshipVendorPrototypeGetConfig");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| UdropshipVendor id |
 **refresh** | **Boolean**|  | [optional]

### Return type

[**HarpoonHpublicVendorconfig**](HarpoonHpublicVendorconfig.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="udropshipVendorPrototypeGetHarpoonHpublicApplicationpartners"></a>
# **udropshipVendorPrototypeGetHarpoonHpublicApplicationpartners**
> List&lt;HarpoonHpublicApplicationpartner&gt; udropshipVendorPrototypeGetHarpoonHpublicApplicationpartners(id, filter)

Queries harpoonHpublicApplicationpartners of UdropshipVendor.

### Example
```java
// Import classes:
//import com.harpoon.api.UdropshipVendorApi;

UdropshipVendorApi apiInstance = new UdropshipVendorApi();
String id = "id_example"; // String | UdropshipVendor id
String filter = "filter_example"; // String | 
try {
    List<HarpoonHpublicApplicationpartner> result = apiInstance.udropshipVendorPrototypeGetHarpoonHpublicApplicationpartners(id, filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling UdropshipVendorApi#udropshipVendorPrototypeGetHarpoonHpublicApplicationpartners");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| UdropshipVendor id |
 **filter** | **String**|  | [optional]

### Return type

[**List&lt;HarpoonHpublicApplicationpartner&gt;**](HarpoonHpublicApplicationpartner.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="udropshipVendorPrototypeGetHarpoonHpublicv12VendorAppCategories"></a>
# **udropshipVendorPrototypeGetHarpoonHpublicv12VendorAppCategories**
> List&lt;HarpoonHpublicv12VendorAppCategory&gt; udropshipVendorPrototypeGetHarpoonHpublicv12VendorAppCategories(id, filter)

Queries harpoonHpublicv12VendorAppCategories of UdropshipVendor.

### Example
```java
// Import classes:
//import com.harpoon.api.UdropshipVendorApi;

UdropshipVendorApi apiInstance = new UdropshipVendorApi();
String id = "id_example"; // String | UdropshipVendor id
String filter = "filter_example"; // String | 
try {
    List<HarpoonHpublicv12VendorAppCategory> result = apiInstance.udropshipVendorPrototypeGetHarpoonHpublicv12VendorAppCategories(id, filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling UdropshipVendorApi#udropshipVendorPrototypeGetHarpoonHpublicv12VendorAppCategories");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| UdropshipVendor id |
 **filter** | **String**|  | [optional]

### Return type

[**List&lt;HarpoonHpublicv12VendorAppCategory&gt;**](HarpoonHpublicv12VendorAppCategory.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="udropshipVendorPrototypeGetPartners"></a>
# **udropshipVendorPrototypeGetPartners**
> List&lt;UdropshipVendor&gt; udropshipVendorPrototypeGetPartners(id, filter)

Queries partners of UdropshipVendor.

### Example
```java
// Import classes:
//import com.harpoon.api.UdropshipVendorApi;

UdropshipVendorApi apiInstance = new UdropshipVendorApi();
String id = "id_example"; // String | UdropshipVendor id
String filter = "filter_example"; // String | 
try {
    List<UdropshipVendor> result = apiInstance.udropshipVendorPrototypeGetPartners(id, filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling UdropshipVendorApi#udropshipVendorPrototypeGetPartners");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| UdropshipVendor id |
 **filter** | **String**|  | [optional]

### Return type

[**List&lt;UdropshipVendor&gt;**](UdropshipVendor.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="udropshipVendorPrototypeGetUdropshipVendorProducts"></a>
# **udropshipVendorPrototypeGetUdropshipVendorProducts**
> List&lt;UdropshipVendorProduct&gt; udropshipVendorPrototypeGetUdropshipVendorProducts(id, filter)

Queries udropshipVendorProducts of UdropshipVendor.

### Example
```java
// Import classes:
//import com.harpoon.api.UdropshipVendorApi;

UdropshipVendorApi apiInstance = new UdropshipVendorApi();
String id = "id_example"; // String | UdropshipVendor id
String filter = "filter_example"; // String | 
try {
    List<UdropshipVendorProduct> result = apiInstance.udropshipVendorPrototypeGetUdropshipVendorProducts(id, filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling UdropshipVendorApi#udropshipVendorPrototypeGetUdropshipVendorProducts");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| UdropshipVendor id |
 **filter** | **String**|  | [optional]

### Return type

[**List&lt;UdropshipVendorProduct&gt;**](UdropshipVendorProduct.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="udropshipVendorPrototypeGetVendorLocations"></a>
# **udropshipVendorPrototypeGetVendorLocations**
> List&lt;HarpoonHpublicBrandvenue&gt; udropshipVendorPrototypeGetVendorLocations(id, filter)

Queries vendorLocations of UdropshipVendor.

### Example
```java
// Import classes:
//import com.harpoon.api.UdropshipVendorApi;

UdropshipVendorApi apiInstance = new UdropshipVendorApi();
String id = "id_example"; // String | UdropshipVendor id
String filter = "filter_example"; // String | 
try {
    List<HarpoonHpublicBrandvenue> result = apiInstance.udropshipVendorPrototypeGetVendorLocations(id, filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling UdropshipVendorApi#udropshipVendorPrototypeGetVendorLocations");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| UdropshipVendor id |
 **filter** | **String**|  | [optional]

### Return type

[**List&lt;HarpoonHpublicBrandvenue&gt;**](HarpoonHpublicBrandvenue.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="udropshipVendorPrototypeLinkCatalogProducts"></a>
# **udropshipVendorPrototypeLinkCatalogProducts**
> UdropshipVendorProduct udropshipVendorPrototypeLinkCatalogProducts(fk, id, data)

Add a related item by id for catalogProducts.

### Example
```java
// Import classes:
//import com.harpoon.api.UdropshipVendorApi;

UdropshipVendorApi apiInstance = new UdropshipVendorApi();
String fk = "fk_example"; // String | Foreign key for catalogProducts
String id = "id_example"; // String | UdropshipVendor id
UdropshipVendorProduct data = new UdropshipVendorProduct(); // UdropshipVendorProduct | 
try {
    UdropshipVendorProduct result = apiInstance.udropshipVendorPrototypeLinkCatalogProducts(fk, id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling UdropshipVendorApi#udropshipVendorPrototypeLinkCatalogProducts");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for catalogProducts |
 **id** | **String**| UdropshipVendor id |
 **data** | [**UdropshipVendorProduct**](UdropshipVendorProduct.md)|  | [optional]

### Return type

[**UdropshipVendorProduct**](UdropshipVendorProduct.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="udropshipVendorPrototypeLinkPartners"></a>
# **udropshipVendorPrototypeLinkPartners**
> UdropshipVendorPartner udropshipVendorPrototypeLinkPartners(fk, id, data)

Add a related item by id for partners.

### Example
```java
// Import classes:
//import com.harpoon.api.UdropshipVendorApi;

UdropshipVendorApi apiInstance = new UdropshipVendorApi();
String fk = "fk_example"; // String | Foreign key for partners
String id = "id_example"; // String | UdropshipVendor id
UdropshipVendorPartner data = new UdropshipVendorPartner(); // UdropshipVendorPartner | 
try {
    UdropshipVendorPartner result = apiInstance.udropshipVendorPrototypeLinkPartners(fk, id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling UdropshipVendorApi#udropshipVendorPrototypeLinkPartners");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for partners |
 **id** | **String**| UdropshipVendor id |
 **data** | [**UdropshipVendorPartner**](UdropshipVendorPartner.md)|  | [optional]

### Return type

[**UdropshipVendorPartner**](UdropshipVendorPartner.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="udropshipVendorPrototypeUnlinkCatalogProducts"></a>
# **udropshipVendorPrototypeUnlinkCatalogProducts**
> udropshipVendorPrototypeUnlinkCatalogProducts(fk, id)

Remove the catalogProducts relation to an item by id.

### Example
```java
// Import classes:
//import com.harpoon.api.UdropshipVendorApi;

UdropshipVendorApi apiInstance = new UdropshipVendorApi();
String fk = "fk_example"; // String | Foreign key for catalogProducts
String id = "id_example"; // String | UdropshipVendor id
try {
    apiInstance.udropshipVendorPrototypeUnlinkCatalogProducts(fk, id);
} catch (ApiException e) {
    System.err.println("Exception when calling UdropshipVendorApi#udropshipVendorPrototypeUnlinkCatalogProducts");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for catalogProducts |
 **id** | **String**| UdropshipVendor id |

### Return type

null (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="udropshipVendorPrototypeUnlinkPartners"></a>
# **udropshipVendorPrototypeUnlinkPartners**
> udropshipVendorPrototypeUnlinkPartners(fk, id)

Remove the partners relation to an item by id.

### Example
```java
// Import classes:
//import com.harpoon.api.UdropshipVendorApi;

UdropshipVendorApi apiInstance = new UdropshipVendorApi();
String fk = "fk_example"; // String | Foreign key for partners
String id = "id_example"; // String | UdropshipVendor id
try {
    apiInstance.udropshipVendorPrototypeUnlinkPartners(fk, id);
} catch (ApiException e) {
    System.err.println("Exception when calling UdropshipVendorApi#udropshipVendorPrototypeUnlinkPartners");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for partners |
 **id** | **String**| UdropshipVendor id |

### Return type

null (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="udropshipVendorPrototypeUpdateAttributesPatchUdropshipVendorsid"></a>
# **udropshipVendorPrototypeUpdateAttributesPatchUdropshipVendorsid**
> UdropshipVendor udropshipVendorPrototypeUpdateAttributesPatchUdropshipVendorsid(id, data)

Patch attributes for a model instance and persist it into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.UdropshipVendorApi;

UdropshipVendorApi apiInstance = new UdropshipVendorApi();
String id = "id_example"; // String | UdropshipVendor id
UdropshipVendor data = new UdropshipVendor(); // UdropshipVendor | An object of model property name/value pairs
try {
    UdropshipVendor result = apiInstance.udropshipVendorPrototypeUpdateAttributesPatchUdropshipVendorsid(id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling UdropshipVendorApi#udropshipVendorPrototypeUpdateAttributesPatchUdropshipVendorsid");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| UdropshipVendor id |
 **data** | [**UdropshipVendor**](UdropshipVendor.md)| An object of model property name/value pairs | [optional]

### Return type

[**UdropshipVendor**](UdropshipVendor.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="udropshipVendorPrototypeUpdateAttributesPutUdropshipVendorsid"></a>
# **udropshipVendorPrototypeUpdateAttributesPutUdropshipVendorsid**
> UdropshipVendor udropshipVendorPrototypeUpdateAttributesPutUdropshipVendorsid(id, data)

Patch attributes for a model instance and persist it into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.UdropshipVendorApi;

UdropshipVendorApi apiInstance = new UdropshipVendorApi();
String id = "id_example"; // String | UdropshipVendor id
UdropshipVendor data = new UdropshipVendor(); // UdropshipVendor | An object of model property name/value pairs
try {
    UdropshipVendor result = apiInstance.udropshipVendorPrototypeUpdateAttributesPutUdropshipVendorsid(id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling UdropshipVendorApi#udropshipVendorPrototypeUpdateAttributesPutUdropshipVendorsid");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| UdropshipVendor id |
 **data** | [**UdropshipVendor**](UdropshipVendor.md)| An object of model property name/value pairs | [optional]

### Return type

[**UdropshipVendor**](UdropshipVendor.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="udropshipVendorPrototypeUpdateByIdCatalogProducts"></a>
# **udropshipVendorPrototypeUpdateByIdCatalogProducts**
> CatalogProduct udropshipVendorPrototypeUpdateByIdCatalogProducts(fk, id, data)

Update a related item by id for catalogProducts.

### Example
```java
// Import classes:
//import com.harpoon.api.UdropshipVendorApi;

UdropshipVendorApi apiInstance = new UdropshipVendorApi();
String fk = "fk_example"; // String | Foreign key for catalogProducts
String id = "id_example"; // String | UdropshipVendor id
CatalogProduct data = new CatalogProduct(); // CatalogProduct | 
try {
    CatalogProduct result = apiInstance.udropshipVendorPrototypeUpdateByIdCatalogProducts(fk, id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling UdropshipVendorApi#udropshipVendorPrototypeUpdateByIdCatalogProducts");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for catalogProducts |
 **id** | **String**| UdropshipVendor id |
 **data** | [**CatalogProduct**](CatalogProduct.md)|  | [optional]

### Return type

[**CatalogProduct**](CatalogProduct.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="udropshipVendorPrototypeUpdateByIdHarpoonHpublicApplicationpartners"></a>
# **udropshipVendorPrototypeUpdateByIdHarpoonHpublicApplicationpartners**
> HarpoonHpublicApplicationpartner udropshipVendorPrototypeUpdateByIdHarpoonHpublicApplicationpartners(fk, id, data)

Update a related item by id for harpoonHpublicApplicationpartners.

### Example
```java
// Import classes:
//import com.harpoon.api.UdropshipVendorApi;

UdropshipVendorApi apiInstance = new UdropshipVendorApi();
String fk = "fk_example"; // String | Foreign key for harpoonHpublicApplicationpartners
String id = "id_example"; // String | UdropshipVendor id
HarpoonHpublicApplicationpartner data = new HarpoonHpublicApplicationpartner(); // HarpoonHpublicApplicationpartner | 
try {
    HarpoonHpublicApplicationpartner result = apiInstance.udropshipVendorPrototypeUpdateByIdHarpoonHpublicApplicationpartners(fk, id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling UdropshipVendorApi#udropshipVendorPrototypeUpdateByIdHarpoonHpublicApplicationpartners");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for harpoonHpublicApplicationpartners |
 **id** | **String**| UdropshipVendor id |
 **data** | [**HarpoonHpublicApplicationpartner**](HarpoonHpublicApplicationpartner.md)|  | [optional]

### Return type

[**HarpoonHpublicApplicationpartner**](HarpoonHpublicApplicationpartner.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="udropshipVendorPrototypeUpdateByIdHarpoonHpublicv12VendorAppCategories"></a>
# **udropshipVendorPrototypeUpdateByIdHarpoonHpublicv12VendorAppCategories**
> HarpoonHpublicv12VendorAppCategory udropshipVendorPrototypeUpdateByIdHarpoonHpublicv12VendorAppCategories(fk, id, data)

Update a related item by id for harpoonHpublicv12VendorAppCategories.

### Example
```java
// Import classes:
//import com.harpoon.api.UdropshipVendorApi;

UdropshipVendorApi apiInstance = new UdropshipVendorApi();
String fk = "fk_example"; // String | Foreign key for harpoonHpublicv12VendorAppCategories
String id = "id_example"; // String | UdropshipVendor id
HarpoonHpublicv12VendorAppCategory data = new HarpoonHpublicv12VendorAppCategory(); // HarpoonHpublicv12VendorAppCategory | 
try {
    HarpoonHpublicv12VendorAppCategory result = apiInstance.udropshipVendorPrototypeUpdateByIdHarpoonHpublicv12VendorAppCategories(fk, id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling UdropshipVendorApi#udropshipVendorPrototypeUpdateByIdHarpoonHpublicv12VendorAppCategories");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for harpoonHpublicv12VendorAppCategories |
 **id** | **String**| UdropshipVendor id |
 **data** | [**HarpoonHpublicv12VendorAppCategory**](HarpoonHpublicv12VendorAppCategory.md)|  | [optional]

### Return type

[**HarpoonHpublicv12VendorAppCategory**](HarpoonHpublicv12VendorAppCategory.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="udropshipVendorPrototypeUpdateByIdPartners"></a>
# **udropshipVendorPrototypeUpdateByIdPartners**
> UdropshipVendor udropshipVendorPrototypeUpdateByIdPartners(fk, id, data)

Update a related item by id for partners.

### Example
```java
// Import classes:
//import com.harpoon.api.UdropshipVendorApi;

UdropshipVendorApi apiInstance = new UdropshipVendorApi();
String fk = "fk_example"; // String | Foreign key for partners
String id = "id_example"; // String | UdropshipVendor id
UdropshipVendor data = new UdropshipVendor(); // UdropshipVendor | 
try {
    UdropshipVendor result = apiInstance.udropshipVendorPrototypeUpdateByIdPartners(fk, id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling UdropshipVendorApi#udropshipVendorPrototypeUpdateByIdPartners");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for partners |
 **id** | **String**| UdropshipVendor id |
 **data** | [**UdropshipVendor**](UdropshipVendor.md)|  | [optional]

### Return type

[**UdropshipVendor**](UdropshipVendor.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="udropshipVendorPrototypeUpdateByIdUdropshipVendorProducts"></a>
# **udropshipVendorPrototypeUpdateByIdUdropshipVendorProducts**
> UdropshipVendorProduct udropshipVendorPrototypeUpdateByIdUdropshipVendorProducts(fk, id, data)

Update a related item by id for udropshipVendorProducts.

### Example
```java
// Import classes:
//import com.harpoon.api.UdropshipVendorApi;

UdropshipVendorApi apiInstance = new UdropshipVendorApi();
String fk = "fk_example"; // String | Foreign key for udropshipVendorProducts
String id = "id_example"; // String | UdropshipVendor id
UdropshipVendorProduct data = new UdropshipVendorProduct(); // UdropshipVendorProduct | 
try {
    UdropshipVendorProduct result = apiInstance.udropshipVendorPrototypeUpdateByIdUdropshipVendorProducts(fk, id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling UdropshipVendorApi#udropshipVendorPrototypeUpdateByIdUdropshipVendorProducts");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for udropshipVendorProducts |
 **id** | **String**| UdropshipVendor id |
 **data** | [**UdropshipVendorProduct**](UdropshipVendorProduct.md)|  | [optional]

### Return type

[**UdropshipVendorProduct**](UdropshipVendorProduct.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="udropshipVendorPrototypeUpdateByIdVendorLocations"></a>
# **udropshipVendorPrototypeUpdateByIdVendorLocations**
> HarpoonHpublicBrandvenue udropshipVendorPrototypeUpdateByIdVendorLocations(fk, id, data)

Update a related item by id for vendorLocations.

### Example
```java
// Import classes:
//import com.harpoon.api.UdropshipVendorApi;

UdropshipVendorApi apiInstance = new UdropshipVendorApi();
String fk = "fk_example"; // String | Foreign key for vendorLocations
String id = "id_example"; // String | UdropshipVendor id
HarpoonHpublicBrandvenue data = new HarpoonHpublicBrandvenue(); // HarpoonHpublicBrandvenue | 
try {
    HarpoonHpublicBrandvenue result = apiInstance.udropshipVendorPrototypeUpdateByIdVendorLocations(fk, id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling UdropshipVendorApi#udropshipVendorPrototypeUpdateByIdVendorLocations");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for vendorLocations |
 **id** | **String**| UdropshipVendor id |
 **data** | [**HarpoonHpublicBrandvenue**](HarpoonHpublicBrandvenue.md)|  | [optional]

### Return type

[**HarpoonHpublicBrandvenue**](HarpoonHpublicBrandvenue.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="udropshipVendorPrototypeUpdateConfig"></a>
# **udropshipVendorPrototypeUpdateConfig**
> HarpoonHpublicVendorconfig udropshipVendorPrototypeUpdateConfig(id, data)

Update config of this model.

### Example
```java
// Import classes:
//import com.harpoon.api.UdropshipVendorApi;

UdropshipVendorApi apiInstance = new UdropshipVendorApi();
String id = "id_example"; // String | UdropshipVendor id
HarpoonHpublicVendorconfig data = new HarpoonHpublicVendorconfig(); // HarpoonHpublicVendorconfig | 
try {
    HarpoonHpublicVendorconfig result = apiInstance.udropshipVendorPrototypeUpdateConfig(id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling UdropshipVendorApi#udropshipVendorPrototypeUpdateConfig");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| UdropshipVendor id |
 **data** | [**HarpoonHpublicVendorconfig**](HarpoonHpublicVendorconfig.md)|  | [optional]

### Return type

[**HarpoonHpublicVendorconfig**](HarpoonHpublicVendorconfig.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="udropshipVendorReplaceById"></a>
# **udropshipVendorReplaceById**
> UdropshipVendor udropshipVendorReplaceById(id, data)

Replace attributes for a model instance and persist it into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.UdropshipVendorApi;

UdropshipVendorApi apiInstance = new UdropshipVendorApi();
String id = "id_example"; // String | Model id
UdropshipVendor data = new UdropshipVendor(); // UdropshipVendor | Model instance data
try {
    UdropshipVendor result = apiInstance.udropshipVendorReplaceById(id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling UdropshipVendorApi#udropshipVendorReplaceById");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |
 **data** | [**UdropshipVendor**](UdropshipVendor.md)| Model instance data | [optional]

### Return type

[**UdropshipVendor**](UdropshipVendor.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="udropshipVendorReplaceOrCreate"></a>
# **udropshipVendorReplaceOrCreate**
> UdropshipVendor udropshipVendorReplaceOrCreate(data)

Replace an existing model instance or insert a new one into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.UdropshipVendorApi;

UdropshipVendorApi apiInstance = new UdropshipVendorApi();
UdropshipVendor data = new UdropshipVendor(); // UdropshipVendor | Model instance data
try {
    UdropshipVendor result = apiInstance.udropshipVendorReplaceOrCreate(data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling UdropshipVendorApi#udropshipVendorReplaceOrCreate");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**UdropshipVendor**](UdropshipVendor.md)| Model instance data | [optional]

### Return type

[**UdropshipVendor**](UdropshipVendor.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="udropshipVendorUpdateAll"></a>
# **udropshipVendorUpdateAll**
> InlineResponse2002 udropshipVendorUpdateAll(where, data)

Update instances of the model matched by {{where}} from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.UdropshipVendorApi;

UdropshipVendorApi apiInstance = new UdropshipVendorApi();
String where = "where_example"; // String | Criteria to match model instances
UdropshipVendor data = new UdropshipVendor(); // UdropshipVendor | An object of model property name/value pairs
try {
    InlineResponse2002 result = apiInstance.udropshipVendorUpdateAll(where, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling UdropshipVendorApi#udropshipVendorUpdateAll");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional]
 **data** | [**UdropshipVendor**](UdropshipVendor.md)| An object of model property name/value pairs | [optional]

### Return type

[**InlineResponse2002**](InlineResponse2002.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="udropshipVendorUpsertPatchUdropshipVendors"></a>
# **udropshipVendorUpsertPatchUdropshipVendors**
> UdropshipVendor udropshipVendorUpsertPatchUdropshipVendors(data)

Patch an existing model instance or insert a new one into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.UdropshipVendorApi;

UdropshipVendorApi apiInstance = new UdropshipVendorApi();
UdropshipVendor data = new UdropshipVendor(); // UdropshipVendor | Model instance data
try {
    UdropshipVendor result = apiInstance.udropshipVendorUpsertPatchUdropshipVendors(data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling UdropshipVendorApi#udropshipVendorUpsertPatchUdropshipVendors");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**UdropshipVendor**](UdropshipVendor.md)| Model instance data | [optional]

### Return type

[**UdropshipVendor**](UdropshipVendor.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="udropshipVendorUpsertPutUdropshipVendors"></a>
# **udropshipVendorUpsertPutUdropshipVendors**
> UdropshipVendor udropshipVendorUpsertPutUdropshipVendors(data)

Patch an existing model instance or insert a new one into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.UdropshipVendorApi;

UdropshipVendorApi apiInstance = new UdropshipVendorApi();
UdropshipVendor data = new UdropshipVendor(); // UdropshipVendor | Model instance data
try {
    UdropshipVendor result = apiInstance.udropshipVendorUpsertPutUdropshipVendors(data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling UdropshipVendorApi#udropshipVendorUpsertPutUdropshipVendors");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**UdropshipVendor**](UdropshipVendor.md)| Model instance data | [optional]

### Return type

[**UdropshipVendor**](UdropshipVendor.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="udropshipVendorUpsertWithWhere"></a>
# **udropshipVendorUpsertWithWhere**
> UdropshipVendor udropshipVendorUpsertWithWhere(where, data)

Update an existing model instance or insert a new one into the data source based on the where criteria.

### Example
```java
// Import classes:
//import com.harpoon.api.UdropshipVendorApi;

UdropshipVendorApi apiInstance = new UdropshipVendorApi();
String where = "where_example"; // String | Criteria to match model instances
UdropshipVendor data = new UdropshipVendor(); // UdropshipVendor | An object of model property name/value pairs
try {
    UdropshipVendor result = apiInstance.udropshipVendorUpsertWithWhere(where, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling UdropshipVendorApi#udropshipVendorUpsertWithWhere");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional]
 **data** | [**UdropshipVendor**](UdropshipVendor.md)| An object of model property name/value pairs | [optional]

### Return type

[**UdropshipVendor**](UdropshipVendor.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

