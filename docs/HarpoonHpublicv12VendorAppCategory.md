
# HarpoonHpublicv12VendorAppCategory

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Double** |  | 
**appId** | **Double** |  | 
**vendorId** | **Double** |  | 
**categoryId** | **Double** |  | 
**isPrimary** | **Double** |  |  [optional]
**createdAt** | [**Date**](Date.md) |  |  [optional]
**updatedAt** | [**Date**](Date.md) |  |  [optional]



