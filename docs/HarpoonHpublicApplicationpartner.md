
# HarpoonHpublicApplicationpartner

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Double** |  | 
**applicationId** | **Double** |  | 
**brandId** | **Double** |  | 
**status** | **String** |  | 
**invitedEmail** | **String** |  |  [optional]
**invitedAt** | [**Date**](Date.md) |  |  [optional]
**acceptedAt** | [**Date**](Date.md) |  |  [optional]
**createdAt** | [**Date**](Date.md) |  |  [optional]
**updatedAt** | [**Date**](Date.md) |  |  [optional]
**configList** | **Double** |  |  [optional]
**configFeed** | **Double** |  |  [optional]
**configNeedFollow** | **Double** |  |  [optional]
**configAcceptEvent** | **Double** |  |  [optional]
**configAcceptCoupon** | **Double** |  |  [optional]
**configAcceptDealsimple** | **Double** |  |  [optional]
**configAcceptDealgroup** | **Double** |  |  [optional]
**configAcceptNotificationpush** | **Double** |  |  [optional]
**configAcceptNotificationbeacon** | **Double** |  |  [optional]
**isOwner** | **Double** |  |  [optional]
**configApproveEvent** | **Double** |  |  [optional]
**configApproveCoupon** | **Double** |  |  [optional]
**configApproveDealsimple** | **Double** |  |  [optional]
**configApproveDealgroup** | **Double** |  |  [optional]
**udropshipVendor** | **Object** |  |  [optional]



