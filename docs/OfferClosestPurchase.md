
# OfferClosestPurchase

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**expiredAt** | [**Date**](Date.md) |  |  [optional]
**purchasedAt** | [**Date**](Date.md) |  |  [optional]
**id** | **Double** |  |  [optional]



