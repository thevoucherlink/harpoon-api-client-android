
# RssFeed

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**category** | **String** |  | 
**subCategory** | **String** |  |  [optional]
**url** | **String** |  | 
**name** | **String** |  |  [optional]
**sortOrder** | **Double** |  |  [optional]
**styleCss** | **String** |  |  [optional]
**noAds** | **Boolean** |  |  [optional]
**adMRectVisible** | **Boolean** |  |  [optional]
**adMRectPosition** | **Double** |  |  [optional]
**id** | **Double** |  |  [optional]
**appId** | **String** |  |  [optional]
**rssFeedGroupId** | **Double** |  |  [optional]
**rssFeedGroup** | **Object** |  |  [optional]



