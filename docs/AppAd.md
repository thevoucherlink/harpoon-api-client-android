
# AppAd

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**unitId** | **String** |  | 
**unitName** | **String** |  |  [optional]
**unitType** | [**UnitTypeEnum**](#UnitTypeEnum) |  |  [optional]


<a name="UnitTypeEnum"></a>
## Enum: UnitTypeEnum
Name | Value
---- | -----



