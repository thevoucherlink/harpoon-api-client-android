# PlaylistApi

All URIs are relative to *https://apicdn.harpoonconnect.com/v1.1.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**playlistCount**](PlaylistApi.md#playlistCount) | **GET** /Playlists/count | Count instances of the model matched by where from the data source.
[**playlistCreate**](PlaylistApi.md#playlistCreate) | **POST** /Playlists | Create a new instance of the model and persist it into the data source.
[**playlistCreateChangeStreamGetPlaylistsChangeStream**](PlaylistApi.md#playlistCreateChangeStreamGetPlaylistsChangeStream) | **GET** /Playlists/change-stream | Create a change stream.
[**playlistCreateChangeStreamPostPlaylistsChangeStream**](PlaylistApi.md#playlistCreateChangeStreamPostPlaylistsChangeStream) | **POST** /Playlists/change-stream | Create a change stream.
[**playlistDeleteById**](PlaylistApi.md#playlistDeleteById) | **DELETE** /Playlists/{id} | Delete a model instance by {{id}} from the data source.
[**playlistExistsGetPlaylistsidExists**](PlaylistApi.md#playlistExistsGetPlaylistsidExists) | **GET** /Playlists/{id}/exists | Check whether a model instance exists in the data source.
[**playlistExistsHeadPlaylistsid**](PlaylistApi.md#playlistExistsHeadPlaylistsid) | **HEAD** /Playlists/{id} | Check whether a model instance exists in the data source.
[**playlistFind**](PlaylistApi.md#playlistFind) | **GET** /Playlists | Find all instances of the model matched by filter from the data source.
[**playlistFindById**](PlaylistApi.md#playlistFindById) | **GET** /Playlists/{id} | Find a model instance by {{id}} from the data source.
[**playlistFindOne**](PlaylistApi.md#playlistFindOne) | **GET** /Playlists/findOne | Find first instance of the model matched by filter from the data source.
[**playlistPrototypeCountPlaylistItems**](PlaylistApi.md#playlistPrototypeCountPlaylistItems) | **GET** /Playlists/{id}/playlistItems/count | Counts playlistItems of Playlist.
[**playlistPrototypeCreatePlaylistItems**](PlaylistApi.md#playlistPrototypeCreatePlaylistItems) | **POST** /Playlists/{id}/playlistItems | Creates a new instance in playlistItems of this model.
[**playlistPrototypeDeletePlaylistItems**](PlaylistApi.md#playlistPrototypeDeletePlaylistItems) | **DELETE** /Playlists/{id}/playlistItems | Deletes all playlistItems of this model.
[**playlistPrototypeDestroyByIdPlaylistItems**](PlaylistApi.md#playlistPrototypeDestroyByIdPlaylistItems) | **DELETE** /Playlists/{id}/playlistItems/{fk} | Delete a related item by id for playlistItems.
[**playlistPrototypeFindByIdPlaylistItems**](PlaylistApi.md#playlistPrototypeFindByIdPlaylistItems) | **GET** /Playlists/{id}/playlistItems/{fk} | Find a related item by id for playlistItems.
[**playlistPrototypeGetPlaylistItems**](PlaylistApi.md#playlistPrototypeGetPlaylistItems) | **GET** /Playlists/{id}/playlistItems | Queries playlistItems of Playlist.
[**playlistPrototypeUpdateAttributesPatchPlaylistsid**](PlaylistApi.md#playlistPrototypeUpdateAttributesPatchPlaylistsid) | **PATCH** /Playlists/{id} | Patch attributes for a model instance and persist it into the data source.
[**playlistPrototypeUpdateAttributesPutPlaylistsid**](PlaylistApi.md#playlistPrototypeUpdateAttributesPutPlaylistsid) | **PUT** /Playlists/{id} | Patch attributes for a model instance and persist it into the data source.
[**playlistPrototypeUpdateByIdPlaylistItems**](PlaylistApi.md#playlistPrototypeUpdateByIdPlaylistItems) | **PUT** /Playlists/{id}/playlistItems/{fk} | Update a related item by id for playlistItems.
[**playlistReplaceById**](PlaylistApi.md#playlistReplaceById) | **POST** /Playlists/{id}/replace | Replace attributes for a model instance and persist it into the data source.
[**playlistReplaceOrCreate**](PlaylistApi.md#playlistReplaceOrCreate) | **POST** /Playlists/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
[**playlistUpdateAll**](PlaylistApi.md#playlistUpdateAll) | **POST** /Playlists/update | Update instances of the model matched by {{where}} from the data source.
[**playlistUpsertPatchPlaylists**](PlaylistApi.md#playlistUpsertPatchPlaylists) | **PATCH** /Playlists | Patch an existing model instance or insert a new one into the data source.
[**playlistUpsertPutPlaylists**](PlaylistApi.md#playlistUpsertPutPlaylists) | **PUT** /Playlists | Patch an existing model instance or insert a new one into the data source.
[**playlistUpsertWithWhere**](PlaylistApi.md#playlistUpsertWithWhere) | **POST** /Playlists/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.


<a name="playlistCount"></a>
# **playlistCount**
> InlineResponse2001 playlistCount(where)

Count instances of the model matched by where from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.PlaylistApi;

PlaylistApi apiInstance = new PlaylistApi();
String where = "where_example"; // String | Criteria to match model instances
try {
    InlineResponse2001 result = apiInstance.playlistCount(where);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PlaylistApi#playlistCount");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional]

### Return type

[**InlineResponse2001**](InlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playlistCreate"></a>
# **playlistCreate**
> Playlist playlistCreate(data)

Create a new instance of the model and persist it into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.PlaylistApi;

PlaylistApi apiInstance = new PlaylistApi();
Playlist data = new Playlist(); // Playlist | Model instance data
try {
    Playlist result = apiInstance.playlistCreate(data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PlaylistApi#playlistCreate");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**Playlist**](Playlist.md)| Model instance data | [optional]

### Return type

[**Playlist**](Playlist.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playlistCreateChangeStreamGetPlaylistsChangeStream"></a>
# **playlistCreateChangeStreamGetPlaylistsChangeStream**
> File playlistCreateChangeStreamGetPlaylistsChangeStream(options)

Create a change stream.

### Example
```java
// Import classes:
//import com.harpoon.api.PlaylistApi;

PlaylistApi apiInstance = new PlaylistApi();
String options = "options_example"; // String | 
try {
    File result = apiInstance.playlistCreateChangeStreamGetPlaylistsChangeStream(options);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PlaylistApi#playlistCreateChangeStreamGetPlaylistsChangeStream");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **String**|  | [optional]

### Return type

[**File**](File.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playlistCreateChangeStreamPostPlaylistsChangeStream"></a>
# **playlistCreateChangeStreamPostPlaylistsChangeStream**
> File playlistCreateChangeStreamPostPlaylistsChangeStream(options)

Create a change stream.

### Example
```java
// Import classes:
//import com.harpoon.api.PlaylistApi;

PlaylistApi apiInstance = new PlaylistApi();
String options = "options_example"; // String | 
try {
    File result = apiInstance.playlistCreateChangeStreamPostPlaylistsChangeStream(options);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PlaylistApi#playlistCreateChangeStreamPostPlaylistsChangeStream");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **String**|  | [optional]

### Return type

[**File**](File.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playlistDeleteById"></a>
# **playlistDeleteById**
> Object playlistDeleteById(id)

Delete a model instance by {{id}} from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.PlaylistApi;

PlaylistApi apiInstance = new PlaylistApi();
String id = "id_example"; // String | Model id
try {
    Object result = apiInstance.playlistDeleteById(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PlaylistApi#playlistDeleteById");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |

### Return type

**Object**

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playlistExistsGetPlaylistsidExists"></a>
# **playlistExistsGetPlaylistsidExists**
> InlineResponse2003 playlistExistsGetPlaylistsidExists(id)

Check whether a model instance exists in the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.PlaylistApi;

PlaylistApi apiInstance = new PlaylistApi();
String id = "id_example"; // String | Model id
try {
    InlineResponse2003 result = apiInstance.playlistExistsGetPlaylistsidExists(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PlaylistApi#playlistExistsGetPlaylistsidExists");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |

### Return type

[**InlineResponse2003**](InlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playlistExistsHeadPlaylistsid"></a>
# **playlistExistsHeadPlaylistsid**
> InlineResponse2003 playlistExistsHeadPlaylistsid(id)

Check whether a model instance exists in the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.PlaylistApi;

PlaylistApi apiInstance = new PlaylistApi();
String id = "id_example"; // String | Model id
try {
    InlineResponse2003 result = apiInstance.playlistExistsHeadPlaylistsid(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PlaylistApi#playlistExistsHeadPlaylistsid");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |

### Return type

[**InlineResponse2003**](InlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playlistFind"></a>
# **playlistFind**
> List&lt;Playlist&gt; playlistFind(filter)

Find all instances of the model matched by filter from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.PlaylistApi;

PlaylistApi apiInstance = new PlaylistApi();
String filter = "filter_example"; // String | Filter defining fields, where, include, order, offset, and limit
try {
    List<Playlist> result = apiInstance.playlistFind(filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PlaylistApi#playlistFind");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **String**| Filter defining fields, where, include, order, offset, and limit | [optional]

### Return type

[**List&lt;Playlist&gt;**](Playlist.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playlistFindById"></a>
# **playlistFindById**
> Playlist playlistFindById(id, filter)

Find a model instance by {{id}} from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.PlaylistApi;

PlaylistApi apiInstance = new PlaylistApi();
String id = "id_example"; // String | Model id
String filter = "filter_example"; // String | Filter defining fields and include
try {
    Playlist result = apiInstance.playlistFindById(id, filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PlaylistApi#playlistFindById");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |
 **filter** | **String**| Filter defining fields and include | [optional]

### Return type

[**Playlist**](Playlist.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playlistFindOne"></a>
# **playlistFindOne**
> Playlist playlistFindOne(filter)

Find first instance of the model matched by filter from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.PlaylistApi;

PlaylistApi apiInstance = new PlaylistApi();
String filter = "filter_example"; // String | Filter defining fields, where, include, order, offset, and limit
try {
    Playlist result = apiInstance.playlistFindOne(filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PlaylistApi#playlistFindOne");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **String**| Filter defining fields, where, include, order, offset, and limit | [optional]

### Return type

[**Playlist**](Playlist.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playlistPrototypeCountPlaylistItems"></a>
# **playlistPrototypeCountPlaylistItems**
> InlineResponse2001 playlistPrototypeCountPlaylistItems(id, where)

Counts playlistItems of Playlist.

### Example
```java
// Import classes:
//import com.harpoon.api.PlaylistApi;

PlaylistApi apiInstance = new PlaylistApi();
String id = "id_example"; // String | Playlist id
String where = "where_example"; // String | Criteria to match model instances
try {
    InlineResponse2001 result = apiInstance.playlistPrototypeCountPlaylistItems(id, where);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PlaylistApi#playlistPrototypeCountPlaylistItems");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Playlist id |
 **where** | **String**| Criteria to match model instances | [optional]

### Return type

[**InlineResponse2001**](InlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playlistPrototypeCreatePlaylistItems"></a>
# **playlistPrototypeCreatePlaylistItems**
> PlaylistItem playlistPrototypeCreatePlaylistItems(id, data)

Creates a new instance in playlistItems of this model.

### Example
```java
// Import classes:
//import com.harpoon.api.PlaylistApi;

PlaylistApi apiInstance = new PlaylistApi();
String id = "id_example"; // String | Playlist id
PlaylistItem data = new PlaylistItem(); // PlaylistItem | 
try {
    PlaylistItem result = apiInstance.playlistPrototypeCreatePlaylistItems(id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PlaylistApi#playlistPrototypeCreatePlaylistItems");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Playlist id |
 **data** | [**PlaylistItem**](PlaylistItem.md)|  | [optional]

### Return type

[**PlaylistItem**](PlaylistItem.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playlistPrototypeDeletePlaylistItems"></a>
# **playlistPrototypeDeletePlaylistItems**
> playlistPrototypeDeletePlaylistItems(id)

Deletes all playlistItems of this model.

### Example
```java
// Import classes:
//import com.harpoon.api.PlaylistApi;

PlaylistApi apiInstance = new PlaylistApi();
String id = "id_example"; // String | Playlist id
try {
    apiInstance.playlistPrototypeDeletePlaylistItems(id);
} catch (ApiException e) {
    System.err.println("Exception when calling PlaylistApi#playlistPrototypeDeletePlaylistItems");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Playlist id |

### Return type

null (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playlistPrototypeDestroyByIdPlaylistItems"></a>
# **playlistPrototypeDestroyByIdPlaylistItems**
> playlistPrototypeDestroyByIdPlaylistItems(fk, id)

Delete a related item by id for playlistItems.

### Example
```java
// Import classes:
//import com.harpoon.api.PlaylistApi;

PlaylistApi apiInstance = new PlaylistApi();
String fk = "fk_example"; // String | Foreign key for playlistItems
String id = "id_example"; // String | Playlist id
try {
    apiInstance.playlistPrototypeDestroyByIdPlaylistItems(fk, id);
} catch (ApiException e) {
    System.err.println("Exception when calling PlaylistApi#playlistPrototypeDestroyByIdPlaylistItems");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for playlistItems |
 **id** | **String**| Playlist id |

### Return type

null (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playlistPrototypeFindByIdPlaylistItems"></a>
# **playlistPrototypeFindByIdPlaylistItems**
> PlaylistItem playlistPrototypeFindByIdPlaylistItems(fk, id)

Find a related item by id for playlistItems.

### Example
```java
// Import classes:
//import com.harpoon.api.PlaylistApi;

PlaylistApi apiInstance = new PlaylistApi();
String fk = "fk_example"; // String | Foreign key for playlistItems
String id = "id_example"; // String | Playlist id
try {
    PlaylistItem result = apiInstance.playlistPrototypeFindByIdPlaylistItems(fk, id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PlaylistApi#playlistPrototypeFindByIdPlaylistItems");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for playlistItems |
 **id** | **String**| Playlist id |

### Return type

[**PlaylistItem**](PlaylistItem.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playlistPrototypeGetPlaylistItems"></a>
# **playlistPrototypeGetPlaylistItems**
> List&lt;PlaylistItem&gt; playlistPrototypeGetPlaylistItems(id, filter)

Queries playlistItems of Playlist.

### Example
```java
// Import classes:
//import com.harpoon.api.PlaylistApi;

PlaylistApi apiInstance = new PlaylistApi();
String id = "id_example"; // String | Playlist id
String filter = "filter_example"; // String | 
try {
    List<PlaylistItem> result = apiInstance.playlistPrototypeGetPlaylistItems(id, filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PlaylistApi#playlistPrototypeGetPlaylistItems");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Playlist id |
 **filter** | **String**|  | [optional]

### Return type

[**List&lt;PlaylistItem&gt;**](PlaylistItem.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playlistPrototypeUpdateAttributesPatchPlaylistsid"></a>
# **playlistPrototypeUpdateAttributesPatchPlaylistsid**
> Playlist playlistPrototypeUpdateAttributesPatchPlaylistsid(id, data)

Patch attributes for a model instance and persist it into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.PlaylistApi;

PlaylistApi apiInstance = new PlaylistApi();
String id = "id_example"; // String | Playlist id
Playlist data = new Playlist(); // Playlist | An object of model property name/value pairs
try {
    Playlist result = apiInstance.playlistPrototypeUpdateAttributesPatchPlaylistsid(id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PlaylistApi#playlistPrototypeUpdateAttributesPatchPlaylistsid");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Playlist id |
 **data** | [**Playlist**](Playlist.md)| An object of model property name/value pairs | [optional]

### Return type

[**Playlist**](Playlist.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playlistPrototypeUpdateAttributesPutPlaylistsid"></a>
# **playlistPrototypeUpdateAttributesPutPlaylistsid**
> Playlist playlistPrototypeUpdateAttributesPutPlaylistsid(id, data)

Patch attributes for a model instance and persist it into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.PlaylistApi;

PlaylistApi apiInstance = new PlaylistApi();
String id = "id_example"; // String | Playlist id
Playlist data = new Playlist(); // Playlist | An object of model property name/value pairs
try {
    Playlist result = apiInstance.playlistPrototypeUpdateAttributesPutPlaylistsid(id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PlaylistApi#playlistPrototypeUpdateAttributesPutPlaylistsid");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Playlist id |
 **data** | [**Playlist**](Playlist.md)| An object of model property name/value pairs | [optional]

### Return type

[**Playlist**](Playlist.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playlistPrototypeUpdateByIdPlaylistItems"></a>
# **playlistPrototypeUpdateByIdPlaylistItems**
> PlaylistItem playlistPrototypeUpdateByIdPlaylistItems(fk, id, data)

Update a related item by id for playlistItems.

### Example
```java
// Import classes:
//import com.harpoon.api.PlaylistApi;

PlaylistApi apiInstance = new PlaylistApi();
String fk = "fk_example"; // String | Foreign key for playlistItems
String id = "id_example"; // String | Playlist id
PlaylistItem data = new PlaylistItem(); // PlaylistItem | 
try {
    PlaylistItem result = apiInstance.playlistPrototypeUpdateByIdPlaylistItems(fk, id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PlaylistApi#playlistPrototypeUpdateByIdPlaylistItems");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for playlistItems |
 **id** | **String**| Playlist id |
 **data** | [**PlaylistItem**](PlaylistItem.md)|  | [optional]

### Return type

[**PlaylistItem**](PlaylistItem.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playlistReplaceById"></a>
# **playlistReplaceById**
> Playlist playlistReplaceById(id, data)

Replace attributes for a model instance and persist it into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.PlaylistApi;

PlaylistApi apiInstance = new PlaylistApi();
String id = "id_example"; // String | Model id
Playlist data = new Playlist(); // Playlist | Model instance data
try {
    Playlist result = apiInstance.playlistReplaceById(id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PlaylistApi#playlistReplaceById");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |
 **data** | [**Playlist**](Playlist.md)| Model instance data | [optional]

### Return type

[**Playlist**](Playlist.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playlistReplaceOrCreate"></a>
# **playlistReplaceOrCreate**
> Playlist playlistReplaceOrCreate(data)

Replace an existing model instance or insert a new one into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.PlaylistApi;

PlaylistApi apiInstance = new PlaylistApi();
Playlist data = new Playlist(); // Playlist | Model instance data
try {
    Playlist result = apiInstance.playlistReplaceOrCreate(data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PlaylistApi#playlistReplaceOrCreate");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**Playlist**](Playlist.md)| Model instance data | [optional]

### Return type

[**Playlist**](Playlist.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playlistUpdateAll"></a>
# **playlistUpdateAll**
> InlineResponse2002 playlistUpdateAll(where, data)

Update instances of the model matched by {{where}} from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.PlaylistApi;

PlaylistApi apiInstance = new PlaylistApi();
String where = "where_example"; // String | Criteria to match model instances
Playlist data = new Playlist(); // Playlist | An object of model property name/value pairs
try {
    InlineResponse2002 result = apiInstance.playlistUpdateAll(where, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PlaylistApi#playlistUpdateAll");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional]
 **data** | [**Playlist**](Playlist.md)| An object of model property name/value pairs | [optional]

### Return type

[**InlineResponse2002**](InlineResponse2002.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playlistUpsertPatchPlaylists"></a>
# **playlistUpsertPatchPlaylists**
> Playlist playlistUpsertPatchPlaylists(data)

Patch an existing model instance or insert a new one into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.PlaylistApi;

PlaylistApi apiInstance = new PlaylistApi();
Playlist data = new Playlist(); // Playlist | Model instance data
try {
    Playlist result = apiInstance.playlistUpsertPatchPlaylists(data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PlaylistApi#playlistUpsertPatchPlaylists");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**Playlist**](Playlist.md)| Model instance data | [optional]

### Return type

[**Playlist**](Playlist.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playlistUpsertPutPlaylists"></a>
# **playlistUpsertPutPlaylists**
> Playlist playlistUpsertPutPlaylists(data)

Patch an existing model instance or insert a new one into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.PlaylistApi;

PlaylistApi apiInstance = new PlaylistApi();
Playlist data = new Playlist(); // Playlist | Model instance data
try {
    Playlist result = apiInstance.playlistUpsertPutPlaylists(data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PlaylistApi#playlistUpsertPutPlaylists");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**Playlist**](Playlist.md)| Model instance data | [optional]

### Return type

[**Playlist**](Playlist.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="playlistUpsertWithWhere"></a>
# **playlistUpsertWithWhere**
> Playlist playlistUpsertWithWhere(where, data)

Update an existing model instance or insert a new one into the data source based on the where criteria.

### Example
```java
// Import classes:
//import com.harpoon.api.PlaylistApi;

PlaylistApi apiInstance = new PlaylistApi();
String where = "where_example"; // String | Criteria to match model instances
Playlist data = new Playlist(); // Playlist | An object of model property name/value pairs
try {
    Playlist result = apiInstance.playlistUpsertWithWhere(where, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling PlaylistApi#playlistUpsertWithWhere");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional]
 **data** | [**Playlist**](Playlist.md)| An object of model property name/value pairs | [optional]

### Return type

[**Playlist**](Playlist.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

