# StripeSubscriptionApi

All URIs are relative to *https://apicdn.harpoonconnect.com/v1.1.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**stripeSubscriptionCount**](StripeSubscriptionApi.md#stripeSubscriptionCount) | **GET** /StripeSubscriptions/count | Count instances of the model matched by where from the data source.
[**stripeSubscriptionCreate**](StripeSubscriptionApi.md#stripeSubscriptionCreate) | **POST** /StripeSubscriptions | Create a new instance of the model and persist it into the data source.
[**stripeSubscriptionCreateChangeStreamGetStripeSubscriptionsChangeStream**](StripeSubscriptionApi.md#stripeSubscriptionCreateChangeStreamGetStripeSubscriptionsChangeStream) | **GET** /StripeSubscriptions/change-stream | Create a change stream.
[**stripeSubscriptionCreateChangeStreamPostStripeSubscriptionsChangeStream**](StripeSubscriptionApi.md#stripeSubscriptionCreateChangeStreamPostStripeSubscriptionsChangeStream) | **POST** /StripeSubscriptions/change-stream | Create a change stream.
[**stripeSubscriptionDeleteById**](StripeSubscriptionApi.md#stripeSubscriptionDeleteById) | **DELETE** /StripeSubscriptions/{id} | Delete a model instance by {{id}} from the data source.
[**stripeSubscriptionExistsGetStripeSubscriptionsidExists**](StripeSubscriptionApi.md#stripeSubscriptionExistsGetStripeSubscriptionsidExists) | **GET** /StripeSubscriptions/{id}/exists | Check whether a model instance exists in the data source.
[**stripeSubscriptionExistsHeadStripeSubscriptionsid**](StripeSubscriptionApi.md#stripeSubscriptionExistsHeadStripeSubscriptionsid) | **HEAD** /StripeSubscriptions/{id} | Check whether a model instance exists in the data source.
[**stripeSubscriptionFind**](StripeSubscriptionApi.md#stripeSubscriptionFind) | **GET** /StripeSubscriptions | Find all instances of the model matched by filter from the data source.
[**stripeSubscriptionFindById**](StripeSubscriptionApi.md#stripeSubscriptionFindById) | **GET** /StripeSubscriptions/{id} | Find a model instance by {{id}} from the data source.
[**stripeSubscriptionFindOne**](StripeSubscriptionApi.md#stripeSubscriptionFindOne) | **GET** /StripeSubscriptions/findOne | Find first instance of the model matched by filter from the data source.
[**stripeSubscriptionPrototypeUpdateAttributesPatchStripeSubscriptionsid**](StripeSubscriptionApi.md#stripeSubscriptionPrototypeUpdateAttributesPatchStripeSubscriptionsid) | **PATCH** /StripeSubscriptions/{id} | Patch attributes for a model instance and persist it into the data source.
[**stripeSubscriptionPrototypeUpdateAttributesPutStripeSubscriptionsid**](StripeSubscriptionApi.md#stripeSubscriptionPrototypeUpdateAttributesPutStripeSubscriptionsid) | **PUT** /StripeSubscriptions/{id} | Patch attributes for a model instance and persist it into the data source.
[**stripeSubscriptionReplaceById**](StripeSubscriptionApi.md#stripeSubscriptionReplaceById) | **POST** /StripeSubscriptions/{id}/replace | Replace attributes for a model instance and persist it into the data source.
[**stripeSubscriptionReplaceOrCreate**](StripeSubscriptionApi.md#stripeSubscriptionReplaceOrCreate) | **POST** /StripeSubscriptions/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
[**stripeSubscriptionUpdateAll**](StripeSubscriptionApi.md#stripeSubscriptionUpdateAll) | **POST** /StripeSubscriptions/update | Update instances of the model matched by {{where}} from the data source.
[**stripeSubscriptionUpsertPatchStripeSubscriptions**](StripeSubscriptionApi.md#stripeSubscriptionUpsertPatchStripeSubscriptions) | **PATCH** /StripeSubscriptions | Patch an existing model instance or insert a new one into the data source.
[**stripeSubscriptionUpsertPutStripeSubscriptions**](StripeSubscriptionApi.md#stripeSubscriptionUpsertPutStripeSubscriptions) | **PUT** /StripeSubscriptions | Patch an existing model instance or insert a new one into the data source.
[**stripeSubscriptionUpsertWithWhere**](StripeSubscriptionApi.md#stripeSubscriptionUpsertWithWhere) | **POST** /StripeSubscriptions/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.


<a name="stripeSubscriptionCount"></a>
# **stripeSubscriptionCount**
> InlineResponse2001 stripeSubscriptionCount(where)

Count instances of the model matched by where from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.StripeSubscriptionApi;

StripeSubscriptionApi apiInstance = new StripeSubscriptionApi();
String where = "where_example"; // String | Criteria to match model instances
try {
    InlineResponse2001 result = apiInstance.stripeSubscriptionCount(where);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling StripeSubscriptionApi#stripeSubscriptionCount");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional]

### Return type

[**InlineResponse2001**](InlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripeSubscriptionCreate"></a>
# **stripeSubscriptionCreate**
> StripeSubscription stripeSubscriptionCreate(data)

Create a new instance of the model and persist it into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.StripeSubscriptionApi;

StripeSubscriptionApi apiInstance = new StripeSubscriptionApi();
StripeSubscription data = new StripeSubscription(); // StripeSubscription | Model instance data
try {
    StripeSubscription result = apiInstance.stripeSubscriptionCreate(data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling StripeSubscriptionApi#stripeSubscriptionCreate");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**StripeSubscription**](StripeSubscription.md)| Model instance data | [optional]

### Return type

[**StripeSubscription**](StripeSubscription.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripeSubscriptionCreateChangeStreamGetStripeSubscriptionsChangeStream"></a>
# **stripeSubscriptionCreateChangeStreamGetStripeSubscriptionsChangeStream**
> File stripeSubscriptionCreateChangeStreamGetStripeSubscriptionsChangeStream(options)

Create a change stream.

### Example
```java
// Import classes:
//import com.harpoon.api.StripeSubscriptionApi;

StripeSubscriptionApi apiInstance = new StripeSubscriptionApi();
String options = "options_example"; // String | 
try {
    File result = apiInstance.stripeSubscriptionCreateChangeStreamGetStripeSubscriptionsChangeStream(options);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling StripeSubscriptionApi#stripeSubscriptionCreateChangeStreamGetStripeSubscriptionsChangeStream");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **String**|  | [optional]

### Return type

[**File**](File.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripeSubscriptionCreateChangeStreamPostStripeSubscriptionsChangeStream"></a>
# **stripeSubscriptionCreateChangeStreamPostStripeSubscriptionsChangeStream**
> File stripeSubscriptionCreateChangeStreamPostStripeSubscriptionsChangeStream(options)

Create a change stream.

### Example
```java
// Import classes:
//import com.harpoon.api.StripeSubscriptionApi;

StripeSubscriptionApi apiInstance = new StripeSubscriptionApi();
String options = "options_example"; // String | 
try {
    File result = apiInstance.stripeSubscriptionCreateChangeStreamPostStripeSubscriptionsChangeStream(options);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling StripeSubscriptionApi#stripeSubscriptionCreateChangeStreamPostStripeSubscriptionsChangeStream");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **String**|  | [optional]

### Return type

[**File**](File.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripeSubscriptionDeleteById"></a>
# **stripeSubscriptionDeleteById**
> Object stripeSubscriptionDeleteById(id)

Delete a model instance by {{id}} from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.StripeSubscriptionApi;

StripeSubscriptionApi apiInstance = new StripeSubscriptionApi();
String id = "id_example"; // String | Model id
try {
    Object result = apiInstance.stripeSubscriptionDeleteById(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling StripeSubscriptionApi#stripeSubscriptionDeleteById");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |

### Return type

**Object**

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripeSubscriptionExistsGetStripeSubscriptionsidExists"></a>
# **stripeSubscriptionExistsGetStripeSubscriptionsidExists**
> InlineResponse2003 stripeSubscriptionExistsGetStripeSubscriptionsidExists(id)

Check whether a model instance exists in the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.StripeSubscriptionApi;

StripeSubscriptionApi apiInstance = new StripeSubscriptionApi();
String id = "id_example"; // String | Model id
try {
    InlineResponse2003 result = apiInstance.stripeSubscriptionExistsGetStripeSubscriptionsidExists(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling StripeSubscriptionApi#stripeSubscriptionExistsGetStripeSubscriptionsidExists");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |

### Return type

[**InlineResponse2003**](InlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripeSubscriptionExistsHeadStripeSubscriptionsid"></a>
# **stripeSubscriptionExistsHeadStripeSubscriptionsid**
> InlineResponse2003 stripeSubscriptionExistsHeadStripeSubscriptionsid(id)

Check whether a model instance exists in the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.StripeSubscriptionApi;

StripeSubscriptionApi apiInstance = new StripeSubscriptionApi();
String id = "id_example"; // String | Model id
try {
    InlineResponse2003 result = apiInstance.stripeSubscriptionExistsHeadStripeSubscriptionsid(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling StripeSubscriptionApi#stripeSubscriptionExistsHeadStripeSubscriptionsid");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |

### Return type

[**InlineResponse2003**](InlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripeSubscriptionFind"></a>
# **stripeSubscriptionFind**
> List&lt;StripeSubscription&gt; stripeSubscriptionFind(filter)

Find all instances of the model matched by filter from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.StripeSubscriptionApi;

StripeSubscriptionApi apiInstance = new StripeSubscriptionApi();
String filter = "filter_example"; // String | Filter defining fields, where, include, order, offset, and limit
try {
    List<StripeSubscription> result = apiInstance.stripeSubscriptionFind(filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling StripeSubscriptionApi#stripeSubscriptionFind");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **String**| Filter defining fields, where, include, order, offset, and limit | [optional]

### Return type

[**List&lt;StripeSubscription&gt;**](StripeSubscription.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripeSubscriptionFindById"></a>
# **stripeSubscriptionFindById**
> StripeSubscription stripeSubscriptionFindById(id, filter)

Find a model instance by {{id}} from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.StripeSubscriptionApi;

StripeSubscriptionApi apiInstance = new StripeSubscriptionApi();
String id = "id_example"; // String | Model id
String filter = "filter_example"; // String | Filter defining fields and include
try {
    StripeSubscription result = apiInstance.stripeSubscriptionFindById(id, filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling StripeSubscriptionApi#stripeSubscriptionFindById");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |
 **filter** | **String**| Filter defining fields and include | [optional]

### Return type

[**StripeSubscription**](StripeSubscription.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripeSubscriptionFindOne"></a>
# **stripeSubscriptionFindOne**
> StripeSubscription stripeSubscriptionFindOne(filter)

Find first instance of the model matched by filter from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.StripeSubscriptionApi;

StripeSubscriptionApi apiInstance = new StripeSubscriptionApi();
String filter = "filter_example"; // String | Filter defining fields, where, include, order, offset, and limit
try {
    StripeSubscription result = apiInstance.stripeSubscriptionFindOne(filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling StripeSubscriptionApi#stripeSubscriptionFindOne");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **String**| Filter defining fields, where, include, order, offset, and limit | [optional]

### Return type

[**StripeSubscription**](StripeSubscription.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripeSubscriptionPrototypeUpdateAttributesPatchStripeSubscriptionsid"></a>
# **stripeSubscriptionPrototypeUpdateAttributesPatchStripeSubscriptionsid**
> StripeSubscription stripeSubscriptionPrototypeUpdateAttributesPatchStripeSubscriptionsid(id, data)

Patch attributes for a model instance and persist it into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.StripeSubscriptionApi;

StripeSubscriptionApi apiInstance = new StripeSubscriptionApi();
String id = "id_example"; // String | StripeSubscription id
StripeSubscription data = new StripeSubscription(); // StripeSubscription | An object of model property name/value pairs
try {
    StripeSubscription result = apiInstance.stripeSubscriptionPrototypeUpdateAttributesPatchStripeSubscriptionsid(id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling StripeSubscriptionApi#stripeSubscriptionPrototypeUpdateAttributesPatchStripeSubscriptionsid");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| StripeSubscription id |
 **data** | [**StripeSubscription**](StripeSubscription.md)| An object of model property name/value pairs | [optional]

### Return type

[**StripeSubscription**](StripeSubscription.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripeSubscriptionPrototypeUpdateAttributesPutStripeSubscriptionsid"></a>
# **stripeSubscriptionPrototypeUpdateAttributesPutStripeSubscriptionsid**
> StripeSubscription stripeSubscriptionPrototypeUpdateAttributesPutStripeSubscriptionsid(id, data)

Patch attributes for a model instance and persist it into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.StripeSubscriptionApi;

StripeSubscriptionApi apiInstance = new StripeSubscriptionApi();
String id = "id_example"; // String | StripeSubscription id
StripeSubscription data = new StripeSubscription(); // StripeSubscription | An object of model property name/value pairs
try {
    StripeSubscription result = apiInstance.stripeSubscriptionPrototypeUpdateAttributesPutStripeSubscriptionsid(id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling StripeSubscriptionApi#stripeSubscriptionPrototypeUpdateAttributesPutStripeSubscriptionsid");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| StripeSubscription id |
 **data** | [**StripeSubscription**](StripeSubscription.md)| An object of model property name/value pairs | [optional]

### Return type

[**StripeSubscription**](StripeSubscription.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripeSubscriptionReplaceById"></a>
# **stripeSubscriptionReplaceById**
> StripeSubscription stripeSubscriptionReplaceById(id, data)

Replace attributes for a model instance and persist it into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.StripeSubscriptionApi;

StripeSubscriptionApi apiInstance = new StripeSubscriptionApi();
String id = "id_example"; // String | Model id
StripeSubscription data = new StripeSubscription(); // StripeSubscription | Model instance data
try {
    StripeSubscription result = apiInstance.stripeSubscriptionReplaceById(id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling StripeSubscriptionApi#stripeSubscriptionReplaceById");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |
 **data** | [**StripeSubscription**](StripeSubscription.md)| Model instance data | [optional]

### Return type

[**StripeSubscription**](StripeSubscription.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripeSubscriptionReplaceOrCreate"></a>
# **stripeSubscriptionReplaceOrCreate**
> StripeSubscription stripeSubscriptionReplaceOrCreate(data)

Replace an existing model instance or insert a new one into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.StripeSubscriptionApi;

StripeSubscriptionApi apiInstance = new StripeSubscriptionApi();
StripeSubscription data = new StripeSubscription(); // StripeSubscription | Model instance data
try {
    StripeSubscription result = apiInstance.stripeSubscriptionReplaceOrCreate(data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling StripeSubscriptionApi#stripeSubscriptionReplaceOrCreate");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**StripeSubscription**](StripeSubscription.md)| Model instance data | [optional]

### Return type

[**StripeSubscription**](StripeSubscription.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripeSubscriptionUpdateAll"></a>
# **stripeSubscriptionUpdateAll**
> InlineResponse2002 stripeSubscriptionUpdateAll(where, data)

Update instances of the model matched by {{where}} from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.StripeSubscriptionApi;

StripeSubscriptionApi apiInstance = new StripeSubscriptionApi();
String where = "where_example"; // String | Criteria to match model instances
StripeSubscription data = new StripeSubscription(); // StripeSubscription | An object of model property name/value pairs
try {
    InlineResponse2002 result = apiInstance.stripeSubscriptionUpdateAll(where, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling StripeSubscriptionApi#stripeSubscriptionUpdateAll");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional]
 **data** | [**StripeSubscription**](StripeSubscription.md)| An object of model property name/value pairs | [optional]

### Return type

[**InlineResponse2002**](InlineResponse2002.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripeSubscriptionUpsertPatchStripeSubscriptions"></a>
# **stripeSubscriptionUpsertPatchStripeSubscriptions**
> StripeSubscription stripeSubscriptionUpsertPatchStripeSubscriptions(data)

Patch an existing model instance or insert a new one into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.StripeSubscriptionApi;

StripeSubscriptionApi apiInstance = new StripeSubscriptionApi();
StripeSubscription data = new StripeSubscription(); // StripeSubscription | Model instance data
try {
    StripeSubscription result = apiInstance.stripeSubscriptionUpsertPatchStripeSubscriptions(data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling StripeSubscriptionApi#stripeSubscriptionUpsertPatchStripeSubscriptions");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**StripeSubscription**](StripeSubscription.md)| Model instance data | [optional]

### Return type

[**StripeSubscription**](StripeSubscription.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripeSubscriptionUpsertPutStripeSubscriptions"></a>
# **stripeSubscriptionUpsertPutStripeSubscriptions**
> StripeSubscription stripeSubscriptionUpsertPutStripeSubscriptions(data)

Patch an existing model instance or insert a new one into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.StripeSubscriptionApi;

StripeSubscriptionApi apiInstance = new StripeSubscriptionApi();
StripeSubscription data = new StripeSubscription(); // StripeSubscription | Model instance data
try {
    StripeSubscription result = apiInstance.stripeSubscriptionUpsertPutStripeSubscriptions(data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling StripeSubscriptionApi#stripeSubscriptionUpsertPutStripeSubscriptions");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**StripeSubscription**](StripeSubscription.md)| Model instance data | [optional]

### Return type

[**StripeSubscription**](StripeSubscription.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripeSubscriptionUpsertWithWhere"></a>
# **stripeSubscriptionUpsertWithWhere**
> StripeSubscription stripeSubscriptionUpsertWithWhere(where, data)

Update an existing model instance or insert a new one into the data source based on the where criteria.

### Example
```java
// Import classes:
//import com.harpoon.api.StripeSubscriptionApi;

StripeSubscriptionApi apiInstance = new StripeSubscriptionApi();
String where = "where_example"; // String | Criteria to match model instances
StripeSubscription data = new StripeSubscription(); // StripeSubscription | An object of model property name/value pairs
try {
    StripeSubscription result = apiInstance.stripeSubscriptionUpsertWithWhere(where, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling StripeSubscriptionApi#stripeSubscriptionUpsertWithWhere");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional]
 **data** | [**StripeSubscription**](StripeSubscription.md)| An object of model property name/value pairs | [optional]

### Return type

[**StripeSubscription**](StripeSubscription.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

