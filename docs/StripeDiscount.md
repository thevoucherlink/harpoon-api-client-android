
# StripeDiscount

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**object** | **String** |  |  [optional]
**customer** | **String** |  |  [optional]
**end** | **Double** |  |  [optional]
**start** | **Double** |  |  [optional]
**subscription** | **String** |  |  [optional]
**coupon** | [**StripeCoupon**](StripeCoupon.md) |  |  [optional]
**id** | **Double** |  |  [optional]



