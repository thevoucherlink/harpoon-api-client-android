
# AwEventbookingEventTicketAttribute

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Double** |  | 
**ticketId** | **Double** |  | 
**storeId** | **Double** |  |  [optional]
**attributeCode** | **String** |  |  [optional]
**value** | **String** |  |  [optional]



