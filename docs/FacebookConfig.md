
# FacebookConfig

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**appId** | **String** |  | 
**appToken** | **String** |  | 
**scopes** | **String** |  |  [optional]



