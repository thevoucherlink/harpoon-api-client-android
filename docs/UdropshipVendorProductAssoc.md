
# UdropshipVendorProductAssoc

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**isUdmulti** | **Double** |  |  [optional]
**id** | **Double** |  | 
**isAttribute** | **Double** |  | 
**productId** | **Double** |  | 
**vendorId** | **Double** |  | 
**udropshipVendor** | **Object** |  |  [optional]
**catalogProduct** | **Object** |  |  [optional]



