# CoreWebsiteApi

All URIs are relative to *https://apicdn.harpoonconnect.com/v1.1.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**coreWebsiteCount**](CoreWebsiteApi.md#coreWebsiteCount) | **GET** /CoreWebsites/count | Count instances of the model matched by where from the data source.
[**coreWebsiteCreate**](CoreWebsiteApi.md#coreWebsiteCreate) | **POST** /CoreWebsites | Create a new instance of the model and persist it into the data source.
[**coreWebsiteCreateChangeStreamGetCoreWebsitesChangeStream**](CoreWebsiteApi.md#coreWebsiteCreateChangeStreamGetCoreWebsitesChangeStream) | **GET** /CoreWebsites/change-stream | Create a change stream.
[**coreWebsiteCreateChangeStreamPostCoreWebsitesChangeStream**](CoreWebsiteApi.md#coreWebsiteCreateChangeStreamPostCoreWebsitesChangeStream) | **POST** /CoreWebsites/change-stream | Create a change stream.
[**coreWebsiteDeleteById**](CoreWebsiteApi.md#coreWebsiteDeleteById) | **DELETE** /CoreWebsites/{id} | Delete a model instance by {{id}} from the data source.
[**coreWebsiteExistsGetCoreWebsitesidExists**](CoreWebsiteApi.md#coreWebsiteExistsGetCoreWebsitesidExists) | **GET** /CoreWebsites/{id}/exists | Check whether a model instance exists in the data source.
[**coreWebsiteExistsHeadCoreWebsitesid**](CoreWebsiteApi.md#coreWebsiteExistsHeadCoreWebsitesid) | **HEAD** /CoreWebsites/{id} | Check whether a model instance exists in the data source.
[**coreWebsiteFind**](CoreWebsiteApi.md#coreWebsiteFind) | **GET** /CoreWebsites | Find all instances of the model matched by filter from the data source.
[**coreWebsiteFindById**](CoreWebsiteApi.md#coreWebsiteFindById) | **GET** /CoreWebsites/{id} | Find a model instance by {{id}} from the data source.
[**coreWebsiteFindOne**](CoreWebsiteApi.md#coreWebsiteFindOne) | **GET** /CoreWebsites/findOne | Find first instance of the model matched by filter from the data source.
[**coreWebsitePrototypeUpdateAttributesPatchCoreWebsitesid**](CoreWebsiteApi.md#coreWebsitePrototypeUpdateAttributesPatchCoreWebsitesid) | **PATCH** /CoreWebsites/{id} | Patch attributes for a model instance and persist it into the data source.
[**coreWebsitePrototypeUpdateAttributesPutCoreWebsitesid**](CoreWebsiteApi.md#coreWebsitePrototypeUpdateAttributesPutCoreWebsitesid) | **PUT** /CoreWebsites/{id} | Patch attributes for a model instance and persist it into the data source.
[**coreWebsiteReplaceById**](CoreWebsiteApi.md#coreWebsiteReplaceById) | **POST** /CoreWebsites/{id}/replace | Replace attributes for a model instance and persist it into the data source.
[**coreWebsiteReplaceOrCreate**](CoreWebsiteApi.md#coreWebsiteReplaceOrCreate) | **POST** /CoreWebsites/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
[**coreWebsiteUpdateAll**](CoreWebsiteApi.md#coreWebsiteUpdateAll) | **POST** /CoreWebsites/update | Update instances of the model matched by {{where}} from the data source.
[**coreWebsiteUpsertPatchCoreWebsites**](CoreWebsiteApi.md#coreWebsiteUpsertPatchCoreWebsites) | **PATCH** /CoreWebsites | Patch an existing model instance or insert a new one into the data source.
[**coreWebsiteUpsertPutCoreWebsites**](CoreWebsiteApi.md#coreWebsiteUpsertPutCoreWebsites) | **PUT** /CoreWebsites | Patch an existing model instance or insert a new one into the data source.
[**coreWebsiteUpsertWithWhere**](CoreWebsiteApi.md#coreWebsiteUpsertWithWhere) | **POST** /CoreWebsites/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.


<a name="coreWebsiteCount"></a>
# **coreWebsiteCount**
> InlineResponse2001 coreWebsiteCount(where)

Count instances of the model matched by where from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.CoreWebsiteApi;

CoreWebsiteApi apiInstance = new CoreWebsiteApi();
String where = "where_example"; // String | Criteria to match model instances
try {
    InlineResponse2001 result = apiInstance.coreWebsiteCount(where);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CoreWebsiteApi#coreWebsiteCount");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional]

### Return type

[**InlineResponse2001**](InlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="coreWebsiteCreate"></a>
# **coreWebsiteCreate**
> CoreWebsite coreWebsiteCreate(data)

Create a new instance of the model and persist it into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.CoreWebsiteApi;

CoreWebsiteApi apiInstance = new CoreWebsiteApi();
CoreWebsite data = new CoreWebsite(); // CoreWebsite | Model instance data
try {
    CoreWebsite result = apiInstance.coreWebsiteCreate(data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CoreWebsiteApi#coreWebsiteCreate");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**CoreWebsite**](CoreWebsite.md)| Model instance data | [optional]

### Return type

[**CoreWebsite**](CoreWebsite.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="coreWebsiteCreateChangeStreamGetCoreWebsitesChangeStream"></a>
# **coreWebsiteCreateChangeStreamGetCoreWebsitesChangeStream**
> File coreWebsiteCreateChangeStreamGetCoreWebsitesChangeStream(options)

Create a change stream.

### Example
```java
// Import classes:
//import com.harpoon.api.CoreWebsiteApi;

CoreWebsiteApi apiInstance = new CoreWebsiteApi();
String options = "options_example"; // String | 
try {
    File result = apiInstance.coreWebsiteCreateChangeStreamGetCoreWebsitesChangeStream(options);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CoreWebsiteApi#coreWebsiteCreateChangeStreamGetCoreWebsitesChangeStream");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **String**|  | [optional]

### Return type

[**File**](File.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="coreWebsiteCreateChangeStreamPostCoreWebsitesChangeStream"></a>
# **coreWebsiteCreateChangeStreamPostCoreWebsitesChangeStream**
> File coreWebsiteCreateChangeStreamPostCoreWebsitesChangeStream(options)

Create a change stream.

### Example
```java
// Import classes:
//import com.harpoon.api.CoreWebsiteApi;

CoreWebsiteApi apiInstance = new CoreWebsiteApi();
String options = "options_example"; // String | 
try {
    File result = apiInstance.coreWebsiteCreateChangeStreamPostCoreWebsitesChangeStream(options);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CoreWebsiteApi#coreWebsiteCreateChangeStreamPostCoreWebsitesChangeStream");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **String**|  | [optional]

### Return type

[**File**](File.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="coreWebsiteDeleteById"></a>
# **coreWebsiteDeleteById**
> Object coreWebsiteDeleteById(id)

Delete a model instance by {{id}} from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.CoreWebsiteApi;

CoreWebsiteApi apiInstance = new CoreWebsiteApi();
String id = "id_example"; // String | Model id
try {
    Object result = apiInstance.coreWebsiteDeleteById(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CoreWebsiteApi#coreWebsiteDeleteById");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |

### Return type

**Object**

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="coreWebsiteExistsGetCoreWebsitesidExists"></a>
# **coreWebsiteExistsGetCoreWebsitesidExists**
> InlineResponse2003 coreWebsiteExistsGetCoreWebsitesidExists(id)

Check whether a model instance exists in the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.CoreWebsiteApi;

CoreWebsiteApi apiInstance = new CoreWebsiteApi();
String id = "id_example"; // String | Model id
try {
    InlineResponse2003 result = apiInstance.coreWebsiteExistsGetCoreWebsitesidExists(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CoreWebsiteApi#coreWebsiteExistsGetCoreWebsitesidExists");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |

### Return type

[**InlineResponse2003**](InlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="coreWebsiteExistsHeadCoreWebsitesid"></a>
# **coreWebsiteExistsHeadCoreWebsitesid**
> InlineResponse2003 coreWebsiteExistsHeadCoreWebsitesid(id)

Check whether a model instance exists in the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.CoreWebsiteApi;

CoreWebsiteApi apiInstance = new CoreWebsiteApi();
String id = "id_example"; // String | Model id
try {
    InlineResponse2003 result = apiInstance.coreWebsiteExistsHeadCoreWebsitesid(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CoreWebsiteApi#coreWebsiteExistsHeadCoreWebsitesid");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |

### Return type

[**InlineResponse2003**](InlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="coreWebsiteFind"></a>
# **coreWebsiteFind**
> List&lt;CoreWebsite&gt; coreWebsiteFind(filter)

Find all instances of the model matched by filter from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.CoreWebsiteApi;

CoreWebsiteApi apiInstance = new CoreWebsiteApi();
String filter = "filter_example"; // String | Filter defining fields, where, include, order, offset, and limit
try {
    List<CoreWebsite> result = apiInstance.coreWebsiteFind(filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CoreWebsiteApi#coreWebsiteFind");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **String**| Filter defining fields, where, include, order, offset, and limit | [optional]

### Return type

[**List&lt;CoreWebsite&gt;**](CoreWebsite.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="coreWebsiteFindById"></a>
# **coreWebsiteFindById**
> CoreWebsite coreWebsiteFindById(id, filter)

Find a model instance by {{id}} from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.CoreWebsiteApi;

CoreWebsiteApi apiInstance = new CoreWebsiteApi();
String id = "id_example"; // String | Model id
String filter = "filter_example"; // String | Filter defining fields and include
try {
    CoreWebsite result = apiInstance.coreWebsiteFindById(id, filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CoreWebsiteApi#coreWebsiteFindById");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |
 **filter** | **String**| Filter defining fields and include | [optional]

### Return type

[**CoreWebsite**](CoreWebsite.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="coreWebsiteFindOne"></a>
# **coreWebsiteFindOne**
> CoreWebsite coreWebsiteFindOne(filter)

Find first instance of the model matched by filter from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.CoreWebsiteApi;

CoreWebsiteApi apiInstance = new CoreWebsiteApi();
String filter = "filter_example"; // String | Filter defining fields, where, include, order, offset, and limit
try {
    CoreWebsite result = apiInstance.coreWebsiteFindOne(filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CoreWebsiteApi#coreWebsiteFindOne");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **String**| Filter defining fields, where, include, order, offset, and limit | [optional]

### Return type

[**CoreWebsite**](CoreWebsite.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="coreWebsitePrototypeUpdateAttributesPatchCoreWebsitesid"></a>
# **coreWebsitePrototypeUpdateAttributesPatchCoreWebsitesid**
> CoreWebsite coreWebsitePrototypeUpdateAttributesPatchCoreWebsitesid(id, data)

Patch attributes for a model instance and persist it into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.CoreWebsiteApi;

CoreWebsiteApi apiInstance = new CoreWebsiteApi();
String id = "id_example"; // String | CoreWebsite id
CoreWebsite data = new CoreWebsite(); // CoreWebsite | An object of model property name/value pairs
try {
    CoreWebsite result = apiInstance.coreWebsitePrototypeUpdateAttributesPatchCoreWebsitesid(id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CoreWebsiteApi#coreWebsitePrototypeUpdateAttributesPatchCoreWebsitesid");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| CoreWebsite id |
 **data** | [**CoreWebsite**](CoreWebsite.md)| An object of model property name/value pairs | [optional]

### Return type

[**CoreWebsite**](CoreWebsite.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="coreWebsitePrototypeUpdateAttributesPutCoreWebsitesid"></a>
# **coreWebsitePrototypeUpdateAttributesPutCoreWebsitesid**
> CoreWebsite coreWebsitePrototypeUpdateAttributesPutCoreWebsitesid(id, data)

Patch attributes for a model instance and persist it into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.CoreWebsiteApi;

CoreWebsiteApi apiInstance = new CoreWebsiteApi();
String id = "id_example"; // String | CoreWebsite id
CoreWebsite data = new CoreWebsite(); // CoreWebsite | An object of model property name/value pairs
try {
    CoreWebsite result = apiInstance.coreWebsitePrototypeUpdateAttributesPutCoreWebsitesid(id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CoreWebsiteApi#coreWebsitePrototypeUpdateAttributesPutCoreWebsitesid");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| CoreWebsite id |
 **data** | [**CoreWebsite**](CoreWebsite.md)| An object of model property name/value pairs | [optional]

### Return type

[**CoreWebsite**](CoreWebsite.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="coreWebsiteReplaceById"></a>
# **coreWebsiteReplaceById**
> CoreWebsite coreWebsiteReplaceById(id, data)

Replace attributes for a model instance and persist it into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.CoreWebsiteApi;

CoreWebsiteApi apiInstance = new CoreWebsiteApi();
String id = "id_example"; // String | Model id
CoreWebsite data = new CoreWebsite(); // CoreWebsite | Model instance data
try {
    CoreWebsite result = apiInstance.coreWebsiteReplaceById(id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CoreWebsiteApi#coreWebsiteReplaceById");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |
 **data** | [**CoreWebsite**](CoreWebsite.md)| Model instance data | [optional]

### Return type

[**CoreWebsite**](CoreWebsite.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="coreWebsiteReplaceOrCreate"></a>
# **coreWebsiteReplaceOrCreate**
> CoreWebsite coreWebsiteReplaceOrCreate(data)

Replace an existing model instance or insert a new one into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.CoreWebsiteApi;

CoreWebsiteApi apiInstance = new CoreWebsiteApi();
CoreWebsite data = new CoreWebsite(); // CoreWebsite | Model instance data
try {
    CoreWebsite result = apiInstance.coreWebsiteReplaceOrCreate(data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CoreWebsiteApi#coreWebsiteReplaceOrCreate");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**CoreWebsite**](CoreWebsite.md)| Model instance data | [optional]

### Return type

[**CoreWebsite**](CoreWebsite.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="coreWebsiteUpdateAll"></a>
# **coreWebsiteUpdateAll**
> InlineResponse2002 coreWebsiteUpdateAll(where, data)

Update instances of the model matched by {{where}} from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.CoreWebsiteApi;

CoreWebsiteApi apiInstance = new CoreWebsiteApi();
String where = "where_example"; // String | Criteria to match model instances
CoreWebsite data = new CoreWebsite(); // CoreWebsite | An object of model property name/value pairs
try {
    InlineResponse2002 result = apiInstance.coreWebsiteUpdateAll(where, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CoreWebsiteApi#coreWebsiteUpdateAll");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional]
 **data** | [**CoreWebsite**](CoreWebsite.md)| An object of model property name/value pairs | [optional]

### Return type

[**InlineResponse2002**](InlineResponse2002.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="coreWebsiteUpsertPatchCoreWebsites"></a>
# **coreWebsiteUpsertPatchCoreWebsites**
> CoreWebsite coreWebsiteUpsertPatchCoreWebsites(data)

Patch an existing model instance or insert a new one into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.CoreWebsiteApi;

CoreWebsiteApi apiInstance = new CoreWebsiteApi();
CoreWebsite data = new CoreWebsite(); // CoreWebsite | Model instance data
try {
    CoreWebsite result = apiInstance.coreWebsiteUpsertPatchCoreWebsites(data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CoreWebsiteApi#coreWebsiteUpsertPatchCoreWebsites");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**CoreWebsite**](CoreWebsite.md)| Model instance data | [optional]

### Return type

[**CoreWebsite**](CoreWebsite.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="coreWebsiteUpsertPutCoreWebsites"></a>
# **coreWebsiteUpsertPutCoreWebsites**
> CoreWebsite coreWebsiteUpsertPutCoreWebsites(data)

Patch an existing model instance or insert a new one into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.CoreWebsiteApi;

CoreWebsiteApi apiInstance = new CoreWebsiteApi();
CoreWebsite data = new CoreWebsite(); // CoreWebsite | Model instance data
try {
    CoreWebsite result = apiInstance.coreWebsiteUpsertPutCoreWebsites(data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CoreWebsiteApi#coreWebsiteUpsertPutCoreWebsites");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**CoreWebsite**](CoreWebsite.md)| Model instance data | [optional]

### Return type

[**CoreWebsite**](CoreWebsite.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="coreWebsiteUpsertWithWhere"></a>
# **coreWebsiteUpsertWithWhere**
> CoreWebsite coreWebsiteUpsertWithWhere(where, data)

Update an existing model instance or insert a new one into the data source based on the where criteria.

### Example
```java
// Import classes:
//import com.harpoon.api.CoreWebsiteApi;

CoreWebsiteApi apiInstance = new CoreWebsiteApi();
String where = "where_example"; // String | Criteria to match model instances
CoreWebsite data = new CoreWebsite(); // CoreWebsite | An object of model property name/value pairs
try {
    CoreWebsite result = apiInstance.coreWebsiteUpsertWithWhere(where, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CoreWebsiteApi#coreWebsiteUpsertWithWhere");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional]
 **data** | [**CoreWebsite**](CoreWebsite.md)| An object of model property name/value pairs | [optional]

### Return type

[**CoreWebsite**](CoreWebsite.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

