
# CustomerActivity

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**customer** | [**Customer**](Customer.md) |  |  [optional]
**message** | **String** |  |  [optional]
**cover** | **String** |  |  [optional]
**related** | [**AnonymousModel8**](AnonymousModel8.md) |  |  [optional]
**link** | **String** |  |  [optional]
**actionCode** | **String** |  |  [optional]
**privacyCode** | **String** |  |  [optional]
**postedAt** | [**Date**](Date.md) |  |  [optional]
**id** | **Double** |  |  [optional]



