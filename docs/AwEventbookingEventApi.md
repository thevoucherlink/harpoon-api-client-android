# AwEventbookingEventApi

All URIs are relative to *https://apicdn.harpoonconnect.com/v1.1.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**awEventbookingEventCount**](AwEventbookingEventApi.md#awEventbookingEventCount) | **GET** /AwEventbookingEvents/count | Count instances of the model matched by where from the data source.
[**awEventbookingEventCreate**](AwEventbookingEventApi.md#awEventbookingEventCreate) | **POST** /AwEventbookingEvents | Create a new instance of the model and persist it into the data source.
[**awEventbookingEventCreateChangeStreamGetAwEventbookingEventsChangeStream**](AwEventbookingEventApi.md#awEventbookingEventCreateChangeStreamGetAwEventbookingEventsChangeStream) | **GET** /AwEventbookingEvents/change-stream | Create a change stream.
[**awEventbookingEventCreateChangeStreamPostAwEventbookingEventsChangeStream**](AwEventbookingEventApi.md#awEventbookingEventCreateChangeStreamPostAwEventbookingEventsChangeStream) | **POST** /AwEventbookingEvents/change-stream | Create a change stream.
[**awEventbookingEventDeleteById**](AwEventbookingEventApi.md#awEventbookingEventDeleteById) | **DELETE** /AwEventbookingEvents/{id} | Delete a model instance by {{id}} from the data source.
[**awEventbookingEventExistsGetAwEventbookingEventsidExists**](AwEventbookingEventApi.md#awEventbookingEventExistsGetAwEventbookingEventsidExists) | **GET** /AwEventbookingEvents/{id}/exists | Check whether a model instance exists in the data source.
[**awEventbookingEventExistsHeadAwEventbookingEventsid**](AwEventbookingEventApi.md#awEventbookingEventExistsHeadAwEventbookingEventsid) | **HEAD** /AwEventbookingEvents/{id} | Check whether a model instance exists in the data source.
[**awEventbookingEventFind**](AwEventbookingEventApi.md#awEventbookingEventFind) | **GET** /AwEventbookingEvents | Find all instances of the model matched by filter from the data source.
[**awEventbookingEventFindById**](AwEventbookingEventApi.md#awEventbookingEventFindById) | **GET** /AwEventbookingEvents/{id} | Find a model instance by {{id}} from the data source.
[**awEventbookingEventFindOne**](AwEventbookingEventApi.md#awEventbookingEventFindOne) | **GET** /AwEventbookingEvents/findOne | Find first instance of the model matched by filter from the data source.
[**awEventbookingEventPrototypeCountAttributes**](AwEventbookingEventApi.md#awEventbookingEventPrototypeCountAttributes) | **GET** /AwEventbookingEvents/{id}/attributes/count | Counts attributes of AwEventbookingEvent.
[**awEventbookingEventPrototypeCountPurchasedTickets**](AwEventbookingEventApi.md#awEventbookingEventPrototypeCountPurchasedTickets) | **GET** /AwEventbookingEvents/{id}/purchasedTickets/count | Counts purchasedTickets of AwEventbookingEvent.
[**awEventbookingEventPrototypeCountTickets**](AwEventbookingEventApi.md#awEventbookingEventPrototypeCountTickets) | **GET** /AwEventbookingEvents/{id}/tickets/count | Counts tickets of AwEventbookingEvent.
[**awEventbookingEventPrototypeCreateAttributes**](AwEventbookingEventApi.md#awEventbookingEventPrototypeCreateAttributes) | **POST** /AwEventbookingEvents/{id}/attributes | Creates a new instance in attributes of this model.
[**awEventbookingEventPrototypeCreatePurchasedTickets**](AwEventbookingEventApi.md#awEventbookingEventPrototypeCreatePurchasedTickets) | **POST** /AwEventbookingEvents/{id}/purchasedTickets | Creates a new instance in purchasedTickets of this model.
[**awEventbookingEventPrototypeCreateTickets**](AwEventbookingEventApi.md#awEventbookingEventPrototypeCreateTickets) | **POST** /AwEventbookingEvents/{id}/tickets | Creates a new instance in tickets of this model.
[**awEventbookingEventPrototypeDeleteAttributes**](AwEventbookingEventApi.md#awEventbookingEventPrototypeDeleteAttributes) | **DELETE** /AwEventbookingEvents/{id}/attributes | Deletes all attributes of this model.
[**awEventbookingEventPrototypeDeletePurchasedTickets**](AwEventbookingEventApi.md#awEventbookingEventPrototypeDeletePurchasedTickets) | **DELETE** /AwEventbookingEvents/{id}/purchasedTickets | Deletes all purchasedTickets of this model.
[**awEventbookingEventPrototypeDeleteTickets**](AwEventbookingEventApi.md#awEventbookingEventPrototypeDeleteTickets) | **DELETE** /AwEventbookingEvents/{id}/tickets | Deletes all tickets of this model.
[**awEventbookingEventPrototypeDestroyByIdAttributes**](AwEventbookingEventApi.md#awEventbookingEventPrototypeDestroyByIdAttributes) | **DELETE** /AwEventbookingEvents/{id}/attributes/{fk} | Delete a related item by id for attributes.
[**awEventbookingEventPrototypeDestroyByIdPurchasedTickets**](AwEventbookingEventApi.md#awEventbookingEventPrototypeDestroyByIdPurchasedTickets) | **DELETE** /AwEventbookingEvents/{id}/purchasedTickets/{fk} | Delete a related item by id for purchasedTickets.
[**awEventbookingEventPrototypeDestroyByIdTickets**](AwEventbookingEventApi.md#awEventbookingEventPrototypeDestroyByIdTickets) | **DELETE** /AwEventbookingEvents/{id}/tickets/{fk} | Delete a related item by id for tickets.
[**awEventbookingEventPrototypeExistsPurchasedTickets**](AwEventbookingEventApi.md#awEventbookingEventPrototypeExistsPurchasedTickets) | **HEAD** /AwEventbookingEvents/{id}/purchasedTickets/rel/{fk} | Check the existence of purchasedTickets relation to an item by id.
[**awEventbookingEventPrototypeFindByIdAttributes**](AwEventbookingEventApi.md#awEventbookingEventPrototypeFindByIdAttributes) | **GET** /AwEventbookingEvents/{id}/attributes/{fk} | Find a related item by id for attributes.
[**awEventbookingEventPrototypeFindByIdPurchasedTickets**](AwEventbookingEventApi.md#awEventbookingEventPrototypeFindByIdPurchasedTickets) | **GET** /AwEventbookingEvents/{id}/purchasedTickets/{fk} | Find a related item by id for purchasedTickets.
[**awEventbookingEventPrototypeFindByIdTickets**](AwEventbookingEventApi.md#awEventbookingEventPrototypeFindByIdTickets) | **GET** /AwEventbookingEvents/{id}/tickets/{fk} | Find a related item by id for tickets.
[**awEventbookingEventPrototypeGetAttributes**](AwEventbookingEventApi.md#awEventbookingEventPrototypeGetAttributes) | **GET** /AwEventbookingEvents/{id}/attributes | Queries attributes of AwEventbookingEvent.
[**awEventbookingEventPrototypeGetPurchasedTickets**](AwEventbookingEventApi.md#awEventbookingEventPrototypeGetPurchasedTickets) | **GET** /AwEventbookingEvents/{id}/purchasedTickets | Queries purchasedTickets of AwEventbookingEvent.
[**awEventbookingEventPrototypeGetTickets**](AwEventbookingEventApi.md#awEventbookingEventPrototypeGetTickets) | **GET** /AwEventbookingEvents/{id}/tickets | Queries tickets of AwEventbookingEvent.
[**awEventbookingEventPrototypeLinkPurchasedTickets**](AwEventbookingEventApi.md#awEventbookingEventPrototypeLinkPurchasedTickets) | **PUT** /AwEventbookingEvents/{id}/purchasedTickets/rel/{fk} | Add a related item by id for purchasedTickets.
[**awEventbookingEventPrototypeUnlinkPurchasedTickets**](AwEventbookingEventApi.md#awEventbookingEventPrototypeUnlinkPurchasedTickets) | **DELETE** /AwEventbookingEvents/{id}/purchasedTickets/rel/{fk} | Remove the purchasedTickets relation to an item by id.
[**awEventbookingEventPrototypeUpdateAttributesPatchAwEventbookingEventsid**](AwEventbookingEventApi.md#awEventbookingEventPrototypeUpdateAttributesPatchAwEventbookingEventsid) | **PATCH** /AwEventbookingEvents/{id} | Patch attributes for a model instance and persist it into the data source.
[**awEventbookingEventPrototypeUpdateAttributesPutAwEventbookingEventsid**](AwEventbookingEventApi.md#awEventbookingEventPrototypeUpdateAttributesPutAwEventbookingEventsid) | **PUT** /AwEventbookingEvents/{id} | Patch attributes for a model instance and persist it into the data source.
[**awEventbookingEventPrototypeUpdateByIdAttributes**](AwEventbookingEventApi.md#awEventbookingEventPrototypeUpdateByIdAttributes) | **PUT** /AwEventbookingEvents/{id}/attributes/{fk} | Update a related item by id for attributes.
[**awEventbookingEventPrototypeUpdateByIdPurchasedTickets**](AwEventbookingEventApi.md#awEventbookingEventPrototypeUpdateByIdPurchasedTickets) | **PUT** /AwEventbookingEvents/{id}/purchasedTickets/{fk} | Update a related item by id for purchasedTickets.
[**awEventbookingEventPrototypeUpdateByIdTickets**](AwEventbookingEventApi.md#awEventbookingEventPrototypeUpdateByIdTickets) | **PUT** /AwEventbookingEvents/{id}/tickets/{fk} | Update a related item by id for tickets.
[**awEventbookingEventReplaceById**](AwEventbookingEventApi.md#awEventbookingEventReplaceById) | **POST** /AwEventbookingEvents/{id}/replace | Replace attributes for a model instance and persist it into the data source.
[**awEventbookingEventReplaceOrCreate**](AwEventbookingEventApi.md#awEventbookingEventReplaceOrCreate) | **POST** /AwEventbookingEvents/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
[**awEventbookingEventUpdateAll**](AwEventbookingEventApi.md#awEventbookingEventUpdateAll) | **POST** /AwEventbookingEvents/update | Update instances of the model matched by {{where}} from the data source.
[**awEventbookingEventUpsertPatchAwEventbookingEvents**](AwEventbookingEventApi.md#awEventbookingEventUpsertPatchAwEventbookingEvents) | **PATCH** /AwEventbookingEvents | Patch an existing model instance or insert a new one into the data source.
[**awEventbookingEventUpsertPutAwEventbookingEvents**](AwEventbookingEventApi.md#awEventbookingEventUpsertPutAwEventbookingEvents) | **PUT** /AwEventbookingEvents | Patch an existing model instance or insert a new one into the data source.
[**awEventbookingEventUpsertWithWhere**](AwEventbookingEventApi.md#awEventbookingEventUpsertWithWhere) | **POST** /AwEventbookingEvents/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.


<a name="awEventbookingEventCount"></a>
# **awEventbookingEventCount**
> InlineResponse2001 awEventbookingEventCount(where)

Count instances of the model matched by where from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.AwEventbookingEventApi;

AwEventbookingEventApi apiInstance = new AwEventbookingEventApi();
String where = "where_example"; // String | Criteria to match model instances
try {
    InlineResponse2001 result = apiInstance.awEventbookingEventCount(where);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AwEventbookingEventApi#awEventbookingEventCount");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional]

### Return type

[**InlineResponse2001**](InlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingEventCreate"></a>
# **awEventbookingEventCreate**
> AwEventbookingEvent awEventbookingEventCreate(data)

Create a new instance of the model and persist it into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.AwEventbookingEventApi;

AwEventbookingEventApi apiInstance = new AwEventbookingEventApi();
AwEventbookingEvent data = new AwEventbookingEvent(); // AwEventbookingEvent | Model instance data
try {
    AwEventbookingEvent result = apiInstance.awEventbookingEventCreate(data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AwEventbookingEventApi#awEventbookingEventCreate");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**AwEventbookingEvent**](AwEventbookingEvent.md)| Model instance data | [optional]

### Return type

[**AwEventbookingEvent**](AwEventbookingEvent.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingEventCreateChangeStreamGetAwEventbookingEventsChangeStream"></a>
# **awEventbookingEventCreateChangeStreamGetAwEventbookingEventsChangeStream**
> File awEventbookingEventCreateChangeStreamGetAwEventbookingEventsChangeStream(options)

Create a change stream.

### Example
```java
// Import classes:
//import com.harpoon.api.AwEventbookingEventApi;

AwEventbookingEventApi apiInstance = new AwEventbookingEventApi();
String options = "options_example"; // String | 
try {
    File result = apiInstance.awEventbookingEventCreateChangeStreamGetAwEventbookingEventsChangeStream(options);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AwEventbookingEventApi#awEventbookingEventCreateChangeStreamGetAwEventbookingEventsChangeStream");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **String**|  | [optional]

### Return type

[**File**](File.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingEventCreateChangeStreamPostAwEventbookingEventsChangeStream"></a>
# **awEventbookingEventCreateChangeStreamPostAwEventbookingEventsChangeStream**
> File awEventbookingEventCreateChangeStreamPostAwEventbookingEventsChangeStream(options)

Create a change stream.

### Example
```java
// Import classes:
//import com.harpoon.api.AwEventbookingEventApi;

AwEventbookingEventApi apiInstance = new AwEventbookingEventApi();
String options = "options_example"; // String | 
try {
    File result = apiInstance.awEventbookingEventCreateChangeStreamPostAwEventbookingEventsChangeStream(options);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AwEventbookingEventApi#awEventbookingEventCreateChangeStreamPostAwEventbookingEventsChangeStream");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **String**|  | [optional]

### Return type

[**File**](File.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingEventDeleteById"></a>
# **awEventbookingEventDeleteById**
> Object awEventbookingEventDeleteById(id)

Delete a model instance by {{id}} from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.AwEventbookingEventApi;

AwEventbookingEventApi apiInstance = new AwEventbookingEventApi();
String id = "id_example"; // String | Model id
try {
    Object result = apiInstance.awEventbookingEventDeleteById(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AwEventbookingEventApi#awEventbookingEventDeleteById");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |

### Return type

**Object**

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingEventExistsGetAwEventbookingEventsidExists"></a>
# **awEventbookingEventExistsGetAwEventbookingEventsidExists**
> InlineResponse2003 awEventbookingEventExistsGetAwEventbookingEventsidExists(id)

Check whether a model instance exists in the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.AwEventbookingEventApi;

AwEventbookingEventApi apiInstance = new AwEventbookingEventApi();
String id = "id_example"; // String | Model id
try {
    InlineResponse2003 result = apiInstance.awEventbookingEventExistsGetAwEventbookingEventsidExists(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AwEventbookingEventApi#awEventbookingEventExistsGetAwEventbookingEventsidExists");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |

### Return type

[**InlineResponse2003**](InlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingEventExistsHeadAwEventbookingEventsid"></a>
# **awEventbookingEventExistsHeadAwEventbookingEventsid**
> InlineResponse2003 awEventbookingEventExistsHeadAwEventbookingEventsid(id)

Check whether a model instance exists in the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.AwEventbookingEventApi;

AwEventbookingEventApi apiInstance = new AwEventbookingEventApi();
String id = "id_example"; // String | Model id
try {
    InlineResponse2003 result = apiInstance.awEventbookingEventExistsHeadAwEventbookingEventsid(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AwEventbookingEventApi#awEventbookingEventExistsHeadAwEventbookingEventsid");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |

### Return type

[**InlineResponse2003**](InlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingEventFind"></a>
# **awEventbookingEventFind**
> List&lt;AwEventbookingEvent&gt; awEventbookingEventFind(filter)

Find all instances of the model matched by filter from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.AwEventbookingEventApi;

AwEventbookingEventApi apiInstance = new AwEventbookingEventApi();
String filter = "filter_example"; // String | Filter defining fields, where, include, order, offset, and limit
try {
    List<AwEventbookingEvent> result = apiInstance.awEventbookingEventFind(filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AwEventbookingEventApi#awEventbookingEventFind");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **String**| Filter defining fields, where, include, order, offset, and limit | [optional]

### Return type

[**List&lt;AwEventbookingEvent&gt;**](AwEventbookingEvent.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingEventFindById"></a>
# **awEventbookingEventFindById**
> AwEventbookingEvent awEventbookingEventFindById(id, filter)

Find a model instance by {{id}} from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.AwEventbookingEventApi;

AwEventbookingEventApi apiInstance = new AwEventbookingEventApi();
String id = "id_example"; // String | Model id
String filter = "filter_example"; // String | Filter defining fields and include
try {
    AwEventbookingEvent result = apiInstance.awEventbookingEventFindById(id, filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AwEventbookingEventApi#awEventbookingEventFindById");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |
 **filter** | **String**| Filter defining fields and include | [optional]

### Return type

[**AwEventbookingEvent**](AwEventbookingEvent.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingEventFindOne"></a>
# **awEventbookingEventFindOne**
> AwEventbookingEvent awEventbookingEventFindOne(filter)

Find first instance of the model matched by filter from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.AwEventbookingEventApi;

AwEventbookingEventApi apiInstance = new AwEventbookingEventApi();
String filter = "filter_example"; // String | Filter defining fields, where, include, order, offset, and limit
try {
    AwEventbookingEvent result = apiInstance.awEventbookingEventFindOne(filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AwEventbookingEventApi#awEventbookingEventFindOne");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **String**| Filter defining fields, where, include, order, offset, and limit | [optional]

### Return type

[**AwEventbookingEvent**](AwEventbookingEvent.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingEventPrototypeCountAttributes"></a>
# **awEventbookingEventPrototypeCountAttributes**
> InlineResponse2001 awEventbookingEventPrototypeCountAttributes(id, where)

Counts attributes of AwEventbookingEvent.

### Example
```java
// Import classes:
//import com.harpoon.api.AwEventbookingEventApi;

AwEventbookingEventApi apiInstance = new AwEventbookingEventApi();
String id = "id_example"; // String | AwEventbookingEvent id
String where = "where_example"; // String | Criteria to match model instances
try {
    InlineResponse2001 result = apiInstance.awEventbookingEventPrototypeCountAttributes(id, where);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AwEventbookingEventApi#awEventbookingEventPrototypeCountAttributes");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| AwEventbookingEvent id |
 **where** | **String**| Criteria to match model instances | [optional]

### Return type

[**InlineResponse2001**](InlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingEventPrototypeCountPurchasedTickets"></a>
# **awEventbookingEventPrototypeCountPurchasedTickets**
> InlineResponse2001 awEventbookingEventPrototypeCountPurchasedTickets(id, where)

Counts purchasedTickets of AwEventbookingEvent.

### Example
```java
// Import classes:
//import com.harpoon.api.AwEventbookingEventApi;

AwEventbookingEventApi apiInstance = new AwEventbookingEventApi();
String id = "id_example"; // String | AwEventbookingEvent id
String where = "where_example"; // String | Criteria to match model instances
try {
    InlineResponse2001 result = apiInstance.awEventbookingEventPrototypeCountPurchasedTickets(id, where);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AwEventbookingEventApi#awEventbookingEventPrototypeCountPurchasedTickets");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| AwEventbookingEvent id |
 **where** | **String**| Criteria to match model instances | [optional]

### Return type

[**InlineResponse2001**](InlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingEventPrototypeCountTickets"></a>
# **awEventbookingEventPrototypeCountTickets**
> InlineResponse2001 awEventbookingEventPrototypeCountTickets(id, where)

Counts tickets of AwEventbookingEvent.

### Example
```java
// Import classes:
//import com.harpoon.api.AwEventbookingEventApi;

AwEventbookingEventApi apiInstance = new AwEventbookingEventApi();
String id = "id_example"; // String | AwEventbookingEvent id
String where = "where_example"; // String | Criteria to match model instances
try {
    InlineResponse2001 result = apiInstance.awEventbookingEventPrototypeCountTickets(id, where);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AwEventbookingEventApi#awEventbookingEventPrototypeCountTickets");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| AwEventbookingEvent id |
 **where** | **String**| Criteria to match model instances | [optional]

### Return type

[**InlineResponse2001**](InlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingEventPrototypeCreateAttributes"></a>
# **awEventbookingEventPrototypeCreateAttributes**
> AwEventbookingEventAttribute awEventbookingEventPrototypeCreateAttributes(id, data)

Creates a new instance in attributes of this model.

### Example
```java
// Import classes:
//import com.harpoon.api.AwEventbookingEventApi;

AwEventbookingEventApi apiInstance = new AwEventbookingEventApi();
String id = "id_example"; // String | AwEventbookingEvent id
AwEventbookingEventAttribute data = new AwEventbookingEventAttribute(); // AwEventbookingEventAttribute | 
try {
    AwEventbookingEventAttribute result = apiInstance.awEventbookingEventPrototypeCreateAttributes(id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AwEventbookingEventApi#awEventbookingEventPrototypeCreateAttributes");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| AwEventbookingEvent id |
 **data** | [**AwEventbookingEventAttribute**](AwEventbookingEventAttribute.md)|  | [optional]

### Return type

[**AwEventbookingEventAttribute**](AwEventbookingEventAttribute.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingEventPrototypeCreatePurchasedTickets"></a>
# **awEventbookingEventPrototypeCreatePurchasedTickets**
> AwEventbookingTicket awEventbookingEventPrototypeCreatePurchasedTickets(id, data)

Creates a new instance in purchasedTickets of this model.

### Example
```java
// Import classes:
//import com.harpoon.api.AwEventbookingEventApi;

AwEventbookingEventApi apiInstance = new AwEventbookingEventApi();
String id = "id_example"; // String | AwEventbookingEvent id
AwEventbookingTicket data = new AwEventbookingTicket(); // AwEventbookingTicket | 
try {
    AwEventbookingTicket result = apiInstance.awEventbookingEventPrototypeCreatePurchasedTickets(id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AwEventbookingEventApi#awEventbookingEventPrototypeCreatePurchasedTickets");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| AwEventbookingEvent id |
 **data** | [**AwEventbookingTicket**](AwEventbookingTicket.md)|  | [optional]

### Return type

[**AwEventbookingTicket**](AwEventbookingTicket.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingEventPrototypeCreateTickets"></a>
# **awEventbookingEventPrototypeCreateTickets**
> AwEventbookingEventTicket awEventbookingEventPrototypeCreateTickets(id, data)

Creates a new instance in tickets of this model.

### Example
```java
// Import classes:
//import com.harpoon.api.AwEventbookingEventApi;

AwEventbookingEventApi apiInstance = new AwEventbookingEventApi();
String id = "id_example"; // String | AwEventbookingEvent id
AwEventbookingEventTicket data = new AwEventbookingEventTicket(); // AwEventbookingEventTicket | 
try {
    AwEventbookingEventTicket result = apiInstance.awEventbookingEventPrototypeCreateTickets(id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AwEventbookingEventApi#awEventbookingEventPrototypeCreateTickets");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| AwEventbookingEvent id |
 **data** | [**AwEventbookingEventTicket**](AwEventbookingEventTicket.md)|  | [optional]

### Return type

[**AwEventbookingEventTicket**](AwEventbookingEventTicket.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingEventPrototypeDeleteAttributes"></a>
# **awEventbookingEventPrototypeDeleteAttributes**
> awEventbookingEventPrototypeDeleteAttributes(id)

Deletes all attributes of this model.

### Example
```java
// Import classes:
//import com.harpoon.api.AwEventbookingEventApi;

AwEventbookingEventApi apiInstance = new AwEventbookingEventApi();
String id = "id_example"; // String | AwEventbookingEvent id
try {
    apiInstance.awEventbookingEventPrototypeDeleteAttributes(id);
} catch (ApiException e) {
    System.err.println("Exception when calling AwEventbookingEventApi#awEventbookingEventPrototypeDeleteAttributes");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| AwEventbookingEvent id |

### Return type

null (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingEventPrototypeDeletePurchasedTickets"></a>
# **awEventbookingEventPrototypeDeletePurchasedTickets**
> awEventbookingEventPrototypeDeletePurchasedTickets(id)

Deletes all purchasedTickets of this model.

### Example
```java
// Import classes:
//import com.harpoon.api.AwEventbookingEventApi;

AwEventbookingEventApi apiInstance = new AwEventbookingEventApi();
String id = "id_example"; // String | AwEventbookingEvent id
try {
    apiInstance.awEventbookingEventPrototypeDeletePurchasedTickets(id);
} catch (ApiException e) {
    System.err.println("Exception when calling AwEventbookingEventApi#awEventbookingEventPrototypeDeletePurchasedTickets");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| AwEventbookingEvent id |

### Return type

null (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingEventPrototypeDeleteTickets"></a>
# **awEventbookingEventPrototypeDeleteTickets**
> awEventbookingEventPrototypeDeleteTickets(id)

Deletes all tickets of this model.

### Example
```java
// Import classes:
//import com.harpoon.api.AwEventbookingEventApi;

AwEventbookingEventApi apiInstance = new AwEventbookingEventApi();
String id = "id_example"; // String | AwEventbookingEvent id
try {
    apiInstance.awEventbookingEventPrototypeDeleteTickets(id);
} catch (ApiException e) {
    System.err.println("Exception when calling AwEventbookingEventApi#awEventbookingEventPrototypeDeleteTickets");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| AwEventbookingEvent id |

### Return type

null (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingEventPrototypeDestroyByIdAttributes"></a>
# **awEventbookingEventPrototypeDestroyByIdAttributes**
> awEventbookingEventPrototypeDestroyByIdAttributes(fk, id)

Delete a related item by id for attributes.

### Example
```java
// Import classes:
//import com.harpoon.api.AwEventbookingEventApi;

AwEventbookingEventApi apiInstance = new AwEventbookingEventApi();
String fk = "fk_example"; // String | Foreign key for attributes
String id = "id_example"; // String | AwEventbookingEvent id
try {
    apiInstance.awEventbookingEventPrototypeDestroyByIdAttributes(fk, id);
} catch (ApiException e) {
    System.err.println("Exception when calling AwEventbookingEventApi#awEventbookingEventPrototypeDestroyByIdAttributes");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for attributes |
 **id** | **String**| AwEventbookingEvent id |

### Return type

null (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingEventPrototypeDestroyByIdPurchasedTickets"></a>
# **awEventbookingEventPrototypeDestroyByIdPurchasedTickets**
> awEventbookingEventPrototypeDestroyByIdPurchasedTickets(fk, id)

Delete a related item by id for purchasedTickets.

### Example
```java
// Import classes:
//import com.harpoon.api.AwEventbookingEventApi;

AwEventbookingEventApi apiInstance = new AwEventbookingEventApi();
String fk = "fk_example"; // String | Foreign key for purchasedTickets
String id = "id_example"; // String | AwEventbookingEvent id
try {
    apiInstance.awEventbookingEventPrototypeDestroyByIdPurchasedTickets(fk, id);
} catch (ApiException e) {
    System.err.println("Exception when calling AwEventbookingEventApi#awEventbookingEventPrototypeDestroyByIdPurchasedTickets");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for purchasedTickets |
 **id** | **String**| AwEventbookingEvent id |

### Return type

null (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingEventPrototypeDestroyByIdTickets"></a>
# **awEventbookingEventPrototypeDestroyByIdTickets**
> awEventbookingEventPrototypeDestroyByIdTickets(fk, id)

Delete a related item by id for tickets.

### Example
```java
// Import classes:
//import com.harpoon.api.AwEventbookingEventApi;

AwEventbookingEventApi apiInstance = new AwEventbookingEventApi();
String fk = "fk_example"; // String | Foreign key for tickets
String id = "id_example"; // String | AwEventbookingEvent id
try {
    apiInstance.awEventbookingEventPrototypeDestroyByIdTickets(fk, id);
} catch (ApiException e) {
    System.err.println("Exception when calling AwEventbookingEventApi#awEventbookingEventPrototypeDestroyByIdTickets");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for tickets |
 **id** | **String**| AwEventbookingEvent id |

### Return type

null (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingEventPrototypeExistsPurchasedTickets"></a>
# **awEventbookingEventPrototypeExistsPurchasedTickets**
> Boolean awEventbookingEventPrototypeExistsPurchasedTickets(fk, id)

Check the existence of purchasedTickets relation to an item by id.

### Example
```java
// Import classes:
//import com.harpoon.api.AwEventbookingEventApi;

AwEventbookingEventApi apiInstance = new AwEventbookingEventApi();
String fk = "fk_example"; // String | Foreign key for purchasedTickets
String id = "id_example"; // String | AwEventbookingEvent id
try {
    Boolean result = apiInstance.awEventbookingEventPrototypeExistsPurchasedTickets(fk, id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AwEventbookingEventApi#awEventbookingEventPrototypeExistsPurchasedTickets");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for purchasedTickets |
 **id** | **String**| AwEventbookingEvent id |

### Return type

**Boolean**

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingEventPrototypeFindByIdAttributes"></a>
# **awEventbookingEventPrototypeFindByIdAttributes**
> AwEventbookingEventAttribute awEventbookingEventPrototypeFindByIdAttributes(fk, id)

Find a related item by id for attributes.

### Example
```java
// Import classes:
//import com.harpoon.api.AwEventbookingEventApi;

AwEventbookingEventApi apiInstance = new AwEventbookingEventApi();
String fk = "fk_example"; // String | Foreign key for attributes
String id = "id_example"; // String | AwEventbookingEvent id
try {
    AwEventbookingEventAttribute result = apiInstance.awEventbookingEventPrototypeFindByIdAttributes(fk, id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AwEventbookingEventApi#awEventbookingEventPrototypeFindByIdAttributes");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for attributes |
 **id** | **String**| AwEventbookingEvent id |

### Return type

[**AwEventbookingEventAttribute**](AwEventbookingEventAttribute.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingEventPrototypeFindByIdPurchasedTickets"></a>
# **awEventbookingEventPrototypeFindByIdPurchasedTickets**
> AwEventbookingTicket awEventbookingEventPrototypeFindByIdPurchasedTickets(fk, id)

Find a related item by id for purchasedTickets.

### Example
```java
// Import classes:
//import com.harpoon.api.AwEventbookingEventApi;

AwEventbookingEventApi apiInstance = new AwEventbookingEventApi();
String fk = "fk_example"; // String | Foreign key for purchasedTickets
String id = "id_example"; // String | AwEventbookingEvent id
try {
    AwEventbookingTicket result = apiInstance.awEventbookingEventPrototypeFindByIdPurchasedTickets(fk, id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AwEventbookingEventApi#awEventbookingEventPrototypeFindByIdPurchasedTickets");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for purchasedTickets |
 **id** | **String**| AwEventbookingEvent id |

### Return type

[**AwEventbookingTicket**](AwEventbookingTicket.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingEventPrototypeFindByIdTickets"></a>
# **awEventbookingEventPrototypeFindByIdTickets**
> AwEventbookingEventTicket awEventbookingEventPrototypeFindByIdTickets(fk, id)

Find a related item by id for tickets.

### Example
```java
// Import classes:
//import com.harpoon.api.AwEventbookingEventApi;

AwEventbookingEventApi apiInstance = new AwEventbookingEventApi();
String fk = "fk_example"; // String | Foreign key for tickets
String id = "id_example"; // String | AwEventbookingEvent id
try {
    AwEventbookingEventTicket result = apiInstance.awEventbookingEventPrototypeFindByIdTickets(fk, id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AwEventbookingEventApi#awEventbookingEventPrototypeFindByIdTickets");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for tickets |
 **id** | **String**| AwEventbookingEvent id |

### Return type

[**AwEventbookingEventTicket**](AwEventbookingEventTicket.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingEventPrototypeGetAttributes"></a>
# **awEventbookingEventPrototypeGetAttributes**
> List&lt;AwEventbookingEventAttribute&gt; awEventbookingEventPrototypeGetAttributes(id, filter)

Queries attributes of AwEventbookingEvent.

### Example
```java
// Import classes:
//import com.harpoon.api.AwEventbookingEventApi;

AwEventbookingEventApi apiInstance = new AwEventbookingEventApi();
String id = "id_example"; // String | AwEventbookingEvent id
String filter = "filter_example"; // String | 
try {
    List<AwEventbookingEventAttribute> result = apiInstance.awEventbookingEventPrototypeGetAttributes(id, filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AwEventbookingEventApi#awEventbookingEventPrototypeGetAttributes");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| AwEventbookingEvent id |
 **filter** | **String**|  | [optional]

### Return type

[**List&lt;AwEventbookingEventAttribute&gt;**](AwEventbookingEventAttribute.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingEventPrototypeGetPurchasedTickets"></a>
# **awEventbookingEventPrototypeGetPurchasedTickets**
> List&lt;AwEventbookingTicket&gt; awEventbookingEventPrototypeGetPurchasedTickets(id, filter)

Queries purchasedTickets of AwEventbookingEvent.

### Example
```java
// Import classes:
//import com.harpoon.api.AwEventbookingEventApi;

AwEventbookingEventApi apiInstance = new AwEventbookingEventApi();
String id = "id_example"; // String | AwEventbookingEvent id
String filter = "filter_example"; // String | 
try {
    List<AwEventbookingTicket> result = apiInstance.awEventbookingEventPrototypeGetPurchasedTickets(id, filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AwEventbookingEventApi#awEventbookingEventPrototypeGetPurchasedTickets");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| AwEventbookingEvent id |
 **filter** | **String**|  | [optional]

### Return type

[**List&lt;AwEventbookingTicket&gt;**](AwEventbookingTicket.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingEventPrototypeGetTickets"></a>
# **awEventbookingEventPrototypeGetTickets**
> List&lt;AwEventbookingEventTicket&gt; awEventbookingEventPrototypeGetTickets(id, filter)

Queries tickets of AwEventbookingEvent.

### Example
```java
// Import classes:
//import com.harpoon.api.AwEventbookingEventApi;

AwEventbookingEventApi apiInstance = new AwEventbookingEventApi();
String id = "id_example"; // String | AwEventbookingEvent id
String filter = "filter_example"; // String | 
try {
    List<AwEventbookingEventTicket> result = apiInstance.awEventbookingEventPrototypeGetTickets(id, filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AwEventbookingEventApi#awEventbookingEventPrototypeGetTickets");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| AwEventbookingEvent id |
 **filter** | **String**|  | [optional]

### Return type

[**List&lt;AwEventbookingEventTicket&gt;**](AwEventbookingEventTicket.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingEventPrototypeLinkPurchasedTickets"></a>
# **awEventbookingEventPrototypeLinkPurchasedTickets**
> AwEventbookingEventTicket awEventbookingEventPrototypeLinkPurchasedTickets(fk, id, data)

Add a related item by id for purchasedTickets.

### Example
```java
// Import classes:
//import com.harpoon.api.AwEventbookingEventApi;

AwEventbookingEventApi apiInstance = new AwEventbookingEventApi();
String fk = "fk_example"; // String | Foreign key for purchasedTickets
String id = "id_example"; // String | AwEventbookingEvent id
AwEventbookingEventTicket data = new AwEventbookingEventTicket(); // AwEventbookingEventTicket | 
try {
    AwEventbookingEventTicket result = apiInstance.awEventbookingEventPrototypeLinkPurchasedTickets(fk, id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AwEventbookingEventApi#awEventbookingEventPrototypeLinkPurchasedTickets");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for purchasedTickets |
 **id** | **String**| AwEventbookingEvent id |
 **data** | [**AwEventbookingEventTicket**](AwEventbookingEventTicket.md)|  | [optional]

### Return type

[**AwEventbookingEventTicket**](AwEventbookingEventTicket.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingEventPrototypeUnlinkPurchasedTickets"></a>
# **awEventbookingEventPrototypeUnlinkPurchasedTickets**
> awEventbookingEventPrototypeUnlinkPurchasedTickets(fk, id)

Remove the purchasedTickets relation to an item by id.

### Example
```java
// Import classes:
//import com.harpoon.api.AwEventbookingEventApi;

AwEventbookingEventApi apiInstance = new AwEventbookingEventApi();
String fk = "fk_example"; // String | Foreign key for purchasedTickets
String id = "id_example"; // String | AwEventbookingEvent id
try {
    apiInstance.awEventbookingEventPrototypeUnlinkPurchasedTickets(fk, id);
} catch (ApiException e) {
    System.err.println("Exception when calling AwEventbookingEventApi#awEventbookingEventPrototypeUnlinkPurchasedTickets");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for purchasedTickets |
 **id** | **String**| AwEventbookingEvent id |

### Return type

null (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingEventPrototypeUpdateAttributesPatchAwEventbookingEventsid"></a>
# **awEventbookingEventPrototypeUpdateAttributesPatchAwEventbookingEventsid**
> AwEventbookingEvent awEventbookingEventPrototypeUpdateAttributesPatchAwEventbookingEventsid(id, data)

Patch attributes for a model instance and persist it into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.AwEventbookingEventApi;

AwEventbookingEventApi apiInstance = new AwEventbookingEventApi();
String id = "id_example"; // String | AwEventbookingEvent id
AwEventbookingEvent data = new AwEventbookingEvent(); // AwEventbookingEvent | An object of model property name/value pairs
try {
    AwEventbookingEvent result = apiInstance.awEventbookingEventPrototypeUpdateAttributesPatchAwEventbookingEventsid(id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AwEventbookingEventApi#awEventbookingEventPrototypeUpdateAttributesPatchAwEventbookingEventsid");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| AwEventbookingEvent id |
 **data** | [**AwEventbookingEvent**](AwEventbookingEvent.md)| An object of model property name/value pairs | [optional]

### Return type

[**AwEventbookingEvent**](AwEventbookingEvent.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingEventPrototypeUpdateAttributesPutAwEventbookingEventsid"></a>
# **awEventbookingEventPrototypeUpdateAttributesPutAwEventbookingEventsid**
> AwEventbookingEvent awEventbookingEventPrototypeUpdateAttributesPutAwEventbookingEventsid(id, data)

Patch attributes for a model instance and persist it into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.AwEventbookingEventApi;

AwEventbookingEventApi apiInstance = new AwEventbookingEventApi();
String id = "id_example"; // String | AwEventbookingEvent id
AwEventbookingEvent data = new AwEventbookingEvent(); // AwEventbookingEvent | An object of model property name/value pairs
try {
    AwEventbookingEvent result = apiInstance.awEventbookingEventPrototypeUpdateAttributesPutAwEventbookingEventsid(id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AwEventbookingEventApi#awEventbookingEventPrototypeUpdateAttributesPutAwEventbookingEventsid");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| AwEventbookingEvent id |
 **data** | [**AwEventbookingEvent**](AwEventbookingEvent.md)| An object of model property name/value pairs | [optional]

### Return type

[**AwEventbookingEvent**](AwEventbookingEvent.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingEventPrototypeUpdateByIdAttributes"></a>
# **awEventbookingEventPrototypeUpdateByIdAttributes**
> AwEventbookingEventAttribute awEventbookingEventPrototypeUpdateByIdAttributes(fk, id, data)

Update a related item by id for attributes.

### Example
```java
// Import classes:
//import com.harpoon.api.AwEventbookingEventApi;

AwEventbookingEventApi apiInstance = new AwEventbookingEventApi();
String fk = "fk_example"; // String | Foreign key for attributes
String id = "id_example"; // String | AwEventbookingEvent id
AwEventbookingEventAttribute data = new AwEventbookingEventAttribute(); // AwEventbookingEventAttribute | 
try {
    AwEventbookingEventAttribute result = apiInstance.awEventbookingEventPrototypeUpdateByIdAttributes(fk, id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AwEventbookingEventApi#awEventbookingEventPrototypeUpdateByIdAttributes");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for attributes |
 **id** | **String**| AwEventbookingEvent id |
 **data** | [**AwEventbookingEventAttribute**](AwEventbookingEventAttribute.md)|  | [optional]

### Return type

[**AwEventbookingEventAttribute**](AwEventbookingEventAttribute.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingEventPrototypeUpdateByIdPurchasedTickets"></a>
# **awEventbookingEventPrototypeUpdateByIdPurchasedTickets**
> AwEventbookingTicket awEventbookingEventPrototypeUpdateByIdPurchasedTickets(fk, id, data)

Update a related item by id for purchasedTickets.

### Example
```java
// Import classes:
//import com.harpoon.api.AwEventbookingEventApi;

AwEventbookingEventApi apiInstance = new AwEventbookingEventApi();
String fk = "fk_example"; // String | Foreign key for purchasedTickets
String id = "id_example"; // String | AwEventbookingEvent id
AwEventbookingTicket data = new AwEventbookingTicket(); // AwEventbookingTicket | 
try {
    AwEventbookingTicket result = apiInstance.awEventbookingEventPrototypeUpdateByIdPurchasedTickets(fk, id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AwEventbookingEventApi#awEventbookingEventPrototypeUpdateByIdPurchasedTickets");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for purchasedTickets |
 **id** | **String**| AwEventbookingEvent id |
 **data** | [**AwEventbookingTicket**](AwEventbookingTicket.md)|  | [optional]

### Return type

[**AwEventbookingTicket**](AwEventbookingTicket.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingEventPrototypeUpdateByIdTickets"></a>
# **awEventbookingEventPrototypeUpdateByIdTickets**
> AwEventbookingEventTicket awEventbookingEventPrototypeUpdateByIdTickets(fk, id, data)

Update a related item by id for tickets.

### Example
```java
// Import classes:
//import com.harpoon.api.AwEventbookingEventApi;

AwEventbookingEventApi apiInstance = new AwEventbookingEventApi();
String fk = "fk_example"; // String | Foreign key for tickets
String id = "id_example"; // String | AwEventbookingEvent id
AwEventbookingEventTicket data = new AwEventbookingEventTicket(); // AwEventbookingEventTicket | 
try {
    AwEventbookingEventTicket result = apiInstance.awEventbookingEventPrototypeUpdateByIdTickets(fk, id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AwEventbookingEventApi#awEventbookingEventPrototypeUpdateByIdTickets");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for tickets |
 **id** | **String**| AwEventbookingEvent id |
 **data** | [**AwEventbookingEventTicket**](AwEventbookingEventTicket.md)|  | [optional]

### Return type

[**AwEventbookingEventTicket**](AwEventbookingEventTicket.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingEventReplaceById"></a>
# **awEventbookingEventReplaceById**
> AwEventbookingEvent awEventbookingEventReplaceById(id, data)

Replace attributes for a model instance and persist it into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.AwEventbookingEventApi;

AwEventbookingEventApi apiInstance = new AwEventbookingEventApi();
String id = "id_example"; // String | Model id
AwEventbookingEvent data = new AwEventbookingEvent(); // AwEventbookingEvent | Model instance data
try {
    AwEventbookingEvent result = apiInstance.awEventbookingEventReplaceById(id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AwEventbookingEventApi#awEventbookingEventReplaceById");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |
 **data** | [**AwEventbookingEvent**](AwEventbookingEvent.md)| Model instance data | [optional]

### Return type

[**AwEventbookingEvent**](AwEventbookingEvent.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingEventReplaceOrCreate"></a>
# **awEventbookingEventReplaceOrCreate**
> AwEventbookingEvent awEventbookingEventReplaceOrCreate(data)

Replace an existing model instance or insert a new one into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.AwEventbookingEventApi;

AwEventbookingEventApi apiInstance = new AwEventbookingEventApi();
AwEventbookingEvent data = new AwEventbookingEvent(); // AwEventbookingEvent | Model instance data
try {
    AwEventbookingEvent result = apiInstance.awEventbookingEventReplaceOrCreate(data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AwEventbookingEventApi#awEventbookingEventReplaceOrCreate");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**AwEventbookingEvent**](AwEventbookingEvent.md)| Model instance data | [optional]

### Return type

[**AwEventbookingEvent**](AwEventbookingEvent.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingEventUpdateAll"></a>
# **awEventbookingEventUpdateAll**
> InlineResponse2002 awEventbookingEventUpdateAll(where, data)

Update instances of the model matched by {{where}} from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.AwEventbookingEventApi;

AwEventbookingEventApi apiInstance = new AwEventbookingEventApi();
String where = "where_example"; // String | Criteria to match model instances
AwEventbookingEvent data = new AwEventbookingEvent(); // AwEventbookingEvent | An object of model property name/value pairs
try {
    InlineResponse2002 result = apiInstance.awEventbookingEventUpdateAll(where, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AwEventbookingEventApi#awEventbookingEventUpdateAll");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional]
 **data** | [**AwEventbookingEvent**](AwEventbookingEvent.md)| An object of model property name/value pairs | [optional]

### Return type

[**InlineResponse2002**](InlineResponse2002.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingEventUpsertPatchAwEventbookingEvents"></a>
# **awEventbookingEventUpsertPatchAwEventbookingEvents**
> AwEventbookingEvent awEventbookingEventUpsertPatchAwEventbookingEvents(data)

Patch an existing model instance or insert a new one into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.AwEventbookingEventApi;

AwEventbookingEventApi apiInstance = new AwEventbookingEventApi();
AwEventbookingEvent data = new AwEventbookingEvent(); // AwEventbookingEvent | Model instance data
try {
    AwEventbookingEvent result = apiInstance.awEventbookingEventUpsertPatchAwEventbookingEvents(data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AwEventbookingEventApi#awEventbookingEventUpsertPatchAwEventbookingEvents");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**AwEventbookingEvent**](AwEventbookingEvent.md)| Model instance data | [optional]

### Return type

[**AwEventbookingEvent**](AwEventbookingEvent.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingEventUpsertPutAwEventbookingEvents"></a>
# **awEventbookingEventUpsertPutAwEventbookingEvents**
> AwEventbookingEvent awEventbookingEventUpsertPutAwEventbookingEvents(data)

Patch an existing model instance or insert a new one into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.AwEventbookingEventApi;

AwEventbookingEventApi apiInstance = new AwEventbookingEventApi();
AwEventbookingEvent data = new AwEventbookingEvent(); // AwEventbookingEvent | Model instance data
try {
    AwEventbookingEvent result = apiInstance.awEventbookingEventUpsertPutAwEventbookingEvents(data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AwEventbookingEventApi#awEventbookingEventUpsertPutAwEventbookingEvents");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**AwEventbookingEvent**](AwEventbookingEvent.md)| Model instance data | [optional]

### Return type

[**AwEventbookingEvent**](AwEventbookingEvent.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingEventUpsertWithWhere"></a>
# **awEventbookingEventUpsertWithWhere**
> AwEventbookingEvent awEventbookingEventUpsertWithWhere(where, data)

Update an existing model instance or insert a new one into the data source based on the where criteria.

### Example
```java
// Import classes:
//import com.harpoon.api.AwEventbookingEventApi;

AwEventbookingEventApi apiInstance = new AwEventbookingEventApi();
String where = "where_example"; // String | Criteria to match model instances
AwEventbookingEvent data = new AwEventbookingEvent(); // AwEventbookingEvent | An object of model property name/value pairs
try {
    AwEventbookingEvent result = apiInstance.awEventbookingEventUpsertWithWhere(where, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AwEventbookingEventApi#awEventbookingEventUpsertWithWhere");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional]
 **data** | [**AwEventbookingEvent**](AwEventbookingEvent.md)| An object of model property name/value pairs | [optional]

### Return type

[**AwEventbookingEvent**](AwEventbookingEvent.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

