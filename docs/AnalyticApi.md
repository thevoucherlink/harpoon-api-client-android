# AnalyticApi

All URIs are relative to *https://apicdn.harpoonconnect.com/v1.1.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**analyticReplaceById**](AnalyticApi.md#analyticReplaceById) | **POST** /Analytics/{id}/replace | Replace attributes for a model instance and persist it into the data source.
[**analyticReplaceOrCreate**](AnalyticApi.md#analyticReplaceOrCreate) | **POST** /Analytics/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
[**analyticTrack**](AnalyticApi.md#analyticTrack) | **POST** /Analytics/track | 
[**analyticUpsertWithWhere**](AnalyticApi.md#analyticUpsertWithWhere) | **POST** /Analytics/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.


<a name="analyticReplaceById"></a>
# **analyticReplaceById**
> Analytic analyticReplaceById(id, data)

Replace attributes for a model instance and persist it into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.AnalyticApi;

AnalyticApi apiInstance = new AnalyticApi();
String id = "id_example"; // String | Model id
Analytic data = new Analytic(); // Analytic | Model instance data
try {
    Analytic result = apiInstance.analyticReplaceById(id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AnalyticApi#analyticReplaceById");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |
 **data** | [**Analytic**](Analytic.md)| Model instance data | [optional]

### Return type

[**Analytic**](Analytic.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="analyticReplaceOrCreate"></a>
# **analyticReplaceOrCreate**
> Analytic analyticReplaceOrCreate(data)

Replace an existing model instance or insert a new one into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.AnalyticApi;

AnalyticApi apiInstance = new AnalyticApi();
Analytic data = new Analytic(); // Analytic | Model instance data
try {
    Analytic result = apiInstance.analyticReplaceOrCreate(data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AnalyticApi#analyticReplaceOrCreate");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**Analytic**](Analytic.md)| Model instance data | [optional]

### Return type

[**Analytic**](Analytic.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="analyticTrack"></a>
# **analyticTrack**
> InlineResponse200 analyticTrack(data)



### Example
```java
// Import classes:
//import com.harpoon.api.AnalyticApi;

AnalyticApi apiInstance = new AnalyticApi();
AnalyticRequest data = new AnalyticRequest(); // AnalyticRequest | 
try {
    InlineResponse200 result = apiInstance.analyticTrack(data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AnalyticApi#analyticTrack");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**AnalyticRequest**](AnalyticRequest.md)|  | [optional]

### Return type

[**InlineResponse200**](InlineResponse200.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="analyticUpsertWithWhere"></a>
# **analyticUpsertWithWhere**
> Analytic analyticUpsertWithWhere(where, data)

Update an existing model instance or insert a new one into the data source based on the where criteria.

### Example
```java
// Import classes:
//import com.harpoon.api.AnalyticApi;

AnalyticApi apiInstance = new AnalyticApi();
String where = "where_example"; // String | Criteria to match model instances
Analytic data = new Analytic(); // Analytic | An object of model property name/value pairs
try {
    Analytic result = apiInstance.analyticUpsertWithWhere(where, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AnalyticApi#analyticUpsertWithWhere");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional]
 **data** | [**Analytic**](Analytic.md)| An object of model property name/value pairs | [optional]

### Return type

[**Analytic**](Analytic.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

