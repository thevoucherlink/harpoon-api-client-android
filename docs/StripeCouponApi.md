# StripeCouponApi

All URIs are relative to *https://apicdn.harpoonconnect.com/v1.1.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**stripeCouponCount**](StripeCouponApi.md#stripeCouponCount) | **GET** /StripeCoupons/count | Count instances of the model matched by where from the data source.
[**stripeCouponCreate**](StripeCouponApi.md#stripeCouponCreate) | **POST** /StripeCoupons | Create a new instance of the model and persist it into the data source.
[**stripeCouponCreateChangeStreamGetStripeCouponsChangeStream**](StripeCouponApi.md#stripeCouponCreateChangeStreamGetStripeCouponsChangeStream) | **GET** /StripeCoupons/change-stream | Create a change stream.
[**stripeCouponCreateChangeStreamPostStripeCouponsChangeStream**](StripeCouponApi.md#stripeCouponCreateChangeStreamPostStripeCouponsChangeStream) | **POST** /StripeCoupons/change-stream | Create a change stream.
[**stripeCouponDeleteById**](StripeCouponApi.md#stripeCouponDeleteById) | **DELETE** /StripeCoupons/{id} | Delete a model instance by {{id}} from the data source.
[**stripeCouponExistsGetStripeCouponsidExists**](StripeCouponApi.md#stripeCouponExistsGetStripeCouponsidExists) | **GET** /StripeCoupons/{id}/exists | Check whether a model instance exists in the data source.
[**stripeCouponExistsHeadStripeCouponsid**](StripeCouponApi.md#stripeCouponExistsHeadStripeCouponsid) | **HEAD** /StripeCoupons/{id} | Check whether a model instance exists in the data source.
[**stripeCouponFind**](StripeCouponApi.md#stripeCouponFind) | **GET** /StripeCoupons | Find all instances of the model matched by filter from the data source.
[**stripeCouponFindById**](StripeCouponApi.md#stripeCouponFindById) | **GET** /StripeCoupons/{id} | Find a model instance by {{id}} from the data source.
[**stripeCouponFindOne**](StripeCouponApi.md#stripeCouponFindOne) | **GET** /StripeCoupons/findOne | Find first instance of the model matched by filter from the data source.
[**stripeCouponPrototypeUpdateAttributesPatchStripeCouponsid**](StripeCouponApi.md#stripeCouponPrototypeUpdateAttributesPatchStripeCouponsid) | **PATCH** /StripeCoupons/{id} | Patch attributes for a model instance and persist it into the data source.
[**stripeCouponPrototypeUpdateAttributesPutStripeCouponsid**](StripeCouponApi.md#stripeCouponPrototypeUpdateAttributesPutStripeCouponsid) | **PUT** /StripeCoupons/{id} | Patch attributes for a model instance and persist it into the data source.
[**stripeCouponReplaceById**](StripeCouponApi.md#stripeCouponReplaceById) | **POST** /StripeCoupons/{id}/replace | Replace attributes for a model instance and persist it into the data source.
[**stripeCouponReplaceOrCreate**](StripeCouponApi.md#stripeCouponReplaceOrCreate) | **POST** /StripeCoupons/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
[**stripeCouponUpdateAll**](StripeCouponApi.md#stripeCouponUpdateAll) | **POST** /StripeCoupons/update | Update instances of the model matched by {{where}} from the data source.
[**stripeCouponUpsertPatchStripeCoupons**](StripeCouponApi.md#stripeCouponUpsertPatchStripeCoupons) | **PATCH** /StripeCoupons | Patch an existing model instance or insert a new one into the data source.
[**stripeCouponUpsertPutStripeCoupons**](StripeCouponApi.md#stripeCouponUpsertPutStripeCoupons) | **PUT** /StripeCoupons | Patch an existing model instance or insert a new one into the data source.
[**stripeCouponUpsertWithWhere**](StripeCouponApi.md#stripeCouponUpsertWithWhere) | **POST** /StripeCoupons/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.


<a name="stripeCouponCount"></a>
# **stripeCouponCount**
> InlineResponse2001 stripeCouponCount(where)

Count instances of the model matched by where from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.StripeCouponApi;

StripeCouponApi apiInstance = new StripeCouponApi();
String where = "where_example"; // String | Criteria to match model instances
try {
    InlineResponse2001 result = apiInstance.stripeCouponCount(where);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling StripeCouponApi#stripeCouponCount");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional]

### Return type

[**InlineResponse2001**](InlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripeCouponCreate"></a>
# **stripeCouponCreate**
> StripeCoupon stripeCouponCreate(data)

Create a new instance of the model and persist it into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.StripeCouponApi;

StripeCouponApi apiInstance = new StripeCouponApi();
StripeCoupon data = new StripeCoupon(); // StripeCoupon | Model instance data
try {
    StripeCoupon result = apiInstance.stripeCouponCreate(data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling StripeCouponApi#stripeCouponCreate");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**StripeCoupon**](StripeCoupon.md)| Model instance data | [optional]

### Return type

[**StripeCoupon**](StripeCoupon.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripeCouponCreateChangeStreamGetStripeCouponsChangeStream"></a>
# **stripeCouponCreateChangeStreamGetStripeCouponsChangeStream**
> File stripeCouponCreateChangeStreamGetStripeCouponsChangeStream(options)

Create a change stream.

### Example
```java
// Import classes:
//import com.harpoon.api.StripeCouponApi;

StripeCouponApi apiInstance = new StripeCouponApi();
String options = "options_example"; // String | 
try {
    File result = apiInstance.stripeCouponCreateChangeStreamGetStripeCouponsChangeStream(options);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling StripeCouponApi#stripeCouponCreateChangeStreamGetStripeCouponsChangeStream");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **String**|  | [optional]

### Return type

[**File**](File.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripeCouponCreateChangeStreamPostStripeCouponsChangeStream"></a>
# **stripeCouponCreateChangeStreamPostStripeCouponsChangeStream**
> File stripeCouponCreateChangeStreamPostStripeCouponsChangeStream(options)

Create a change stream.

### Example
```java
// Import classes:
//import com.harpoon.api.StripeCouponApi;

StripeCouponApi apiInstance = new StripeCouponApi();
String options = "options_example"; // String | 
try {
    File result = apiInstance.stripeCouponCreateChangeStreamPostStripeCouponsChangeStream(options);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling StripeCouponApi#stripeCouponCreateChangeStreamPostStripeCouponsChangeStream");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **String**|  | [optional]

### Return type

[**File**](File.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripeCouponDeleteById"></a>
# **stripeCouponDeleteById**
> Object stripeCouponDeleteById(id)

Delete a model instance by {{id}} from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.StripeCouponApi;

StripeCouponApi apiInstance = new StripeCouponApi();
String id = "id_example"; // String | Model id
try {
    Object result = apiInstance.stripeCouponDeleteById(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling StripeCouponApi#stripeCouponDeleteById");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |

### Return type

**Object**

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripeCouponExistsGetStripeCouponsidExists"></a>
# **stripeCouponExistsGetStripeCouponsidExists**
> InlineResponse2003 stripeCouponExistsGetStripeCouponsidExists(id)

Check whether a model instance exists in the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.StripeCouponApi;

StripeCouponApi apiInstance = new StripeCouponApi();
String id = "id_example"; // String | Model id
try {
    InlineResponse2003 result = apiInstance.stripeCouponExistsGetStripeCouponsidExists(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling StripeCouponApi#stripeCouponExistsGetStripeCouponsidExists");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |

### Return type

[**InlineResponse2003**](InlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripeCouponExistsHeadStripeCouponsid"></a>
# **stripeCouponExistsHeadStripeCouponsid**
> InlineResponse2003 stripeCouponExistsHeadStripeCouponsid(id)

Check whether a model instance exists in the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.StripeCouponApi;

StripeCouponApi apiInstance = new StripeCouponApi();
String id = "id_example"; // String | Model id
try {
    InlineResponse2003 result = apiInstance.stripeCouponExistsHeadStripeCouponsid(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling StripeCouponApi#stripeCouponExistsHeadStripeCouponsid");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |

### Return type

[**InlineResponse2003**](InlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripeCouponFind"></a>
# **stripeCouponFind**
> List&lt;StripeCoupon&gt; stripeCouponFind(filter)

Find all instances of the model matched by filter from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.StripeCouponApi;

StripeCouponApi apiInstance = new StripeCouponApi();
String filter = "filter_example"; // String | Filter defining fields, where, include, order, offset, and limit
try {
    List<StripeCoupon> result = apiInstance.stripeCouponFind(filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling StripeCouponApi#stripeCouponFind");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **String**| Filter defining fields, where, include, order, offset, and limit | [optional]

### Return type

[**List&lt;StripeCoupon&gt;**](StripeCoupon.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripeCouponFindById"></a>
# **stripeCouponFindById**
> StripeCoupon stripeCouponFindById(id, filter)

Find a model instance by {{id}} from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.StripeCouponApi;

StripeCouponApi apiInstance = new StripeCouponApi();
String id = "id_example"; // String | Model id
String filter = "filter_example"; // String | Filter defining fields and include
try {
    StripeCoupon result = apiInstance.stripeCouponFindById(id, filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling StripeCouponApi#stripeCouponFindById");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |
 **filter** | **String**| Filter defining fields and include | [optional]

### Return type

[**StripeCoupon**](StripeCoupon.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripeCouponFindOne"></a>
# **stripeCouponFindOne**
> StripeCoupon stripeCouponFindOne(filter)

Find first instance of the model matched by filter from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.StripeCouponApi;

StripeCouponApi apiInstance = new StripeCouponApi();
String filter = "filter_example"; // String | Filter defining fields, where, include, order, offset, and limit
try {
    StripeCoupon result = apiInstance.stripeCouponFindOne(filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling StripeCouponApi#stripeCouponFindOne");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **String**| Filter defining fields, where, include, order, offset, and limit | [optional]

### Return type

[**StripeCoupon**](StripeCoupon.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripeCouponPrototypeUpdateAttributesPatchStripeCouponsid"></a>
# **stripeCouponPrototypeUpdateAttributesPatchStripeCouponsid**
> StripeCoupon stripeCouponPrototypeUpdateAttributesPatchStripeCouponsid(id, data)

Patch attributes for a model instance and persist it into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.StripeCouponApi;

StripeCouponApi apiInstance = new StripeCouponApi();
String id = "id_example"; // String | StripeCoupon id
StripeCoupon data = new StripeCoupon(); // StripeCoupon | An object of model property name/value pairs
try {
    StripeCoupon result = apiInstance.stripeCouponPrototypeUpdateAttributesPatchStripeCouponsid(id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling StripeCouponApi#stripeCouponPrototypeUpdateAttributesPatchStripeCouponsid");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| StripeCoupon id |
 **data** | [**StripeCoupon**](StripeCoupon.md)| An object of model property name/value pairs | [optional]

### Return type

[**StripeCoupon**](StripeCoupon.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripeCouponPrototypeUpdateAttributesPutStripeCouponsid"></a>
# **stripeCouponPrototypeUpdateAttributesPutStripeCouponsid**
> StripeCoupon stripeCouponPrototypeUpdateAttributesPutStripeCouponsid(id, data)

Patch attributes for a model instance and persist it into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.StripeCouponApi;

StripeCouponApi apiInstance = new StripeCouponApi();
String id = "id_example"; // String | StripeCoupon id
StripeCoupon data = new StripeCoupon(); // StripeCoupon | An object of model property name/value pairs
try {
    StripeCoupon result = apiInstance.stripeCouponPrototypeUpdateAttributesPutStripeCouponsid(id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling StripeCouponApi#stripeCouponPrototypeUpdateAttributesPutStripeCouponsid");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| StripeCoupon id |
 **data** | [**StripeCoupon**](StripeCoupon.md)| An object of model property name/value pairs | [optional]

### Return type

[**StripeCoupon**](StripeCoupon.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripeCouponReplaceById"></a>
# **stripeCouponReplaceById**
> StripeCoupon stripeCouponReplaceById(id, data)

Replace attributes for a model instance and persist it into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.StripeCouponApi;

StripeCouponApi apiInstance = new StripeCouponApi();
String id = "id_example"; // String | Model id
StripeCoupon data = new StripeCoupon(); // StripeCoupon | Model instance data
try {
    StripeCoupon result = apiInstance.stripeCouponReplaceById(id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling StripeCouponApi#stripeCouponReplaceById");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |
 **data** | [**StripeCoupon**](StripeCoupon.md)| Model instance data | [optional]

### Return type

[**StripeCoupon**](StripeCoupon.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripeCouponReplaceOrCreate"></a>
# **stripeCouponReplaceOrCreate**
> StripeCoupon stripeCouponReplaceOrCreate(data)

Replace an existing model instance or insert a new one into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.StripeCouponApi;

StripeCouponApi apiInstance = new StripeCouponApi();
StripeCoupon data = new StripeCoupon(); // StripeCoupon | Model instance data
try {
    StripeCoupon result = apiInstance.stripeCouponReplaceOrCreate(data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling StripeCouponApi#stripeCouponReplaceOrCreate");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**StripeCoupon**](StripeCoupon.md)| Model instance data | [optional]

### Return type

[**StripeCoupon**](StripeCoupon.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripeCouponUpdateAll"></a>
# **stripeCouponUpdateAll**
> InlineResponse2002 stripeCouponUpdateAll(where, data)

Update instances of the model matched by {{where}} from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.StripeCouponApi;

StripeCouponApi apiInstance = new StripeCouponApi();
String where = "where_example"; // String | Criteria to match model instances
StripeCoupon data = new StripeCoupon(); // StripeCoupon | An object of model property name/value pairs
try {
    InlineResponse2002 result = apiInstance.stripeCouponUpdateAll(where, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling StripeCouponApi#stripeCouponUpdateAll");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional]
 **data** | [**StripeCoupon**](StripeCoupon.md)| An object of model property name/value pairs | [optional]

### Return type

[**InlineResponse2002**](InlineResponse2002.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripeCouponUpsertPatchStripeCoupons"></a>
# **stripeCouponUpsertPatchStripeCoupons**
> StripeCoupon stripeCouponUpsertPatchStripeCoupons(data)

Patch an existing model instance or insert a new one into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.StripeCouponApi;

StripeCouponApi apiInstance = new StripeCouponApi();
StripeCoupon data = new StripeCoupon(); // StripeCoupon | Model instance data
try {
    StripeCoupon result = apiInstance.stripeCouponUpsertPatchStripeCoupons(data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling StripeCouponApi#stripeCouponUpsertPatchStripeCoupons");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**StripeCoupon**](StripeCoupon.md)| Model instance data | [optional]

### Return type

[**StripeCoupon**](StripeCoupon.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripeCouponUpsertPutStripeCoupons"></a>
# **stripeCouponUpsertPutStripeCoupons**
> StripeCoupon stripeCouponUpsertPutStripeCoupons(data)

Patch an existing model instance or insert a new one into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.StripeCouponApi;

StripeCouponApi apiInstance = new StripeCouponApi();
StripeCoupon data = new StripeCoupon(); // StripeCoupon | Model instance data
try {
    StripeCoupon result = apiInstance.stripeCouponUpsertPutStripeCoupons(data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling StripeCouponApi#stripeCouponUpsertPutStripeCoupons");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**StripeCoupon**](StripeCoupon.md)| Model instance data | [optional]

### Return type

[**StripeCoupon**](StripeCoupon.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="stripeCouponUpsertWithWhere"></a>
# **stripeCouponUpsertWithWhere**
> StripeCoupon stripeCouponUpsertWithWhere(where, data)

Update an existing model instance or insert a new one into the data source based on the where criteria.

### Example
```java
// Import classes:
//import com.harpoon.api.StripeCouponApi;

StripeCouponApi apiInstance = new StripeCouponApi();
String where = "where_example"; // String | Criteria to match model instances
StripeCoupon data = new StripeCoupon(); // StripeCoupon | An object of model property name/value pairs
try {
    StripeCoupon result = apiInstance.stripeCouponUpsertWithWhere(where, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling StripeCouponApi#stripeCouponUpsertWithWhere");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional]
 **data** | [**StripeCoupon**](StripeCoupon.md)| An object of model property name/value pairs | [optional]

### Return type

[**StripeCoupon**](StripeCoupon.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

