
# AnalyticRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**requests** | **List&lt;Object&gt;** |  | 
**scope** | [**ScopeEnum**](#ScopeEnum) |  |  [optional]
**id** | **Double** |  |  [optional]


<a name="ScopeEnum"></a>
## Enum: ScopeEnum
Name | Value
---- | -----



