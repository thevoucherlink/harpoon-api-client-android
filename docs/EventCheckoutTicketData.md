
# EventCheckoutTicketData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Double** | Ticket to checkout | 
**qty** | **Double** | Quantity to checkout |  [optional]



