
# CatalogCategory

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Double** |  | 
**parentId** | **Double** |  | 
**createdAt** | [**Date**](Date.md) |  |  [optional]
**updatedAt** | [**Date**](Date.md) |  |  [optional]
**path** | **String** |  | 
**position** | **Double** |  | 
**level** | **Double** |  | 
**childrenCount** | **Double** |  | 
**storeId** | **Double** |  | 
**allChildren** | **String** |  |  [optional]
**availableSortBy** | **String** |  |  [optional]
**children** | **String** |  |  [optional]
**customApplyToProducts** | **Double** |  |  [optional]
**customDesign** | **String** |  |  [optional]
**customDesignFrom** | [**Date**](Date.md) |  |  [optional]
**customDesignTo** | [**Date**](Date.md) |  |  [optional]
**customLayoutUpdate** | **String** |  |  [optional]
**customUseParentSettings** | **Double** |  |  [optional]
**defaultSortBy** | **String** |  |  [optional]
**description** | **String** |  |  [optional]
**displayMode** | **String** |  |  [optional]
**filterPriceRange** | **String** |  |  [optional]
**image** | **String** |  |  [optional]
**includeInMenu** | **Double** |  |  [optional]
**isActive** | **Double** |  |  [optional]
**isAnchor** | **Double** |  |  [optional]
**landingPage** | **Double** |  |  [optional]
**metaDescription** | **String** |  |  [optional]
**metaKeywords** | **String** |  |  [optional]
**metaTitle** | **String** |  |  [optional]
**name** | **String** |  |  [optional]
**pageLayout** | **String** |  |  [optional]
**pathInStore** | **String** |  |  [optional]
**thumbnail** | **String** |  |  [optional]
**ummCatLabel** | **String** |  |  [optional]
**ummCatTarget** | **String** |  |  [optional]
**ummDdBlocks** | **String** |  |  [optional]
**ummDdColumns** | **Double** |  |  [optional]
**ummDdProportions** | **String** |  |  [optional]
**ummDdType** | **String** |  |  [optional]
**ummDdWidth** | **String** |  |  [optional]
**urlKey** | **String** |  |  [optional]
**urlPath** | **String** |  |  [optional]
**catalogProducts** | **List&lt;Object&gt;** |  |  [optional]



