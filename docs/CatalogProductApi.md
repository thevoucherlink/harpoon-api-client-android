# CatalogProductApi

All URIs are relative to *https://apicdn.harpoonconnect.com/v1.1.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**catalogProductCount**](CatalogProductApi.md#catalogProductCount) | **GET** /CatalogProducts/count | Count instances of the model matched by where from the data source.
[**catalogProductCreate**](CatalogProductApi.md#catalogProductCreate) | **POST** /CatalogProducts | Create a new instance of the model and persist it into the data source.
[**catalogProductCreateChangeStreamGetCatalogProductsChangeStream**](CatalogProductApi.md#catalogProductCreateChangeStreamGetCatalogProductsChangeStream) | **GET** /CatalogProducts/change-stream | Create a change stream.
[**catalogProductCreateChangeStreamPostCatalogProductsChangeStream**](CatalogProductApi.md#catalogProductCreateChangeStreamPostCatalogProductsChangeStream) | **POST** /CatalogProducts/change-stream | Create a change stream.
[**catalogProductDeleteById**](CatalogProductApi.md#catalogProductDeleteById) | **DELETE** /CatalogProducts/{id} | Delete a model instance by {{id}} from the data source.
[**catalogProductExistsGetCatalogProductsidExists**](CatalogProductApi.md#catalogProductExistsGetCatalogProductsidExists) | **GET** /CatalogProducts/{id}/exists | Check whether a model instance exists in the data source.
[**catalogProductExistsHeadCatalogProductsid**](CatalogProductApi.md#catalogProductExistsHeadCatalogProductsid) | **HEAD** /CatalogProducts/{id} | Check whether a model instance exists in the data source.
[**catalogProductFind**](CatalogProductApi.md#catalogProductFind) | **GET** /CatalogProducts | Find all instances of the model matched by filter from the data source.
[**catalogProductFindById**](CatalogProductApi.md#catalogProductFindById) | **GET** /CatalogProducts/{id} | Find a model instance by {{id}} from the data source.
[**catalogProductFindOne**](CatalogProductApi.md#catalogProductFindOne) | **GET** /CatalogProducts/findOne | Find first instance of the model matched by filter from the data source.
[**catalogProductPrototypeCountCatalogCategories**](CatalogProductApi.md#catalogProductPrototypeCountCatalogCategories) | **GET** /CatalogProducts/{id}/catalogCategories/count | Counts catalogCategories of CatalogProduct.
[**catalogProductPrototypeCountUdropshipVendorProducts**](CatalogProductApi.md#catalogProductPrototypeCountUdropshipVendorProducts) | **GET** /CatalogProducts/{id}/udropshipVendorProducts/count | Counts udropshipVendorProducts of CatalogProduct.
[**catalogProductPrototypeCountUdropshipVendors**](CatalogProductApi.md#catalogProductPrototypeCountUdropshipVendors) | **GET** /CatalogProducts/{id}/udropshipVendors/count | Counts udropshipVendors of CatalogProduct.
[**catalogProductPrototypeCreateAwCollpurDeal**](CatalogProductApi.md#catalogProductPrototypeCreateAwCollpurDeal) | **POST** /CatalogProducts/{id}/awCollpurDeal | Creates a new instance in awCollpurDeal of this model.
[**catalogProductPrototypeCreateCatalogCategories**](CatalogProductApi.md#catalogProductPrototypeCreateCatalogCategories) | **POST** /CatalogProducts/{id}/catalogCategories | Creates a new instance in catalogCategories of this model.
[**catalogProductPrototypeCreateInventory**](CatalogProductApi.md#catalogProductPrototypeCreateInventory) | **POST** /CatalogProducts/{id}/inventory | Creates a new instance in inventory of this model.
[**catalogProductPrototypeCreateProductEventCompetition**](CatalogProductApi.md#catalogProductPrototypeCreateProductEventCompetition) | **POST** /CatalogProducts/{id}/productEventCompetition | Creates a new instance in productEventCompetition of this model.
[**catalogProductPrototypeCreateUdropshipVendorProducts**](CatalogProductApi.md#catalogProductPrototypeCreateUdropshipVendorProducts) | **POST** /CatalogProducts/{id}/udropshipVendorProducts | Creates a new instance in udropshipVendorProducts of this model.
[**catalogProductPrototypeCreateUdropshipVendors**](CatalogProductApi.md#catalogProductPrototypeCreateUdropshipVendors) | **POST** /CatalogProducts/{id}/udropshipVendors | Creates a new instance in udropshipVendors of this model.
[**catalogProductPrototypeDeleteCatalogCategories**](CatalogProductApi.md#catalogProductPrototypeDeleteCatalogCategories) | **DELETE** /CatalogProducts/{id}/catalogCategories | Deletes all catalogCategories of this model.
[**catalogProductPrototypeDeleteUdropshipVendorProducts**](CatalogProductApi.md#catalogProductPrototypeDeleteUdropshipVendorProducts) | **DELETE** /CatalogProducts/{id}/udropshipVendorProducts | Deletes all udropshipVendorProducts of this model.
[**catalogProductPrototypeDeleteUdropshipVendors**](CatalogProductApi.md#catalogProductPrototypeDeleteUdropshipVendors) | **DELETE** /CatalogProducts/{id}/udropshipVendors | Deletes all udropshipVendors of this model.
[**catalogProductPrototypeDestroyAwCollpurDeal**](CatalogProductApi.md#catalogProductPrototypeDestroyAwCollpurDeal) | **DELETE** /CatalogProducts/{id}/awCollpurDeal | Deletes awCollpurDeal of this model.
[**catalogProductPrototypeDestroyByIdCatalogCategories**](CatalogProductApi.md#catalogProductPrototypeDestroyByIdCatalogCategories) | **DELETE** /CatalogProducts/{id}/catalogCategories/{fk} | Delete a related item by id for catalogCategories.
[**catalogProductPrototypeDestroyByIdUdropshipVendorProducts**](CatalogProductApi.md#catalogProductPrototypeDestroyByIdUdropshipVendorProducts) | **DELETE** /CatalogProducts/{id}/udropshipVendorProducts/{fk} | Delete a related item by id for udropshipVendorProducts.
[**catalogProductPrototypeDestroyByIdUdropshipVendors**](CatalogProductApi.md#catalogProductPrototypeDestroyByIdUdropshipVendors) | **DELETE** /CatalogProducts/{id}/udropshipVendors/{fk} | Delete a related item by id for udropshipVendors.
[**catalogProductPrototypeDestroyInventory**](CatalogProductApi.md#catalogProductPrototypeDestroyInventory) | **DELETE** /CatalogProducts/{id}/inventory | Deletes inventory of this model.
[**catalogProductPrototypeDestroyProductEventCompetition**](CatalogProductApi.md#catalogProductPrototypeDestroyProductEventCompetition) | **DELETE** /CatalogProducts/{id}/productEventCompetition | Deletes productEventCompetition of this model.
[**catalogProductPrototypeExistsCatalogCategories**](CatalogProductApi.md#catalogProductPrototypeExistsCatalogCategories) | **HEAD** /CatalogProducts/{id}/catalogCategories/rel/{fk} | Check the existence of catalogCategories relation to an item by id.
[**catalogProductPrototypeExistsUdropshipVendors**](CatalogProductApi.md#catalogProductPrototypeExistsUdropshipVendors) | **HEAD** /CatalogProducts/{id}/udropshipVendors/rel/{fk} | Check the existence of udropshipVendors relation to an item by id.
[**catalogProductPrototypeFindByIdCatalogCategories**](CatalogProductApi.md#catalogProductPrototypeFindByIdCatalogCategories) | **GET** /CatalogProducts/{id}/catalogCategories/{fk} | Find a related item by id for catalogCategories.
[**catalogProductPrototypeFindByIdUdropshipVendorProducts**](CatalogProductApi.md#catalogProductPrototypeFindByIdUdropshipVendorProducts) | **GET** /CatalogProducts/{id}/udropshipVendorProducts/{fk} | Find a related item by id for udropshipVendorProducts.
[**catalogProductPrototypeFindByIdUdropshipVendors**](CatalogProductApi.md#catalogProductPrototypeFindByIdUdropshipVendors) | **GET** /CatalogProducts/{id}/udropshipVendors/{fk} | Find a related item by id for udropshipVendors.
[**catalogProductPrototypeGetAwCollpurDeal**](CatalogProductApi.md#catalogProductPrototypeGetAwCollpurDeal) | **GET** /CatalogProducts/{id}/awCollpurDeal | Fetches hasOne relation awCollpurDeal.
[**catalogProductPrototypeGetCatalogCategories**](CatalogProductApi.md#catalogProductPrototypeGetCatalogCategories) | **GET** /CatalogProducts/{id}/catalogCategories | Queries catalogCategories of CatalogProduct.
[**catalogProductPrototypeGetCheckoutAgreement**](CatalogProductApi.md#catalogProductPrototypeGetCheckoutAgreement) | **GET** /CatalogProducts/{id}/checkoutAgreement | Fetches belongsTo relation checkoutAgreement.
[**catalogProductPrototypeGetCreator**](CatalogProductApi.md#catalogProductPrototypeGetCreator) | **GET** /CatalogProducts/{id}/creator | Fetches belongsTo relation creator.
[**catalogProductPrototypeGetInventory**](CatalogProductApi.md#catalogProductPrototypeGetInventory) | **GET** /CatalogProducts/{id}/inventory | Fetches hasOne relation inventory.
[**catalogProductPrototypeGetProductEventCompetition**](CatalogProductApi.md#catalogProductPrototypeGetProductEventCompetition) | **GET** /CatalogProducts/{id}/productEventCompetition | Fetches hasOne relation productEventCompetition.
[**catalogProductPrototypeGetUdropshipVendorProducts**](CatalogProductApi.md#catalogProductPrototypeGetUdropshipVendorProducts) | **GET** /CatalogProducts/{id}/udropshipVendorProducts | Queries udropshipVendorProducts of CatalogProduct.
[**catalogProductPrototypeGetUdropshipVendors**](CatalogProductApi.md#catalogProductPrototypeGetUdropshipVendors) | **GET** /CatalogProducts/{id}/udropshipVendors | Queries udropshipVendors of CatalogProduct.
[**catalogProductPrototypeLinkCatalogCategories**](CatalogProductApi.md#catalogProductPrototypeLinkCatalogCategories) | **PUT** /CatalogProducts/{id}/catalogCategories/rel/{fk} | Add a related item by id for catalogCategories.
[**catalogProductPrototypeLinkUdropshipVendors**](CatalogProductApi.md#catalogProductPrototypeLinkUdropshipVendors) | **PUT** /CatalogProducts/{id}/udropshipVendors/rel/{fk} | Add a related item by id for udropshipVendors.
[**catalogProductPrototypeUnlinkCatalogCategories**](CatalogProductApi.md#catalogProductPrototypeUnlinkCatalogCategories) | **DELETE** /CatalogProducts/{id}/catalogCategories/rel/{fk} | Remove the catalogCategories relation to an item by id.
[**catalogProductPrototypeUnlinkUdropshipVendors**](CatalogProductApi.md#catalogProductPrototypeUnlinkUdropshipVendors) | **DELETE** /CatalogProducts/{id}/udropshipVendors/rel/{fk} | Remove the udropshipVendors relation to an item by id.
[**catalogProductPrototypeUpdateAttributesPatchCatalogProductsid**](CatalogProductApi.md#catalogProductPrototypeUpdateAttributesPatchCatalogProductsid) | **PATCH** /CatalogProducts/{id} | Patch attributes for a model instance and persist it into the data source.
[**catalogProductPrototypeUpdateAttributesPutCatalogProductsid**](CatalogProductApi.md#catalogProductPrototypeUpdateAttributesPutCatalogProductsid) | **PUT** /CatalogProducts/{id} | Patch attributes for a model instance and persist it into the data source.
[**catalogProductPrototypeUpdateAwCollpurDeal**](CatalogProductApi.md#catalogProductPrototypeUpdateAwCollpurDeal) | **PUT** /CatalogProducts/{id}/awCollpurDeal | Update awCollpurDeal of this model.
[**catalogProductPrototypeUpdateByIdCatalogCategories**](CatalogProductApi.md#catalogProductPrototypeUpdateByIdCatalogCategories) | **PUT** /CatalogProducts/{id}/catalogCategories/{fk} | Update a related item by id for catalogCategories.
[**catalogProductPrototypeUpdateByIdUdropshipVendorProducts**](CatalogProductApi.md#catalogProductPrototypeUpdateByIdUdropshipVendorProducts) | **PUT** /CatalogProducts/{id}/udropshipVendorProducts/{fk} | Update a related item by id for udropshipVendorProducts.
[**catalogProductPrototypeUpdateByIdUdropshipVendors**](CatalogProductApi.md#catalogProductPrototypeUpdateByIdUdropshipVendors) | **PUT** /CatalogProducts/{id}/udropshipVendors/{fk} | Update a related item by id for udropshipVendors.
[**catalogProductPrototypeUpdateInventory**](CatalogProductApi.md#catalogProductPrototypeUpdateInventory) | **PUT** /CatalogProducts/{id}/inventory | Update inventory of this model.
[**catalogProductPrototypeUpdateProductEventCompetition**](CatalogProductApi.md#catalogProductPrototypeUpdateProductEventCompetition) | **PUT** /CatalogProducts/{id}/productEventCompetition | Update productEventCompetition of this model.
[**catalogProductReplaceById**](CatalogProductApi.md#catalogProductReplaceById) | **POST** /CatalogProducts/{id}/replace | Replace attributes for a model instance and persist it into the data source.
[**catalogProductReplaceOrCreate**](CatalogProductApi.md#catalogProductReplaceOrCreate) | **POST** /CatalogProducts/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
[**catalogProductUpdateAll**](CatalogProductApi.md#catalogProductUpdateAll) | **POST** /CatalogProducts/update | Update instances of the model matched by {{where}} from the data source.
[**catalogProductUpsertPatchCatalogProducts**](CatalogProductApi.md#catalogProductUpsertPatchCatalogProducts) | **PATCH** /CatalogProducts | Patch an existing model instance or insert a new one into the data source.
[**catalogProductUpsertPutCatalogProducts**](CatalogProductApi.md#catalogProductUpsertPutCatalogProducts) | **PUT** /CatalogProducts | Patch an existing model instance or insert a new one into the data source.
[**catalogProductUpsertWithWhere**](CatalogProductApi.md#catalogProductUpsertWithWhere) | **POST** /CatalogProducts/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.


<a name="catalogProductCount"></a>
# **catalogProductCount**
> InlineResponse2001 catalogProductCount(where)

Count instances of the model matched by where from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.CatalogProductApi;

CatalogProductApi apiInstance = new CatalogProductApi();
String where = "where_example"; // String | Criteria to match model instances
try {
    InlineResponse2001 result = apiInstance.catalogProductCount(where);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CatalogProductApi#catalogProductCount");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional]

### Return type

[**InlineResponse2001**](InlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="catalogProductCreate"></a>
# **catalogProductCreate**
> CatalogProduct catalogProductCreate(data)

Create a new instance of the model and persist it into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.CatalogProductApi;

CatalogProductApi apiInstance = new CatalogProductApi();
CatalogProduct data = new CatalogProduct(); // CatalogProduct | Model instance data
try {
    CatalogProduct result = apiInstance.catalogProductCreate(data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CatalogProductApi#catalogProductCreate");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**CatalogProduct**](CatalogProduct.md)| Model instance data | [optional]

### Return type

[**CatalogProduct**](CatalogProduct.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="catalogProductCreateChangeStreamGetCatalogProductsChangeStream"></a>
# **catalogProductCreateChangeStreamGetCatalogProductsChangeStream**
> File catalogProductCreateChangeStreamGetCatalogProductsChangeStream(options)

Create a change stream.

### Example
```java
// Import classes:
//import com.harpoon.api.CatalogProductApi;

CatalogProductApi apiInstance = new CatalogProductApi();
String options = "options_example"; // String | 
try {
    File result = apiInstance.catalogProductCreateChangeStreamGetCatalogProductsChangeStream(options);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CatalogProductApi#catalogProductCreateChangeStreamGetCatalogProductsChangeStream");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **String**|  | [optional]

### Return type

[**File**](File.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="catalogProductCreateChangeStreamPostCatalogProductsChangeStream"></a>
# **catalogProductCreateChangeStreamPostCatalogProductsChangeStream**
> File catalogProductCreateChangeStreamPostCatalogProductsChangeStream(options)

Create a change stream.

### Example
```java
// Import classes:
//import com.harpoon.api.CatalogProductApi;

CatalogProductApi apiInstance = new CatalogProductApi();
String options = "options_example"; // String | 
try {
    File result = apiInstance.catalogProductCreateChangeStreamPostCatalogProductsChangeStream(options);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CatalogProductApi#catalogProductCreateChangeStreamPostCatalogProductsChangeStream");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **String**|  | [optional]

### Return type

[**File**](File.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="catalogProductDeleteById"></a>
# **catalogProductDeleteById**
> Object catalogProductDeleteById(id)

Delete a model instance by {{id}} from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.CatalogProductApi;

CatalogProductApi apiInstance = new CatalogProductApi();
String id = "id_example"; // String | Model id
try {
    Object result = apiInstance.catalogProductDeleteById(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CatalogProductApi#catalogProductDeleteById");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |

### Return type

**Object**

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="catalogProductExistsGetCatalogProductsidExists"></a>
# **catalogProductExistsGetCatalogProductsidExists**
> InlineResponse2003 catalogProductExistsGetCatalogProductsidExists(id)

Check whether a model instance exists in the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.CatalogProductApi;

CatalogProductApi apiInstance = new CatalogProductApi();
String id = "id_example"; // String | Model id
try {
    InlineResponse2003 result = apiInstance.catalogProductExistsGetCatalogProductsidExists(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CatalogProductApi#catalogProductExistsGetCatalogProductsidExists");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |

### Return type

[**InlineResponse2003**](InlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="catalogProductExistsHeadCatalogProductsid"></a>
# **catalogProductExistsHeadCatalogProductsid**
> InlineResponse2003 catalogProductExistsHeadCatalogProductsid(id)

Check whether a model instance exists in the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.CatalogProductApi;

CatalogProductApi apiInstance = new CatalogProductApi();
String id = "id_example"; // String | Model id
try {
    InlineResponse2003 result = apiInstance.catalogProductExistsHeadCatalogProductsid(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CatalogProductApi#catalogProductExistsHeadCatalogProductsid");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |

### Return type

[**InlineResponse2003**](InlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="catalogProductFind"></a>
# **catalogProductFind**
> List&lt;CatalogProduct&gt; catalogProductFind(filter)

Find all instances of the model matched by filter from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.CatalogProductApi;

CatalogProductApi apiInstance = new CatalogProductApi();
String filter = "filter_example"; // String | Filter defining fields, where, include, order, offset, and limit
try {
    List<CatalogProduct> result = apiInstance.catalogProductFind(filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CatalogProductApi#catalogProductFind");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **String**| Filter defining fields, where, include, order, offset, and limit | [optional]

### Return type

[**List&lt;CatalogProduct&gt;**](CatalogProduct.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="catalogProductFindById"></a>
# **catalogProductFindById**
> CatalogProduct catalogProductFindById(id, filter)

Find a model instance by {{id}} from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.CatalogProductApi;

CatalogProductApi apiInstance = new CatalogProductApi();
String id = "id_example"; // String | Model id
String filter = "filter_example"; // String | Filter defining fields and include
try {
    CatalogProduct result = apiInstance.catalogProductFindById(id, filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CatalogProductApi#catalogProductFindById");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |
 **filter** | **String**| Filter defining fields and include | [optional]

### Return type

[**CatalogProduct**](CatalogProduct.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="catalogProductFindOne"></a>
# **catalogProductFindOne**
> CatalogProduct catalogProductFindOne(filter)

Find first instance of the model matched by filter from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.CatalogProductApi;

CatalogProductApi apiInstance = new CatalogProductApi();
String filter = "filter_example"; // String | Filter defining fields, where, include, order, offset, and limit
try {
    CatalogProduct result = apiInstance.catalogProductFindOne(filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CatalogProductApi#catalogProductFindOne");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **String**| Filter defining fields, where, include, order, offset, and limit | [optional]

### Return type

[**CatalogProduct**](CatalogProduct.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="catalogProductPrototypeCountCatalogCategories"></a>
# **catalogProductPrototypeCountCatalogCategories**
> InlineResponse2001 catalogProductPrototypeCountCatalogCategories(id, where)

Counts catalogCategories of CatalogProduct.

### Example
```java
// Import classes:
//import com.harpoon.api.CatalogProductApi;

CatalogProductApi apiInstance = new CatalogProductApi();
String id = "id_example"; // String | CatalogProduct id
String where = "where_example"; // String | Criteria to match model instances
try {
    InlineResponse2001 result = apiInstance.catalogProductPrototypeCountCatalogCategories(id, where);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CatalogProductApi#catalogProductPrototypeCountCatalogCategories");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| CatalogProduct id |
 **where** | **String**| Criteria to match model instances | [optional]

### Return type

[**InlineResponse2001**](InlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="catalogProductPrototypeCountUdropshipVendorProducts"></a>
# **catalogProductPrototypeCountUdropshipVendorProducts**
> InlineResponse2001 catalogProductPrototypeCountUdropshipVendorProducts(id, where)

Counts udropshipVendorProducts of CatalogProduct.

### Example
```java
// Import classes:
//import com.harpoon.api.CatalogProductApi;

CatalogProductApi apiInstance = new CatalogProductApi();
String id = "id_example"; // String | CatalogProduct id
String where = "where_example"; // String | Criteria to match model instances
try {
    InlineResponse2001 result = apiInstance.catalogProductPrototypeCountUdropshipVendorProducts(id, where);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CatalogProductApi#catalogProductPrototypeCountUdropshipVendorProducts");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| CatalogProduct id |
 **where** | **String**| Criteria to match model instances | [optional]

### Return type

[**InlineResponse2001**](InlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="catalogProductPrototypeCountUdropshipVendors"></a>
# **catalogProductPrototypeCountUdropshipVendors**
> InlineResponse2001 catalogProductPrototypeCountUdropshipVendors(id, where)

Counts udropshipVendors of CatalogProduct.

### Example
```java
// Import classes:
//import com.harpoon.api.CatalogProductApi;

CatalogProductApi apiInstance = new CatalogProductApi();
String id = "id_example"; // String | CatalogProduct id
String where = "where_example"; // String | Criteria to match model instances
try {
    InlineResponse2001 result = apiInstance.catalogProductPrototypeCountUdropshipVendors(id, where);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CatalogProductApi#catalogProductPrototypeCountUdropshipVendors");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| CatalogProduct id |
 **where** | **String**| Criteria to match model instances | [optional]

### Return type

[**InlineResponse2001**](InlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="catalogProductPrototypeCreateAwCollpurDeal"></a>
# **catalogProductPrototypeCreateAwCollpurDeal**
> AwCollpurDeal catalogProductPrototypeCreateAwCollpurDeal(id, data)

Creates a new instance in awCollpurDeal of this model.

### Example
```java
// Import classes:
//import com.harpoon.api.CatalogProductApi;

CatalogProductApi apiInstance = new CatalogProductApi();
String id = "id_example"; // String | CatalogProduct id
AwCollpurDeal data = new AwCollpurDeal(); // AwCollpurDeal | 
try {
    AwCollpurDeal result = apiInstance.catalogProductPrototypeCreateAwCollpurDeal(id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CatalogProductApi#catalogProductPrototypeCreateAwCollpurDeal");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| CatalogProduct id |
 **data** | [**AwCollpurDeal**](AwCollpurDeal.md)|  | [optional]

### Return type

[**AwCollpurDeal**](AwCollpurDeal.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="catalogProductPrototypeCreateCatalogCategories"></a>
# **catalogProductPrototypeCreateCatalogCategories**
> CatalogCategory catalogProductPrototypeCreateCatalogCategories(id, data)

Creates a new instance in catalogCategories of this model.

### Example
```java
// Import classes:
//import com.harpoon.api.CatalogProductApi;

CatalogProductApi apiInstance = new CatalogProductApi();
String id = "id_example"; // String | CatalogProduct id
CatalogCategory data = new CatalogCategory(); // CatalogCategory | 
try {
    CatalogCategory result = apiInstance.catalogProductPrototypeCreateCatalogCategories(id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CatalogProductApi#catalogProductPrototypeCreateCatalogCategories");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| CatalogProduct id |
 **data** | [**CatalogCategory**](CatalogCategory.md)|  | [optional]

### Return type

[**CatalogCategory**](CatalogCategory.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="catalogProductPrototypeCreateInventory"></a>
# **catalogProductPrototypeCreateInventory**
> CatalogInventoryStockItem catalogProductPrototypeCreateInventory(id, data)

Creates a new instance in inventory of this model.

### Example
```java
// Import classes:
//import com.harpoon.api.CatalogProductApi;

CatalogProductApi apiInstance = new CatalogProductApi();
String id = "id_example"; // String | CatalogProduct id
CatalogInventoryStockItem data = new CatalogInventoryStockItem(); // CatalogInventoryStockItem | 
try {
    CatalogInventoryStockItem result = apiInstance.catalogProductPrototypeCreateInventory(id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CatalogProductApi#catalogProductPrototypeCreateInventory");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| CatalogProduct id |
 **data** | [**CatalogInventoryStockItem**](CatalogInventoryStockItem.md)|  | [optional]

### Return type

[**CatalogInventoryStockItem**](CatalogInventoryStockItem.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="catalogProductPrototypeCreateProductEventCompetition"></a>
# **catalogProductPrototypeCreateProductEventCompetition**
> AwEventbookingEvent catalogProductPrototypeCreateProductEventCompetition(id, data)

Creates a new instance in productEventCompetition of this model.

### Example
```java
// Import classes:
//import com.harpoon.api.CatalogProductApi;

CatalogProductApi apiInstance = new CatalogProductApi();
String id = "id_example"; // String | CatalogProduct id
AwEventbookingEvent data = new AwEventbookingEvent(); // AwEventbookingEvent | 
try {
    AwEventbookingEvent result = apiInstance.catalogProductPrototypeCreateProductEventCompetition(id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CatalogProductApi#catalogProductPrototypeCreateProductEventCompetition");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| CatalogProduct id |
 **data** | [**AwEventbookingEvent**](AwEventbookingEvent.md)|  | [optional]

### Return type

[**AwEventbookingEvent**](AwEventbookingEvent.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="catalogProductPrototypeCreateUdropshipVendorProducts"></a>
# **catalogProductPrototypeCreateUdropshipVendorProducts**
> UdropshipVendorProduct catalogProductPrototypeCreateUdropshipVendorProducts(id, data)

Creates a new instance in udropshipVendorProducts of this model.

### Example
```java
// Import classes:
//import com.harpoon.api.CatalogProductApi;

CatalogProductApi apiInstance = new CatalogProductApi();
String id = "id_example"; // String | CatalogProduct id
UdropshipVendorProduct data = new UdropshipVendorProduct(); // UdropshipVendorProduct | 
try {
    UdropshipVendorProduct result = apiInstance.catalogProductPrototypeCreateUdropshipVendorProducts(id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CatalogProductApi#catalogProductPrototypeCreateUdropshipVendorProducts");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| CatalogProduct id |
 **data** | [**UdropshipVendorProduct**](UdropshipVendorProduct.md)|  | [optional]

### Return type

[**UdropshipVendorProduct**](UdropshipVendorProduct.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="catalogProductPrototypeCreateUdropshipVendors"></a>
# **catalogProductPrototypeCreateUdropshipVendors**
> UdropshipVendor catalogProductPrototypeCreateUdropshipVendors(id, data)

Creates a new instance in udropshipVendors of this model.

### Example
```java
// Import classes:
//import com.harpoon.api.CatalogProductApi;

CatalogProductApi apiInstance = new CatalogProductApi();
String id = "id_example"; // String | CatalogProduct id
UdropshipVendor data = new UdropshipVendor(); // UdropshipVendor | 
try {
    UdropshipVendor result = apiInstance.catalogProductPrototypeCreateUdropshipVendors(id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CatalogProductApi#catalogProductPrototypeCreateUdropshipVendors");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| CatalogProduct id |
 **data** | [**UdropshipVendor**](UdropshipVendor.md)|  | [optional]

### Return type

[**UdropshipVendor**](UdropshipVendor.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="catalogProductPrototypeDeleteCatalogCategories"></a>
# **catalogProductPrototypeDeleteCatalogCategories**
> catalogProductPrototypeDeleteCatalogCategories(id)

Deletes all catalogCategories of this model.

### Example
```java
// Import classes:
//import com.harpoon.api.CatalogProductApi;

CatalogProductApi apiInstance = new CatalogProductApi();
String id = "id_example"; // String | CatalogProduct id
try {
    apiInstance.catalogProductPrototypeDeleteCatalogCategories(id);
} catch (ApiException e) {
    System.err.println("Exception when calling CatalogProductApi#catalogProductPrototypeDeleteCatalogCategories");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| CatalogProduct id |

### Return type

null (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="catalogProductPrototypeDeleteUdropshipVendorProducts"></a>
# **catalogProductPrototypeDeleteUdropshipVendorProducts**
> catalogProductPrototypeDeleteUdropshipVendorProducts(id)

Deletes all udropshipVendorProducts of this model.

### Example
```java
// Import classes:
//import com.harpoon.api.CatalogProductApi;

CatalogProductApi apiInstance = new CatalogProductApi();
String id = "id_example"; // String | CatalogProduct id
try {
    apiInstance.catalogProductPrototypeDeleteUdropshipVendorProducts(id);
} catch (ApiException e) {
    System.err.println("Exception when calling CatalogProductApi#catalogProductPrototypeDeleteUdropshipVendorProducts");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| CatalogProduct id |

### Return type

null (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="catalogProductPrototypeDeleteUdropshipVendors"></a>
# **catalogProductPrototypeDeleteUdropshipVendors**
> catalogProductPrototypeDeleteUdropshipVendors(id)

Deletes all udropshipVendors of this model.

### Example
```java
// Import classes:
//import com.harpoon.api.CatalogProductApi;

CatalogProductApi apiInstance = new CatalogProductApi();
String id = "id_example"; // String | CatalogProduct id
try {
    apiInstance.catalogProductPrototypeDeleteUdropshipVendors(id);
} catch (ApiException e) {
    System.err.println("Exception when calling CatalogProductApi#catalogProductPrototypeDeleteUdropshipVendors");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| CatalogProduct id |

### Return type

null (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="catalogProductPrototypeDestroyAwCollpurDeal"></a>
# **catalogProductPrototypeDestroyAwCollpurDeal**
> catalogProductPrototypeDestroyAwCollpurDeal(id)

Deletes awCollpurDeal of this model.

### Example
```java
// Import classes:
//import com.harpoon.api.CatalogProductApi;

CatalogProductApi apiInstance = new CatalogProductApi();
String id = "id_example"; // String | CatalogProduct id
try {
    apiInstance.catalogProductPrototypeDestroyAwCollpurDeal(id);
} catch (ApiException e) {
    System.err.println("Exception when calling CatalogProductApi#catalogProductPrototypeDestroyAwCollpurDeal");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| CatalogProduct id |

### Return type

null (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="catalogProductPrototypeDestroyByIdCatalogCategories"></a>
# **catalogProductPrototypeDestroyByIdCatalogCategories**
> catalogProductPrototypeDestroyByIdCatalogCategories(fk, id)

Delete a related item by id for catalogCategories.

### Example
```java
// Import classes:
//import com.harpoon.api.CatalogProductApi;

CatalogProductApi apiInstance = new CatalogProductApi();
String fk = "fk_example"; // String | Foreign key for catalogCategories
String id = "id_example"; // String | CatalogProduct id
try {
    apiInstance.catalogProductPrototypeDestroyByIdCatalogCategories(fk, id);
} catch (ApiException e) {
    System.err.println("Exception when calling CatalogProductApi#catalogProductPrototypeDestroyByIdCatalogCategories");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for catalogCategories |
 **id** | **String**| CatalogProduct id |

### Return type

null (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="catalogProductPrototypeDestroyByIdUdropshipVendorProducts"></a>
# **catalogProductPrototypeDestroyByIdUdropshipVendorProducts**
> catalogProductPrototypeDestroyByIdUdropshipVendorProducts(fk, id)

Delete a related item by id for udropshipVendorProducts.

### Example
```java
// Import classes:
//import com.harpoon.api.CatalogProductApi;

CatalogProductApi apiInstance = new CatalogProductApi();
String fk = "fk_example"; // String | Foreign key for udropshipVendorProducts
String id = "id_example"; // String | CatalogProduct id
try {
    apiInstance.catalogProductPrototypeDestroyByIdUdropshipVendorProducts(fk, id);
} catch (ApiException e) {
    System.err.println("Exception when calling CatalogProductApi#catalogProductPrototypeDestroyByIdUdropshipVendorProducts");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for udropshipVendorProducts |
 **id** | **String**| CatalogProduct id |

### Return type

null (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="catalogProductPrototypeDestroyByIdUdropshipVendors"></a>
# **catalogProductPrototypeDestroyByIdUdropshipVendors**
> catalogProductPrototypeDestroyByIdUdropshipVendors(fk, id)

Delete a related item by id for udropshipVendors.

### Example
```java
// Import classes:
//import com.harpoon.api.CatalogProductApi;

CatalogProductApi apiInstance = new CatalogProductApi();
String fk = "fk_example"; // String | Foreign key for udropshipVendors
String id = "id_example"; // String | CatalogProduct id
try {
    apiInstance.catalogProductPrototypeDestroyByIdUdropshipVendors(fk, id);
} catch (ApiException e) {
    System.err.println("Exception when calling CatalogProductApi#catalogProductPrototypeDestroyByIdUdropshipVendors");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for udropshipVendors |
 **id** | **String**| CatalogProduct id |

### Return type

null (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="catalogProductPrototypeDestroyInventory"></a>
# **catalogProductPrototypeDestroyInventory**
> catalogProductPrototypeDestroyInventory(id)

Deletes inventory of this model.

### Example
```java
// Import classes:
//import com.harpoon.api.CatalogProductApi;

CatalogProductApi apiInstance = new CatalogProductApi();
String id = "id_example"; // String | CatalogProduct id
try {
    apiInstance.catalogProductPrototypeDestroyInventory(id);
} catch (ApiException e) {
    System.err.println("Exception when calling CatalogProductApi#catalogProductPrototypeDestroyInventory");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| CatalogProduct id |

### Return type

null (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="catalogProductPrototypeDestroyProductEventCompetition"></a>
# **catalogProductPrototypeDestroyProductEventCompetition**
> catalogProductPrototypeDestroyProductEventCompetition(id)

Deletes productEventCompetition of this model.

### Example
```java
// Import classes:
//import com.harpoon.api.CatalogProductApi;

CatalogProductApi apiInstance = new CatalogProductApi();
String id = "id_example"; // String | CatalogProduct id
try {
    apiInstance.catalogProductPrototypeDestroyProductEventCompetition(id);
} catch (ApiException e) {
    System.err.println("Exception when calling CatalogProductApi#catalogProductPrototypeDestroyProductEventCompetition");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| CatalogProduct id |

### Return type

null (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="catalogProductPrototypeExistsCatalogCategories"></a>
# **catalogProductPrototypeExistsCatalogCategories**
> Boolean catalogProductPrototypeExistsCatalogCategories(fk, id)

Check the existence of catalogCategories relation to an item by id.

### Example
```java
// Import classes:
//import com.harpoon.api.CatalogProductApi;

CatalogProductApi apiInstance = new CatalogProductApi();
String fk = "fk_example"; // String | Foreign key for catalogCategories
String id = "id_example"; // String | CatalogProduct id
try {
    Boolean result = apiInstance.catalogProductPrototypeExistsCatalogCategories(fk, id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CatalogProductApi#catalogProductPrototypeExistsCatalogCategories");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for catalogCategories |
 **id** | **String**| CatalogProduct id |

### Return type

**Boolean**

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="catalogProductPrototypeExistsUdropshipVendors"></a>
# **catalogProductPrototypeExistsUdropshipVendors**
> Boolean catalogProductPrototypeExistsUdropshipVendors(fk, id)

Check the existence of udropshipVendors relation to an item by id.

### Example
```java
// Import classes:
//import com.harpoon.api.CatalogProductApi;

CatalogProductApi apiInstance = new CatalogProductApi();
String fk = "fk_example"; // String | Foreign key for udropshipVendors
String id = "id_example"; // String | CatalogProduct id
try {
    Boolean result = apiInstance.catalogProductPrototypeExistsUdropshipVendors(fk, id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CatalogProductApi#catalogProductPrototypeExistsUdropshipVendors");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for udropshipVendors |
 **id** | **String**| CatalogProduct id |

### Return type

**Boolean**

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="catalogProductPrototypeFindByIdCatalogCategories"></a>
# **catalogProductPrototypeFindByIdCatalogCategories**
> CatalogCategory catalogProductPrototypeFindByIdCatalogCategories(fk, id)

Find a related item by id for catalogCategories.

### Example
```java
// Import classes:
//import com.harpoon.api.CatalogProductApi;

CatalogProductApi apiInstance = new CatalogProductApi();
String fk = "fk_example"; // String | Foreign key for catalogCategories
String id = "id_example"; // String | CatalogProduct id
try {
    CatalogCategory result = apiInstance.catalogProductPrototypeFindByIdCatalogCategories(fk, id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CatalogProductApi#catalogProductPrototypeFindByIdCatalogCategories");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for catalogCategories |
 **id** | **String**| CatalogProduct id |

### Return type

[**CatalogCategory**](CatalogCategory.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="catalogProductPrototypeFindByIdUdropshipVendorProducts"></a>
# **catalogProductPrototypeFindByIdUdropshipVendorProducts**
> UdropshipVendorProduct catalogProductPrototypeFindByIdUdropshipVendorProducts(fk, id)

Find a related item by id for udropshipVendorProducts.

### Example
```java
// Import classes:
//import com.harpoon.api.CatalogProductApi;

CatalogProductApi apiInstance = new CatalogProductApi();
String fk = "fk_example"; // String | Foreign key for udropshipVendorProducts
String id = "id_example"; // String | CatalogProduct id
try {
    UdropshipVendorProduct result = apiInstance.catalogProductPrototypeFindByIdUdropshipVendorProducts(fk, id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CatalogProductApi#catalogProductPrototypeFindByIdUdropshipVendorProducts");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for udropshipVendorProducts |
 **id** | **String**| CatalogProduct id |

### Return type

[**UdropshipVendorProduct**](UdropshipVendorProduct.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="catalogProductPrototypeFindByIdUdropshipVendors"></a>
# **catalogProductPrototypeFindByIdUdropshipVendors**
> UdropshipVendor catalogProductPrototypeFindByIdUdropshipVendors(fk, id)

Find a related item by id for udropshipVendors.

### Example
```java
// Import classes:
//import com.harpoon.api.CatalogProductApi;

CatalogProductApi apiInstance = new CatalogProductApi();
String fk = "fk_example"; // String | Foreign key for udropshipVendors
String id = "id_example"; // String | CatalogProduct id
try {
    UdropshipVendor result = apiInstance.catalogProductPrototypeFindByIdUdropshipVendors(fk, id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CatalogProductApi#catalogProductPrototypeFindByIdUdropshipVendors");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for udropshipVendors |
 **id** | **String**| CatalogProduct id |

### Return type

[**UdropshipVendor**](UdropshipVendor.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="catalogProductPrototypeGetAwCollpurDeal"></a>
# **catalogProductPrototypeGetAwCollpurDeal**
> AwCollpurDeal catalogProductPrototypeGetAwCollpurDeal(id, refresh)

Fetches hasOne relation awCollpurDeal.

### Example
```java
// Import classes:
//import com.harpoon.api.CatalogProductApi;

CatalogProductApi apiInstance = new CatalogProductApi();
String id = "id_example"; // String | CatalogProduct id
Boolean refresh = true; // Boolean | 
try {
    AwCollpurDeal result = apiInstance.catalogProductPrototypeGetAwCollpurDeal(id, refresh);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CatalogProductApi#catalogProductPrototypeGetAwCollpurDeal");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| CatalogProduct id |
 **refresh** | **Boolean**|  | [optional]

### Return type

[**AwCollpurDeal**](AwCollpurDeal.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="catalogProductPrototypeGetCatalogCategories"></a>
# **catalogProductPrototypeGetCatalogCategories**
> List&lt;CatalogCategory&gt; catalogProductPrototypeGetCatalogCategories(id, filter)

Queries catalogCategories of CatalogProduct.

### Example
```java
// Import classes:
//import com.harpoon.api.CatalogProductApi;

CatalogProductApi apiInstance = new CatalogProductApi();
String id = "id_example"; // String | CatalogProduct id
String filter = "filter_example"; // String | 
try {
    List<CatalogCategory> result = apiInstance.catalogProductPrototypeGetCatalogCategories(id, filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CatalogProductApi#catalogProductPrototypeGetCatalogCategories");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| CatalogProduct id |
 **filter** | **String**|  | [optional]

### Return type

[**List&lt;CatalogCategory&gt;**](CatalogCategory.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="catalogProductPrototypeGetCheckoutAgreement"></a>
# **catalogProductPrototypeGetCheckoutAgreement**
> CheckoutAgreement catalogProductPrototypeGetCheckoutAgreement(id, refresh)

Fetches belongsTo relation checkoutAgreement.

### Example
```java
// Import classes:
//import com.harpoon.api.CatalogProductApi;

CatalogProductApi apiInstance = new CatalogProductApi();
String id = "id_example"; // String | CatalogProduct id
Boolean refresh = true; // Boolean | 
try {
    CheckoutAgreement result = apiInstance.catalogProductPrototypeGetCheckoutAgreement(id, refresh);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CatalogProductApi#catalogProductPrototypeGetCheckoutAgreement");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| CatalogProduct id |
 **refresh** | **Boolean**|  | [optional]

### Return type

[**CheckoutAgreement**](CheckoutAgreement.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="catalogProductPrototypeGetCreator"></a>
# **catalogProductPrototypeGetCreator**
> UdropshipVendor catalogProductPrototypeGetCreator(id, refresh)

Fetches belongsTo relation creator.

### Example
```java
// Import classes:
//import com.harpoon.api.CatalogProductApi;

CatalogProductApi apiInstance = new CatalogProductApi();
String id = "id_example"; // String | CatalogProduct id
Boolean refresh = true; // Boolean | 
try {
    UdropshipVendor result = apiInstance.catalogProductPrototypeGetCreator(id, refresh);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CatalogProductApi#catalogProductPrototypeGetCreator");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| CatalogProduct id |
 **refresh** | **Boolean**|  | [optional]

### Return type

[**UdropshipVendor**](UdropshipVendor.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="catalogProductPrototypeGetInventory"></a>
# **catalogProductPrototypeGetInventory**
> CatalogInventoryStockItem catalogProductPrototypeGetInventory(id, refresh)

Fetches hasOne relation inventory.

### Example
```java
// Import classes:
//import com.harpoon.api.CatalogProductApi;

CatalogProductApi apiInstance = new CatalogProductApi();
String id = "id_example"; // String | CatalogProduct id
Boolean refresh = true; // Boolean | 
try {
    CatalogInventoryStockItem result = apiInstance.catalogProductPrototypeGetInventory(id, refresh);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CatalogProductApi#catalogProductPrototypeGetInventory");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| CatalogProduct id |
 **refresh** | **Boolean**|  | [optional]

### Return type

[**CatalogInventoryStockItem**](CatalogInventoryStockItem.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="catalogProductPrototypeGetProductEventCompetition"></a>
# **catalogProductPrototypeGetProductEventCompetition**
> AwEventbookingEvent catalogProductPrototypeGetProductEventCompetition(id, refresh)

Fetches hasOne relation productEventCompetition.

### Example
```java
// Import classes:
//import com.harpoon.api.CatalogProductApi;

CatalogProductApi apiInstance = new CatalogProductApi();
String id = "id_example"; // String | CatalogProduct id
Boolean refresh = true; // Boolean | 
try {
    AwEventbookingEvent result = apiInstance.catalogProductPrototypeGetProductEventCompetition(id, refresh);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CatalogProductApi#catalogProductPrototypeGetProductEventCompetition");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| CatalogProduct id |
 **refresh** | **Boolean**|  | [optional]

### Return type

[**AwEventbookingEvent**](AwEventbookingEvent.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="catalogProductPrototypeGetUdropshipVendorProducts"></a>
# **catalogProductPrototypeGetUdropshipVendorProducts**
> List&lt;UdropshipVendorProduct&gt; catalogProductPrototypeGetUdropshipVendorProducts(id, filter)

Queries udropshipVendorProducts of CatalogProduct.

### Example
```java
// Import classes:
//import com.harpoon.api.CatalogProductApi;

CatalogProductApi apiInstance = new CatalogProductApi();
String id = "id_example"; // String | CatalogProduct id
String filter = "filter_example"; // String | 
try {
    List<UdropshipVendorProduct> result = apiInstance.catalogProductPrototypeGetUdropshipVendorProducts(id, filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CatalogProductApi#catalogProductPrototypeGetUdropshipVendorProducts");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| CatalogProduct id |
 **filter** | **String**|  | [optional]

### Return type

[**List&lt;UdropshipVendorProduct&gt;**](UdropshipVendorProduct.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="catalogProductPrototypeGetUdropshipVendors"></a>
# **catalogProductPrototypeGetUdropshipVendors**
> List&lt;UdropshipVendor&gt; catalogProductPrototypeGetUdropshipVendors(id, filter)

Queries udropshipVendors of CatalogProduct.

### Example
```java
// Import classes:
//import com.harpoon.api.CatalogProductApi;

CatalogProductApi apiInstance = new CatalogProductApi();
String id = "id_example"; // String | CatalogProduct id
String filter = "filter_example"; // String | 
try {
    List<UdropshipVendor> result = apiInstance.catalogProductPrototypeGetUdropshipVendors(id, filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CatalogProductApi#catalogProductPrototypeGetUdropshipVendors");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| CatalogProduct id |
 **filter** | **String**|  | [optional]

### Return type

[**List&lt;UdropshipVendor&gt;**](UdropshipVendor.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="catalogProductPrototypeLinkCatalogCategories"></a>
# **catalogProductPrototypeLinkCatalogCategories**
> CatalogCategoryProduct catalogProductPrototypeLinkCatalogCategories(fk, id, data)

Add a related item by id for catalogCategories.

### Example
```java
// Import classes:
//import com.harpoon.api.CatalogProductApi;

CatalogProductApi apiInstance = new CatalogProductApi();
String fk = "fk_example"; // String | Foreign key for catalogCategories
String id = "id_example"; // String | CatalogProduct id
CatalogCategoryProduct data = new CatalogCategoryProduct(); // CatalogCategoryProduct | 
try {
    CatalogCategoryProduct result = apiInstance.catalogProductPrototypeLinkCatalogCategories(fk, id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CatalogProductApi#catalogProductPrototypeLinkCatalogCategories");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for catalogCategories |
 **id** | **String**| CatalogProduct id |
 **data** | [**CatalogCategoryProduct**](CatalogCategoryProduct.md)|  | [optional]

### Return type

[**CatalogCategoryProduct**](CatalogCategoryProduct.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="catalogProductPrototypeLinkUdropshipVendors"></a>
# **catalogProductPrototypeLinkUdropshipVendors**
> UdropshipVendorProductAssoc catalogProductPrototypeLinkUdropshipVendors(fk, id, data)

Add a related item by id for udropshipVendors.

### Example
```java
// Import classes:
//import com.harpoon.api.CatalogProductApi;

CatalogProductApi apiInstance = new CatalogProductApi();
String fk = "fk_example"; // String | Foreign key for udropshipVendors
String id = "id_example"; // String | CatalogProduct id
UdropshipVendorProductAssoc data = new UdropshipVendorProductAssoc(); // UdropshipVendorProductAssoc | 
try {
    UdropshipVendorProductAssoc result = apiInstance.catalogProductPrototypeLinkUdropshipVendors(fk, id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CatalogProductApi#catalogProductPrototypeLinkUdropshipVendors");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for udropshipVendors |
 **id** | **String**| CatalogProduct id |
 **data** | [**UdropshipVendorProductAssoc**](UdropshipVendorProductAssoc.md)|  | [optional]

### Return type

[**UdropshipVendorProductAssoc**](UdropshipVendorProductAssoc.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="catalogProductPrototypeUnlinkCatalogCategories"></a>
# **catalogProductPrototypeUnlinkCatalogCategories**
> catalogProductPrototypeUnlinkCatalogCategories(fk, id)

Remove the catalogCategories relation to an item by id.

### Example
```java
// Import classes:
//import com.harpoon.api.CatalogProductApi;

CatalogProductApi apiInstance = new CatalogProductApi();
String fk = "fk_example"; // String | Foreign key for catalogCategories
String id = "id_example"; // String | CatalogProduct id
try {
    apiInstance.catalogProductPrototypeUnlinkCatalogCategories(fk, id);
} catch (ApiException e) {
    System.err.println("Exception when calling CatalogProductApi#catalogProductPrototypeUnlinkCatalogCategories");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for catalogCategories |
 **id** | **String**| CatalogProduct id |

### Return type

null (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="catalogProductPrototypeUnlinkUdropshipVendors"></a>
# **catalogProductPrototypeUnlinkUdropshipVendors**
> catalogProductPrototypeUnlinkUdropshipVendors(fk, id)

Remove the udropshipVendors relation to an item by id.

### Example
```java
// Import classes:
//import com.harpoon.api.CatalogProductApi;

CatalogProductApi apiInstance = new CatalogProductApi();
String fk = "fk_example"; // String | Foreign key for udropshipVendors
String id = "id_example"; // String | CatalogProduct id
try {
    apiInstance.catalogProductPrototypeUnlinkUdropshipVendors(fk, id);
} catch (ApiException e) {
    System.err.println("Exception when calling CatalogProductApi#catalogProductPrototypeUnlinkUdropshipVendors");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for udropshipVendors |
 **id** | **String**| CatalogProduct id |

### Return type

null (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="catalogProductPrototypeUpdateAttributesPatchCatalogProductsid"></a>
# **catalogProductPrototypeUpdateAttributesPatchCatalogProductsid**
> CatalogProduct catalogProductPrototypeUpdateAttributesPatchCatalogProductsid(id, data)

Patch attributes for a model instance and persist it into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.CatalogProductApi;

CatalogProductApi apiInstance = new CatalogProductApi();
String id = "id_example"; // String | CatalogProduct id
CatalogProduct data = new CatalogProduct(); // CatalogProduct | An object of model property name/value pairs
try {
    CatalogProduct result = apiInstance.catalogProductPrototypeUpdateAttributesPatchCatalogProductsid(id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CatalogProductApi#catalogProductPrototypeUpdateAttributesPatchCatalogProductsid");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| CatalogProduct id |
 **data** | [**CatalogProduct**](CatalogProduct.md)| An object of model property name/value pairs | [optional]

### Return type

[**CatalogProduct**](CatalogProduct.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="catalogProductPrototypeUpdateAttributesPutCatalogProductsid"></a>
# **catalogProductPrototypeUpdateAttributesPutCatalogProductsid**
> CatalogProduct catalogProductPrototypeUpdateAttributesPutCatalogProductsid(id, data)

Patch attributes for a model instance and persist it into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.CatalogProductApi;

CatalogProductApi apiInstance = new CatalogProductApi();
String id = "id_example"; // String | CatalogProduct id
CatalogProduct data = new CatalogProduct(); // CatalogProduct | An object of model property name/value pairs
try {
    CatalogProduct result = apiInstance.catalogProductPrototypeUpdateAttributesPutCatalogProductsid(id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CatalogProductApi#catalogProductPrototypeUpdateAttributesPutCatalogProductsid");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| CatalogProduct id |
 **data** | [**CatalogProduct**](CatalogProduct.md)| An object of model property name/value pairs | [optional]

### Return type

[**CatalogProduct**](CatalogProduct.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="catalogProductPrototypeUpdateAwCollpurDeal"></a>
# **catalogProductPrototypeUpdateAwCollpurDeal**
> AwCollpurDeal catalogProductPrototypeUpdateAwCollpurDeal(id, data)

Update awCollpurDeal of this model.

### Example
```java
// Import classes:
//import com.harpoon.api.CatalogProductApi;

CatalogProductApi apiInstance = new CatalogProductApi();
String id = "id_example"; // String | CatalogProduct id
AwCollpurDeal data = new AwCollpurDeal(); // AwCollpurDeal | 
try {
    AwCollpurDeal result = apiInstance.catalogProductPrototypeUpdateAwCollpurDeal(id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CatalogProductApi#catalogProductPrototypeUpdateAwCollpurDeal");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| CatalogProduct id |
 **data** | [**AwCollpurDeal**](AwCollpurDeal.md)|  | [optional]

### Return type

[**AwCollpurDeal**](AwCollpurDeal.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="catalogProductPrototypeUpdateByIdCatalogCategories"></a>
# **catalogProductPrototypeUpdateByIdCatalogCategories**
> CatalogCategory catalogProductPrototypeUpdateByIdCatalogCategories(fk, id, data)

Update a related item by id for catalogCategories.

### Example
```java
// Import classes:
//import com.harpoon.api.CatalogProductApi;

CatalogProductApi apiInstance = new CatalogProductApi();
String fk = "fk_example"; // String | Foreign key for catalogCategories
String id = "id_example"; // String | CatalogProduct id
CatalogCategory data = new CatalogCategory(); // CatalogCategory | 
try {
    CatalogCategory result = apiInstance.catalogProductPrototypeUpdateByIdCatalogCategories(fk, id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CatalogProductApi#catalogProductPrototypeUpdateByIdCatalogCategories");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for catalogCategories |
 **id** | **String**| CatalogProduct id |
 **data** | [**CatalogCategory**](CatalogCategory.md)|  | [optional]

### Return type

[**CatalogCategory**](CatalogCategory.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="catalogProductPrototypeUpdateByIdUdropshipVendorProducts"></a>
# **catalogProductPrototypeUpdateByIdUdropshipVendorProducts**
> UdropshipVendorProduct catalogProductPrototypeUpdateByIdUdropshipVendorProducts(fk, id, data)

Update a related item by id for udropshipVendorProducts.

### Example
```java
// Import classes:
//import com.harpoon.api.CatalogProductApi;

CatalogProductApi apiInstance = new CatalogProductApi();
String fk = "fk_example"; // String | Foreign key for udropshipVendorProducts
String id = "id_example"; // String | CatalogProduct id
UdropshipVendorProduct data = new UdropshipVendorProduct(); // UdropshipVendorProduct | 
try {
    UdropshipVendorProduct result = apiInstance.catalogProductPrototypeUpdateByIdUdropshipVendorProducts(fk, id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CatalogProductApi#catalogProductPrototypeUpdateByIdUdropshipVendorProducts");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for udropshipVendorProducts |
 **id** | **String**| CatalogProduct id |
 **data** | [**UdropshipVendorProduct**](UdropshipVendorProduct.md)|  | [optional]

### Return type

[**UdropshipVendorProduct**](UdropshipVendorProduct.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="catalogProductPrototypeUpdateByIdUdropshipVendors"></a>
# **catalogProductPrototypeUpdateByIdUdropshipVendors**
> UdropshipVendor catalogProductPrototypeUpdateByIdUdropshipVendors(fk, id, data)

Update a related item by id for udropshipVendors.

### Example
```java
// Import classes:
//import com.harpoon.api.CatalogProductApi;

CatalogProductApi apiInstance = new CatalogProductApi();
String fk = "fk_example"; // String | Foreign key for udropshipVendors
String id = "id_example"; // String | CatalogProduct id
UdropshipVendor data = new UdropshipVendor(); // UdropshipVendor | 
try {
    UdropshipVendor result = apiInstance.catalogProductPrototypeUpdateByIdUdropshipVendors(fk, id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CatalogProductApi#catalogProductPrototypeUpdateByIdUdropshipVendors");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for udropshipVendors |
 **id** | **String**| CatalogProduct id |
 **data** | [**UdropshipVendor**](UdropshipVendor.md)|  | [optional]

### Return type

[**UdropshipVendor**](UdropshipVendor.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="catalogProductPrototypeUpdateInventory"></a>
# **catalogProductPrototypeUpdateInventory**
> CatalogInventoryStockItem catalogProductPrototypeUpdateInventory(id, data)

Update inventory of this model.

### Example
```java
// Import classes:
//import com.harpoon.api.CatalogProductApi;

CatalogProductApi apiInstance = new CatalogProductApi();
String id = "id_example"; // String | CatalogProduct id
CatalogInventoryStockItem data = new CatalogInventoryStockItem(); // CatalogInventoryStockItem | 
try {
    CatalogInventoryStockItem result = apiInstance.catalogProductPrototypeUpdateInventory(id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CatalogProductApi#catalogProductPrototypeUpdateInventory");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| CatalogProduct id |
 **data** | [**CatalogInventoryStockItem**](CatalogInventoryStockItem.md)|  | [optional]

### Return type

[**CatalogInventoryStockItem**](CatalogInventoryStockItem.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="catalogProductPrototypeUpdateProductEventCompetition"></a>
# **catalogProductPrototypeUpdateProductEventCompetition**
> AwEventbookingEvent catalogProductPrototypeUpdateProductEventCompetition(id, data)

Update productEventCompetition of this model.

### Example
```java
// Import classes:
//import com.harpoon.api.CatalogProductApi;

CatalogProductApi apiInstance = new CatalogProductApi();
String id = "id_example"; // String | CatalogProduct id
AwEventbookingEvent data = new AwEventbookingEvent(); // AwEventbookingEvent | 
try {
    AwEventbookingEvent result = apiInstance.catalogProductPrototypeUpdateProductEventCompetition(id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CatalogProductApi#catalogProductPrototypeUpdateProductEventCompetition");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| CatalogProduct id |
 **data** | [**AwEventbookingEvent**](AwEventbookingEvent.md)|  | [optional]

### Return type

[**AwEventbookingEvent**](AwEventbookingEvent.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="catalogProductReplaceById"></a>
# **catalogProductReplaceById**
> CatalogProduct catalogProductReplaceById(id, data)

Replace attributes for a model instance and persist it into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.CatalogProductApi;

CatalogProductApi apiInstance = new CatalogProductApi();
String id = "id_example"; // String | Model id
CatalogProduct data = new CatalogProduct(); // CatalogProduct | Model instance data
try {
    CatalogProduct result = apiInstance.catalogProductReplaceById(id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CatalogProductApi#catalogProductReplaceById");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |
 **data** | [**CatalogProduct**](CatalogProduct.md)| Model instance data | [optional]

### Return type

[**CatalogProduct**](CatalogProduct.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="catalogProductReplaceOrCreate"></a>
# **catalogProductReplaceOrCreate**
> CatalogProduct catalogProductReplaceOrCreate(data)

Replace an existing model instance or insert a new one into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.CatalogProductApi;

CatalogProductApi apiInstance = new CatalogProductApi();
CatalogProduct data = new CatalogProduct(); // CatalogProduct | Model instance data
try {
    CatalogProduct result = apiInstance.catalogProductReplaceOrCreate(data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CatalogProductApi#catalogProductReplaceOrCreate");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**CatalogProduct**](CatalogProduct.md)| Model instance data | [optional]

### Return type

[**CatalogProduct**](CatalogProduct.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="catalogProductUpdateAll"></a>
# **catalogProductUpdateAll**
> InlineResponse2002 catalogProductUpdateAll(where, data)

Update instances of the model matched by {{where}} from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.CatalogProductApi;

CatalogProductApi apiInstance = new CatalogProductApi();
String where = "where_example"; // String | Criteria to match model instances
CatalogProduct data = new CatalogProduct(); // CatalogProduct | An object of model property name/value pairs
try {
    InlineResponse2002 result = apiInstance.catalogProductUpdateAll(where, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CatalogProductApi#catalogProductUpdateAll");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional]
 **data** | [**CatalogProduct**](CatalogProduct.md)| An object of model property name/value pairs | [optional]

### Return type

[**InlineResponse2002**](InlineResponse2002.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="catalogProductUpsertPatchCatalogProducts"></a>
# **catalogProductUpsertPatchCatalogProducts**
> CatalogProduct catalogProductUpsertPatchCatalogProducts(data)

Patch an existing model instance or insert a new one into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.CatalogProductApi;

CatalogProductApi apiInstance = new CatalogProductApi();
CatalogProduct data = new CatalogProduct(); // CatalogProduct | Model instance data
try {
    CatalogProduct result = apiInstance.catalogProductUpsertPatchCatalogProducts(data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CatalogProductApi#catalogProductUpsertPatchCatalogProducts");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**CatalogProduct**](CatalogProduct.md)| Model instance data | [optional]

### Return type

[**CatalogProduct**](CatalogProduct.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="catalogProductUpsertPutCatalogProducts"></a>
# **catalogProductUpsertPutCatalogProducts**
> CatalogProduct catalogProductUpsertPutCatalogProducts(data)

Patch an existing model instance or insert a new one into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.CatalogProductApi;

CatalogProductApi apiInstance = new CatalogProductApi();
CatalogProduct data = new CatalogProduct(); // CatalogProduct | Model instance data
try {
    CatalogProduct result = apiInstance.catalogProductUpsertPutCatalogProducts(data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CatalogProductApi#catalogProductUpsertPutCatalogProducts");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**CatalogProduct**](CatalogProduct.md)| Model instance data | [optional]

### Return type

[**CatalogProduct**](CatalogProduct.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="catalogProductUpsertWithWhere"></a>
# **catalogProductUpsertWithWhere**
> CatalogProduct catalogProductUpsertWithWhere(where, data)

Update an existing model instance or insert a new one into the data source based on the where criteria.

### Example
```java
// Import classes:
//import com.harpoon.api.CatalogProductApi;

CatalogProductApi apiInstance = new CatalogProductApi();
String where = "where_example"; // String | Criteria to match model instances
CatalogProduct data = new CatalogProduct(); // CatalogProduct | An object of model property name/value pairs
try {
    CatalogProduct result = apiInstance.catalogProductUpsertWithWhere(where, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CatalogProductApi#catalogProductUpsertWithWhere");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional]
 **data** | [**CatalogProduct**](CatalogProduct.md)| An object of model property name/value pairs | [optional]

### Return type

[**CatalogProduct**](CatalogProduct.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

