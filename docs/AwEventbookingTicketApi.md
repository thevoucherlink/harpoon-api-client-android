# AwEventbookingTicketApi

All URIs are relative to *https://apicdn.harpoonconnect.com/v1.1.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**awEventbookingTicketCount**](AwEventbookingTicketApi.md#awEventbookingTicketCount) | **GET** /AwEventbookingTickets/count | Count instances of the model matched by where from the data source.
[**awEventbookingTicketCreate**](AwEventbookingTicketApi.md#awEventbookingTicketCreate) | **POST** /AwEventbookingTickets | Create a new instance of the model and persist it into the data source.
[**awEventbookingTicketCreateChangeStreamGetAwEventbookingTicketsChangeStream**](AwEventbookingTicketApi.md#awEventbookingTicketCreateChangeStreamGetAwEventbookingTicketsChangeStream) | **GET** /AwEventbookingTickets/change-stream | Create a change stream.
[**awEventbookingTicketCreateChangeStreamPostAwEventbookingTicketsChangeStream**](AwEventbookingTicketApi.md#awEventbookingTicketCreateChangeStreamPostAwEventbookingTicketsChangeStream) | **POST** /AwEventbookingTickets/change-stream | Create a change stream.
[**awEventbookingTicketDeleteById**](AwEventbookingTicketApi.md#awEventbookingTicketDeleteById) | **DELETE** /AwEventbookingTickets/{id} | Delete a model instance by {{id}} from the data source.
[**awEventbookingTicketExistsGetAwEventbookingTicketsidExists**](AwEventbookingTicketApi.md#awEventbookingTicketExistsGetAwEventbookingTicketsidExists) | **GET** /AwEventbookingTickets/{id}/exists | Check whether a model instance exists in the data source.
[**awEventbookingTicketExistsHeadAwEventbookingTicketsid**](AwEventbookingTicketApi.md#awEventbookingTicketExistsHeadAwEventbookingTicketsid) | **HEAD** /AwEventbookingTickets/{id} | Check whether a model instance exists in the data source.
[**awEventbookingTicketFind**](AwEventbookingTicketApi.md#awEventbookingTicketFind) | **GET** /AwEventbookingTickets | Find all instances of the model matched by filter from the data source.
[**awEventbookingTicketFindById**](AwEventbookingTicketApi.md#awEventbookingTicketFindById) | **GET** /AwEventbookingTickets/{id} | Find a model instance by {{id}} from the data source.
[**awEventbookingTicketFindOne**](AwEventbookingTicketApi.md#awEventbookingTicketFindOne) | **GET** /AwEventbookingTickets/findOne | Find first instance of the model matched by filter from the data source.
[**awEventbookingTicketPrototypeUpdateAttributesPatchAwEventbookingTicketsid**](AwEventbookingTicketApi.md#awEventbookingTicketPrototypeUpdateAttributesPatchAwEventbookingTicketsid) | **PATCH** /AwEventbookingTickets/{id} | Patch attributes for a model instance and persist it into the data source.
[**awEventbookingTicketPrototypeUpdateAttributesPutAwEventbookingTicketsid**](AwEventbookingTicketApi.md#awEventbookingTicketPrototypeUpdateAttributesPutAwEventbookingTicketsid) | **PUT** /AwEventbookingTickets/{id} | Patch attributes for a model instance and persist it into the data source.
[**awEventbookingTicketReplaceById**](AwEventbookingTicketApi.md#awEventbookingTicketReplaceById) | **POST** /AwEventbookingTickets/{id}/replace | Replace attributes for a model instance and persist it into the data source.
[**awEventbookingTicketReplaceOrCreate**](AwEventbookingTicketApi.md#awEventbookingTicketReplaceOrCreate) | **POST** /AwEventbookingTickets/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
[**awEventbookingTicketUpdateAll**](AwEventbookingTicketApi.md#awEventbookingTicketUpdateAll) | **POST** /AwEventbookingTickets/update | Update instances of the model matched by {{where}} from the data source.
[**awEventbookingTicketUpsertPatchAwEventbookingTickets**](AwEventbookingTicketApi.md#awEventbookingTicketUpsertPatchAwEventbookingTickets) | **PATCH** /AwEventbookingTickets | Patch an existing model instance or insert a new one into the data source.
[**awEventbookingTicketUpsertPutAwEventbookingTickets**](AwEventbookingTicketApi.md#awEventbookingTicketUpsertPutAwEventbookingTickets) | **PUT** /AwEventbookingTickets | Patch an existing model instance or insert a new one into the data source.
[**awEventbookingTicketUpsertWithWhere**](AwEventbookingTicketApi.md#awEventbookingTicketUpsertWithWhere) | **POST** /AwEventbookingTickets/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.


<a name="awEventbookingTicketCount"></a>
# **awEventbookingTicketCount**
> InlineResponse2001 awEventbookingTicketCount(where)

Count instances of the model matched by where from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.AwEventbookingTicketApi;

AwEventbookingTicketApi apiInstance = new AwEventbookingTicketApi();
String where = "where_example"; // String | Criteria to match model instances
try {
    InlineResponse2001 result = apiInstance.awEventbookingTicketCount(where);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AwEventbookingTicketApi#awEventbookingTicketCount");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional]

### Return type

[**InlineResponse2001**](InlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingTicketCreate"></a>
# **awEventbookingTicketCreate**
> AwEventbookingTicket awEventbookingTicketCreate(data)

Create a new instance of the model and persist it into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.AwEventbookingTicketApi;

AwEventbookingTicketApi apiInstance = new AwEventbookingTicketApi();
AwEventbookingTicket data = new AwEventbookingTicket(); // AwEventbookingTicket | Model instance data
try {
    AwEventbookingTicket result = apiInstance.awEventbookingTicketCreate(data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AwEventbookingTicketApi#awEventbookingTicketCreate");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**AwEventbookingTicket**](AwEventbookingTicket.md)| Model instance data | [optional]

### Return type

[**AwEventbookingTicket**](AwEventbookingTicket.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingTicketCreateChangeStreamGetAwEventbookingTicketsChangeStream"></a>
# **awEventbookingTicketCreateChangeStreamGetAwEventbookingTicketsChangeStream**
> File awEventbookingTicketCreateChangeStreamGetAwEventbookingTicketsChangeStream(options)

Create a change stream.

### Example
```java
// Import classes:
//import com.harpoon.api.AwEventbookingTicketApi;

AwEventbookingTicketApi apiInstance = new AwEventbookingTicketApi();
String options = "options_example"; // String | 
try {
    File result = apiInstance.awEventbookingTicketCreateChangeStreamGetAwEventbookingTicketsChangeStream(options);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AwEventbookingTicketApi#awEventbookingTicketCreateChangeStreamGetAwEventbookingTicketsChangeStream");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **String**|  | [optional]

### Return type

[**File**](File.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingTicketCreateChangeStreamPostAwEventbookingTicketsChangeStream"></a>
# **awEventbookingTicketCreateChangeStreamPostAwEventbookingTicketsChangeStream**
> File awEventbookingTicketCreateChangeStreamPostAwEventbookingTicketsChangeStream(options)

Create a change stream.

### Example
```java
// Import classes:
//import com.harpoon.api.AwEventbookingTicketApi;

AwEventbookingTicketApi apiInstance = new AwEventbookingTicketApi();
String options = "options_example"; // String | 
try {
    File result = apiInstance.awEventbookingTicketCreateChangeStreamPostAwEventbookingTicketsChangeStream(options);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AwEventbookingTicketApi#awEventbookingTicketCreateChangeStreamPostAwEventbookingTicketsChangeStream");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **String**|  | [optional]

### Return type

[**File**](File.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingTicketDeleteById"></a>
# **awEventbookingTicketDeleteById**
> Object awEventbookingTicketDeleteById(id)

Delete a model instance by {{id}} from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.AwEventbookingTicketApi;

AwEventbookingTicketApi apiInstance = new AwEventbookingTicketApi();
String id = "id_example"; // String | Model id
try {
    Object result = apiInstance.awEventbookingTicketDeleteById(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AwEventbookingTicketApi#awEventbookingTicketDeleteById");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |

### Return type

**Object**

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingTicketExistsGetAwEventbookingTicketsidExists"></a>
# **awEventbookingTicketExistsGetAwEventbookingTicketsidExists**
> InlineResponse2003 awEventbookingTicketExistsGetAwEventbookingTicketsidExists(id)

Check whether a model instance exists in the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.AwEventbookingTicketApi;

AwEventbookingTicketApi apiInstance = new AwEventbookingTicketApi();
String id = "id_example"; // String | Model id
try {
    InlineResponse2003 result = apiInstance.awEventbookingTicketExistsGetAwEventbookingTicketsidExists(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AwEventbookingTicketApi#awEventbookingTicketExistsGetAwEventbookingTicketsidExists");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |

### Return type

[**InlineResponse2003**](InlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingTicketExistsHeadAwEventbookingTicketsid"></a>
# **awEventbookingTicketExistsHeadAwEventbookingTicketsid**
> InlineResponse2003 awEventbookingTicketExistsHeadAwEventbookingTicketsid(id)

Check whether a model instance exists in the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.AwEventbookingTicketApi;

AwEventbookingTicketApi apiInstance = new AwEventbookingTicketApi();
String id = "id_example"; // String | Model id
try {
    InlineResponse2003 result = apiInstance.awEventbookingTicketExistsHeadAwEventbookingTicketsid(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AwEventbookingTicketApi#awEventbookingTicketExistsHeadAwEventbookingTicketsid");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |

### Return type

[**InlineResponse2003**](InlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingTicketFind"></a>
# **awEventbookingTicketFind**
> List&lt;AwEventbookingTicket&gt; awEventbookingTicketFind(filter)

Find all instances of the model matched by filter from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.AwEventbookingTicketApi;

AwEventbookingTicketApi apiInstance = new AwEventbookingTicketApi();
String filter = "filter_example"; // String | Filter defining fields, where, include, order, offset, and limit
try {
    List<AwEventbookingTicket> result = apiInstance.awEventbookingTicketFind(filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AwEventbookingTicketApi#awEventbookingTicketFind");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **String**| Filter defining fields, where, include, order, offset, and limit | [optional]

### Return type

[**List&lt;AwEventbookingTicket&gt;**](AwEventbookingTicket.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingTicketFindById"></a>
# **awEventbookingTicketFindById**
> AwEventbookingTicket awEventbookingTicketFindById(id, filter)

Find a model instance by {{id}} from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.AwEventbookingTicketApi;

AwEventbookingTicketApi apiInstance = new AwEventbookingTicketApi();
String id = "id_example"; // String | Model id
String filter = "filter_example"; // String | Filter defining fields and include
try {
    AwEventbookingTicket result = apiInstance.awEventbookingTicketFindById(id, filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AwEventbookingTicketApi#awEventbookingTicketFindById");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |
 **filter** | **String**| Filter defining fields and include | [optional]

### Return type

[**AwEventbookingTicket**](AwEventbookingTicket.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingTicketFindOne"></a>
# **awEventbookingTicketFindOne**
> AwEventbookingTicket awEventbookingTicketFindOne(filter)

Find first instance of the model matched by filter from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.AwEventbookingTicketApi;

AwEventbookingTicketApi apiInstance = new AwEventbookingTicketApi();
String filter = "filter_example"; // String | Filter defining fields, where, include, order, offset, and limit
try {
    AwEventbookingTicket result = apiInstance.awEventbookingTicketFindOne(filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AwEventbookingTicketApi#awEventbookingTicketFindOne");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **String**| Filter defining fields, where, include, order, offset, and limit | [optional]

### Return type

[**AwEventbookingTicket**](AwEventbookingTicket.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingTicketPrototypeUpdateAttributesPatchAwEventbookingTicketsid"></a>
# **awEventbookingTicketPrototypeUpdateAttributesPatchAwEventbookingTicketsid**
> AwEventbookingTicket awEventbookingTicketPrototypeUpdateAttributesPatchAwEventbookingTicketsid(id, data)

Patch attributes for a model instance and persist it into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.AwEventbookingTicketApi;

AwEventbookingTicketApi apiInstance = new AwEventbookingTicketApi();
String id = "id_example"; // String | AwEventbookingTicket id
AwEventbookingTicket data = new AwEventbookingTicket(); // AwEventbookingTicket | An object of model property name/value pairs
try {
    AwEventbookingTicket result = apiInstance.awEventbookingTicketPrototypeUpdateAttributesPatchAwEventbookingTicketsid(id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AwEventbookingTicketApi#awEventbookingTicketPrototypeUpdateAttributesPatchAwEventbookingTicketsid");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| AwEventbookingTicket id |
 **data** | [**AwEventbookingTicket**](AwEventbookingTicket.md)| An object of model property name/value pairs | [optional]

### Return type

[**AwEventbookingTicket**](AwEventbookingTicket.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingTicketPrototypeUpdateAttributesPutAwEventbookingTicketsid"></a>
# **awEventbookingTicketPrototypeUpdateAttributesPutAwEventbookingTicketsid**
> AwEventbookingTicket awEventbookingTicketPrototypeUpdateAttributesPutAwEventbookingTicketsid(id, data)

Patch attributes for a model instance and persist it into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.AwEventbookingTicketApi;

AwEventbookingTicketApi apiInstance = new AwEventbookingTicketApi();
String id = "id_example"; // String | AwEventbookingTicket id
AwEventbookingTicket data = new AwEventbookingTicket(); // AwEventbookingTicket | An object of model property name/value pairs
try {
    AwEventbookingTicket result = apiInstance.awEventbookingTicketPrototypeUpdateAttributesPutAwEventbookingTicketsid(id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AwEventbookingTicketApi#awEventbookingTicketPrototypeUpdateAttributesPutAwEventbookingTicketsid");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| AwEventbookingTicket id |
 **data** | [**AwEventbookingTicket**](AwEventbookingTicket.md)| An object of model property name/value pairs | [optional]

### Return type

[**AwEventbookingTicket**](AwEventbookingTicket.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingTicketReplaceById"></a>
# **awEventbookingTicketReplaceById**
> AwEventbookingTicket awEventbookingTicketReplaceById(id, data)

Replace attributes for a model instance and persist it into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.AwEventbookingTicketApi;

AwEventbookingTicketApi apiInstance = new AwEventbookingTicketApi();
String id = "id_example"; // String | Model id
AwEventbookingTicket data = new AwEventbookingTicket(); // AwEventbookingTicket | Model instance data
try {
    AwEventbookingTicket result = apiInstance.awEventbookingTicketReplaceById(id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AwEventbookingTicketApi#awEventbookingTicketReplaceById");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |
 **data** | [**AwEventbookingTicket**](AwEventbookingTicket.md)| Model instance data | [optional]

### Return type

[**AwEventbookingTicket**](AwEventbookingTicket.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingTicketReplaceOrCreate"></a>
# **awEventbookingTicketReplaceOrCreate**
> AwEventbookingTicket awEventbookingTicketReplaceOrCreate(data)

Replace an existing model instance or insert a new one into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.AwEventbookingTicketApi;

AwEventbookingTicketApi apiInstance = new AwEventbookingTicketApi();
AwEventbookingTicket data = new AwEventbookingTicket(); // AwEventbookingTicket | Model instance data
try {
    AwEventbookingTicket result = apiInstance.awEventbookingTicketReplaceOrCreate(data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AwEventbookingTicketApi#awEventbookingTicketReplaceOrCreate");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**AwEventbookingTicket**](AwEventbookingTicket.md)| Model instance data | [optional]

### Return type

[**AwEventbookingTicket**](AwEventbookingTicket.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingTicketUpdateAll"></a>
# **awEventbookingTicketUpdateAll**
> InlineResponse2002 awEventbookingTicketUpdateAll(where, data)

Update instances of the model matched by {{where}} from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.AwEventbookingTicketApi;

AwEventbookingTicketApi apiInstance = new AwEventbookingTicketApi();
String where = "where_example"; // String | Criteria to match model instances
AwEventbookingTicket data = new AwEventbookingTicket(); // AwEventbookingTicket | An object of model property name/value pairs
try {
    InlineResponse2002 result = apiInstance.awEventbookingTicketUpdateAll(where, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AwEventbookingTicketApi#awEventbookingTicketUpdateAll");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional]
 **data** | [**AwEventbookingTicket**](AwEventbookingTicket.md)| An object of model property name/value pairs | [optional]

### Return type

[**InlineResponse2002**](InlineResponse2002.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingTicketUpsertPatchAwEventbookingTickets"></a>
# **awEventbookingTicketUpsertPatchAwEventbookingTickets**
> AwEventbookingTicket awEventbookingTicketUpsertPatchAwEventbookingTickets(data)

Patch an existing model instance or insert a new one into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.AwEventbookingTicketApi;

AwEventbookingTicketApi apiInstance = new AwEventbookingTicketApi();
AwEventbookingTicket data = new AwEventbookingTicket(); // AwEventbookingTicket | Model instance data
try {
    AwEventbookingTicket result = apiInstance.awEventbookingTicketUpsertPatchAwEventbookingTickets(data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AwEventbookingTicketApi#awEventbookingTicketUpsertPatchAwEventbookingTickets");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**AwEventbookingTicket**](AwEventbookingTicket.md)| Model instance data | [optional]

### Return type

[**AwEventbookingTicket**](AwEventbookingTicket.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingTicketUpsertPutAwEventbookingTickets"></a>
# **awEventbookingTicketUpsertPutAwEventbookingTickets**
> AwEventbookingTicket awEventbookingTicketUpsertPutAwEventbookingTickets(data)

Patch an existing model instance or insert a new one into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.AwEventbookingTicketApi;

AwEventbookingTicketApi apiInstance = new AwEventbookingTicketApi();
AwEventbookingTicket data = new AwEventbookingTicket(); // AwEventbookingTicket | Model instance data
try {
    AwEventbookingTicket result = apiInstance.awEventbookingTicketUpsertPutAwEventbookingTickets(data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AwEventbookingTicketApi#awEventbookingTicketUpsertPutAwEventbookingTickets");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**AwEventbookingTicket**](AwEventbookingTicket.md)| Model instance data | [optional]

### Return type

[**AwEventbookingTicket**](AwEventbookingTicket.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingTicketUpsertWithWhere"></a>
# **awEventbookingTicketUpsertWithWhere**
> AwEventbookingTicket awEventbookingTicketUpsertWithWhere(where, data)

Update an existing model instance or insert a new one into the data source based on the where criteria.

### Example
```java
// Import classes:
//import com.harpoon.api.AwEventbookingTicketApi;

AwEventbookingTicketApi apiInstance = new AwEventbookingTicketApi();
String where = "where_example"; // String | Criteria to match model instances
AwEventbookingTicket data = new AwEventbookingTicket(); // AwEventbookingTicket | An object of model property name/value pairs
try {
    AwEventbookingTicket result = apiInstance.awEventbookingTicketUpsertWithWhere(where, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AwEventbookingTicketApi#awEventbookingTicketUpsertWithWhere");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional]
 **data** | [**AwEventbookingTicket**](AwEventbookingTicket.md)| An object of model property name/value pairs | [optional]

### Return type

[**AwEventbookingTicket**](AwEventbookingTicket.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

