# AwEventbookingEventTicketApi

All URIs are relative to *https://apicdn.harpoonconnect.com/v1.1.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**awEventbookingEventTicketCount**](AwEventbookingEventTicketApi.md#awEventbookingEventTicketCount) | **GET** /AwEventbookingEventTickets/count | Count instances of the model matched by where from the data source.
[**awEventbookingEventTicketCreate**](AwEventbookingEventTicketApi.md#awEventbookingEventTicketCreate) | **POST** /AwEventbookingEventTickets | Create a new instance of the model and persist it into the data source.
[**awEventbookingEventTicketCreateChangeStreamGetAwEventbookingEventTicketsChangeStream**](AwEventbookingEventTicketApi.md#awEventbookingEventTicketCreateChangeStreamGetAwEventbookingEventTicketsChangeStream) | **GET** /AwEventbookingEventTickets/change-stream | Create a change stream.
[**awEventbookingEventTicketCreateChangeStreamPostAwEventbookingEventTicketsChangeStream**](AwEventbookingEventTicketApi.md#awEventbookingEventTicketCreateChangeStreamPostAwEventbookingEventTicketsChangeStream) | **POST** /AwEventbookingEventTickets/change-stream | Create a change stream.
[**awEventbookingEventTicketDeleteById**](AwEventbookingEventTicketApi.md#awEventbookingEventTicketDeleteById) | **DELETE** /AwEventbookingEventTickets/{id} | Delete a model instance by {{id}} from the data source.
[**awEventbookingEventTicketExistsGetAwEventbookingEventTicketsidExists**](AwEventbookingEventTicketApi.md#awEventbookingEventTicketExistsGetAwEventbookingEventTicketsidExists) | **GET** /AwEventbookingEventTickets/{id}/exists | Check whether a model instance exists in the data source.
[**awEventbookingEventTicketExistsHeadAwEventbookingEventTicketsid**](AwEventbookingEventTicketApi.md#awEventbookingEventTicketExistsHeadAwEventbookingEventTicketsid) | **HEAD** /AwEventbookingEventTickets/{id} | Check whether a model instance exists in the data source.
[**awEventbookingEventTicketFind**](AwEventbookingEventTicketApi.md#awEventbookingEventTicketFind) | **GET** /AwEventbookingEventTickets | Find all instances of the model matched by filter from the data source.
[**awEventbookingEventTicketFindById**](AwEventbookingEventTicketApi.md#awEventbookingEventTicketFindById) | **GET** /AwEventbookingEventTickets/{id} | Find a model instance by {{id}} from the data source.
[**awEventbookingEventTicketFindOne**](AwEventbookingEventTicketApi.md#awEventbookingEventTicketFindOne) | **GET** /AwEventbookingEventTickets/findOne | Find first instance of the model matched by filter from the data source.
[**awEventbookingEventTicketPrototypeCountAttributes**](AwEventbookingEventTicketApi.md#awEventbookingEventTicketPrototypeCountAttributes) | **GET** /AwEventbookingEventTickets/{id}/attributes/count | Counts attributes of AwEventbookingEventTicket.
[**awEventbookingEventTicketPrototypeCountAwEventbookingTicket**](AwEventbookingEventTicketApi.md#awEventbookingEventTicketPrototypeCountAwEventbookingTicket) | **GET** /AwEventbookingEventTickets/{id}/awEventbookingTicket/count | Counts awEventbookingTicket of AwEventbookingEventTicket.
[**awEventbookingEventTicketPrototypeCountTickets**](AwEventbookingEventTicketApi.md#awEventbookingEventTicketPrototypeCountTickets) | **GET** /AwEventbookingEventTickets/{id}/tickets/count | Counts tickets of AwEventbookingEventTicket.
[**awEventbookingEventTicketPrototypeCreateAttributes**](AwEventbookingEventTicketApi.md#awEventbookingEventTicketPrototypeCreateAttributes) | **POST** /AwEventbookingEventTickets/{id}/attributes | Creates a new instance in attributes of this model.
[**awEventbookingEventTicketPrototypeCreateAwEventbookingTicket**](AwEventbookingEventTicketApi.md#awEventbookingEventTicketPrototypeCreateAwEventbookingTicket) | **POST** /AwEventbookingEventTickets/{id}/awEventbookingTicket | Creates a new instance in awEventbookingTicket of this model.
[**awEventbookingEventTicketPrototypeCreateTickets**](AwEventbookingEventTicketApi.md#awEventbookingEventTicketPrototypeCreateTickets) | **POST** /AwEventbookingEventTickets/{id}/tickets | Creates a new instance in tickets of this model.
[**awEventbookingEventTicketPrototypeDeleteAttributes**](AwEventbookingEventTicketApi.md#awEventbookingEventTicketPrototypeDeleteAttributes) | **DELETE** /AwEventbookingEventTickets/{id}/attributes | Deletes all attributes of this model.
[**awEventbookingEventTicketPrototypeDeleteAwEventbookingTicket**](AwEventbookingEventTicketApi.md#awEventbookingEventTicketPrototypeDeleteAwEventbookingTicket) | **DELETE** /AwEventbookingEventTickets/{id}/awEventbookingTicket | Deletes all awEventbookingTicket of this model.
[**awEventbookingEventTicketPrototypeDeleteTickets**](AwEventbookingEventTicketApi.md#awEventbookingEventTicketPrototypeDeleteTickets) | **DELETE** /AwEventbookingEventTickets/{id}/tickets | Deletes all tickets of this model.
[**awEventbookingEventTicketPrototypeDestroyByIdAttributes**](AwEventbookingEventTicketApi.md#awEventbookingEventTicketPrototypeDestroyByIdAttributes) | **DELETE** /AwEventbookingEventTickets/{id}/attributes/{fk} | Delete a related item by id for attributes.
[**awEventbookingEventTicketPrototypeDestroyByIdAwEventbookingTicket**](AwEventbookingEventTicketApi.md#awEventbookingEventTicketPrototypeDestroyByIdAwEventbookingTicket) | **DELETE** /AwEventbookingEventTickets/{id}/awEventbookingTicket/{fk} | Delete a related item by id for awEventbookingTicket.
[**awEventbookingEventTicketPrototypeDestroyByIdTickets**](AwEventbookingEventTicketApi.md#awEventbookingEventTicketPrototypeDestroyByIdTickets) | **DELETE** /AwEventbookingEventTickets/{id}/tickets/{fk} | Delete a related item by id for tickets.
[**awEventbookingEventTicketPrototypeFindByIdAttributes**](AwEventbookingEventTicketApi.md#awEventbookingEventTicketPrototypeFindByIdAttributes) | **GET** /AwEventbookingEventTickets/{id}/attributes/{fk} | Find a related item by id for attributes.
[**awEventbookingEventTicketPrototypeFindByIdAwEventbookingTicket**](AwEventbookingEventTicketApi.md#awEventbookingEventTicketPrototypeFindByIdAwEventbookingTicket) | **GET** /AwEventbookingEventTickets/{id}/awEventbookingTicket/{fk} | Find a related item by id for awEventbookingTicket.
[**awEventbookingEventTicketPrototypeFindByIdTickets**](AwEventbookingEventTicketApi.md#awEventbookingEventTicketPrototypeFindByIdTickets) | **GET** /AwEventbookingEventTickets/{id}/tickets/{fk} | Find a related item by id for tickets.
[**awEventbookingEventTicketPrototypeGetAttributes**](AwEventbookingEventTicketApi.md#awEventbookingEventTicketPrototypeGetAttributes) | **GET** /AwEventbookingEventTickets/{id}/attributes | Queries attributes of AwEventbookingEventTicket.
[**awEventbookingEventTicketPrototypeGetAwEventbookingTicket**](AwEventbookingEventTicketApi.md#awEventbookingEventTicketPrototypeGetAwEventbookingTicket) | **GET** /AwEventbookingEventTickets/{id}/awEventbookingTicket | Queries awEventbookingTicket of AwEventbookingEventTicket.
[**awEventbookingEventTicketPrototypeGetTickets**](AwEventbookingEventTicketApi.md#awEventbookingEventTicketPrototypeGetTickets) | **GET** /AwEventbookingEventTickets/{id}/tickets | Queries tickets of AwEventbookingEventTicket.
[**awEventbookingEventTicketPrototypeUpdateAttributesPatchAwEventbookingEventTicketsid**](AwEventbookingEventTicketApi.md#awEventbookingEventTicketPrototypeUpdateAttributesPatchAwEventbookingEventTicketsid) | **PATCH** /AwEventbookingEventTickets/{id} | Patch attributes for a model instance and persist it into the data source.
[**awEventbookingEventTicketPrototypeUpdateAttributesPutAwEventbookingEventTicketsid**](AwEventbookingEventTicketApi.md#awEventbookingEventTicketPrototypeUpdateAttributesPutAwEventbookingEventTicketsid) | **PUT** /AwEventbookingEventTickets/{id} | Patch attributes for a model instance and persist it into the data source.
[**awEventbookingEventTicketPrototypeUpdateByIdAttributes**](AwEventbookingEventTicketApi.md#awEventbookingEventTicketPrototypeUpdateByIdAttributes) | **PUT** /AwEventbookingEventTickets/{id}/attributes/{fk} | Update a related item by id for attributes.
[**awEventbookingEventTicketPrototypeUpdateByIdAwEventbookingTicket**](AwEventbookingEventTicketApi.md#awEventbookingEventTicketPrototypeUpdateByIdAwEventbookingTicket) | **PUT** /AwEventbookingEventTickets/{id}/awEventbookingTicket/{fk} | Update a related item by id for awEventbookingTicket.
[**awEventbookingEventTicketPrototypeUpdateByIdTickets**](AwEventbookingEventTicketApi.md#awEventbookingEventTicketPrototypeUpdateByIdTickets) | **PUT** /AwEventbookingEventTickets/{id}/tickets/{fk} | Update a related item by id for tickets.
[**awEventbookingEventTicketReplaceById**](AwEventbookingEventTicketApi.md#awEventbookingEventTicketReplaceById) | **POST** /AwEventbookingEventTickets/{id}/replace | Replace attributes for a model instance and persist it into the data source.
[**awEventbookingEventTicketReplaceOrCreate**](AwEventbookingEventTicketApi.md#awEventbookingEventTicketReplaceOrCreate) | **POST** /AwEventbookingEventTickets/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
[**awEventbookingEventTicketUpdateAll**](AwEventbookingEventTicketApi.md#awEventbookingEventTicketUpdateAll) | **POST** /AwEventbookingEventTickets/update | Update instances of the model matched by {{where}} from the data source.
[**awEventbookingEventTicketUpsertPatchAwEventbookingEventTickets**](AwEventbookingEventTicketApi.md#awEventbookingEventTicketUpsertPatchAwEventbookingEventTickets) | **PATCH** /AwEventbookingEventTickets | Patch an existing model instance or insert a new one into the data source.
[**awEventbookingEventTicketUpsertPutAwEventbookingEventTickets**](AwEventbookingEventTicketApi.md#awEventbookingEventTicketUpsertPutAwEventbookingEventTickets) | **PUT** /AwEventbookingEventTickets | Patch an existing model instance or insert a new one into the data source.
[**awEventbookingEventTicketUpsertWithWhere**](AwEventbookingEventTicketApi.md#awEventbookingEventTicketUpsertWithWhere) | **POST** /AwEventbookingEventTickets/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.


<a name="awEventbookingEventTicketCount"></a>
# **awEventbookingEventTicketCount**
> InlineResponse2001 awEventbookingEventTicketCount(where)

Count instances of the model matched by where from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.AwEventbookingEventTicketApi;

AwEventbookingEventTicketApi apiInstance = new AwEventbookingEventTicketApi();
String where = "where_example"; // String | Criteria to match model instances
try {
    InlineResponse2001 result = apiInstance.awEventbookingEventTicketCount(where);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AwEventbookingEventTicketApi#awEventbookingEventTicketCount");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional]

### Return type

[**InlineResponse2001**](InlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingEventTicketCreate"></a>
# **awEventbookingEventTicketCreate**
> AwEventbookingEventTicket awEventbookingEventTicketCreate(data)

Create a new instance of the model and persist it into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.AwEventbookingEventTicketApi;

AwEventbookingEventTicketApi apiInstance = new AwEventbookingEventTicketApi();
AwEventbookingEventTicket data = new AwEventbookingEventTicket(); // AwEventbookingEventTicket | Model instance data
try {
    AwEventbookingEventTicket result = apiInstance.awEventbookingEventTicketCreate(data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AwEventbookingEventTicketApi#awEventbookingEventTicketCreate");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**AwEventbookingEventTicket**](AwEventbookingEventTicket.md)| Model instance data | [optional]

### Return type

[**AwEventbookingEventTicket**](AwEventbookingEventTicket.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingEventTicketCreateChangeStreamGetAwEventbookingEventTicketsChangeStream"></a>
# **awEventbookingEventTicketCreateChangeStreamGetAwEventbookingEventTicketsChangeStream**
> File awEventbookingEventTicketCreateChangeStreamGetAwEventbookingEventTicketsChangeStream(options)

Create a change stream.

### Example
```java
// Import classes:
//import com.harpoon.api.AwEventbookingEventTicketApi;

AwEventbookingEventTicketApi apiInstance = new AwEventbookingEventTicketApi();
String options = "options_example"; // String | 
try {
    File result = apiInstance.awEventbookingEventTicketCreateChangeStreamGetAwEventbookingEventTicketsChangeStream(options);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AwEventbookingEventTicketApi#awEventbookingEventTicketCreateChangeStreamGetAwEventbookingEventTicketsChangeStream");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **String**|  | [optional]

### Return type

[**File**](File.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingEventTicketCreateChangeStreamPostAwEventbookingEventTicketsChangeStream"></a>
# **awEventbookingEventTicketCreateChangeStreamPostAwEventbookingEventTicketsChangeStream**
> File awEventbookingEventTicketCreateChangeStreamPostAwEventbookingEventTicketsChangeStream(options)

Create a change stream.

### Example
```java
// Import classes:
//import com.harpoon.api.AwEventbookingEventTicketApi;

AwEventbookingEventTicketApi apiInstance = new AwEventbookingEventTicketApi();
String options = "options_example"; // String | 
try {
    File result = apiInstance.awEventbookingEventTicketCreateChangeStreamPostAwEventbookingEventTicketsChangeStream(options);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AwEventbookingEventTicketApi#awEventbookingEventTicketCreateChangeStreamPostAwEventbookingEventTicketsChangeStream");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **String**|  | [optional]

### Return type

[**File**](File.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingEventTicketDeleteById"></a>
# **awEventbookingEventTicketDeleteById**
> Object awEventbookingEventTicketDeleteById(id)

Delete a model instance by {{id}} from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.AwEventbookingEventTicketApi;

AwEventbookingEventTicketApi apiInstance = new AwEventbookingEventTicketApi();
String id = "id_example"; // String | Model id
try {
    Object result = apiInstance.awEventbookingEventTicketDeleteById(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AwEventbookingEventTicketApi#awEventbookingEventTicketDeleteById");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |

### Return type

**Object**

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingEventTicketExistsGetAwEventbookingEventTicketsidExists"></a>
# **awEventbookingEventTicketExistsGetAwEventbookingEventTicketsidExists**
> InlineResponse2003 awEventbookingEventTicketExistsGetAwEventbookingEventTicketsidExists(id)

Check whether a model instance exists in the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.AwEventbookingEventTicketApi;

AwEventbookingEventTicketApi apiInstance = new AwEventbookingEventTicketApi();
String id = "id_example"; // String | Model id
try {
    InlineResponse2003 result = apiInstance.awEventbookingEventTicketExistsGetAwEventbookingEventTicketsidExists(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AwEventbookingEventTicketApi#awEventbookingEventTicketExistsGetAwEventbookingEventTicketsidExists");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |

### Return type

[**InlineResponse2003**](InlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingEventTicketExistsHeadAwEventbookingEventTicketsid"></a>
# **awEventbookingEventTicketExistsHeadAwEventbookingEventTicketsid**
> InlineResponse2003 awEventbookingEventTicketExistsHeadAwEventbookingEventTicketsid(id)

Check whether a model instance exists in the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.AwEventbookingEventTicketApi;

AwEventbookingEventTicketApi apiInstance = new AwEventbookingEventTicketApi();
String id = "id_example"; // String | Model id
try {
    InlineResponse2003 result = apiInstance.awEventbookingEventTicketExistsHeadAwEventbookingEventTicketsid(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AwEventbookingEventTicketApi#awEventbookingEventTicketExistsHeadAwEventbookingEventTicketsid");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |

### Return type

[**InlineResponse2003**](InlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingEventTicketFind"></a>
# **awEventbookingEventTicketFind**
> List&lt;AwEventbookingEventTicket&gt; awEventbookingEventTicketFind(filter)

Find all instances of the model matched by filter from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.AwEventbookingEventTicketApi;

AwEventbookingEventTicketApi apiInstance = new AwEventbookingEventTicketApi();
String filter = "filter_example"; // String | Filter defining fields, where, include, order, offset, and limit
try {
    List<AwEventbookingEventTicket> result = apiInstance.awEventbookingEventTicketFind(filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AwEventbookingEventTicketApi#awEventbookingEventTicketFind");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **String**| Filter defining fields, where, include, order, offset, and limit | [optional]

### Return type

[**List&lt;AwEventbookingEventTicket&gt;**](AwEventbookingEventTicket.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingEventTicketFindById"></a>
# **awEventbookingEventTicketFindById**
> AwEventbookingEventTicket awEventbookingEventTicketFindById(id, filter)

Find a model instance by {{id}} from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.AwEventbookingEventTicketApi;

AwEventbookingEventTicketApi apiInstance = new AwEventbookingEventTicketApi();
String id = "id_example"; // String | Model id
String filter = "filter_example"; // String | Filter defining fields and include
try {
    AwEventbookingEventTicket result = apiInstance.awEventbookingEventTicketFindById(id, filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AwEventbookingEventTicketApi#awEventbookingEventTicketFindById");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |
 **filter** | **String**| Filter defining fields and include | [optional]

### Return type

[**AwEventbookingEventTicket**](AwEventbookingEventTicket.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingEventTicketFindOne"></a>
# **awEventbookingEventTicketFindOne**
> AwEventbookingEventTicket awEventbookingEventTicketFindOne(filter)

Find first instance of the model matched by filter from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.AwEventbookingEventTicketApi;

AwEventbookingEventTicketApi apiInstance = new AwEventbookingEventTicketApi();
String filter = "filter_example"; // String | Filter defining fields, where, include, order, offset, and limit
try {
    AwEventbookingEventTicket result = apiInstance.awEventbookingEventTicketFindOne(filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AwEventbookingEventTicketApi#awEventbookingEventTicketFindOne");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **String**| Filter defining fields, where, include, order, offset, and limit | [optional]

### Return type

[**AwEventbookingEventTicket**](AwEventbookingEventTicket.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingEventTicketPrototypeCountAttributes"></a>
# **awEventbookingEventTicketPrototypeCountAttributes**
> InlineResponse2001 awEventbookingEventTicketPrototypeCountAttributes(id, where)

Counts attributes of AwEventbookingEventTicket.

### Example
```java
// Import classes:
//import com.harpoon.api.AwEventbookingEventTicketApi;

AwEventbookingEventTicketApi apiInstance = new AwEventbookingEventTicketApi();
String id = "id_example"; // String | AwEventbookingEventTicket id
String where = "where_example"; // String | Criteria to match model instances
try {
    InlineResponse2001 result = apiInstance.awEventbookingEventTicketPrototypeCountAttributes(id, where);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AwEventbookingEventTicketApi#awEventbookingEventTicketPrototypeCountAttributes");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| AwEventbookingEventTicket id |
 **where** | **String**| Criteria to match model instances | [optional]

### Return type

[**InlineResponse2001**](InlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingEventTicketPrototypeCountAwEventbookingTicket"></a>
# **awEventbookingEventTicketPrototypeCountAwEventbookingTicket**
> InlineResponse2001 awEventbookingEventTicketPrototypeCountAwEventbookingTicket(id, where)

Counts awEventbookingTicket of AwEventbookingEventTicket.

### Example
```java
// Import classes:
//import com.harpoon.api.AwEventbookingEventTicketApi;

AwEventbookingEventTicketApi apiInstance = new AwEventbookingEventTicketApi();
String id = "id_example"; // String | AwEventbookingEventTicket id
String where = "where_example"; // String | Criteria to match model instances
try {
    InlineResponse2001 result = apiInstance.awEventbookingEventTicketPrototypeCountAwEventbookingTicket(id, where);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AwEventbookingEventTicketApi#awEventbookingEventTicketPrototypeCountAwEventbookingTicket");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| AwEventbookingEventTicket id |
 **where** | **String**| Criteria to match model instances | [optional]

### Return type

[**InlineResponse2001**](InlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingEventTicketPrototypeCountTickets"></a>
# **awEventbookingEventTicketPrototypeCountTickets**
> InlineResponse2001 awEventbookingEventTicketPrototypeCountTickets(id, where)

Counts tickets of AwEventbookingEventTicket.

### Example
```java
// Import classes:
//import com.harpoon.api.AwEventbookingEventTicketApi;

AwEventbookingEventTicketApi apiInstance = new AwEventbookingEventTicketApi();
String id = "id_example"; // String | AwEventbookingEventTicket id
String where = "where_example"; // String | Criteria to match model instances
try {
    InlineResponse2001 result = apiInstance.awEventbookingEventTicketPrototypeCountTickets(id, where);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AwEventbookingEventTicketApi#awEventbookingEventTicketPrototypeCountTickets");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| AwEventbookingEventTicket id |
 **where** | **String**| Criteria to match model instances | [optional]

### Return type

[**InlineResponse2001**](InlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingEventTicketPrototypeCreateAttributes"></a>
# **awEventbookingEventTicketPrototypeCreateAttributes**
> AwEventbookingEventTicketAttribute awEventbookingEventTicketPrototypeCreateAttributes(id, data)

Creates a new instance in attributes of this model.

### Example
```java
// Import classes:
//import com.harpoon.api.AwEventbookingEventTicketApi;

AwEventbookingEventTicketApi apiInstance = new AwEventbookingEventTicketApi();
String id = "id_example"; // String | AwEventbookingEventTicket id
AwEventbookingEventTicketAttribute data = new AwEventbookingEventTicketAttribute(); // AwEventbookingEventTicketAttribute | 
try {
    AwEventbookingEventTicketAttribute result = apiInstance.awEventbookingEventTicketPrototypeCreateAttributes(id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AwEventbookingEventTicketApi#awEventbookingEventTicketPrototypeCreateAttributes");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| AwEventbookingEventTicket id |
 **data** | [**AwEventbookingEventTicketAttribute**](AwEventbookingEventTicketAttribute.md)|  | [optional]

### Return type

[**AwEventbookingEventTicketAttribute**](AwEventbookingEventTicketAttribute.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingEventTicketPrototypeCreateAwEventbookingTicket"></a>
# **awEventbookingEventTicketPrototypeCreateAwEventbookingTicket**
> AwEventbookingTicket awEventbookingEventTicketPrototypeCreateAwEventbookingTicket(id, data)

Creates a new instance in awEventbookingTicket of this model.

### Example
```java
// Import classes:
//import com.harpoon.api.AwEventbookingEventTicketApi;

AwEventbookingEventTicketApi apiInstance = new AwEventbookingEventTicketApi();
String id = "id_example"; // String | AwEventbookingEventTicket id
AwEventbookingTicket data = new AwEventbookingTicket(); // AwEventbookingTicket | 
try {
    AwEventbookingTicket result = apiInstance.awEventbookingEventTicketPrototypeCreateAwEventbookingTicket(id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AwEventbookingEventTicketApi#awEventbookingEventTicketPrototypeCreateAwEventbookingTicket");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| AwEventbookingEventTicket id |
 **data** | [**AwEventbookingTicket**](AwEventbookingTicket.md)|  | [optional]

### Return type

[**AwEventbookingTicket**](AwEventbookingTicket.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingEventTicketPrototypeCreateTickets"></a>
# **awEventbookingEventTicketPrototypeCreateTickets**
> AwEventbookingTicket awEventbookingEventTicketPrototypeCreateTickets(id, data)

Creates a new instance in tickets of this model.

### Example
```java
// Import classes:
//import com.harpoon.api.AwEventbookingEventTicketApi;

AwEventbookingEventTicketApi apiInstance = new AwEventbookingEventTicketApi();
String id = "id_example"; // String | AwEventbookingEventTicket id
AwEventbookingTicket data = new AwEventbookingTicket(); // AwEventbookingTicket | 
try {
    AwEventbookingTicket result = apiInstance.awEventbookingEventTicketPrototypeCreateTickets(id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AwEventbookingEventTicketApi#awEventbookingEventTicketPrototypeCreateTickets");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| AwEventbookingEventTicket id |
 **data** | [**AwEventbookingTicket**](AwEventbookingTicket.md)|  | [optional]

### Return type

[**AwEventbookingTicket**](AwEventbookingTicket.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingEventTicketPrototypeDeleteAttributes"></a>
# **awEventbookingEventTicketPrototypeDeleteAttributes**
> awEventbookingEventTicketPrototypeDeleteAttributes(id)

Deletes all attributes of this model.

### Example
```java
// Import classes:
//import com.harpoon.api.AwEventbookingEventTicketApi;

AwEventbookingEventTicketApi apiInstance = new AwEventbookingEventTicketApi();
String id = "id_example"; // String | AwEventbookingEventTicket id
try {
    apiInstance.awEventbookingEventTicketPrototypeDeleteAttributes(id);
} catch (ApiException e) {
    System.err.println("Exception when calling AwEventbookingEventTicketApi#awEventbookingEventTicketPrototypeDeleteAttributes");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| AwEventbookingEventTicket id |

### Return type

null (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingEventTicketPrototypeDeleteAwEventbookingTicket"></a>
# **awEventbookingEventTicketPrototypeDeleteAwEventbookingTicket**
> awEventbookingEventTicketPrototypeDeleteAwEventbookingTicket(id)

Deletes all awEventbookingTicket of this model.

### Example
```java
// Import classes:
//import com.harpoon.api.AwEventbookingEventTicketApi;

AwEventbookingEventTicketApi apiInstance = new AwEventbookingEventTicketApi();
String id = "id_example"; // String | AwEventbookingEventTicket id
try {
    apiInstance.awEventbookingEventTicketPrototypeDeleteAwEventbookingTicket(id);
} catch (ApiException e) {
    System.err.println("Exception when calling AwEventbookingEventTicketApi#awEventbookingEventTicketPrototypeDeleteAwEventbookingTicket");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| AwEventbookingEventTicket id |

### Return type

null (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingEventTicketPrototypeDeleteTickets"></a>
# **awEventbookingEventTicketPrototypeDeleteTickets**
> awEventbookingEventTicketPrototypeDeleteTickets(id)

Deletes all tickets of this model.

### Example
```java
// Import classes:
//import com.harpoon.api.AwEventbookingEventTicketApi;

AwEventbookingEventTicketApi apiInstance = new AwEventbookingEventTicketApi();
String id = "id_example"; // String | AwEventbookingEventTicket id
try {
    apiInstance.awEventbookingEventTicketPrototypeDeleteTickets(id);
} catch (ApiException e) {
    System.err.println("Exception when calling AwEventbookingEventTicketApi#awEventbookingEventTicketPrototypeDeleteTickets");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| AwEventbookingEventTicket id |

### Return type

null (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingEventTicketPrototypeDestroyByIdAttributes"></a>
# **awEventbookingEventTicketPrototypeDestroyByIdAttributes**
> awEventbookingEventTicketPrototypeDestroyByIdAttributes(fk, id)

Delete a related item by id for attributes.

### Example
```java
// Import classes:
//import com.harpoon.api.AwEventbookingEventTicketApi;

AwEventbookingEventTicketApi apiInstance = new AwEventbookingEventTicketApi();
String fk = "fk_example"; // String | Foreign key for attributes
String id = "id_example"; // String | AwEventbookingEventTicket id
try {
    apiInstance.awEventbookingEventTicketPrototypeDestroyByIdAttributes(fk, id);
} catch (ApiException e) {
    System.err.println("Exception when calling AwEventbookingEventTicketApi#awEventbookingEventTicketPrototypeDestroyByIdAttributes");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for attributes |
 **id** | **String**| AwEventbookingEventTicket id |

### Return type

null (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingEventTicketPrototypeDestroyByIdAwEventbookingTicket"></a>
# **awEventbookingEventTicketPrototypeDestroyByIdAwEventbookingTicket**
> awEventbookingEventTicketPrototypeDestroyByIdAwEventbookingTicket(fk, id)

Delete a related item by id for awEventbookingTicket.

### Example
```java
// Import classes:
//import com.harpoon.api.AwEventbookingEventTicketApi;

AwEventbookingEventTicketApi apiInstance = new AwEventbookingEventTicketApi();
String fk = "fk_example"; // String | Foreign key for awEventbookingTicket
String id = "id_example"; // String | AwEventbookingEventTicket id
try {
    apiInstance.awEventbookingEventTicketPrototypeDestroyByIdAwEventbookingTicket(fk, id);
} catch (ApiException e) {
    System.err.println("Exception when calling AwEventbookingEventTicketApi#awEventbookingEventTicketPrototypeDestroyByIdAwEventbookingTicket");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for awEventbookingTicket |
 **id** | **String**| AwEventbookingEventTicket id |

### Return type

null (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingEventTicketPrototypeDestroyByIdTickets"></a>
# **awEventbookingEventTicketPrototypeDestroyByIdTickets**
> awEventbookingEventTicketPrototypeDestroyByIdTickets(fk, id)

Delete a related item by id for tickets.

### Example
```java
// Import classes:
//import com.harpoon.api.AwEventbookingEventTicketApi;

AwEventbookingEventTicketApi apiInstance = new AwEventbookingEventTicketApi();
String fk = "fk_example"; // String | Foreign key for tickets
String id = "id_example"; // String | AwEventbookingEventTicket id
try {
    apiInstance.awEventbookingEventTicketPrototypeDestroyByIdTickets(fk, id);
} catch (ApiException e) {
    System.err.println("Exception when calling AwEventbookingEventTicketApi#awEventbookingEventTicketPrototypeDestroyByIdTickets");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for tickets |
 **id** | **String**| AwEventbookingEventTicket id |

### Return type

null (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingEventTicketPrototypeFindByIdAttributes"></a>
# **awEventbookingEventTicketPrototypeFindByIdAttributes**
> AwEventbookingEventTicketAttribute awEventbookingEventTicketPrototypeFindByIdAttributes(fk, id)

Find a related item by id for attributes.

### Example
```java
// Import classes:
//import com.harpoon.api.AwEventbookingEventTicketApi;

AwEventbookingEventTicketApi apiInstance = new AwEventbookingEventTicketApi();
String fk = "fk_example"; // String | Foreign key for attributes
String id = "id_example"; // String | AwEventbookingEventTicket id
try {
    AwEventbookingEventTicketAttribute result = apiInstance.awEventbookingEventTicketPrototypeFindByIdAttributes(fk, id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AwEventbookingEventTicketApi#awEventbookingEventTicketPrototypeFindByIdAttributes");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for attributes |
 **id** | **String**| AwEventbookingEventTicket id |

### Return type

[**AwEventbookingEventTicketAttribute**](AwEventbookingEventTicketAttribute.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingEventTicketPrototypeFindByIdAwEventbookingTicket"></a>
# **awEventbookingEventTicketPrototypeFindByIdAwEventbookingTicket**
> AwEventbookingTicket awEventbookingEventTicketPrototypeFindByIdAwEventbookingTicket(fk, id)

Find a related item by id for awEventbookingTicket.

### Example
```java
// Import classes:
//import com.harpoon.api.AwEventbookingEventTicketApi;

AwEventbookingEventTicketApi apiInstance = new AwEventbookingEventTicketApi();
String fk = "fk_example"; // String | Foreign key for awEventbookingTicket
String id = "id_example"; // String | AwEventbookingEventTicket id
try {
    AwEventbookingTicket result = apiInstance.awEventbookingEventTicketPrototypeFindByIdAwEventbookingTicket(fk, id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AwEventbookingEventTicketApi#awEventbookingEventTicketPrototypeFindByIdAwEventbookingTicket");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for awEventbookingTicket |
 **id** | **String**| AwEventbookingEventTicket id |

### Return type

[**AwEventbookingTicket**](AwEventbookingTicket.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingEventTicketPrototypeFindByIdTickets"></a>
# **awEventbookingEventTicketPrototypeFindByIdTickets**
> AwEventbookingTicket awEventbookingEventTicketPrototypeFindByIdTickets(fk, id)

Find a related item by id for tickets.

### Example
```java
// Import classes:
//import com.harpoon.api.AwEventbookingEventTicketApi;

AwEventbookingEventTicketApi apiInstance = new AwEventbookingEventTicketApi();
String fk = "fk_example"; // String | Foreign key for tickets
String id = "id_example"; // String | AwEventbookingEventTicket id
try {
    AwEventbookingTicket result = apiInstance.awEventbookingEventTicketPrototypeFindByIdTickets(fk, id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AwEventbookingEventTicketApi#awEventbookingEventTicketPrototypeFindByIdTickets");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for tickets |
 **id** | **String**| AwEventbookingEventTicket id |

### Return type

[**AwEventbookingTicket**](AwEventbookingTicket.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingEventTicketPrototypeGetAttributes"></a>
# **awEventbookingEventTicketPrototypeGetAttributes**
> List&lt;AwEventbookingEventTicketAttribute&gt; awEventbookingEventTicketPrototypeGetAttributes(id, filter)

Queries attributes of AwEventbookingEventTicket.

### Example
```java
// Import classes:
//import com.harpoon.api.AwEventbookingEventTicketApi;

AwEventbookingEventTicketApi apiInstance = new AwEventbookingEventTicketApi();
String id = "id_example"; // String | AwEventbookingEventTicket id
String filter = "filter_example"; // String | 
try {
    List<AwEventbookingEventTicketAttribute> result = apiInstance.awEventbookingEventTicketPrototypeGetAttributes(id, filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AwEventbookingEventTicketApi#awEventbookingEventTicketPrototypeGetAttributes");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| AwEventbookingEventTicket id |
 **filter** | **String**|  | [optional]

### Return type

[**List&lt;AwEventbookingEventTicketAttribute&gt;**](AwEventbookingEventTicketAttribute.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingEventTicketPrototypeGetAwEventbookingTicket"></a>
# **awEventbookingEventTicketPrototypeGetAwEventbookingTicket**
> List&lt;AwEventbookingTicket&gt; awEventbookingEventTicketPrototypeGetAwEventbookingTicket(id, filter)

Queries awEventbookingTicket of AwEventbookingEventTicket.

### Example
```java
// Import classes:
//import com.harpoon.api.AwEventbookingEventTicketApi;

AwEventbookingEventTicketApi apiInstance = new AwEventbookingEventTicketApi();
String id = "id_example"; // String | AwEventbookingEventTicket id
String filter = "filter_example"; // String | 
try {
    List<AwEventbookingTicket> result = apiInstance.awEventbookingEventTicketPrototypeGetAwEventbookingTicket(id, filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AwEventbookingEventTicketApi#awEventbookingEventTicketPrototypeGetAwEventbookingTicket");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| AwEventbookingEventTicket id |
 **filter** | **String**|  | [optional]

### Return type

[**List&lt;AwEventbookingTicket&gt;**](AwEventbookingTicket.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingEventTicketPrototypeGetTickets"></a>
# **awEventbookingEventTicketPrototypeGetTickets**
> List&lt;AwEventbookingTicket&gt; awEventbookingEventTicketPrototypeGetTickets(id, filter)

Queries tickets of AwEventbookingEventTicket.

### Example
```java
// Import classes:
//import com.harpoon.api.AwEventbookingEventTicketApi;

AwEventbookingEventTicketApi apiInstance = new AwEventbookingEventTicketApi();
String id = "id_example"; // String | AwEventbookingEventTicket id
String filter = "filter_example"; // String | 
try {
    List<AwEventbookingTicket> result = apiInstance.awEventbookingEventTicketPrototypeGetTickets(id, filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AwEventbookingEventTicketApi#awEventbookingEventTicketPrototypeGetTickets");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| AwEventbookingEventTicket id |
 **filter** | **String**|  | [optional]

### Return type

[**List&lt;AwEventbookingTicket&gt;**](AwEventbookingTicket.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingEventTicketPrototypeUpdateAttributesPatchAwEventbookingEventTicketsid"></a>
# **awEventbookingEventTicketPrototypeUpdateAttributesPatchAwEventbookingEventTicketsid**
> AwEventbookingEventTicket awEventbookingEventTicketPrototypeUpdateAttributesPatchAwEventbookingEventTicketsid(id, data)

Patch attributes for a model instance and persist it into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.AwEventbookingEventTicketApi;

AwEventbookingEventTicketApi apiInstance = new AwEventbookingEventTicketApi();
String id = "id_example"; // String | AwEventbookingEventTicket id
AwEventbookingEventTicket data = new AwEventbookingEventTicket(); // AwEventbookingEventTicket | An object of model property name/value pairs
try {
    AwEventbookingEventTicket result = apiInstance.awEventbookingEventTicketPrototypeUpdateAttributesPatchAwEventbookingEventTicketsid(id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AwEventbookingEventTicketApi#awEventbookingEventTicketPrototypeUpdateAttributesPatchAwEventbookingEventTicketsid");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| AwEventbookingEventTicket id |
 **data** | [**AwEventbookingEventTicket**](AwEventbookingEventTicket.md)| An object of model property name/value pairs | [optional]

### Return type

[**AwEventbookingEventTicket**](AwEventbookingEventTicket.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingEventTicketPrototypeUpdateAttributesPutAwEventbookingEventTicketsid"></a>
# **awEventbookingEventTicketPrototypeUpdateAttributesPutAwEventbookingEventTicketsid**
> AwEventbookingEventTicket awEventbookingEventTicketPrototypeUpdateAttributesPutAwEventbookingEventTicketsid(id, data)

Patch attributes for a model instance and persist it into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.AwEventbookingEventTicketApi;

AwEventbookingEventTicketApi apiInstance = new AwEventbookingEventTicketApi();
String id = "id_example"; // String | AwEventbookingEventTicket id
AwEventbookingEventTicket data = new AwEventbookingEventTicket(); // AwEventbookingEventTicket | An object of model property name/value pairs
try {
    AwEventbookingEventTicket result = apiInstance.awEventbookingEventTicketPrototypeUpdateAttributesPutAwEventbookingEventTicketsid(id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AwEventbookingEventTicketApi#awEventbookingEventTicketPrototypeUpdateAttributesPutAwEventbookingEventTicketsid");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| AwEventbookingEventTicket id |
 **data** | [**AwEventbookingEventTicket**](AwEventbookingEventTicket.md)| An object of model property name/value pairs | [optional]

### Return type

[**AwEventbookingEventTicket**](AwEventbookingEventTicket.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingEventTicketPrototypeUpdateByIdAttributes"></a>
# **awEventbookingEventTicketPrototypeUpdateByIdAttributes**
> AwEventbookingEventTicketAttribute awEventbookingEventTicketPrototypeUpdateByIdAttributes(fk, id, data)

Update a related item by id for attributes.

### Example
```java
// Import classes:
//import com.harpoon.api.AwEventbookingEventTicketApi;

AwEventbookingEventTicketApi apiInstance = new AwEventbookingEventTicketApi();
String fk = "fk_example"; // String | Foreign key for attributes
String id = "id_example"; // String | AwEventbookingEventTicket id
AwEventbookingEventTicketAttribute data = new AwEventbookingEventTicketAttribute(); // AwEventbookingEventTicketAttribute | 
try {
    AwEventbookingEventTicketAttribute result = apiInstance.awEventbookingEventTicketPrototypeUpdateByIdAttributes(fk, id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AwEventbookingEventTicketApi#awEventbookingEventTicketPrototypeUpdateByIdAttributes");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for attributes |
 **id** | **String**| AwEventbookingEventTicket id |
 **data** | [**AwEventbookingEventTicketAttribute**](AwEventbookingEventTicketAttribute.md)|  | [optional]

### Return type

[**AwEventbookingEventTicketAttribute**](AwEventbookingEventTicketAttribute.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingEventTicketPrototypeUpdateByIdAwEventbookingTicket"></a>
# **awEventbookingEventTicketPrototypeUpdateByIdAwEventbookingTicket**
> AwEventbookingTicket awEventbookingEventTicketPrototypeUpdateByIdAwEventbookingTicket(fk, id, data)

Update a related item by id for awEventbookingTicket.

### Example
```java
// Import classes:
//import com.harpoon.api.AwEventbookingEventTicketApi;

AwEventbookingEventTicketApi apiInstance = new AwEventbookingEventTicketApi();
String fk = "fk_example"; // String | Foreign key for awEventbookingTicket
String id = "id_example"; // String | AwEventbookingEventTicket id
AwEventbookingTicket data = new AwEventbookingTicket(); // AwEventbookingTicket | 
try {
    AwEventbookingTicket result = apiInstance.awEventbookingEventTicketPrototypeUpdateByIdAwEventbookingTicket(fk, id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AwEventbookingEventTicketApi#awEventbookingEventTicketPrototypeUpdateByIdAwEventbookingTicket");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for awEventbookingTicket |
 **id** | **String**| AwEventbookingEventTicket id |
 **data** | [**AwEventbookingTicket**](AwEventbookingTicket.md)|  | [optional]

### Return type

[**AwEventbookingTicket**](AwEventbookingTicket.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingEventTicketPrototypeUpdateByIdTickets"></a>
# **awEventbookingEventTicketPrototypeUpdateByIdTickets**
> AwEventbookingTicket awEventbookingEventTicketPrototypeUpdateByIdTickets(fk, id, data)

Update a related item by id for tickets.

### Example
```java
// Import classes:
//import com.harpoon.api.AwEventbookingEventTicketApi;

AwEventbookingEventTicketApi apiInstance = new AwEventbookingEventTicketApi();
String fk = "fk_example"; // String | Foreign key for tickets
String id = "id_example"; // String | AwEventbookingEventTicket id
AwEventbookingTicket data = new AwEventbookingTicket(); // AwEventbookingTicket | 
try {
    AwEventbookingTicket result = apiInstance.awEventbookingEventTicketPrototypeUpdateByIdTickets(fk, id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AwEventbookingEventTicketApi#awEventbookingEventTicketPrototypeUpdateByIdTickets");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for tickets |
 **id** | **String**| AwEventbookingEventTicket id |
 **data** | [**AwEventbookingTicket**](AwEventbookingTicket.md)|  | [optional]

### Return type

[**AwEventbookingTicket**](AwEventbookingTicket.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingEventTicketReplaceById"></a>
# **awEventbookingEventTicketReplaceById**
> AwEventbookingEventTicket awEventbookingEventTicketReplaceById(id, data)

Replace attributes for a model instance and persist it into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.AwEventbookingEventTicketApi;

AwEventbookingEventTicketApi apiInstance = new AwEventbookingEventTicketApi();
String id = "id_example"; // String | Model id
AwEventbookingEventTicket data = new AwEventbookingEventTicket(); // AwEventbookingEventTicket | Model instance data
try {
    AwEventbookingEventTicket result = apiInstance.awEventbookingEventTicketReplaceById(id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AwEventbookingEventTicketApi#awEventbookingEventTicketReplaceById");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |
 **data** | [**AwEventbookingEventTicket**](AwEventbookingEventTicket.md)| Model instance data | [optional]

### Return type

[**AwEventbookingEventTicket**](AwEventbookingEventTicket.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingEventTicketReplaceOrCreate"></a>
# **awEventbookingEventTicketReplaceOrCreate**
> AwEventbookingEventTicket awEventbookingEventTicketReplaceOrCreate(data)

Replace an existing model instance or insert a new one into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.AwEventbookingEventTicketApi;

AwEventbookingEventTicketApi apiInstance = new AwEventbookingEventTicketApi();
AwEventbookingEventTicket data = new AwEventbookingEventTicket(); // AwEventbookingEventTicket | Model instance data
try {
    AwEventbookingEventTicket result = apiInstance.awEventbookingEventTicketReplaceOrCreate(data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AwEventbookingEventTicketApi#awEventbookingEventTicketReplaceOrCreate");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**AwEventbookingEventTicket**](AwEventbookingEventTicket.md)| Model instance data | [optional]

### Return type

[**AwEventbookingEventTicket**](AwEventbookingEventTicket.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingEventTicketUpdateAll"></a>
# **awEventbookingEventTicketUpdateAll**
> InlineResponse2002 awEventbookingEventTicketUpdateAll(where, data)

Update instances of the model matched by {{where}} from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.AwEventbookingEventTicketApi;

AwEventbookingEventTicketApi apiInstance = new AwEventbookingEventTicketApi();
String where = "where_example"; // String | Criteria to match model instances
AwEventbookingEventTicket data = new AwEventbookingEventTicket(); // AwEventbookingEventTicket | An object of model property name/value pairs
try {
    InlineResponse2002 result = apiInstance.awEventbookingEventTicketUpdateAll(where, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AwEventbookingEventTicketApi#awEventbookingEventTicketUpdateAll");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional]
 **data** | [**AwEventbookingEventTicket**](AwEventbookingEventTicket.md)| An object of model property name/value pairs | [optional]

### Return type

[**InlineResponse2002**](InlineResponse2002.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingEventTicketUpsertPatchAwEventbookingEventTickets"></a>
# **awEventbookingEventTicketUpsertPatchAwEventbookingEventTickets**
> AwEventbookingEventTicket awEventbookingEventTicketUpsertPatchAwEventbookingEventTickets(data)

Patch an existing model instance or insert a new one into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.AwEventbookingEventTicketApi;

AwEventbookingEventTicketApi apiInstance = new AwEventbookingEventTicketApi();
AwEventbookingEventTicket data = new AwEventbookingEventTicket(); // AwEventbookingEventTicket | Model instance data
try {
    AwEventbookingEventTicket result = apiInstance.awEventbookingEventTicketUpsertPatchAwEventbookingEventTickets(data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AwEventbookingEventTicketApi#awEventbookingEventTicketUpsertPatchAwEventbookingEventTickets");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**AwEventbookingEventTicket**](AwEventbookingEventTicket.md)| Model instance data | [optional]

### Return type

[**AwEventbookingEventTicket**](AwEventbookingEventTicket.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingEventTicketUpsertPutAwEventbookingEventTickets"></a>
# **awEventbookingEventTicketUpsertPutAwEventbookingEventTickets**
> AwEventbookingEventTicket awEventbookingEventTicketUpsertPutAwEventbookingEventTickets(data)

Patch an existing model instance or insert a new one into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.AwEventbookingEventTicketApi;

AwEventbookingEventTicketApi apiInstance = new AwEventbookingEventTicketApi();
AwEventbookingEventTicket data = new AwEventbookingEventTicket(); // AwEventbookingEventTicket | Model instance data
try {
    AwEventbookingEventTicket result = apiInstance.awEventbookingEventTicketUpsertPutAwEventbookingEventTickets(data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AwEventbookingEventTicketApi#awEventbookingEventTicketUpsertPutAwEventbookingEventTickets");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**AwEventbookingEventTicket**](AwEventbookingEventTicket.md)| Model instance data | [optional]

### Return type

[**AwEventbookingEventTicket**](AwEventbookingEventTicket.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingEventTicketUpsertWithWhere"></a>
# **awEventbookingEventTicketUpsertWithWhere**
> AwEventbookingEventTicket awEventbookingEventTicketUpsertWithWhere(where, data)

Update an existing model instance or insert a new one into the data source based on the where criteria.

### Example
```java
// Import classes:
//import com.harpoon.api.AwEventbookingEventTicketApi;

AwEventbookingEventTicketApi apiInstance = new AwEventbookingEventTicketApi();
String where = "where_example"; // String | Criteria to match model instances
AwEventbookingEventTicket data = new AwEventbookingEventTicket(); // AwEventbookingEventTicket | An object of model property name/value pairs
try {
    AwEventbookingEventTicket result = apiInstance.awEventbookingEventTicketUpsertWithWhere(where, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AwEventbookingEventTicketApi#awEventbookingEventTicketUpsertWithWhere");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional]
 **data** | [**AwEventbookingEventTicket**](AwEventbookingEventTicket.md)| An object of model property name/value pairs | [optional]

### Return type

[**AwEventbookingEventTicket**](AwEventbookingEventTicket.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

