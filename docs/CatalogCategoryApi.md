# CatalogCategoryApi

All URIs are relative to *https://apicdn.harpoonconnect.com/v1.1.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**catalogCategoryCount**](CatalogCategoryApi.md#catalogCategoryCount) | **GET** /CatalogCategories/count | Count instances of the model matched by where from the data source.
[**catalogCategoryCreate**](CatalogCategoryApi.md#catalogCategoryCreate) | **POST** /CatalogCategories | Create a new instance of the model and persist it into the data source.
[**catalogCategoryCreateChangeStreamGetCatalogCategoriesChangeStream**](CatalogCategoryApi.md#catalogCategoryCreateChangeStreamGetCatalogCategoriesChangeStream) | **GET** /CatalogCategories/change-stream | Create a change stream.
[**catalogCategoryCreateChangeStreamPostCatalogCategoriesChangeStream**](CatalogCategoryApi.md#catalogCategoryCreateChangeStreamPostCatalogCategoriesChangeStream) | **POST** /CatalogCategories/change-stream | Create a change stream.
[**catalogCategoryDeleteById**](CatalogCategoryApi.md#catalogCategoryDeleteById) | **DELETE** /CatalogCategories/{id} | Delete a model instance by {{id}} from the data source.
[**catalogCategoryExistsGetCatalogCategoriesidExists**](CatalogCategoryApi.md#catalogCategoryExistsGetCatalogCategoriesidExists) | **GET** /CatalogCategories/{id}/exists | Check whether a model instance exists in the data source.
[**catalogCategoryExistsHeadCatalogCategoriesid**](CatalogCategoryApi.md#catalogCategoryExistsHeadCatalogCategoriesid) | **HEAD** /CatalogCategories/{id} | Check whether a model instance exists in the data source.
[**catalogCategoryFind**](CatalogCategoryApi.md#catalogCategoryFind) | **GET** /CatalogCategories | Find all instances of the model matched by filter from the data source.
[**catalogCategoryFindById**](CatalogCategoryApi.md#catalogCategoryFindById) | **GET** /CatalogCategories/{id} | Find a model instance by {{id}} from the data source.
[**catalogCategoryFindOne**](CatalogCategoryApi.md#catalogCategoryFindOne) | **GET** /CatalogCategories/findOne | Find first instance of the model matched by filter from the data source.
[**catalogCategoryPrototypeCountCatalogProducts**](CatalogCategoryApi.md#catalogCategoryPrototypeCountCatalogProducts) | **GET** /CatalogCategories/{id}/catalogProducts/count | Counts catalogProducts of CatalogCategory.
[**catalogCategoryPrototypeCreateCatalogProducts**](CatalogCategoryApi.md#catalogCategoryPrototypeCreateCatalogProducts) | **POST** /CatalogCategories/{id}/catalogProducts | Creates a new instance in catalogProducts of this model.
[**catalogCategoryPrototypeDeleteCatalogProducts**](CatalogCategoryApi.md#catalogCategoryPrototypeDeleteCatalogProducts) | **DELETE** /CatalogCategories/{id}/catalogProducts | Deletes all catalogProducts of this model.
[**catalogCategoryPrototypeDestroyByIdCatalogProducts**](CatalogCategoryApi.md#catalogCategoryPrototypeDestroyByIdCatalogProducts) | **DELETE** /CatalogCategories/{id}/catalogProducts/{fk} | Delete a related item by id for catalogProducts.
[**catalogCategoryPrototypeExistsCatalogProducts**](CatalogCategoryApi.md#catalogCategoryPrototypeExistsCatalogProducts) | **HEAD** /CatalogCategories/{id}/catalogProducts/rel/{fk} | Check the existence of catalogProducts relation to an item by id.
[**catalogCategoryPrototypeFindByIdCatalogProducts**](CatalogCategoryApi.md#catalogCategoryPrototypeFindByIdCatalogProducts) | **GET** /CatalogCategories/{id}/catalogProducts/{fk} | Find a related item by id for catalogProducts.
[**catalogCategoryPrototypeGetCatalogProducts**](CatalogCategoryApi.md#catalogCategoryPrototypeGetCatalogProducts) | **GET** /CatalogCategories/{id}/catalogProducts | Queries catalogProducts of CatalogCategory.
[**catalogCategoryPrototypeLinkCatalogProducts**](CatalogCategoryApi.md#catalogCategoryPrototypeLinkCatalogProducts) | **PUT** /CatalogCategories/{id}/catalogProducts/rel/{fk} | Add a related item by id for catalogProducts.
[**catalogCategoryPrototypeUnlinkCatalogProducts**](CatalogCategoryApi.md#catalogCategoryPrototypeUnlinkCatalogProducts) | **DELETE** /CatalogCategories/{id}/catalogProducts/rel/{fk} | Remove the catalogProducts relation to an item by id.
[**catalogCategoryPrototypeUpdateAttributesPatchCatalogCategoriesid**](CatalogCategoryApi.md#catalogCategoryPrototypeUpdateAttributesPatchCatalogCategoriesid) | **PATCH** /CatalogCategories/{id} | Patch attributes for a model instance and persist it into the data source.
[**catalogCategoryPrototypeUpdateAttributesPutCatalogCategoriesid**](CatalogCategoryApi.md#catalogCategoryPrototypeUpdateAttributesPutCatalogCategoriesid) | **PUT** /CatalogCategories/{id} | Patch attributes for a model instance and persist it into the data source.
[**catalogCategoryPrototypeUpdateByIdCatalogProducts**](CatalogCategoryApi.md#catalogCategoryPrototypeUpdateByIdCatalogProducts) | **PUT** /CatalogCategories/{id}/catalogProducts/{fk} | Update a related item by id for catalogProducts.
[**catalogCategoryReplaceById**](CatalogCategoryApi.md#catalogCategoryReplaceById) | **POST** /CatalogCategories/{id}/replace | Replace attributes for a model instance and persist it into the data source.
[**catalogCategoryReplaceOrCreate**](CatalogCategoryApi.md#catalogCategoryReplaceOrCreate) | **POST** /CatalogCategories/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
[**catalogCategoryUpdateAll**](CatalogCategoryApi.md#catalogCategoryUpdateAll) | **POST** /CatalogCategories/update | Update instances of the model matched by {{where}} from the data source.
[**catalogCategoryUpsertPatchCatalogCategories**](CatalogCategoryApi.md#catalogCategoryUpsertPatchCatalogCategories) | **PATCH** /CatalogCategories | Patch an existing model instance or insert a new one into the data source.
[**catalogCategoryUpsertPutCatalogCategories**](CatalogCategoryApi.md#catalogCategoryUpsertPutCatalogCategories) | **PUT** /CatalogCategories | Patch an existing model instance or insert a new one into the data source.
[**catalogCategoryUpsertWithWhere**](CatalogCategoryApi.md#catalogCategoryUpsertWithWhere) | **POST** /CatalogCategories/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.


<a name="catalogCategoryCount"></a>
# **catalogCategoryCount**
> InlineResponse2001 catalogCategoryCount(where)

Count instances of the model matched by where from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.CatalogCategoryApi;

CatalogCategoryApi apiInstance = new CatalogCategoryApi();
String where = "where_example"; // String | Criteria to match model instances
try {
    InlineResponse2001 result = apiInstance.catalogCategoryCount(where);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CatalogCategoryApi#catalogCategoryCount");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional]

### Return type

[**InlineResponse2001**](InlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="catalogCategoryCreate"></a>
# **catalogCategoryCreate**
> CatalogCategory catalogCategoryCreate(data)

Create a new instance of the model and persist it into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.CatalogCategoryApi;

CatalogCategoryApi apiInstance = new CatalogCategoryApi();
CatalogCategory data = new CatalogCategory(); // CatalogCategory | Model instance data
try {
    CatalogCategory result = apiInstance.catalogCategoryCreate(data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CatalogCategoryApi#catalogCategoryCreate");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**CatalogCategory**](CatalogCategory.md)| Model instance data | [optional]

### Return type

[**CatalogCategory**](CatalogCategory.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="catalogCategoryCreateChangeStreamGetCatalogCategoriesChangeStream"></a>
# **catalogCategoryCreateChangeStreamGetCatalogCategoriesChangeStream**
> File catalogCategoryCreateChangeStreamGetCatalogCategoriesChangeStream(options)

Create a change stream.

### Example
```java
// Import classes:
//import com.harpoon.api.CatalogCategoryApi;

CatalogCategoryApi apiInstance = new CatalogCategoryApi();
String options = "options_example"; // String | 
try {
    File result = apiInstance.catalogCategoryCreateChangeStreamGetCatalogCategoriesChangeStream(options);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CatalogCategoryApi#catalogCategoryCreateChangeStreamGetCatalogCategoriesChangeStream");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **String**|  | [optional]

### Return type

[**File**](File.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="catalogCategoryCreateChangeStreamPostCatalogCategoriesChangeStream"></a>
# **catalogCategoryCreateChangeStreamPostCatalogCategoriesChangeStream**
> File catalogCategoryCreateChangeStreamPostCatalogCategoriesChangeStream(options)

Create a change stream.

### Example
```java
// Import classes:
//import com.harpoon.api.CatalogCategoryApi;

CatalogCategoryApi apiInstance = new CatalogCategoryApi();
String options = "options_example"; // String | 
try {
    File result = apiInstance.catalogCategoryCreateChangeStreamPostCatalogCategoriesChangeStream(options);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CatalogCategoryApi#catalogCategoryCreateChangeStreamPostCatalogCategoriesChangeStream");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **String**|  | [optional]

### Return type

[**File**](File.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="catalogCategoryDeleteById"></a>
# **catalogCategoryDeleteById**
> Object catalogCategoryDeleteById(id)

Delete a model instance by {{id}} from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.CatalogCategoryApi;

CatalogCategoryApi apiInstance = new CatalogCategoryApi();
String id = "id_example"; // String | Model id
try {
    Object result = apiInstance.catalogCategoryDeleteById(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CatalogCategoryApi#catalogCategoryDeleteById");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |

### Return type

**Object**

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="catalogCategoryExistsGetCatalogCategoriesidExists"></a>
# **catalogCategoryExistsGetCatalogCategoriesidExists**
> InlineResponse2003 catalogCategoryExistsGetCatalogCategoriesidExists(id)

Check whether a model instance exists in the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.CatalogCategoryApi;

CatalogCategoryApi apiInstance = new CatalogCategoryApi();
String id = "id_example"; // String | Model id
try {
    InlineResponse2003 result = apiInstance.catalogCategoryExistsGetCatalogCategoriesidExists(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CatalogCategoryApi#catalogCategoryExistsGetCatalogCategoriesidExists");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |

### Return type

[**InlineResponse2003**](InlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="catalogCategoryExistsHeadCatalogCategoriesid"></a>
# **catalogCategoryExistsHeadCatalogCategoriesid**
> InlineResponse2003 catalogCategoryExistsHeadCatalogCategoriesid(id)

Check whether a model instance exists in the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.CatalogCategoryApi;

CatalogCategoryApi apiInstance = new CatalogCategoryApi();
String id = "id_example"; // String | Model id
try {
    InlineResponse2003 result = apiInstance.catalogCategoryExistsHeadCatalogCategoriesid(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CatalogCategoryApi#catalogCategoryExistsHeadCatalogCategoriesid");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |

### Return type

[**InlineResponse2003**](InlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="catalogCategoryFind"></a>
# **catalogCategoryFind**
> List&lt;CatalogCategory&gt; catalogCategoryFind(filter)

Find all instances of the model matched by filter from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.CatalogCategoryApi;

CatalogCategoryApi apiInstance = new CatalogCategoryApi();
String filter = "filter_example"; // String | Filter defining fields, where, include, order, offset, and limit
try {
    List<CatalogCategory> result = apiInstance.catalogCategoryFind(filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CatalogCategoryApi#catalogCategoryFind");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **String**| Filter defining fields, where, include, order, offset, and limit | [optional]

### Return type

[**List&lt;CatalogCategory&gt;**](CatalogCategory.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="catalogCategoryFindById"></a>
# **catalogCategoryFindById**
> CatalogCategory catalogCategoryFindById(id, filter)

Find a model instance by {{id}} from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.CatalogCategoryApi;

CatalogCategoryApi apiInstance = new CatalogCategoryApi();
String id = "id_example"; // String | Model id
String filter = "filter_example"; // String | Filter defining fields and include
try {
    CatalogCategory result = apiInstance.catalogCategoryFindById(id, filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CatalogCategoryApi#catalogCategoryFindById");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |
 **filter** | **String**| Filter defining fields and include | [optional]

### Return type

[**CatalogCategory**](CatalogCategory.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="catalogCategoryFindOne"></a>
# **catalogCategoryFindOne**
> CatalogCategory catalogCategoryFindOne(filter)

Find first instance of the model matched by filter from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.CatalogCategoryApi;

CatalogCategoryApi apiInstance = new CatalogCategoryApi();
String filter = "filter_example"; // String | Filter defining fields, where, include, order, offset, and limit
try {
    CatalogCategory result = apiInstance.catalogCategoryFindOne(filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CatalogCategoryApi#catalogCategoryFindOne");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **String**| Filter defining fields, where, include, order, offset, and limit | [optional]

### Return type

[**CatalogCategory**](CatalogCategory.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="catalogCategoryPrototypeCountCatalogProducts"></a>
# **catalogCategoryPrototypeCountCatalogProducts**
> InlineResponse2001 catalogCategoryPrototypeCountCatalogProducts(id, where)

Counts catalogProducts of CatalogCategory.

### Example
```java
// Import classes:
//import com.harpoon.api.CatalogCategoryApi;

CatalogCategoryApi apiInstance = new CatalogCategoryApi();
String id = "id_example"; // String | CatalogCategory id
String where = "where_example"; // String | Criteria to match model instances
try {
    InlineResponse2001 result = apiInstance.catalogCategoryPrototypeCountCatalogProducts(id, where);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CatalogCategoryApi#catalogCategoryPrototypeCountCatalogProducts");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| CatalogCategory id |
 **where** | **String**| Criteria to match model instances | [optional]

### Return type

[**InlineResponse2001**](InlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="catalogCategoryPrototypeCreateCatalogProducts"></a>
# **catalogCategoryPrototypeCreateCatalogProducts**
> CatalogProduct catalogCategoryPrototypeCreateCatalogProducts(id, data)

Creates a new instance in catalogProducts of this model.

### Example
```java
// Import classes:
//import com.harpoon.api.CatalogCategoryApi;

CatalogCategoryApi apiInstance = new CatalogCategoryApi();
String id = "id_example"; // String | CatalogCategory id
CatalogProduct data = new CatalogProduct(); // CatalogProduct | 
try {
    CatalogProduct result = apiInstance.catalogCategoryPrototypeCreateCatalogProducts(id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CatalogCategoryApi#catalogCategoryPrototypeCreateCatalogProducts");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| CatalogCategory id |
 **data** | [**CatalogProduct**](CatalogProduct.md)|  | [optional]

### Return type

[**CatalogProduct**](CatalogProduct.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="catalogCategoryPrototypeDeleteCatalogProducts"></a>
# **catalogCategoryPrototypeDeleteCatalogProducts**
> catalogCategoryPrototypeDeleteCatalogProducts(id)

Deletes all catalogProducts of this model.

### Example
```java
// Import classes:
//import com.harpoon.api.CatalogCategoryApi;

CatalogCategoryApi apiInstance = new CatalogCategoryApi();
String id = "id_example"; // String | CatalogCategory id
try {
    apiInstance.catalogCategoryPrototypeDeleteCatalogProducts(id);
} catch (ApiException e) {
    System.err.println("Exception when calling CatalogCategoryApi#catalogCategoryPrototypeDeleteCatalogProducts");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| CatalogCategory id |

### Return type

null (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="catalogCategoryPrototypeDestroyByIdCatalogProducts"></a>
# **catalogCategoryPrototypeDestroyByIdCatalogProducts**
> catalogCategoryPrototypeDestroyByIdCatalogProducts(fk, id)

Delete a related item by id for catalogProducts.

### Example
```java
// Import classes:
//import com.harpoon.api.CatalogCategoryApi;

CatalogCategoryApi apiInstance = new CatalogCategoryApi();
String fk = "fk_example"; // String | Foreign key for catalogProducts
String id = "id_example"; // String | CatalogCategory id
try {
    apiInstance.catalogCategoryPrototypeDestroyByIdCatalogProducts(fk, id);
} catch (ApiException e) {
    System.err.println("Exception when calling CatalogCategoryApi#catalogCategoryPrototypeDestroyByIdCatalogProducts");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for catalogProducts |
 **id** | **String**| CatalogCategory id |

### Return type

null (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="catalogCategoryPrototypeExistsCatalogProducts"></a>
# **catalogCategoryPrototypeExistsCatalogProducts**
> Boolean catalogCategoryPrototypeExistsCatalogProducts(fk, id)

Check the existence of catalogProducts relation to an item by id.

### Example
```java
// Import classes:
//import com.harpoon.api.CatalogCategoryApi;

CatalogCategoryApi apiInstance = new CatalogCategoryApi();
String fk = "fk_example"; // String | Foreign key for catalogProducts
String id = "id_example"; // String | CatalogCategory id
try {
    Boolean result = apiInstance.catalogCategoryPrototypeExistsCatalogProducts(fk, id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CatalogCategoryApi#catalogCategoryPrototypeExistsCatalogProducts");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for catalogProducts |
 **id** | **String**| CatalogCategory id |

### Return type

**Boolean**

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="catalogCategoryPrototypeFindByIdCatalogProducts"></a>
# **catalogCategoryPrototypeFindByIdCatalogProducts**
> CatalogProduct catalogCategoryPrototypeFindByIdCatalogProducts(fk, id)

Find a related item by id for catalogProducts.

### Example
```java
// Import classes:
//import com.harpoon.api.CatalogCategoryApi;

CatalogCategoryApi apiInstance = new CatalogCategoryApi();
String fk = "fk_example"; // String | Foreign key for catalogProducts
String id = "id_example"; // String | CatalogCategory id
try {
    CatalogProduct result = apiInstance.catalogCategoryPrototypeFindByIdCatalogProducts(fk, id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CatalogCategoryApi#catalogCategoryPrototypeFindByIdCatalogProducts");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for catalogProducts |
 **id** | **String**| CatalogCategory id |

### Return type

[**CatalogProduct**](CatalogProduct.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="catalogCategoryPrototypeGetCatalogProducts"></a>
# **catalogCategoryPrototypeGetCatalogProducts**
> List&lt;CatalogProduct&gt; catalogCategoryPrototypeGetCatalogProducts(id, filter)

Queries catalogProducts of CatalogCategory.

### Example
```java
// Import classes:
//import com.harpoon.api.CatalogCategoryApi;

CatalogCategoryApi apiInstance = new CatalogCategoryApi();
String id = "id_example"; // String | CatalogCategory id
String filter = "filter_example"; // String | 
try {
    List<CatalogProduct> result = apiInstance.catalogCategoryPrototypeGetCatalogProducts(id, filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CatalogCategoryApi#catalogCategoryPrototypeGetCatalogProducts");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| CatalogCategory id |
 **filter** | **String**|  | [optional]

### Return type

[**List&lt;CatalogProduct&gt;**](CatalogProduct.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="catalogCategoryPrototypeLinkCatalogProducts"></a>
# **catalogCategoryPrototypeLinkCatalogProducts**
> CatalogCategoryProduct catalogCategoryPrototypeLinkCatalogProducts(fk, id, data)

Add a related item by id for catalogProducts.

### Example
```java
// Import classes:
//import com.harpoon.api.CatalogCategoryApi;

CatalogCategoryApi apiInstance = new CatalogCategoryApi();
String fk = "fk_example"; // String | Foreign key for catalogProducts
String id = "id_example"; // String | CatalogCategory id
CatalogCategoryProduct data = new CatalogCategoryProduct(); // CatalogCategoryProduct | 
try {
    CatalogCategoryProduct result = apiInstance.catalogCategoryPrototypeLinkCatalogProducts(fk, id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CatalogCategoryApi#catalogCategoryPrototypeLinkCatalogProducts");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for catalogProducts |
 **id** | **String**| CatalogCategory id |
 **data** | [**CatalogCategoryProduct**](CatalogCategoryProduct.md)|  | [optional]

### Return type

[**CatalogCategoryProduct**](CatalogCategoryProduct.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="catalogCategoryPrototypeUnlinkCatalogProducts"></a>
# **catalogCategoryPrototypeUnlinkCatalogProducts**
> catalogCategoryPrototypeUnlinkCatalogProducts(fk, id)

Remove the catalogProducts relation to an item by id.

### Example
```java
// Import classes:
//import com.harpoon.api.CatalogCategoryApi;

CatalogCategoryApi apiInstance = new CatalogCategoryApi();
String fk = "fk_example"; // String | Foreign key for catalogProducts
String id = "id_example"; // String | CatalogCategory id
try {
    apiInstance.catalogCategoryPrototypeUnlinkCatalogProducts(fk, id);
} catch (ApiException e) {
    System.err.println("Exception when calling CatalogCategoryApi#catalogCategoryPrototypeUnlinkCatalogProducts");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for catalogProducts |
 **id** | **String**| CatalogCategory id |

### Return type

null (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="catalogCategoryPrototypeUpdateAttributesPatchCatalogCategoriesid"></a>
# **catalogCategoryPrototypeUpdateAttributesPatchCatalogCategoriesid**
> CatalogCategory catalogCategoryPrototypeUpdateAttributesPatchCatalogCategoriesid(id, data)

Patch attributes for a model instance and persist it into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.CatalogCategoryApi;

CatalogCategoryApi apiInstance = new CatalogCategoryApi();
String id = "id_example"; // String | CatalogCategory id
CatalogCategory data = new CatalogCategory(); // CatalogCategory | An object of model property name/value pairs
try {
    CatalogCategory result = apiInstance.catalogCategoryPrototypeUpdateAttributesPatchCatalogCategoriesid(id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CatalogCategoryApi#catalogCategoryPrototypeUpdateAttributesPatchCatalogCategoriesid");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| CatalogCategory id |
 **data** | [**CatalogCategory**](CatalogCategory.md)| An object of model property name/value pairs | [optional]

### Return type

[**CatalogCategory**](CatalogCategory.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="catalogCategoryPrototypeUpdateAttributesPutCatalogCategoriesid"></a>
# **catalogCategoryPrototypeUpdateAttributesPutCatalogCategoriesid**
> CatalogCategory catalogCategoryPrototypeUpdateAttributesPutCatalogCategoriesid(id, data)

Patch attributes for a model instance and persist it into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.CatalogCategoryApi;

CatalogCategoryApi apiInstance = new CatalogCategoryApi();
String id = "id_example"; // String | CatalogCategory id
CatalogCategory data = new CatalogCategory(); // CatalogCategory | An object of model property name/value pairs
try {
    CatalogCategory result = apiInstance.catalogCategoryPrototypeUpdateAttributesPutCatalogCategoriesid(id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CatalogCategoryApi#catalogCategoryPrototypeUpdateAttributesPutCatalogCategoriesid");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| CatalogCategory id |
 **data** | [**CatalogCategory**](CatalogCategory.md)| An object of model property name/value pairs | [optional]

### Return type

[**CatalogCategory**](CatalogCategory.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="catalogCategoryPrototypeUpdateByIdCatalogProducts"></a>
# **catalogCategoryPrototypeUpdateByIdCatalogProducts**
> CatalogProduct catalogCategoryPrototypeUpdateByIdCatalogProducts(fk, id, data)

Update a related item by id for catalogProducts.

### Example
```java
// Import classes:
//import com.harpoon.api.CatalogCategoryApi;

CatalogCategoryApi apiInstance = new CatalogCategoryApi();
String fk = "fk_example"; // String | Foreign key for catalogProducts
String id = "id_example"; // String | CatalogCategory id
CatalogProduct data = new CatalogProduct(); // CatalogProduct | 
try {
    CatalogProduct result = apiInstance.catalogCategoryPrototypeUpdateByIdCatalogProducts(fk, id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CatalogCategoryApi#catalogCategoryPrototypeUpdateByIdCatalogProducts");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for catalogProducts |
 **id** | **String**| CatalogCategory id |
 **data** | [**CatalogProduct**](CatalogProduct.md)|  | [optional]

### Return type

[**CatalogProduct**](CatalogProduct.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="catalogCategoryReplaceById"></a>
# **catalogCategoryReplaceById**
> CatalogCategory catalogCategoryReplaceById(id, data)

Replace attributes for a model instance and persist it into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.CatalogCategoryApi;

CatalogCategoryApi apiInstance = new CatalogCategoryApi();
String id = "id_example"; // String | Model id
CatalogCategory data = new CatalogCategory(); // CatalogCategory | Model instance data
try {
    CatalogCategory result = apiInstance.catalogCategoryReplaceById(id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CatalogCategoryApi#catalogCategoryReplaceById");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |
 **data** | [**CatalogCategory**](CatalogCategory.md)| Model instance data | [optional]

### Return type

[**CatalogCategory**](CatalogCategory.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="catalogCategoryReplaceOrCreate"></a>
# **catalogCategoryReplaceOrCreate**
> CatalogCategory catalogCategoryReplaceOrCreate(data)

Replace an existing model instance or insert a new one into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.CatalogCategoryApi;

CatalogCategoryApi apiInstance = new CatalogCategoryApi();
CatalogCategory data = new CatalogCategory(); // CatalogCategory | Model instance data
try {
    CatalogCategory result = apiInstance.catalogCategoryReplaceOrCreate(data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CatalogCategoryApi#catalogCategoryReplaceOrCreate");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**CatalogCategory**](CatalogCategory.md)| Model instance data | [optional]

### Return type

[**CatalogCategory**](CatalogCategory.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="catalogCategoryUpdateAll"></a>
# **catalogCategoryUpdateAll**
> InlineResponse2002 catalogCategoryUpdateAll(where, data)

Update instances of the model matched by {{where}} from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.CatalogCategoryApi;

CatalogCategoryApi apiInstance = new CatalogCategoryApi();
String where = "where_example"; // String | Criteria to match model instances
CatalogCategory data = new CatalogCategory(); // CatalogCategory | An object of model property name/value pairs
try {
    InlineResponse2002 result = apiInstance.catalogCategoryUpdateAll(where, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CatalogCategoryApi#catalogCategoryUpdateAll");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional]
 **data** | [**CatalogCategory**](CatalogCategory.md)| An object of model property name/value pairs | [optional]

### Return type

[**InlineResponse2002**](InlineResponse2002.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="catalogCategoryUpsertPatchCatalogCategories"></a>
# **catalogCategoryUpsertPatchCatalogCategories**
> CatalogCategory catalogCategoryUpsertPatchCatalogCategories(data)

Patch an existing model instance or insert a new one into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.CatalogCategoryApi;

CatalogCategoryApi apiInstance = new CatalogCategoryApi();
CatalogCategory data = new CatalogCategory(); // CatalogCategory | Model instance data
try {
    CatalogCategory result = apiInstance.catalogCategoryUpsertPatchCatalogCategories(data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CatalogCategoryApi#catalogCategoryUpsertPatchCatalogCategories");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**CatalogCategory**](CatalogCategory.md)| Model instance data | [optional]

### Return type

[**CatalogCategory**](CatalogCategory.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="catalogCategoryUpsertPutCatalogCategories"></a>
# **catalogCategoryUpsertPutCatalogCategories**
> CatalogCategory catalogCategoryUpsertPutCatalogCategories(data)

Patch an existing model instance or insert a new one into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.CatalogCategoryApi;

CatalogCategoryApi apiInstance = new CatalogCategoryApi();
CatalogCategory data = new CatalogCategory(); // CatalogCategory | Model instance data
try {
    CatalogCategory result = apiInstance.catalogCategoryUpsertPutCatalogCategories(data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CatalogCategoryApi#catalogCategoryUpsertPutCatalogCategories");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**CatalogCategory**](CatalogCategory.md)| Model instance data | [optional]

### Return type

[**CatalogCategory**](CatalogCategory.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="catalogCategoryUpsertWithWhere"></a>
# **catalogCategoryUpsertWithWhere**
> CatalogCategory catalogCategoryUpsertWithWhere(where, data)

Update an existing model instance or insert a new one into the data source based on the where criteria.

### Example
```java
// Import classes:
//import com.harpoon.api.CatalogCategoryApi;

CatalogCategoryApi apiInstance = new CatalogCategoryApi();
String where = "where_example"; // String | Criteria to match model instances
CatalogCategory data = new CatalogCategory(); // CatalogCategory | An object of model property name/value pairs
try {
    CatalogCategory result = apiInstance.catalogCategoryUpsertWithWhere(where, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CatalogCategoryApi#catalogCategoryUpsertWithWhere");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional]
 **data** | [**CatalogCategory**](CatalogCategory.md)| An object of model property name/value pairs | [optional]

### Return type

[**CatalogCategory**](CatalogCategory.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

