
# CustomerPrivacy

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**activity** | **String** |  | 
**notificationPush** | **Boolean** |  |  [optional]
**notificationBeacon** | **Boolean** |  |  [optional]
**notificationLocation** | **Boolean** |  |  [optional]



