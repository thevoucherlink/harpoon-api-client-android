
# CatalogCategoryProduct

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**categoryId** | **Double** |  | 
**productId** | **Double** |  | 
**position** | **Double** |  | 
**catalogProduct** | **Object** |  |  [optional]
**catalogCategory** | **Object** |  |  [optional]



