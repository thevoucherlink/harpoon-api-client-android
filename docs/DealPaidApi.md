# DealPaidApi

All URIs are relative to *https://apicdn.harpoonconnect.com/v1.1.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**dealPaidCheckout**](DealPaidApi.md#dealPaidCheckout) | **POST** /DealPaids/{id}/checkout | 
[**dealPaidFind**](DealPaidApi.md#dealPaidFind) | **GET** /DealPaids | Find all instances of the model matched by filter from the data source.
[**dealPaidFindById**](DealPaidApi.md#dealPaidFindById) | **GET** /DealPaids/{id} | Find a model instance by {{id}} from the data source.
[**dealPaidFindOne**](DealPaidApi.md#dealPaidFindOne) | **GET** /DealPaids/findOne | Find first instance of the model matched by filter from the data source.
[**dealPaidRedeem**](DealPaidApi.md#dealPaidRedeem) | **POST** /DealPaids/{id}/redeem | 
[**dealPaidReplaceById**](DealPaidApi.md#dealPaidReplaceById) | **POST** /DealPaids/{id}/replace | Replace attributes for a model instance and persist it into the data source.
[**dealPaidReplaceOrCreate**](DealPaidApi.md#dealPaidReplaceOrCreate) | **POST** /DealPaids/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
[**dealPaidUpsertWithWhere**](DealPaidApi.md#dealPaidUpsertWithWhere) | **POST** /DealPaids/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.
[**dealPaidVenues**](DealPaidApi.md#dealPaidVenues) | **GET** /DealPaids/{id}/venues | 


<a name="dealPaidCheckout"></a>
# **dealPaidCheckout**
> DealPaidCheckout dealPaidCheckout(id, data)



### Example
```java
// Import classes:
//import com.harpoon.api.DealPaidApi;

DealPaidApi apiInstance = new DealPaidApi();
String id = "id_example"; // String | Model id
DealPaidCheckoutData data = new DealPaidCheckoutData(); // DealPaidCheckoutData | 
try {
    DealPaidCheckout result = apiInstance.dealPaidCheckout(id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DealPaidApi#dealPaidCheckout");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |
 **data** | [**DealPaidCheckoutData**](DealPaidCheckoutData.md)|  | [optional]

### Return type

[**DealPaidCheckout**](DealPaidCheckout.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="dealPaidFind"></a>
# **dealPaidFind**
> List&lt;DealPaid&gt; dealPaidFind(filter)

Find all instances of the model matched by filter from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.DealPaidApi;

DealPaidApi apiInstance = new DealPaidApi();
String filter = "filter_example"; // String | Filter defining fields, where, include, order, offset, and limit
try {
    List<DealPaid> result = apiInstance.dealPaidFind(filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DealPaidApi#dealPaidFind");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **String**| Filter defining fields, where, include, order, offset, and limit | [optional]

### Return type

[**List&lt;DealPaid&gt;**](DealPaid.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="dealPaidFindById"></a>
# **dealPaidFindById**
> DealPaid dealPaidFindById(id, filter)

Find a model instance by {{id}} from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.DealPaidApi;

DealPaidApi apiInstance = new DealPaidApi();
String id = "id_example"; // String | Model id
String filter = "filter_example"; // String | Filter defining fields and include
try {
    DealPaid result = apiInstance.dealPaidFindById(id, filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DealPaidApi#dealPaidFindById");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |
 **filter** | **String**| Filter defining fields and include | [optional]

### Return type

[**DealPaid**](DealPaid.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="dealPaidFindOne"></a>
# **dealPaidFindOne**
> DealPaid dealPaidFindOne(filter)

Find first instance of the model matched by filter from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.DealPaidApi;

DealPaidApi apiInstance = new DealPaidApi();
String filter = "filter_example"; // String | Filter defining fields, where, include, order, offset, and limit
try {
    DealPaid result = apiInstance.dealPaidFindOne(filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DealPaidApi#dealPaidFindOne");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **String**| Filter defining fields, where, include, order, offset, and limit | [optional]

### Return type

[**DealPaid**](DealPaid.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="dealPaidRedeem"></a>
# **dealPaidRedeem**
> DealPaidPurchase dealPaidRedeem(id, data)



### Example
```java
// Import classes:
//import com.harpoon.api.DealPaidApi;

DealPaidApi apiInstance = new DealPaidApi();
String id = "id_example"; // String | Model id
DealPaidRedeemData data = new DealPaidRedeemData(); // DealPaidRedeemData | 
try {
    DealPaidPurchase result = apiInstance.dealPaidRedeem(id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DealPaidApi#dealPaidRedeem");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |
 **data** | [**DealPaidRedeemData**](DealPaidRedeemData.md)|  | [optional]

### Return type

[**DealPaidPurchase**](DealPaidPurchase.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="dealPaidReplaceById"></a>
# **dealPaidReplaceById**
> DealPaid dealPaidReplaceById(id, data)

Replace attributes for a model instance and persist it into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.DealPaidApi;

DealPaidApi apiInstance = new DealPaidApi();
String id = "id_example"; // String | Model id
DealPaid data = new DealPaid(); // DealPaid | Model instance data
try {
    DealPaid result = apiInstance.dealPaidReplaceById(id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DealPaidApi#dealPaidReplaceById");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |
 **data** | [**DealPaid**](DealPaid.md)| Model instance data | [optional]

### Return type

[**DealPaid**](DealPaid.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="dealPaidReplaceOrCreate"></a>
# **dealPaidReplaceOrCreate**
> DealPaid dealPaidReplaceOrCreate(data)

Replace an existing model instance or insert a new one into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.DealPaidApi;

DealPaidApi apiInstance = new DealPaidApi();
DealPaid data = new DealPaid(); // DealPaid | Model instance data
try {
    DealPaid result = apiInstance.dealPaidReplaceOrCreate(data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DealPaidApi#dealPaidReplaceOrCreate");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**DealPaid**](DealPaid.md)| Model instance data | [optional]

### Return type

[**DealPaid**](DealPaid.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="dealPaidUpsertWithWhere"></a>
# **dealPaidUpsertWithWhere**
> DealPaid dealPaidUpsertWithWhere(where, data)

Update an existing model instance or insert a new one into the data source based on the where criteria.

### Example
```java
// Import classes:
//import com.harpoon.api.DealPaidApi;

DealPaidApi apiInstance = new DealPaidApi();
String where = "where_example"; // String | Criteria to match model instances
DealPaid data = new DealPaid(); // DealPaid | An object of model property name/value pairs
try {
    DealPaid result = apiInstance.dealPaidUpsertWithWhere(where, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DealPaidApi#dealPaidUpsertWithWhere");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional]
 **data** | [**DealPaid**](DealPaid.md)| An object of model property name/value pairs | [optional]

### Return type

[**DealPaid**](DealPaid.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="dealPaidVenues"></a>
# **dealPaidVenues**
> List&lt;Venue&gt; dealPaidVenues(id, filter)



### Example
```java
// Import classes:
//import com.harpoon.api.DealPaidApi;

DealPaidApi apiInstance = new DealPaidApi();
String id = "id_example"; // String | Model id
String filter = "filter_example"; // String | Filter defining fields and include
try {
    List<Venue> result = apiInstance.dealPaidVenues(id, filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DealPaidApi#dealPaidVenues");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |
 **filter** | **String**| Filter defining fields and include | [optional]

### Return type

[**List&lt;Venue&gt;**](Venue.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

