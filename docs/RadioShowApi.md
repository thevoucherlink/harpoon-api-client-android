# RadioShowApi

All URIs are relative to *https://apicdn.harpoonconnect.com/v1.1.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**radioShowCount**](RadioShowApi.md#radioShowCount) | **GET** /RadioShows/count | Count instances of the model matched by where from the data source.
[**radioShowCreate**](RadioShowApi.md#radioShowCreate) | **POST** /RadioShows | Create a new instance of the model and persist it into the data source.
[**radioShowCreateChangeStreamGetRadioShowsChangeStream**](RadioShowApi.md#radioShowCreateChangeStreamGetRadioShowsChangeStream) | **GET** /RadioShows/change-stream | Create a change stream.
[**radioShowCreateChangeStreamPostRadioShowsChangeStream**](RadioShowApi.md#radioShowCreateChangeStreamPostRadioShowsChangeStream) | **POST** /RadioShows/change-stream | Create a change stream.
[**radioShowDeleteById**](RadioShowApi.md#radioShowDeleteById) | **DELETE** /RadioShows/{id} | Delete a model instance by {{id}} from the data source.
[**radioShowExistsGetRadioShowsidExists**](RadioShowApi.md#radioShowExistsGetRadioShowsidExists) | **GET** /RadioShows/{id}/exists | Check whether a model instance exists in the data source.
[**radioShowExistsHeadRadioShowsid**](RadioShowApi.md#radioShowExistsHeadRadioShowsid) | **HEAD** /RadioShows/{id} | Check whether a model instance exists in the data source.
[**radioShowFind**](RadioShowApi.md#radioShowFind) | **GET** /RadioShows | Find all instances of the model matched by filter from the data source.
[**radioShowFindById**](RadioShowApi.md#radioShowFindById) | **GET** /RadioShows/{id} | Find a model instance by {{id}} from the data source.
[**radioShowFindOne**](RadioShowApi.md#radioShowFindOne) | **GET** /RadioShows/findOne | Find first instance of the model matched by filter from the data source.
[**radioShowPrototypeCountRadioPresenters**](RadioShowApi.md#radioShowPrototypeCountRadioPresenters) | **GET** /RadioShows/{id}/radioPresenters/count | Counts radioPresenters of RadioShow.
[**radioShowPrototypeCountRadioShowTimes**](RadioShowApi.md#radioShowPrototypeCountRadioShowTimes) | **GET** /RadioShows/{id}/radioShowTimes/count | Counts radioShowTimes of RadioShow.
[**radioShowPrototypeCreateRadioPresenters**](RadioShowApi.md#radioShowPrototypeCreateRadioPresenters) | **POST** /RadioShows/{id}/radioPresenters | Creates a new instance in radioPresenters of this model.
[**radioShowPrototypeCreateRadioShowTimes**](RadioShowApi.md#radioShowPrototypeCreateRadioShowTimes) | **POST** /RadioShows/{id}/radioShowTimes | Creates a new instance in radioShowTimes of this model.
[**radioShowPrototypeDeleteRadioPresenters**](RadioShowApi.md#radioShowPrototypeDeleteRadioPresenters) | **DELETE** /RadioShows/{id}/radioPresenters | Deletes all radioPresenters of this model.
[**radioShowPrototypeDeleteRadioShowTimes**](RadioShowApi.md#radioShowPrototypeDeleteRadioShowTimes) | **DELETE** /RadioShows/{id}/radioShowTimes | Deletes all radioShowTimes of this model.
[**radioShowPrototypeDestroyByIdRadioPresenters**](RadioShowApi.md#radioShowPrototypeDestroyByIdRadioPresenters) | **DELETE** /RadioShows/{id}/radioPresenters/{fk} | Delete a related item by id for radioPresenters.
[**radioShowPrototypeDestroyByIdRadioShowTimes**](RadioShowApi.md#radioShowPrototypeDestroyByIdRadioShowTimes) | **DELETE** /RadioShows/{id}/radioShowTimes/{fk} | Delete a related item by id for radioShowTimes.
[**radioShowPrototypeExistsRadioPresenters**](RadioShowApi.md#radioShowPrototypeExistsRadioPresenters) | **HEAD** /RadioShows/{id}/radioPresenters/rel/{fk} | Check the existence of radioPresenters relation to an item by id.
[**radioShowPrototypeFindByIdRadioPresenters**](RadioShowApi.md#radioShowPrototypeFindByIdRadioPresenters) | **GET** /RadioShows/{id}/radioPresenters/{fk} | Find a related item by id for radioPresenters.
[**radioShowPrototypeFindByIdRadioShowTimes**](RadioShowApi.md#radioShowPrototypeFindByIdRadioShowTimes) | **GET** /RadioShows/{id}/radioShowTimes/{fk} | Find a related item by id for radioShowTimes.
[**radioShowPrototypeGetPlaylistItem**](RadioShowApi.md#radioShowPrototypeGetPlaylistItem) | **GET** /RadioShows/{id}/playlistItem | Fetches belongsTo relation playlistItem.
[**radioShowPrototypeGetRadioPresenters**](RadioShowApi.md#radioShowPrototypeGetRadioPresenters) | **GET** /RadioShows/{id}/radioPresenters | Queries radioPresenters of RadioShow.
[**radioShowPrototypeGetRadioShowTimes**](RadioShowApi.md#radioShowPrototypeGetRadioShowTimes) | **GET** /RadioShows/{id}/radioShowTimes | Queries radioShowTimes of RadioShow.
[**radioShowPrototypeGetRadioStream**](RadioShowApi.md#radioShowPrototypeGetRadioStream) | **GET** /RadioShows/{id}/radioStream | Fetches belongsTo relation radioStream.
[**radioShowPrototypeLinkRadioPresenters**](RadioShowApi.md#radioShowPrototypeLinkRadioPresenters) | **PUT** /RadioShows/{id}/radioPresenters/rel/{fk} | Add a related item by id for radioPresenters.
[**radioShowPrototypeUnlinkRadioPresenters**](RadioShowApi.md#radioShowPrototypeUnlinkRadioPresenters) | **DELETE** /RadioShows/{id}/radioPresenters/rel/{fk} | Remove the radioPresenters relation to an item by id.
[**radioShowPrototypeUpdateAttributesPatchRadioShowsid**](RadioShowApi.md#radioShowPrototypeUpdateAttributesPatchRadioShowsid) | **PATCH** /RadioShows/{id} | Patch attributes for a model instance and persist it into the data source.
[**radioShowPrototypeUpdateAttributesPutRadioShowsid**](RadioShowApi.md#radioShowPrototypeUpdateAttributesPutRadioShowsid) | **PUT** /RadioShows/{id} | Patch attributes for a model instance and persist it into the data source.
[**radioShowPrototypeUpdateByIdRadioPresenters**](RadioShowApi.md#radioShowPrototypeUpdateByIdRadioPresenters) | **PUT** /RadioShows/{id}/radioPresenters/{fk} | Update a related item by id for radioPresenters.
[**radioShowPrototypeUpdateByIdRadioShowTimes**](RadioShowApi.md#radioShowPrototypeUpdateByIdRadioShowTimes) | **PUT** /RadioShows/{id}/radioShowTimes/{fk} | Update a related item by id for radioShowTimes.
[**radioShowReplaceById**](RadioShowApi.md#radioShowReplaceById) | **POST** /RadioShows/{id}/replace | Replace attributes for a model instance and persist it into the data source.
[**radioShowReplaceOrCreate**](RadioShowApi.md#radioShowReplaceOrCreate) | **POST** /RadioShows/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
[**radioShowUpdateAll**](RadioShowApi.md#radioShowUpdateAll) | **POST** /RadioShows/update | Update instances of the model matched by {{where}} from the data source.
[**radioShowUploadFile**](RadioShowApi.md#radioShowUploadFile) | **POST** /RadioShows/{id}/file | Upload File to Radio Show
[**radioShowUpsertPatchRadioShows**](RadioShowApi.md#radioShowUpsertPatchRadioShows) | **PATCH** /RadioShows | Patch an existing model instance or insert a new one into the data source.
[**radioShowUpsertPutRadioShows**](RadioShowApi.md#radioShowUpsertPutRadioShows) | **PUT** /RadioShows | Patch an existing model instance or insert a new one into the data source.
[**radioShowUpsertWithWhere**](RadioShowApi.md#radioShowUpsertWithWhere) | **POST** /RadioShows/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.


<a name="radioShowCount"></a>
# **radioShowCount**
> InlineResponse2001 radioShowCount(where)

Count instances of the model matched by where from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.RadioShowApi;

RadioShowApi apiInstance = new RadioShowApi();
String where = "where_example"; // String | Criteria to match model instances
try {
    InlineResponse2001 result = apiInstance.radioShowCount(where);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RadioShowApi#radioShowCount");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional]

### Return type

[**InlineResponse2001**](InlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioShowCreate"></a>
# **radioShowCreate**
> RadioShow radioShowCreate(data)

Create a new instance of the model and persist it into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.RadioShowApi;

RadioShowApi apiInstance = new RadioShowApi();
RadioShow data = new RadioShow(); // RadioShow | Model instance data
try {
    RadioShow result = apiInstance.radioShowCreate(data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RadioShowApi#radioShowCreate");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**RadioShow**](RadioShow.md)| Model instance data | [optional]

### Return type

[**RadioShow**](RadioShow.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioShowCreateChangeStreamGetRadioShowsChangeStream"></a>
# **radioShowCreateChangeStreamGetRadioShowsChangeStream**
> File radioShowCreateChangeStreamGetRadioShowsChangeStream(options)

Create a change stream.

### Example
```java
// Import classes:
//import com.harpoon.api.RadioShowApi;

RadioShowApi apiInstance = new RadioShowApi();
String options = "options_example"; // String | 
try {
    File result = apiInstance.radioShowCreateChangeStreamGetRadioShowsChangeStream(options);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RadioShowApi#radioShowCreateChangeStreamGetRadioShowsChangeStream");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **String**|  | [optional]

### Return type

[**File**](File.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioShowCreateChangeStreamPostRadioShowsChangeStream"></a>
# **radioShowCreateChangeStreamPostRadioShowsChangeStream**
> File radioShowCreateChangeStreamPostRadioShowsChangeStream(options)

Create a change stream.

### Example
```java
// Import classes:
//import com.harpoon.api.RadioShowApi;

RadioShowApi apiInstance = new RadioShowApi();
String options = "options_example"; // String | 
try {
    File result = apiInstance.radioShowCreateChangeStreamPostRadioShowsChangeStream(options);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RadioShowApi#radioShowCreateChangeStreamPostRadioShowsChangeStream");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **String**|  | [optional]

### Return type

[**File**](File.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioShowDeleteById"></a>
# **radioShowDeleteById**
> Object radioShowDeleteById(id)

Delete a model instance by {{id}} from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.RadioShowApi;

RadioShowApi apiInstance = new RadioShowApi();
String id = "id_example"; // String | Model id
try {
    Object result = apiInstance.radioShowDeleteById(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RadioShowApi#radioShowDeleteById");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |

### Return type

**Object**

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioShowExistsGetRadioShowsidExists"></a>
# **radioShowExistsGetRadioShowsidExists**
> InlineResponse2003 radioShowExistsGetRadioShowsidExists(id)

Check whether a model instance exists in the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.RadioShowApi;

RadioShowApi apiInstance = new RadioShowApi();
String id = "id_example"; // String | Model id
try {
    InlineResponse2003 result = apiInstance.radioShowExistsGetRadioShowsidExists(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RadioShowApi#radioShowExistsGetRadioShowsidExists");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |

### Return type

[**InlineResponse2003**](InlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioShowExistsHeadRadioShowsid"></a>
# **radioShowExistsHeadRadioShowsid**
> InlineResponse2003 radioShowExistsHeadRadioShowsid(id)

Check whether a model instance exists in the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.RadioShowApi;

RadioShowApi apiInstance = new RadioShowApi();
String id = "id_example"; // String | Model id
try {
    InlineResponse2003 result = apiInstance.radioShowExistsHeadRadioShowsid(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RadioShowApi#radioShowExistsHeadRadioShowsid");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |

### Return type

[**InlineResponse2003**](InlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioShowFind"></a>
# **radioShowFind**
> List&lt;RadioShow&gt; radioShowFind(filter)

Find all instances of the model matched by filter from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.RadioShowApi;

RadioShowApi apiInstance = new RadioShowApi();
String filter = "filter_example"; // String | Filter defining fields, where, include, order, offset, and limit
try {
    List<RadioShow> result = apiInstance.radioShowFind(filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RadioShowApi#radioShowFind");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **String**| Filter defining fields, where, include, order, offset, and limit | [optional]

### Return type

[**List&lt;RadioShow&gt;**](RadioShow.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioShowFindById"></a>
# **radioShowFindById**
> RadioShow radioShowFindById(id, filter)

Find a model instance by {{id}} from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.RadioShowApi;

RadioShowApi apiInstance = new RadioShowApi();
String id = "id_example"; // String | Model id
String filter = "filter_example"; // String | Filter defining fields and include
try {
    RadioShow result = apiInstance.radioShowFindById(id, filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RadioShowApi#radioShowFindById");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |
 **filter** | **String**| Filter defining fields and include | [optional]

### Return type

[**RadioShow**](RadioShow.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioShowFindOne"></a>
# **radioShowFindOne**
> RadioShow radioShowFindOne(filter)

Find first instance of the model matched by filter from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.RadioShowApi;

RadioShowApi apiInstance = new RadioShowApi();
String filter = "filter_example"; // String | Filter defining fields, where, include, order, offset, and limit
try {
    RadioShow result = apiInstance.radioShowFindOne(filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RadioShowApi#radioShowFindOne");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **String**| Filter defining fields, where, include, order, offset, and limit | [optional]

### Return type

[**RadioShow**](RadioShow.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioShowPrototypeCountRadioPresenters"></a>
# **radioShowPrototypeCountRadioPresenters**
> InlineResponse2001 radioShowPrototypeCountRadioPresenters(id, where)

Counts radioPresenters of RadioShow.

### Example
```java
// Import classes:
//import com.harpoon.api.RadioShowApi;

RadioShowApi apiInstance = new RadioShowApi();
String id = "id_example"; // String | RadioShow id
String where = "where_example"; // String | Criteria to match model instances
try {
    InlineResponse2001 result = apiInstance.radioShowPrototypeCountRadioPresenters(id, where);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RadioShowApi#radioShowPrototypeCountRadioPresenters");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| RadioShow id |
 **where** | **String**| Criteria to match model instances | [optional]

### Return type

[**InlineResponse2001**](InlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioShowPrototypeCountRadioShowTimes"></a>
# **radioShowPrototypeCountRadioShowTimes**
> InlineResponse2001 radioShowPrototypeCountRadioShowTimes(id, where)

Counts radioShowTimes of RadioShow.

### Example
```java
// Import classes:
//import com.harpoon.api.RadioShowApi;

RadioShowApi apiInstance = new RadioShowApi();
String id = "id_example"; // String | RadioShow id
String where = "where_example"; // String | Criteria to match model instances
try {
    InlineResponse2001 result = apiInstance.radioShowPrototypeCountRadioShowTimes(id, where);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RadioShowApi#radioShowPrototypeCountRadioShowTimes");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| RadioShow id |
 **where** | **String**| Criteria to match model instances | [optional]

### Return type

[**InlineResponse2001**](InlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioShowPrototypeCreateRadioPresenters"></a>
# **radioShowPrototypeCreateRadioPresenters**
> RadioPresenter radioShowPrototypeCreateRadioPresenters(id, data)

Creates a new instance in radioPresenters of this model.

### Example
```java
// Import classes:
//import com.harpoon.api.RadioShowApi;

RadioShowApi apiInstance = new RadioShowApi();
String id = "id_example"; // String | RadioShow id
RadioPresenter data = new RadioPresenter(); // RadioPresenter | 
try {
    RadioPresenter result = apiInstance.radioShowPrototypeCreateRadioPresenters(id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RadioShowApi#radioShowPrototypeCreateRadioPresenters");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| RadioShow id |
 **data** | [**RadioPresenter**](RadioPresenter.md)|  | [optional]

### Return type

[**RadioPresenter**](RadioPresenter.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioShowPrototypeCreateRadioShowTimes"></a>
# **radioShowPrototypeCreateRadioShowTimes**
> RadioShowTime radioShowPrototypeCreateRadioShowTimes(id, data)

Creates a new instance in radioShowTimes of this model.

### Example
```java
// Import classes:
//import com.harpoon.api.RadioShowApi;

RadioShowApi apiInstance = new RadioShowApi();
String id = "id_example"; // String | RadioShow id
RadioShowTime data = new RadioShowTime(); // RadioShowTime | 
try {
    RadioShowTime result = apiInstance.radioShowPrototypeCreateRadioShowTimes(id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RadioShowApi#radioShowPrototypeCreateRadioShowTimes");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| RadioShow id |
 **data** | [**RadioShowTime**](RadioShowTime.md)|  | [optional]

### Return type

[**RadioShowTime**](RadioShowTime.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioShowPrototypeDeleteRadioPresenters"></a>
# **radioShowPrototypeDeleteRadioPresenters**
> radioShowPrototypeDeleteRadioPresenters(id)

Deletes all radioPresenters of this model.

### Example
```java
// Import classes:
//import com.harpoon.api.RadioShowApi;

RadioShowApi apiInstance = new RadioShowApi();
String id = "id_example"; // String | RadioShow id
try {
    apiInstance.radioShowPrototypeDeleteRadioPresenters(id);
} catch (ApiException e) {
    System.err.println("Exception when calling RadioShowApi#radioShowPrototypeDeleteRadioPresenters");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| RadioShow id |

### Return type

null (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioShowPrototypeDeleteRadioShowTimes"></a>
# **radioShowPrototypeDeleteRadioShowTimes**
> radioShowPrototypeDeleteRadioShowTimes(id)

Deletes all radioShowTimes of this model.

### Example
```java
// Import classes:
//import com.harpoon.api.RadioShowApi;

RadioShowApi apiInstance = new RadioShowApi();
String id = "id_example"; // String | RadioShow id
try {
    apiInstance.radioShowPrototypeDeleteRadioShowTimes(id);
} catch (ApiException e) {
    System.err.println("Exception when calling RadioShowApi#radioShowPrototypeDeleteRadioShowTimes");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| RadioShow id |

### Return type

null (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioShowPrototypeDestroyByIdRadioPresenters"></a>
# **radioShowPrototypeDestroyByIdRadioPresenters**
> radioShowPrototypeDestroyByIdRadioPresenters(fk, id)

Delete a related item by id for radioPresenters.

### Example
```java
// Import classes:
//import com.harpoon.api.RadioShowApi;

RadioShowApi apiInstance = new RadioShowApi();
String fk = "fk_example"; // String | Foreign key for radioPresenters
String id = "id_example"; // String | RadioShow id
try {
    apiInstance.radioShowPrototypeDestroyByIdRadioPresenters(fk, id);
} catch (ApiException e) {
    System.err.println("Exception when calling RadioShowApi#radioShowPrototypeDestroyByIdRadioPresenters");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for radioPresenters |
 **id** | **String**| RadioShow id |

### Return type

null (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioShowPrototypeDestroyByIdRadioShowTimes"></a>
# **radioShowPrototypeDestroyByIdRadioShowTimes**
> radioShowPrototypeDestroyByIdRadioShowTimes(fk, id)

Delete a related item by id for radioShowTimes.

### Example
```java
// Import classes:
//import com.harpoon.api.RadioShowApi;

RadioShowApi apiInstance = new RadioShowApi();
String fk = "fk_example"; // String | Foreign key for radioShowTimes
String id = "id_example"; // String | RadioShow id
try {
    apiInstance.radioShowPrototypeDestroyByIdRadioShowTimes(fk, id);
} catch (ApiException e) {
    System.err.println("Exception when calling RadioShowApi#radioShowPrototypeDestroyByIdRadioShowTimes");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for radioShowTimes |
 **id** | **String**| RadioShow id |

### Return type

null (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioShowPrototypeExistsRadioPresenters"></a>
# **radioShowPrototypeExistsRadioPresenters**
> Boolean radioShowPrototypeExistsRadioPresenters(fk, id)

Check the existence of radioPresenters relation to an item by id.

### Example
```java
// Import classes:
//import com.harpoon.api.RadioShowApi;

RadioShowApi apiInstance = new RadioShowApi();
String fk = "fk_example"; // String | Foreign key for radioPresenters
String id = "id_example"; // String | RadioShow id
try {
    Boolean result = apiInstance.radioShowPrototypeExistsRadioPresenters(fk, id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RadioShowApi#radioShowPrototypeExistsRadioPresenters");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for radioPresenters |
 **id** | **String**| RadioShow id |

### Return type

**Boolean**

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioShowPrototypeFindByIdRadioPresenters"></a>
# **radioShowPrototypeFindByIdRadioPresenters**
> RadioPresenter radioShowPrototypeFindByIdRadioPresenters(fk, id)

Find a related item by id for radioPresenters.

### Example
```java
// Import classes:
//import com.harpoon.api.RadioShowApi;

RadioShowApi apiInstance = new RadioShowApi();
String fk = "fk_example"; // String | Foreign key for radioPresenters
String id = "id_example"; // String | RadioShow id
try {
    RadioPresenter result = apiInstance.radioShowPrototypeFindByIdRadioPresenters(fk, id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RadioShowApi#radioShowPrototypeFindByIdRadioPresenters");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for radioPresenters |
 **id** | **String**| RadioShow id |

### Return type

[**RadioPresenter**](RadioPresenter.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioShowPrototypeFindByIdRadioShowTimes"></a>
# **radioShowPrototypeFindByIdRadioShowTimes**
> RadioShowTime radioShowPrototypeFindByIdRadioShowTimes(fk, id)

Find a related item by id for radioShowTimes.

### Example
```java
// Import classes:
//import com.harpoon.api.RadioShowApi;

RadioShowApi apiInstance = new RadioShowApi();
String fk = "fk_example"; // String | Foreign key for radioShowTimes
String id = "id_example"; // String | RadioShow id
try {
    RadioShowTime result = apiInstance.radioShowPrototypeFindByIdRadioShowTimes(fk, id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RadioShowApi#radioShowPrototypeFindByIdRadioShowTimes");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for radioShowTimes |
 **id** | **String**| RadioShow id |

### Return type

[**RadioShowTime**](RadioShowTime.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioShowPrototypeGetPlaylistItem"></a>
# **radioShowPrototypeGetPlaylistItem**
> PlaylistItem radioShowPrototypeGetPlaylistItem(id, refresh)

Fetches belongsTo relation playlistItem.

### Example
```java
// Import classes:
//import com.harpoon.api.RadioShowApi;

RadioShowApi apiInstance = new RadioShowApi();
String id = "id_example"; // String | RadioShow id
Boolean refresh = true; // Boolean | 
try {
    PlaylistItem result = apiInstance.radioShowPrototypeGetPlaylistItem(id, refresh);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RadioShowApi#radioShowPrototypeGetPlaylistItem");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| RadioShow id |
 **refresh** | **Boolean**|  | [optional]

### Return type

[**PlaylistItem**](PlaylistItem.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioShowPrototypeGetRadioPresenters"></a>
# **radioShowPrototypeGetRadioPresenters**
> List&lt;RadioPresenter&gt; radioShowPrototypeGetRadioPresenters(id, filter)

Queries radioPresenters of RadioShow.

### Example
```java
// Import classes:
//import com.harpoon.api.RadioShowApi;

RadioShowApi apiInstance = new RadioShowApi();
String id = "id_example"; // String | RadioShow id
String filter = "filter_example"; // String | 
try {
    List<RadioPresenter> result = apiInstance.radioShowPrototypeGetRadioPresenters(id, filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RadioShowApi#radioShowPrototypeGetRadioPresenters");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| RadioShow id |
 **filter** | **String**|  | [optional]

### Return type

[**List&lt;RadioPresenter&gt;**](RadioPresenter.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioShowPrototypeGetRadioShowTimes"></a>
# **radioShowPrototypeGetRadioShowTimes**
> List&lt;RadioShowTime&gt; radioShowPrototypeGetRadioShowTimes(id, filter)

Queries radioShowTimes of RadioShow.

### Example
```java
// Import classes:
//import com.harpoon.api.RadioShowApi;

RadioShowApi apiInstance = new RadioShowApi();
String id = "id_example"; // String | RadioShow id
String filter = "filter_example"; // String | 
try {
    List<RadioShowTime> result = apiInstance.radioShowPrototypeGetRadioShowTimes(id, filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RadioShowApi#radioShowPrototypeGetRadioShowTimes");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| RadioShow id |
 **filter** | **String**|  | [optional]

### Return type

[**List&lt;RadioShowTime&gt;**](RadioShowTime.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioShowPrototypeGetRadioStream"></a>
# **radioShowPrototypeGetRadioStream**
> RadioStream radioShowPrototypeGetRadioStream(id, refresh)

Fetches belongsTo relation radioStream.

### Example
```java
// Import classes:
//import com.harpoon.api.RadioShowApi;

RadioShowApi apiInstance = new RadioShowApi();
String id = "id_example"; // String | RadioShow id
Boolean refresh = true; // Boolean | 
try {
    RadioStream result = apiInstance.radioShowPrototypeGetRadioStream(id, refresh);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RadioShowApi#radioShowPrototypeGetRadioStream");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| RadioShow id |
 **refresh** | **Boolean**|  | [optional]

### Return type

[**RadioStream**](RadioStream.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioShowPrototypeLinkRadioPresenters"></a>
# **radioShowPrototypeLinkRadioPresenters**
> RadioPresenterRadioShow radioShowPrototypeLinkRadioPresenters(fk, id, data)

Add a related item by id for radioPresenters.

### Example
```java
// Import classes:
//import com.harpoon.api.RadioShowApi;

RadioShowApi apiInstance = new RadioShowApi();
String fk = "fk_example"; // String | Foreign key for radioPresenters
String id = "id_example"; // String | RadioShow id
RadioPresenterRadioShow data = new RadioPresenterRadioShow(); // RadioPresenterRadioShow | 
try {
    RadioPresenterRadioShow result = apiInstance.radioShowPrototypeLinkRadioPresenters(fk, id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RadioShowApi#radioShowPrototypeLinkRadioPresenters");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for radioPresenters |
 **id** | **String**| RadioShow id |
 **data** | [**RadioPresenterRadioShow**](RadioPresenterRadioShow.md)|  | [optional]

### Return type

[**RadioPresenterRadioShow**](RadioPresenterRadioShow.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioShowPrototypeUnlinkRadioPresenters"></a>
# **radioShowPrototypeUnlinkRadioPresenters**
> radioShowPrototypeUnlinkRadioPresenters(fk, id)

Remove the radioPresenters relation to an item by id.

### Example
```java
// Import classes:
//import com.harpoon.api.RadioShowApi;

RadioShowApi apiInstance = new RadioShowApi();
String fk = "fk_example"; // String | Foreign key for radioPresenters
String id = "id_example"; // String | RadioShow id
try {
    apiInstance.radioShowPrototypeUnlinkRadioPresenters(fk, id);
} catch (ApiException e) {
    System.err.println("Exception when calling RadioShowApi#radioShowPrototypeUnlinkRadioPresenters");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for radioPresenters |
 **id** | **String**| RadioShow id |

### Return type

null (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioShowPrototypeUpdateAttributesPatchRadioShowsid"></a>
# **radioShowPrototypeUpdateAttributesPatchRadioShowsid**
> RadioShow radioShowPrototypeUpdateAttributesPatchRadioShowsid(id, data)

Patch attributes for a model instance and persist it into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.RadioShowApi;

RadioShowApi apiInstance = new RadioShowApi();
String id = "id_example"; // String | RadioShow id
RadioShow data = new RadioShow(); // RadioShow | An object of model property name/value pairs
try {
    RadioShow result = apiInstance.radioShowPrototypeUpdateAttributesPatchRadioShowsid(id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RadioShowApi#radioShowPrototypeUpdateAttributesPatchRadioShowsid");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| RadioShow id |
 **data** | [**RadioShow**](RadioShow.md)| An object of model property name/value pairs | [optional]

### Return type

[**RadioShow**](RadioShow.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioShowPrototypeUpdateAttributesPutRadioShowsid"></a>
# **radioShowPrototypeUpdateAttributesPutRadioShowsid**
> RadioShow radioShowPrototypeUpdateAttributesPutRadioShowsid(id, data)

Patch attributes for a model instance and persist it into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.RadioShowApi;

RadioShowApi apiInstance = new RadioShowApi();
String id = "id_example"; // String | RadioShow id
RadioShow data = new RadioShow(); // RadioShow | An object of model property name/value pairs
try {
    RadioShow result = apiInstance.radioShowPrototypeUpdateAttributesPutRadioShowsid(id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RadioShowApi#radioShowPrototypeUpdateAttributesPutRadioShowsid");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| RadioShow id |
 **data** | [**RadioShow**](RadioShow.md)| An object of model property name/value pairs | [optional]

### Return type

[**RadioShow**](RadioShow.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioShowPrototypeUpdateByIdRadioPresenters"></a>
# **radioShowPrototypeUpdateByIdRadioPresenters**
> RadioPresenter radioShowPrototypeUpdateByIdRadioPresenters(fk, id, data)

Update a related item by id for radioPresenters.

### Example
```java
// Import classes:
//import com.harpoon.api.RadioShowApi;

RadioShowApi apiInstance = new RadioShowApi();
String fk = "fk_example"; // String | Foreign key for radioPresenters
String id = "id_example"; // String | RadioShow id
RadioPresenter data = new RadioPresenter(); // RadioPresenter | 
try {
    RadioPresenter result = apiInstance.radioShowPrototypeUpdateByIdRadioPresenters(fk, id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RadioShowApi#radioShowPrototypeUpdateByIdRadioPresenters");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for radioPresenters |
 **id** | **String**| RadioShow id |
 **data** | [**RadioPresenter**](RadioPresenter.md)|  | [optional]

### Return type

[**RadioPresenter**](RadioPresenter.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioShowPrototypeUpdateByIdRadioShowTimes"></a>
# **radioShowPrototypeUpdateByIdRadioShowTimes**
> RadioShowTime radioShowPrototypeUpdateByIdRadioShowTimes(fk, id, data)

Update a related item by id for radioShowTimes.

### Example
```java
// Import classes:
//import com.harpoon.api.RadioShowApi;

RadioShowApi apiInstance = new RadioShowApi();
String fk = "fk_example"; // String | Foreign key for radioShowTimes
String id = "id_example"; // String | RadioShow id
RadioShowTime data = new RadioShowTime(); // RadioShowTime | 
try {
    RadioShowTime result = apiInstance.radioShowPrototypeUpdateByIdRadioShowTimes(fk, id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RadioShowApi#radioShowPrototypeUpdateByIdRadioShowTimes");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fk** | **String**| Foreign key for radioShowTimes |
 **id** | **String**| RadioShow id |
 **data** | [**RadioShowTime**](RadioShowTime.md)|  | [optional]

### Return type

[**RadioShowTime**](RadioShowTime.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioShowReplaceById"></a>
# **radioShowReplaceById**
> RadioShow radioShowReplaceById(id, data)

Replace attributes for a model instance and persist it into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.RadioShowApi;

RadioShowApi apiInstance = new RadioShowApi();
String id = "id_example"; // String | Model id
RadioShow data = new RadioShow(); // RadioShow | Model instance data
try {
    RadioShow result = apiInstance.radioShowReplaceById(id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RadioShowApi#radioShowReplaceById");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |
 **data** | [**RadioShow**](RadioShow.md)| Model instance data | [optional]

### Return type

[**RadioShow**](RadioShow.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioShowReplaceOrCreate"></a>
# **radioShowReplaceOrCreate**
> RadioShow radioShowReplaceOrCreate(data)

Replace an existing model instance or insert a new one into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.RadioShowApi;

RadioShowApi apiInstance = new RadioShowApi();
RadioShow data = new RadioShow(); // RadioShow | Model instance data
try {
    RadioShow result = apiInstance.radioShowReplaceOrCreate(data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RadioShowApi#radioShowReplaceOrCreate");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**RadioShow**](RadioShow.md)| Model instance data | [optional]

### Return type

[**RadioShow**](RadioShow.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioShowUpdateAll"></a>
# **radioShowUpdateAll**
> InlineResponse2002 radioShowUpdateAll(where, data)

Update instances of the model matched by {{where}} from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.RadioShowApi;

RadioShowApi apiInstance = new RadioShowApi();
String where = "where_example"; // String | Criteria to match model instances
RadioShow data = new RadioShow(); // RadioShow | An object of model property name/value pairs
try {
    InlineResponse2002 result = apiInstance.radioShowUpdateAll(where, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RadioShowApi#radioShowUpdateAll");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional]
 **data** | [**RadioShow**](RadioShow.md)| An object of model property name/value pairs | [optional]

### Return type

[**InlineResponse2002**](InlineResponse2002.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioShowUploadFile"></a>
# **radioShowUploadFile**
> MagentoFileUpload radioShowUploadFile(id, data)

Upload File to Radio Show

### Example
```java
// Import classes:
//import com.harpoon.api.RadioShowApi;

RadioShowApi apiInstance = new RadioShowApi();
String id = "id_example"; // String | Radio Show Id
MagentoFileUpload data = new MagentoFileUpload(); // MagentoFileUpload | Corresponds to the file you're uploading formatted as an object
try {
    MagentoFileUpload result = apiInstance.radioShowUploadFile(id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RadioShowApi#radioShowUploadFile");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Radio Show Id |
 **data** | [**MagentoFileUpload**](MagentoFileUpload.md)| Corresponds to the file you&#39;re uploading formatted as an object | [optional]

### Return type

[**MagentoFileUpload**](MagentoFileUpload.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioShowUpsertPatchRadioShows"></a>
# **radioShowUpsertPatchRadioShows**
> RadioShow radioShowUpsertPatchRadioShows(data)

Patch an existing model instance or insert a new one into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.RadioShowApi;

RadioShowApi apiInstance = new RadioShowApi();
RadioShow data = new RadioShow(); // RadioShow | Model instance data
try {
    RadioShow result = apiInstance.radioShowUpsertPatchRadioShows(data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RadioShowApi#radioShowUpsertPatchRadioShows");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**RadioShow**](RadioShow.md)| Model instance data | [optional]

### Return type

[**RadioShow**](RadioShow.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioShowUpsertPutRadioShows"></a>
# **radioShowUpsertPutRadioShows**
> RadioShow radioShowUpsertPutRadioShows(data)

Patch an existing model instance or insert a new one into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.RadioShowApi;

RadioShowApi apiInstance = new RadioShowApi();
RadioShow data = new RadioShow(); // RadioShow | Model instance data
try {
    RadioShow result = apiInstance.radioShowUpsertPutRadioShows(data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RadioShowApi#radioShowUpsertPutRadioShows");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**RadioShow**](RadioShow.md)| Model instance data | [optional]

### Return type

[**RadioShow**](RadioShow.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioShowUpsertWithWhere"></a>
# **radioShowUpsertWithWhere**
> RadioShow radioShowUpsertWithWhere(where, data)

Update an existing model instance or insert a new one into the data source based on the where criteria.

### Example
```java
// Import classes:
//import com.harpoon.api.RadioShowApi;

RadioShowApi apiInstance = new RadioShowApi();
String where = "where_example"; // String | Criteria to match model instances
RadioShow data = new RadioShow(); // RadioShow | An object of model property name/value pairs
try {
    RadioShow result = apiInstance.radioShowUpsertWithWhere(where, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RadioShowApi#radioShowUpsertWithWhere");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional]
 **data** | [**RadioShow**](RadioShow.md)| An object of model property name/value pairs | [optional]

### Return type

[**RadioShow**](RadioShow.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

