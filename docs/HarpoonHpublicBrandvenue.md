
# HarpoonHpublicBrandvenue

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Double** |  | 
**brandId** | **Double** |  | 
**name** | **String** |  | 
**address** | **String** |  |  [optional]
**latitude** | **Double** |  | 
**longitude** | **Double** |  | 
**createdAt** | [**Date**](Date.md) |  | 
**updatedAt** | [**Date**](Date.md) |  | 
**email** | **String** |  |  [optional]
**phone** | **String** |  |  [optional]
**country** | **String** |  | 



