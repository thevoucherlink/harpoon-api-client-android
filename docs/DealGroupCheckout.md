
# DealGroupCheckout

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cartId** | **Double** |  |  [optional]
**url** | **String** |  |  [optional]
**status** | **String** |  | 
**id** | **Double** |  |  [optional]



