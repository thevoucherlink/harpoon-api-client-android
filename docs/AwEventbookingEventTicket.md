
# AwEventbookingEventTicket

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Double** |  | 
**eventId** | **Double** |  | 
**price** | **String** |  |  [optional]
**priceType** | **String** |  |  [optional]
**sku** | **String** |  |  [optional]
**qty** | **Double** |  |  [optional]
**codeprefix** | **String** |  |  [optional]
**fee** | **String** |  | 
**isPassOnFee** | **Double** |  | 
**chance** | **Double** |  | 
**attributes** | **List&lt;Object&gt;** |  |  [optional]
**tickets** | **List&lt;Object&gt;** |  |  [optional]
**awEventbookingTicket** | **List&lt;Object&gt;** |  |  [optional]



