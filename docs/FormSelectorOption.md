
# FormSelectorOption

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**title** | **String** |  | 
**value** | **String** |  | 
**format** | [**FormatEnum**](#FormatEnum) |  |  [optional]
**id** | **Double** |  |  [optional]


<a name="FormatEnum"></a>
## Enum: FormatEnum
Name | Value
---- | -----



