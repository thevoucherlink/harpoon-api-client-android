
# AppColor

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**appPrimary** | **String** |  | 
**appSecondary** | **String** |  |  [optional]
**textPrimary** | **String** |  |  [optional]
**textSecondary** | **String** |  |  [optional]
**radioNavBar** | **String** |  |  [optional]
**radioPlayer** | **String** |  |  [optional]
**radioProgressBar** | **String** |  |  [optional]
**menuBackground** | **String** |  |  [optional]
**radioTextSecondary** | **String** |  |  [optional]
**radioTextPrimary** | **String** |  |  [optional]
**menuItem** | **String** |  |  [optional]
**appGradientPrimary** | **String** |  |  [optional]
**feedSegmentIndicator** | **String** |  |  [optional]
**radioPlayButton** | **String** |  |  [optional]
**radioPauseButton** | **String** |  |  [optional]
**radioPlayButtonBackground** | **String** |  |  [optional]
**radioPauseButtonBackground** | **String** |  |  [optional]
**radioPlayerItem** | **String** |  |  [optional]



