
# CustomerCardCreateData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** |  | 
**number** | **String** |  | 
**cvc** | **String** |  | 
**exMonth** | **String** |  | 
**exYear** | **String** |  | 
**cardholderName** | **String** |  | 



