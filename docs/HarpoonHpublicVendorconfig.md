
# HarpoonHpublicVendorconfig

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Double** |  | 
**type** | **Double** |  | 
**category** | **String** |  | 
**logo** | **String** |  | 
**cover** | **String** |  | 
**facebookPageId** | **String** |  | 
**facebookPageToken** | **String** |  | 
**facebookEventEnabled** | **Double** |  | 
**facebookEventUpdatedAt** | [**Date**](Date.md) |  | 
**facebookEventCount** | **Double** |  | 
**facebookFeedEnabled** | **Double** |  | 
**facebookFeedUpdatedAt** | [**Date**](Date.md) |  | 
**facebookFeedCount** | **Double** |  | 
**stripePublishableKey** | **String** |  | 
**stripeRefreshToken** | **String** |  | 
**stripeAccessToken** | **String** |  | 
**feesEventTicket** | **String** |  | 
**stripeUserId** | **String** |  | 
**vendorconfigId** | **Double** |  | 
**createdAt** | [**Date**](Date.md) |  | 
**updatedAt** | [**Date**](Date.md) |  | 



