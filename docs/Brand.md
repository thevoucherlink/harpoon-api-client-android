
# Brand

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** |  |  [optional]
**logo** | **String** |  |  [optional]
**cover** | **String** |  |  [optional]
**description** | **String** |  |  [optional]
**email** | **String** |  |  [optional]
**phone** | **String** |  |  [optional]
**address** | **String** |  |  [optional]
**managerName** | **String** |  |  [optional]
**followerCount** | **Double** |  |  [optional]
**isFollowed** | **Boolean** |  |  [optional]
**categories** | [**List&lt;Category&gt;**](Category.md) |  |  [optional]
**website** | **String** |  |  [optional]
**id** | **Double** |  |  [optional]



