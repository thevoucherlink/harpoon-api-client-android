
# PlayerTrack

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**file** | **String** | Source file |  [optional]
**type** | **String** | Source type (used only in JS SDK) |  [optional]
**label** | **String** | Source label |  [optional]
**_default** | **Boolean** | If Source is the default Source |  [optional]
**order** | **Double** | Sort order index |  [optional]
**id** | **Double** |  |  [optional]
**playlistItemId** | **Double** |  |  [optional]
**playlistItem** | **Object** |  |  [optional]



