
# CheckoutAgreement

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** |  |  [optional]
**contentHeight** | **String** |  |  [optional]
**checkboxText** | **String** |  |  [optional]
**isActive** | **Double** |  | 
**isHtml** | **Double** |  | 
**id** | **Double** |  | 
**content** | **String** |  |  [optional]



