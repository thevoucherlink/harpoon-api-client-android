# CompetitionApi

All URIs are relative to *https://apicdn.harpoonconnect.com/v1.1.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**competitionAttendees**](CompetitionApi.md#competitionAttendees) | **GET** /Competitions/{id}/attendees | 
[**competitionCheckout**](CompetitionApi.md#competitionCheckout) | **POST** /Competitions/{id}/checkout | 
[**competitionFind**](CompetitionApi.md#competitionFind) | **GET** /Competitions | Find all instances of the model matched by filter from the data source.
[**competitionFindById**](CompetitionApi.md#competitionFindById) | **GET** /Competitions/{id} | Find a model instance by {{id}} from the data source.
[**competitionFindOne**](CompetitionApi.md#competitionFindOne) | **GET** /Competitions/findOne | Find first instance of the model matched by filter from the data source.
[**competitionRedeem**](CompetitionApi.md#competitionRedeem) | **POST** /Competitions/{id}/redeem | 
[**competitionReplaceById**](CompetitionApi.md#competitionReplaceById) | **POST** /Competitions/{id}/replace | Replace attributes for a model instance and persist it into the data source.
[**competitionReplaceOrCreate**](CompetitionApi.md#competitionReplaceOrCreate) | **POST** /Competitions/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
[**competitionUpsertWithWhere**](CompetitionApi.md#competitionUpsertWithWhere) | **POST** /Competitions/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.
[**competitionValidateAnswer**](CompetitionApi.md#competitionValidateAnswer) | **POST** /Competitions/{id}/validateAnswer | 
[**competitionVenues**](CompetitionApi.md#competitionVenues) | **GET** /Competitions/{id}/venues | 


<a name="competitionAttendees"></a>
# **competitionAttendees**
> List&lt;Customer&gt; competitionAttendees(id, filter)



### Example
```java
// Import classes:
//import com.harpoon.api.CompetitionApi;

CompetitionApi apiInstance = new CompetitionApi();
String id = "id_example"; // String | Model id
String filter = "filter_example"; // String | Filter defining fields and include
try {
    List<Customer> result = apiInstance.competitionAttendees(id, filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CompetitionApi#competitionAttendees");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |
 **filter** | **String**| Filter defining fields and include | [optional]

### Return type

[**List&lt;Customer&gt;**](Customer.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="competitionCheckout"></a>
# **competitionCheckout**
> CompetitionCheckout competitionCheckout(id, data)



### Example
```java
// Import classes:
//import com.harpoon.api.CompetitionApi;

CompetitionApi apiInstance = new CompetitionApi();
String id = "id_example"; // String | Model id
CompetitionCheckoutData data = new CompetitionCheckoutData(); // CompetitionCheckoutData | 
try {
    CompetitionCheckout result = apiInstance.competitionCheckout(id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CompetitionApi#competitionCheckout");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |
 **data** | [**CompetitionCheckoutData**](CompetitionCheckoutData.md)|  | [optional]

### Return type

[**CompetitionCheckout**](CompetitionCheckout.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="competitionFind"></a>
# **competitionFind**
> List&lt;Competition&gt; competitionFind(filter)

Find all instances of the model matched by filter from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.CompetitionApi;

CompetitionApi apiInstance = new CompetitionApi();
String filter = "filter_example"; // String | Filter defining fields, where, include, order, offset, and limit
try {
    List<Competition> result = apiInstance.competitionFind(filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CompetitionApi#competitionFind");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **String**| Filter defining fields, where, include, order, offset, and limit | [optional]

### Return type

[**List&lt;Competition&gt;**](Competition.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="competitionFindById"></a>
# **competitionFindById**
> Competition competitionFindById(id, filter)

Find a model instance by {{id}} from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.CompetitionApi;

CompetitionApi apiInstance = new CompetitionApi();
String id = "id_example"; // String | Model id
String filter = "filter_example"; // String | Filter defining fields and include
try {
    Competition result = apiInstance.competitionFindById(id, filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CompetitionApi#competitionFindById");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |
 **filter** | **String**| Filter defining fields and include | [optional]

### Return type

[**Competition**](Competition.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="competitionFindOne"></a>
# **competitionFindOne**
> Competition competitionFindOne(filter)

Find first instance of the model matched by filter from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.CompetitionApi;

CompetitionApi apiInstance = new CompetitionApi();
String filter = "filter_example"; // String | Filter defining fields, where, include, order, offset, and limit
try {
    Competition result = apiInstance.competitionFindOne(filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CompetitionApi#competitionFindOne");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **String**| Filter defining fields, where, include, order, offset, and limit | [optional]

### Return type

[**Competition**](Competition.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="competitionRedeem"></a>
# **competitionRedeem**
> CompetitionPurchase competitionRedeem(id, data)



### Example
```java
// Import classes:
//import com.harpoon.api.CompetitionApi;

CompetitionApi apiInstance = new CompetitionApi();
String id = "id_example"; // String | Model id
CompetitionRedeemData data = new CompetitionRedeemData(); // CompetitionRedeemData | 
try {
    CompetitionPurchase result = apiInstance.competitionRedeem(id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CompetitionApi#competitionRedeem");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |
 **data** | [**CompetitionRedeemData**](CompetitionRedeemData.md)|  | [optional]

### Return type

[**CompetitionPurchase**](CompetitionPurchase.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="competitionReplaceById"></a>
# **competitionReplaceById**
> Competition competitionReplaceById(id, data)

Replace attributes for a model instance and persist it into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.CompetitionApi;

CompetitionApi apiInstance = new CompetitionApi();
String id = "id_example"; // String | Model id
Competition data = new Competition(); // Competition | Model instance data
try {
    Competition result = apiInstance.competitionReplaceById(id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CompetitionApi#competitionReplaceById");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |
 **data** | [**Competition**](Competition.md)| Model instance data | [optional]

### Return type

[**Competition**](Competition.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="competitionReplaceOrCreate"></a>
# **competitionReplaceOrCreate**
> Competition competitionReplaceOrCreate(data)

Replace an existing model instance or insert a new one into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.CompetitionApi;

CompetitionApi apiInstance = new CompetitionApi();
Competition data = new Competition(); // Competition | Model instance data
try {
    Competition result = apiInstance.competitionReplaceOrCreate(data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CompetitionApi#competitionReplaceOrCreate");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**Competition**](Competition.md)| Model instance data | [optional]

### Return type

[**Competition**](Competition.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="competitionUpsertWithWhere"></a>
# **competitionUpsertWithWhere**
> Competition competitionUpsertWithWhere(where, data)

Update an existing model instance or insert a new one into the data source based on the where criteria.

### Example
```java
// Import classes:
//import com.harpoon.api.CompetitionApi;

CompetitionApi apiInstance = new CompetitionApi();
String where = "where_example"; // String | Criteria to match model instances
Competition data = new Competition(); // Competition | An object of model property name/value pairs
try {
    Competition result = apiInstance.competitionUpsertWithWhere(where, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CompetitionApi#competitionUpsertWithWhere");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional]
 **data** | [**Competition**](Competition.md)| An object of model property name/value pairs | [optional]

### Return type

[**Competition**](Competition.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="competitionValidateAnswer"></a>
# **competitionValidateAnswer**
> Object competitionValidateAnswer(id, answer)



### Example
```java
// Import classes:
//import com.harpoon.api.CompetitionApi;

CompetitionApi apiInstance = new CompetitionApi();
String id = "id_example"; // String | Model id
CompetitionAnswerData answer = new CompetitionAnswerData(); // CompetitionAnswerData | Competition Answer
try {
    Object result = apiInstance.competitionValidateAnswer(id, answer);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CompetitionApi#competitionValidateAnswer");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |
 **answer** | [**CompetitionAnswerData**](CompetitionAnswerData.md)| Competition Answer |

### Return type

**Object**

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="competitionVenues"></a>
# **competitionVenues**
> List&lt;Venue&gt; competitionVenues(id, filter)



### Example
```java
// Import classes:
//import com.harpoon.api.CompetitionApi;

CompetitionApi apiInstance = new CompetitionApi();
String id = "id_example"; // String | Model id
String filter = "filter_example"; // String | Filter defining fields and include
try {
    List<Venue> result = apiInstance.competitionVenues(id, filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CompetitionApi#competitionVenues");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |
 **filter** | **String**| Filter defining fields and include | [optional]

### Return type

[**List&lt;Venue&gt;**](Venue.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

