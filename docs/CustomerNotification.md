
# CustomerNotification

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**message** | **String** |  |  [optional]
**cover** | **String** |  |  [optional]
**coverUpload** | [**MagentoImageUpload**](MagentoImageUpload.md) |  |  [optional]
**from** | [**NotificationFrom**](NotificationFrom.md) |  |  [optional]
**related** | [**NotificationRelated**](NotificationRelated.md) |  |  [optional]
**link** | **String** |  |  [optional]
**actionCode** | **String** |  |  [optional]
**status** | **String** |  |  [optional]
**sentAt** | [**Date**](Date.md) |  |  [optional]
**id** | **Double** |  |  [optional]



