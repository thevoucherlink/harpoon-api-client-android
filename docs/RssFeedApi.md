# RssFeedApi

All URIs are relative to *https://apicdn.harpoonconnect.com/v1.1.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**rssFeedCount**](RssFeedApi.md#rssFeedCount) | **GET** /RssFeeds/count | Count instances of the model matched by where from the data source.
[**rssFeedCreate**](RssFeedApi.md#rssFeedCreate) | **POST** /RssFeeds | Create a new instance of the model and persist it into the data source.
[**rssFeedCreateChangeStreamGetRssFeedsChangeStream**](RssFeedApi.md#rssFeedCreateChangeStreamGetRssFeedsChangeStream) | **GET** /RssFeeds/change-stream | Create a change stream.
[**rssFeedCreateChangeStreamPostRssFeedsChangeStream**](RssFeedApi.md#rssFeedCreateChangeStreamPostRssFeedsChangeStream) | **POST** /RssFeeds/change-stream | Create a change stream.
[**rssFeedDeleteById**](RssFeedApi.md#rssFeedDeleteById) | **DELETE** /RssFeeds/{id} | Delete a model instance by {{id}} from the data source.
[**rssFeedExistsGetRssFeedsidExists**](RssFeedApi.md#rssFeedExistsGetRssFeedsidExists) | **GET** /RssFeeds/{id}/exists | Check whether a model instance exists in the data source.
[**rssFeedExistsHeadRssFeedsid**](RssFeedApi.md#rssFeedExistsHeadRssFeedsid) | **HEAD** /RssFeeds/{id} | Check whether a model instance exists in the data source.
[**rssFeedFind**](RssFeedApi.md#rssFeedFind) | **GET** /RssFeeds | Find all instances of the model matched by filter from the data source.
[**rssFeedFindById**](RssFeedApi.md#rssFeedFindById) | **GET** /RssFeeds/{id} | Find a model instance by {{id}} from the data source.
[**rssFeedFindOne**](RssFeedApi.md#rssFeedFindOne) | **GET** /RssFeeds/findOne | Find first instance of the model matched by filter from the data source.
[**rssFeedPrototypeGetRssFeedGroup**](RssFeedApi.md#rssFeedPrototypeGetRssFeedGroup) | **GET** /RssFeeds/{id}/rssFeedGroup | Fetches belongsTo relation rssFeedGroup.
[**rssFeedPrototypeUpdateAttributesPatchRssFeedsid**](RssFeedApi.md#rssFeedPrototypeUpdateAttributesPatchRssFeedsid) | **PATCH** /RssFeeds/{id} | Patch attributes for a model instance and persist it into the data source.
[**rssFeedPrototypeUpdateAttributesPutRssFeedsid**](RssFeedApi.md#rssFeedPrototypeUpdateAttributesPutRssFeedsid) | **PUT** /RssFeeds/{id} | Patch attributes for a model instance and persist it into the data source.
[**rssFeedReplaceById**](RssFeedApi.md#rssFeedReplaceById) | **POST** /RssFeeds/{id}/replace | Replace attributes for a model instance and persist it into the data source.
[**rssFeedReplaceOrCreate**](RssFeedApi.md#rssFeedReplaceOrCreate) | **POST** /RssFeeds/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
[**rssFeedUpdateAll**](RssFeedApi.md#rssFeedUpdateAll) | **POST** /RssFeeds/update | Update instances of the model matched by {{where}} from the data source.
[**rssFeedUpsertPatchRssFeeds**](RssFeedApi.md#rssFeedUpsertPatchRssFeeds) | **PATCH** /RssFeeds | Patch an existing model instance or insert a new one into the data source.
[**rssFeedUpsertPutRssFeeds**](RssFeedApi.md#rssFeedUpsertPutRssFeeds) | **PUT** /RssFeeds | Patch an existing model instance or insert a new one into the data source.
[**rssFeedUpsertWithWhere**](RssFeedApi.md#rssFeedUpsertWithWhere) | **POST** /RssFeeds/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.


<a name="rssFeedCount"></a>
# **rssFeedCount**
> InlineResponse2001 rssFeedCount(where)

Count instances of the model matched by where from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.RssFeedApi;

RssFeedApi apiInstance = new RssFeedApi();
String where = "where_example"; // String | Criteria to match model instances
try {
    InlineResponse2001 result = apiInstance.rssFeedCount(where);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RssFeedApi#rssFeedCount");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional]

### Return type

[**InlineResponse2001**](InlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="rssFeedCreate"></a>
# **rssFeedCreate**
> RssFeed rssFeedCreate(data)

Create a new instance of the model and persist it into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.RssFeedApi;

RssFeedApi apiInstance = new RssFeedApi();
RssFeed data = new RssFeed(); // RssFeed | Model instance data
try {
    RssFeed result = apiInstance.rssFeedCreate(data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RssFeedApi#rssFeedCreate");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**RssFeed**](RssFeed.md)| Model instance data | [optional]

### Return type

[**RssFeed**](RssFeed.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="rssFeedCreateChangeStreamGetRssFeedsChangeStream"></a>
# **rssFeedCreateChangeStreamGetRssFeedsChangeStream**
> File rssFeedCreateChangeStreamGetRssFeedsChangeStream(options)

Create a change stream.

### Example
```java
// Import classes:
//import com.harpoon.api.RssFeedApi;

RssFeedApi apiInstance = new RssFeedApi();
String options = "options_example"; // String | 
try {
    File result = apiInstance.rssFeedCreateChangeStreamGetRssFeedsChangeStream(options);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RssFeedApi#rssFeedCreateChangeStreamGetRssFeedsChangeStream");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **String**|  | [optional]

### Return type

[**File**](File.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="rssFeedCreateChangeStreamPostRssFeedsChangeStream"></a>
# **rssFeedCreateChangeStreamPostRssFeedsChangeStream**
> File rssFeedCreateChangeStreamPostRssFeedsChangeStream(options)

Create a change stream.

### Example
```java
// Import classes:
//import com.harpoon.api.RssFeedApi;

RssFeedApi apiInstance = new RssFeedApi();
String options = "options_example"; // String | 
try {
    File result = apiInstance.rssFeedCreateChangeStreamPostRssFeedsChangeStream(options);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RssFeedApi#rssFeedCreateChangeStreamPostRssFeedsChangeStream");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **String**|  | [optional]

### Return type

[**File**](File.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="rssFeedDeleteById"></a>
# **rssFeedDeleteById**
> Object rssFeedDeleteById(id)

Delete a model instance by {{id}} from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.RssFeedApi;

RssFeedApi apiInstance = new RssFeedApi();
String id = "id_example"; // String | Model id
try {
    Object result = apiInstance.rssFeedDeleteById(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RssFeedApi#rssFeedDeleteById");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |

### Return type

**Object**

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="rssFeedExistsGetRssFeedsidExists"></a>
# **rssFeedExistsGetRssFeedsidExists**
> InlineResponse2003 rssFeedExistsGetRssFeedsidExists(id)

Check whether a model instance exists in the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.RssFeedApi;

RssFeedApi apiInstance = new RssFeedApi();
String id = "id_example"; // String | Model id
try {
    InlineResponse2003 result = apiInstance.rssFeedExistsGetRssFeedsidExists(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RssFeedApi#rssFeedExistsGetRssFeedsidExists");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |

### Return type

[**InlineResponse2003**](InlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="rssFeedExistsHeadRssFeedsid"></a>
# **rssFeedExistsHeadRssFeedsid**
> InlineResponse2003 rssFeedExistsHeadRssFeedsid(id)

Check whether a model instance exists in the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.RssFeedApi;

RssFeedApi apiInstance = new RssFeedApi();
String id = "id_example"; // String | Model id
try {
    InlineResponse2003 result = apiInstance.rssFeedExistsHeadRssFeedsid(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RssFeedApi#rssFeedExistsHeadRssFeedsid");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |

### Return type

[**InlineResponse2003**](InlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="rssFeedFind"></a>
# **rssFeedFind**
> List&lt;RssFeed&gt; rssFeedFind(filter)

Find all instances of the model matched by filter from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.RssFeedApi;

RssFeedApi apiInstance = new RssFeedApi();
String filter = "filter_example"; // String | Filter defining fields, where, include, order, offset, and limit
try {
    List<RssFeed> result = apiInstance.rssFeedFind(filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RssFeedApi#rssFeedFind");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **String**| Filter defining fields, where, include, order, offset, and limit | [optional]

### Return type

[**List&lt;RssFeed&gt;**](RssFeed.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="rssFeedFindById"></a>
# **rssFeedFindById**
> RssFeed rssFeedFindById(id, filter)

Find a model instance by {{id}} from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.RssFeedApi;

RssFeedApi apiInstance = new RssFeedApi();
String id = "id_example"; // String | Model id
String filter = "filter_example"; // String | Filter defining fields and include
try {
    RssFeed result = apiInstance.rssFeedFindById(id, filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RssFeedApi#rssFeedFindById");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |
 **filter** | **String**| Filter defining fields and include | [optional]

### Return type

[**RssFeed**](RssFeed.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="rssFeedFindOne"></a>
# **rssFeedFindOne**
> RssFeed rssFeedFindOne(filter)

Find first instance of the model matched by filter from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.RssFeedApi;

RssFeedApi apiInstance = new RssFeedApi();
String filter = "filter_example"; // String | Filter defining fields, where, include, order, offset, and limit
try {
    RssFeed result = apiInstance.rssFeedFindOne(filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RssFeedApi#rssFeedFindOne");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **String**| Filter defining fields, where, include, order, offset, and limit | [optional]

### Return type

[**RssFeed**](RssFeed.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="rssFeedPrototypeGetRssFeedGroup"></a>
# **rssFeedPrototypeGetRssFeedGroup**
> RssFeedGroup rssFeedPrototypeGetRssFeedGroup(id, refresh)

Fetches belongsTo relation rssFeedGroup.

### Example
```java
// Import classes:
//import com.harpoon.api.RssFeedApi;

RssFeedApi apiInstance = new RssFeedApi();
String id = "id_example"; // String | RssFeed id
Boolean refresh = true; // Boolean | 
try {
    RssFeedGroup result = apiInstance.rssFeedPrototypeGetRssFeedGroup(id, refresh);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RssFeedApi#rssFeedPrototypeGetRssFeedGroup");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| RssFeed id |
 **refresh** | **Boolean**|  | [optional]

### Return type

[**RssFeedGroup**](RssFeedGroup.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="rssFeedPrototypeUpdateAttributesPatchRssFeedsid"></a>
# **rssFeedPrototypeUpdateAttributesPatchRssFeedsid**
> RssFeed rssFeedPrototypeUpdateAttributesPatchRssFeedsid(id, data)

Patch attributes for a model instance and persist it into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.RssFeedApi;

RssFeedApi apiInstance = new RssFeedApi();
String id = "id_example"; // String | RssFeed id
RssFeed data = new RssFeed(); // RssFeed | An object of model property name/value pairs
try {
    RssFeed result = apiInstance.rssFeedPrototypeUpdateAttributesPatchRssFeedsid(id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RssFeedApi#rssFeedPrototypeUpdateAttributesPatchRssFeedsid");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| RssFeed id |
 **data** | [**RssFeed**](RssFeed.md)| An object of model property name/value pairs | [optional]

### Return type

[**RssFeed**](RssFeed.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="rssFeedPrototypeUpdateAttributesPutRssFeedsid"></a>
# **rssFeedPrototypeUpdateAttributesPutRssFeedsid**
> RssFeed rssFeedPrototypeUpdateAttributesPutRssFeedsid(id, data)

Patch attributes for a model instance and persist it into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.RssFeedApi;

RssFeedApi apiInstance = new RssFeedApi();
String id = "id_example"; // String | RssFeed id
RssFeed data = new RssFeed(); // RssFeed | An object of model property name/value pairs
try {
    RssFeed result = apiInstance.rssFeedPrototypeUpdateAttributesPutRssFeedsid(id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RssFeedApi#rssFeedPrototypeUpdateAttributesPutRssFeedsid");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| RssFeed id |
 **data** | [**RssFeed**](RssFeed.md)| An object of model property name/value pairs | [optional]

### Return type

[**RssFeed**](RssFeed.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="rssFeedReplaceById"></a>
# **rssFeedReplaceById**
> RssFeed rssFeedReplaceById(id, data)

Replace attributes for a model instance and persist it into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.RssFeedApi;

RssFeedApi apiInstance = new RssFeedApi();
String id = "id_example"; // String | Model id
RssFeed data = new RssFeed(); // RssFeed | Model instance data
try {
    RssFeed result = apiInstance.rssFeedReplaceById(id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RssFeedApi#rssFeedReplaceById");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |
 **data** | [**RssFeed**](RssFeed.md)| Model instance data | [optional]

### Return type

[**RssFeed**](RssFeed.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="rssFeedReplaceOrCreate"></a>
# **rssFeedReplaceOrCreate**
> RssFeed rssFeedReplaceOrCreate(data)

Replace an existing model instance or insert a new one into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.RssFeedApi;

RssFeedApi apiInstance = new RssFeedApi();
RssFeed data = new RssFeed(); // RssFeed | Model instance data
try {
    RssFeed result = apiInstance.rssFeedReplaceOrCreate(data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RssFeedApi#rssFeedReplaceOrCreate");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**RssFeed**](RssFeed.md)| Model instance data | [optional]

### Return type

[**RssFeed**](RssFeed.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="rssFeedUpdateAll"></a>
# **rssFeedUpdateAll**
> InlineResponse2002 rssFeedUpdateAll(where, data)

Update instances of the model matched by {{where}} from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.RssFeedApi;

RssFeedApi apiInstance = new RssFeedApi();
String where = "where_example"; // String | Criteria to match model instances
RssFeed data = new RssFeed(); // RssFeed | An object of model property name/value pairs
try {
    InlineResponse2002 result = apiInstance.rssFeedUpdateAll(where, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RssFeedApi#rssFeedUpdateAll");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional]
 **data** | [**RssFeed**](RssFeed.md)| An object of model property name/value pairs | [optional]

### Return type

[**InlineResponse2002**](InlineResponse2002.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="rssFeedUpsertPatchRssFeeds"></a>
# **rssFeedUpsertPatchRssFeeds**
> RssFeed rssFeedUpsertPatchRssFeeds(data)

Patch an existing model instance or insert a new one into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.RssFeedApi;

RssFeedApi apiInstance = new RssFeedApi();
RssFeed data = new RssFeed(); // RssFeed | Model instance data
try {
    RssFeed result = apiInstance.rssFeedUpsertPatchRssFeeds(data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RssFeedApi#rssFeedUpsertPatchRssFeeds");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**RssFeed**](RssFeed.md)| Model instance data | [optional]

### Return type

[**RssFeed**](RssFeed.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="rssFeedUpsertPutRssFeeds"></a>
# **rssFeedUpsertPutRssFeeds**
> RssFeed rssFeedUpsertPutRssFeeds(data)

Patch an existing model instance or insert a new one into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.RssFeedApi;

RssFeedApi apiInstance = new RssFeedApi();
RssFeed data = new RssFeed(); // RssFeed | Model instance data
try {
    RssFeed result = apiInstance.rssFeedUpsertPutRssFeeds(data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RssFeedApi#rssFeedUpsertPutRssFeeds");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**RssFeed**](RssFeed.md)| Model instance data | [optional]

### Return type

[**RssFeed**](RssFeed.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="rssFeedUpsertWithWhere"></a>
# **rssFeedUpsertWithWhere**
> RssFeed rssFeedUpsertWithWhere(where, data)

Update an existing model instance or insert a new one into the data source based on the where criteria.

### Example
```java
// Import classes:
//import com.harpoon.api.RssFeedApi;

RssFeedApi apiInstance = new RssFeedApi();
String where = "where_example"; // String | Criteria to match model instances
RssFeed data = new RssFeed(); // RssFeed | An object of model property name/value pairs
try {
    RssFeed result = apiInstance.rssFeedUpsertWithWhere(where, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RssFeedApi#rssFeedUpsertWithWhere");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional]
 **data** | [**RssFeed**](RssFeed.md)| An object of model property name/value pairs | [optional]

### Return type

[**RssFeed**](RssFeed.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

