
# AnonymousModel8

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**brandId** | **Double** |  |  [optional]
**competitionId** | **Double** |  |  [optional]
**couponId** | **Double** |  |  [optional]
**customerId** | **Double** |  |  [optional]
**eventId** | **Double** |  |  [optional]
**dealPaid** | **Double** |  |  [optional]
**dealGroup** | **Double** |  |  [optional]



