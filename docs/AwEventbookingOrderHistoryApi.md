# AwEventbookingOrderHistoryApi

All URIs are relative to *https://apicdn.harpoonconnect.com/v1.1.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**awEventbookingOrderHistoryCount**](AwEventbookingOrderHistoryApi.md#awEventbookingOrderHistoryCount) | **GET** /AwEventbookingOrderHistories/count | Count instances of the model matched by where from the data source.
[**awEventbookingOrderHistoryCreate**](AwEventbookingOrderHistoryApi.md#awEventbookingOrderHistoryCreate) | **POST** /AwEventbookingOrderHistories | Create a new instance of the model and persist it into the data source.
[**awEventbookingOrderHistoryCreateChangeStreamGetAwEventbookingOrderHistoriesChangeStream**](AwEventbookingOrderHistoryApi.md#awEventbookingOrderHistoryCreateChangeStreamGetAwEventbookingOrderHistoriesChangeStream) | **GET** /AwEventbookingOrderHistories/change-stream | Create a change stream.
[**awEventbookingOrderHistoryCreateChangeStreamPostAwEventbookingOrderHistoriesChangeStream**](AwEventbookingOrderHistoryApi.md#awEventbookingOrderHistoryCreateChangeStreamPostAwEventbookingOrderHistoriesChangeStream) | **POST** /AwEventbookingOrderHistories/change-stream | Create a change stream.
[**awEventbookingOrderHistoryDeleteById**](AwEventbookingOrderHistoryApi.md#awEventbookingOrderHistoryDeleteById) | **DELETE** /AwEventbookingOrderHistories/{id} | Delete a model instance by {{id}} from the data source.
[**awEventbookingOrderHistoryExistsGetAwEventbookingOrderHistoriesidExists**](AwEventbookingOrderHistoryApi.md#awEventbookingOrderHistoryExistsGetAwEventbookingOrderHistoriesidExists) | **GET** /AwEventbookingOrderHistories/{id}/exists | Check whether a model instance exists in the data source.
[**awEventbookingOrderHistoryExistsHeadAwEventbookingOrderHistoriesid**](AwEventbookingOrderHistoryApi.md#awEventbookingOrderHistoryExistsHeadAwEventbookingOrderHistoriesid) | **HEAD** /AwEventbookingOrderHistories/{id} | Check whether a model instance exists in the data source.
[**awEventbookingOrderHistoryFind**](AwEventbookingOrderHistoryApi.md#awEventbookingOrderHistoryFind) | **GET** /AwEventbookingOrderHistories | Find all instances of the model matched by filter from the data source.
[**awEventbookingOrderHistoryFindById**](AwEventbookingOrderHistoryApi.md#awEventbookingOrderHistoryFindById) | **GET** /AwEventbookingOrderHistories/{id} | Find a model instance by {{id}} from the data source.
[**awEventbookingOrderHistoryFindOne**](AwEventbookingOrderHistoryApi.md#awEventbookingOrderHistoryFindOne) | **GET** /AwEventbookingOrderHistories/findOne | Find first instance of the model matched by filter from the data source.
[**awEventbookingOrderHistoryPrototypeUpdateAttributesPatchAwEventbookingOrderHistoriesid**](AwEventbookingOrderHistoryApi.md#awEventbookingOrderHistoryPrototypeUpdateAttributesPatchAwEventbookingOrderHistoriesid) | **PATCH** /AwEventbookingOrderHistories/{id} | Patch attributes for a model instance and persist it into the data source.
[**awEventbookingOrderHistoryPrototypeUpdateAttributesPutAwEventbookingOrderHistoriesid**](AwEventbookingOrderHistoryApi.md#awEventbookingOrderHistoryPrototypeUpdateAttributesPutAwEventbookingOrderHistoriesid) | **PUT** /AwEventbookingOrderHistories/{id} | Patch attributes for a model instance and persist it into the data source.
[**awEventbookingOrderHistoryReplaceById**](AwEventbookingOrderHistoryApi.md#awEventbookingOrderHistoryReplaceById) | **POST** /AwEventbookingOrderHistories/{id}/replace | Replace attributes for a model instance and persist it into the data source.
[**awEventbookingOrderHistoryReplaceOrCreate**](AwEventbookingOrderHistoryApi.md#awEventbookingOrderHistoryReplaceOrCreate) | **POST** /AwEventbookingOrderHistories/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
[**awEventbookingOrderHistoryUpdateAll**](AwEventbookingOrderHistoryApi.md#awEventbookingOrderHistoryUpdateAll) | **POST** /AwEventbookingOrderHistories/update | Update instances of the model matched by {{where}} from the data source.
[**awEventbookingOrderHistoryUpsertPatchAwEventbookingOrderHistories**](AwEventbookingOrderHistoryApi.md#awEventbookingOrderHistoryUpsertPatchAwEventbookingOrderHistories) | **PATCH** /AwEventbookingOrderHistories | Patch an existing model instance or insert a new one into the data source.
[**awEventbookingOrderHistoryUpsertPutAwEventbookingOrderHistories**](AwEventbookingOrderHistoryApi.md#awEventbookingOrderHistoryUpsertPutAwEventbookingOrderHistories) | **PUT** /AwEventbookingOrderHistories | Patch an existing model instance or insert a new one into the data source.
[**awEventbookingOrderHistoryUpsertWithWhere**](AwEventbookingOrderHistoryApi.md#awEventbookingOrderHistoryUpsertWithWhere) | **POST** /AwEventbookingOrderHistories/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.


<a name="awEventbookingOrderHistoryCount"></a>
# **awEventbookingOrderHistoryCount**
> InlineResponse2001 awEventbookingOrderHistoryCount(where)

Count instances of the model matched by where from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.AwEventbookingOrderHistoryApi;

AwEventbookingOrderHistoryApi apiInstance = new AwEventbookingOrderHistoryApi();
String where = "where_example"; // String | Criteria to match model instances
try {
    InlineResponse2001 result = apiInstance.awEventbookingOrderHistoryCount(where);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AwEventbookingOrderHistoryApi#awEventbookingOrderHistoryCount");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional]

### Return type

[**InlineResponse2001**](InlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingOrderHistoryCreate"></a>
# **awEventbookingOrderHistoryCreate**
> AwEventbookingOrderHistory awEventbookingOrderHistoryCreate(data)

Create a new instance of the model and persist it into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.AwEventbookingOrderHistoryApi;

AwEventbookingOrderHistoryApi apiInstance = new AwEventbookingOrderHistoryApi();
AwEventbookingOrderHistory data = new AwEventbookingOrderHistory(); // AwEventbookingOrderHistory | Model instance data
try {
    AwEventbookingOrderHistory result = apiInstance.awEventbookingOrderHistoryCreate(data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AwEventbookingOrderHistoryApi#awEventbookingOrderHistoryCreate");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**AwEventbookingOrderHistory**](AwEventbookingOrderHistory.md)| Model instance data | [optional]

### Return type

[**AwEventbookingOrderHistory**](AwEventbookingOrderHistory.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingOrderHistoryCreateChangeStreamGetAwEventbookingOrderHistoriesChangeStream"></a>
# **awEventbookingOrderHistoryCreateChangeStreamGetAwEventbookingOrderHistoriesChangeStream**
> File awEventbookingOrderHistoryCreateChangeStreamGetAwEventbookingOrderHistoriesChangeStream(options)

Create a change stream.

### Example
```java
// Import classes:
//import com.harpoon.api.AwEventbookingOrderHistoryApi;

AwEventbookingOrderHistoryApi apiInstance = new AwEventbookingOrderHistoryApi();
String options = "options_example"; // String | 
try {
    File result = apiInstance.awEventbookingOrderHistoryCreateChangeStreamGetAwEventbookingOrderHistoriesChangeStream(options);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AwEventbookingOrderHistoryApi#awEventbookingOrderHistoryCreateChangeStreamGetAwEventbookingOrderHistoriesChangeStream");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **String**|  | [optional]

### Return type

[**File**](File.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingOrderHistoryCreateChangeStreamPostAwEventbookingOrderHistoriesChangeStream"></a>
# **awEventbookingOrderHistoryCreateChangeStreamPostAwEventbookingOrderHistoriesChangeStream**
> File awEventbookingOrderHistoryCreateChangeStreamPostAwEventbookingOrderHistoriesChangeStream(options)

Create a change stream.

### Example
```java
// Import classes:
//import com.harpoon.api.AwEventbookingOrderHistoryApi;

AwEventbookingOrderHistoryApi apiInstance = new AwEventbookingOrderHistoryApi();
String options = "options_example"; // String | 
try {
    File result = apiInstance.awEventbookingOrderHistoryCreateChangeStreamPostAwEventbookingOrderHistoriesChangeStream(options);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AwEventbookingOrderHistoryApi#awEventbookingOrderHistoryCreateChangeStreamPostAwEventbookingOrderHistoriesChangeStream");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **String**|  | [optional]

### Return type

[**File**](File.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingOrderHistoryDeleteById"></a>
# **awEventbookingOrderHistoryDeleteById**
> Object awEventbookingOrderHistoryDeleteById(id)

Delete a model instance by {{id}} from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.AwEventbookingOrderHistoryApi;

AwEventbookingOrderHistoryApi apiInstance = new AwEventbookingOrderHistoryApi();
String id = "id_example"; // String | Model id
try {
    Object result = apiInstance.awEventbookingOrderHistoryDeleteById(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AwEventbookingOrderHistoryApi#awEventbookingOrderHistoryDeleteById");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |

### Return type

**Object**

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingOrderHistoryExistsGetAwEventbookingOrderHistoriesidExists"></a>
# **awEventbookingOrderHistoryExistsGetAwEventbookingOrderHistoriesidExists**
> InlineResponse2003 awEventbookingOrderHistoryExistsGetAwEventbookingOrderHistoriesidExists(id)

Check whether a model instance exists in the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.AwEventbookingOrderHistoryApi;

AwEventbookingOrderHistoryApi apiInstance = new AwEventbookingOrderHistoryApi();
String id = "id_example"; // String | Model id
try {
    InlineResponse2003 result = apiInstance.awEventbookingOrderHistoryExistsGetAwEventbookingOrderHistoriesidExists(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AwEventbookingOrderHistoryApi#awEventbookingOrderHistoryExistsGetAwEventbookingOrderHistoriesidExists");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |

### Return type

[**InlineResponse2003**](InlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingOrderHistoryExistsHeadAwEventbookingOrderHistoriesid"></a>
# **awEventbookingOrderHistoryExistsHeadAwEventbookingOrderHistoriesid**
> InlineResponse2003 awEventbookingOrderHistoryExistsHeadAwEventbookingOrderHistoriesid(id)

Check whether a model instance exists in the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.AwEventbookingOrderHistoryApi;

AwEventbookingOrderHistoryApi apiInstance = new AwEventbookingOrderHistoryApi();
String id = "id_example"; // String | Model id
try {
    InlineResponse2003 result = apiInstance.awEventbookingOrderHistoryExistsHeadAwEventbookingOrderHistoriesid(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AwEventbookingOrderHistoryApi#awEventbookingOrderHistoryExistsHeadAwEventbookingOrderHistoriesid");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |

### Return type

[**InlineResponse2003**](InlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingOrderHistoryFind"></a>
# **awEventbookingOrderHistoryFind**
> List&lt;AwEventbookingOrderHistory&gt; awEventbookingOrderHistoryFind(filter)

Find all instances of the model matched by filter from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.AwEventbookingOrderHistoryApi;

AwEventbookingOrderHistoryApi apiInstance = new AwEventbookingOrderHistoryApi();
String filter = "filter_example"; // String | Filter defining fields, where, include, order, offset, and limit
try {
    List<AwEventbookingOrderHistory> result = apiInstance.awEventbookingOrderHistoryFind(filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AwEventbookingOrderHistoryApi#awEventbookingOrderHistoryFind");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **String**| Filter defining fields, where, include, order, offset, and limit | [optional]

### Return type

[**List&lt;AwEventbookingOrderHistory&gt;**](AwEventbookingOrderHistory.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingOrderHistoryFindById"></a>
# **awEventbookingOrderHistoryFindById**
> AwEventbookingOrderHistory awEventbookingOrderHistoryFindById(id, filter)

Find a model instance by {{id}} from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.AwEventbookingOrderHistoryApi;

AwEventbookingOrderHistoryApi apiInstance = new AwEventbookingOrderHistoryApi();
String id = "id_example"; // String | Model id
String filter = "filter_example"; // String | Filter defining fields and include
try {
    AwEventbookingOrderHistory result = apiInstance.awEventbookingOrderHistoryFindById(id, filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AwEventbookingOrderHistoryApi#awEventbookingOrderHistoryFindById");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |
 **filter** | **String**| Filter defining fields and include | [optional]

### Return type

[**AwEventbookingOrderHistory**](AwEventbookingOrderHistory.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingOrderHistoryFindOne"></a>
# **awEventbookingOrderHistoryFindOne**
> AwEventbookingOrderHistory awEventbookingOrderHistoryFindOne(filter)

Find first instance of the model matched by filter from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.AwEventbookingOrderHistoryApi;

AwEventbookingOrderHistoryApi apiInstance = new AwEventbookingOrderHistoryApi();
String filter = "filter_example"; // String | Filter defining fields, where, include, order, offset, and limit
try {
    AwEventbookingOrderHistory result = apiInstance.awEventbookingOrderHistoryFindOne(filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AwEventbookingOrderHistoryApi#awEventbookingOrderHistoryFindOne");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **String**| Filter defining fields, where, include, order, offset, and limit | [optional]

### Return type

[**AwEventbookingOrderHistory**](AwEventbookingOrderHistory.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingOrderHistoryPrototypeUpdateAttributesPatchAwEventbookingOrderHistoriesid"></a>
# **awEventbookingOrderHistoryPrototypeUpdateAttributesPatchAwEventbookingOrderHistoriesid**
> AwEventbookingOrderHistory awEventbookingOrderHistoryPrototypeUpdateAttributesPatchAwEventbookingOrderHistoriesid(id, data)

Patch attributes for a model instance and persist it into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.AwEventbookingOrderHistoryApi;

AwEventbookingOrderHistoryApi apiInstance = new AwEventbookingOrderHistoryApi();
String id = "id_example"; // String | AwEventbookingOrderHistory id
AwEventbookingOrderHistory data = new AwEventbookingOrderHistory(); // AwEventbookingOrderHistory | An object of model property name/value pairs
try {
    AwEventbookingOrderHistory result = apiInstance.awEventbookingOrderHistoryPrototypeUpdateAttributesPatchAwEventbookingOrderHistoriesid(id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AwEventbookingOrderHistoryApi#awEventbookingOrderHistoryPrototypeUpdateAttributesPatchAwEventbookingOrderHistoriesid");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| AwEventbookingOrderHistory id |
 **data** | [**AwEventbookingOrderHistory**](AwEventbookingOrderHistory.md)| An object of model property name/value pairs | [optional]

### Return type

[**AwEventbookingOrderHistory**](AwEventbookingOrderHistory.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingOrderHistoryPrototypeUpdateAttributesPutAwEventbookingOrderHistoriesid"></a>
# **awEventbookingOrderHistoryPrototypeUpdateAttributesPutAwEventbookingOrderHistoriesid**
> AwEventbookingOrderHistory awEventbookingOrderHistoryPrototypeUpdateAttributesPutAwEventbookingOrderHistoriesid(id, data)

Patch attributes for a model instance and persist it into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.AwEventbookingOrderHistoryApi;

AwEventbookingOrderHistoryApi apiInstance = new AwEventbookingOrderHistoryApi();
String id = "id_example"; // String | AwEventbookingOrderHistory id
AwEventbookingOrderHistory data = new AwEventbookingOrderHistory(); // AwEventbookingOrderHistory | An object of model property name/value pairs
try {
    AwEventbookingOrderHistory result = apiInstance.awEventbookingOrderHistoryPrototypeUpdateAttributesPutAwEventbookingOrderHistoriesid(id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AwEventbookingOrderHistoryApi#awEventbookingOrderHistoryPrototypeUpdateAttributesPutAwEventbookingOrderHistoriesid");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| AwEventbookingOrderHistory id |
 **data** | [**AwEventbookingOrderHistory**](AwEventbookingOrderHistory.md)| An object of model property name/value pairs | [optional]

### Return type

[**AwEventbookingOrderHistory**](AwEventbookingOrderHistory.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingOrderHistoryReplaceById"></a>
# **awEventbookingOrderHistoryReplaceById**
> AwEventbookingOrderHistory awEventbookingOrderHistoryReplaceById(id, data)

Replace attributes for a model instance and persist it into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.AwEventbookingOrderHistoryApi;

AwEventbookingOrderHistoryApi apiInstance = new AwEventbookingOrderHistoryApi();
String id = "id_example"; // String | Model id
AwEventbookingOrderHistory data = new AwEventbookingOrderHistory(); // AwEventbookingOrderHistory | Model instance data
try {
    AwEventbookingOrderHistory result = apiInstance.awEventbookingOrderHistoryReplaceById(id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AwEventbookingOrderHistoryApi#awEventbookingOrderHistoryReplaceById");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |
 **data** | [**AwEventbookingOrderHistory**](AwEventbookingOrderHistory.md)| Model instance data | [optional]

### Return type

[**AwEventbookingOrderHistory**](AwEventbookingOrderHistory.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingOrderHistoryReplaceOrCreate"></a>
# **awEventbookingOrderHistoryReplaceOrCreate**
> AwEventbookingOrderHistory awEventbookingOrderHistoryReplaceOrCreate(data)

Replace an existing model instance or insert a new one into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.AwEventbookingOrderHistoryApi;

AwEventbookingOrderHistoryApi apiInstance = new AwEventbookingOrderHistoryApi();
AwEventbookingOrderHistory data = new AwEventbookingOrderHistory(); // AwEventbookingOrderHistory | Model instance data
try {
    AwEventbookingOrderHistory result = apiInstance.awEventbookingOrderHistoryReplaceOrCreate(data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AwEventbookingOrderHistoryApi#awEventbookingOrderHistoryReplaceOrCreate");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**AwEventbookingOrderHistory**](AwEventbookingOrderHistory.md)| Model instance data | [optional]

### Return type

[**AwEventbookingOrderHistory**](AwEventbookingOrderHistory.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingOrderHistoryUpdateAll"></a>
# **awEventbookingOrderHistoryUpdateAll**
> InlineResponse2002 awEventbookingOrderHistoryUpdateAll(where, data)

Update instances of the model matched by {{where}} from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.AwEventbookingOrderHistoryApi;

AwEventbookingOrderHistoryApi apiInstance = new AwEventbookingOrderHistoryApi();
String where = "where_example"; // String | Criteria to match model instances
AwEventbookingOrderHistory data = new AwEventbookingOrderHistory(); // AwEventbookingOrderHistory | An object of model property name/value pairs
try {
    InlineResponse2002 result = apiInstance.awEventbookingOrderHistoryUpdateAll(where, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AwEventbookingOrderHistoryApi#awEventbookingOrderHistoryUpdateAll");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional]
 **data** | [**AwEventbookingOrderHistory**](AwEventbookingOrderHistory.md)| An object of model property name/value pairs | [optional]

### Return type

[**InlineResponse2002**](InlineResponse2002.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingOrderHistoryUpsertPatchAwEventbookingOrderHistories"></a>
# **awEventbookingOrderHistoryUpsertPatchAwEventbookingOrderHistories**
> AwEventbookingOrderHistory awEventbookingOrderHistoryUpsertPatchAwEventbookingOrderHistories(data)

Patch an existing model instance or insert a new one into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.AwEventbookingOrderHistoryApi;

AwEventbookingOrderHistoryApi apiInstance = new AwEventbookingOrderHistoryApi();
AwEventbookingOrderHistory data = new AwEventbookingOrderHistory(); // AwEventbookingOrderHistory | Model instance data
try {
    AwEventbookingOrderHistory result = apiInstance.awEventbookingOrderHistoryUpsertPatchAwEventbookingOrderHistories(data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AwEventbookingOrderHistoryApi#awEventbookingOrderHistoryUpsertPatchAwEventbookingOrderHistories");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**AwEventbookingOrderHistory**](AwEventbookingOrderHistory.md)| Model instance data | [optional]

### Return type

[**AwEventbookingOrderHistory**](AwEventbookingOrderHistory.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingOrderHistoryUpsertPutAwEventbookingOrderHistories"></a>
# **awEventbookingOrderHistoryUpsertPutAwEventbookingOrderHistories**
> AwEventbookingOrderHistory awEventbookingOrderHistoryUpsertPutAwEventbookingOrderHistories(data)

Patch an existing model instance or insert a new one into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.AwEventbookingOrderHistoryApi;

AwEventbookingOrderHistoryApi apiInstance = new AwEventbookingOrderHistoryApi();
AwEventbookingOrderHistory data = new AwEventbookingOrderHistory(); // AwEventbookingOrderHistory | Model instance data
try {
    AwEventbookingOrderHistory result = apiInstance.awEventbookingOrderHistoryUpsertPutAwEventbookingOrderHistories(data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AwEventbookingOrderHistoryApi#awEventbookingOrderHistoryUpsertPutAwEventbookingOrderHistories");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**AwEventbookingOrderHistory**](AwEventbookingOrderHistory.md)| Model instance data | [optional]

### Return type

[**AwEventbookingOrderHistory**](AwEventbookingOrderHistory.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="awEventbookingOrderHistoryUpsertWithWhere"></a>
# **awEventbookingOrderHistoryUpsertWithWhere**
> AwEventbookingOrderHistory awEventbookingOrderHistoryUpsertWithWhere(where, data)

Update an existing model instance or insert a new one into the data source based on the where criteria.

### Example
```java
// Import classes:
//import com.harpoon.api.AwEventbookingOrderHistoryApi;

AwEventbookingOrderHistoryApi apiInstance = new AwEventbookingOrderHistoryApi();
String where = "where_example"; // String | Criteria to match model instances
AwEventbookingOrderHistory data = new AwEventbookingOrderHistory(); // AwEventbookingOrderHistory | An object of model property name/value pairs
try {
    AwEventbookingOrderHistory result = apiInstance.awEventbookingOrderHistoryUpsertWithWhere(where, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AwEventbookingOrderHistoryApi#awEventbookingOrderHistoryUpsertWithWhere");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional]
 **data** | [**AwEventbookingOrderHistory**](AwEventbookingOrderHistory.md)| An object of model property name/value pairs | [optional]

### Return type

[**AwEventbookingOrderHistory**](AwEventbookingOrderHistory.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

