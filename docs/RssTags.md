
# RssTags

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**imgTumbnail** | **String** |  |  [optional]
**imgMain** | **String** |  |  [optional]
**link** | **String** |  |  [optional]
**postDateTime** | **String** |  |  [optional]
**description** | **String** |  |  [optional]
**category** | **String** |  |  [optional]
**title** | **String** |  |  [optional]
**id** | **Double** |  |  [optional]



