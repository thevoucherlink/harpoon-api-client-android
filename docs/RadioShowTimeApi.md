# RadioShowTimeApi

All URIs are relative to *https://apicdn.harpoonconnect.com/v1.1.1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**radioShowTimeCount**](RadioShowTimeApi.md#radioShowTimeCount) | **GET** /RadioShowTimes/count | Count instances of the model matched by where from the data source.
[**radioShowTimeCreate**](RadioShowTimeApi.md#radioShowTimeCreate) | **POST** /RadioShowTimes | Create a new instance of the model and persist it into the data source.
[**radioShowTimeCreateChangeStreamGetRadioShowTimesChangeStream**](RadioShowTimeApi.md#radioShowTimeCreateChangeStreamGetRadioShowTimesChangeStream) | **GET** /RadioShowTimes/change-stream | Create a change stream.
[**radioShowTimeCreateChangeStreamPostRadioShowTimesChangeStream**](RadioShowTimeApi.md#radioShowTimeCreateChangeStreamPostRadioShowTimesChangeStream) | **POST** /RadioShowTimes/change-stream | Create a change stream.
[**radioShowTimeDeleteById**](RadioShowTimeApi.md#radioShowTimeDeleteById) | **DELETE** /RadioShowTimes/{id} | Delete a model instance by {{id}} from the data source.
[**radioShowTimeExistsGetRadioShowTimesidExists**](RadioShowTimeApi.md#radioShowTimeExistsGetRadioShowTimesidExists) | **GET** /RadioShowTimes/{id}/exists | Check whether a model instance exists in the data source.
[**radioShowTimeExistsHeadRadioShowTimesid**](RadioShowTimeApi.md#radioShowTimeExistsHeadRadioShowTimesid) | **HEAD** /RadioShowTimes/{id} | Check whether a model instance exists in the data source.
[**radioShowTimeFind**](RadioShowTimeApi.md#radioShowTimeFind) | **GET** /RadioShowTimes | Find all instances of the model matched by filter from the data source.
[**radioShowTimeFindById**](RadioShowTimeApi.md#radioShowTimeFindById) | **GET** /RadioShowTimes/{id} | Find a model instance by {{id}} from the data source.
[**radioShowTimeFindOne**](RadioShowTimeApi.md#radioShowTimeFindOne) | **GET** /RadioShowTimes/findOne | Find first instance of the model matched by filter from the data source.
[**radioShowTimePrototypeGetRadioShow**](RadioShowTimeApi.md#radioShowTimePrototypeGetRadioShow) | **GET** /RadioShowTimes/{id}/radioShow | Fetches belongsTo relation radioShow.
[**radioShowTimePrototypeUpdateAttributesPatchRadioShowTimesid**](RadioShowTimeApi.md#radioShowTimePrototypeUpdateAttributesPatchRadioShowTimesid) | **PATCH** /RadioShowTimes/{id} | Patch attributes for a model instance and persist it into the data source.
[**radioShowTimePrototypeUpdateAttributesPutRadioShowTimesid**](RadioShowTimeApi.md#radioShowTimePrototypeUpdateAttributesPutRadioShowTimesid) | **PUT** /RadioShowTimes/{id} | Patch attributes for a model instance and persist it into the data source.
[**radioShowTimeReplaceById**](RadioShowTimeApi.md#radioShowTimeReplaceById) | **POST** /RadioShowTimes/{id}/replace | Replace attributes for a model instance and persist it into the data source.
[**radioShowTimeReplaceOrCreate**](RadioShowTimeApi.md#radioShowTimeReplaceOrCreate) | **POST** /RadioShowTimes/replaceOrCreate | Replace an existing model instance or insert a new one into the data source.
[**radioShowTimeUpdateAll**](RadioShowTimeApi.md#radioShowTimeUpdateAll) | **POST** /RadioShowTimes/update | Update instances of the model matched by {{where}} from the data source.
[**radioShowTimeUpsertPatchRadioShowTimes**](RadioShowTimeApi.md#radioShowTimeUpsertPatchRadioShowTimes) | **PATCH** /RadioShowTimes | Patch an existing model instance or insert a new one into the data source.
[**radioShowTimeUpsertPutRadioShowTimes**](RadioShowTimeApi.md#radioShowTimeUpsertPutRadioShowTimes) | **PUT** /RadioShowTimes | Patch an existing model instance or insert a new one into the data source.
[**radioShowTimeUpsertWithWhere**](RadioShowTimeApi.md#radioShowTimeUpsertWithWhere) | **POST** /RadioShowTimes/upsertWithWhere | Update an existing model instance or insert a new one into the data source based on the where criteria.


<a name="radioShowTimeCount"></a>
# **radioShowTimeCount**
> InlineResponse2001 radioShowTimeCount(where)

Count instances of the model matched by where from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.RadioShowTimeApi;

RadioShowTimeApi apiInstance = new RadioShowTimeApi();
String where = "where_example"; // String | Criteria to match model instances
try {
    InlineResponse2001 result = apiInstance.radioShowTimeCount(where);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RadioShowTimeApi#radioShowTimeCount");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional]

### Return type

[**InlineResponse2001**](InlineResponse2001.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioShowTimeCreate"></a>
# **radioShowTimeCreate**
> RadioShowTime radioShowTimeCreate(data)

Create a new instance of the model and persist it into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.RadioShowTimeApi;

RadioShowTimeApi apiInstance = new RadioShowTimeApi();
RadioShowTime data = new RadioShowTime(); // RadioShowTime | Model instance data
try {
    RadioShowTime result = apiInstance.radioShowTimeCreate(data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RadioShowTimeApi#radioShowTimeCreate");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**RadioShowTime**](RadioShowTime.md)| Model instance data | [optional]

### Return type

[**RadioShowTime**](RadioShowTime.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioShowTimeCreateChangeStreamGetRadioShowTimesChangeStream"></a>
# **radioShowTimeCreateChangeStreamGetRadioShowTimesChangeStream**
> File radioShowTimeCreateChangeStreamGetRadioShowTimesChangeStream(options)

Create a change stream.

### Example
```java
// Import classes:
//import com.harpoon.api.RadioShowTimeApi;

RadioShowTimeApi apiInstance = new RadioShowTimeApi();
String options = "options_example"; // String | 
try {
    File result = apiInstance.radioShowTimeCreateChangeStreamGetRadioShowTimesChangeStream(options);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RadioShowTimeApi#radioShowTimeCreateChangeStreamGetRadioShowTimesChangeStream");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **String**|  | [optional]

### Return type

[**File**](File.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioShowTimeCreateChangeStreamPostRadioShowTimesChangeStream"></a>
# **radioShowTimeCreateChangeStreamPostRadioShowTimesChangeStream**
> File radioShowTimeCreateChangeStreamPostRadioShowTimesChangeStream(options)

Create a change stream.

### Example
```java
// Import classes:
//import com.harpoon.api.RadioShowTimeApi;

RadioShowTimeApi apiInstance = new RadioShowTimeApi();
String options = "options_example"; // String | 
try {
    File result = apiInstance.radioShowTimeCreateChangeStreamPostRadioShowTimesChangeStream(options);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RadioShowTimeApi#radioShowTimeCreateChangeStreamPostRadioShowTimesChangeStream");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **options** | **String**|  | [optional]

### Return type

[**File**](File.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioShowTimeDeleteById"></a>
# **radioShowTimeDeleteById**
> Object radioShowTimeDeleteById(id)

Delete a model instance by {{id}} from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.RadioShowTimeApi;

RadioShowTimeApi apiInstance = new RadioShowTimeApi();
String id = "id_example"; // String | Model id
try {
    Object result = apiInstance.radioShowTimeDeleteById(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RadioShowTimeApi#radioShowTimeDeleteById");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |

### Return type

**Object**

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioShowTimeExistsGetRadioShowTimesidExists"></a>
# **radioShowTimeExistsGetRadioShowTimesidExists**
> InlineResponse2003 radioShowTimeExistsGetRadioShowTimesidExists(id)

Check whether a model instance exists in the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.RadioShowTimeApi;

RadioShowTimeApi apiInstance = new RadioShowTimeApi();
String id = "id_example"; // String | Model id
try {
    InlineResponse2003 result = apiInstance.radioShowTimeExistsGetRadioShowTimesidExists(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RadioShowTimeApi#radioShowTimeExistsGetRadioShowTimesidExists");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |

### Return type

[**InlineResponse2003**](InlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioShowTimeExistsHeadRadioShowTimesid"></a>
# **radioShowTimeExistsHeadRadioShowTimesid**
> InlineResponse2003 radioShowTimeExistsHeadRadioShowTimesid(id)

Check whether a model instance exists in the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.RadioShowTimeApi;

RadioShowTimeApi apiInstance = new RadioShowTimeApi();
String id = "id_example"; // String | Model id
try {
    InlineResponse2003 result = apiInstance.radioShowTimeExistsHeadRadioShowTimesid(id);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RadioShowTimeApi#radioShowTimeExistsHeadRadioShowTimesid");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |

### Return type

[**InlineResponse2003**](InlineResponse2003.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioShowTimeFind"></a>
# **radioShowTimeFind**
> List&lt;RadioShowTime&gt; radioShowTimeFind(filter)

Find all instances of the model matched by filter from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.RadioShowTimeApi;

RadioShowTimeApi apiInstance = new RadioShowTimeApi();
String filter = "filter_example"; // String | Filter defining fields, where, include, order, offset, and limit
try {
    List<RadioShowTime> result = apiInstance.radioShowTimeFind(filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RadioShowTimeApi#radioShowTimeFind");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **String**| Filter defining fields, where, include, order, offset, and limit | [optional]

### Return type

[**List&lt;RadioShowTime&gt;**](RadioShowTime.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioShowTimeFindById"></a>
# **radioShowTimeFindById**
> RadioShowTime radioShowTimeFindById(id, filter)

Find a model instance by {{id}} from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.RadioShowTimeApi;

RadioShowTimeApi apiInstance = new RadioShowTimeApi();
String id = "id_example"; // String | Model id
String filter = "filter_example"; // String | Filter defining fields and include
try {
    RadioShowTime result = apiInstance.radioShowTimeFindById(id, filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RadioShowTimeApi#radioShowTimeFindById");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |
 **filter** | **String**| Filter defining fields and include | [optional]

### Return type

[**RadioShowTime**](RadioShowTime.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioShowTimeFindOne"></a>
# **radioShowTimeFindOne**
> RadioShowTime radioShowTimeFindOne(filter)

Find first instance of the model matched by filter from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.RadioShowTimeApi;

RadioShowTimeApi apiInstance = new RadioShowTimeApi();
String filter = "filter_example"; // String | Filter defining fields, where, include, order, offset, and limit
try {
    RadioShowTime result = apiInstance.radioShowTimeFindOne(filter);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RadioShowTimeApi#radioShowTimeFindOne");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | **String**| Filter defining fields, where, include, order, offset, and limit | [optional]

### Return type

[**RadioShowTime**](RadioShowTime.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioShowTimePrototypeGetRadioShow"></a>
# **radioShowTimePrototypeGetRadioShow**
> RadioShow radioShowTimePrototypeGetRadioShow(id, refresh)

Fetches belongsTo relation radioShow.

### Example
```java
// Import classes:
//import com.harpoon.api.RadioShowTimeApi;

RadioShowTimeApi apiInstance = new RadioShowTimeApi();
String id = "id_example"; // String | RadioShowTime id
Boolean refresh = true; // Boolean | 
try {
    RadioShow result = apiInstance.radioShowTimePrototypeGetRadioShow(id, refresh);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RadioShowTimeApi#radioShowTimePrototypeGetRadioShow");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| RadioShowTime id |
 **refresh** | **Boolean**|  | [optional]

### Return type

[**RadioShow**](RadioShow.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioShowTimePrototypeUpdateAttributesPatchRadioShowTimesid"></a>
# **radioShowTimePrototypeUpdateAttributesPatchRadioShowTimesid**
> RadioShowTime radioShowTimePrototypeUpdateAttributesPatchRadioShowTimesid(id, data)

Patch attributes for a model instance and persist it into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.RadioShowTimeApi;

RadioShowTimeApi apiInstance = new RadioShowTimeApi();
String id = "id_example"; // String | RadioShowTime id
RadioShowTime data = new RadioShowTime(); // RadioShowTime | An object of model property name/value pairs
try {
    RadioShowTime result = apiInstance.radioShowTimePrototypeUpdateAttributesPatchRadioShowTimesid(id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RadioShowTimeApi#radioShowTimePrototypeUpdateAttributesPatchRadioShowTimesid");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| RadioShowTime id |
 **data** | [**RadioShowTime**](RadioShowTime.md)| An object of model property name/value pairs | [optional]

### Return type

[**RadioShowTime**](RadioShowTime.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioShowTimePrototypeUpdateAttributesPutRadioShowTimesid"></a>
# **radioShowTimePrototypeUpdateAttributesPutRadioShowTimesid**
> RadioShowTime radioShowTimePrototypeUpdateAttributesPutRadioShowTimesid(id, data)

Patch attributes for a model instance and persist it into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.RadioShowTimeApi;

RadioShowTimeApi apiInstance = new RadioShowTimeApi();
String id = "id_example"; // String | RadioShowTime id
RadioShowTime data = new RadioShowTime(); // RadioShowTime | An object of model property name/value pairs
try {
    RadioShowTime result = apiInstance.radioShowTimePrototypeUpdateAttributesPutRadioShowTimesid(id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RadioShowTimeApi#radioShowTimePrototypeUpdateAttributesPutRadioShowTimesid");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| RadioShowTime id |
 **data** | [**RadioShowTime**](RadioShowTime.md)| An object of model property name/value pairs | [optional]

### Return type

[**RadioShowTime**](RadioShowTime.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioShowTimeReplaceById"></a>
# **radioShowTimeReplaceById**
> RadioShowTime radioShowTimeReplaceById(id, data)

Replace attributes for a model instance and persist it into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.RadioShowTimeApi;

RadioShowTimeApi apiInstance = new RadioShowTimeApi();
String id = "id_example"; // String | Model id
RadioShowTime data = new RadioShowTime(); // RadioShowTime | Model instance data
try {
    RadioShowTime result = apiInstance.radioShowTimeReplaceById(id, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RadioShowTimeApi#radioShowTimeReplaceById");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| Model id |
 **data** | [**RadioShowTime**](RadioShowTime.md)| Model instance data | [optional]

### Return type

[**RadioShowTime**](RadioShowTime.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioShowTimeReplaceOrCreate"></a>
# **radioShowTimeReplaceOrCreate**
> RadioShowTime radioShowTimeReplaceOrCreate(data)

Replace an existing model instance or insert a new one into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.RadioShowTimeApi;

RadioShowTimeApi apiInstance = new RadioShowTimeApi();
RadioShowTime data = new RadioShowTime(); // RadioShowTime | Model instance data
try {
    RadioShowTime result = apiInstance.radioShowTimeReplaceOrCreate(data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RadioShowTimeApi#radioShowTimeReplaceOrCreate");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**RadioShowTime**](RadioShowTime.md)| Model instance data | [optional]

### Return type

[**RadioShowTime**](RadioShowTime.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioShowTimeUpdateAll"></a>
# **radioShowTimeUpdateAll**
> InlineResponse2002 radioShowTimeUpdateAll(where, data)

Update instances of the model matched by {{where}} from the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.RadioShowTimeApi;

RadioShowTimeApi apiInstance = new RadioShowTimeApi();
String where = "where_example"; // String | Criteria to match model instances
RadioShowTime data = new RadioShowTime(); // RadioShowTime | An object of model property name/value pairs
try {
    InlineResponse2002 result = apiInstance.radioShowTimeUpdateAll(where, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RadioShowTimeApi#radioShowTimeUpdateAll");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional]
 **data** | [**RadioShowTime**](RadioShowTime.md)| An object of model property name/value pairs | [optional]

### Return type

[**InlineResponse2002**](InlineResponse2002.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioShowTimeUpsertPatchRadioShowTimes"></a>
# **radioShowTimeUpsertPatchRadioShowTimes**
> RadioShowTime radioShowTimeUpsertPatchRadioShowTimes(data)

Patch an existing model instance or insert a new one into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.RadioShowTimeApi;

RadioShowTimeApi apiInstance = new RadioShowTimeApi();
RadioShowTime data = new RadioShowTime(); // RadioShowTime | Model instance data
try {
    RadioShowTime result = apiInstance.radioShowTimeUpsertPatchRadioShowTimes(data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RadioShowTimeApi#radioShowTimeUpsertPatchRadioShowTimes");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**RadioShowTime**](RadioShowTime.md)| Model instance data | [optional]

### Return type

[**RadioShowTime**](RadioShowTime.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioShowTimeUpsertPutRadioShowTimes"></a>
# **radioShowTimeUpsertPutRadioShowTimes**
> RadioShowTime radioShowTimeUpsertPutRadioShowTimes(data)

Patch an existing model instance or insert a new one into the data source.

### Example
```java
// Import classes:
//import com.harpoon.api.RadioShowTimeApi;

RadioShowTimeApi apiInstance = new RadioShowTimeApi();
RadioShowTime data = new RadioShowTime(); // RadioShowTime | Model instance data
try {
    RadioShowTime result = apiInstance.radioShowTimeUpsertPutRadioShowTimes(data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RadioShowTimeApi#radioShowTimeUpsertPutRadioShowTimes");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**RadioShowTime**](RadioShowTime.md)| Model instance data | [optional]

### Return type

[**RadioShowTime**](RadioShowTime.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

<a name="radioShowTimeUpsertWithWhere"></a>
# **radioShowTimeUpsertWithWhere**
> RadioShowTime radioShowTimeUpsertWithWhere(where, data)

Update an existing model instance or insert a new one into the data source based on the where criteria.

### Example
```java
// Import classes:
//import com.harpoon.api.RadioShowTimeApi;

RadioShowTimeApi apiInstance = new RadioShowTimeApi();
String where = "where_example"; // String | Criteria to match model instances
RadioShowTime data = new RadioShowTime(); // RadioShowTime | An object of model property name/value pairs
try {
    RadioShowTime result = apiInstance.radioShowTimeUpsertWithWhere(where, data);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling RadioShowTimeApi#radioShowTimeUpsertWithWhere");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **where** | **String**| Criteria to match model instances | [optional]
 **data** | [**RadioShowTime**](RadioShowTime.md)| An object of model property name/value pairs | [optional]

### Return type

[**RadioShowTime**](RadioShowTime.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

 - **Content-Type**: application/json, application/x-www-form-urlencoded, application/xml, text/xml
 - **Accept**: application/json, application/xml, text/xml, application/javascript, text/javascript

