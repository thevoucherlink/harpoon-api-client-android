
# AwCollpurDeal

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Double** |  | 
**productId** | **Double** |  | 
**productName** | **String** |  | 
**storeIds** | **String** |  | 
**isActive** | **Double** |  | 
**isSuccess** | **Double** |  | 
**closeState** | **Double** |  | 
**qtyToReachDeal** | **Double** |  | 
**purchasesLeft** | **Double** |  | 
**maximumAllowedPurchases** | **Double** |  | 
**availableFrom** | [**Date**](Date.md) |  |  [optional]
**availableTo** | [**Date**](Date.md) |  |  [optional]
**price** | **String** |  | 
**autoClose** | **Double** |  | 
**name** | **String** |  | 
**description** | **String** |  | 
**fullDescription** | **String** |  |  [optional]
**dealImage** | **String** |  | 
**isFeatured** | **Double** |  |  [optional]
**enableCoupons** | **Double** |  | 
**couponPrefix** | **String** |  | 
**couponExpireAfterDays** | **Double** |  |  [optional]
**expiredFlag** | **Double** |  |  [optional]
**sentBeforeFlag** | **Double** |  |  [optional]
**isSuccessedFlag** | **Double** |  |  [optional]
**awCollpurDealPurchases** | **List&lt;Object&gt;** |  |  [optional]



